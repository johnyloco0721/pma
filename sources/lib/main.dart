import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:receive_sharing_intent/receive_sharing_intent.dart';
import 'package:sources/args/task_args.dart';
import 'package:sources/args/workplace_args.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/constants/app_data_models.dart';
import 'package:sources/models/alert_post_model.dart';
import 'package:sources/services/alert_service.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/error_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/view_models/app_view_model.dart';
import 'package:sources/views/auth/login_view.dart';
import 'package:sources/views/auth/reset_password_view.dart';
import 'package:sources/home_view.dart';
import 'package:sources/views/load_data_screen.dart';
import 'package:sources/views/miscellaneous/app_maintenance_view.dart';
import 'package:sources/views/miscellaneous/app_update_view.dart';
import 'package:sources/views/miscellaneous/os_minimum_requirement_view.dart';
import 'package:sources/views/splash_screen.dart';
import 'package:sources/widgets/custom_mentions/custom_mention_view.dart';

// We use this to `show` new notifications from the app (instead of from firebase)
// You'll want to be sending high priority `data` messages through fcm, not notifications
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
int appBadgeCount = 0;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // Initialize Firebase Services
  await Firebase.initializeApp();

  AndroidInitializationSettings initializationSettingsAndroid = AndroidInitializationSettings('mipmap/launcher_icon');

  /// Note: permissions aren't requested here just to demonstrate that can be done later
  final DarwinInitializationSettings initializationSettingsIOS = DarwinInitializationSettings(
    requestAlertPermission: false,
    requestBadgePermission: false,
    requestSoundPermission: false,
    onDidReceiveLocalNotification: (int id, String? title, String? body, String? payload) async {}
  );

  var initializationSettings = InitializationSettings(
    android: initializationSettingsAndroid,
    iOS: initializationSettingsIOS
  );
  await flutterLocalNotificationsPlugin.initialize(initializationSettings);
  const AndroidNotificationChannel channel = AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    description: 'This channel is used for important notifications.',
    importance: Importance.max,
  );

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  await flutterLocalNotificationsPlugin.initialize(initializationSettings);

  FirebaseMessaging.onBackgroundMessage(_messageHandler);

  // Initialize GetIt Singleton for AppDatabase
  await setupLocator();
  await setupBackgroundDB();

  await SpManager.getInstance();

  FirebaseMessaging.instance.getToken().then((token) async {
    print(token);
    try {
      if (AppDataModel.getByPassCode()) {
        await Api().postHTTP('RegisterDevice?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', '{ "token" : "$token" }', checking: false);
      }else{
        await Api().postHTTP('RegisterDevice', '{ "token" : "$token" }', checking: false);
      }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  });
  runApp(MyApp());
}

Future<void> _messageHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print('Handling a background message');
  appBadgeFunction("increase");
}

void appBadgeFunction(String function) async {
  try {
    bool res = await FlutterAppBadger.isAppBadgeSupported();
    if (res) {
      if (function == "increase") {
        appBadgeCount++;
        print(appBadgeCount);
        FlutterAppBadger.updateBadgeCount(appBadgeCount);
      } else {
        FlutterAppBadger.removeBadge();
        await flutterLocalNotificationsPlugin.cancelAll();
      }
    } else {
      print('App Badge not supported');
    }
  } on PlatformException {
    print('App Badge not supported');
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Portal(
      child: KeyboardDismisser(
        gestures: [
          GestureType.onTap,
          GestureType.onPanUpdateDownDirection,
        ],
        child: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver {
  AppViewModel appViewModel = locator<AppViewModel>();
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<List<ConnectivityResult>> _connectivitySubscription;
  late StreamSubscription _intentDataStreamSubscription;
  List<SharedMediaFile>? _sharedFiles;

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    _intentDataStreamSubscription.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    
    // Add observer to detect app state: inactive, paused, resumed
    WidgetsBinding.instance.addObserver(this);

    initPermissionRequest();

    receiveSharingItems();

    getMessageNotifications();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
      appBadgeFunction("clear");
    }
  }

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      builder: (ctx, child) {
        final data = MediaQuery.of(ctx);
        return MediaQuery(
          data: data.copyWith(textScaleFactor: data.textScaleFactor > 1.35 ? 1.35 : data.textScaleFactor),
          child: child!,
        );
      },
      navigatorKey: appViewModel.mainNavigatorKey,
      home: SplashScreen(),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        loginScreen: (context) => LoginView(),
        appMaintenanceScreen: (context) => MaintenanceView(),
        appUpdateScreen: (context) => UpdateView(),
        IOSosRequireScreen: (context) => OSRequireView(platform: 'ios'),
        AndroidosRequireScreen: (context) => OSRequireView(platform: 'android'),
        forgotPasswordScreen: (context) => ResetPasswordView(),
        homeScreen: (context) => HomeView(),
        loadDataScreen: (context) => LoadDataScreen(),
      },
      onGenerateRoute: (settings) {
        if (settings.name == errorLoginScreen) {
          final args = settings.arguments as String;
          return MaterialPageRoute(builder: (_) => LoginView(errorMessage: args));
        }
        return null;
      }
    );
  }

  initPermissionRequest() async {
    try {
      if (Platform.isAndroid) {
        await Permission.notification.request();
      }
    } on PlatformException catch (e) {
      ErrorUtils.handlePlatformError(context, e);
    }
    try {
      if (Platform.isAndroid) {
        await Api().requestPermission(Permission.storage);
      } else if (Platform.isIOS) {
        await Api().requestPermission(Permission.photos);
      }
    } on PlatformException catch (e) {
      ErrorUtils.handlePlatformError(context, e);
    }
    try {
      await Api().requestPermission(Permission.camera);
    } on PlatformException catch (e) {
      ErrorUtils.handlePlatformError(context, e);
    }
    // try {
    //   bool isAccessGranted = await FlutterMute.isNotificationPolicyAccessGranted;

    //   if (!isAccessGranted) {
    //     // Opens the notification settings to grant the access.
    //     await FlutterMute.openNotificationPolicySettings();
    //   }
    // } on PlatformException catch (e) {
    //   ErrorUtils.handlePlatformError(context, e);
    // }
  }

  receiveSharingItems() {
    // For sharing images coming from outside the app while the app is in the memory
    _intentDataStreamSubscription = ReceiveSharingIntent.instance.getMediaStream()
        .listen((List<SharedMediaFile> sharedFiles) {
      setState(() {
        _sharedFiles = sharedFiles;
        // used for debug
        // print("Shared:" + (_sharedFiles?.map((f) => f.path).join(",") ?? ""));
        appViewModel
          .tabsNavigatorKey[0]
          .currentState!
          .pushNamedAndRemoveUntil('workplaceList', (route) => false, 
              arguments: WorkplaceArguments(null, null, null, null, (_sharedFiles?.map((f) => f.path) ?? []).toList()));
      });
    }, onError: (err) {
      print("getIntentDataStream error: $err");
    });

    // For sharing images coming from outside the app while the app is closed
    ReceiveSharingIntent.instance.getInitialMedia().then((List<SharedMediaFile> sharedFiles) {
      setState(() {
        _sharedFiles = sharedFiles;
        // used for debug
        // print("Shared:" + (_sharedFiles?.map((f) => f.path).join(",") ?? ""));
        if (appViewModel.bottomBarCurrentIndex.value != 0) {
          HomeView.of(appViewModel.tabsNavigatorKey[appViewModel.bottomBarCurrentIndex.value].currentContext!)!.navigateFromRootToPage('workplaceList', null, null, null, (_sharedFiles?.map((f) => f.path) ?? []).toList());
        }
      });
    });
  }

  getMessageNotifications() async {
    await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );

    //When the app is terminated
    FirebaseMessaging.instance.getInitialMessage().then((RemoteMessage? message) {
      if (message != null) {
        print("terminated");
        appBadgeFunction("increase");
        SpManager.saveFcmPayload(jsonEncode(message.data));
        // handleNotificationClick(message, 'onLauch');
      }
    });

    if (Platform.isIOS) {
      NotificationSettings settings = await FirebaseMessaging.instance
          .requestPermission(
              alert: true,
              announcement: false,
              badge: true,
              carPlay: false,
              criticalAlert: false,
              sound: true,
              provisional: false);
      print("user granted permission : ${settings.authorizationStatus}");
    }

    // Receive push notification and show notification bubble
    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      RemoteNotification? notification = event.notification;
      AndroidNotification? android = event.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
          notification.hashCode,
          notification.title,
          notification.body,
          NotificationDetails(
            android: AndroidNotificationDetails(
              'high_importance_channel',
              'High Importance Notifications',
              channelDescription: 'This channel is used for important notifications.',
              icon: android.smallIcon,
            ),
          ),
        );
      }
    });
    
    // When the app is in the foreground or background, but not terminated.
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      handleNotificationClick(message, 'onResume');
    });
  }

  handleNotificationClick(RemoteMessage message, String debugMessage) {
    print(message.data);
    // Redirect to next route once widget built completed
    AlertService().updateNotificationStatus(AlertPostModel(message.data['notificationID'], message.data['allNotificationTxt']));
    switch (message.data['moduleName']) {
      case "workplace":
        // go to chat page
        notificationRedirection(
          'workplaceChat', 
          appViewModel.bottomBarCurrentIndex.value, 
          toIndex: 0, 
          args: WorkplaceArguments(int.parse(message.data['projectID']), null, null, null, null)
        );
        break;
      case "workplace-reply":
        // if (appViewModel.bottomBarCurrentIndex.value == 0) {
          // when the current index equals 0, then use 'pushNamedAndRemoveUntil' to replace a new route
        // go to chat page
        notificationRedirection(
          'workplaceChat', 
          appViewModel.bottomBarCurrentIndex.value, 
          toIndex: 0, 
          args: WorkplaceArguments(int.parse(message.data['projectID']), int.parse(message.data['replyToID']), null, null, null)
        );
        //   appViewModel
        //     .tabsNavigatorKey[0]
        //     .currentState!
        //     .pushNamedAndRemoveUntil('workplaceList', (route) => false,
        //         arguments: WorkplaceArguments(int.parse(message.data['projectID']), int.parse(message.data['replyToID']), null, null));
        // } else {
        //   // should use tabsNavigatorKey to found current homeView context then called the 'navigateFromRootToPage' function to switch the bottom bar index
        //   HomeView.of(appViewModel.tabsNavigatorKey[appViewModel.bottomBarCurrentIndex.value].currentContext!)!.navigateFromRootToPage('workplaceList', int.parse(message.data['projectID']), int.parse(message.data['replyToID']), null, null);
        // }
        break;
      case "task-by-project--list-all":
      case "task-by-project--list-incomplete":
      case "task-by-project--list-complete":
        // go to chat page
        notificationRedirection(
          'workplaceChat', 
          appViewModel.bottomBarCurrentIndex.value, 
          toIndex: 0, 
          args: WorkplaceArguments(int.parse(message.data['projectID']), null, message.data['moduleName'], null, null)
        );
        break;
      case "task-details":
        // go to task details page
        notificationRedirection(taskMainScreen, appViewModel.bottomBarCurrentIndex.value, toIndex: 1, args: TaskArguments(projectID: int.parse(message.data['projectID']), taskID: int.parse(message.data['affectedModuleID'])));
        break;
      case "task-milestone":
        // go to task milestone page
        notificationRedirection(taskMainScreen, appViewModel.bottomBarCurrentIndex.value, toIndex: 1, args: TaskArguments(projectID: int.parse(message.data['affectedModuleID'])));
        break;
      case "task-comment":
        // go to comment page
        notificationRedirection(taskMainScreen, appViewModel.bottomBarCurrentIndex.value, toIndex: 1, args: TaskArguments(projectID: int.parse(message.data['projectID']), taskID: int.parse(message.data['affectedModuleID']), moduleName: message.data['moduleName']));
        break;
      default:
        // should use tabsNavigatorKey to found current homeView context then called the 'navigateFromRootToPage' function to switch the bottom bar index
        HomeView.of(appViewModel.tabsNavigatorKey[appViewModel.bottomBarCurrentIndex.value].currentContext!)!.navigateFromRootToPage('alertList', null, null, null, null);
        break;
    }
  }
  
  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    late List<ConnectivityResult> result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(List<ConnectivityResult> result) async {
    if (result.contains(ConnectivityResult.wifi) || result.contains(ConnectivityResult.mobile)) {
      appViewModel.isConnectionFailed.value = false;
    } else if (result.contains(ConnectivityResult.none)) {
      appViewModel.isConnectionFailed.value = true;
    } else {
      appViewModel.isConnectionFailed.value = true;
    }
  }
}
