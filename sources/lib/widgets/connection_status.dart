import 'package:flutter/material.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/view_models/app_view_model.dart';

class ConnectionStatusWidget extends StatefulWidget {
  final Color bgColor;
  final String msgText;

  const ConnectionStatusWidget({
    this.bgColor = black,
    required this.msgText,
    Key? key
  }) : super(key: key);

  @override
  State<ConnectionStatusWidget> createState() => _ConnectionStatusWidgetState();
}

class _ConnectionStatusWidgetState extends State<ConnectionStatusWidget> {
  AppViewModel appViewModel = locator<AppViewModel>();

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: appViewModel.isConnectionFailed,
      builder: (context, bool show, Widget? child) {
        return Container(
          color: widget.bgColor,
          width: double.infinity,
          child: AnimatedSize(
            curve: Curves.easeOut,
            duration: const Duration(milliseconds: 500),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: (show ? 2 : 0)),
              child: Text(
                widget.msgText,
                textAlign: TextAlign.center,
                style: regularFontsStyle(color: white, fontSize: show ? 12.0 : 0.0, height: 1.35),
              ),
            ),
          ),
        );
      },
    );
  }
}