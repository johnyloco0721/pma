import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:sources/args/media_slider_args.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/services/api.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/size_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/widgets/content_widget.dart';
import 'package:sources/widgets/video_player_widget.dart';
import 'package:sources/widgets/video_thumbnail_widget.dart';

class MediaSliderView extends StatefulWidget {
  final MediaSliderArguments mediaSliderArgs;
  const MediaSliderView({Key? key, required this.mediaSliderArgs}) : super(key: key);

  @override
  _MediaSliderViewState createState() => _MediaSliderViewState();
}

class _MediaSliderViewState extends State<MediaSliderView> {
  late PageController mediaSliderPageController;
  PhotoViewScaleStateController imageScaleContaoller = PhotoViewScaleStateController();
  late List<MediaInfoArguments> sliderList;
  bool mediaSliderShowPreview = true;
  bool isMediaSliderAnimating = false;
  int mediaSliderCurrentIndex = 0;

  @override
  void initState() {
    // implement initState
    super.initState();
    mediaSliderPageController = PageController(initialPage: widget.mediaSliderArgs.initialIndex);
    sliderList = widget.mediaSliderArgs.mediaInfoList;
    mediaSliderCurrentIndex = widget.mediaSliderArgs.initialIndex;
  }

  @override
  void dispose() {
    // implement dispose
    super.dispose();
    mediaSliderPageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: darkGrey,
      appBar: _buildAppBar(context),
      body: GestureDetector(
        onTap: () {
          setState(() {
            mediaSliderShowPreview = !mediaSliderShowPreview;
          });
        },
        child: _buildMediaSlider(context),
      ),
    );
  }
  
  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.light,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: SvgPicture.asset(ic_chevron_left, width: 10.0),
      ),
      actions: [
        GestureDetector(
          onTap: () {
            // download image
            this.downloadImage(sliderList[mediaSliderCurrentIndex].mediaPath);
          },
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: SvgPicture.asset(
              ic_download,
              width: 20.0,
              color: white,
            ),
          ),
        ),
        SizedBox(width: 10.0),
      ],
      centerTitle: false,
      backgroundColor: black,
    );
  }

  Widget _buildMediaSlider(BuildContext context) => Stack(
    children: [
      // Slider Layout
      PhotoViewGallery.builder(
        scrollPhysics: BouncingScrollPhysics(),
        pageController: mediaSliderPageController,
        itemCount: sliderList.length,
        loadingBuilder: (context, event) => Center(
          child: Container(
            width: 20,
            height: 20,
            child: CircularProgressIndicator(
              value: event == null ? 0 : event.cumulativeBytesLoaded / event.expectedTotalBytes!,
            ),
          ),
        ),
        backgroundDecoration: BoxDecoration(color: darkGrey),
        onPageChanged: (int index) {
          // Highlight image thumbnail after sliding animation completed
          if (!isMediaSliderAnimating) {
            setState(() {
              mediaSliderCurrentIndex = index;
            });
          }
          // reset the image Scale on page changed
          imageScaleContaoller.reset();
        },
        builder: (BuildContext context, int index) {
          if (sliderList[index].mediaType == 'mp4') {
            return PhotoViewGalleryPageOptions.customChild(
              child: VideoPlayerWidget(
                url: sliderList[index].mediaPath,
                autoplay: false,
                looping: false,
              ),
              initialScale: PhotoViewComputedScale.contained * 1.0,
              minScale: PhotoViewComputedScale.contained * 1.0,
              heroAttributes: PhotoViewHeroAttributes(tag: sliderList[index].mediaPath),
            );
          } 
          else {
            return PhotoViewGalleryPageOptions(
              scaleStateController: imageScaleContaoller,
              imageProvider: (sliderList[index].mediaPath[0] == '/' ? FileImage(File(sliderList[index].mediaPath)) : CachedNetworkImageProvider(sliderList[index].mediaPath)) as ImageProvider,
              initialScale: PhotoViewComputedScale.contained * 1.0,
              minScale: PhotoViewComputedScale.contained * 1.0,
              heroAttributes: PhotoViewHeroAttributes(tag: sliderList[index].mediaPath),
            );
          }
        },
      ),
      // Overlay media description and thumbnails
      Positioned(
        bottom: 0,
        child: Visibility(
          visible: mediaSliderShowPreview,
          child: Container(
            color: Color(0x99000000),
            child: Column(
              children: [
                // Feed description
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(top: 16, bottom: 16, left: 24, right: 24),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(bottom: 8),
                        child: ContentWidget(
                          content: sliderList[mediaSliderCurrentIndex].mediaDescription.toString(),
                          contentColor: white,
                          fontSize: 17,
                          linkColor: linkYellow,
                        ),
                      ),
                      Text(
                        formatDatetime(datetime: sliderList[mediaSliderCurrentIndex].datetime ?? '', format: 'dd MMM yyyy') + ' ' + formatDatetime(datetime: sliderList[mediaSliderCurrentIndex].datetime ?? '', format: 'hh:mma').toLowerCase(),
                        style: regularFontsStyle(fontSize: 15.0, color: white, height: 1.27),
                      ),
                    ]
                  ),
                ),
                // Media thumbnail list
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(bottom: 15),
                  height: 40,
                  child: Center(
                    child: GridView(
                      scrollDirection: Axis.horizontal,
                      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                        maxCrossAxisExtent: 40,
                        childAspectRatio: 1,
                        mainAxisSpacing: 8,
                      ),
                      shrinkWrap: true,
                      children: [
                        for(var i = 0; i < sliderList.length; i++) 
                          GestureDetector(
                            onTap: () {
                              // Set highlighted thumbnail
                              mediaSliderCurrentIndex = i;
                              // Animate to selected image
                              isMediaSliderAnimating = true;
                              mediaSliderPageController.animateToPage(i, duration: Duration(milliseconds: 200), curve: Curves.linear).then((value) {
                                setState(() {
                                  isMediaSliderAnimating = false;
                                });
                              });
                            },
                            child: ClipRect(
                              child: Stack(
                                children: [
                                  // Image thumbnail
                                  (sliderList[i].mediaType == 'MOV' || sliderList[i].mediaType == 'mp4') ? 
                                  sliderList[i].thumbnailPath != null && sliderList[i].thumbnailPath != '' ? Stack(
                                    alignment: AlignmentDirectional.center,
                                    children: [
                                      Container(
                                        child: CachedNetworkImage(
                                          imageUrl: sliderList[i].thumbnailPath ?? '',
                                          height: 40,
                                          width: 40,
                                          maxHeightDiskCache: 120,
                                          maxWidthDiskCache: 120,
                                        ),
                                      ),
                                      Container(
                                        constraints: BoxConstraints(maxHeight: 36.0),
                                        child: SvgPicture.asset(
                                          ic_play_button,
                                          height: 13.0,
                                          width: 13.0,
                                        ),
                                      ),
                                    ]
                                  ) : 
                                  VideoThumbnailWidget(
                                    videoPath: sliderList[i].mediaPath,
                                    height: 40,
                                    width: 40,
                                  ) : sliderList[i].mediaPath[0] == '/' ? Image.file(
                                    File(sliderList[i].mediaPath),
                                    height: 60,
                                    width: 60,
                                    fit: BoxFit.cover,
                                    cacheWidth: SizeUtils.width(context, 60).toInt(),
                                    cacheHeight: SizeUtils.height(context, 30).toInt(),
                                  ) : CachedNetworkImage(
                                    width: 40,
                                    height: 40,
                                    placeholder: (context, url) => Container(color: Colors.grey[50]),
                                    imageUrl: sliderList[i].mediaPath,
                                    fit: BoxFit.cover,
                                    alignment: Alignment.center,
                                    maxHeightDiskCache: 80,
                                    maxWidthDiskCache: 80,
                                  ),
                                  // Thumbnail overlay
                                  Visibility(
                                    visible: mediaSliderCurrentIndex == i ? false : true,
                                    child: Container(
                                      width: 40,
                                      height: 40,
                                      color: Color(0x99000000),
                                    ),  
                                  )
                                ],
                              ),
                            ),
                          ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        )
      ),
    ],
  );

  downloadImage(String url) async {
    try {
      showToastMessage(context, message: 'Downloading', duration: 1);
      Api().downloadFile(url, 'img', context);
    } 
    on Exception catch (_) {
      showError(context, 'Unexpected error, please try again.');
    }
  }
}