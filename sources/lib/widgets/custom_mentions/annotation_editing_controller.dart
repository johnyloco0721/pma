import 'package:flutter/material.dart';
import 'package:sources/widgets/custom_mentions/mention_model.dart';

/// A custom implementation of [TextEditingController] to support @ mention or other
/// trigger based mentions.
class MentionAnnotationEditingController extends TextEditingController {
  Map<String, Annotation> _mapping;
  String? _pattern;

  // Generate the Regex pattern for matching all the suggestions in one.
  MentionAnnotationEditingController(this._mapping)
      : _pattern = _mapping.keys.isNotEmpty
            ? "(${_mapping.keys.map((key) => key.startsWith('@') ? RegExp.escape(key) : key).join('|')})"
            : null;

  /// Can be used to get the markup from the controller directly.
  String get markupText {
    
    final someVal = _mapping.isEmpty
        ? text
        : text.splitMapJoin(
            RegExp('$_pattern'),
            onMatch: (Match match) {
              String patternMatched = '';
              final mention = _mapping[match[0]!] ??
                  _mapping[_mapping.keys.firstWhere((element) {
                    final reg = RegExp(element);
                    patternMatched = element;

                    return reg.hasMatch(match[0]!);
                  })]!;

              // Default markup format for mentions
              if (!mention.disableMarkup) {
                if (patternMatched == r"(?<=^|\s)\*(.*?)\*(?=\s|$)") {
                  return "<b>" + match[0]!.substring(1, match[0]!.length - 1) + "</b>";
                } else if (patternMatched == r'(?<=^|\s)\^(.*?)\^(?=\s|$)') {
                  return "<i>" + match[0]!.substring(1, match[0]!.length - 1) + "</i>";
                } else if (patternMatched == r'(?<=^|\s)(_(.*?)\_)(?=\s|$)') {
                  return "<u>" + match[0]!.substring(1, match[0]!.length - 1) + "</u>";
                } else if (patternMatched == r"(?<=^|\s)(~(.*?)~)(?=\s|$)") {
                  return "<s>" + match[0]!.substring(1, match[0]!.length - 1) + "</s>";
                } else {
                  return mention.markupBuilder != null
                      ? mention.markupBuilder!(
                          mention.trigger, mention.id!, mention.display!)
                      : '${mention.trigger}[__${mention.id}__](__${mention.display}__)';
                }
              } else {
                return match[0]!;
              }
            },
            onNonMatch: (String text) {
              return text;
            },
          );

    return someVal;
  }

  Map<String, Annotation> get mapping {
    return _mapping;
  }

  set mapping(Map<String, Annotation> _mapping) {
    this._mapping = _mapping;

    _pattern =
        "(${_mapping.keys.map((key) => key.startsWith('@') ? RegExp.escape(key) : key).join('|')})";
  }

  @override
  TextSpan buildTextSpan(
      {BuildContext? context, TextStyle? style, bool? withComposing}) {
    var children = <InlineSpan>[];
    String patternMatched = '';
    String formatText = '';

    if (_pattern == null || _pattern == '()') {
      children.add(TextSpan(text: text, style: style));
    } else {
      text.splitMapJoin(
        RegExp('$_pattern'),
        onMatch: (Match match) {
          if (_mapping.isNotEmpty) {
            final mention = _mapping[match[0]!] ??
                _mapping[_mapping.keys.firstWhere((element) {
                  final reg = RegExp(element);
                  patternMatched = element;

                  return reg.hasMatch(match[0]!);
                })]!;

            if (patternMatched == r"(?<=^|\s)\*(.*?)\*(?=\s|$)") {
              formatText = match[0]!.replaceAll("*", " ");
            } else if (patternMatched == r'(?<=^|\s)\^(.*?)\^(?=\s|$)') {
              formatText = match[0]!.replaceAll("^", " ");
            } else if (patternMatched == r'(?<=^|\s)(_(.*?)\_)(?=\s|$)') {
              formatText = match[0]!.replaceAll("_", " ");
            } else if (patternMatched == r'(?<=^|\s)(~(.*?)~)(?=\s|$)') {
              formatText = match[0]!.replaceAll("~", " ");
            } else {
              formatText = match[0]!;
            }

            children.add(
              TextSpan(
                text: formatText,
                style: style!.merge(mention.style),
              ),
            );
          }

          return '';
        },
        onNonMatch: (String text) {
          children.add(TextSpan(text: text, style: style));
          return '';
        },
      );
    }

    return TextSpan(style: style, children: children);
  }
}
