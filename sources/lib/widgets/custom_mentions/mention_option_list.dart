import 'package:flutter/material.dart';
import 'package:sources/constants/app_colors.dart';

class MentionOptionList extends StatelessWidget {
  MentionOptionList({
    required this.data,
    required this.onTap,
    required this.suggestionListWidth,
    required this.suggestionListHeight,
    this.suggestionBuilder,
    this.suggestionListDecoration,
    this.suggestionListBorderRadius = BorderRadius.zero,
    this.backgroundColor = white,
    this.suggestionListMargin = EdgeInsets.zero,
  });

  final Widget Function(Map<String, dynamic>)? suggestionBuilder;

  final List<Map<String, dynamic>> data;

  final Function(Map<String, dynamic>) onTap;

  final Color backgroundColor;

  final double suggestionListWidth;

  final double suggestionListHeight;

  final BorderRadius suggestionListBorderRadius;

  final BoxDecoration? suggestionListDecoration;

  final EdgeInsets suggestionListMargin;

  @override
  Widget build(BuildContext context) {
    return data.isNotEmpty
        ? Container(
            decoration: suggestionListDecoration ?? BoxDecoration(color: white),
            constraints: BoxConstraints(
              maxWidth: suggestionListWidth != 0 ? suggestionListWidth : double.infinity,
              maxHeight: suggestionListHeight,
              minHeight: 0,
            ),
            margin: suggestionListMargin,
            child: ClipRRect(
              borderRadius: suggestionListBorderRadius,
              child: ListView.separated(
                itemCount: data.length,
                shrinkWrap: true,
                separatorBuilder: (context, index) {
                  return Divider(thickness: 0.5, height: 0, color: lightGrey);
                },
                itemBuilder: (context, index) {
                  return ElevatedButton(
                    onPressed: (){
                      onTap(data[index]);
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: whiteGrey,
                      elevation: 0,
                      foregroundColor: yellow,
                      padding: EdgeInsets.symmetric(horizontal: 0),
                    ),
                    child: suggestionBuilder != null
                        ? suggestionBuilder!(data[index])
                        : Container(
                            color: blue,
                            padding: EdgeInsets.all(20.0),
                            child: Text(
                              data[index]['display'],
                              style: TextStyle(fontSize: 12),
                            ),
                          ),
                  );
                },
              ),
            ),
          )
        : Container();
  }
}
