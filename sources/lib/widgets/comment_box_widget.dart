import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../constants/app_colors.dart';
import '../constants/app_images.dart';
import '../constants/app_routes.dart';
import '../models/task_details_model.dart';
import '../utils/text_utils.dart';

// ignore: must_be_immutable
class CommentOutlineButton extends StatefulWidget {
  final TaskDetailsModel? taskInfo;
  final SubtaskDetailsModel? subtaskInfo;
  final int taskID;
  final bool isSubtask;
  Color textColor;
  Color borderColor;
  final VoidCallback? callback;

  CommentOutlineButton({
    this.taskInfo,
    this.subtaskInfo,
    this.taskID = 0,
    this.isSubtask = false,
    this.textColor = grey,
    this.borderColor = lightGrey,
    this.callback,
    Key? key,
  }) : super(key: key);

  @override
  _CommentOutlineButtonState createState() => _CommentOutlineButtonState();
}

class _CommentOutlineButtonState extends State<CommentOutlineButton> {
  late ValueSetter<bool> callback;
  dynamic response = false;
  int taskID = 0;
  int commentCount = 0;
  String isRead = '';

  @override
  void initState() {
    super.initState();
    taskID = widget.isSubtask ? widget.subtaskInfo?.taskID ?? 0 : widget.taskInfo?.taskID ?? 0;
    commentCount = widget.isSubtask ? widget.subtaskInfo?.replyCount ?? 0 : widget.taskInfo?.replyCount ?? 0;
    isRead = widget.isSubtask ? widget.subtaskInfo?.isRead ?? '' : widget.taskInfo?.isRead ?? '';
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    taskID = widget.isSubtask ? widget.subtaskInfo?.taskID ?? 0 : widget.taskInfo?.taskID ?? 0;
    commentCount = widget.isSubtask ? widget.subtaskInfo?.replyCount ?? 0 : widget.taskInfo?.replyCount ?? 0;
    isRead = widget.isSubtask ? widget.subtaskInfo?.isRead ?? '' : widget.taskInfo?.isRead ?? '';
    return GestureDetector(
      onTap: () async {
        await Navigator.pushNamed(context, taskCommentScreen, arguments: widget.taskID);
        if (widget.callback != null) widget.callback!();
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        decoration: BoxDecoration(
                      border: Border.all(color: lightGrey), 
                      borderRadius: BorderRadius.circular(500.0)
                    ),
        child: commentCount == 0 ? noneCommentButtonContent() : commentButtonContent(),
      ),
    );
  }

  Widget commentButtonContent() => Row(
  mainAxisSize: MainAxisSize.min,
  children: [
    Text(
      '$commentCount ', 
      style: boldFontsStyle(fontSize: 14.0, color: widget.textColor)
    ),
    Text(
      commentCount > 1 ? 'Replies' : 'Reply', 
      style: boldFontsStyle(fontSize: 14.0, color: widget.textColor)
    ),
    SizedBox(width: 4.0),
    if (isRead == "unread")
    Container(
      width: 10.0,
      height: 10.0,
      margin: EdgeInsets.symmetric(horizontal: 4.0),
      decoration: BoxDecoration(
        color: red,
        borderRadius: BorderRadius.circular(500.0),
      ),
    ),
    SizedBox(width: 4.0),
    SvgPicture.asset(ic_arrow_right_workplace, color: widget.textColor)
  ],
);

Widget noneCommentButtonContent() => Row(
  mainAxisSize: MainAxisSize.min,
  children: [
    SvgPicture.asset(ic_comment, width: 18.0, color: widget.textColor),
    SizedBox(width: 8.0),
    Text(
      'Reply In Thread',
      style: boldFontsStyle(fontSize: 14.0, color: widget.textColor),
    ),
  ],
);
}
