import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/utils/text_utils.dart';

class CustomTextInput extends StatefulWidget {
  const CustomTextInput(
      { required this.inputType,
        required this.hintTextString,
        required this.textEditController,
        required this.themeColor,
        this.textColor,
        this.obscureText = false,
        this.obscuringCharacter = '*',
        required this.suffixIcon,
        this.maxLength,
        required this.isNull,
        required this.isMatch});

  // ignore: prefer_typing_uninitialized_variables
  final String inputType;
  final hintTextString;
  final TextEditingController textEditController;
  final Color themeColor;
  final Color? textColor;
  final bool obscureText;
  final String obscuringCharacter;
  final String suffixIcon;
  final int? maxLength;
  final bool isNull;
  final bool isMatch;

  @override
  _CustomTextInputState createState() => _CustomTextInputState();
}

class _CustomTextInputState extends State<CustomTextInput> {
  @override
  Widget build(BuildContext context){
    return TextFormField(
      controller: widget.textEditController,
      obscureText: widget.obscureText,
      obscuringCharacter: widget.obscuringCharacter,
      cursorColor: white,
      style: boldFontsStyle(color: widget.isMatch ? widget.themeColor : lightRed, height: 1.32),
      decoration: InputDecoration(
        hintText: widget.hintTextString,
        hintStyle: italicFontsStyle(color: shimmerGrey, height: 1.32),
        contentPadding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: widget.isMatch ? widget.themeColor : lightRed, width: 1.5)
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: widget.isMatch ? widget.themeColor : lightRed, width: 3)
        ),
        suffixIcon: SvgPicture.asset(
          widget.isNull && widget.isMatch ? 'assets/icons/Verified_Icon.svg' : widget.suffixIcon,
          color: widget.isMatch ? widget.themeColor : lightRed,
          fit: BoxFit.none
        )
      ),
    );
  }
}
