import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';

class Avatar extends StatelessWidget {
  final String? url;
  final double? width;
  final Color? borderColor;
  final double? borderWidth;
  final bool? dots;

  const Avatar({
    this.url,
    this.width,
    this.borderColor,
    this.borderWidth,
    this.dots = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return (this.dots!) ? 
    Stack(
      children: [
        _buildAvatar(context),
        _buildDots(context),
      ],
    ) :
    _buildAvatar(context);
  }

  Widget _buildAvatar(BuildContext context) {
    return Container(
      width: this.width != null ? this.width : 26,
      height: this.width != null ? this.width : 26,
      decoration: BoxDecoration(
        color: Colors.grey[50],
        border: Border.all(
          color: this.borderColor != null ? this.borderColor! : Colors.grey,
          width: this.borderWidth != null ? this.borderWidth! : 1,
        ),
        borderRadius: BorderRadius.all(Radius.circular(this.width != null ? this.width! : 26)),
      ),
      child: ClipOval(
        child: CachedNetworkImage(
          placeholder: (context, url) => Container(color: Colors.grey[50],),
          imageUrl: url!,
          fit: BoxFit.cover,
          alignment: Alignment.center,
          maxHeightDiskCache: 120,
          maxWidthDiskCache: 120,
          width: this.width != null ? this.width : 26,
          errorWidget: (context, string, dynamic) {
            return Image.asset(
              img_default_avatar, 
              width: this.width != null ? this.width : 26
            );
          }
        ),
      ),
    );
  }

  Widget _buildDots(BuildContext context) {
    return Container(
      width: 20,
      height: 20,
      decoration: BoxDecoration(
        color: darkGrey,
        border: Border.all(color: Color(0x00ffffff)),
        borderRadius: BorderRadius.circular(12),
      ),
      child: Center(
        child: SvgPicture.asset(
          ic_dots,
          width: 12.0,
          color: white,
        ),
      ),
    );
  }
}