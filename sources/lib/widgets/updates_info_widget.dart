import 'dart:io';

import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_svg/svg.dart';
import 'package:open_filex/open_filex.dart';
import 'package:sources/args/media_slider_args.dart';
import 'package:sources/args/task_args.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/database/feeds.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/widgets/content_widget.dart';
import 'package:sources/widgets/video_thumbnail_widget.dart';

class UpdatesInfo extends StatefulWidget {
  final FeedWithDetail feedWithDetail;
  UpdatesInfo({Key? key, required this.feedWithDetail}) : super(key: key);

  @override
  _UpdatesInfoState createState() => _UpdatesInfoState();
}

class _UpdatesInfoState extends State<UpdatesInfo> {
    final database = locator<AppDatabase>();
  String completedDateTime = '';
  int commentCount = 0;
  int hasNewComments = 0;
  List<MediaInfoArguments> mediaArgsList = [];
  List<TaskMedia> imageList = [];
  List<TaskMedia> docList = [];

  @override
  void initState() {
    // implement initState
    super.initState();
    setAttachmentValue();
  }

  setAttachmentValue() async {
    completedDateTime = '';
    commentCount = 0;
    hasNewComments = 0;

    imageList = widget.feedWithDetail.taskMediaList!.where((e) => e.mediaType != 'pdf').toList();
    docList = widget.feedWithDetail.taskMediaList!.where((e) => e.mediaType == 'pdf').toList();

    mediaArgsList = imageList.map((m) => MediaInfoArguments(
      mediaPath: m.mediaPath ?? '',
      mediaDescription: widget.feedWithDetail.feedDetail.updatesDescription,
      mediaType: m.mediaType ?? '',
      datetime: completedDateTime
    )).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
      color: (widget.feedWithDetail.feedDetail.moduleName == "task-flag")
             ? pink : transparent,
      border: Border.all(
                width: 1.0, 
                color: (widget.feedWithDetail.feedDetail.moduleName == "task-flag")
                       ? red : transparent),
      borderRadius: BorderRadius.all(
        Radius.circular(12.0),
      ),
    ),

      child: ClipRRect(
        borderRadius: BorderRadius.circular(12.0),
        child: Column(
          children: [
            // image grid view
            imageList.length > 0 ? imageGridView(imageList) : Container(),
            infoLayout(),
          ],
        ),
      )
    );
  }

  // image custom grid view
  Widget imageGridView(List<TaskMedia> imageList, {int flex = 6}) => Container(
    height: 165.0,
    child: Row(
      children: [
        imageBoxLayout(imageList, 0),
        if (imageList.length > 1)
          SizedBox(width: 4.0),
        if (imageList.length > 1)
          Expanded(
            child: Column(
              children: [
                imageBoxLayout(imageList, 1),
                if (imageList.length > 2)
                  SizedBox(height: 4.0),
                if (imageList.length > 2)
                  imageBoxLayout(imageList, 2),
              ],
            )
          ),
      ],
    ),
  );

  Widget imageBoxLayout(List<TaskMedia> imageUrl, int index) => Expanded(
    child: Stack(
      children: [
        imageUrl[index].mediaType == 'mp4' ? VideoThumbnailWidget(
          videoPath: imageUrl[index].mediaPath ?? '',
          height: 200.0,
          width: 400.0,
        ) : CachedNetworkImage(
          imageUrl: imageUrl[index].mediaPath ?? '',
          height: double.infinity,
          width: double.infinity,
          fit: BoxFit.cover,
          maxHeightDiskCache: 120,
          maxWidthDiskCache: 120,
        ),
        if (index > 1 && imageUrl.length > 3)
          Opacity(
            opacity: 0.5,
            child: Container(
              color: black,
            ),
          ),        
        if (index > 1 && imageUrl.length > 3)
          Center(
            child: Text(
              '+${imageUrl.length - 2}',
              style: boldFontsStyle(fontSize: 30.0, height: 1.33, color: white),
            ),
          ),
        Material(
          color: transparent,
          child: InkWell(
            // splashColor: grey,
            onTap: () {
              // go to media viewer
              goToNextNamedRoute(context, mediaSliderScreen, args: MediaSliderArguments(mediaArgsList, index));
            },
          ),
        ),
      ],
    ),
  );

  Widget infoLayout({int flex = 5}) {
    Feed feed = widget.feedWithDetail.feedDetail;

    return Container(
      decoration: BoxDecoration(
        color: darkGrey,
        border: Border(
          top: BorderSide(color: (widget.feedWithDetail.feedDetail.moduleName == "task-flag") ? red : transparent),
        ),
      ),
      padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 24.0),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // title
          if (feed.updatesDescription != null && feed.updatesDescription != '')
            Padding(
              padding: const EdgeInsets.only(bottom: 12.0),
              child: feed.updatesDescription == 'This task is marked as completed'
              ? Text(
                feed.updatesDescription!,
                style: italicFontsStyle(fontSize: 16.0, height: 1.5, color: white),
              ) : ContentWidget(
                content: feed.updatesDescription!,
                contentColor: white,
                fontSize: 16.0,
                linkColor: linkYellow,
              ),
            ),
          // doc listing layout
          ExpandableNotifier(
            child: Expandable(
              collapsed: Wrap(
                children: [
                  for(int i=0; i<docList.length && i<3; i++)
                    docBoxLayout(docList[i], i),
                  if (docList.length > 3)
                    ExpandableButton(
                      child: Container(
                        margin: EdgeInsets.only(bottom: 12.0),
                        child: Text(
                          '${docList.length - 3} More...',
                          style: boldFontsStyle(fontSize: 14.0, color: yellow),
                        ),
                      ),
                    ),
                ],
              ),
              expanded: Wrap(
                children: [
                  for(int i=0; i<docList.length; i++)
                    docBoxLayout(docList[i], i),
                  ExpandableButton(
                    child: Container(
                      margin: EdgeInsets.only(bottom: 12.0),
                      child: Text(
                        'Collapse..',
                        style: boldFontsStyle(fontSize: 14.0, color: yellow),
                      ),
                    ),
                  ),
                ],
              )
            ),
          ),
          // go to task details button
          ElevatedButton(
            onPressed: () {
              goToNextNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: int.parse(feed.moduleID!)));
            }, 
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                SvgPicture.asset(ic_task_incomplete, width: 24.0),
                SizedBox(width: 8.0),
                Text(
                  'View Task Details',
                  style: boldFontsStyle(fontSize: 16.0, height: 1.5),
                ),
              ],
            ),
            style: ElevatedButton.styleFrom(
              backgroundColor: yellow,
              padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
            ),
          ),
        ],
      ),
    );
  }

  Widget docBoxLayout(TaskMedia file, int index) => Column(
    children: [
      Material(
        color: transparent,
        child: InkWell(
          borderRadius: BorderRadius.circular(500.0),
          onTap: () async {
            // go to pdf viewer
            String? temporaryPath;
            String? savedPath = SpManager.getString(file.mediaPath.toString());
            if (savedPath != null && await File(savedPath).exists()){
              temporaryPath = savedPath;
            } else {
              showToastMessage(context, message: 'Downloading', duration: 1);
              await Api().downloadFile(file.mediaPath ?? '', 'pdf', context).then((localSavedPath) {
                temporaryPath = localSavedPath;
              });
              await SpManager.putString(file.mediaPath.toString(), temporaryPath ?? '');
              await Future.delayed(Duration(milliseconds: 1500));
            }
            await OpenFilex.open(temporaryPath);
          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
            decoration: BoxDecoration(
              color: transparent,
              border: Border.all(width: 1.0, color: white),
              borderRadius: BorderRadius.circular(500.0)
            ),
            child: Row(
              children: [
                SvgPicture.asset(ic_doc, width: 18.0, color: yellow),
                SizedBox(width: 8.0),
                Expanded(
                  child: Text(
                    file.mediaName ?? file.mediaPath!.substring(file.mediaPath!.lastIndexOf('/') + 1),
                    style: regularFontsStyle(fontSize: 14.0, color: white),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      SizedBox(height: index == docList.length - 1 ? 12.0 : 6.0),
    ],
  );
}