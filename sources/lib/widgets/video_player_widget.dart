import 'dart:io';

import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerWidget extends StatefulWidget {
  final String? url;
  final bool? autoplay;
  final bool? looping;

  const VideoPlayerWidget({
    Key? key,
    this.url,
    this.autoplay,
    this.looping,
  }) : super(key: key);

  @override
  _VideoPlayerWidgetState createState() => _VideoPlayerWidgetState();
}

class _VideoPlayerWidgetState extends State<VideoPlayerWidget> {
  VideoPlayerController? _videoPlayerController;
  ChewieController? cwController;

  @override
  void initState() {
    // implement initState
    super.initState();
    initializePlayer();
  }

  @override
  void dispose() {
    // implement dispose
    _videoPlayerController?.dispose();
    cwController?.dispose();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return cwController != null &&
            cwController!.videoPlayerController.value.isInitialized
        ? Center(
            child: AspectRatio(
              aspectRatio:
                  cwController!.videoPlayerController.value.aspectRatio,
              child: Chewie(
                controller: cwController!,
              ),
            ),
          )
        : Center(child: CircularProgressIndicator());
  }

  Future<void> initializePlayer() async {
    File fetchedFile = widget.url![0] == '/' ? File(widget.url ?? '')
            : await DefaultCacheManager().getSingleFile(widget.url ?? "");

    _videoPlayerController = VideoPlayerController.file(
      fetchedFile,
      videoPlayerOptions: VideoPlayerOptions(),
    );
    await Future.wait([_videoPlayerController!.initialize()]);
    cwController = ChewieController(
      videoPlayerController: _videoPlayerController!,
      autoPlay: widget.autoplay ?? false,
      looping: widget.looping ?? false,
      fullScreenByDefault: true,
      deviceOrientationsAfterFullScreen: [DeviceOrientation.portraitUp],
      deviceOrientationsOnEnterFullScreen: [
        DeviceOrientation.portraitUp,
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
        DeviceOrientation.portraitDown
      ],
    );
    setState(() {});
  }
}
