import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:video_player/video_player.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import 'package:sources/constants/app_images.dart';

class VideoThumbnailWidget extends StatefulWidget {
  final String videoPath;
  final double? height;
  final double? width;

  const VideoThumbnailWidget({
    this.videoPath = '',
    this.height,
    this.width,
    Key? key,
  }) : super(key: key);

  @override
  VideoThumbnailWidgetState createState() => VideoThumbnailWidgetState();
}

class VideoThumbnailWidgetState extends State<VideoThumbnailWidget> {
  ValueNotifier<Uint8List?> videoThumbnailData = ValueNotifier(null);
  VideoPlayerController? _videoPlayerController;
  bool isFail = false;
  String temporaryPath = "";

  @override
  void initState() {
    super.initState();
    String? savedVideoThumbnailStr = SpManager.getString(widget.videoPath);
    temporaryPath = widget.videoPath;
    if (savedVideoThumbnailStr != null && savedVideoThumbnailStr.codeUnits.length > 100) {
      Uint8List savedVideoThumbnail = Uint8List.fromList(savedVideoThumbnailStr.codeUnits);
      videoThumbnailData.value = savedVideoThumbnail;
    } else {
      initialVideo();
    }
  }

  initialVideo() async {
    if (widget.videoPath[0] == "/") {
      getThumbnail();
    } else {
      File fetchedFile = await DefaultCacheManager().getSingleFile(widget.videoPath);
      temporaryPath = fetchedFile.path;
      _videoPlayerController = VideoPlayerController.file(fetchedFile);
      await Future.wait([_videoPlayerController!.initialize()]).then((value) {
        getThumbnail();
      });
    }
  }

  getThumbnail({int timeMs = 0}) async {
    await VideoThumbnail.thumbnailData(
      video: temporaryPath,
      imageFormat: ImageFormat.PNG,
      maxHeight: widget.height?.toInt() ?? MediaQuery.of(context).size.height.toInt(),
      maxWidth: widget.width?.toInt() ?? MediaQuery.of(context).size.width.toInt(),
      timeMs: timeMs,
      quality: 100
    ).then((value) async {
      // for IOS
      if (value == null && timeMs <= _videoPlayerController!.value.duration.inMilliseconds) {
        await getThumbnail(timeMs: timeMs += 100);
      } else {
        videoThumbnailData.value = value;
      }
    }).onError((error, stackTrace) async {
      print("video data error: " + timeMs.toString());
      // for Android
      if (timeMs <= _videoPlayerController!.value.duration.inMilliseconds) {
        await getThumbnail(timeMs: timeMs += (_videoPlayerController!.value.duration.inMilliseconds / 5).floor());
      } else {
        setState(() {
          isFail = true;
        });
      }
    });
    SpManager.putString(widget.videoPath, String.fromCharCodes(videoThumbnailData.value ?? []));
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: videoThumbnailData,
      builder: (BuildContext context, Uint8List? videoThumbnail, Widget? child) {
        return !isFail ? videoThumbnail != null ? Stack(
          alignment: AlignmentDirectional.center,
          children: [
            Container(
              child: Image.memory(
                videoThumbnailData.value!,
                height: widget.height,
                width: widget.width,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              constraints: BoxConstraints(maxHeight: 36.0),
              child: SvgPicture.asset(
                ic_play_button,
                height: widget.height != null ? widget.height! / 3 : null,
                width: widget.width != null ? widget.width! / 3 : null,
              ),
            ),
          ]
        ) : Container(
          height: widget.height,
          width: widget.width,
          child: Center(
            child: CircularProgressIndicator(color: yellow)
          ),
        ) : Stack(
          alignment: AlignmentDirectional.center,
          children: [
            Container(
              child: Image.asset(
                img_thumbnail_placeholder,
                height: widget.height,
                width: widget.width,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              constraints: BoxConstraints(maxHeight: 36.0),
              child: SvgPicture.asset(
                ic_play_button,
                height: widget.height != null ? widget.height! / 3 : null,
                width: widget.width != null ? widget.width! / 3 : null,
              ),
            ),
          ]
        );
      }
    );
  }
}