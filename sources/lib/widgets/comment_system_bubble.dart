import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/models/feed_item_model.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';

import '../models/comment_details_model.dart';
import 'comment_updates_info_widget.dart';
import 'html_widget/html_stylist.dart';

class CommentSystemBubble extends StatelessWidget {
  final CommentDetailsResponse commentWithDetails;
  final FeedItemModel comments;
  final int index;

  CommentSystemBubble({
    required this.commentWithDetails,
    required this.comments,
    required this.index,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final comments = this.comments;
    String postTime = formatDatetime(datetime: comments.postDateTime ?? '', format: "dd MMM yyyy • hh:mma");

    return Bubble(
      stick: false,
      margin: BubbleEdges.only(top: 0.0, bottom: 0.0, right: 24, left: 24),
      padding: BubbleEdges.all(16),
      radius: Radius.circular(12),
      borderColor: hexToColor(comments.outlineColor.toString()),
      borderWidth: 0.5,
      color: hexToColor(comments.backgroundColor.toString()),
      nip: BubbleNip.no,
      nipHeight: 12,
      alignment: Alignment.centerLeft,
      elevation: 0,
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (comments.description != null && comments.description != '')
              Padding(
                padding: EdgeInsets.only(bottom: 16.0),
                child: RichText(
                  text: HTML.toTextSpan(
                    context,
                    comments.description!.replaceAll(' xss=removed', ''),
                    defaultTextStyle: italicFontsStyle(color: darkGrey, fontSize: 16.0),
                  ),
                  maxLines: 4,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            Container(
              margin: EdgeInsets.only(bottom: 16.0),
              child: CommentSystemUpdatesInfo(
                commentWithDetails: commentWithDetails,
                comments: comments,
              )
            ),
            // system label & time row
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      decoration: BoxDecoration(
                          color: white,
                          border: Border.all(
                            color: lightGrey,
                            width: 1.0,
                          ),
                          borderRadius: BorderRadius.circular(12.0)),
                      child: Padding(padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 8.0), child: Text('System', style: regularFontsStyle(fontSize: 12.0, color: grey)))),             
                  // Render feed post time
                  _buildTime(postTime, grey),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Feed post time
  Widget _buildTime(String time, Color color) {
    return Text(
      time,
      style: regularFontsStyle(color: color, fontSize: 12.0, height: 1.33),
    );
  }
}
