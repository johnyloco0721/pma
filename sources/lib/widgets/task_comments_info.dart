import 'dart:io';

import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_svg/svg.dart';
import 'package:open_filex/open_filex.dart';

import 'package:sources/args/media_slider_args.dart';
import 'package:sources/constants/app_box_shadow.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/models/attach_model.dart';
import 'package:sources/models/task_details_model.dart';
import 'package:sources/services/api.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/widgets/content_widget.dart';
import 'package:sources/widgets/html_widget/html_stylist.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:autoscale_tabbarview/autoscale_tabbarview.dart' as CustomTabView;

import '../args/push_update_info_args.dart';
import '../models/comment_details_model.dart';

class TaskCommentsInfo extends StatefulWidget {
  final TaskDetailsModel? taskInfo;
  final SubtaskDetailsModel? subtaskInfo;
  final bool? autoUpdateCA;
  final String? isPushedToClientApp;
  final bool isSubtask;
  final isCollapsed;
  final VoidCallback? callback;

  TaskCommentsInfo({
    this.taskInfo,
    this.subtaskInfo,
    this.autoUpdateCA,
    this.isPushedToClientApp,
    this.isSubtask = false,
    this.isCollapsed,
    this.callback,
    Key? key,
  }) : super(key: key);

  @override
  _TaskCommentsInfoState createState() => _TaskCommentsInfoState();
}

class _TaskCommentsInfoState extends State<TaskCommentsInfo> with TickerProviderStateMixin{
  String taskName = '';
  String taskDescription = '';
  String clientTaskDescription = '';
  String completedDateTime = '';
  String createdDateTime = '';
  String updatedDateTime = '';
  String internalExtraDate = '';
  String clientExtraDate = '';
  int projectID = 0;
  int taskID = 0;
  int commentCount = 0;
  int hasNewComments = 0;
  List<MediaInfoArguments> mediaArgsList = [];
  List<AttachModel> imageList = [];
  List<AttachModel> docList = [];
  List<MediaInfoArguments> internalMediaArgsList = [];
  List<AttachModel> internalImageList = [];
  List<AttachModel> internalDocList = [];
  List<MediaInfoArguments> clientMediaArgsList = [];
  List<AttachModel> clientImageList = [];
  List<AttachModel> clientDocList = [];

  String? updatesPushDateTime;
  int autoPushRemainingTime = 0;
  bool isCancel = false;
  bool isShownInCA = false;
  int imageUpdateCA = 1;

  bool isReadmore = false;
  bool isCollapsed = true;

  CommentDetailsModel? commentDetailsResponse;
  CommentDetailsResponse taskInfoResponse =
  CommentDetailsResponse(taskDetails: null, commentList: []);
  String? isPushedToClientApp;
  String? clientAppPhotoAutoUpdate;
  String? taskUpdates;
  List<AttachModel>? updatesImages;
  List<AttachModel>? updatesFiles;

  late ValueSetter<bool> callback;
  final GlobalKey _internalContainerKey = GlobalKey();
  final GlobalKey _clientContainerKey = GlobalKey();
  double? internalContainerHeight;
  double? clientContainerHeight;
  late final TabController _tabController = TabController(length: 2, vsync: this, initialIndex: 0);

  @override
  void initState() {
    // implement initState
    super.initState();
    _tabController.addListener(() {
      // Control Tab
      setState(() {});
    });
    _tabController.addListener(_handleTabChange);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _measureContainerHeight();
    });
    initiateDataValue();
  }

  void _handleTabChange() {
      _measureContainerHeight();
  }

  void _measureContainerHeight() {
    final internalHeight = _internalContainerKey.currentContext?.size?.height;
    final clientHeight = _clientContainerKey.currentContext?.size?.height;
    setState(() {
      if (_tabController.index == 0) {
        internalContainerHeight = internalHeight ?? internalContainerHeight;
      } else if (_tabController.index == 1) {
        clientContainerHeight = clientHeight ?? clientContainerHeight;
      }
    });
  }

  initiateDataValue({bool initState = true}) {
    taskID = widget.isSubtask
        ? widget.subtaskInfo?.taskID ?? 0
        : widget.taskInfo?.taskID ?? 0;
    taskName = widget.isSubtask
        ? widget.subtaskInfo?.taskName ?? ''
        : widget.taskInfo?.taskName ?? '';
    taskDescription = (widget.isSubtask
      ? (widget.subtaskInfo?.forInternal?.taskUpdates!.isNotEmpty == true
          ? widget.subtaskInfo?.forInternal?.taskUpdates
          : "-")
      : (widget.taskInfo?.forInternal?.taskUpdates!.isNotEmpty == true 
          ? widget.taskInfo?.forInternal?.taskUpdates
          : "-")
    )!;
    clientTaskDescription = (widget.isSubtask
      ? (widget.subtaskInfo?.forClient?.taskUpdates!.isNotEmpty == true
          ? widget.subtaskInfo?.forClient?.taskUpdates
          : "-")
      : (widget.taskInfo?.forClient?.taskUpdates!.isNotEmpty == true 
          ? widget.taskInfo?.forClient?.taskUpdates
          : "-")
    )!;
    completedDateTime = widget.isSubtask
        ? widget.subtaskInfo?.updatedDateTime ?? ''
        : widget.taskInfo?.completedDateTime ?? '';
    createdDateTime = widget.isSubtask
        ? widget.subtaskInfo?.createdDateTime ?? ''
        : widget.taskInfo?.createdDateTime ?? '';
    updatedDateTime = widget.isSubtask
        ? widget.subtaskInfo?.updatedDateTime ?? ''
        : widget.taskInfo?.updatedDateTime ?? '';
    commentCount = widget.isSubtask
        ? widget.subtaskInfo?.replyCount ?? 0
        : widget.taskInfo?.replyCount ?? 0;
        
    internalDocList = widget.isSubtask 
      ? widget.subtaskInfo?.forInternal?.updatesFiles?.where((e) => e.selectedStatus == 1).toList() ?? []
      : widget.taskInfo?.forInternal?.updatesFiles?.where((e) => e.selectedStatus == 1).toList() ?? [];

    internalImageList = widget.isSubtask 
      ? widget.subtaskInfo?.forInternal?.updatesImages?.where((e) => e.selectedStatus == 1).toList() ?? []
      : widget.taskInfo?.forInternal?.updatesImages?.where((e) => e.selectedStatus == 1).toList() ?? [];

    internalMediaArgsList = internalImageList
      .map((m) => MediaInfoArguments(
          mediaPath: m.imagePath ?? '',
          thumbnailPath: m.thumbnailPath ?? '',
          mediaDescription: taskDescription,
          mediaType: m.imageType ?? '',
          datetime: completedDateTime))
      .toList();

    clientDocList = widget.isSubtask 
      ? widget.subtaskInfo?.forClient?.updatesFiles?.where((e) => e.selectedStatus == 1).toList() ?? []
      : widget.taskInfo?.forClient?.updatesFiles?.where((e) => e.selectedStatus == 1).toList() ?? [];

    clientImageList = widget.isSubtask 
      ? widget.subtaskInfo?.forClient?.updatesImages?.where((e) => e.selectedStatus == 1).toList() ?? []
      : widget.taskInfo?.forClient?.updatesImages?.where((e) => e.selectedStatus == 1).toList() ?? [];

    clientMediaArgsList = clientImageList
        .map((m) => MediaInfoArguments(
            mediaPath: m.imagePath ?? '',
            thumbnailPath: m.thumbnailPath ?? '',
            mediaDescription: clientTaskDescription,
            mediaType: m.imageType ?? '',
            datetime: completedDateTime))
        .toList();

    updatesPushDateTime = widget.isSubtask
        ? widget.subtaskInfo?.updatesPushDateTime
        : widget.taskInfo?.updatesPushDateTime;

    if (updatesPushDateTime != null)
      autoPushRemainingTime = DateTime.parse(updatesPushDateTime!)
          .difference(DateTime.now())
          .inSeconds;

    isCancel = widget.isSubtask
        ? widget.subtaskInfo?.isPushCancelled ?? false
        : widget.taskInfo?.isPushCancelled ?? false;
    isShownInCA = widget.isSubtask
        ? widget.autoUpdateCA ?? false
        : widget.autoUpdateCA ?? false;
    imageUpdateCA = widget.isSubtask
        ? widget.subtaskInfo?.isFileRequired ?? 1
        : widget.taskInfo?.isFileRequired ?? 1;

    isPushedToClientApp = widget.isSubtask
        ? widget.subtaskInfo?.isPushedToClientApp ?? ""
        : widget.taskInfo?.isPushedToClientApp ?? "";

    if (initState) isCollapsed = widget.isCollapsed;
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    initiateDataValue(initState: false);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        widget.isSubtask
            ? Container(
                width: double.infinity,
                padding: EdgeInsets.all(24.0),
                margin: EdgeInsets.only(bottom: 24.0),
                decoration: BoxDecoration(
                    color: white,
                    borderRadius: BorderRadius.circular(12.0),
                    boxShadow: app_box_shadow),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 16.0),
                    Text(
                      widget.subtaskInfo?.taskName ?? '',
                      style: boldFontsStyle(
                          fontSize: 20.0, height: 1.4, color: white),
                      maxLines: 1,
                      overflow: TextOverflow.visible,
                    ),
                    if (widget.subtaskInfo?.taskDescription != '' &&
                        widget.subtaskInfo?.taskDescription != null)
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0),
                        child: Text(
                          widget.subtaskInfo?.taskDescription ?? '',
                          style: regularFontsStyle(
                              fontSize: 20.0, height: 1.5, color: white),
                          maxLines: 1,
                          overflow: TextOverflow.visible,
                        ),
                      ),
                    SizedBox(height: 20.0),
                    // imageWithInfoLayout(),
                  ],
                ),
              )
            : imageWithInfoLayout(internalImageList),
      ],
    );
  }

  Widget imageWithInfoLayout(List<AttachModel> imageList) => Container(
      width: double.infinity,
      margin: EdgeInsets.only(bottom: widget.isSubtask ? 0.0 : 12.0),
      child: ClipRRect(
        child: Column(
          children: [
            Row(children: [
              if (isCollapsed && imageList.length > 0)
                Padding(
                  padding: EdgeInsets.only(right: 4.0),
                  child: Row(
                    children: [
                      imageBoxLayout2(imageList, 0),
                      if (imageList.length > 1) SizedBox(width: 4.0),
                      if (imageList.length > 1) imageBoxLayout2(imageList, 1),
                    ],
                  ),
                ),
              Expanded(flex: 7, child: infoLayout(tabController: _tabController)),
            ])
          ],
        ),
      ));

  // image custom grid view
  Widget imageGridView(List<AttachModel> imageList, List<MediaInfoArguments> mediaInfoList, {int flex = 6}) =>
      Container(
        height: 100.0,
        child: Row(
          children: [
            imageBoxLayout(imageList, 0, mediaInfoList),
            if (imageList.length > 1) SizedBox(width: 8.0),
            if (imageList.length > 1) imageBoxLayout(imageList, 1, mediaInfoList),
            if (imageList.length > 2) SizedBox(width: 8.0),
            if (imageList.length > 2) imageBoxLayout(imageList, 2, mediaInfoList),
          ],
        ),
      );

  // Expanded
  Widget imageBoxLayout(List<AttachModel> imageUrl, int index, List<MediaInfoArguments> mediaInfoList) => Container(
      width: 70,
      height: 70,
      decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(6),
      border: Border.all(color: Colors.grey, width: 1),
    ),
        child: Stack(
          children: [
            imageUrl[index].imageType == 'mp4'
                ? Stack(alignment: AlignmentDirectional.center, children: [
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: lightGrey),
                          borderRadius: BorderRadius.only(
                            topLeft: index == 0
                                ? Radius.circular(12.0)
                                : Radius.zero,
                            topRight: index == 1
                                ? Radius.circular(12.0)
                                : Radius.zero,
                          )),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(12.0),
                        child: CachedNetworkImage(
                          imageUrl: imageUrl[index].thumbnailPath ?? '',
                          height: double.infinity,
                          width: double.infinity,
                          fit: BoxFit.cover,
                          maxHeightDiskCache: 120,
                          maxWidthDiskCache: 120,
                        ),
                      ),
                    ),
                    Container(
                      constraints: BoxConstraints(maxHeight: 36.0),
                      child: SvgPicture.asset(
                        ic_play_button,
                      ),
                    ),
                  ])
                : CachedNetworkImage(
                    imageUrl: imageUrl[index].imagePath ?? '',
                    height: double.infinity,
                    width: double.infinity,
                    fit: BoxFit.cover,
                    maxHeightDiskCache: 120,
                    maxWidthDiskCache: 120,
                  ),
            if (index > 1 && imageUrl.length > 3)
              Opacity(
                opacity: 0.5,
                child: Container(
                  color: black,
                ),
              ),
            if (index > 1 && imageUrl.length > 3)
              Center(
                child: Text(
                  '+${imageUrl.length - 2}',
                  style: boldFontsStyle(
                      fontFamily: platForm,
                      fontSize: 30.0,
                      height: 1.33,
                      color: white,
                      letterSpacing: 0.8),
                ),
              ),
            Material(
              color: transparent,
              child: InkWell(
                // splashColor: grey,
                onTap: () {
                  // go to media viewer
                   goToNextNamedRoute(context, mediaSliderScreen,
                      args: MediaSliderArguments(mediaInfoList, index));
                },
              ),
            ),
          ],
        ),
      );

  // Collapse
  Widget imageBoxLayout2(List<AttachModel> imageUrl, int index) => Container(
        height: 56.0,
        width: 56.0,
        child: Stack(
          children: [
            imageUrl[index].imageType == 'mp4'
                ? Stack(alignment: AlignmentDirectional.center, children: [
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: lightGrey),
                          borderRadius: BorderRadius.only(
                            topLeft: index == 0
                                ? Radius.circular(12.0)
                                : Radius.zero,
                            bottomLeft: index == 0
                                ? Radius.circular(12.0)
                                : Radius.zero,
                          )),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(12.0),
                        child: CachedNetworkImage(
                          imageUrl: imageUrl[index].thumbnailPath ?? '',
                          height: double.infinity,
                          width: double.infinity,
                          fit: BoxFit.cover,
                          maxHeightDiskCache: 100,
                          maxWidthDiskCache: 100,
                        ),
                      ),
                    ),
                    Container(
                      constraints: BoxConstraints(maxHeight: 36.0),
                      child: SvgPicture.asset(
                        ic_play_button,
                      ),
                    ),
                  ])
                : ClipRRect(
                    borderRadius: (index == 0)
                        ? BorderRadius.only(
                            topLeft: Radius.circular(6.0),
                            bottomLeft: Radius.circular(6.0))
                        : BorderRadius.circular(0.0),
                    child: CachedNetworkImage(
                      imageUrl: imageUrl[index].imagePath ?? '',
                      height: double.infinity,
                      width: double.infinity,
                      fit: BoxFit.cover,
                      maxHeightDiskCache: 100,
                      maxWidthDiskCache: 100,
                    ),
                  ),
            if (index > 0 && imageUrl.length > 2)
              Opacity(
                opacity: 0.5,
                child: Container(
                  color: black,
                ),
              ),
            if (index > 0 && imageUrl.length > 2)
              Center(
                child: Text(
                  '+${imageUrl.length - 1}',
                  style: boldFontsStyle(
                      fontFamily: platForm,
                      fontSize: 30.0,
                      height: 1.33,
                      color: white,
                      letterSpacing: 0.8),
                ),
              ),
            Material(
              color: transparent,
              child: InkWell(
                // splashColor: grey,
                onTap: () {
                  // go to media viewer
                  goToNextNamedRoute(context, mediaSliderScreen,
                      args: MediaSliderArguments(internalMediaArgsList, index));
                },
              ),
            ),
          ],
        ),
      );

  Widget infoLayout({required TabController tabController, int flex = 5}) {
    return Container(
      padding: isCollapsed
          ? EdgeInsets.symmetric(vertical: 8.0, horizontal: 12.0)
          : EdgeInsets.only(left: 24.0, right: 24.0, bottom: 24.0, top: 6.0),
      decoration: BoxDecoration(
        color: darkGrey,
        borderRadius: isCollapsed && imageList.length > 0
            ? BorderRadius.horizontal(right: Radius.circular(6.0))
            : BorderRadius.circular(6.0),
      ),
      constraints: BoxConstraints(minHeight: 56.0),
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // title
          Row(
            crossAxisAlignment: isCollapsed
                ? CrossAxisAlignment.center
                : CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: isCollapsed
                    ? imageList.length == 1
                        ? 9
                        : 8
                    : 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    isCollapsed
                    ? ContentWidget(
                      content: this.taskDescription,
                      contentColor: white,
                      fontSize: isCollapsed ? 12.0 : 16.0,
                      maxLines: 2,
                      enabledReadMore: isCollapsed ? false : true,
                      linkColor: linkYellow,
                    )
                    : Container(
                      child: TabBar.secondary(
                        controller: _tabController,
                        indicatorColor: darkYellow,
                        labelStyle: boldFontsStyle(color: white, fontSize: 14.0, height: 1.35),
                        unselectedLabelStyle: boldFontsStyle(color: white, fontSize: 14.0, height: 1.35),
                        dividerColor: transparent,
                        tabs: [
                          Tab(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset(ic_team),
                                SizedBox(width: 8),
                                Text('For Internal'),
                              ],
                            ),
                          ),
                          Tab(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset(ic_moggy_head),
                                SizedBox(width: 8),
                                Text('For Client'),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    if (!isCollapsed) SizedBox(height: 8.0),
                    if (!isCollapsed)
                    CustomTabView.AutoScaleTabBarView(
                      controller: _tabController,
                      children: [
                        // For Internal
                        Container(
                          child: Container(
                            key: _internalContainerKey,
                            height: internalContainerHeight,
                            child: RawScrollbar(
                              thumbVisibility: true,
                              thumbColor: darkYellow,
                              trackVisibility: true,
                              trackColor: darkGrey,
                              minOverscrollLength: 50,
                              radius: Radius.circular(25),
                              thickness: 4,
                              minThumbLength: 80,
                              child: SingleChildScrollView(
                                padding: EdgeInsets.only(right: 7),
                                child: Container(
                                  child: Column(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(top: 8.0),
                                        child: Center(
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                child: RichText(
                                                  text: HTML.toTextSpan(
                                                    context,
                                                    (!widget.isSubtask &&
                                                            (widget.taskInfo?.taskStatus != "flagged" ||
                                                                widget.taskInfo?.taskStatus == "flagged"))
                                                        ? '${widget.taskInfo?.forInternal?.taskCompletedBy}'
                                                        : '${widget.subtaskInfo?.forInternal?.taskCompletedBy}',
                                                    defaultTextStyle: boldFontsStyle(fontSize: 14.0, color: white)
                                                        .copyWith(height: 24 / 14.0),
                                                  ),
                                                  maxLines: 2,
                                                  overflow: TextOverflow.ellipsis,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(bottom: 8.0),
                                        child: Center(
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                child: RichText(
                                                  text: HTML.toTextSpan(
                                                    context,
                                                    (!widget.isSubtask &&
                                                            (widget.taskInfo?.taskStatus != "flagged" ||
                                                                widget.taskInfo?.taskStatus == "flagged"))
                                                    ? '${widget.taskInfo?.forInternal?.taskUpdates!.isNotEmpty == true 
                                                          ? widget.taskInfo?.forInternal?.taskUpdates : "-"}'
                                                    : '${widget.subtaskInfo?.forInternal?.taskUpdates!.isNotEmpty == true 
                                                          ? widget.subtaskInfo?.forInternal?.taskUpdates : "-"}',
                                                    defaultTextStyle: boldFontsStyle(fontSize: 14.0, color: white)
                                                        .copyWith(height: 24 / 14.0),
                                                  ),
                                                  maxLines: 2,
                                                  overflow: TextOverflow.ellipsis,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      // Extra Dates
                                      if (widget.taskInfo?.forInternal?.extraDatesDisplayArr != null)
                                      Column(
                                        children: [
                                          Container(
                                            width: double.infinity,
                                            margin: EdgeInsets.only(
                                              top: 8.0, 
                                              bottom: widget.taskInfo?.specialTaskType != null && 
                                                (internalImageList.isEmpty && internalDocList.isEmpty) ? 16.0 : 8.0
                                            ),
                                            padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                                            decoration: BoxDecoration(
                                              border: Border.all(color: white),
                                              borderRadius: BorderRadius.all(Radius.circular(6.0)),
                                            ),
                                            child: RichText(
                                              text: TextSpan(
                                                text: '${widget.taskInfo?.forInternal?.extraDatesDisplayArr?.label}: ',
                                                style: regularFontsStyle(fontSize: 16.0, height: 1.5, color: white),
                                                children: [
                                                  TextSpan(
                                                    text: '${widget.taskInfo?.forInternal?.extraDatesDisplayArr?.date}',
                                                    style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: white),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      // Display Images
                                      if(internalImageList.length > 0 && !isCollapsed)
                                      imageGridView(internalImageList, internalMediaArgsList),
                                      // Internal Document Lists
                                      if(internalDocList.length > 0)
                                      Container(
                                        margin: EdgeInsets.only(top: 8.0,bottom: 8.0),
                                        child: ExpandableNotifier(
                                          child: Expandable(
                                            collapsed: Wrap(
                                              children: [
                                                for (int i = 0; i < internalDocList.length && i < 3; i++)
                                                  docBoxLayout(internalDocList[i], i),
                                                if (internalDocList.length > 3)
                                                  ExpandableButton(
                                                    child: Container(
                                                      margin: EdgeInsets.only(bottom: 12.0),
                                                      child: Text(
                                                        '${internalDocList.length - 3} More...',
                                                        style: boldFontsStyle(
                                                            fontSize: 14.0, color: Colors.yellow),
                                                      ),
                                                    ),
                                                  ),
                                              ],
                                            ),
                                            expanded: Wrap(
                                              children: [
                                                for (int i = 0; i < internalDocList.length; i++)
                                                  docBoxLayout(internalDocList[i], i),
                                                ExpandableButton(
                                                  child: Container(
                                                    margin: EdgeInsets.only(bottom: 12.0),
                                                    child: Text(
                                                      'Collapse..',
                                                      style: boldFontsStyle(
                                                          fontSize: 14.0, color: Colors.yellow),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      // Dropbox Link
                                      if (widget.taskInfo?.specialTaskType == "edited-photo" && widget.taskInfo?.forInternal?.dropboxLink != null)
                                      Container(
                                        alignment: Alignment.topLeft,
                                        margin: EdgeInsets.only(top: 0.0,bottom: 8.0),
                                        padding: EdgeInsets.symmetric(
                                            vertical: 8.0, horizontal: 16.0),
                                        decoration: BoxDecoration(
                                            border: Border.all(color: white),
                                            borderRadius: BorderRadius.circular(6.0)),
                                        child: GestureDetector(
                                          onTap: () {
                                            launch(widget.taskInfo?.forInternal?.dropboxLink ?? '');
                                          },
                                          child: Text(
                                            widget.taskInfo?.forInternal?.dropboxLink ?? '',
                                            style: boldFontsStyle(
                                                fontSize: 16.0, height: 1.5, color: yellow),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        // For Client
                        Container(
                          child: Container(
                            key: _clientContainerKey,
                            height: clientContainerHeight,
                            child: RawScrollbar(
                              thumbVisibility: true,
                              thumbColor: darkYellow,
                              trackVisibility: true,
                              trackColor: darkGrey,
                              minOverscrollLength: 50,
                              radius: Radius.circular(25),
                              thickness: 4,
                              minThumbLength: 80,
                              child: SingleChildScrollView(
                                padding: EdgeInsets.only(right: 7),
                                child: Container(
                                  child: Column(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
                                        child: Center(
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Align(
                                                alignment: Alignment.topCenter,
                                                child: Padding(
                                                  padding: EdgeInsets.only(top: 6.0),
                                                  child: SvgPicture.asset(ic_moggy_head, width: 26),
                                                ),
                                              ),
                                              SizedBox(width: 8),
                                              Expanded(
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    if (!widget.isSubtask &&
                                                      widget.taskInfo?.taskStatus != "flagged" &&
                                                      widget.taskInfo?.forClient?.taskCompletedBy?.isNotEmpty == true)
                                                    RichText(
                                                      text: HTML.toTextSpan(
                                                        context,
                                                        '${widget.taskInfo?.forClient?.taskCompletedBy}',
                                                        defaultTextStyle: boldFontsStyle(
                                                          fontSize: 14.0,
                                                          color: white,
                                                        ).copyWith(height: 24 / 14.0),
                                                      ),
                                                      maxLines: 2,
                                                      overflow: TextOverflow.ellipsis,
                                                    ),
                                                    RichText(
                                                      text: HTML.toTextSpan(
                                                        context,
                                                        (!widget.isSubtask &&
                                                          (widget.taskInfo?.taskStatus != "flagged" ||
                                                            widget.taskInfo?.taskStatus == "flagged"))
                                                          ? '${widget.taskInfo?.forClient?.taskUpdates!.isNotEmpty == true 
                                                          ? widget.taskInfo?.forClient?.taskUpdates : "-"}'
                                                          : '${widget.subtaskInfo?.forClient?.taskUpdates!.isNotEmpty == true 
                                                          ? widget.subtaskInfo?.forClient?.taskUpdates : "-"}',
                                                        defaultTextStyle: boldFontsStyle(
                                                          fontSize: 14.0,
                                                          color: white,
                                                        ).copyWith(height: 24 / 14.0),
                                                      ),
                                                      maxLines: 2,
                                                      overflow: TextOverflow.ellipsis,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      // Extra Dates
                                    if (widget.taskInfo?.forClient?.extraDatesDisplayArr != null && widget.taskInfo?.forClient?.extraDatesDisplayArr?.date != '')
                                      Column(
                                        children: [
                                          Container(
                                            width: double.infinity,
                                            margin: EdgeInsets.only(
                                              top: 8.0, 
                                              bottom: widget.taskInfo?.specialTaskType != null && 
                                                (internalImageList.isEmpty && internalDocList.isEmpty) ? 16.0 : 8.0
                                            ),                                      
                                            padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                                            decoration: BoxDecoration(
                                              border: Border.all(color: white),
                                              borderRadius: BorderRadius.all(Radius.circular(6.0)),
                                            ),
                                            child: RichText(
                                              text: TextSpan(
                                                text: '${widget.taskInfo?.forClient?.extraDatesDisplayArr?.label}: ',
                                                style: regularFontsStyle(fontSize: 16.0, height: 1.5, color: white),
                                                children: [
                                                  TextSpan(
                                                    text: '${widget.taskInfo?.forClient?.extraDatesDisplayArr?.date}',
                                                    style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: white),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      // Display Images
                                      if(clientImageList.length > 0 && !isCollapsed)
                                      imageGridView(clientImageList, clientMediaArgsList),
                                      // Client Document Lists
                                      if(clientDocList.length > 0)
                                      Container(
                                        margin: EdgeInsets.only(top: 8.0,bottom: 8.0),
                                        child: ExpandableNotifier(
                                          child: Expandable(
                                            collapsed: Wrap(
                                              children: [
                                                for (int i = 0; i < clientDocList.length && i < 3; i++)
                                                  docBoxLayout(clientDocList[i], i),
                                                if (clientDocList.length > 3)
                                                  ExpandableButton(
                                                    child: Container(
                                                      margin: EdgeInsets.only(bottom: 12.0),
                                                      child: Text(
                                                        '${clientDocList.length - 3} More...',
                                                        style: boldFontsStyle(fontSize: 14.0, color: Colors.yellow),
                                                      ),
                                                    ),
                                                  ),
                                              ],
                                            ),
                                            expanded: Wrap(
                                              children: [
                                                for (int i = 0; i < clientDocList.length; i++)
                                                  docBoxLayout(clientDocList[i], i),
                                                ExpandableButton(
                                                  child: Container(
                                                    margin: EdgeInsets.only(bottom: 12.0),
                                                    child: Text(
                                                      'Collapse..',
                                                      style: boldFontsStyle(fontSize: 14.0, color: Colors.yellow),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      // Dropbox Link
                                      if (widget.taskInfo?.specialTaskType == "edited-photo")
                                      Container(
                                        alignment: Alignment.topLeft,
                                        margin: EdgeInsets.only(top: 0.0,bottom: 8.0),
                                        padding: EdgeInsets.symmetric(
                                            vertical: 8.0, horizontal: 16.0),
                                        decoration: BoxDecoration(
                                            border: Border.all(color: white),
                                            borderRadius: BorderRadius.circular(6.0)),
                                        child: GestureDetector(
                                          onTap: () {
                                            launch(widget.taskInfo?.forClient?.dropboxLink ?? '');
                                          },
                                          child: Text(
                                            widget.taskInfo?.forClient?.dropboxLink ?? '',
                                            style: boldFontsStyle(
                                                fontSize: 16.0, height: 1.5, color: yellow),
                                          ),
                                        ),
                                      ),                 
                                    ],
                                  )
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: updatesPushDateTime != null ? 16.0 : (widget.taskInfo?.forInternal?.extraDatesDisplayArr != null) ? 0.0 : 8.0),
                    if (!isCollapsed)
                    // date time layout
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Expanded(
                          flex: isPushedToClientApp == 'pushed' ? 8 : 12,
                          child: Column(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Text(
                                        formatDatetime(
                                          datetime: updatedDateTime != '' && updatedDateTime != null
                                              ? updatedDateTime
                                              : createdDateTime,
                                        ),
                                        style: regularFontsStyle(
                                          fontSize: 12.0,
                                          height: 1.5,
                                          color: white,
                                        ),
                                      ),
                                      SizedBox(width: 8.0,),
                                      Container(
                                        width: 4.0,
                                        height: 4.0,
                                        margin: EdgeInsets.all(6.0),
                                        decoration: BoxDecoration(
                                          color: white,
                                          borderRadius: BorderRadius.circular(500.0),
                                        ),
                                      ),
                                      SizedBox(width: 8.0,),
                                      if((widget.isSubtask == false &&
                                          widget.taskInfo?.taskStatus == "flagged") ||
                                          (widget.isSubtask == true &&
                                              widget.subtaskInfo?.taskStatus == "flagged") ||
                                          isPushedToClientApp == 'pushed')
                                      Text(
                                        formatDatetime(
                                          datetime: updatedDateTime != '' && updatedDateTime != null
                                              ? updatedDateTime
                                              : createdDateTime,
                                          format: 'hh:mma',
                                        ).toLowerCase(),
                                        style: regularFontsStyle(
                                          fontSize: 12.0,
                                          height: 1.5,
                                          color: white,
                                        ),
                                      ),
                                    ],
                                  ),
                                  ((widget.isSubtask == false &&
                                        widget.taskInfo?.taskStatus == "flagged") ||
                                    (widget.isSubtask == true &&
                                        widget.subtaskInfo?.taskStatus == "flagged") ||
                                    isPushedToClientApp == 'pushed')
                                  ? Container()
                                  : Text(
                                        formatDatetime(
                                          datetime: updatedDateTime != '' && updatedDateTime != null
                                              ? updatedDateTime
                                              : createdDateTime,
                                          format: 'hh:mma',
                                        ).toLowerCase(),
                                        style: regularFontsStyle(
                                          fontSize: 12.0,
                                          height: 1.5,
                                          color: white,
                                        ),
                                      ),
                                ],
                              ),
                              if (isPushedToClientApp == 'pushed' && _tabController.index == 1)
                              // isPushedToClientApp == 'pushed', Add the text if the task had push
                              Column(
                                children: [
                                  Row(
                                    children: [
                                      Text('Updates is published to client', style: italicFontsStyle(color: white, fontSize: 14.0, fontWeight: FontWeight.w700, height:1.54 ),),
                                    ],
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                        (widget.isSubtask == false &&
                                    widget.taskInfo?.taskStatus == "flagged") ||
                                (widget.isSubtask == true &&
                                    widget.subtaskInfo?.taskStatus == "flagged") ||
                                isPushedToClientApp == 'pushed'
                            ? Container()
                            : Expanded(
                                flex: 8,
                                child: GestureDetector(
                                  onTap: () async {
                                    bool response = await onManualPush(
                                        context,
                                        PushUpdateInfoArguments(
                                            taskID: widget.isSubtask
                                                ? widget.subtaskInfo?.taskID ?? 0
                                                : widget.taskInfo?.taskID ?? 0,
                                            projectID: widget.taskInfo?.projectID,
                                            action: "flagged",
                                            // remark: widget.isSubtask
                                            //     ? widget.subtaskInfo?.taskUpdates ?? ''
                                            //     : widget.taskInfo?.taskUpdates ?? '',
                                            imageUpdateCA: widget
                                                    .taskInfo?.clientAppPhotoAutoUpdate ==
                                                'yes',
                                            updatesImages: widget.isSubtask
                                                ? widget.subtaskInfo?.forInternal?.updatesImages ?? []
                                                : widget.taskInfo?.forInternal?.updatesImages ?? [],
                                            updatesFiles: widget.isSubtask
                                                ? widget.subtaskInfo?.forInternal?.updatesFiles ?? []
                                                : widget.taskInfo?.forInternal?.updatesFiles ?? [],
                                            updateScreen: "Flagged",
                                            // extraDatesDisplayArr: widget.subtaskInfo?.extraDatesDisplayArr,
                                            isFileRequired: widget.subtaskInfo?.isFileRequired == 1));
                                    if (response) widget.callback!();
                                  },
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Container(
                                          padding: EdgeInsets.symmetric(
                                              vertical: 9.0, horizontal: 21.0),
                                          decoration: BoxDecoration(
                                              border:
                                                  Border.all(width: 1.0, color: white),
                                              borderRadius: BorderRadius.circular(500.0)),
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              SvgPicture.asset(ic_task_transparent_flag),
                                              SizedBox(width: 9.38),
                                              Text(
                                                "Flag",
                                                style: boldFontsStyle(
                                                    fontSize: 14.0, color: white),
                                              ),
                                            ],
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                        if ((widget.isSubtask == false && widget.taskInfo?.taskStatus != "flagged") ||
                            (widget.isSubtask == true && widget.subtaskInfo?.taskStatus != "flagged"))
                          Flexible(
                            flex: 0,
                            child: Container(
                              margin: EdgeInsets.only(left: 8.0),
                              child: GestureDetector(
                                onTap: () async {
                                  bool response = await onManualPush(
                                    context,
                                    PushUpdateInfoArguments(
                                      taskID: widget.isSubtask
                                          ? widget.subtaskInfo?.taskID ?? 0
                                          : widget.taskInfo?.taskID ?? 0,
                                      action: "update",
                                      // remark: widget.isSubtask
                                      //     ? widget.subtaskInfo?.taskUpdates
                                      //     : widget.taskInfo?.taskUpdates,
                                      imageUpdateCA: widget.taskInfo?.clientAppPhotoAutoUpdate ==
                                          'yes',
                                      updateScreen: "Edit Update",
                                      isFileRequired: widget.isSubtask
                                          ? widget.subtaskInfo?.isFileRequired == 1
                                          : widget.taskInfo?.isFileRequired == 1,
                                      specialTaskType: widget.isSubtask
                                          ? widget.subtaskInfo?.specialTaskType
                                          : widget.taskInfo?.specialTaskType,
                                      linkValue: widget.taskInfo?.forInternal?.dropboxLink,
                                      taskRelatedDate: widget.subtaskInfo?.taskRelatedDate ??
                                          widget.taskInfo?.taskRelatedDate,
                                      // extraDatesDisplayArr: widget.subtaskInfo?.extraDatesDisplayArr,
                                      forClient: widget.isSubtask
                                          ? widget.subtaskInfo?.forClient
                                          : widget.taskInfo?.forClient,
                                      forInternal: widget.isSubtask
                                          ? widget.subtaskInfo?.forInternal
                                          : widget.taskInfo?.forInternal,
                                    ),
                                  );
                                  if(response) goToReplacementNamedRoute(context, taskCommentScreen, args: widget.taskInfo?.taskID);
                                  // if (response) widget.callback!();
                                },
                                child: Container(
                                  padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                                  decoration: BoxDecoration(
                                    color: yellow,
                                    borderRadius: BorderRadius.circular(50),
                                  ),
                                  child: SvgPicture.asset(ic_edit, width: 20.0),
                                ),
                              ),
                            ),
                          ),
                      ],
                    ),
                  ],
                ),
              ),
              isCollapsed
                  ? widget.taskInfo?.isFlag == 0 &&
                          widget.isPushedToClientApp != 'pushed'
                      ? GestureDetector(
                          onTap: () async {
                            bool response = await onManualPush(
                                context,
                                PushUpdateInfoArguments(
                                    taskID: widget.isSubtask
                                        ? widget.subtaskInfo?.taskID ?? 0
                                        : widget.taskInfo?.taskID ?? 0,
                                    projectID: widget.taskInfo?.projectID,
                                    action: "flagged",
                                    // remark: widget.isSubtask
                                    //     ? widget.subtaskInfo?.taskUpdates ?? ''
                                    //     : widget.taskInfo?.taskUpdates ?? '',
                                    imageUpdateCA: widget.taskInfo
                                            ?.clientAppPhotoAutoUpdate ==
                                        'yes',
                                    updatesImages: widget.isSubtask
                                        ? widget.subtaskInfo?.forInternal?.updatesImages ?? []
                                        : widget.taskInfo?.forInternal?.updatesImages ?? [],
                                    updatesFiles: widget.isSubtask
                                        ? widget.subtaskInfo?.forInternal?.updatesFiles ?? []
                                        : widget.taskInfo?.forInternal?.updatesFiles ?? [],
                                    updateScreen: "Flagged",
                                    isFileRequired:
                                        widget.taskInfo?.isFileRequired == 1,
                                    // extraDatesDisplayArr: widget.taskInfo?.extraDatesDisplayArr,
                                        ));
                            if (response) widget.callback!();
                          },
                          child: Container(
                            margin: EdgeInsets.only(left: 4.0),
                            padding: EdgeInsets.symmetric(
                                vertical: 9.38, horizontal: 11.0),
                            decoration: BoxDecoration(
                                border: Border.all(width: 1.0, color: white),
                                borderRadius: BorderRadius.circular(500.0)),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                SvgPicture.asset(ic_task_transparent_flag,
                                    width: 11.0),
                              ],
                            ),
                          ),
                        )
                      : Container()
                  : Container(),
              if (widget.taskInfo?.taskStatus != 'flagged' && isCollapsed)
                Container(
                  margin: EdgeInsets.only(left: 8.0),
                  child: GestureDetector(
                    onTap: () async {
                      bool response = await onManualPush(
                          context,
                          PushUpdateInfoArguments(
                              taskID: taskID,
                              action: "update",
                              remark: taskDescription,
                              imageUpdateCA: clientAppPhotoAutoUpdate == 1,
                              updatesImages: updatesImages,
                              updatesFiles: updatesFiles,
                              updateScreen: "Edit Update",
                              isFileRequired: widget.taskInfo?.isFileRequired == 1,
                              specialTaskType: widget.taskInfo?.specialTaskType,
                              linkValue: widget.taskInfo?.forInternal?.dropboxLink,
                              taskRelatedDate: widget.taskInfo?.taskRelatedDate,
                              // extraDatesDisplayArr: widget.taskInfo?.extraDatesDisplayArr,
                              forClient: widget.isSubtask
                                          ? widget.subtaskInfo?.forClient
                                          : widget.taskInfo?.forClient,
                                      forInternal: widget.isSubtask
                                          ? widget.subtaskInfo?.forInternal
                                          : widget.taskInfo?.forInternal,
                            ));
                      if (response) widget.callback!();
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                      decoration: BoxDecoration(
                        color: yellow,
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: SvgPicture.asset(ic_edit,
                          width: isCollapsed ? 18.0 : 20.0),
                    ),
                  ),
                ),
            ],
          ),
          isCollapsed
              ? Container()
              : ExpandableNotifier(
                  child: Expandable(
                      collapsed: Wrap(
                        children: [
                          for (int i = 0; i < docList.length && i < 1; i++)
                            docBoxLayout(docList[i], i),
                          if (docList.length > 1)
                            ExpandableButton(
                              child: Container(
                                margin: EdgeInsets.only(bottom: 6.0),
                                child: Text(
                                  '${docList.length - 1} More...',
                                  style: boldFontsStyle(
                                      fontSize: 14.0, color: yellow),
                                ),
                              ),
                            ),
                        ],
                      ),
                      expanded: Wrap(
                        children: [
                          for (int i = 0; i < docList.length; i++)
                            docBoxLayout(docList[i], i),
                          ExpandableButton(
                            child: Container(
                              margin: EdgeInsets.only(bottom: 12.0),
                              child: Text(
                                'Collapse..',
                                style: boldFontsStyle(
                                    fontSize: 14.0, color: yellow),
                              ),
                            ),
                          ),
                        ],
                      )),
                ),
        ],
      ),
    );
  }

  Widget docBoxLayout(AttachModel file, int index) => Column(
        children: [
          Material(
            color: transparent,
            child: InkWell(
              borderRadius: BorderRadius.circular(500.0),
              onTap: () async {
                // go to pdf viewer
                String? temporaryPath;
                String? savedPath =
                    SpManager.getString(file.imagePath.toString());
                if (savedPath != null && await File(savedPath).exists()) {
                  temporaryPath = savedPath;
                } else {
                  showToastMessage(context,
                      message: 'Downloading', duration: 1);
                  await Api()
                      .downloadFile(file.imagePath ?? '', 'pdf', context)
                      .then((localSavedPath) {
                    temporaryPath = localSavedPath;
                  });
                  await SpManager.putString(
                      file.imagePath.toString(), temporaryPath ?? '');
                  await Future.delayed(Duration(milliseconds: 1500));
                }
                await OpenFilex.open(temporaryPath);
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                decoration: BoxDecoration(
                    color: transparent,
                    border: Border.all(width: 1.0, color: white),
                    borderRadius: BorderRadius.circular(500.0)),
                child: Row(
                  children: [
                    SvgPicture.asset(ic_doc, width: 18.0, color: yellow),
                    SizedBox(width: 8.0),
                    Expanded(
                      child: Text(
                        file.imageName ??
                            file.imagePath!.substring(
                                file.imagePath!.lastIndexOf('/') + 1),
                        style: regularFontsStyle(fontSize: 14.0, color: white),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 12.0),
        ],
      );
}
