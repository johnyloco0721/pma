import 'dart:io';

import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:open_filex/open_filex.dart';
import 'package:sources/args/task_args.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/models/push_update_model.dart';
import 'package:sources/services/task_services.dart';
import 'package:sources/widgets/html_widget/html_stylist.dart';
import 'package:timer_count_down/timer_count_down.dart';

import 'package:sources/args/media_slider_args.dart';
import 'package:sources/constants/app_box_shadow.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/models/attach_model.dart';
import 'package:sources/models/task_details_model.dart';
import 'package:sources/services/api.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/widgets/comment_box_widget.dart';
import 'package:sources/widgets/content_widget.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:autoscale_tabbarview/autoscale_tabbarview.dart' as CustomTabView;

import '../args/push_update_info_args.dart';

class CompletedInfo extends StatefulWidget {
  final TaskDetailsModel? taskInfo;
  final SubtaskDetailsModel? subtaskInfo;
  final bool? autoUpdateCA;
  final bool isSubtask;
  final VoidCallback? isloadingTrue;
  final ValueGetter onManualPush;
  final VoidCallback? callback;
  final Function(bool) setLoadingState;
  final VoidCallback stopLoading;

  CompletedInfo({
    this.taskInfo,
    this.subtaskInfo,
    this.autoUpdateCA,
    this.isSubtask = false,
    this.isloadingTrue,
    required this.onManualPush,
    this.callback,
    required this.setLoadingState,
    required this.stopLoading,
    Key? key,
  }) : super(key: key);

  @override
  _CompletedInfoState createState() => _CompletedInfoState();
}

class _CompletedInfoState extends State<CompletedInfo> with TickerProviderStateMixin{
  TaskService _taskService = TaskService();

  String taskName = '';
  String taskDescription = '';
  String clientTaskDescription = '';
  String completedDateTime = '';
  String createdDateTime = '';
  String updatedDateTime = '';
  int projectID = 0;
  int taskID = 0;
  int commentCount = 0;
  int replyCount = 0;
  List<MediaInfoArguments> internalMediaArgsList = [];
  List<AttachModel> internalImageList = [];
  List<AttachModel> internalDocList = [];
  List<MediaInfoArguments> clientMediaArgsList = [];
  List<AttachModel> clientImageList = [];
  List<AttachModel> clientDocList = [];
  List<int> uploadedAttachmentIDs = [];

  String? updatesPushDateTime;
  int autoPushRemainingTime = 0;
  bool isCancel = false;
  bool isShownInCA = false;
  bool isShownMediasInCA = false;
  int imageUpdateCA = 1;
  int onlyClickOnce = 0;

  bool isLoading = false;
  bool isTabInternal = false;  
  String? isPushedToClientApp;
  String? clientAppPhotoAutoUpdate;
  String? taskUpdates;
  List<AttachModel>? updatesImages;
  List<AttachModel>? updatesFiles;
  final GlobalKey _internalContainerKey = GlobalKey();
  final GlobalKey _clientContainerKey = GlobalKey();
  double? internalContainerHeight;
  double? clientContainerHeight;

  late ValueSetter<bool> callback;
  late final TabController _tabController = TabController(length: 2, vsync: this, initialIndex: 0);

  @override
  void initState() {
    // implement initState
    super.initState();
    _tabController.addListener(() {
      // Control Tab
      setState(() {});
    });
    _tabController.addListener(_handleTabChange);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _measureContainerHeight();
    });
    initiateDataValue();
  }

  void _handleTabChange() {
      _measureContainerHeight();
  }

  void _measureContainerHeight() {
    final internalHeight = _internalContainerKey.currentContext?.size?.height;
    final clientHeight = _clientContainerKey.currentContext?.size?.height;
    setState(() {
      if (_tabController.index == 0) {
        internalContainerHeight = internalHeight ?? internalContainerHeight;
      } else if (_tabController.index == 1) {
        clientContainerHeight = clientHeight ?? clientContainerHeight;
      }
    });
  }

  initiateDataValue() {
    taskID = widget.isSubtask
        ? widget.subtaskInfo?.taskID ?? 0
        : widget.taskInfo?.taskID ?? 0;
    taskName = widget.isSubtask
        ? widget.subtaskInfo?.taskName ?? ''
        : widget.taskInfo?.taskName ?? '';
    taskDescription = widget.isSubtask
        ? widget.subtaskInfo?.forInternal?.taskUpdates ?? ''
        : widget.taskInfo?.forInternal?.taskUpdates ?? '';
    clientTaskDescription = widget.isSubtask
        ? widget.subtaskInfo?.forClient?.taskUpdates ?? ''
        : widget.taskInfo?.forClient?.taskUpdates ?? '';
    completedDateTime = widget.isSubtask
        ? widget.subtaskInfo?.updatedDateTime ?? ''
        : widget.taskInfo?.completedDateTime ?? '';
    createdDateTime = widget.isSubtask
        ? widget.subtaskInfo?.createdDateTime ?? ''
        : widget.taskInfo?.createdDateTime ?? '';
    updatedDateTime = widget.isSubtask
        ? widget.subtaskInfo?.updatedDateTime ?? ''
        : widget.taskInfo?.updatedDateTime ?? '';
    commentCount = widget.isSubtask
        ? widget.subtaskInfo?.replyCount ?? 0
        : widget.taskInfo?.replyCount ?? 0;
        
    internalDocList = widget.isSubtask 
      ? widget.subtaskInfo?.forInternal?.updatesFiles?.where((e) => e.selectedStatus == 1).toList() ?? []
      : widget.taskInfo?.forInternal?.updatesFiles?.where((e) => e.selectedStatus == 1).toList() ?? [];

    internalImageList = widget.isSubtask 
      ? widget.subtaskInfo?.forInternal?.updatesImages?.where((e) => e.selectedStatus == 1).toList() ?? []
      : widget.taskInfo?.forInternal?.updatesImages?.where((e) => e.selectedStatus == 1).toList() ?? [];

    internalMediaArgsList = internalImageList
        .map((m) => MediaInfoArguments(
            mediaPath: m.imagePath ?? '',
            thumbnailPath: m.thumbnailPath ?? '',
            mediaDescription: taskDescription,
            mediaType: m.imageType ?? '',
            datetime: completedDateTime))
        .toList();
    
    clientDocList = widget.isSubtask 
      ? widget.subtaskInfo?.forClient?.updatesFiles?.where((e) => e.selectedStatus == 1).toList() ?? []
      : widget.taskInfo?.forClient?.updatesFiles?.where((e) => e.selectedStatus == 1).toList() ?? [];

    clientImageList = widget.isSubtask 
      ? widget.subtaskInfo?.forClient?.updatesImages?.where((e) => e.selectedStatus == 1).toList() ?? []
      : widget.taskInfo?.forClient?.updatesImages?.where((e) => e.selectedStatus == 1).toList() ?? [];

    clientMediaArgsList = clientImageList
        .map((m) => MediaInfoArguments(
            mediaPath: m.imagePath ?? '',
            thumbnailPath: m.thumbnailPath ?? '',
            mediaDescription: clientTaskDescription,
            mediaType: m.imageType ?? '',
            datetime: completedDateTime))
        .toList();

    updatesPushDateTime = widget.isSubtask
        ? widget.subtaskInfo?.updatesPushDateTime
        : widget.taskInfo?.updatesPushDateTime;

    if (updatesPushDateTime != null) {
      autoPushRemainingTime = DateTime.parse(updatesPushDateTime!)
          .difference(DateTime.now())
          .inSeconds;
    } else {
      autoPushRemainingTime = 0;
    }

    isCancel = widget.isSubtask
        ? widget.subtaskInfo?.isPushCancelled ?? false
        : widget.taskInfo?.isPushCancelled ?? false;
    isShownInCA = widget.isSubtask
        ? (widget.subtaskInfo?.clientAppAutoUpdate == 'yes')
        : (widget.taskInfo?.clientAppAutoUpdate == 'yes');
    isShownMediasInCA = widget.isSubtask
        ? (widget.subtaskInfo?.clientAppPhotoAutoUpdate == 'yes')
        : (widget.taskInfo?.clientAppPhotoAutoUpdate == 'yes');
    imageUpdateCA = widget.isSubtask
        ? widget.subtaskInfo?.isFileRequired ?? 1
        : widget.taskInfo?.isFileRequired ?? 1;
    isPushedToClientApp = widget.isSubtask
        ? widget.subtaskInfo?.isPushedToClientApp ?? ""
        : widget.taskInfo?.isPushedToClientApp ?? "";
    if (isPushedToClientApp == "") {
      setState(() {
        isLoading = true;
      });
    } else { 
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    initiateDataValue();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.isSubtask)
          isLoading
          ? Container(
            width: double.infinity,
            padding: EdgeInsets.all(24.0),
            margin: EdgeInsets.only(bottom: 24.0),
            decoration: BoxDecoration(
                color: white,
                borderRadius: BorderRadius.circular(12.0),
                boxShadow: app_box_shadow),
              child: Column(children: [
                Lottie.asset(
                  lottie_loading_spinner,
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.fill,
                ),
                SizedBox(height: 16.0),
                Text(
                  'just a few secs...',
                  style: regularFontsStyle(fontSize: 14.0, color: grey),
                ),
              ]),
            )
          :
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(24.0),
            margin: EdgeInsets.only(bottom: 24.0),
            decoration: BoxDecoration(
                color: white,
                borderRadius: BorderRadius.circular(12.0),
                boxShadow: app_box_shadow),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: widget.subtaskInfo?.taskStatus == 'completed'
                              ? green
                              : widget.subtaskInfo?.taskStatus == 'flagged'
                                  ? lightPink
                                  : transparent,
                          border: Border.all(
                              color: widget.subtaskInfo?.taskStatus ==
                                      'completed'
                                  ? green
                                  : widget.subtaskInfo?.taskStatus == 'flagged'
                                      ? lightPink
                                      : lightGrey),
                          borderRadius: BorderRadius.circular(500.0)),
                      child: Text(
                        widget.subtaskInfo?.taskStatus == 'completed'
                            ? 'Completed'
                            : widget.subtaskInfo?.taskStatus == 'flagged'
                                ? 'Flagged'
                                : formatDatetime(
                                    datetime: widget
                                            .subtaskInfo?.taskTargetDateTime ??
                                        ''),
                        style: boldFontsStyle(
                            fontSize: 14.0,
                            color: widget.subtaskInfo?.taskStatus == 'completed'
                                ? white
                                : widget.subtaskInfo?.taskStatus == 'flagged'
                                    ? red
                                    : grey),
                      ),
                    ),
                    if (widget.subtaskInfo?.taskStatus == 'completed')
                      SvgPicture.asset(ic_task_complete, width: 26.0),
                    if (widget.subtaskInfo?.taskStatus == 'flagged')
                      SvgPicture.asset(ic_task_flag, width: 26.0),
                  ],
                ),
                SizedBox(height: 16.0),
                if (widget.subtaskInfo?.isSpecial ?? false)
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.only(right: 8.0, top: 3.0),
                        child: SvgPicture.asset(ic_star, width: 20.0)
                      ), 
                      Flexible(
                        child: Column(
                          children: [
                            Text(
                              widget.subtaskInfo?.taskName ?? '',
                              style: boldFontsStyle(fontSize: 20.0, height: 1.4),
                              maxLines: 3,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                if (!(widget.subtaskInfo?.isSpecial ?? false))
                Text(
                  widget.subtaskInfo?.taskName ?? '',
                  style: boldFontsStyle(fontSize: 20.0, height: 1.4),
                ),
                if (widget.subtaskInfo?.taskDescription != '' &&
                    widget.subtaskInfo?.taskDescription != null)
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: Text(
                      widget.subtaskInfo?.taskDescription ?? '',
                      style: regularFontsStyle(fontSize: 16.0, height: 1.5),
                    ),
                  ),
                  SizedBox(height: 8.0),
                  //subtask requried files description
                  if(widget.subtaskInfo?.isFileRequiredLabel != '')
                    Row(
                      children: [
                        SvgPicture.asset(ic_attach, width: 18.0),
                        SizedBox(width: 8.0),
                        RichText(
                          text: HTML.toTextSpan(
                            context,
                            widget.subtaskInfo?.isFileRequiredLabel ?? "",
                            defaultTextStyle: regularFontsStyle(fontSize: 14.0, color: grey),
                          ),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                SizedBox(height: 16.0),
                // Comments Layout
                CommentOutlineButton(
                  taskID: widget.subtaskInfo!.taskID!,
                  subtaskInfo: widget.subtaskInfo!,
                  isSubtask: true,
                  callback: widget.callback,
                ),
                SizedBox(height: 20.0),
                if (widget.subtaskInfo?.taskStatus == "flagged")
                  Container(
                    decoration: BoxDecoration(
                      color: pink,
                      border: Border.all(
                        color: red,
                        width: 1.0,
                        style: BorderStyle.solid,
                      ),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    padding:
                        EdgeInsets.symmetric(vertical: 12.0, horizontal: 12.0),
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        //remarks
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              //flagLabel
                              'This update is disapproved',
                              style: boldFontsStyle(
                                  fontSize: 14.0, height: 1.8, color: red),
                            ),
                            //flagRemark
                            Padding(
                              padding: const EdgeInsets.only(bottom: 6.0),
                              child: ContentWidget(
                                content: widget.subtaskInfo!.flagRemark ?? '',
                                contentColor: red,
                                fontSize: 14.0,
                                linkColor: linkYellow,
                              ),
                            )
                          ],
                        ),
                      ],
                      // doc listing layout
                    ),
                  ),
                SizedBox(height: 8.0),
                imageWithInfoLayout(),
                if (widget.subtaskInfo?.taskStatus != "flagged")
                Padding(
                  padding: EdgeInsets.only(top: 24.0),
                  child: pushUpdateLayout(),
                ),
                // Resolved Task Button
                if (widget.subtaskInfo?.taskStatus == "flagged")
                  Padding(
                    padding: EdgeInsets.only(top: 24.0),
                    child: Container(
                      margin: EdgeInsets.only(left: 8.0),
                      child: GestureDetector(
                        onTap: () async {
                          bool response = await onManualPush(
                              context,
                              PushUpdateInfoArguments(
                                  taskID: widget.isSubtask
                                      ? widget.subtaskInfo?.taskID ?? 0
                                      : widget.taskInfo?.taskID ?? 0,
                                  projectID: widget.taskInfo?.projectID,
                                  action: "resolve",
                                  remark: '',
                                  imageUpdateCA: widget
                                          .taskInfo?.clientAppPhotoAutoUpdate ==
                                      'yes',
                                  updateScreen: "Resolve Task",
                                  isFileRequired:
                                      widget.subtaskInfo?.isFileRequired == 1,
                                  specialTaskType: widget.isSubtask ? widget.subtaskInfo?.specialTaskType : widget.taskInfo?.specialTaskType,
                                  linkValue: widget.taskInfo?.forInternal?.dropboxLink,
                                  taskRelatedDate: widget.subtaskInfo?.taskRelatedDate ?? widget.taskInfo?.taskRelatedDate,
                                  forClient: widget.isSubtask
                                      ? widget.subtaskInfo?.forClient
                                      : widget.taskInfo?.forClient,
                                  forInternal: widget.isSubtask
                                      ? widget.subtaskInfo?.forInternal
                                      : widget.taskInfo?.forInternal,
                                ));
                          // if(response) goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: widget.taskInfo?.taskID));
                          if (response) widget.callback!();
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              vertical: 12.0, horizontal: 52.0),
                          decoration: BoxDecoration(
                            color: yellow,
                            borderRadius: BorderRadius.circular(50),
                          ),
                          child: Text(
                            'Resolve Task',
                            style: boldFontsStyle(fontSize: 16.0, height: 1.5),
                          ),
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        if (!widget.isSubtask && widget.taskInfo?.taskStatus == 'flagged')
          Container(
            decoration: BoxDecoration(
              color: pink,
              border: Border.all(
                color: red,
                width: 1.0,
                style: BorderStyle.solid,
              ),
              borderRadius: BorderRadius.circular(8.0),
            ),
            padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 12.0),
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //remarks
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      //flagLabel
                      'This update is disapproved',
                      style: boldFontsStyle(
                          fontSize: 14.0, height: 1.8, color: red),
                    ),
                    //flagRemark
                    Padding(
                      padding: const EdgeInsets.only(bottom: 6.0),
                      child: ContentWidget(
                        content: widget.taskInfo!.flagRemark ?? '',
                        contentColor: red,
                        fontSize: 14.0,
                        linkColor: linkYellow,
                      ),
                    )
                  ],
                ),
              ],
              // doc listing layout
            ),
          ),
        SizedBox(height: 8.0),
        if (!widget.isSubtask) imageWithInfoLayout(),
        if (!widget.isSubtask && widget.taskInfo?.taskStatus == 'flagged')
          resolveTaskLayout(),
        if (!widget.isSubtask && widget.taskInfo?.taskStatus != 'flagged') 
          pushUpdateLayout(),        
      ],
    );
  }

  Widget imageWithInfoLayout() => Container(
      width: double.infinity,
      margin: EdgeInsets.only(bottom: widget.isSubtask ? 0.0 : 24.0),
      decoration: BoxDecoration(
        color:
            (!widget.isSubtask && widget.taskInfo?.taskStatus == "flagged") ||
                    (widget.isSubtask &&
                        widget.subtaskInfo?.taskStatus == "flagged")
                ? pink
                : transparent,
        border: Border.all(
            width: 1.0,
            color: (!widget.isSubtask &&
                        widget.taskInfo?.taskStatus == "flagged") ||
                    (widget.isSubtask &&
                        widget.subtaskInfo?.taskStatus == "flagged")
                ? red
                : transparent),
        borderRadius: BorderRadius.all(
          Radius.circular(12.0),
        ),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(12.0),
          child: Column(
            children: [
              infoLayout(tabController: _tabController),
            ],
          ),
        ),
    );

  // image custom grid view
  Widget imageGridView(List<AttachModel> imageList, List<MediaInfoArguments> mediaInfoList, {int flex = 6}) =>
      Container(
        height: 100.0,
        child: Row(
          children: [
            imageBoxLayout(imageList, 0, mediaInfoList),
            if (imageList.length > 1) SizedBox(width: 8.0),
            if (imageList.length > 1) imageBoxLayout(imageList, 1, mediaInfoList),
            if (imageList.length > 2) SizedBox(width: 8.0),
            if (imageList.length > 2) imageBoxLayout(imageList, 2, mediaInfoList),
          ],
        ),
      );

  Widget imageBoxLayout(List<AttachModel> imageUrl, int index, List<MediaInfoArguments> mediaInfoList) => Container(
      width: 70,
      height: 70,
      decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(6),
      border: Border.all(color: Colors.grey, width: 1),
    ),
        child: Stack(
          children: [
            imageUrl[index].imageType == 'mp4'
                ? Stack(alignment: AlignmentDirectional.center, children: [
                    Container(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(6.0),
                        child: CachedNetworkImage(
                          imageUrl: imageUrl[index].thumbnailPath ?? '',
                          height: double.infinity,
                          width: double.infinity,
                          fit: BoxFit.cover,
                          maxHeightDiskCache: 120,
                          maxWidthDiskCache: 120,
                        ),
                      ),
                    ),
                    Container(
                      constraints: BoxConstraints(maxHeight: 36.0),
                      child: SvgPicture.asset(
                        ic_play_button,
                      ),
                    ),
                  ])
                : ClipRRect(
                  borderRadius: BorderRadius.circular(6.0),
                  child: CachedNetworkImage(
                      imageUrl: imageUrl[index].imagePath ?? '',
                      height: double.infinity,
                      width: double.infinity,
                      fit: BoxFit.cover,
                      maxHeightDiskCache: 120,
                      maxWidthDiskCache: 120,
                    ),
                ),
            if (index > 1 && imageUrl.length > 3)
              Opacity(
                opacity: 0.5,
                child: Container(
                  color: black,
                ),
              ),
            if (index > 1 && imageUrl.length > 3)
              Center(
                child: Text(
                  '+${imageUrl.length - 2}',
                  style: boldFontsStyle(
                      fontFamily: platForm,
                      fontSize: 30.0,
                      height: 1.33,
                      color: white,
                      letterSpacing: 0.8),
                ),
              ),
            Material(
              color: transparent,
              child: InkWell(
                // splashColor: grey,
                onTap: () {
                  // go to media viewer
                   goToNextNamedRoute(context, mediaSliderScreen,
                      args: MediaSliderArguments(mediaInfoList, index));
                },
              ),
            ),
          ],
        ),
      );

  Widget infoLayout({required TabController tabController, int flex = 5}) {
    return Container(
      decoration: BoxDecoration(
        color: darkGrey,
        border: Border(
            top: BorderSide(
          color:
              (!widget.isSubtask && widget.taskInfo?.taskStatus == "flagged") ||
                      (widget.isSubtask &&
                          widget.subtaskInfo?.taskStatus == "flagged")
                  ? red
                  : transparent,
        )),
      ),
      padding: EdgeInsets.only(left: 24.0, right: 24.0, bottom: 24.0, top: 6.0),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: TabBar(
              tabAlignment:TabAlignment.start,
              controller: _tabController,
              isScrollable: true,
              indicatorColor: darkYellow,
              labelStyle: boldFontsStyle(color: white, fontSize: 14.0, height: 1.35),
              unselectedLabelStyle: boldFontsStyle(color: white, fontSize: 14.0, height: 1.35),
              indicatorSize: TabBarIndicatorSize.tab,
              dividerColor: transparent,
              tabs: [
                Tab(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(ic_team),
                      SizedBox(width: 8),
                      Text('For Internal'),
                    ],
                  ),
                ),
                Tab(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(ic_moggy_head),
                      SizedBox(width: 8),
                      Text('For Client'),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 8.0),
          CustomTabView.AutoScaleTabBarView(
            controller: _tabController,
            children: [
              // For Internal
              Container(
                child: Container(
                  key: _internalContainerKey,
                  height: internalContainerHeight,
                  child: RawScrollbar(
                    thumbVisibility: true,
                    thumbColor: darkYellow,
                    trackVisibility: true,
                    trackColor: darkGrey,
                    minOverscrollLength: 50,
                    radius: Radius.circular(25),
                    thickness: 4,
                    minThumbLength: 80,
                    child: SingleChildScrollView(
                      padding: EdgeInsets.only(right: 7),
                      child: Container(
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 8.0),
                              child: Center(
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      child: RichText(
                                        text: HTML.toTextSpan(
                                          context,
                                          (!widget.isSubtask &&
                                                  (widget.taskInfo?.taskStatus != "flagged" ||
                                                      widget.taskInfo?.taskStatus == "flagged"))
                                              ? '${widget.taskInfo?.forInternal?.taskCompletedBy}'
                                              : '${widget.subtaskInfo?.forInternal?.taskCompletedBy}',
                                          defaultTextStyle: boldFontsStyle(fontSize: 14.0, color: white)
                                              .copyWith(height: 24 / 14.0),
                                        ),
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 8.0),
                              child: Center(
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      child: RichText(
                                        text: HTML.toTextSpan(
                                          context,
                                          (!widget.isSubtask &&
                                                  (widget.taskInfo?.taskStatus != "flagged" ||
                                                      widget.taskInfo?.taskStatus == "flagged"))
                                              ? '${widget.taskInfo?.forInternal?.taskUpdates!.isNotEmpty == true 
                                                    ? widget.taskInfo?.forInternal?.taskUpdates : "-"}'
                                              : '${widget.subtaskInfo?.forInternal?.taskUpdates!.isNotEmpty == true 
                                                    ? widget.subtaskInfo?.forInternal?.taskUpdates : "-"}',
                                          defaultTextStyle: boldFontsStyle(fontSize: 14.0, color: white)
                                              .copyWith(height: 24 / 14.0),
                                        ),
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            // Extra Dates
                            if (widget.subtaskInfo?.forInternal?.extraDatesDisplayArr != null)
                            Column(
                              children: [
                                Container(
                                  width: double.infinity,
                                  margin: EdgeInsets.only(
                                    top: 8.0, 
                                    bottom: widget.subtaskInfo?.specialTaskType != null && 
                                      (internalImageList.isEmpty && internalDocList.isEmpty) ? 16.0 : 8.0
                                  ),
                                  padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                                  decoration: BoxDecoration(
                                    border: Border.all(color: white),
                                    borderRadius: BorderRadius.all(Radius.circular(6.0)),
                                  ),
                                  child: RichText(
                                    text: TextSpan(
                                      text: '${widget.subtaskInfo?.forInternal?.extraDatesDisplayArr?.label}: ',
                                      style: regularFontsStyle(fontSize: 16.0, height: 1.5, color: white),
                                      children: [
                                        TextSpan(
                                          text: '${widget.subtaskInfo?.forInternal?.extraDatesDisplayArr?.date}',
                                          style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: white),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            // Display Images
                             if(internalImageList.length > 0)
                            imageGridView(internalImageList, internalMediaArgsList),
                            // Internal Document Lists
                            if(internalDocList.length > 0)
                            Container(
                              margin: EdgeInsets.only(top: 8.0,bottom: 8.0),
                              child: ExpandableNotifier(
                                child: Expandable(
                                  collapsed: Wrap(
                                    children: [
                                      for (int i = 0; i < internalDocList.length && i < 3; i++)
                                        docBoxLayout(internalDocList[i], i),
                                      if (internalDocList.length > 3)
                                        ExpandableButton(
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 12.0),
                                            child: Text(
                                              '${internalDocList.length - 3} More...',
                                              style: boldFontsStyle(
                                                  fontSize: 14.0, color: Colors.yellow),
                                            ),
                                          ),
                                        ),
                                    ],
                                  ),
                                  expanded: Wrap(
                                    children: [
                                      for (int i = 0; i < internalDocList.length; i++)
                                        docBoxLayout(internalDocList[i], i),
                                      ExpandableButton(
                                        child: Container(
                                          margin: EdgeInsets.only(bottom: 12.0),
                                          child: Text(
                                            'Collapse..',
                                            style: boldFontsStyle(
                                                fontSize: 14.0, color: Colors.yellow),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            // Dropbox Link
                            if (widget.taskInfo?.specialTaskType == "edited-photo" && widget.taskInfo?.forInternal?.dropboxLink != null)
                            Container(
                              alignment: Alignment.topLeft,
                              margin: EdgeInsets.only(top: 8.0,bottom: 8.0),
                              padding: EdgeInsets.symmetric(
                                  vertical: 8.0, horizontal: 16.0),
                              decoration: BoxDecoration(
                                  border: Border.all(color: white),
                                  borderRadius: BorderRadius.circular(6.0)),
                              child: GestureDetector(
                                onTap: () {
                                  launch(widget.taskInfo?.forInternal?.dropboxLink ?? '');
                                },
                                child: Text(
                                  widget.taskInfo?.forInternal?.dropboxLink ?? '',
                                  style: boldFontsStyle(
                                      fontSize: 16.0, height: 1.5, color: yellow),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              // For Client
              Container(
                child: Container(
                  key: _clientContainerKey,
                  height: clientContainerHeight,
                  child: RawScrollbar(
                    thumbVisibility: true,
                    thumbColor: darkYellow,
                    trackVisibility: true,
                    trackColor: darkGrey,
                    minOverscrollLength: 50,
                    radius: Radius.circular(25),
                    thickness: 4,
                    minThumbLength: 80,
                    child: SingleChildScrollView(
                      padding: EdgeInsets.only(right: 7),
                      child: Container(
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
                              child: Center(
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Align(
                                      alignment: Alignment.topCenter,
                                      child: Padding(
                                        padding: EdgeInsets.only(top: 6.0),
                                        child: SvgPicture.asset(ic_moggy_head, width: 26),
                                      ),
                                    ),
                                  SizedBox(width: 8),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            if (!widget.isSubtask &&
                                              widget.taskInfo?.taskStatus != "flagged" &&
                                              widget.taskInfo?.forClient?.taskCompletedBy?.isNotEmpty == true)
                                            RichText(
                                              text: HTML.toTextSpan(
                                                context,
                                                '${widget.taskInfo?.forClient?.taskCompletedBy}',
                                                defaultTextStyle: boldFontsStyle(
                                                  fontSize: 14.0,
                                                  color: white,
                                                ).copyWith(height: 24 / 14.0),
                                              ),
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          if (widget.isSubtask &&
                                              widget.subtaskInfo?.forClient?.taskCompletedBy?.isNotEmpty == true)
                                            RichText(
                                              text: HTML.toTextSpan(
                                                context,
                                                '${widget.subtaskInfo?.forClient?.taskCompletedBy}',
                                                defaultTextStyle: boldFontsStyle(
                                                  fontSize: 14.0,
                                                  color: white,
                                                ).copyWith(height: 24 / 14.0),
                                              ),
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            RichText(
                                              text: HTML.toTextSpan(
                                                context,
                                                  (!widget.isSubtask &&
                                                    (widget.taskInfo?.taskStatus != "flagged" ||
                                                      widget.taskInfo?.taskStatus == "flagged"))
                                                    ? '${widget.taskInfo?.forClient?.taskUpdates!.isNotEmpty == true 
                                                    ? widget.taskInfo?.forClient?.taskUpdates : "-"}'
                                                    : '${widget.subtaskInfo?.forClient?.taskUpdates!.isNotEmpty == true 
                                                    ? widget.subtaskInfo?.forClient?.taskUpdates : "-"}',
                                                      defaultTextStyle: boldFontsStyle(
                                                      fontSize: 14.0,
                                                      color: white,
                                                ).copyWith(height: 24 / 14.0),
                                              ),
                                                maxLines: 2,
                                                  overflow: TextOverflow.ellipsis,
                                              ),
                                            ],
                                        ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            // Extra Dates
                          if (widget.subtaskInfo?.forClient?.extraDatesDisplayArr != null && widget.subtaskInfo?.forClient?.extraDatesDisplayArr?.date != '')
                            Column(
                              children: [
                                Container(
                                  width: double.infinity,
                                  margin: EdgeInsets.only(
                                    top: 8.0, 
                                    bottom: widget.subtaskInfo?.specialTaskType != null && 
                                      (internalImageList.isEmpty && internalDocList.isEmpty) ? 16.0 : 8.0
                                  ),
                                  padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                                  decoration: BoxDecoration(
                                    border: Border.all(color: white),
                                    borderRadius: BorderRadius.all(Radius.circular(6.0)),
                                  ),
                                  child: RichText(
                                    text: TextSpan(
                                      text: '${widget.subtaskInfo?.forClient?.extraDatesDisplayArr?.label}: ',
                                      style: regularFontsStyle(fontSize: 16.0, height: 1.5, color: white),
                                      children: [
                                        TextSpan(
                                          text: '${widget.subtaskInfo?.forClient?.extraDatesDisplayArr?.date}',
                                          style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: white),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            // Display Images
                            if(clientImageList.length > 0)
                            imageGridView(clientImageList, clientMediaArgsList),
                            // Client Document Lists
                            if(clientDocList.length > 0)
                            Container(
                              margin: EdgeInsets.only(top: 8.0,bottom: 8.0),
                              child: ExpandableNotifier(
                                child: Expandable(
                                  collapsed: Wrap(
                                    children: [
                                      for (int i = 0; i < clientDocList.length && i < 3; i++)
                                        docBoxLayout(clientDocList[i], i),
                                      if (clientDocList.length > 3)
                                        ExpandableButton(
                                          child: Container(
                                            margin: EdgeInsets.only(bottom: 12.0),
                                            child: Text(
                                              '${clientDocList.length - 3} More...',
                                              style: boldFontsStyle(fontSize: 14.0, color: Colors.yellow),
                                            ),
                                          ),
                                        ),
                                    ],
                                  ),
                                  expanded: Wrap(
                                    children: [
                                      for (int i = 0; i < clientDocList.length; i++)
                                        docBoxLayout(clientDocList[i], i),
                                      ExpandableButton(
                                        child: Container(
                                          margin: EdgeInsets.only(bottom: 12.0),
                                          child: Text(
                                            'Collapse..',
                                            style: boldFontsStyle(fontSize: 14.0, color: Colors.yellow),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            // Dropbox Link
                            if (widget.taskInfo?.specialTaskType == "edited-photo")
                            Container(
                              alignment: Alignment.topLeft,
                              margin: EdgeInsets.only(top: 8.0,bottom: 8.0),
                              padding: EdgeInsets.symmetric(
                                  vertical: 8.0, horizontal: 16.0),
                              decoration: BoxDecoration(
                                  border: Border.all(color: white),
                                  borderRadius: BorderRadius.circular(6.0)),
                              child: GestureDetector(
                                onTap: () {
                                  launch(widget.taskInfo?.forClient?.dropboxLink ?? '');
                                },
                                child: Text(
                                  widget.taskInfo?.forClient?.dropboxLink ?? '',
                                  style: boldFontsStyle(
                                      fontSize: 16.0, height: 1.5, color: yellow),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: updatesPushDateTime != null ? 16.0 : (widget.subtaskInfo?.forInternal?.extraDatesDisplayArr != null) ? 0.0 : 8.0),
          // date time layout
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Expanded(
                flex: isPushedToClientApp == 'pushed' ? 8 : 12,
                child: Column(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              formatDatetime(
                                datetime: updatedDateTime != '' && updatedDateTime != null
                                    ? updatedDateTime
                                    : createdDateTime,
                              ),
                              style: regularFontsStyle(
                                fontSize: 12.0,
                                height: 1.5,
                                color: white,
                              ),
                            ),
                            SizedBox(width: 8.0,),
                            Container(
                              width: 4.0,
                              height: 4.0,
                              margin: EdgeInsets.all(6.0),
                              decoration: BoxDecoration(
                                color: white,
                                borderRadius: BorderRadius.circular(500.0),
                              ),
                            ),
                            SizedBox(width: 8.0,),
                            if((widget.isSubtask == false &&
                                widget.taskInfo?.taskStatus == "flagged") ||
                                (widget.isSubtask == true &&
                                    widget.subtaskInfo?.taskStatus == "flagged") ||
                                isPushedToClientApp == 'pushed')
                            Text(
                              formatDatetime(
                                datetime: updatedDateTime != '' && updatedDateTime != null
                                    ? updatedDateTime
                                    : createdDateTime,
                                format: 'hh:mma',
                              ).toLowerCase(),
                              style: regularFontsStyle(
                                fontSize: 12.0,
                                height: 1.5,
                                color: white,
                              ),
                            ),
                          ],
                        ),
                        ((widget.isSubtask == false &&
                              widget.taskInfo?.taskStatus == "flagged") ||
                          (widget.isSubtask == true &&
                              widget.subtaskInfo?.taskStatus == "flagged") ||
                          isPushedToClientApp == 'pushed')
                        ? Container()
                        : Text(
                              formatDatetime(
                                datetime: updatedDateTime != '' && updatedDateTime != null
                                    ? updatedDateTime
                                    : createdDateTime,
                                format: 'hh:mma',
                              ).toLowerCase(),
                              style: regularFontsStyle(
                                fontSize: 12.0,
                                height: 1.5,
                                color: white,
                              ),
                            ),
                      ],
                    ),
                    if (isPushedToClientApp == 'pushed' && _tabController.index == 1)
                    // isPushedToClientApp == 'pushed', Add the text if the task had push
                    Column(
                      children: [
                        Row(
                          children: [
                            Text('Updates is published to client', style: italicFontsStyle(color: white, fontSize: 14.0, fontWeight: FontWeight.w700, height:1.54 ),),
                          ],
                        )
                      ],
                    )
                  ],
                ),
              ),
              (widget.isSubtask == false &&
                          widget.taskInfo?.taskStatus == "flagged") ||
                      (widget.isSubtask == true &&
                          widget.subtaskInfo?.taskStatus == "flagged") ||
                      isPushedToClientApp == 'pushed'
                  ? Container()
                  : Expanded(
                      flex: MediaQuery.of(context).size.width > 374.0 ? 12 : 4,
                      child: GestureDetector(
                        onTap: () async {
                          bool response = await onManualPush(
                              context,
                              PushUpdateInfoArguments(
                                  taskID: widget.isSubtask
                                      ? widget.subtaskInfo?.taskID ?? 0
                                      : widget.taskInfo?.taskID ?? 0,
                                  projectID: widget.taskInfo?.projectID,
                                  action: "flagged",
                                  imageUpdateCA: widget
                                          .taskInfo?.clientAppPhotoAutoUpdate ==
                                      'yes',
                                  updatesImages: widget.isSubtask
                                      ? widget.subtaskInfo?.forInternal?.updatesImages ?? []
                                      : widget.taskInfo?.forInternal?.updatesImages ?? [],
                                  updatesFiles: widget.isSubtask
                                      ? widget.subtaskInfo?.forInternal?.updatesFiles ?? []
                                      : widget.taskInfo?.forInternal?.updatesFiles ?? [],
                                  updateScreen: "Flagged",
                                  isFileRequired: widget.subtaskInfo?.isFileRequired == 1,
                              ));
                          // if(response) goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: widget.taskInfo?.taskID));
                          if (response) widget.callback!();
                        },
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 9.38, horizontal: MediaQuery.of(context).size.width > 374.0 ? 21.0 : 11.0),
                                decoration: BoxDecoration(
                                    border:
                                        Border.all(width: 1.0, color: white),
                                    borderRadius: BorderRadius.circular(500.0)),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    SvgPicture.asset(ic_task_transparent_flag),
                                    if(MediaQuery.of(context).size.width > 374.0) SizedBox(width: 9.38),
                                    if(MediaQuery.of(context).size.width > 374.0)
                                    Text(
                                      "Flag",
                                      style: boldFontsStyle(
                                          fontSize: 14.0, color: white),
                                    ),
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ),
              if ((widget.isSubtask == false && widget.taskInfo?.taskStatus != "flagged") ||
                  (widget.isSubtask == true && widget.subtaskInfo?.taskStatus != "flagged"))
                Flexible(
                  flex: 0,
                  child: Container(
                    margin: EdgeInsets.only(left: 8.0),
                    child: GestureDetector(
                      onTap: () async {
                        bool response = await onManualPush(
                          context,
                          PushUpdateInfoArguments(
                            taskID: widget.isSubtask
                                ? widget.subtaskInfo?.taskID ?? 0
                                : widget.taskInfo?.taskID ?? 0,
                            action: "update",
                            isShownMediasInCA: isShownMediasInCA,
                            imageUpdateCA: widget.taskInfo?.clientAppPhotoAutoUpdate ==
                                'yes',
                            updateScreen: "Edit Update",
                            isFileRequired: widget.isSubtask
                                ? widget.subtaskInfo?.isFileRequired == 1
                                : widget.taskInfo?.isFileRequired == 1,
                            specialTaskType: widget.isSubtask
                                ? widget.subtaskInfo?.specialTaskType
                                : widget.taskInfo?.specialTaskType,
                            linkValue: widget.taskInfo?.forInternal?.dropboxLink,
                            taskRelatedDate: widget.subtaskInfo?.taskRelatedDate ??
                                widget.taskInfo?.taskRelatedDate,
                            forClient: widget.isSubtask
                                ? widget.subtaskInfo?.forClient
                                : widget.taskInfo?.forClient,
                            forInternal: widget.isSubtask
                                ? widget.subtaskInfo?.forInternal
                                : widget.taskInfo?.forInternal,
                          ),
                        );
                        // if(response) goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: widget.taskInfo?.taskID));
                        if (response) widget.callback!();
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                        decoration: BoxDecoration(
                          color: yellow,
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: SvgPicture.asset(ic_edit, width: 20.0),
                      ),
                    ),
                  ),
                ),
            ],
          ),
        ],
      ),
    );
  }

  Widget docBoxLayout(AttachModel file, int index) => Column(
        children: [
          Material(
            color: transparent,
            child: InkWell(
              borderRadius: BorderRadius.circular(500.0),
              onTap: () async {
                // go to pdf viewer
                String? temporaryPath;
                String? savedPath = SpManager.getString(file.imagePath.toString());
                if (savedPath != null && await File(savedPath).exists()) {
                  temporaryPath = savedPath;
                } else {
                  showToastMessage(context, message: 'Downloading', duration: 1);
                  await Api().downloadFile(file.imagePath ?? '', 'pdf', context).then((localSavedPath) {
                    temporaryPath = localSavedPath;
                  });
                  await SpManager.putString(file.imagePath.toString(), temporaryPath ?? '');
                  await Future.delayed(Duration(milliseconds: 1500));
                }
                await OpenFilex.open(temporaryPath);
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                decoration: BoxDecoration(
                    color: transparent,
                    border: Border.all(width: 1.0, color: white),
                    borderRadius: BorderRadius.circular(500.0)),
                child: Row(
                  children: [
                    SvgPicture.asset(ic_doc, width: 18.0, color: yellow),
                    SizedBox(width: 8.0),
                    Expanded(
                      child: Text(
                        file.imageName ??
                            file.imagePath!.substring(
                                file.imagePath!.lastIndexOf('/') + 1),
                        style: regularFontsStyle(fontSize: 14.0, color: white),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 12.0),
        ],
      );

  Widget resolveTaskLayout() {
    return Container(
      margin: EdgeInsets.only(left: 8.0),
      child: GestureDetector(
        onTap: () async {
          bool response = await onManualPush(
              context,
              PushUpdateInfoArguments(
                  taskID: widget.taskInfo?.taskID,
                  projectID: widget.taskInfo?.projectID,
                  action: "resolve",
                  remark: '',
                  imageUpdateCA:
                      widget.taskInfo?.clientAppPhotoAutoUpdate == 'yes',
                  updateScreen: "Resolve Task",
                  isFileRequired: widget.isSubtask ? widget.subtaskInfo?.isFileRequired == 1 : widget.taskInfo?.isFileRequired == 1,
                  specialTaskType: widget.isSubtask ? widget.subtaskInfo?.specialTaskType : widget.taskInfo?.specialTaskType,
                  linkValue: widget.taskInfo?.forInternal?.dropboxLink,
                  taskRelatedDate: widget.subtaskInfo?.taskRelatedDate ?? widget.taskInfo?.taskRelatedDate,
                  forClient: widget.isSubtask
                    ? widget.subtaskInfo?.forClient
                    : widget.taskInfo?.forClient,
                  forInternal: widget.isSubtask
                    ? widget.subtaskInfo?.forInternal
                    : widget.taskInfo?.forInternal,
                ));
            // if(response) goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: widget.taskInfo?.taskID));
          if (response) widget.callback!();
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 52.0),
          decoration: BoxDecoration(
            color: yellow,
            borderRadius: BorderRadius.circular(50),
          ),
          child: Text(
            'Resolve Task',
            style: boldFontsStyle(fontSize: 16.0, height: 1.5),
          ),
        ),
      ),
    );
  }

  Widget pushUpdateLayout() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (isPushedToClientApp == 'cancelled' ||
            (isPushedToClientApp == 'pending' && autoPushRemainingTime == 0))
          ElevatedButton(
            onPressed: () async {
              var confirmPushed = await showDecision(
                context, 
                'Are you sure you want to notify client about the update?', 
                'Cancel', 
                'Notify Client',
                buttonColor: yellow,
                textColor: black,
              );
              if (confirmPushed) {
                setState(() {
                  onlyClickOnce = onlyClickOnce + 1;
                });
                if (widget.subtaskInfo?.forInternal?.updatesImages?.length != 0) {
                  updatesImages?.forEach((image) {
                    if (image.selectedStatus == 1 && image.taskImageID != null)
                      uploadedAttachmentIDs.add(image.taskImageID!);
                  });
                }

                if (widget.subtaskInfo?.forInternal?.updatesFiles?.length != 0) {
                  updatesFiles?.forEach((file) {
                    if (file.selectedStatus == 1 && file.taskImageID != null)
                      uploadedAttachmentIDs.add(file.taskImageID!);
                  });
                }
                if (onlyClickOnce == 1) {
                  await _taskService.postTaskPushClientApp(
                    PushUpdateModel(
                      action: "push",
                      taskID: widget.isSubtask
                          ? widget.subtaskInfo?.taskID
                          : widget.taskInfo?.taskID,
                      taskImageID: uploadedAttachmentIDs,
                      remark: widget.isSubtask
                          ? widget.subtaskInfo?.forInternal?.taskUpdates
                          : widget.taskInfo?.forInternal?.taskUpdates,
                    ),
                  ).timeout(Duration(seconds: 60), onTimeout: () {
                    onlyClickOnce = 0;
                    // goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: widget.taskInfo?.taskID));
                    widget.callback!();
                  }).then((response) {
                    setState(() {
                      autoPushRemainingTime = 0;
                      isCancel = false;
                      isShownInCA = false;
                    });
                  }).whenComplete(() {
                    // goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: widget.taskInfo?.taskID));
                    widget.callback!();
                    setState(() {
                      onlyClickOnce = 0;
                    });
                  }).onError((error, stackTrace) {
                    // goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: widget.taskInfo?.taskID));
                    widget.callback!();
                    setState(() {
                      onlyClickOnce = 0;
                    });
                  });
                }
              }
            },
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                SvgPicture.asset(ic_logo, width: 24.0),
                SizedBox(width: 8.0),
                Text(
                  'Notify Client',
                  style: boldFontsStyle(fontSize: 16.0, height: 1.5),
                ),
              ],
            ),
            style: ElevatedButton.styleFrom(
              backgroundColor: yellow,
              padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)),
            ),
          ),
        if (isPushedToClientApp == 'pending' && !isCancel && isShownInCA)
          cancelAndPushButton(),
        if (isPushedToClientApp == 'pushed')
        ElevatedButton(
          onPressed: () async {
            bool decision = await showDecision(
              context, 
              'Are you sure you want to unpublish the update to client?', 
              'Cancel', 
              'Unpublish',
            );
            if (decision) {
              // widget.setLoadingState(true);
              setState(() {
                onlyClickOnce = onlyClickOnce + 1;
              });

              if (onlyClickOnce == 1) {
                await _taskService.postUnpushedTaskPushClientApp(taskID).then((response) {
                  // goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: widget.taskInfo?.taskID));
                  widget.callback!();
                }).timeout(Duration(seconds: 60), onTimeout: () {
                  setState(() {
                    onlyClickOnce = 0;
                  });
                }).whenComplete(() {
                  setState(() {
                    onlyClickOnce = 0;
                  });
                });
              }
              // widget.setLoadingState(false);
            }
          }, 
          child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(width: 8.0),
                Text(
                  'Unpublish Update To Client',
                  style: boldFontsStyle(fontSize: 16.0, height: 1.5),
                ),
              ],
            ),
            style: ElevatedButton.styleFrom(
              backgroundColor: (widget.isSubtask) ? white : whiteGrey,
              side: BorderSide(color: black),
              padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)),
            ),
        ),
      ],
    );
  }

  Widget cancelAndPushButton() => Container(
        padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 24.0),
        decoration: BoxDecoration(
          color: white,
          border: Border.all(color: lightGrey),
          borderRadius: BorderRadius.circular(12.0),
        ),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                    padding: EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                        color: yellow,
                        borderRadius: BorderRadius.circular(500.0)),
                    child: SvgPicture.asset(ic_logo, width: 20.0)),
                SizedBox(width: 16.0),
                Expanded(
                  child: Countdown(
                      seconds: autoPushRemainingTime,
                      build: (context, time) {
                        Duration duration = Duration(seconds: time.toInt());
                        return RichText(
                            text: TextSpan(children: [
                          TextSpan(
                              text: 'This will be ',
                              style: regularFontsStyle(fontSize: 14.0)),
                          TextSpan(
                              text: 'auto-publish to client ',
                              style: boldFontsStyle(fontSize: 14.0)),
                          TextSpan(
                              text: 'in ',
                              style: regularFontsStyle(fontSize: 14.0)),
                          TextSpan(
                              text:
                                  "${duration.inHours}hrs : ${duration.inMinutes % 60}mins : ${duration.inSeconds % 60}secs",
                              style: boldFontsStyle(fontSize: 16.0)),
                        ]));
                      }),
                ),
              ],
            ),
            SizedBox(height: 16.0),
            // unpublish task
            Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    onPressed: () async {
                      bool decision = await showDecision(
                        context, 
                        'Are you sure you want to cancel auto-publish of the update to client?', 
                        'Nope', 
                        'Yes',
                      );
                      if (decision) {
                        setState(() {
                          onlyClickOnce = onlyClickOnce + 1;
                        });

                        if (onlyClickOnce == 1) {
                            _taskService
                              .postTaskPushClientApp(PushUpdateModel(
                          taskID: widget.isSubtask
                              ? widget.subtaskInfo?.taskID
                              : widget.taskInfo?.taskID,
                          action: "cancel",
                          ))
                              .then((response) {
                            setState(() {
                              autoPushRemainingTime = 0;
                              isCancel = true;
                            });
                            // goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: widget.taskInfo?.taskID));
                            widget.callback!();
                          }).timeout(Duration(seconds: 60), onTimeout: () {
                            setState(() {
                              onlyClickOnce = 0;
                            });
                          }).whenComplete(() {
                            setState(() {
                              onlyClickOnce = 0;
                            });
                          });
                        }
                      }
                    },
                    child: Text(
                      'Cancel',
                      style: boldFontsStyle(
                          fontSize: 16.0, color: white, height: 1.5),
                    ),
                    style: ElevatedButton.styleFrom(
                      backgroundColor: red,
                      padding: EdgeInsets.symmetric(vertical: 12.0),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0)),
                    ),
                  ),
                ),
                SizedBox(width: 16.0),
                Expanded(
                  child: ElevatedButton(
                    onPressed: () async {
                      // var confirmPushed = await widget.onManualPush();
                      bool confirmPushed = await showDecision(
                        context, 
                        'Are you sure you want to publish the update to client now?', 
                        'Nope', 
                        'Publish Now',
                        buttonColor: yellow,
                        textColor: black,
                      );
                      if (confirmPushed) {
                        setState(() {
                          onlyClickOnce = onlyClickOnce + 1;
                        });
                
                        if (widget.subtaskInfo?.forInternal?.updatesImages?.length != 0) {
                          updatesImages?.forEach((image) {
                            if (image.selectedStatus == 1 && image.taskImageID != null)
                              uploadedAttachmentIDs.add(image.taskImageID!);
                          });
                        }

                        if (widget.subtaskInfo?.forInternal?.updatesFiles?.length != 0) {
                          updatesFiles?.forEach((file) {
                            if (file.selectedStatus == 1 && file.taskImageID != null)
                              uploadedAttachmentIDs.add(file.taskImageID!);
                          });
                        }

                        if (onlyClickOnce == 1) {
                          await _taskService.postTaskPushClientApp(
                            PushUpdateModel(
                              action: "push",
                              taskID: widget.isSubtask
                                  ? widget.subtaskInfo?.taskID
                                  : widget.taskInfo?.taskID,
                              taskImageID: uploadedAttachmentIDs,
                              remark: widget.isSubtask
                                  ? widget.subtaskInfo?.forInternal?.taskUpdates
                                  : widget.taskInfo?.taskUpdates,
                            ),
                          ).timeout(Duration(seconds: 60), onTimeout: () {
                            onlyClickOnce = 0;
                            // goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: widget.taskInfo?.taskID));
                            widget.callback!();
                          }).then((response) {
                            setState(() {
                              autoPushRemainingTime = 0;
                              isCancel = false;
                              isShownInCA = false;
                              onlyClickOnce = 0;
                            });
                          }).whenComplete(() {
                            onlyClickOnce = 0;
                            // goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: widget.taskInfo?.taskID));
                            widget.callback!();
                          }).onError((error, stackTrace) {
                            onlyClickOnce = 0;
                            // goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: widget.taskInfo?.taskID));
                            widget.callback!();
                          });
                        }
                      }
                    },
                    child: Text(
                      'Publish Now',
                      style: boldFontsStyle(fontSize: 16.0, height: 1.5),
                    ),
                    style: ElevatedButton.styleFrom(
                      backgroundColor: yellow,
                      padding: EdgeInsets.symmetric(vertical: 12.0),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0)),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
}
