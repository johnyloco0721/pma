import 'package:flukit/flukit.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:table_calendar/table_calendar.dart';

import '../constants/app_colors.dart';
import '../models/task_item_model.dart';
import '../models/task_milestone_model.dart';
import '../models/task_project_item_model.dart';

class CustomTableCalender extends StatefulWidget {
  final double calendarTopPadding;
  final List<DateTime> hasProjectTask;
  final ValueSetter dateFilter;
  final String dateSelected;

  CustomTableCalender({
    required this.calendarTopPadding,
    required this.hasProjectTask,
    required this.dateFilter,
    required this.dateSelected,
    Key? key,
  }) : super(key: key);

  @override
  TableCalenderState createState() => TableCalenderState();
}

class TableCalenderState extends State<CustomTableCalender> {
  late ValueSetter<String> dateFilter;
  bool fullCalendar = false;
  DateTime? _selectedDay;
  DateTime? _focusedDay;
  bool toggleSelectedStyle = false;
  DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss.SSS'Z'");
  String filterDateTime = "";
  List<TaskMilestoneDetail>? defaultMilestoneList;
  List<TaskProjectItemModel>? defaultProjectList;
  List<TaskItemModel>? defaultTaskList;
  PanelController panelController = PanelController();
  double panelMaxHeight = 360;

  @override
  void initState() {
    super.initState();
    _selectedDay = DateTime.tryParse(widget.dateSelected);
    if (_selectedDay != null)
      toggleSelectedStyle = true;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      key: PageStorageKey<String>('CustomTableCalender'),
      duration: Duration(milliseconds: 300),
      // Provide an optional curve to make the animation feel smoother.
      curve: Curves.easeIn,
      child: SlidingUpPanel(
        controller: panelController,
        color: whiteGrey,
        onPanelOpened: () {
          setState(() {
            fullCalendar = true;
          });
        },
        onPanelClosed: () {
          setState(() {
            fullCalendar = false;
          });
        },
        slideDirection: SlideDirection.DOWN,
        maxHeight: panelMaxHeight,
        panel: _fullCalendar(),
      ),
    );
  }

  Widget _fullCalendar() {
    return Container(
      padding: EdgeInsets.only(
          top: fullCalendar ? 0 : widget.calendarTopPadding, bottom: 0),
      decoration: BoxDecoration(
        color: darkGrey,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(12.0),
          bottomRight: Radius.circular(12.0),
        ),
      ),
      child: Column(
        children: [
          AfterLayout(
            callback: (RenderAfterLayout value) {
              setState(() {
                if (value.size.height == 308.0 || value.size.height == 62.0)
                  panelMaxHeight = 360.0;
                else if (value.size.height == 270.0) {
                  panelMaxHeight = 317.0;
                }
              });
            },
            child: TableCalendar(
              onPageChanged: (datetime) {
                setState(() {
                  _focusedDay = datetime;
                });
              },
              calendarBuilders: CalendarBuilders(
                markerBuilder: (context, date, _) {
                  return widget.hasProjectTask.contains(date)
                      ? Container(
                          transform: Matrix4.translationValues(0.0, -9.5, 0.0),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: red), //Change color
                          width: 5.0,
                          height: 5.0,
                          margin: const EdgeInsets.symmetric(horizontal: 1.5),
                        )
                      : Container();
                },
              ),
              headerVisible: fullCalendar ? true : false,
              calendarFormat:
                  fullCalendar ? CalendarFormat.month : CalendarFormat.week,
              startingDayOfWeek: StartingDayOfWeek.monday,
              headerStyle: HeaderStyle(
                headerPadding: EdgeInsets.only(top: 8.0, bottom: 0.0),
                titleCentered: true,
                formatButtonShowsNext: false,
                formatButtonVisible: false,
                titleTextFormatter: (date, locale) =>
                    DateFormat.yMMM(locale).format(date),
                titleTextStyle: boldFontsStyle(color: white, fontSize: 14.0),
                rightChevronIcon: Icon(Icons.chevron_right, color: white),
                leftChevronIcon: Icon(Icons.chevron_left, color: white),
              ),
              daysOfWeekStyle: DaysOfWeekStyle(
                dowTextFormatter: (date, locale) =>
                    DateFormat.E(locale).format(date)[0],
                weekdayStyle: boldFontsStyle(color: white, fontSize: 12.0),
                weekendStyle: boldFontsStyle(color: white, fontSize: 12.0),
              ),
              daysOfWeekHeight: 24.0,
              rowHeight: 38.0,
              firstDay: DateTime.utc(2000, 01, 01),
              lastDay: DateTime.utc(2100, 12, 31),
              focusedDay: _focusedDay != null
                  ? _focusedDay ?? DateTime.now()
                  : DateTime.now(),
              selectedDayPredicate: (day) => isSameDay(day, _selectedDay),
              onDaySelected: (selectedDay, focusedDay) {
                setState(() {
                  // collapsed when select the date
                  panelController.panelPosition = 0.0;
                  // condition for changing the style
                  if (_selectedDay != selectedDay) {
                    toggleSelectedStyle = true;
                    filterDateTime = selectedDay.toString();
                  } else {
                    toggleSelectedStyle = !toggleSelectedStyle;
                    if (toggleSelectedStyle == false)
                      filterDateTime = "";
                    else
                      filterDateTime = selectedDay.toString();
                  }
                  _focusedDay = focusedDay;
                  _selectedDay = selectedDay;
                  widget.dateFilter(filterDateTime);
                });
              },
              calendarStyle: CalendarStyle(
                  todayDecoration: BoxDecoration(
                    color: transparent,
                    shape: BoxShape.circle,
                  ),
                  // highlighted color for selected day
                  cellPadding: EdgeInsets.only(
                      bottom: 8.0, left: 8.0, right: 8.0, top: 4.0),
                  // highlighted color for selected day
                  selectedDecoration: BoxDecoration(
                    color: toggleSelectedStyle ? yellow : transparent,
                    shape: BoxShape.circle,
                  ),
                  markerDecoration:
                      const BoxDecoration(color: red, shape: BoxShape.circle),
                  markerSize: 5.0,
                  defaultTextStyle:
                      regularFontsStyle(color: white, fontSize: 12.0),
                  todayTextStyle: boldFontsStyle(color: yellow, fontSize: 12.0),
                  weekendTextStyle:
                      regularFontsStyle(color: white, fontSize: 12.0),
                  selectedTextStyle: toggleSelectedStyle
                      ? boldFontsStyle(color: darkGrey, fontSize: 12.0)
                      : DateFormat.yMMMd()
                                  .format(_selectedDay ?? DateTime.now()) ==
                              DateFormat.yMMMd().format(DateTime.now())
                          ? boldFontsStyle(color: yellow, fontSize: 12.0)
                          : regularFontsStyle(color: white, fontSize: 12.0),
                  outsideTextStyle:
                      regularFontsStyle(color: lightGrey, fontSize: 12.0),
                  outsideDaysVisible: false),
            ),
          ),
          // Clear Button
          fullCalendar
              ? Container(
                  child: Column(
                    children: [
                      Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                        GestureDetector(
                            onTap: () {
                              setState(() {
                                // collapsed when select the date
                                panelController.panelPosition = 0.0;
                                _focusedDay = DateTime.now();
                                _selectedDay = DateTime.now();
                                fullCalendar = false;
                                toggleSelectedStyle = false;
                                filterDateTime = "";
                                widget.dateFilter(filterDateTime);
                              });
                            },
                            child: Container(
                                width: 84.0,
                                height: 28.0,
                                margin: EdgeInsets.only(right: 24.5),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  border: Border.all(color: white),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(6.0)),
                                ),
                                child: Text("X Clear",
                                    style: regularFontsStyle(
                                        fontSize: 14.0, color: white)))),
                        SizedBox(height: 9.0)
                      ]),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(width: 41.0, height: 3.0, color: grey),
                        ],
                      ),
                      SizedBox(height: 16.0),
                    ],
                  ),
                )
              : Column(
                  children: [
                    SizedBox(
                        height:
                            widget.calendarTopPadding == 270.0 ? 8.0 : 16.0),
                    Container(width: 41.0, height: 3.0, color: grey),
                    SizedBox(height: 16.0),
                  ],
                )
        ],
      ),
    );
  }
}
