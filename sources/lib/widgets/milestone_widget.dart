import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:lottie/lottie.dart';
import 'package:sources/args/delay_args.dart';
import 'package:sources/args/task_args.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_constants.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/models/permissions_model.dart';
import 'package:sources/models/task_item_model.dart';
import 'package:sources/models/task_milestone_model.dart';
import 'package:sources/services/task_services.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/status_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/widgets/html_widget/html_stylist.dart';
import 'package:sources/widgets/task_label_widget.dart';
import 'package:sticky_headers/sticky_headers.dart';

class MilestoneWidget extends StatefulWidget {
  final ScrollController scrollController;
  final PagingController<int, StageDetail> milestonePagingController;
  final bool isWorkplace;
  final LivePreviewArguments? livePreviewArgs;
  final List<DisclaimerList>? disclamerList;
  final PermissionsModel? permissions;
  final Function(bool)? setLoadingState;
  final int? projectID;

  MilestoneWidget({
    required this.scrollController,
    required this.milestonePagingController,
    this.isWorkplace = false,
    this.livePreviewArgs,
    this.disclamerList,
    this.permissions,
    this.projectID,
    this.setLoadingState,
    Key? key,
  }) : super(key: key);

  @override
  _MilestoneWidgetState createState() => _MilestoneWidgetState();
}

class _MilestoneWidgetState extends State<MilestoneWidget> {
  TaskService _taskService = TaskService();

  int onlyClickOnce = 0;
  int displayDisclaimerID = 0;

  @override
  Widget build(BuildContext context) {
    for (var i = 0; i < (widget.milestonePagingController.itemList?.length ?? 0); i++) {
      if (widget.milestonePagingController.itemList?[i].taskList?.length != 0) {
        displayDisclaimerID = i;
        break;
      }
    }
    return PagedListView.separated(
      physics: AlwaysScrollableScrollPhysics(),
      scrollController: widget.scrollController,
      pagingController: widget.milestonePagingController,
      separatorBuilder: (context, index) => Container(
        color: shimmerGrey,
        height: (widget.milestonePagingController.itemList?[index].taskList?.length ?? 0) > 0 ? 16.0 : 0.0,
      ),
      builderDelegate: PagedChildBuilderDelegate<StageDetail>(
        firstPageProgressIndicatorBuilder: (context) => Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Lottie.asset(
              lottie_loading_spinner,
              width: 50.0,
              height: 50.0,
              fit: BoxFit.fill,
            ),
            SizedBox(height: 16.0),
            Text(
              'just a few secs...',
              style: regularFontsStyle(fontSize: 14.0, color: grey),
            ),
          ],
        ),
        newPageProgressIndicatorBuilder: (context) => Container(
          padding: EdgeInsets.all(8.0),
          child: Center(
            child: Lottie.asset(
              lottie_loading_spinner,
              width: 50.0,
              height: 50.0,
              fit: BoxFit.fill,
            ),
          ),
        ),
        noMoreItemsIndicatorBuilder: (context) => Container(
          padding: EdgeInsets.all(24.0),
          child: Center(
            child: Text(
              'You’ve reach the end of the list.',
              style: regularFontsStyle(fontSize: 12.0, height: 1.33, color: grey),
            ),
          ),
        ),
        noItemsFoundIndicatorBuilder: (context) => Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              child: Lottie.asset(
                lottie_empty_docs,
                width: 250.0,
                height: 250.0,
                fit: BoxFit.fill,
              ),
            ),
            Container(
              width: 250.0,
              child: Text(
                text_empty_task,
                style: regularFontsStyle(fontSize: 17.0, height: 1.35), 
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
        itemBuilder: (context, stage, index) {
          return (stage.taskList?.length ?? 0) > 0 ? Container(
            color: white,
            child: Column(
              children: [
                if (widget.disclamerList?.length != 0 && index == displayDisclaimerID) 
                Container(
                  color: white,
                  padding: EdgeInsets.symmetric(vertical:8),
                ),
                if (widget.disclamerList?.length != 0 && index == displayDisclaimerID) 
                for (int i = 0; i < (widget.disclamerList?.length as int); i++)
                Container(
                  padding: EdgeInsets.only(right: 15, left: 15),
                  color: white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      if (index == displayDisclaimerID) 
                      Container(
                        child: RichText(
                          text: HTML.toTextSpan(
                            context, 
                            widget.disclamerList?[i].htmlText ?? '',
                          )),
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 1,
                            color: Color(int.parse('0xFF' + (widget.disclamerList?[i].borderColor?.substring(1) ?? 'EE4056') ))
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          color: Color(int.parse('0xFF' + (widget.disclamerList?[i].backgroundColor?.substring(1) ?? 'FFF4F4') ))
                        ),
                        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                      ),
                      SizedBox(height: 15),
                    ]
                  ),
                ),
                if (widget.isWorkplace == false && widget.permissions?.canGenerateMakeoverTask == true && index == displayDisclaimerID) 
                SizedBox(height: 12.0,),
                if (widget.isWorkplace == true && widget.permissions?.canGenerateMakeoverTask == true && index == displayDisclaimerID && widget.disclamerList?.length == 0) 
                SizedBox(height: 12.0,),
                if (widget.permissions?.canGenerateMakeoverTask == true && index == displayDisclaimerID) 
                ElevatedButton (
                  style: ElevatedButton.styleFrom(
                    backgroundColor: yellow,
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                    maximumSize: Size(200.0, 50.0)
                  ),
                  onPressed: () async {
                    var confirmGenerate = await showDecision(
                      context, 
                      'Are you sure you want to generate tasks without dates or assignees?', 
                      'Cancel', 
                      'Yes',
                      buttonColor: yellow,
                      textColor: black,
                    );
                    if (confirmGenerate) {
                      setState(() {
                        onlyClickOnce = onlyClickOnce + 1;
                      });

                      if (onlyClickOnce == 1) {
                        widget.setLoadingState!(true);

                        try {
                          // Show the dialog using a Builder widget to get the right context
                          var overlay = Overlay.of(context);
                          OverlayEntry overlayEntry;

                          var response = await _taskService.manualGenerateTask(widget.projectID);

                          overlayEntry = OverlayEntry(
                            builder: (context) => Stack(
                              children: [
                                Positioned.fill(
                                  child: Container(
                                    color: Colors.black.withOpacity(0.5), // Dark background with opacity
                                  ),
                                ),
                                Center(
                                  child: Container(
                                    margin: EdgeInsets.only(top: 50.0),
                                    width: MediaQuery.of(context).size.width * 0.8,
                                    height: MediaQuery.of(context).size.height * 0.2,
                                    child: Material(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(12),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          SvgPicture.asset(ic_completed, width: 45),
                                          SizedBox(height: 12.0),
                                          Text(
                                            'Tasks Generated Successfully!',
                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Colors.black),
                                            textAlign: TextAlign.center,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                          if (response['status'] == true) overlay?.insert(overlayEntry);

                          await Future.delayed(Duration(seconds:2));

                          // Remove the overlay
                          overlayEntry.remove();
                          
                                                      
                        } catch (error) {
                          showError(context, "Something went wrong, please try again!");
                          widget.milestonePagingController.refresh();
                          widget.setLoadingState!(false);
                          // Handle errors
                        } finally {
                          // Set loading state to false regardless of success or failure
                          widget.milestonePagingController.refresh();
                          widget.setLoadingState!(false);
                        }
                        widget.milestonePagingController.refresh();
                        widget.setLoadingState!(false);
                      }
                    }
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(ic_add, width: 16.0),
                      SizedBox(width: 8.0),
                      Text (
                        "Makeover Tasks",
                        style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: black),
                      ),
                    ],
                  ),
                ),
                if (widget.permissions?.canGenerateMakeoverTask == true && index == 0) 
                Container(
                  color: white,
                  padding: EdgeInsets.symmetric(vertical:5),
                ),
                if ((widget.permissions?.canGenerateMakeoverTask == true || widget.disclamerList?.length != 0) && index == 0) 
                Container(
                  color: shimmerGrey,
                  padding: EdgeInsets.symmetric(vertical:5),
                ),
                StickyHeader(
                  header: stageLabel(stage),
                  content: ListView.separated(
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: stage.taskList?.length ?? 0,
                    primary: false,
                    shrinkWrap: true,
                    separatorBuilder: (context, index) => Container(
                      color: shimmerGrey,
                      height: 8.0,
                    ),
                    itemBuilder: (BuildContext context, int subGroupIndex) {
                      return StickyHeader(
                        header: taskItemLayout(stage.taskList![subGroupIndex]),
                        content: (stage.taskList![subGroupIndex].isExpanded!) ? taskListView(context, stage.taskList![subGroupIndex].tasks ?? []) : Container(),
                      );
                    }
                  ),
                ),
              ],
            ),
          ) : Container();
        },
      ),
    );
  }

  // stage label widget
  Widget stageLabel(StageDetail stage) => Row(
      children: [
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
            width: double.infinity,color:white,
            child: Row(
              children: [
                Container(
                  height: 10.0,
                  width: 10.0,
                  decoration: new BoxDecoration(
                    color: hexToColor(stage.iconColor ?? '#ffffff'),
                    shape: BoxShape.circle,
                  ),
                ),
                SizedBox(width:8.0), 
                Text(stage.label ?? '',style:boldFontsStyle(color:grey,fontSize: 14.0)),
              ],
            )
          )
        ),
      ],
    );

  // task item layout
  Widget taskItemLayout(TaskMilestoneDetail milestone) {
    Status _taskStatus = statusConfig(milestone.milestoneStatus ?? '');

    return GestureDetector(
      onTap: () {
        setState(() {
          milestone.isExpanded = !milestone.isExpanded!;
        });
        // avoid sticky header UI bug
        widget.scrollController.animateTo(widget.scrollController.offset - 0.1, duration: Duration(milliseconds: 100), curve: Curves.ease);
      },
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 24.0),
                  width: double.infinity,
                  color: whiteGrey,
                  child: Row(
                    children: [
                      // task content layout
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // task status & week label layout
                            Row(
                              children: [
                                // completed label, (outline layout) / status icon
                                Container(
                                  margin: EdgeInsets.only(right: milestone.milestoneStatus != 'incomplete' ? 8.0 : 0.0),
                                  child: milestone.milestoneStatus == 'completed' ? Container(
                                    padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                                    decoration: BoxDecoration(
                                      color: green,
                                      borderRadius: BorderRadius.circular(500)
                                    ),
                                    child: Text(
                                      'Completed',
                                      style: boldFontsStyle(fontSize: 14.0, color: white),
                                    )
                                  ) : milestone.milestoneStatus != 'incomplete' ? _taskStatus.icon : Container(),
                                ),
                                // week label
                                Text(
                                  milestone.milestoneWeek ?? '',
                                  style: boldFontsStyle(fontSize: 14.0, color: _taskStatus.textColor),
                                ),
                                if (milestone.milestoneWeek == null)
                                Text(
                                  milestone.milestoneWeek ?? 'TBC',
                                  style: italicFontsStyle(fontSize: 14.0, color: grey),
                                )
                              ],
                            ),
                            SizedBox(height: 4.0),
                            // Task title
                            Text(
                              milestone.milestoneCategoryName ?? '',
                              style: boldFontsStyle(fontSize: 20.0, height: 1.4),
                            ),
                          ],
                        ),
                      ),
                      // task count & dropdown layout
                      Row(
                        children: [
                          // task count
                          Text(
                            (milestone.tasks?.length ?? 0).toString(),
                            style: boldFontsStyle(fontSize: 14.0, color: milestone.isExpanded! ? darkYellow : black),
                          ),
                          // dropdown icon
                          AnimatedRotation(
                            turns: milestone.isExpanded! ? 1.0 : 0.5,
                            duration: Duration(milliseconds: 450),
                            child: SvgPicture.asset(ic_dropdown, color: milestone.isExpanded! ? darkYellow : black)
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  // task List View
  Widget taskListView(BuildContext context, List<TaskItemModel> tasks) => ListView.separated(
    itemCount: tasks.length,
    physics: NeverScrollableScrollPhysics(), 
    shrinkWrap: true,
    separatorBuilder: (context, index) => Divider(height: 1.0, color: shimmerGrey),
    itemBuilder: (context, index) {
      Status _taskStatus = statusConfig(tasks[index].taskStatus ?? '', iconWidth: 24.0);

      return GestureDetector(
        onTap: () {
          goToNextNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: tasks[index].taskID, milestonePagingController: widget.milestonePagingController), callback: () {
            widget.milestonePagingController.refresh();
          });
        },
        child: Container(
          padding: EdgeInsets.all(20.0),
          color: _taskStatus.secondaryColor,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              // task's leading icon
              _taskStatus.icon,
              SizedBox(width: 16.0),
              // task's content
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                      decoration: BoxDecoration(
                        color: tasks[index].taskStatus == 'completed' ? shimmerGrey : transparent,
                        border: Border.all(color: tasks[index].taskStatus == 'completed' ? shimmerGrey : (tasks[index].taskEndDateTime == null && tasks[index].taskStatus != 'completed' ) ? red : _taskStatus.primaryColor),
                        borderRadius: BorderRadius.circular(500)
                      ),
                      child: RichText(
                        text: TextSpan(
                          children: [
                            if (tasks[index].taskStatus == 'completed')
                              TextSpan(
                                text: "Completed: ",
                                style: regularFontsStyle(color: grey, fontSize: 12.0, height: 1.33)
                              ),
                              TextSpan(
                                text: tasks[index].taskStatus == 'completed' 
                                      ? formatDatetime(datetime: tasks[index].completedDateTime ?? '') 
                                      : widget.livePreviewArgs != null && widget.livePreviewArgs?.delay != 0
                                      ? formatDatetime(datetime: tasks[index].taskEndDateTimeArr?[widget.livePreviewArgs?.delayDateIndex ?? 0] ?? tasks[index].taskEndDateTime) 
                                      : formatDatetime(datetime: tasks[index].taskEndDateTime ?? ''),
                                style: boldFontsStyle(
                                  color: tasks[index].taskStatus == 'completed' ? grey : _taskStatus.textColor,
                                  fontSize: 12.0,
                                  height: 1.33
                                )
                              ),
                              if (tasks[index].taskEndDateTime == null && tasks[index].taskStatus != 'completed' )
                              TextSpan(
                                text: 'No Due Date',
                                style: italicFontsStyle(
                                  color: red,
                                  fontSize: 12.0,
                                  height: 1.33
                                )
                              ),
                          ]
                        )
                      ),
                    ),
                    SizedBox(height: 8.0),
                    RichText(
                      text: TextSpan(
                        children: [
                          if (tasks[index].isSpecial ?? false)
                            WidgetSpan(
                              alignment: PlaceholderAlignment.middle,
                              child: Padding(
                                padding: EdgeInsets.only(right: 8.0),
                                child: SvgPicture.asset(ic_star, width: 18.0),
                              ),
                            ),
                          TextSpan(
                            text: tasks[index].taskName ?? '',
                            style: boldFontsStyle(fontSize: 16.0, height: 1.5),
                          ),
                        ]
                      ),
                    ),
                    if (widget.isWorkplace && tasks[index].assignToName != null && tasks[index].assignToName != '')
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0),
                        child: Row(
                          children: [
                            Text(
                              'Assigned to:',
                              style: regularFontsStyle(fontSize: 14.0, color: grey),
                            ),
                            SizedBox(width: 4.0),
                            Text(
                              tasks[index].assignToName ?? '',
                              style: boldFontsStyle(fontSize: 14.0, color: grey),
                            ),
                          ],
                        ),
                      ),
                    if (widget.isWorkplace && (tasks[index].assignToName == null || tasks[index].assignToName == ''))
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0),
                        child: Row(
                          children: [
                            Text(
                              'Assigned to:',
                              style: regularFontsStyle(fontSize: 14.0, color: grey),
                            ),
                            SizedBox(width: 4.0),
                            Text(
                              'No Assignee',
                              style: italicFontsStyle(fontSize: 14.0, color: red),
                            ),
                          ],
                        ),
                      ),
                    SizedBox(height: 8.0),
                    TaskLabels(
                      isShowSubtaskLabel: (tasks[index].totalSubTask ?? 0) > 0,
                      totalSubTask: tasks[index].totalSubTask ?? 0,
                      completedSubTask: tasks[index].completedSubTask ?? 0,
                      isShowFileRequireLabel: (tasks[index].isFileRequiredLabel != '') && tasks[index].taskStatus != 'completed', 
                      isFileRequiredLabel: tasks[index].isFileRequiredLabel ?? '',
                      replyCount: tasks[index].replyCount ?? 0,
                      taskID: tasks[index].taskID!,
                      isRead: tasks[index].isRead ?? 'read',
                    ),
                  ],
                ),
              ),
              // task's trailing
              SvgPicture.asset(ic_task_right_arrow, width: 20.0, color: tasks[index].taskStatus == 'completed' ? black : _taskStatus.primaryColor),
            ],
          ),
        ),
      );
    },
  );
}