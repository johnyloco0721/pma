import 'package:flutter/material.dart';
import 'dart:ui';

class Backdrop extends StatelessWidget {
  final VoidCallback onTap;

  Backdrop({
    required this.onTap,
    Key? key,
  }) : super(key:  key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => this.onTap(),
      child: BackdropFilter(
        filter: new ImageFilter.blur(sigmaX: 10, sigmaY: 10),
        child: Container(
          decoration: BoxDecoration(
            color: Color(0xAA383633),
          ),
        ),
      ),
    );
  }
}