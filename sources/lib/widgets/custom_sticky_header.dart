import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:sticky_headers/sticky_headers.dart';

class MyStickyHeader extends StickyHeader {
  MyStickyHeader({
    Key? key,
    required this.header,
    required this.content,
    this.overlapHeaders = false,
    this.controller,
    this.callback,
  }) : super(
          key: key,
          header: header,
          content: content,
          overlapHeaders: overlapHeaders,
          controller: controller,
          callback: callback,
        );

  final Widget header;

  final Widget content;

  final bool overlapHeaders;

  final ScrollController? controller;

  final RenderStickyHeaderCallback? callback;

  @override
  _MyRenderStickyHeader createRenderObject(BuildContext context) {
    final scrollPosition =
        this.controller?.position ?? Scrollable.of(context).position;
    return _MyRenderStickyHeader(
      scrollPosition: scrollPosition,
      callback: this.callback,
      overlapHeaders: this.overlapHeaders,
    );
  }

  @override
  void updateRenderObject(
      BuildContext context, _MyRenderStickyHeader renderObject) {
    final scrollPosition =
        this.controller?.position ?? Scrollable.of(context).position;

    renderObject
      ..scrollPosition = scrollPosition
      ..callback = this.callback
      ..overlapHeaders = this.overlapHeaders;
  }
}

class _MyRenderStickyHeader extends RenderStickyHeader {
  bool _overlapHeaders;
  RenderStickyHeaderCallback? _callback;
  ScrollPosition _scrollPosition;

  _MyRenderStickyHeader({
    required ScrollPosition scrollPosition,
    RenderStickyHeaderCallback? callback,
    bool overlapHeaders = false,
    RenderBox? header,
    RenderBox? content,
  })  : _overlapHeaders = overlapHeaders,
        _callback = callback,
        _scrollPosition = scrollPosition,
        super(
          scrollPosition: scrollPosition,
          callback: callback,
          overlapHeaders: overlapHeaders,
          header: header,
          content: content,
        );

  RenderBox get _headerBox => lastChild!;

  RenderBox get _contentBox => firstChild!;

  @override
  void performLayout() {
    assert(childCount == 2);

    final childConstraints = constraints.loosen();
    _headerBox.layout(childConstraints, parentUsesSize: true);
    _contentBox.layout(childConstraints, parentUsesSize: true);

    final headerHeight = _headerBox.size.height;
    final contentHeight = _contentBox.size.height;

    final width = max(constraints.minWidth, _contentBox.size.width);
    final height = max(constraints.minHeight,
        _overlapHeaders ? contentHeight : headerHeight + contentHeight);
    size = Size(width, height);
    assert(size.width == constraints.constrainWidth(width));
    assert(size.height == constraints.constrainHeight(height));
    assert(size.isFinite);

    final contentParentData =
        _contentBox.parentData as MultiChildLayoutParentData;
    contentParentData.offset =
        Offset(0.0, _overlapHeaders ? 0.0 : headerHeight);

    final double stuckOffset = determineStuckOffsetWithHeight(headerHeight);

    final double maxOffset = height - headerHeight;
    final headerParentData =
        _headerBox.parentData as MultiChildLayoutParentData;

    headerParentData.offset =
        Offset(0.0, max(0.0, min(-stuckOffset, maxOffset)));

    if (_callback != null) {
      final stuckAmount =
          max(min(headerHeight, stuckOffset), -headerHeight) / headerHeight;
      _callback!(stuckAmount);
    }
  }

  double determineStuckOffsetWithHeight(double headerHeight) {
    final scrollBox =
        _scrollPosition.context.notificationContext!.findRenderObject();
    if (scrollBox?.attached ?? false) {
      try {
        return localToGlobal(Offset.zero, ancestor: scrollBox).dy -
            headerHeight;
      } catch (e) {
        // ignore and fall-through and return 0.0
      }
    }
    return 0.0;
  }
}
