import 'dart:io';

import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_svg/svg.dart';
import 'package:open_filex/open_filex.dart';
import 'package:sources/args/media_slider_args.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/models/feed_item_model.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/widgets/content_widget.dart';

import '../models/attach_model.dart';
import '../models/comment_details_model.dart';
import '../utils/util_helper.dart';

class CommentSystemUpdatesInfo extends StatefulWidget {
  final CommentDetailsResponse commentWithDetails;
  final FeedItemModel comments;
  CommentSystemUpdatesInfo(
      {Key? key, required this.commentWithDetails, required this.comments})
      : super(key: key);

  @override
  _CommentSystemUpdatesInfoState createState() =>
      _CommentSystemUpdatesInfoState();
}

class _CommentSystemUpdatesInfoState extends State<CommentSystemUpdatesInfo> {
  final database = locator<AppDatabase>();
  String completedDateTime = '';
  int commentCount = 0;
  int hasNewComments = 0;
  List<MediaInfoArguments> mediaArgsList = [];
  List<AttachModel> imageList = [];
  List<AttachModel> docList = [];

  @override
  void initState() {
    // implement initState
    super.initState();
    setAttachmentValue();
  }

  setAttachmentValue() async {
    completedDateTime = '';
    commentCount = 0;
    hasNewComments = 0;

    imageList = widget.comments.updatesInfo!.updatesImages ?? [];
    docList = widget.comments.updatesInfo!.updatesFiles ?? [];
    mediaArgsList = imageList
        .map((m) => MediaInfoArguments(
            mediaPath: m.imagePath ?? '',
            thumbnailPath: m.thumbnailPath ?? '',
            mediaDescription:
                widget.commentWithDetails.taskDetails?.taskDescription,
            mediaType: m.imageType ?? '',
            datetime: widget.commentWithDetails.taskDetails?.taskStartDateTime))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (widget.comments.updatesInfo!.updatesDescription != '' &&
                widget.comments.updatesInfo?.updatesDescription !=
                    "Task marked as completed")
              infoLayout(),
            // if (widget.comments.taskRelatedDate != "" && widget.comments.taskRelatedDate != null)
            //   actualDateLabel(),
            if (widget.comments.extraDatesDisplayArr != null)
              invoiceDateLabel(),
            // image listing layout
            imageList.length > 0 ? imageGridView(imageList) : Container(),
            // doc listing layout
            Row(
              children: [
                Expanded(
                  child: ExpandableNotifier(
                    child: Expandable(
                        collapsed: Wrap(
                          children: [
                            for (int i = 0; i < docList.length && i < 3; i++)
                              docBoxLayout(docList[i], i),
                            if (docList.length > 3)
                              ExpandableButton(
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 12.0),
                                  child: Text(
                                    '${docList.length - 3} More...',
                                    style: boldFontsStyle(
                                        fontSize: 14.0, color: yellow),
                                  ),
                                ),
                              ),
                          ],
                        ),
                        expanded: Wrap(
                          children: [
                            for (int i = 0; i < docList.length; i++)
                              docBoxLayout(docList[i], i),
                            ExpandableButton(
                              child: Container(
                                margin: EdgeInsets.only(bottom: 12.0),
                                child: Text(
                                  'Collapse..',
                                  style: boldFontsStyle(
                                      fontSize: 14.0, color: yellow),
                                ),
                              ),
                            ),
                          ],
                        )),
                  ),
                ),
              ],
            )
          ],
        ));
  }

  Widget infoLayout({int flex = 5}) {
    FeedItemModel comments = widget.comments;

    return Container(
      decoration: BoxDecoration(
        color: white,
        border: Border.all(
          color: hexToColor(comments.outlineColor!),
          width: 1.0,
          style: BorderStyle.solid,
        ),
        borderRadius: BorderRadius.circular(15),
      ),
      padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 12.0),
      margin: EdgeInsets.only(bottom: 8.0),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //remarks
          Text(
            comments.updatesInfo!.updatesLabel ?? '',
            style: boldFontsStyle(
                fontSize: 14.0,
                height: 1.8,
                color: comments.fontDescriptionColor != ""
                    ? hexToColor(comments.fontDescriptionColor!)
                    : grey),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 6.0),
            child: ContentWidget(
              content: comments.updatesInfo!.updatesDescription!,
              contentColor: comments.fontDescriptionColor != ""
                  ? hexToColor(comments.fontDescriptionColor!)
                  : grey,
              fontSize: 14.0,
            ),
          ),
        ],
      ),
    );
  }

  // display handover actual date
  Widget invoiceDateLabel() => Container(
        padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        margin: EdgeInsets.only(bottom: 8.0),
        width: double.infinity,
        decoration: BoxDecoration(
          border: Border.all(color: lightGrey),
          borderRadius: BorderRadius.all(Radius.circular(6.0))
        ),
        child: Wrap(
          children: [
            Text(
              (widget.comments.extraDatesDisplayArr?.label ?? 'Null') + ':',
              style: boldFontsStyle(fontSize: 14.0, color: grey),
            ),
            SizedBox(width: 8.0),
            Text(
              widget.comments.extraDatesDisplayArr?.date ?? 'Null',
              style: boldFontsStyle(fontSize: 14.0, color: black),
            ),
          ],
        ),
      );

// image custom grid view
  Widget imageGridView(List<AttachModel> imageList, {int flex = 6}) =>
      Container(
        height: 72.0,
        margin: EdgeInsets.only(bottom: docList.length == 0 ? 8.0 : 16.0),
        width: (imageList.length == 2)
            ? 144.0
            : (imageList.length == 3)
                ? 216.0
                : (imageList.length >= 4)
                    ? 288.0
                    : 72.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6.0),
        ),
        child: Row(
          children: [
            imageBoxLayout(imageList, 0),
            if (imageList.length > 1) SizedBox(width: 8.0),
            if (imageList.length > 1) imageBoxLayout(imageList, 1),
            if (imageList.length > 2) SizedBox(width: 8.0),
            if (imageList.length > 2) imageBoxLayout(imageList, 2),
            if (imageList.length > 3) SizedBox(width: 8.0),
            if (imageList.length > 3) imageBoxLayout(imageList, 3),
          ],
        ),
      );

  Widget imageBoxLayout(List<AttachModel> imageUrl, int index) => Expanded(
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: lightGrey),
            borderRadius: BorderRadius.circular(6.0),
          ),
          child: Stack(
            children: [
              imageUrl[index].imageType == 'mp4'
                  ? Stack(alignment: AlignmentDirectional.center, children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(6.0),
                        child: CachedNetworkImage(
                          imageUrl: imageUrl[index].thumbnailPath ?? '',
                          height: double.infinity,
                          width: double.infinity,
                          fit: BoxFit.cover,
                          maxHeightDiskCache: 120,
                          maxWidthDiskCache: 120,
                        ),
                      ),
                      Container(
                        constraints: BoxConstraints(maxHeight: 36.0),
                        child: SvgPicture.asset(
                          ic_play_button,
                        ),
                      ),
                    ])
                  : ClipRRect(
                      borderRadius: BorderRadius.circular(6.0),
                      child: CachedNetworkImage(
                        imageUrl: imageUrl[index].imagePath ?? '',
                        height: double.infinity,
                        width: double.infinity,
                        fit: BoxFit.cover,
                        maxHeightDiskCache: 100,
                        maxWidthDiskCache: 100,
                      ),
                    ),
              if (index > 2 && imageUrl.length > 4)
                Opacity(
                  opacity: 0.5,
                  child: Container(
                    color: black,
                  ),
                ),
              if (index > 2 && imageUrl.length > 4)
                Center(
                  child: Text(
                    '+${imageUrl.length - 3}',
                    style: boldFontsStyle(
                        fontFamily: platForm,
                        fontSize: 30.0,
                        height: 1.33,
                        color: white,
                        letterSpacing: 0.8),
                  ),
                ),
              Material(
                color: transparent,
                child: InkWell(
                  // splashColor: grey,
                  onTap: () {
                    // go to media viewer
                    goToNextNamedRoute(context, mediaSliderScreen,
                        args: MediaSliderArguments(mediaArgsList, index));
                  },
                ),
              ),
            ],
          ),
        ),
      );

  Widget docBoxLayout(AttachModel file, int index) => Column(
        children: [
          Material(
            color: transparent,
            child: InkWell(
              borderRadius: BorderRadius.circular(500.0),
              onTap: () async {
                // go to pdf viewer
                String? temporaryPath;
                String? savedPath =
                    SpManager.getString(file.imagePath.toString());
                if (savedPath != null && await File(savedPath).exists()) {
                  temporaryPath = savedPath;
                } else {
                  showToastMessage(context,
                      message: 'Downloading', duration: 1);
                  await Api()
                      .downloadFile(file.imagePath ?? '', 'pdf', context)
                      .then((localSavedPath) {
                    temporaryPath = localSavedPath;
                  });
                  await SpManager.putString(
                      file.imagePath.toString(), temporaryPath ?? '');
                  await Future.delayed(Duration(milliseconds: 1500));
                }
                await OpenFilex.open(temporaryPath);
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                decoration: BoxDecoration(
                    color: transparent,
                    border: Border.all(width: 1.0, color: black),
                    borderRadius: BorderRadius.circular(500.0)),
                child: Row(
                  children: [
                    SvgPicture.asset(ic_doc, width: 18.0, color: yellow),
                    SizedBox(width: 8.0),
                    Expanded(
                      child: Text(
                        file.imageName ??
                            file.imagePath!.substring(
                                file.imagePath!.lastIndexOf('/') + 1),
                        style: regularFontsStyle(fontSize: 14.0, color: black),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: index == docList.length - 1 ? 12.0 : 6.0),
        ],
      );
}
