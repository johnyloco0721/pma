import 'package:bubble/bubble.dart';
import 'package:drift/drift.dart' as DRIFT;
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/database/feeds.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/widgets/content_widget.dart';
import 'package:sources/widgets/topic_pill.dart';
import 'package:sources/widgets/updates_info_widget.dart';

import '../constants/app_images.dart';
import '../constants/app_routes.dart';
import '../services/service_locator.dart';
import '../view_models/workplace_view_model.dart';

class SystemBubble extends StatelessWidget {
  final FeedWithDetail feedWithDetail;
  final int index;
  final void Function(dynamic)? onLongPressCallback;
  final bool showToolbox;
  final bool showTopic;

  SystemBubble({
    required this.feedWithDetail,
    required this.index,
    this.onLongPressCallback,
    required this.showToolbox,
    this.showTopic = true,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final database = locator<AppDatabase>();
    final topicDetail = this.feedWithDetail.topicDetail;
    final feed = this.feedWithDetail.feedDetail;
    final onLongPressCallback = this.onLongPressCallback;
    final replyCount = feedWithDetail.feedDetail.replyCount;
    final avatarList = feedWithDetail.feedDetail.replyTeamMemberAvatar!.split("|");

    String postTime = formatDatetime(datetime: feed.postDateTime ?? '', format: "dd MMM yyyy • hh:mma");

    return GestureDetector(
      onLongPress: () {
        RenderObject object = context.findRenderObject()!;
        var translation = object.getTransformTo(null).getTranslation();
        if (onLongPressCallback != null) onLongPressCallback(translation);
      },
      child: Bubble(
        stick: false,
        margin: BubbleEdges.only(top: 8, bottom: 20, right: 24, left: 24),
        padding: BubbleEdges.all(16),
        radius: Radius.circular(12),
        borderColor: hexToColor(feed.outlineColor.toString()),
        borderWidth: 0.5,
        color: hexToColor(feed.backgroundColor.toString()),
        nip: BubbleNip.no,
        nipHeight: 12,
        alignment: Alignment.centerLeft,
        elevation: 0,
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if(feed.moduleName == 'task-flag')
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                    decoration: BoxDecoration(
                        color: lightPink,
                        borderRadius: BorderRadius.circular(500.0)),
                    child: Text(
                      'Flagged',
                      style: boldFontsStyle(
                        fontSize: 14.0,
                        color: red
                      ),
                    ),
                  ),
                  SvgPicture.asset(ic_task_flag, width: 26.0),
                ],
              ),
              if(feed.moduleName == 'task-flag')
              SizedBox(height:8.0),
              if (feed.description != null && feed.description != '') Padding(padding: EdgeInsets.only(bottom: 16.0), child: ContentWidget(content: feed.description ?? '')),
              // update info layout
              if (feed.moduleName == 'task-details')
                Container(
                  margin: EdgeInsets.only(bottom: 16.0),
                  child: UpdatesInfo(
                    feedWithDetail: feedWithDetail,
                  )),
              if(feed.moduleName == 'task-flag')
              Container(
                  margin: EdgeInsets.only(bottom: 16.0),
                  child: UpdatesInfo(
                    feedWithDetail: feedWithDetail,
                  )),
              // Message category topic & time row
              Container(
                height: 26,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    // Render feed category topic
                    if (this.showTopic && topicDetail != null) _buildTopicPill(context, topicDetail),
                    // Render feed post time
                    _buildTime(postTime, grey),
                  ],
                ),
              ),
              if (feedWithDetail.feedDetail.replyCount > 0)
                Column(
                  children: [
                    Divider(thickness: 1, height: 32, color: lightGrey),
                    GestureDetector(
                      onTap: () {
                        int taskID = int.parse(feedWithDetail.feedDetail.moduleID!);
                        goToNextNamedRoute(context, taskCommentScreen, args: taskID, callback: () {
                          database.feedDao.updateFeed(feed.copyWith(replyReadStatus: DRIFT.Value('read')));
                        });
                        hideToolbox();
                      },
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              if (feedWithDetail.feedDetail.replyCount != 0)
                                for (int i = 0; i < avatarList.length; i++)
                                  // Team members avatars
                                  Container(
                                    transform: Matrix4.translationValues(i != 0 ? -6.0 : 0.0, 0.0, 0.0),
                                    child: CircleAvatar(
                                      backgroundImage: NetworkImage(avatarList[i]),
                                      backgroundColor: white,
                                      radius: 16,
                                    ),
                                  ),

                              SizedBox(width: 6.0),
                              Text(replyCount > 1 ? '$replyCount Replies' : '$replyCount Reply',style: boldFontsStyle(color: grey, fontSize: 14.0)),
                              if (feedWithDetail.feedDetail.replyReadStatus == "unread")
                                Container(
                                  width: 10.0,
                                  height: 10.0,
                                  margin: EdgeInsets.symmetric(horizontal: 4.0),
                                  decoration: BoxDecoration(
                                    color: red,
                                    borderRadius: BorderRadius.circular(500.0),
                                  ),
                                ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
            ],
          ),
        ),
      ),
    );
  }

  // Feed topic pill
  Widget _buildTopicPill(BuildContext context, Topic topicDetail) {
    return TopicPill(
      text: topicDetail.topicName!,
      color: hexToColor(topicDetail.topicColour!),
      borderColor: hexToColor(topicDetail.topicColour!),
      backgroundColor: white,
      margin: EdgeInsets.zero,
    );
  }

  // Feed post time
  Widget _buildTime(String time, Color color) {
    return Text(
      time,
      style: regularFontsStyle(color: color, fontSize: 12.0, height: 1.33),
    );
  }

  final WorkplaceViewModel workplaceViewModel = locator<WorkplaceViewModel>();
  void hideToolbox() {
    workplaceViewModel.toolboxMode.value = 'none';
    workplaceViewModel.feedDescriptionNode.unfocus();
    workplaceViewModel.selectedTopic.value = workplaceViewModel.defaultTopic;
    workplaceViewModel.mediaImageFiles.value = [];
    workplaceViewModel.mediaPdfFiles.value = [];
  }
}
