import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/widgets/html_widget/html_stylist.dart';

import '../models/comment_details_model.dart';

class TaskLabels extends StatefulWidget {
  final bool isShowSubtaskLabel;
  final int completedSubTask;
  final int totalSubTask;
  final bool isShowFileRequireLabel;
  final String isFileRequiredLabel;
  final int replyCount;
  final int taskID;
  final String isRead;

  const TaskLabels({
    required this.isShowSubtaskLabel,
    required this.isFileRequiredLabel,
    this.completedSubTask = 0,
    this.totalSubTask = 0,
    required this.isShowFileRequireLabel,
    required this.replyCount,
    required this.isRead,
    required this.taskID,
    Key? key,
  }) : super(key: key);

  @override
  _TaskLabelsState createState() => _TaskLabelsState();
}

class _TaskLabelsState extends State<TaskLabels> {
  CommentDetailsModel? commentDetailsResponse;
  CommentDetailsResponse taskInfoResponse = CommentDetailsResponse(taskDetails: null, commentList: []);
  late bool isLoading;
  dynamic response = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(spacing: 8.0, runSpacing: 4.0, children: [
      // subtask label
      if (widget.isShowSubtaskLabel)
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            SvgPicture.asset(ic_career, width: 18.0, color: grey),
            SizedBox(width: 8.0),
            Text(
              "${widget.completedSubTask}/${widget.totalSubTask} Sub Tasks",
              style: regularFontsStyle(fontSize: 14.0, color: grey),
            ),
          ],
        ),
      // file require label
      if (widget.isShowFileRequireLabel)
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            SvgPicture.asset(ic_attach, width: 18.0, color: grey),
            SizedBox(width: 8.0),
            RichText(
              text: HTML.toTextSpan(
                context,
                widget.isFileRequiredLabel,
                defaultTextStyle: regularFontsStyle(fontSize: 14.0, color: grey),
              ),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      // new comments label
      if (widget.replyCount > 0)
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            widget.isRead == 'read' ? SvgPicture.asset(ic_comment, width: 18.0) : SvgPicture.asset(ic_comment_has_new, width: 18.0),
            SizedBox(width: 8.0),
            Text(
              widget.replyCount > 1 ? '${widget.replyCount}  Replies' : '${widget.replyCount} Reply',
              style: regularFontsStyle(fontSize: 14.0, color: grey),
            ),
          ],
        ),
    ]);
  }
}
