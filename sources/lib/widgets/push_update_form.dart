import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:getwidget/getwidget.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart' as p;
import 'package:lottie/lottie.dart';
import 'package:open_filex/open_filex.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import 'package:sources/args/media_slider_args.dart';
import 'package:sources/args/push_update_info_args.dart';
import 'package:sources/constants/app_box_shadow.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/models/attach_model.dart';
import 'package:sources/models/config_model.dart';
import 'package:sources/models/push_update_model.dart';
import 'package:sources/models/task_flagging_model.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/app_service.dart';
import 'package:sources/services/file_upload_service.dart';
import 'package:sources/services/task_services.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/error_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/utils/size_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/widgets/video_thumbnail_widget.dart';
import 'package:validators/validators.dart';
import 'package:video_compress/video_compress.dart';

import '../models/task_edit_model.dart';
import '../models/task_resolve_model.dart';
import '../services/comment_service.dart';

class PushUpdateForm extends StatefulWidget {
  final bool isFileRequire;
  final String title;
  final String remarkClientLabel;
  final String remarkInternalLabel;
  final String remarkInternalHints;
  final String remarkClientHints;
  final PushUpdateInfoArguments? pushUpdateInfo;
  final ValueSetter onPush;

  PushUpdateForm({
    this.isFileRequire = false,
    this.title = 'Push Update',
    this.remarkInternalLabel = 'Internal Notes',
    this.remarkClientLabel = 'Update shown in Client App',
    this.remarkInternalHints = 'Add Internal Notes',
    this.remarkClientHints = 'Add Client Notes',
    this.pushUpdateInfo,
    required this.onPush,
    Key? key,
  }) : super(key: key);

  @override
  _PushUpdateFormState createState() => _PushUpdateFormState();
}

class _PushUpdateFormState extends State<PushUpdateForm> {
  TaskService _taskService = TaskService();
  FileUploadService _fileUploadService = FileUploadService();
  final _formKey = GlobalKey<FormState>();
  final _picker = ImagePicker();
  TextEditingController clientRemarkInputController = TextEditingController();
  TextEditingController internalRemarkInputController = TextEditingController();
  TextEditingController dropboxInputController = TextEditingController();
  TextEditingController internalInvoiceDateController = TextEditingController();
  TextEditingController clientInvoiceDateController = TextEditingController();
  String? actualHandoverDate = "";
  String? extraInvoiceDate = "";
  ValueNotifier<List<AttachModel>> internalDocAttachments = ValueNotifier([]);
  ValueNotifier<List<AttachModel>> internalMediaAttachments = ValueNotifier([]);
  ValueNotifier<List<AttachModel>> clientDocAttachments = ValueNotifier([]);
  ValueNotifier<List<AttachModel>> clientMediaAttachments = ValueNotifier([]);
  List<MediaInfoArguments> mediaArgsList = [];
  bool fileRequireValidation = true;
  bool datetimeValidation = true;
  bool linkRequireValidation = true;
  bool mediaUploading = false;
  bool uploading = false;
  bool completed = false;
  bool isEditComments = false;
  int onlyClickOnce = 0;
  int isInternalView = 0;

  CommentService _commentService = CommentService();
  ConfigDetails? config;

  @override
  void initState() {
    // implement initState
    super.initState();
    _initializeTextControllers();
    _initializeInvoiceDate();

    internalDocAttachments.value = widget.pushUpdateInfo?.forInternal?.updatesFiles ?? [];
    internalMediaAttachments.value = widget.pushUpdateInfo?.forInternal?.updatesImages ?? [];

    clientDocAttachments.value = widget.pushUpdateInfo?.forClient?.updatesFiles ?? [];
    clientMediaAttachments.value = widget.pushUpdateInfo?.forClient?.updatesImages ?? [];

    if (widget.pushUpdateInfo?.specialTaskType == "edited-photo") {
      dropboxInputController.text = widget.pushUpdateInfo?.forInternal?.dropboxLink ?? widget.pushUpdateInfo?.linkValue ?? '';
    }
    config = SpManager.getConfigData();
  }


  void _initializeTextControllers() {
    if (isInternalView == 0 &&
        widget.pushUpdateInfo?.updateScreen != 'Flagged' &&
        widget.pushUpdateInfo?.updateScreen != 'Resolve Task') {
      internalRemarkInputController.text = Bidi.stripHtmlIfNeeded(
          widget.pushUpdateInfo?.forInternal?.taskUpdates ?? '').trim();
    }

    if (isInternalView == 1 &&
        widget.pushUpdateInfo?.updateScreen != 'Flagged') {
      clientRemarkInputController.text = Bidi.stripHtmlIfNeeded(
          widget.pushUpdateInfo?.forClient?.taskUpdates ?? '').trim();
    }
  }

  void _initializeInvoiceDate() {
    if (isInternalView == 0 &&
        widget.pushUpdateInfo?.forInternal?.extraDatesDisplayArr != null && 
        widget.pushUpdateInfo?.updateScreen != 'Flagged') {
      setState(() {
        String? dateStr = widget.pushUpdateInfo?.forInternal?.extraDatesDisplayArr?.date;
        if (dateStr != null && dateStr.isNotEmpty) {
            extraInvoiceDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(
              DateFormat('dd MMM yyyy', 'en_US').parse(dateStr),
            );
            var dateTimeFormat = DateTime.parse(extraInvoiceDate.toString());
            internalInvoiceDateController.text = DateFormat('dd MMM yyyy').format(dateTimeFormat);
        } else {
          internalInvoiceDateController.text = '';
        }
      });
    } 
    if (widget.pushUpdateInfo?.forClient?.extraDatesDisplayArr != null) {
      setState(() {
        String? dateStr = widget.pushUpdateInfo?.forClient?.extraDatesDisplayArr?.date;
        if (dateStr != null && dateStr.isNotEmpty) {
            extraInvoiceDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(
              DateFormat('dd MMM yyyy', 'en_US').parse(dateStr),
            );
            var dateTimeFormat = DateTime.parse(extraInvoiceDate.toString());
            clientInvoiceDateController.text = DateFormat('dd MMM yyyy').format(dateTimeFormat);
        } else {
          clientInvoiceDateController.text = "";
        }
      });
    }
  }

  void _updateTextControllers() {
    if (isInternalView == 0 &&
        widget.pushUpdateInfo?.updateScreen != 'Flagged' &&
        widget.pushUpdateInfo?.updateScreen != 'Resolve Task') {
      setState(() {
        internalRemarkInputController.text = Bidi.stripHtmlIfNeeded(
            widget.pushUpdateInfo?.forInternal?.taskUpdates ?? '').trim();
      });
    } else if (isInternalView == 1 &&
        widget.pushUpdateInfo?.updateScreen != 'Flagged') {
      setState(() {
        clientRemarkInputController.text = Bidi.stripHtmlIfNeeded(
            widget.pushUpdateInfo?.forClient?.taskUpdates ?? '').trim();
      });
    }
  }

  String? _validateLink(String? link) {
  if (link == null || link.isEmpty) {
    return '*This is a required field';
  } else if (!isURL(link)) {
    return '*Invalid Dropbox link';
  }
  return null;
}

  @override
  Widget build(BuildContext context) {
    return uploading ? uploadProgressLayout() : formLayout();
  }

  Widget formLayout() => Container(
    color: white,
    padding: EdgeInsets.only(top: 24.0, right: 4.0, bottom: 24.0, left: 4.0),
    height: MediaQuery.of(context).size.height * 0.78,
    child: Column(
      children: [
        Center(
          child: Text(
            widget.title,
            style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, height: 1.25, letterSpacing: 0.8),
          ),
        ),
        SizedBox(height: 12.0),
        Expanded(
          child: SingleChildScrollView(
            child: isInternalView == 0 ? internalForm() : clientForm(),
          ),
        ),
          navigationButtons(),
        ],
    ),
  );

  Widget internalForm() => Form(
    key: _formKey,
    child: Container(
      height: MediaQuery.of(context).size.height * 0.6,
      child: RawScrollbar(
        thumbVisibility: true,
        thumbColor: darkYellow,
        trackVisibility: true,
        trackColor: darkGrey,
        minOverscrollLength: 50,
        radius: Radius.circular(25),
        thickness: 4,
        minThumbLength: 80,
        child: SingleChildScrollView(
          padding: EdgeInsets.all(20.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (widget.pushUpdateInfo?.updateScreen != 'Flagged' && widget.pushUpdateInfo?.updateScreen != 'Resolve Task') ...[
                Row(
                  children: [
                    SvgPicture.asset(ic_team, color: black),
                    SizedBox(width: 8),
                    Text(
                      'Internal View:',
                      style: boldFontsStyle(fontSize: 18.0, color: black, fontWeight: FontWeight.w700, height: 1.5),
                    ),
                  ],
                ),
                SizedBox(height: 24.0),
              ],
              if (widget.pushUpdateInfo?.updateScreen == 'Flagged')
                Padding(
                  padding: const EdgeInsets.only(bottom: 16.0),
                  child: Text(
                    "By flagging this update, the task will be sending back to the assigned person to redo the update.",
                    style: regularFontsStyle(fontSize: 14.0, color: black, height: 1.29),
                  ),
                ),
              Text(
                widget.pushUpdateInfo?.updateScreen == 'Flagged'
                    ? 'Reason of Flagging*'
                    : widget.pushUpdateInfo?.updateScreen == 'Resolve Task'
                        ? 'Remarks'
                        : widget.remarkInternalLabel,
                style: boldFontsStyle(fontSize: 14.0, color: grey, height: 1.29),
              ),
              SizedBox(height: 8.0),
              TextFormField(
                controller: internalRemarkInputController,
                cursorColor: black,
                decoration: InputDecoration(
                  hintText: widget.pushUpdateInfo?.updateScreen == 'Flagged'
                    ? 'Fill in reason'
                    : widget.pushUpdateInfo?.updateScreen == 'Resolve Task'
                        ? 'Add Remarks'
                        : widget.remarkInternalHints,                  
                  hintStyle: italicFontsStyle(fontSize: 16.0, color: grey, height: 1.5),
                  contentPadding: EdgeInsets.symmetric(vertical: 6, horizontal: 16),
                  focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(6.0), borderSide: BorderSide(color: lightGrey)),
                  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(6.0), borderSide: BorderSide(color: lightGrey)),
                ),
                // minLines: widget.pushUpdateInfo?.updateScreen == 'Flagged' ? 5 : 1,
                maxLines: 3,
              ),
              SizedBox(height: 16.0),
              if (widget.pushUpdateInfo?.updateScreen != 'Flagged')
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: (widget.pushUpdateInfo?.specialTaskType == "edited-photo") ? [
                    Text(
                      'Link*',
                      style: boldFontsStyle(fontSize: 14.0, color: grey),
                    ),
                    SizedBox(height: 8.0),
                    TextFormField(
                      controller: dropboxInputController,
                      cursorColor: black,
                      validator: (value) {
                        return _validateLink(value);
                      },
                      decoration: InputDecoration(
                        hintText: 'Add Link',
                        hintStyle: italicFontsStyle(fontSize: 16.0, height: 1.5, color: grey),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: darkYellow),
                          borderRadius: BorderRadius.circular(6.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: lightGrey),
                          borderRadius: BorderRadius.circular(6.0),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: red),
                          borderRadius: BorderRadius.circular(6.0),
                        ),
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                        fillColor: white,
                        filled: true,
                      ),
                      style: regularFontsStyle(fontSize: 16.0, height: 1.5),
                      maxLines: 3,
                    ),
                  ] : [
                    if (widget.pushUpdateInfo?.forInternal?.extraDatesDisplayArr != null)
                      ExtraDate(),
                    if (widget.pushUpdateInfo?.specialTaskType != "handover-actual-date")
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: 'Photos / Files' + (widget.isFileRequire ? '*' : ''),
                                  style: boldFontsStyle(fontSize: 14.0, color: grey, height: 1.29),
                                ),
                                TextSpan(
                                  text: ' (max. 20mb)' + (widget.isFileRequire ? '' : ' (optional)'),
                                  style: italicFontsStyle(fontSize: 14.0, color: grey, height: 1.29),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 4.0),
                          imageListingLayout(internalMediaAttachments),
                          docListingLayout(internalDocAttachments),
                          if (!fileRequireValidation)
                            Container(
                              margin: EdgeInsets.only(top: 8.0),
                              child: Text(
                                '*Please attach photos / files.',
                                style: regularFontsStyle(fontSize: 12.0, height: 1.33, color: red),
                              ),
                            ),
                        ],
                      ),
                  ],
                ),
            ],
          ),
        ),
      ),
    ),
  );

  Widget clientForm() => Form(
    key: _formKey,
    child: Container(
      height: MediaQuery.of(context).size.height * 0.6,
      child: RawScrollbar(
        thumbVisibility: true,
        thumbColor: darkYellow,
        trackVisibility: true,
        trackColor: darkGrey,
        minOverscrollLength: 50,
        radius: Radius.circular(25),
        thickness: 4,
        minThumbLength: 80,
        child: SingleChildScrollView(
          padding: EdgeInsets.all(20.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  SvgPicture.asset(ic_moggy_head, color: black),
                  SizedBox(width: 8),
                  Text(
                    'Client View:',
                    style: boldFontsStyle(fontSize: 18.0, color: black, fontWeight: FontWeight.w700, height: 1.5),
                  ),
                ],
              ),
              SizedBox(height: 24.0),
              if (widget.pushUpdateInfo?.updateScreen == 'Flagged')
                Padding(
                  padding: const EdgeInsets.only(bottom: 16.0),
                  child: Text(
                    "By flagging this update, the task will be sending back to the assigned person to redo the update.",
                    style: regularFontsStyle(fontSize: 14.0, color: black, height: 1.29),
                  ),
                ),
              Text(
                widget.remarkClientLabel,
                style: boldFontsStyle(fontSize: 14.0, color: grey, height: 1.29),
              ),
              SizedBox(height: 8.0),
              TextFormField(
                controller: clientRemarkInputController,
                cursorColor: black,
                decoration: InputDecoration(
                  hintText: widget.pushUpdateInfo?.updateScreen == 'Flagged' ? 'Fill in reason' : widget.remarkClientHints,
                  hintStyle: italicFontsStyle(fontSize: 16.0, color: grey, height: 1.5),
                  contentPadding: EdgeInsets.symmetric(vertical: 6, horizontal: 16),
                  focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(6.0), borderSide: BorderSide(color: lightGrey)),
                  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(6.0), borderSide: BorderSide(color: lightGrey)),
                ),
                // minLines: widget.pushUpdateInfo?.updateScreen == 'Flagged' ? 5 : 1,
                maxLines: 3,
              ),
              SizedBox(height: 16.0),
              if (widget.pushUpdateInfo?.updateScreen != 'Flagged')
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: 
                  (widget.pushUpdateInfo?.specialTaskType == "edited-photo") 
                  ? [ 
                    Container()
                  ] : [
                    if (widget.pushUpdateInfo?.specialTaskType != "handover-actual-date" && widget.pushUpdateInfo?.forClient?.extraDatesDisplayArr != null)
                      ExtraDate(),
                    if (widget.pushUpdateInfo?.specialTaskType != "handover-actual-date" && (clientMediaAttachments.value.length > 0 || clientDocAttachments.value.length > 0))
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: 'Photos / Files',
                                  style: boldFontsStyle(fontSize: 14.0, color: grey, height: 1.29),
                                ),
                                TextSpan(
                                  text: ' shown in Client App',
                                  style: boldFontsStyle(fontSize: 14.0, color: grey, height: 1.29),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 4.0),
                          imageListingLayout(clientMediaAttachments),
                          docListingLayout(clientDocAttachments),
                          if (!fileRequireValidation)
                            Container(
                              margin: EdgeInsets.only(top: 8.0),
                              child: Text(
                                '*Please attach photos / files.',
                                style: regularFontsStyle(fontSize: 12.0, height: 1.33, color: red),
                              ),
                            ),
                        ],
                      ),
                  ],
                ),
            ],
          ),
        ),
      ),
    ),
  );

  Widget navigationButtons() => Container(
    padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 24.0),
    child: Row(
      children: [
        Expanded(
          child: OutlinedButton(
            style: OutlinedButton.styleFrom(
              padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
              side: BorderSide(color: black),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
            ),
            onPressed: () {
              if (isInternalView == 0) {
                Navigator.pop(context);
              } else {
                setState(() {
                  isInternalView = 0;
                });
              }
            },
            child: Text(
              isInternalView == 0 ? "Cancel" : "Back",
              style: boldFontsStyle(fontSize: 16.0, height: 1.5),
            ),
          ),
        ),
        SizedBox(width: 16.0),
        Expanded(
          child: Opacity(
            opacity: (mediaUploading || onlyClickOnce >= 1) ? 0.5 : 1.0,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: yellow,
                padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
              ),
              onPressed: () {
                if (!mediaUploading) {
                  if (isInternalView == 0 && widget.pushUpdateInfo?.updateScreen != 'Flagged' && widget.pushUpdateInfo?.updateScreen != 'Resolve Task') {
                    setState(() {
                      if(widget.isFileRequire && widget.pushUpdateInfo?.specialTaskType == "handover-actual-date" || widget.pushUpdateInfo?.specialTaskType == "edited-photo"){
                        fileRequireValidation = true;
                      }else{
                        fileRequireValidation = !widget.isFileRequire;
                      }
                      
                      if (!fileRequireValidation && 
                            (internalMediaAttachments.value.isNotEmpty ||
                              internalDocAttachments.value.isNotEmpty)) 
                      fileRequireValidation = true;  

                      if (!_formKey.currentState!.validate()) {
                        setState(() {
                          linkRequireValidation = false;
                        });
                      }
                      
                      if(fileRequireValidation && linkRequireValidation)
                         isInternalView = 1;
                        _updateTextControllers();
                    });
                  } else {
                    widget.pushUpdateInfo?.updateScreen == 'Edit Update'
                        ? _showConfirm()
                        : widget.pushUpdateInfo?.updateScreen == 'Flagged'
                            ? flaggedTask()
                            : widget.pushUpdateInfo?.updateScreen == 'Resolve Task'
                                ? resolveTask()
                                : markAsComplete();
                  }
                }
              },
              child: Text(
                isInternalView == 0 && widget.pushUpdateInfo?.updateScreen != 'Flagged' && widget.pushUpdateInfo?.updateScreen != 'Resolve Task' ? 'Next' : 'Save',
                style: boldFontsStyle(fontSize: 16.0, height: 1.5),
              ),
            ),
          ),
        ),
      ],
    ),
  );

  ExtraDate() {
    bool showDateTimePicker = false;
    bool showLabel = true;
    if (widget.pushUpdateInfo?.specialTaskType == 'handover-actual-date') {
      if (isInternalView == 0) {
        showDateTimePicker = true;
      }else{
        showLabel = false;
      }
    } else {
      showDateTimePicker = true;
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if(showLabel)
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: 
          (isInternalView == 0)
              ? Text(
                  '${widget.pushUpdateInfo?.forInternal?.extraDatesDisplayArr?.label}*',
                  style: boldFontsStyle(fontSize: 14.0, color: grey),
                )
              : widget.pushUpdateInfo?.forClient?.extraDatesDisplayArr?.date != null &&
                      widget.pushUpdateInfo?.forClient?.extraDatesDisplayArr?.date != ''
                  ? Text(
                      '${widget.pushUpdateInfo?.forClient?.extraDatesDisplayArr?.label}*',
                      style: boldFontsStyle(fontSize: 14.0, color: grey),
                    )
                  : Container(),
        ),
        if (showDateTimePicker && isInternalView == 0) ...[
          TextFormField(
            controller: internalInvoiceDateController,
            onTap: () async {
              DateTime dateTime = DateFormat('d MMM yyyy').parse(widget.pushUpdateInfo?.forInternal?.extraDatesDisplayArr?.date ?? '');
              var dateTimeValue = await showDatePicker(
                context: context,
                initialDate: dateTime,
                firstDate: DateTime.now().subtract(Duration(days: 365)),
                lastDate: DateTime.now().add(Duration(days: 3650)),
                builder: (context, child) {
                  return Theme(
                    data: ThemeData.light().copyWith(
                      datePickerTheme: DatePickerThemeData(
                        backgroundColor: white,
                        surfaceTintColor: Colors.transparent,
                        headerForegroundColor: Colors.black54,
                        dayBackgroundColor: WidgetStateProperty.resolveWith((states) {
                          if (states.contains(WidgetState.selected)) {
                            return darkYellow;
                          }
                          return white;
                        }),
                        todayBackgroundColor: WidgetStateProperty.resolveWith((states) {
                          if (states.contains(WidgetState.selected)) {
                            return darkYellow;
                          }
                          return white;
                        }),
                        todayForegroundColor: WidgetStateProperty.resolveWith((states) {
                          if (states.contains(WidgetState.selected)) {
                            return white;
                          }
                          return darkYellow;
                        }),
                        yearOverlayColor: WidgetStateProperty.resolveWith((states) {
                          if (states.contains(WidgetState.selected)) {
                            return darkYellow;
                          }
                          return white;
                        }),
                        yearForegroundColor: WidgetStateProperty.resolveWith((states) {
                          if (states.contains(WidgetState.selected)) {
                            return white;
                          }
                          return black;
                        }),
                        yearBackgroundColor: WidgetStateProperty.resolveWith((states) {
                          if (states.contains(WidgetState.selected)) {
                            return darkYellow;
                          }
                          return white;
                        }),
                        confirmButtonStyle: ButtonStyle(
                          backgroundColor: WidgetStateProperty.resolveWith((states) {
                            return white;
                          }),
                          foregroundColor: WidgetStateProperty.resolveWith((states) {
                            return darkYellow;
                          }),
                        ),
                        cancelButtonStyle: ButtonStyle(
                          backgroundColor: WidgetStateProperty.resolveWith((states) {
                            return white;
                          }),
                          foregroundColor: WidgetStateProperty.resolveWith((states) {
                            return grey;
                          }),
                        ),
                        inputDecorationTheme: InputDecorationTheme(
                          focusColor: darkYellow,
                          iconColor: darkYellow
                        )
                      ),
                    ),
                    child: child!,
                  );
                },
              );
              if (dateTimeValue != null) {
                var text = dateTimeValue;
                internalInvoiceDateController.text = DateFormat('d MMM yyyy').format(text);
              }
            },
            decoration: InputDecoration(
              hintText: "Select Date",
              hintStyle: italicFontsStyle(fontSize: 16.0, height: 1.5, color: grey),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: darkYellow),
                borderRadius: BorderRadius.circular(6.0),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: lightGrey),
                borderRadius: BorderRadius.circular(6.0),
              ),
              border: OutlineInputBorder(),
              contentPadding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
              fillColor: white,
              filled: true,
              suffix: Padding(
                padding: EdgeInsets.only(left: 10),
                child: Material(
                color: transparent,
                child: InkWell(
                  onTap: () {
                    setState(() {
                      internalInvoiceDateController.text = '';
                    });
                  },
                  child: Container(
                    transform: Matrix4.translationValues(2, 3, 5),
                    child: SvgPicture.asset(ic_close, color: black)
                  ),
                  ),
                ),
              ),
            ),
            style: regularFontsStyle(fontSize: 16.0, height: 1.5),
            onChanged: (value) {
              internalInvoiceDateController.text = value;
            },
            validator: (value) {
              if (value == null || value.isEmpty) {
                return '*Please fill up';
              }
              return null;
            },
            onSaved: (val) => {},
          ),
          SizedBox(height: 16.0),
        ] else if(widget.pushUpdateInfo?.forClient?.extraDatesDisplayArr?.date != '' && showDateTimePicker && isInternalView == 1) ...[
          TextFormField(
            controller: clientInvoiceDateController,
            onTap: () async {
              DateTime dateTime = DateFormat('d MMM yyyy').parse(widget.pushUpdateInfo?.forClient?.extraDatesDisplayArr?.date ?? '');
              var dateTimeValue = await showDatePicker(
                context: context,
                initialDate: dateTime,
                firstDate: DateTime.now().subtract(Duration(days: 365)),
                lastDate: DateTime.now().add(Duration(days: 3650)),
                builder: (context, child) {
                  return Theme(
                    data: ThemeData.light().copyWith(
                      datePickerTheme: DatePickerThemeData(
                        backgroundColor: white,
                        surfaceTintColor: Colors.transparent,
                        headerForegroundColor: Colors.black54,
                        dayBackgroundColor: WidgetStateProperty.resolveWith((states) {
                          if (states.contains(WidgetState.selected)) {
                            return darkYellow;
                          }
                          return white;
                        }),
                        todayBackgroundColor: WidgetStateProperty.resolveWith((states) {
                          if (states.contains(WidgetState.selected)) {
                            return darkYellow;
                          }
                          return white;
                        }),
                        todayForegroundColor: WidgetStateProperty.resolveWith((states) {
                          if (states.contains(WidgetState.selected)) {
                            return white;
                          }
                          return darkYellow;
                        }),
                        yearOverlayColor: WidgetStateProperty.resolveWith((states) {
                          if (states.contains(WidgetState.selected)) {
                            return darkYellow;
                          }
                          return white;
                        }),
                        yearForegroundColor: WidgetStateProperty.resolveWith((states) {
                          if (states.contains(WidgetState.selected)) {
                            return white;
                          }
                          return black;
                        }),
                        yearBackgroundColor: WidgetStateProperty.resolveWith((states) {
                          if (states.contains(WidgetState.selected)) {
                            return darkYellow;
                          }
                          return white;
                        }),
                        confirmButtonStyle: ButtonStyle(
                          backgroundColor: WidgetStateProperty.resolveWith((states) {
                            return white;
                          }),
                          foregroundColor: WidgetStateProperty.resolveWith((states) {
                            return darkYellow;
                          }),
                        ),
                        cancelButtonStyle: ButtonStyle(
                          backgroundColor: WidgetStateProperty.resolveWith((states) {
                            return white;
                          }),
                          foregroundColor: WidgetStateProperty.resolveWith((states) {
                            return grey;
                          }),
                        ),
                      ),
                    ),
                    child: child!,
                  );
                },
              );
              if (dateTimeValue != null) {
                var text = dateTimeValue;
                clientInvoiceDateController.text = DateFormat('d MMM yyyy').format(text);
              }
            },
            decoration: InputDecoration(
              hintText: "Select Date",
              hintStyle: italicFontsStyle(fontSize: 16.0, height: 1.5, color: grey),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: darkYellow),
                borderRadius: BorderRadius.circular(6.0),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: lightGrey),
                borderRadius: BorderRadius.circular(6.0),
              ),
              border: OutlineInputBorder(),
              contentPadding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
              fillColor: white,
              filled: true,
              suffix: Padding(
                padding: EdgeInsets.only(left: 10),
                child: Material(
                color: transparent,
                child: InkWell(
                  onTap: () {
                    setState(() {
                      clientInvoiceDateController.text = '';
                    });
                  },
                  child: Container(
                    transform: Matrix4.translationValues(2, 3, 5),
                    child: SvgPicture.asset(ic_close, color: black)
                  ),
                  ),
                ),
              ),
            ),
            style: regularFontsStyle(fontSize: 16.0, height: 1.5),
            onChanged: (value) {
              clientInvoiceDateController.text = value;
            },
            validator: (value) {
              if (value == null || value.isEmpty) {
                return '*Please fill up';
              }
              return null;
            },
            onSaved: (val) => {},
          ),
        ],
      ],
    );
  }

  Widget uploadProgressLayout() => Container(
    color: white,
    padding: EdgeInsets.symmetric(vertical: 36.0),
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: !completed
      ? [
          Lottie.asset(
            lottie_loading_spinner,
            width: 50.0,
            height: 50.0,
            fit: BoxFit.fill,
          ),
          SizedBox(height: 16.0),
          Text(
            'just a few secs...',
            style: regularFontsStyle(fontSize: 14.0, color: grey),
          ),
        ]
      : [
          SvgPicture.asset(ic_completed, width: 80.0, color: darkYellow),
          SizedBox(height: 32.0),
          Text(
            'You’ve successfully marked the task as completed!',
            style: boldFontsStyle(fontSize: 16.0, height: 1.5),
            textAlign: TextAlign.center,
          )
        ],
    ),
  );

  Widget docListingLayout(docAttachments) => ValueListenableBuilder(
  valueListenable: docAttachments,
  builder: (BuildContext context, List mediaFiles, Widget? child) {
    return Column(
      children: [
        for (var i = 0; i < docAttachments.value.length; i++)
          Stack(
            alignment: AlignmentDirectional.center,
            children: [
              GestureDetector(
                onTap: () async {
                  // go to pdf viewer
                  String? temporaryPath;
                  String? savedPath = SpManager.getString(docAttachments.value[i].imagePath.toString());
                  if (savedPath != null && await File(savedPath).exists()) {
                    temporaryPath = savedPath;
                  } else {
                    showToastMessage(context, message: 'Downloading', duration: 1);
                    await Api().downloadFile(docAttachments.value[i].imagePath ?? '', 'pdf', context).then((localSavedPath) {
                      temporaryPath = localSavedPath;
                    });
                    await SpManager.putString(docAttachments.value[i].imagePath.toString(), temporaryPath ?? '');
                    await Future.delayed(Duration(milliseconds: 1500));
                  }
                  await OpenFilex.open(temporaryPath);
                },
                child: Container(
                  margin: EdgeInsets.only(right: 10.0, top: 4.0, bottom: 4.0),
                  padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
                  decoration: BoxDecoration(
                  border: Border.all(
                    color: isInternalView == 1 
                      ? (docAttachments.value[i].selectedStatus == 1 ? darkYellow : darkGrey.withOpacity(0.5))
                      : darkGrey.withOpacity(0.5),
                  ),
                    borderRadius: BorderRadius.circular(500.0)
                  ),
                  child: Row(children: [
                    SvgPicture.asset(ic_doc, width: 18.0),
                    SizedBox(width: 8.0),
                    Expanded(
                      child: Text(
                        docAttachments.value[i].imagePath?.substring((docAttachments.value[i].imagePath?.lastIndexOf("/") ?? 0) + 1) ?? '',
                        style: boldFontsStyle(fontSize: 14.0, color: isInternalView == 1 ? (docAttachments.value[i].selectedStatus == 1 ? null : darkGrey.withOpacity(0.5)) : null,),
                        overflow: TextOverflow.clip,
                      ),
                    )
                  ]),
                ),
              ),
              Positioned(
                right: 0.0,
                child: GestureDetector(
                  onTap: () {
                    // remove the doc
                    docAttachments.value.removeAt(i);
                    docAttachments.notifyListeners();
                    clientDocAttachments.value.removeAt(i);
                    clientDocAttachments.notifyListeners();
                  },
                  child: GFCheckbox(
                    size: 26.0,
                    type: GFCheckboxType.circle,
                    activeBgColor: yellow,
                    inactiveBorderColor: lightGrey,
                    activeBorderColor: widget.pushUpdateInfo?.updateScreen == "Resolve Task" ? transparent : black,
                    activeIcon: 
                    (isInternalView == 0)
                    ? Icon(Icons.close, size: 16, color: black)
                    : Icon(Icons.check, size: 16, color: black),
                    onChanged: (value) {
                      setState(() {
                        if (isInternalView == 0) {
                          docAttachments.value.removeAt(i);
                          docAttachments.notifyListeners();
                          clientDocAttachments.value.removeAt(i);
                          clientDocAttachments.notifyListeners();
                        }else{
                          docAttachments.value[i].selectedStatus = value ? 1 : 0;
                        }
                      });
                    },
                    value: 
                    (isInternalView == 0 && (docAttachments.value[i].selectedStatus == 0 || docAttachments.value[i].selectedStatus == 1))
                    ? true
                    :  docAttachments.value[i].selectedStatus == 1,
                    inactiveIcon: null,
                  ),
                ),
              ),
            ],
          ),
      ],
    );
  });

  Widget imageListingLayout(ValueNotifier<List<AttachModel>> mediaAttachments) => ValueListenableBuilder<List<AttachModel>>(
  valueListenable: mediaAttachments,
  builder: (BuildContext context, List<AttachModel> mediaFiles, Widget? child) {
    return Wrap(
      children: [
        if (isInternalView == 0)
        // upload file widget
        Container(
          margin: EdgeInsets.only(right: 16.0),
          child: GestureDetector(
            onTap: () {
              if (!mediaUploading) {
                mediaSelectionDialog(
                  context,
                  cameraEnabled: true,
                  cameraClick: () {
                    getCamera();
                  },
                  photoEnabled: true,
                  photoClick: () {
                    getImage();
                  },
                  takeVideoEnabled: true,
                  takeVideoClick: () {
                    takeVideo();
                  },
                  videoEnabled: true,
                  videoClick: () {
                    getVideo();
                  },
                  fileEnabled: true,
                  fileClick: () {
                    getPDF();
                  },
                );
                setState(() {
                  fileRequireValidation = true;
                });
              }
            },
            child: Container(
              width: 64.0,
              height: 64.0,
              margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: DottedBorder(
                padding: !mediaUploading ? EdgeInsets.all(20) : EdgeInsets.symmetric(horizontal: 8.0),
                radius: Radius.circular(6.0),
                color: fileRequireValidation ? lightGrey : red,
                strokeWidth: 1.5,
                dashPattern: [8, 7],
                borderType: BorderType.RRect,
                child: !mediaUploading
                  ? SvgPicture.asset(ic_attach, color: fileRequireValidation ? grey : red, width: 24.0)
                  : Center(
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
                      decoration: BoxDecoration(
                          color: white,
                          borderRadius: BorderRadius.circular(6.0),
                          boxShadow: app_box_shadow),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            'Uploading...',
                            style: boldFontsStyle(fontSize: 7.0)
                          ),
                          SizedBox(height: 2.0),
                          LinearPercentIndicator(
                            lineHeight: 8.0,
                            animationDuration: 1500,
                            animation: true,
                            animateFromLastPercent: true,
                            percent: 85 / 100,
                            progressColor: yellow,
                            backgroundColor: lightGrey,
                            padding: EdgeInsets.zero,
                            barRadius: Radius.circular(4.0),
                          ),
                        ],
                      ),
                    ),
                  ),
              ),
            ),
          ),
        ),
        for (var i = 0; i < mediaAttachments.value.length; i++)
          Stack(
            children: [
              GestureDetector(
                onTap: () {
                  mediaArgsList = mediaAttachments.value.map((m) => MediaInfoArguments(
                    mediaPath: m.imagePath ?? '', 
                    mediaDescription: '', 
                    mediaType: (m.imagePath ?? '').substring((m.imagePath?.lastIndexOf(".") ?? 0) + 1), 
                    datetime: ''
                  )).toList();

                  goToNextNamedRoute(context, mediaSliderScreen, args: MediaSliderArguments(mediaArgsList, i));
                },
                child: Container(
                  padding: EdgeInsets.only(top: 8.0, right: 16.0, bottom: 8.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(6.0),
                    child: mediaAttachments.value[i].imagePath?.substring((mediaAttachments.value[i].imagePath?.lastIndexOf(".") ?? 0) + 1) == 'mp4'
                        ? VideoThumbnailWidget(
                            videoPath: mediaAttachments.value[i].imagePath ?? '',
                            height: 64,
                            width: 64,
                          )
                        : mediaAttachments.value[i].imagePath?[0] == '/'
                            ? Image.file(
                                File(mediaAttachments.value[i].imagePath ?? ''),
                                height: 64,
                                width: 64,
                                fit: BoxFit.cover,
                                cacheHeight: SizeUtils.height(context, 30).toInt(),
                                cacheWidth: SizeUtils.width(context, 60).toInt(),
                              )
                            : Container(
                              decoration: BoxDecoration(
                                color: mediaAttachments.value[i].selectedStatus == 1 ? yellow : shimmerGrey,
                                borderRadius: BorderRadius.circular(6.0),
                                border: Border.all(
                                  color: mediaAttachments.value[i].selectedStatus == 1 ? yellow : shimmerGrey,
                                  width: 2.0,
                                ),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(6.0),
                                child: CachedNetworkImage(
                                  width: 64,
                                  height: 64,
                                  placeholder: (context, url) => Container(color: Colors.grey[50]),
                                  imageUrl: mediaAttachments.value[i].imagePath ?? '',
                                  fit: BoxFit.cover,
                                  alignment: Alignment.center,
                                  maxHeightDiskCache: 100,
                                  maxWidthDiskCache: 100,
                                  color: isInternalView == 0 ? null : mediaAttachments.value[i].selectedStatus == 1 ? null : shimmerGrey.withOpacity(0.5),
                                  colorBlendMode: isInternalView == 0 ? null : mediaAttachments.value[i].selectedStatus == 1 ? null : BlendMode.srcOver,
                                ),
                              ),
                            ),
                  ),
                ),
              ),
              Positioned(
                top: 0,
                right: 4,
                child: GestureDetector(
                  onTap: () {
                    // remove the image
                    mediaAttachments.value.removeAt(i);
                    mediaAttachments.notifyListeners();
                    clientMediaAttachments.value.removeAt(i);
                    clientMediaAttachments.notifyListeners();
                  },
                  child: GFCheckbox(
                    size: 26.0,
                    type: GFCheckboxType.circle,
                    activeBgColor: yellow,
                    inactiveBorderColor: lightGrey,
                    activeBorderColor: widget.pushUpdateInfo?.updateScreen == "Resolve Task"? transparent : black,
                    activeIcon: 
                    (isInternalView == 0)
                    ? Icon(Icons.close, size: 16, color: black)
                    : Icon(Icons.check, size: 16, color: black),
                    onChanged: (value) {
                      setState(() {
                        if (isInternalView == 0) {
                          // remove the image
                          mediaAttachments.value.removeAt(i);
                          mediaAttachments.notifyListeners();
                          clientMediaAttachments.value.removeAt(i);
                          clientMediaAttachments.notifyListeners();
                        }else{
                          mediaAttachments.value[i].selectedStatus = value ? 1 : 0;
                        }
                      });
                    },
                    value: 
                    (isInternalView == 0 && (mediaAttachments.value[i].selectedStatus == 0 || mediaAttachments.value[i].selectedStatus == 1))
                    ? true
                    : mediaAttachments.value[i].selectedStatus == 1,
                    inactiveIcon: null,
                  ),
                ),
              ),
            ],
          ),
      ],
    );
  });

// Open camera
  Future getCamera() async {
    try {
      await AppService.getConfig().then((response) async {
        final pickedFile = await _picker.pickImage(source: ImageSource.camera, imageQuality: response.data['response']['taskMediaUploadQuality']);
        setState(() {
          mediaUploading = true;
        });
        if (pickedFile != null) {
          await _fileUploadService.taskFileUpload(context, pickedFile.path).then((response) {
            setState(() {
              internalMediaAttachments.value.add(AttachModel(
                  taskImageID: response['taskImageID'],
                  imagePath: response['url'],
                  imageName: response['filename'],
                  selectedStatus: 1
                ));
              clientMediaAttachments.value.add(AttachModel(
                taskImageID: response['taskImageID'],
                imagePath: response['url'],
                imageName: response['filename'],
                selectedStatus: (widget.pushUpdateInfo?.isShownMediasInCA ?? false) ? 1 : 0
              ));
            });
          });
        }
        setState(() {
          mediaUploading = false;
        });
      });
    } on PlatformException catch (e) {
      ErrorUtils.handlePlatformError(context, e);
    } catch (e) {
      showError(context, 'Unexpected error, please try again.');
    }
    internalMediaAttachments.notifyListeners();
    clientMediaAttachments.notifyListeners();    
  }

  // Image picker
  Future getImage() async {
    var pickedFile;
    bool pickSingleImage = false;
    if (Platform.isIOS) {
      var iosData = await DeviceInfoPlugin().iosInfo;
      if ((iosData.systemVersion ?? '0.0').compareTo('14.0') < 0) {
        pickSingleImage = true;
      }
    }
    try {
      await AppService.getConfig().then((response) async {
        if (pickSingleImage) {
          await _picker.pickImage(source: ImageSource.gallery, imageQuality: response.data['response']['taskMediaUploadQuality']).then((value) {
            pickedFile = [];
            pickedFile.add(value);
          });
        } else {
          pickedFile = await _picker.pickMultiImage(imageQuality: response.data['response']['taskMediaUploadQuality']);
        }
        setState(() {
          mediaUploading = true;
        });
        if (pickedFile != null) {
          await Future.forEach(pickedFile, (XFile file) async {
            await _fileUploadService.taskFileUpload(context, file.path).then((response) {
              setState(() {
                internalMediaAttachments.value.add(AttachModel(
                  taskImageID: response['taskImageID'],
                  imagePath: response['url'],
                  imageName: response['filename'],
                  selectedStatus: 1
                ));
                clientMediaAttachments.value.add(AttachModel(
                  taskImageID: response['taskImageID'],
                  imagePath: response['url'],
                  imageName: response['filename'],
                  selectedStatus: (widget.pushUpdateInfo?.isShownMediasInCA ?? false) ? 1 : 0
                ));
              });
            });
          });
        }
        setState(() {
          mediaUploading = false;
        });
      });
    } on PlatformException catch (e) {
      ErrorUtils.handlePlatformError(context, e);
    } catch (e) {
      showError(context, 'Unexpected error, please try again.');
    }
    internalMediaAttachments.notifyListeners();
    clientMediaAttachments.notifyListeners();
  }

  // Take Video via Camera
  Future takeVideo() async {
    try {
      final pickedFile = await _picker.pickVideo(source: ImageSource.camera);
      setState(() {
        mediaUploading = true;
      });
      if (pickedFile != null) {
        await VideoCompress.setLogLevel(0);
        File renamedFile;
        if (Platform.isIOS) {
          String fileExtension = p.extension(pickedFile.path);
          String uniqueIdentifier = DateTime.now().millisecondsSinceEpoch.toString();
          String shortenedFileName = p.basenameWithoutExtension(pickedFile.path).substring(0, 10) + '_' + uniqueIdentifier + fileExtension;
          String shortenedFilePath = p.join(p.dirname(pickedFile.path), shortenedFileName);
          renamedFile = File(pickedFile.path).renameSync(shortenedFilePath);
        } else {
          renamedFile = File(pickedFile.path);
        }
        final MediaInfo? comprossedVideo = await VideoCompress.compressVideo(
          renamedFile.path,
          quality: VideoQuality.Res1280x720Quality,
          deleteOrigin: true,
          includeAudio: true,
        );
        if (!internalMediaAttachments.value.contains(comprossedVideo!.path)) {
          await _fileUploadService.taskFileUpload(context, comprossedVideo.path).then((response) {
            setState(() {
              internalMediaAttachments.value.add(AttachModel(
                taskImageID: response['taskImageID'],
                imagePath: response['url'],
                imageName: response['filename'],
                selectedStatus: 1
              ));
              clientMediaAttachments.value.add(AttachModel(
                taskImageID: response['taskImageID'],
                imagePath: response['url'],
                imageName: response['filename'],
                selectedStatus: (widget.pushUpdateInfo?.isShownMediasInCA ?? false) ? 1 : 0
              ));
            });
          });
        }
      } 
      setState(() {
        mediaUploading = false;
      });
    } on PlatformException catch (e) {
      ErrorUtils.handlePlatformError(context, e);
    } catch (e) {
      showError(context, 'Unexpected error, please try again.');
    }
    internalMediaAttachments.notifyListeners();
    clientMediaAttachments.notifyListeners();
  }

  // Video picker
  Future getVideo() async {
    try {
      setState(() {
        mediaUploading = true;
      });
      final pickedFile = await _picker.pickVideo(source: ImageSource.gallery);
      if (pickedFile != null) {
        await VideoCompress.setLogLevel(0);
        File renamedFile;
        if (Platform.isIOS) {
          String fileExtension = p.extension(pickedFile.path);
          String uniqueIdentifier = DateTime.now().millisecondsSinceEpoch.toString();
          String shortenedFileName = p.basenameWithoutExtension(pickedFile.path).substring(0, 10) + '_' + uniqueIdentifier + fileExtension;
          String shortenedFilePath = p.join(p.dirname(pickedFile.path), shortenedFileName);
          renamedFile = File(pickedFile.path).renameSync(shortenedFilePath);
        } else {
          renamedFile = File(pickedFile.path);
        }
        final MediaInfo? comprossedVideo = await VideoCompress.compressVideo(
          renamedFile.path,
          quality: VideoQuality.Res1280x720Quality,
          deleteOrigin: true,
          includeAudio: true,
        );
        if (!internalMediaAttachments.value.contains(comprossedVideo!.path)) {
          await _fileUploadService.taskFileUpload(context, comprossedVideo.path).then((response) {
            setState(() {
              internalMediaAttachments.value.add(AttachModel(
                taskImageID: response['taskImageID'],
                imagePath: response['url'],
                imageName: response['filename'],
                selectedStatus: 1
              ));
              clientMediaAttachments.value.add(AttachModel(
                taskImageID: response['taskImageID'],
                imagePath: response['url'],
                imageName: response['filename'],
                selectedStatus: (widget.pushUpdateInfo?.isShownMediasInCA ?? false) ? 1 : 0
              ));
            });
          });
        }
      } 
      setState(() {
        mediaUploading = false;
      });
    } on PlatformException catch (e) {
      ErrorUtils.handlePlatformError(context, e);
    } catch (e) {
      showError(context, 'Unexpected error, please try again.');
    }
    internalMediaAttachments.notifyListeners();
    clientMediaAttachments.notifyListeners();
  }

  // PDF file picker
  Future getPDF() async {
    try {
      FilePickerResult? pickedFile = await FilePicker.platform.pickFiles(
        type: FileType.custom, 
        allowedExtensions: ['pdf'],
        allowMultiple: true
      );
      
      setState(() {
        mediaUploading = true;
      });
      if (pickedFile != null) {
        await Future.forEach(
          pickedFile.files, (PlatformFile file) async {
          if (file.extension?.toLowerCase() != 'pdf') {
            showError(context, 'Must be PDF');
          } else if (file.size > 20971520) {
            showError(context, 'PDF size must below 20MB');
          } else {
            await _fileUploadService.taskFileUpload(context, file.path).then((response) {
              setState(() {
                internalDocAttachments.value.add(AttachModel(
                  taskImageID: response['taskImageID'],
                  imagePath: response['url'],
                  imageName: response['filename'],
                  selectedStatus: 1
                ));
                clientDocAttachments.value.add(AttachModel(
                  taskImageID: response['taskImageID'],
                  imagePath: response['url'],
                  imageName: response['filename'],
                  selectedStatus: (widget.pushUpdateInfo?.isShownMediasInCA ?? false) ? 1 : 0
                ));
              });
            });
          }
        });
      } 
      setState(() {
        mediaUploading = false;
      });
    } on PlatformException catch (e) {
      ErrorUtils.handlePlatformError(context, e);
    } catch (e) {
      showError(context, 'Unexpected error, please try again.');
    }
    internalDocAttachments.notifyListeners();
    clientDocAttachments.notifyListeners();
  }

  markAsComplete() async {
    if (internalMediaAttachments.value.length == 0 && internalDocAttachments.value.length == 0 && widget.isFileRequire) {
      setState(() {
        fileRequireValidation = false;
      });
    } else {
      setState(() {
        uploading = true;
      });
      List<int> uploadedAttachmentIDs = [];

      internalDocAttachments.value.forEach((file) {
        if (file.selectedStatus == 1 && file.taskImageID != null) {
          uploadedAttachmentIDs.add(file.taskImageID!);
        }
      });

      internalMediaAttachments.value.forEach((image) {
        if (image.selectedStatus == 1 && image.taskImageID != null) {
          uploadedAttachmentIDs.add(image.taskImageID!);
        }
      });

      PushUpdateModel data = PushUpdateModel(
        taskID: widget.pushUpdateInfo?.taskID,
        action: widget.pushUpdateInfo?.action,
        remark: internalRemarkInputController.text,
        taskImageID: uploadedAttachmentIDs,
      );

      setState(() {
        onlyClickOnce = onlyClickOnce + 1;
      });
      if (onlyClickOnce == 1) {
        await _taskService.postTaskPushClientApp(data).then((response) {
          setState(() {
            completed = true;
          });

          Future.delayed(Duration(milliseconds: 2000)).then((value) {
            Navigator.pop(context, true);
            widget.onPush(true);
          });
        }).onError((error, stackTrace) {
          setState(() {
            uploading = false;
          });
          showError(context, error.toString());
        }).timeout(Duration(seconds: 60), onTimeout: () {
            setState(() {
              onlyClickOnce = 0;
            });
        }).whenComplete(() {
          setState(() {
            onlyClickOnce = 0;
          });
        });
      }
    }
  }

  flaggedTask() async {
    TaskFlaggingModel formData = TaskFlaggingModel(
      taskID: widget.pushUpdateInfo?.taskID,
      projectID: widget.pushUpdateInfo?.projectID,
      reason: internalRemarkInputController.text,
    );

    setState(() {
      onlyClickOnce = onlyClickOnce + 1;
    });

    if (onlyClickOnce == 1) {
      _commentService.postTaskFlagging(formData).then((response) {
        showToastMessage(context, message: 'Task Flagged.');
        Navigator.pop(context, true);
        widget.onPush(true);
      }).onError((error, stackTrace) {
        showToastMessage(context, message: error.toString());
      }).timeout(Duration(seconds: 60), onTimeout: () {
        setState(() {
          onlyClickOnce = 0;
        });
      }).whenComplete(() {
        setState(() {
          onlyClickOnce = 0;
        });
      });
    }
  }

  resolveTask() async {
    bool isMediaAttachmentSelected = false;
    bool isExtraDateTimeValidated = false;
    bool isSpecialTask = widget.pushUpdateInfo?.specialTaskType == "edited-photo" || widget.pushUpdateInfo?.specialTaskType == "handover-actual-date";

    if (widget.pushUpdateInfo?.extraDatesDisplayArr != null) {
      if (internalInvoiceDateController.text == '' || internalInvoiceDateController.text == null) {
        isExtraDateTimeValidated = true;
      }
    }
    if (_formKey.currentState!.validate()) {
      setState(() {
        if (isExtraDateTimeValidated) datetimeValidation = false;
      });
    }
    if ((isSpecialTask) && !_formKey.currentState!.validate()) {
      setState(() {
        linkRequireValidation = false;
      });
    }
    else if (internalMediaAttachments.value.length == 0 && internalDocAttachments.value.length == 0 && widget.isFileRequire && !isSpecialTask) {
      setState(() {
        fileRequireValidation = false;
      });
    } 
      List<int> uploadedAttachmentIDs = [];

      internalDocAttachments.value.forEach((file) {
        if (file.selectedStatus == 1 && file.taskImageID != null) {
          uploadedAttachmentIDs.add(file.taskImageID!);
          isMediaAttachmentSelected = true;
        }
      });

      internalMediaAttachments.value.forEach((image) {
        if (image.selectedStatus == 1 && image.taskImageID != null) {
          uploadedAttachmentIDs.add(image.taskImageID!);
          isMediaAttachmentSelected = true;
        }
      });
      
      if (widget.isFileRequire && !isSpecialTask && (!isMediaAttachmentSelected || isExtraDateTimeValidated)) {
        setState(() {
          uploading = false;
          if (isExtraDateTimeValidated) datetimeValidation = false;
          if (!isMediaAttachmentSelected) fileRequireValidation = false;
        });
      } else if (widget.pushUpdateInfo?.specialTaskType == "handover-actual-date" && isExtraDateTimeValidated ) {
        setState(() {
          uploading = false;
          if (isExtraDateTimeValidated) datetimeValidation = false;
        });
      } else {
        setState(() {
          uploading = true;
          fileRequireValidation = true;
          datetimeValidation = true;
        });

        TaskResolveModel formData = TaskResolveModel(
          taskID: widget.pushUpdateInfo?.taskID,
          projectID: widget.pushUpdateInfo?.projectID,
          mediaID: uploadedAttachmentIDs,
          remarks: internalRemarkInputController.text,
          dropboxLink: dropboxInputController.text,
          actualHandoverDate: (widget.pushUpdateInfo?.specialTaskType == "handover-actual-date") ? internalInvoiceDateController.text : "",
          invoiceDate: (widget.pushUpdateInfo?.specialTaskType != "handover-actual-date") ? internalInvoiceDateController.text : "",
        );

        setState(() {
          onlyClickOnce = onlyClickOnce + 1;
        });

        if (onlyClickOnce == 1) {
          _commentService.postMyTaskResolve(formData).then((response) {
            showToastMessage(context, message: 'Task Resolved Successfully.');
            Navigator.pop(context, true);
            widget.onPush(true);
          }).onError((error, stackTrace) {
            Navigator.pop(context, true);
            widget.onPush(false);
            showToastMessage(context, message: error.toString());
          }).timeout(Duration(seconds: 60), onTimeout: () {
            setState(() {
              onlyClickOnce = 0;
            });
          }).whenComplete(() {
            setState(() {
              onlyClickOnce = 0;
            });
          });
        }
      }
    }

// confirm popup
  _showConfirm() async {
    bool isMediaAttachmentSelected = false;
    bool isExtraDateTimeValidated = false;
    bool isSpecialTask = widget.pushUpdateInfo?.specialTaskType == "edited-photo" || widget.pushUpdateInfo?.specialTaskType == "handover-actual-date";
    int onlyClickOnce = 0;

    if (widget.pushUpdateInfo?.extraDatesDisplayArr != null) {
      if ((internalInvoiceDateController.text == '' || internalInvoiceDateController.text == null) && (clientInvoiceDateController.text == "" || clientInvoiceDateController.text == null)) {
        isExtraDateTimeValidated = true;
      }
    }
    if (_formKey.currentState!.validate()) {
      setState(() {
        if (isExtraDateTimeValidated) datetimeValidation = false;
      });
    }

    if (isSpecialTask && !_formKey.currentState!.validate()) {
      setState(() {
        linkRequireValidation = false;
      });
    }
    else if (internalMediaAttachments.value.length == 0 && internalDocAttachments.value.length == 0 && widget.isFileRequire && !isSpecialTask) {
      setState(() {
        fileRequireValidation = false;
        if (isExtraDateTimeValidated) datetimeValidation = false;
      });
    } 

    List<int> allAttachmentIDs = [];
    List<int> uploadedClientAttachmentIDs = [];

      internalDocAttachments.value.forEach((file) {
        allAttachmentIDs.add(file.taskImageID!);
        isMediaAttachmentSelected = true;
      });

      internalMediaAttachments.value.forEach((image) {
        allAttachmentIDs.add(image.taskImageID!);
        isMediaAttachmentSelected = true;
      });

      clientDocAttachments.value.forEach((file) {
        if (file.selectedStatus == 1 && file.taskImageID != null) {
          uploadedClientAttachmentIDs.add(file.taskImageID!);
        }
      });

      clientMediaAttachments.value.forEach((image) {
        if (image.selectedStatus == 1 && image.taskImageID != null) {
          uploadedClientAttachmentIDs.add(image.taskImageID!);
        }
      });

    if (widget.isFileRequire && !isSpecialTask && (!isMediaAttachmentSelected || isExtraDateTimeValidated)) {
      setState(() {
        uploading = false;
        if (isExtraDateTimeValidated) datetimeValidation = false;
        if (!isMediaAttachmentSelected) fileRequireValidation = false;
      });
    } else if (widget.pushUpdateInfo?.specialTaskType == "handover-actual-date" && isExtraDateTimeValidated ) {
      setState(() {
        uploading = false;
        if (isExtraDateTimeValidated) datetimeValidation = false;
      });
    } else {
      setState(() {
        uploading = true;
      });

      String formattedClientDate = "";
      String formattedInternalDate = "";
      
      if (widget.pushUpdateInfo?.specialTaskType != "handover-actual-date") {
        if (internalInvoiceDateController.text != '') {
          DateTime internalDateTimeText = DateFormat('d MMM yyyy').parse(internalInvoiceDateController.text);
          formattedInternalDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(internalDateTimeText);
        }
        if (clientInvoiceDateController.text != '') {
          DateTime clientDateTimeText = DateFormat('d MMM yyyy').parse(clientInvoiceDateController.text);
          formattedClientDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(clientDateTimeText);
        }
      } else {
        DateTime internalDateTimeText = DateFormat('d MMM yyyy').parse(internalInvoiceDateController.text);
        formattedInternalDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(internalDateTimeText);
      } 

      ForInternal forInternalAttachments = ForInternal(
        taskImageID: allAttachmentIDs,
        remark: internalRemarkInputController.text,
        dropboxLink: dropboxInputController.text,
        actualHandoverDate: (widget.pushUpdateInfo?.specialTaskType == "handover-actual-date") ? formattedInternalDate : "",
        invoiceDate: (widget.pushUpdateInfo?.specialTaskType != "handover-actual-date") ? formattedInternalDate : "",
        paymentDueDate: (widget.pushUpdateInfo?.specialTaskType != "handover-actual-date") ? formattedClientDate : "",
      );

      ForClient forClientAttachments = ForClient(
        taskImageID: uploadedClientAttachmentIDs,

        remark: clientRemarkInputController.text,
        dropboxLink: dropboxInputController.text,
        actualHandoverDate: (widget.pushUpdateInfo?.specialTaskType == "handover-actual-date") ? formattedInternalDate : "",
        invoiceDate: (widget.pushUpdateInfo?.specialTaskType != "handover-actual-date") ? formattedInternalDate : "",
        paymentDueDate: (widget.pushUpdateInfo?.specialTaskType != "handover-actual-date") ? formattedClientDate : "",
      );

      MyTaskEditModel formData = MyTaskEditModel(
        taskID: widget.pushUpdateInfo?.taskID,
        forInternal: forInternalAttachments,
        forClient: forClientAttachments,
      );

      showDialog(
        context: this.context,
        builder: (BuildContext context) => AlertDialog(
          backgroundColor: white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          title: Stack(
            children: [
              Positioned(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  IconButton(
                    color: transparent,
                    splashColor: transparent,
                    focusColor: transparent,
                    onPressed: () {
                      Navigator.pop(context, true);
                      widget.onPush(false);
                    },
                    icon: SvgPicture.asset(ic_close, width: 20.0, color: black),
                  ),
                ],
              ))
            ],
          ),
          titlePadding: EdgeInsets.fromLTRB(5.0, 10.0, 10.0, 0.0),
          content: StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Are the changes confirm?',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, height: 1.5),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 32.0),
                Row(
                  children: [
                    Expanded(
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          foregroundColor: black,
                          side: BorderSide(width: 1, color: black),
                          padding: EdgeInsets.symmetric(vertical: 12.0),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                        ),
                        onPressed: () {
                          Navigator.pop(context, false);
                        },
                        child: Text(
                          'Nope',
                          style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: black),
                        ),
                      ),
                    ),
                    SizedBox(width: 16.0),
                    Expanded(
                      child: Opacity(
                        opacity: (onlyClickOnce >= 1) ? 0.5 : 1.0,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: yellow,
                            padding: EdgeInsets.symmetric(vertical: 12.0),
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                          ),
                          onPressed: () {
                            setState(() {
                              onlyClickOnce = onlyClickOnce + 1;
                            });
                            if (onlyClickOnce == 1) {
                              _commentService.postMyTaskEdit(formData).then((response) {
                                showToastMessage(context, message: 'Task Updated!');
                                Navigator.pop(context, true);
                                widget.onPush(true);
                              }).onError((error, stackTrace) {
                                showToastMessage(context, message: error.toString());
                              }).timeout(Duration(seconds: 60), onTimeout: () {
                                setState(() {
                                  onlyClickOnce = 0;
                                });
                              }).whenComplete(() {
                                setState(() {
                                  onlyClickOnce = 0;
                                });
                              });
                            }
                          },
                          child: Text(
                            'Confirm',
                            style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: black),
                          ),
                        ),
                      ),
                    )
                  ],
                )
              ],
            );
          },),)).then((val) {
          if (val == null) return;
          if (val) Navigator.pop(context, false);
          setState(() {});
        });
    }
  }
}
