import 'package:flutter/material.dart';

class LabelSeparator extends StatelessWidget {
  final String label;
  final Color color;

  LabelSeparator({
    required this.label,
    required this.color,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.only(right: 15, left: 24),
              child: Divider(thickness: 0.5, color: this.color),
            ),
          ),
          Text(
            this.label,
            style: TextStyle(color: this.color),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(right: 24, left: 15),
              child: Divider(thickness: 0.5, color: this.color),
            ),
          ),
        ]
      ),
    );
  }
}