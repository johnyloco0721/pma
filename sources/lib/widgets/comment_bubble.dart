import 'package:assorted_layout_widgets/assorted_layout_widgets.dart';
import 'package:bubble/bubble.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:open_filex/open_filex.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/models/feed_item_model.dart';
import 'package:sources/models/media_item_model.dart';

import '../args/media_slider_args.dart';
import '../constants/app_colors.dart';
import '../constants/app_fonts.dart';
import '../constants/app_routes.dart';
import '../database/app_database.dart';
import '../models/comment_details_model.dart';
import '../services/api.dart';
import '../services/service_locator.dart';
import '../utils/dialog_utils.dart';
import '../utils/route_utils.dart';
import '../utils/shared_preference_utils/sp_manager.dart';
import '../utils/text_utils.dart';
import '../utils/util_helper.dart';
import '../view_models/app_view_model.dart';
import 'avatar.dart';
import 'content_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';

class CommentBubble extends StatefulWidget {
  final CommentDetailsResponse commentWithDetails;
  final FeedItemModel commentList;
  final int index;
  final String type;
  final bool showBubble;
  final bool showTopic;
  final bool showNip;
  final bool showToolbox;
  final bool enableReplyButton;
  final String readDotStatus;
  final void Function(dynamic)? onLongPressCallback;
  final void Function()? onResend;
  final void Function()? onForward;
  final void Function()? onShare;
  final void Function()? onEdit;
  final void Function()? onCopy;
  final void Function()? onDelete;
  final void Function(Media media) onPdfClicked;
  final void Function(List<Media> medias, List<FeedItemModel> feeds, int index)
      onImageClicked;

  CommentBubble({
    required this.commentWithDetails,
    required this.commentList,
    required this.index,
    this.type = 'self', // self or other
    this.showBubble = true,
    this.showTopic = true,
    this.showNip = true,
    this.showToolbox = false,
    this.enableReplyButton = true,
    this.readDotStatus = 'read',
    this.onLongPressCallback,
    this.onResend,
    this.onForward,
    this.onShare,
    this.onEdit,
    this.onCopy,
    this.onDelete,
    required this.onPdfClicked,
    required this.onImageClicked,
    Key? key,
  }) : super(key: key);

  @override
  _CommentBubbleState createState() => _CommentBubbleState();
}

class _CommentBubbleState extends State<CommentBubble> {
  final AppViewModel appViewModel = locator<AppViewModel>();
  final database = locator<AppDatabase>();

  List<MediaInfoArguments> mediaArgsList = [];
  List<MediaItemModel>? imageList = [];
  List<MediaItemModel>? docList = [];

  ExpandableController additionalController = ExpandableController();

  @override
  void initState() {
    super.initState();

    imageList = widget.commentList.mediaList
        ?.where((e) => e.fileExtension != 'pdf')
        .toList();
    docList = widget.commentList.mediaList
        ?.where((e) => e.fileExtension == 'pdf')
        .toList();
    mediaArgsList = imageList
            ?.map((m) => MediaInfoArguments(
                mediaPath: m.url ?? '',
                thumbnailPath: m.thumbnailPath ?? '',
                mediaDescription:
                    widget.commentWithDetails.taskDetails?.taskDescription,
                mediaType: m.fileExtension ?? '',
                datetime: widget.commentWithDetails.commentList?[widget.index]
                    .postDateTime))
            .toList() ??
        [];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: widget.showToolbox
          ? EdgeInsets.only(
              left: 20.0,
              right: 20.0,
            )
          : null,
      child: Stack(
        children: [
          GestureDetector(
            onLongPress: () {
              RenderObject object = context.findRenderObject()!;
              var translation = object.getTransformTo(null).getTranslation();
              if (widget.onLongPressCallback != null)
                widget.onLongPressCallback!(translation);
            },
            child: Container(
              margin: EdgeInsets.only(
                  bottom: (widget.showToolbox && widget.type == 'other')
                      ? 16.0
                      : 0.0),
              child: widget.type == 'self'
                  ? _buildMyBubble(context)
                  : _buildOthersBubble(context),
            ),
          ),
          // Build message toolbox
          if (widget.showToolbox)
            Positioned(
              right: widget.type == 'self' ? 20 : 60,
              child: _buildCommentTools(context),
            ),
        ],
        alignment: Alignment.bottomRight,
      ),
    );
  }

// Build current user's message
  Widget _buildMyBubble(BuildContext context) {
    final FeedItemModel commentDetails = widget.commentList;
    String postTime = formatDatetime(
        datetime: commentDetails.postDateTime, format: 'dd MMM yyyy • hh:mma');
    final String feedDesription = commentDetails.description ?? '';

    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        // if (widget.showToolbox)
        //   GestureDetector(
        //     onTap: widget.onResend,
        //     child: SvgPicture.asset(ic_resend),
        //   ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Bubble(
              stick: true,
              margin: BubbleEdges.only(
                  top: 0.0,
                  bottom: (widget.showToolbox) ? 20.0 : 0.0,
                  right: widget.showNip ? 0.0 : 8.0,
                  left: 26.0),
              padding: BubbleEdges.all(16),
              radius: Radius.circular(12),
              borderColor: yellow,
              borderWidth: 1,
              nip: widget.showNip ? BubbleNip.rightTop : BubbleNip.no,
              color: yellow,
              nipHeight: 12,
              alignment: Alignment.centerRight,
              shadowColor: transparent,
              elevation: 0,
              child: Container(
                constraints: BoxConstraints(
                  minWidth: MediaQuery.of(context).size.width * 0.70,
                  maxWidth: MediaQuery.of(context).size.width * 0.70,
                ),
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      // User title
                      if (widget.showNip)
                        Text('Me',
                            style: boldFontsStyle(fontSize: 14.0, height: 1.8)),
                      // User designation name
                      if (widget.showNip)
                        Text(
                          commentDetails.designationName ?? '',
                          style: regularFontsStyle(fontSize: 12.0, height: 1.6),
                        ),
                      // Spacing
                      SizedBox(height: 16.0),
                      // If content is too long, use expandable widget else use html widget
                      if (commentDetails.description != '')
                        Padding(
                          padding: const EdgeInsets.only(bottom: 16.0),
                          child: ContentWidget(
                              content: feedDesription,
                              readMoreColor: darkerYellow,
                              tagNameColor: darkerYellow,
                              fontSize: 16),
                        ),
                      // Media & PDF list
                      if (imageList!.length > 0)
                        Container(
                          child: imageList!.length > 0
                              ? imageGridView(imageList!)
                              : Container(),
                        ),
                      // doc listing layout
                      ExpandableNotifier(
                        controller: additionalController,
                        child: Expandable(
                            collapsed: Wrap(
                              children: [
                                for (int i = 0;
                                    i < docList!.length && i < 3;
                                    i++)
                                  docBoxLayout(docList![i], i),
                                if (docList!.length > 3)
                                  ExpandableButton(
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          additionalController =
                                              ExpandableController(
                                            initialExpanded: true,
                                          );
                                        });
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(bottom: 12.0),
                                        child: Text(
                                          '${docList!.length - 3} More...',
                                          style: boldFontsStyle(
                                              fontSize: 14.0,
                                              color: darkerYellow),
                                        ),
                                      ),
                                    ),
                                  )
                              ],
                            ),
                            expanded: Wrap(
                              children: [
                                for (int i = 0; i < docList!.length; i++)
                                  docBoxLayout(docList![i], i),
                                ExpandableButton(
                                    child: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      additionalController =
                                          ExpandableController(
                                        initialExpanded: false,
                                      );
                                    });
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 12.0),
                                    child: Text(
                                      'Collapse..',
                                      style: boldFontsStyle(
                                          fontSize: 14.0, color: darkerYellow),
                                    ),
                                  ),
                                )),
                              ],
                            )),
                      ),
                      // Message category topic & time row
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            // Render feed post time
                            _buildTime(postTime, black),
                          ],
                        ),
                      ),
                    ]),
              ),
            ),
            // if (!widget.showToolbox)
            //   Container(
            //     margin: EdgeInsets.only(left: 24, top: 8),
            //     child: Row(
            //       children: [
            //         SvgPicture.asset(
            //           ic_warning,
            //           color: red,
            //           width: 25.0,
            //         ),
            //         SizedBox(width: 10.0),
            //         Text(
            //           'Not Delivered',
            //           style: boldFontsStyle(color: red, fontSize: 15.0, height: 1.27),
            //         )
            //       ],
            //     ),
            //   )
          ],
        ),
      ],
    );
  }

// Build other users' message
  Widget _buildOthersBubble(BuildContext context) {
    final FeedItemModel commentDetails = widget.commentList;
    String postTime = formatDatetime(
        datetime: commentDetails.postDateTime, format: 'dd MMM yyyy • hh:mma');
    final String feedDesription = commentDetails.description ?? '';

    return Container(
      padding: EdgeInsets.only(left: 0, right: 0),
      child: Stack(
        children: [
          // User avatar
          if (widget.showNip && commentDetails.teamMemberAvatar != null)
            Positioned(
              top: 16,
              left: 0,
              child: Avatar(
                url: commentDetails.teamMemberAvatar ?? null,
                width: 26,
                borderColor: grey,
              ),
            ),
          // Message bubble
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Bubble(
                stick: true,
                margin: BubbleEdges.only(
                    top: 0.0,
                    bottom: 0,
                    right: 24.0,
                    left: widget.showNip ? 26.0 : 32),
                padding: BubbleEdges.all(16),
                radius: Radius.circular(12),
                borderColor: grey,
                borderWidth: 0.5,
                color: white,
                nip: widget.showNip ? BubbleNip.leftTop : BubbleNip.no,
                nipHeight: 12,
                alignment: Alignment.centerLeft,
                shadowColor: transparent,
                elevation: 0,
                child: Container(
                  // Set a fix width to bubble size
                  constraints: BoxConstraints(
                    minWidth: MediaQuery.of(context).size.width * 0.70,
                    maxWidth: MediaQuery.of(context).size.width * 0.70,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      // User title
                      if (widget.showNip)
                        Text(
                          (commentDetails.teamMemberName ?? ''),
                          style: boldFontsStyle(fontSize: 14.0, height: 1.8),
                        ),
                      // User designation name
                      if (widget.showNip)
                        Text(
                          commentDetails.designationName ?? '',
                          style: regularFontsStyle(
                              color: grey, fontSize: 12.0, height: 1.6),
                        ),
                      SizedBox(height: 10),
                      // If content is too long, use expandable widget else use html widget
                      if (feedDesription != '')
                        Padding(
                          padding: const EdgeInsets.only(bottom: 16.0),
                          child: ContentWidget(
                              content: feedDesription,
                              readMoreColor: darkYellow,
                              tagNameColor: darkYellow,
                              fontSize: 16.0),
                        ),
                      // Media & PDF list
                      if (imageList!.length > 0)
                        Container(
                          child: imageList!.length > 0
                              ? imageGridView(imageList!)
                              : Container(),
                        ),
                      // doc listing layout
                      ExpandableNotifier(
                        controller: additionalController,
                        child: Expandable(
                            collapsed: Wrap(
                              children: [
                                for (int i = 0;
                                    i < docList!.length && i < 3;
                                    i++)
                                  docBoxLayout(docList![i], i),
                                if (docList!.length > 3)
                                  ExpandableButton(
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          additionalController =
                                              ExpandableController(
                                            initialExpanded: true,
                                          );
                                        });
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(bottom: 12.0),
                                        child: Text(
                                          '${docList!.length - 3} More...',
                                          style: boldFontsStyle(
                                              fontSize: 14.0,
                                              color: darkerYellow),
                                        ),
                                      ),
                                    ),
                                  )
                              ],
                            ),
                            expanded: Wrap(
                              children: [
                                for (int i = 0; i < docList!.length; i++)
                                  docBoxLayout(docList![i], i),
                                ExpandableButton(
                                    child: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      additionalController =
                                          ExpandableController(
                                        initialExpanded: false,
                                      );
                                    });
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 12.0),
                                    child: Text(
                                      'Collapse..',
                                      style: boldFontsStyle(
                                          fontSize: 14.0, color: darkerYellow),
                                    ),
                                  ),
                                )),
                              ],
                            )),
                      ),
                      // Message category topic & time row
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            // Render feed post time
                            _buildTime(postTime, grey),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  // image custom grid view
  Widget imageGridView(List<MediaItemModel> imageList, {int flex = 4}) =>
      Container(
        height: 68.0,
        margin: EdgeInsets.only(bottom: docList!.length == 0 ? 8.0 : 16.0),
        width: (imageList.length == 2)
            ? 144.0
            : (imageList.length == 3)
                ? 216.0
                : (imageList.length >= 4)
                    ? 273.0
                    : 60.0,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: Row(
          children: [
            imageBoxLayout(imageList, 0),
            if (imageList.length > 1) SizedBox(width: 8.0),
            if (imageList.length > 1) imageBoxLayout(imageList, 1),
            if (imageList.length > 2) SizedBox(width: 8.0),
            if (imageList.length > 2) imageBoxLayout(imageList, 2),
            if (imageList.length > 3) SizedBox(width: 8.0),
            if (imageList.length > 3) imageBoxLayout(imageList, 3),
          ],
        ),
      );

  Widget imageBoxLayout(List<MediaItemModel> imageUrl, int index) => Expanded(
        child: Stack(
          children: [
            imageUrl[index].fileExtension == 'mp4' ||
                    imageUrl[index].fileExtension == 'MOV'
                ? Stack(alignment: AlignmentDirectional.center, children: [
                    Container(
                      child: CachedNetworkImage(
                        imageUrl: imageUrl[index].thumbnailPath ?? '',
                        height: double.infinity,
                        width: double.infinity,
                        fit: BoxFit.cover,
                        maxHeightDiskCache: 120,
                        maxWidthDiskCache: 120,
                      ),
                    ),
                    Container(
                      constraints: BoxConstraints(maxHeight: 36.0),
                      child: SvgPicture.asset(
                        ic_play_button,
                      ),
                    ),
                  ])
                : ClipRRect(
                    borderRadius: BorderRadius.circular(6.0),
                    child: CachedNetworkImage(
                      imageUrl: imageUrl[index].url ?? '',
                      height: double.infinity,
                      width: double.infinity,
                      maxHeightDiskCache: 100,
                      maxWidthDiskCache: 100,
                      fit: BoxFit.cover,
                    ),
                  ),
            if (index > 2 && imageUrl.length > 4)
              Opacity(
                opacity: 0.5,
                child: Container(
                  color: black,
                ),
              ),
            if (index > 2 && imageUrl.length > 4)
              Center(
                child: Text(
                  '+${imageUrl.length - 3}',
                  style: boldFontsStyle(
                      fontFamily: platForm,
                      fontSize: 30.0,
                      height: 1.33,
                      color: white,
                      letterSpacing: 0.8),
                ),
              ),
            Material(
              color: transparent,
              child: InkWell(
                // splashColor: grey,
                onTap: () {
                  // go to media viewer
                  goToNextNamedRoute(context, mediaSliderScreen,
                      args: MediaSliderArguments(mediaArgsList, index));
                },
              ),
            ),
          ],
        ),
      );

  Widget docBoxLayout(MediaItemModel file, int index) => Column(
        children: [
          Material(
            color: transparent,
            child: InkWell(
              borderRadius: BorderRadius.circular(500.0),
              onTap: () async {
                // go to pdf viewer
                String? temporaryPath;
                String? savedPath = SpManager.getString(file.url.toString());
                if (savedPath != null) {
                  temporaryPath = savedPath;
                } else {
                  showToastMessage(context,
                      message: 'Downloading', duration: 1);
                  await Api()
                      .downloadFile(file.url ?? '', 'pdf', context)
                      .then((localSavedPath) {
                    temporaryPath = localSavedPath;
                  });
                  await SpManager.putString(
                      file.url.toString(), temporaryPath ?? '');
                  await Future.delayed(Duration(milliseconds: 1500));
                }
                await OpenFilex.open(temporaryPath);
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                decoration: BoxDecoration(
                    color: transparent,
                    border: Border.all(width: 1.0, color: black),
                    borderRadius: BorderRadius.circular(500.0)),
                child: Row(
                  children: [
                    SvgPicture.asset(ic_doc, width: 18.0),
                    SizedBox(width: 8.0),
                    Expanded(
                      child: Text(
                        file.url!.substring(file.url!.lastIndexOf('/') + 1),
                        style: regularFontsStyle(fontSize: 14.0, color: black),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: index == docList!.length - 1 ? 8.0 : 8.0),
        ],
      );

  // Feed post time
  Widget _buildTime(String time, Color color) {
    return Text(
      time,
      style: regularFontsStyle(color: color, fontSize: 12.0, height: 1.33),
    );
  }

  // Message toolbox
  Widget _buildCommentTools(BuildContext context) {
    return RowSuper(
      innerDistance: 8,
      children: [
        GestureDetector(
          onTap: widget.onForward,
          child: SvgPicture.asset(ic_toolbox_forward, width: 36),
        ),
        GestureDetector(
          onTap: widget.onShare,
          child: SvgPicture.asset(ic_toolbox_share, width: 36),
        ),
        if (widget.type == 'self')
          GestureDetector(
            onTap: widget.onEdit,
            child: SvgPicture.asset(ic_toolbox_edit, width: 36),
          ),
        GestureDetector(
          onTap: widget.onCopy,
          child: SvgPicture.asset(ic_toolbox_copy, width: 36),
        ),
        if (widget.type == 'self')
          GestureDetector(
            onTap: widget.onDelete,
            child: SvgPicture.asset(ic_toolbox_delete, width: 36),
          ),
      ],
    );
  }
}
