import 'package:flutter/material.dart';

import 'package:sources/constants/app_colors.dart';

class TopicPill extends StatelessWidget {
  final String text;
  final double? fontSize;
  final FontWeight? fontWeight;
  final Color color;
  final Color? backgroundColor;
  final Color borderColor;
  final EdgeInsets? margin;
  final EdgeInsets? padding;
  final VoidCallback? onTap;

  const TopicPill({
    required this.text, 
    this.fontSize,
    this.fontWeight,
    required this.color,
    this.backgroundColor,
    required this.borderColor,
    this.margin,
    this.padding,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (this.onTap != null) {
      return GestureDetector(
        behavior: HitTestBehavior.opaque, // Make empty space clickable
        onTap: this.onTap,
        child: _buildPill(context)
      );
    }
    else {
      return _buildPill(context);
    }
  }

  Widget _buildPill(BuildContext context) {
    return Container(
      margin: this.margin != null ? this.margin : EdgeInsets.symmetric(horizontal: 6),
      padding: this.padding != null ? this.padding : EdgeInsets.symmetric(horizontal: 8, vertical: 4),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: this.backgroundColor != null ? this.backgroundColor : white,
        border: Border.all(
          color: this.borderColor,
        ),
        borderRadius: BorderRadius.circular(20)
      ),
      child: Text(
        this.text,
        style: TextStyle(
          color: this.color,
          fontSize: this.fontSize != null ? this.fontSize : 12,
          fontWeight: this.fontWeight != null ? this.fontWeight : FontWeight.w400,
        ),
      ),
    );
  }
}