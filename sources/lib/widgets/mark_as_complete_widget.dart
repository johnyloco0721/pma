import 'dart:io';

import 'package:deleteable_tile/deleteable_tile.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:open_filex/open_filex.dart';

import 'package:sources/args/media_slider_args.dart';
import 'package:path/path.dart' as p;
import 'package:sources/constants/app_box_shadow.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/models/attach_model.dart';
import 'package:sources/models/config_model.dart';
import 'package:sources/models/profile_model.dart';
import 'package:sources/models/task_details_model.dart';
import 'package:sources/models/task_mark_complete_model.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/app_service.dart';
import 'package:sources/services/file_upload_service.dart';
import 'package:sources/services/task_services.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/error_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/utils/size_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/widgets/comment_box_widget.dart';
import 'package:sources/widgets/html_widget/html_stylist.dart';
import 'package:sources/widgets/video_thumbnail_widget.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:validators/validators.dart';
import 'package:video_compress/video_compress.dart';

class MarkAsCompleteForm extends StatefulWidget {
  final bool isFileRequire;
  final bool isSubtaskForm;
  final TaskDetailsModel taskInfo;
  final SubtaskDetailsModel? subtaskInfo;
  final bool imageUpdateCA;
  final bool autoUpdateCA;
  final VoidCallback? callback;
  final ValueChanged? onMarkCompleted;
  final bool isOrder;

  MarkAsCompleteForm({
    this.isFileRequire = false,
    this.isSubtaskForm = false,
    required this.taskInfo,
    this.subtaskInfo,
    this.imageUpdateCA = false,
    this.autoUpdateCA = false,
    this.callback,
    this.onMarkCompleted,
    this.isOrder = false,
    Key? key,
  }) : super(key: key);

  @override
  _MarkAsCompleteFormState createState() => _MarkAsCompleteFormState();
}

class _MarkAsCompleteFormState extends State<MarkAsCompleteForm> {
  TaskService _taskService = TaskService();
  FileUploadService _fileUploadService = FileUploadService();
  final _formKey = GlobalKey<FormState>();
  final _picker = ImagePicker();
  TextEditingController remarkInputController = TextEditingController();
  TextEditingController dropboxInputController = TextEditingController();
  late TextEditingController invoiceDateController = TextEditingController();
  String handoverActualDate = "";
  ValueNotifier<List<AttachModel>> docAttachments = ValueNotifier([]);
  ValueNotifier<List<AttachModel>> mediaAttachments = ValueNotifier([]);
  List<MediaInfoArguments> mediaArgsList = [];
  bool fileRequireValidation = true;

  bool mediaUploading = false;
  bool uploading = false;
  bool completed = false;
  int onlyClickOnce = 0;

  TaskDetailsModel? completedTaskInfo;
  SubtaskDetailsModel? completedSubtaskInfo = SubtaskDetailsModel();
  ProfileDetails loggedUserInfo = SpManager.getUserdata();
  ConfigDetails? config;

  @override
  void initState() {
    super.initState();
    config = SpManager.getConfigData();
    invoiceDateController.text = DateFormat('d MMM yyyy').format(DateTime.now());
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          padding: EdgeInsets.all(24.0),
          margin: EdgeInsets.only(bottom: 24.0),
          decoration: BoxDecoration(
              color: white,
              borderRadius: BorderRadius.circular(12.0),
              boxShadow: app_box_shadow),
          child: uploading ? uploadProgressLayout() : formLayout(),
        ),
      ],
    );
  }

  // completed layout
  Widget uploadProgressLayout() => Center(
    child: Column(
      children: !completed ? [
        Lottie.asset(
          lottie_loading_spinner,
          width: 50.0,
          height: 50.0,
          fit: BoxFit.fill,
        ),
        SizedBox(height: 16.0),
        Text(
          'just a few secs...',
          style: regularFontsStyle(fontSize: 14.0, color: grey),
        ),
      ] : [
        SvgPicture.asset(ic_completed, width: 80.0, color: darkYellow),
        SizedBox(height: 32.0),
        Text(
          'You’ve successfully marked the task as completed!',
          style: boldFontsStyle(fontSize: 16.0, height: 1.5),
          textAlign: TextAlign.center,
        )
      ],
    ),
  );

  // form layout
  Widget formLayout() => Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (widget.isSubtaskForm)
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // subtask's due date, (outline layout)
                  Container(
                      padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                      decoration: BoxDecoration(
                          border: Border.all(color: widget.subtaskInfo?.taskEndDateTime != null ? lightGrey : red),
                          borderRadius: BorderRadius.circular(500)),
                      child: Text(
                        widget.subtaskInfo?.taskEndDateTime != null ? formatDatetime(datetime: widget.subtaskInfo?.taskEndDateTime ?? '') : 'No Due Date',
                        style: widget.subtaskInfo?.taskEndDateTime != null ? boldFontsStyle(fontSize: 14.0, color: grey) : italicFontsStyle(fontSize: 14.0, color: red),
                      )),
                  SizedBox(height: 16.0),
                  // subtask's title
                  // SvgPicture.asset(ic_star, width: 20.0), //add row for display subtask star icon
                  if (widget.subtaskInfo?.isSpecial ?? false)
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.only(right: 8.0, top: 3.0),
                        child: SvgPicture.asset(ic_star, width: 20.0)
                      ), 
                      Flexible(
                        child: Column(
                          children: [
                            Text(
                              widget.subtaskInfo?.taskName ?? '',
                              style: boldFontsStyle(fontSize: 20.0, height: 1.4),
                              maxLines: 3,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  if (!(widget.subtaskInfo?.isSpecial ?? false))
                  Text(
                    widget.subtaskInfo?.taskName ?? '',
                    style: boldFontsStyle(fontSize: 20.0, height: 1.4),
                  ),
                  // SizedBox(height: 8.0),
                  // subtask's description
                  if (widget.subtaskInfo?.taskDescription != '' &&
                      widget.subtaskInfo?.taskDescription != null)
                    Padding(
                      padding: EdgeInsets.only(top: 8.0),
                      child: Text(
                        widget.subtaskInfo?.taskDescription ?? '',
                        style: regularFontsStyle(fontSize: 16.0, height: 1.5),
                      ),
                    ),
                  SizedBox(height: 8.0),
                  //subtask requried files description
                  if(widget.subtaskInfo?.isFileRequiredLabel != '')
                  Row(
                    children: [
                      SvgPicture.asset(ic_attach, width: 18.0),
                      SizedBox(width: 8.0),
                      RichText(
                        text: HTML.toTextSpan(
                          context,
                          widget.subtaskInfo?.isFileRequiredLabel ?? "",
                          defaultTextStyle: regularFontsStyle(fontSize: 14.0, color: grey),
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                  SizedBox(height: 16.0),
                  // subtask's comment outline layout
                  CommentOutlineButton(
                    taskID: widget.subtaskInfo!.taskID!,
                    subtaskInfo: widget.subtaskInfo!,
                    isSubtask: true,
                    callback: widget.callback,
                  ),
                  SizedBox(height: 16.0),
                ],
              ),
            if (!widget.isOrder && !(widget.taskInfo.hideCompletedButton ?? false))
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // remark label
                  Text(
                    'Remarks',
                    style: boldFontsStyle(fontSize: 14.0, color: grey),
                  ),
                  SizedBox(height: 8.0),
                  // remark text field
                  TextFormField(
                    controller: remarkInputController,
                    cursorColor: black,
                    decoration: InputDecoration(
                      hintText: 'Add Remarks',
                      hintStyle: italicFontsStyle(fontSize: 16.0, color: grey, height: 1.5),
                      contentPadding: EdgeInsets.symmetric(vertical: 6, horizontal: 16),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6.0),
                          borderSide: BorderSide(color: lightGrey)),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6.0),
                          borderSide: BorderSide(color: lightGrey)),
                    ),
                  ),
                  SizedBox(height: 16.0),
                  // attachment label
                  if (widget.subtaskInfo?.forInternal?.extraDatesDisplayArr != null)
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 8.0),
                          child: Text(
                            "${widget.subtaskInfo?.forInternal?.extraDatesDisplayArr?.label}*",
                            style: boldFontsStyle(fontSize: 14.0, color: grey),
                          ),
                        ),
                        TextFormField(
                          controller: invoiceDateController,
                          onTap: () async {
                            var dateTimeValue = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime.now().subtract(Duration(days: 365)),
                              lastDate: DateTime.now().add(Duration(days: 3650)),
                              builder: (context, child) {
                                return Theme(
                                  data: ThemeData.light().copyWith(
                                    datePickerTheme: DatePickerThemeData(
                                      backgroundColor: white,
                                      surfaceTintColor: Colors.transparent,
                                      headerForegroundColor: Colors.black54,
                                      dayBackgroundColor: WidgetStateProperty.resolveWith((states) {
                                        if (states.contains(WidgetState.selected)) {
                                          return darkYellow;
                                        }
                                        return white;
                                      }),
                                      todayBackgroundColor: WidgetStateProperty.resolveWith((states) {
                                        if (states.contains(WidgetState.selected)) {
                                          return darkYellow;
                                        }
                                        return white;
                                      }),
                                      todayForegroundColor: WidgetStateProperty.resolveWith((states) {
                                        if (states.contains(WidgetState.selected)) {
                                          return white;
                                        }
                                        return darkYellow;
                                      }),
                                      yearOverlayColor: WidgetStateProperty.resolveWith((states) {
                                        if (states.contains(WidgetState.selected)) {
                                          return darkYellow;
                                        }
                                        return white;
                                      }),
                                      yearForegroundColor: WidgetStateProperty.resolveWith((states) {
                                        if (states.contains(WidgetState.selected)) {
                                          return white;
                                        }
                                        return black;
                                      }),
                                      yearBackgroundColor: WidgetStateProperty.resolveWith((states) {
                                        if (states.contains(WidgetState.selected)) {
                                          return darkYellow;
                                        }
                                        return white;
                                      }),
                                      confirmButtonStyle: ButtonStyle(
                                        backgroundColor: WidgetStateProperty.resolveWith((states) {
                                          return white;
                                        }),
                                        foregroundColor: WidgetStateProperty.resolveWith((states) {
                                          return darkYellow;
                                        }),
                                      ),
                                      cancelButtonStyle: ButtonStyle(
                                        backgroundColor: WidgetStateProperty.resolveWith((states) {
                                          return white;
                                        }),
                                        foregroundColor: WidgetStateProperty.resolveWith((states) {
                                          return grey;
                                        }),
                                      ),
                                    ),
                                  ),
                                  child: child!,
                                );
                              },
                            );
                            if (dateTimeValue != null) {
                              var text = dateTimeValue;
                              invoiceDateController.text = DateFormat('d MMM yyyy').format(text);
                            }
                          },
                          decoration: InputDecoration(
                            hintText: "Select Date",
                            hintStyle: italicFontsStyle(fontSize: 16.0, height: 1.5, color: grey),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: darkYellow),
                              borderRadius: BorderRadius.circular(6.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: lightGrey),
                              borderRadius: BorderRadius.circular(6.0),
                            ),
                            border: OutlineInputBorder(),
                            contentPadding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                            fillColor: white,
                            filled: true,
                            suffix: Padding(
                              padding: EdgeInsets.only(left: 10),
                              child: Material(
                              color: transparent,
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    invoiceDateController.text = '';
                                  });
                                },
                                child: Container(
                                  transform: Matrix4.translationValues(2, 3, 5),
                                  child: SvgPicture.asset(ic_close, color: black)
                                ),
                                ),
                              ),
                            ),
                          ),
                          style: regularFontsStyle(fontSize: 16.0, height: 1.5),
                          onChanged: (value) {
                            invoiceDateController.text = value;
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return '*Please fill up';
                            }
                            return null;
                          },
                          onSaved: (val) => {},
                        ),
                        SizedBox(height: 16.0),
                      ],
                    ),
                  if (widget.taskInfo.specialTaskType == "edited-photo")
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Link*',
                          style: boldFontsStyle(fontSize: 14.0, color: grey),
                        ),
                        SizedBox(height: 8.0),
                        // dropbox link text field
                        TextFormField(
                          controller: dropboxInputController,
                          cursorColor: black,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return '*This is a required field';
                            } else if (!isURL(value)) {
                              return '*Invalid dropbox link';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            hintText: 'Add Link',
                            hintStyle: italicFontsStyle(fontSize: 16.0, height: 1.5, color: grey),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: darkYellow),
                              borderRadius: BorderRadius.circular(6.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: lightGrey),
                              borderRadius: BorderRadius.circular(6.0),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: red),
                              borderRadius: BorderRadius.circular(6.0),
                            ),
                            border: OutlineInputBorder(),
                            contentPadding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                            fillColor: white,
                            filled: true,
                          ),
                          style: regularFontsStyle(fontSize: 16.0, height: 1.5),
                        ),
                      ],
                    )
                  else
                    if (widget.subtaskInfo?.specialTaskType != "handover-actual-date")
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: 'Photos / Files' + (widget.subtaskInfo?.isFileRequired == 1 ? '*' : ((widget.taskInfo.isFileRequired == 1 && widget.subtaskInfo?.isFileRequired == null) ? '*' : '')),
                                style: boldFontsStyle(fontSize: 14.0, color: grey),
                              ),
                              TextSpan(
                                text: ' (max. 20mb)' + (widget.subtaskInfo?.isFileRequired == 0 ? ' (optional)' : ((widget.taskInfo.isFileRequired == 0 && widget.subtaskInfo?.isFileRequired == null) ? ' (optional)' : '')),
                                style: italicFontsStyle(fontSize: 14.0, color: grey),
                              ),
                            ]
                          )
                        ),
                        SizedBox(height: 4.0),
                        // file listing layout
                        docListingLayout(),
                        // image Listing & upload file widget
                        imageListingLayout(),
                        if (!fileRequireValidation)
                          Container(
                            margin: EdgeInsets.only(top: 8.0),
                            child: Text(
                              '*Please attach photos / files.',
                              style: regularFontsStyle(fontSize: 12.0, height: 1.33, color: red),
                            ),
                          ),
                      ],
                    ),
                  SizedBox(height: 32.0),
                  // mark as complete button
                  Opacity(
                    opacity: (mediaUploading || onlyClickOnce >= 1) ? 0.5 : 1.0,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: yellow,
                        padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                      ),
                      onPressed: () {
                        if (!mediaUploading) {
                          markAsCompleteValidate();
                        }
                      },
                      child: Text(
                        "Mark As Completed",
                        style: boldFontsStyle(fontSize: 16.0, height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            if (widget.isOrder)
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                      color: whiteGrey,
                      border: Border.all(color: shimmerGrey),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    child: Row(
                      children: [
                        Image.asset(
                          img_mark_orders,
                          width: 70.0,
                          height: 70.0,
                          fit: BoxFit.fill,
                        ),
                        SizedBox(width: 16.0),
                        Expanded(
                          child: Text(
                            'The task can only be marked completed on the orders page. Use your laptop / desktop for a better experience',
                            style: boldFontsStyle(fontSize: 14.0),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 32.0),
                  OutlinedButton(
                    onPressed: () {
                      launch('https://mog-ordering.wearelibrary.com/order/${widget.taskInfo.projectID}/shortlist');
                    },
                    style: OutlinedButton.styleFrom(
                      side: BorderSide(),
                      padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          'Go To Orders Page',
                          style: boldFontsStyle(fontSize: 16.0),
                        ),
                        SizedBox(width: 8.0),
                        SvgPicture.asset(ic_enter, width: 24.0)
                      ],
                    ),
                  )
                ],
              )
          ],
        ),
      );

  Widget docListingLayout() => ValueListenableBuilder(
      valueListenable: docAttachments,
      builder: (BuildContext context, List mediaFiles, Widget? child) {
        return Column(
          children: [
            for (var i = 0; i < docAttachments.value.length; i++)
              Stack(
                alignment: AlignmentDirectional.center,
                children: [
                  GestureDetector(
                    onTap: () async {
                      // go to pdf viewer
                      String? temporaryPath;
                      String? savedPath = SpManager.getString(docAttachments.value[i].imagePath.toString());
                      if (savedPath != null && await File(savedPath).exists()) {
                        temporaryPath = savedPath;
                      } else {
                        showToastMessage(context, message: 'Downloading', duration: 1);
                        await Api().downloadFile(docAttachments.value[i].imagePath ?? '', 'pdf', context).then((localSavedPath) {
                          temporaryPath = localSavedPath;
                        });
                        await SpManager.putString(docAttachments.value[i].imagePath.toString(), temporaryPath ?? '');
                        await Future.delayed(Duration(milliseconds: 1500));
                      }
                      await OpenFilex.open(temporaryPath);
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 10.0, top: 4.0, bottom: 4.0),
                      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
                      decoration: BoxDecoration(
                          border: Border.all(color: darkGrey),
                          borderRadius: BorderRadius.circular(500.0)
                          ),
                      child: Row(
                        children: [
                          SvgPicture.asset(ic_doc, width: 18.0),
                          SizedBox(width: 8.0),
                          Expanded(
                            child: Text(
                              docAttachments.value[i].imagePath?.substring((docAttachments.value[i].imagePath?.lastIndexOf("/") ?? 0) + 1) ?? '',
                              style: boldFontsStyle(fontSize: 14.0, color: darkGrey),
                              overflow: TextOverflow.clip,
                            ),
                          )
                        ]
                      ),
                    ),
                  ),
                  Positioned(
                    right: 0.0,
                    child: GestureDetector(
                      onTap: () {
                        // remove the doc
                        docAttachments.value.removeAt(i);
                        docAttachments.notifyListeners();
                      },
                      child: SvgPicture.asset(ic_media_delete, width: 26),
                    ),
                  ),
                ],
              ),
          ],
        );
      });

  Widget imageListingLayout() => ValueListenableBuilder(
      valueListenable: mediaAttachments,
      builder: (BuildContext context, List mediaFiles, Widget? child) {
        return Wrap(
          children: [
            for (var i = 0; i < mediaAttachments.value.length; i++)
              Container(
                width: 80.0,
                child: Deleteable(
                    key: ValueKey(mediaAttachments.value[i]),
                    showExitAnimation: false,
                    showEntryAnimation: false,
                    builder: (context, delete) {
                      return Stack(
                        children: [
                          GestureDetector(
                            onTap: () {
                              mediaArgsList = mediaAttachments.value.map((m) => MediaInfoArguments(
                                mediaPath: m.imagePath ?? '',
                                mediaDescription: '',
                                mediaType: m.imagePath?.substring((m.imagePath?.lastIndexOf(".") ?? 0) + 1) ?? '',
                                datetime: ''
                              )).toList();

                              goToNextNamedRoute(context, mediaSliderScreen, args: MediaSliderArguments(mediaArgsList, i));
                            },
                            child: Container(
                              padding: EdgeInsets.only(top: 8.0, right: 16.0, bottom: 8.0),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(6.0),
                                child: mediaAttachments.value[i].imagePath?.substring((mediaAttachments.value[i].imagePath?.lastIndexOf(".") ?? 0) + 1) == 'mp4' ? VideoThumbnailWidget(
                                  videoPath: mediaAttachments.value[i].imagePath ?? '',
                                  height: 64,
                                  width: 64,
                                ) : Image.network(
                                  mediaAttachments.value[i].imagePath ?? '',
                                  height: 64,
                                  width: 64,
                                  fit: BoxFit.cover,
                                  cacheHeight: SizeUtils.height(context, 30).toInt(),
                                  cacheWidth: SizeUtils.width(context, 60).toInt(),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            top: 0,
                            right: 4,
                            child: GestureDetector(
                              onTap: () async {
                                await delete();
                                // remove the image
                                setState(() {
                                  mediaAttachments.value.removeAt(i);
                                });
                              },
                              child: SvgPicture.asset(ic_media_delete, width: 26),
                            ),
                          ),
                        ],
                      );
                    }),
              ),
            // upload file widget
            GestureDetector(
              onTap: () {
                if (!mediaUploading) {
                  mediaSelectionDialog(
                    context,
                    cameraEnabled: true,
                    cameraClick: () {
                      getCamera();
                    },
                    photoEnabled: true,
                    photoClick: () {
                      getImage();
                    },
                    takeVideoEnabled: true,
                    takeVideoClick: () {
                      takeVideo();
                    },
                    videoEnabled: true,
                    videoClick: () {
                      getVideo();
                    },
                    fileEnabled: true,
                    fileClick: () {
                      getPDF();
                    },
                  );
                  setState(() {
                    fileRequireValidation = true;
                  });
                }
              },
              child: Container(
                width: 64.0,
                height: 64.0,
                margin: EdgeInsets.only(top: 8.0),
                child: DottedBorder(
                  padding: !mediaUploading ? EdgeInsets.all(20) : EdgeInsets.symmetric(horizontal: 8.0),
                  radius: Radius.circular(6.0),
                  color: fileRequireValidation ? lightGrey : red,
                  strokeWidth: 1.5,
                  dashPattern: [8, 7],
                  borderType: BorderType.RRect,
                  child: !mediaUploading
                    ? SvgPicture.asset(ic_attach, color: fileRequireValidation ? grey : red, width: 24.0)
                    : Center(
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
                        decoration: BoxDecoration(
                            color: white,
                            borderRadius: BorderRadius.circular(6.0),
                            boxShadow: app_box_shadow),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              'Uploading...',
                              style: boldFontsStyle(fontSize: 7.0)
                            ),
                            SizedBox(height: 2.0),
                            LinearPercentIndicator(
                              lineHeight: 8.0,
                              animationDuration: 1500,
                              animation: true,
                              animateFromLastPercent: true,
                              percent: 85 / 100,
                              progressColor: yellow,
                              backgroundColor: lightGrey,
                              padding: EdgeInsets.zero,
                              barRadius: Radius.circular(4.0),
                            ),
                          ],
                        ),
                      ),
                    ),
                ),
              ),
            )
          ],
        );
      }
    );

  // Open camera
  Future getCamera() async {
    try {
      await AppService.getConfig().then((response) async {
        final pickedFile = await _picker.pickImage(source: ImageSource.camera, imageQuality: response.data['response']['taskMediaUploadQuality']);
        setState(() {
          mediaUploading = true;
        });
        if (pickedFile != null) {
          await _fileUploadService.taskFileUpload(context, pickedFile.path).then((response) {
            setState(() {
              mediaAttachments.value.add(AttachModel(
                taskImageID: response['taskImageID'],
                imagePath: response['url'],
                imageName: response['filename'],
                selectedStatus: 1
              ));
            });
          });
        }
        setState(() {
          mediaUploading = false;
        });
      });
    } on PlatformException catch (e) {
      ErrorUtils.handlePlatformError(context, e);
    } catch (e) {
      setState(() {
        mediaUploading = false;
      });
      showError(context, 'Unexpected error, please try again.');
    }
    mediaAttachments.notifyListeners();
  }

  // Image picker
  Future getImage() async {
    var pickedFile;
    bool pickSingleImage = false;
    if (Platform.isIOS) {
      var iosData = await DeviceInfoPlugin().iosInfo;
      if ((iosData.systemVersion ?? '0.0').compareTo('14.0') < 0) {
        pickSingleImage = true;
      }
    }
    try {
      await AppService.getConfig().then((response) async {
        setState(() {
          mediaUploading = true;
        });
        if (pickSingleImage) {
          await _picker.pickImage(source: ImageSource.gallery, imageQuality: response.data['response']['taskMediaUploadQuality']).then((value) {
            pickedFile = [];
            pickedFile.add(value);
          });
        } else {
          pickedFile = await _picker.pickMultiImage(imageQuality: response.data['response']['taskMediaUploadQuality']);
        }
        if (pickedFile != null) {
          await Future.forEach(pickedFile, (XFile file) async {
            await _fileUploadService.taskFileUpload(context, file.path).then((response) {
              setState(() {
                mediaAttachments.value.add(AttachModel(
                  taskImageID: response['taskImageID'],
                  imagePath: response['url'],
                  imageName: response['filename'],
                  selectedStatus: 1
                ));
              });
            });
          });
        }
        setState(() {
          mediaUploading = false;
        });
      });
    } on PlatformException catch (e) {
      ErrorUtils.handlePlatformError(context, e);
    } catch (e) {
      setState(() {
        mediaUploading = false;
      });
      showError(context, 'Unexpected error, please try again.');
    }
    mediaAttachments.notifyListeners();
  }

  // Take Video via Camera
  Future takeVideo() async {
    try {
      final pickedFile = await _picker.pickVideo(source: ImageSource.camera);
      setState(() {
        mediaUploading = true;
      });
      if (pickedFile != null) {
        await VideoCompress.setLogLevel(0);
        File renamedFile;
        if (Platform.isIOS) {
          String fileExtension = p.extension(pickedFile.path);
          String uniqueIdentifier = DateTime.now().millisecondsSinceEpoch.toString();
          String shortenedFileName = p.basenameWithoutExtension(pickedFile.path).substring(0, 10) + '_' + uniqueIdentifier + fileExtension;
          String shortenedFilePath = p.join(p.dirname(pickedFile.path), shortenedFileName);
          renamedFile = File(pickedFile.path).renameSync(shortenedFilePath);
        } else {
          renamedFile = File(pickedFile.path);
        }
        final MediaInfo? comprossedVideo = await VideoCompress.compressVideo(
          renamedFile.path,
          quality: VideoQuality.Res1280x720Quality,
          deleteOrigin: true,
          includeAudio: true,
        );
        if (!mediaAttachments.value.contains(comprossedVideo!.path)) {
          await _fileUploadService.taskFileUpload(context, comprossedVideo.path).then((response) {
            setState(() {
              mediaAttachments.value.add(AttachModel(
                taskImageID: response['taskImageID'],
                imagePath: response['url'],
                imageName: response['filename'],
                selectedStatus: 1
              ));
            });
          });
        }
      }
      setState(() {
        mediaUploading = false;
      });
    } on PlatformException catch (e) {
      ErrorUtils.handlePlatformError(context, e);
    } catch (e) {
      setState(() {
        mediaUploading = false;
      });
      showError(context, 'Unexpected error, please try again.');
    }
    mediaAttachments.notifyListeners();
  }

  // Video picker
  Future getVideo() async {
    try {
      final pickedFile = await _picker.pickVideo(source: ImageSource.gallery);
      setState(() {
        mediaUploading = true;
      });
      if (pickedFile != null) {
        await VideoCompress.setLogLevel(0);
        File renamedFile;
        if (Platform.isIOS) {
          String fileExtension = p.extension(pickedFile.path);
          String uniqueIdentifier = DateTime.now().millisecondsSinceEpoch.toString();
          String shortenedFileName = p.basenameWithoutExtension(pickedFile.path).substring(0, 10) + '_' + uniqueIdentifier + fileExtension;
          String shortenedFilePath = p.join(p.dirname(pickedFile.path), shortenedFileName);
          renamedFile = File(pickedFile.path).renameSync(shortenedFilePath);
        } else {
          renamedFile = File(pickedFile.path);
        }
        
        final MediaInfo? comprossedVideo = await VideoCompress.compressVideo(
          renamedFile.path,
          quality: VideoQuality.Res1280x720Quality,
          deleteOrigin: true,
          includeAudio: true,
        );
        if (!mediaAttachments.value.contains(comprossedVideo!.path)) {
          await _fileUploadService.taskFileUpload(context, comprossedVideo.path).then((response) {
            setState(() {
              mediaAttachments.value.add(AttachModel(
                taskImageID: response['taskImageID'],
                imagePath: response['url'],
                imageName: response['filename'],
                selectedStatus: 1
              ));
            });
          });
        }
      }
      setState(() {
        mediaUploading = false;
      });
    } on PlatformException catch (e) {
      ErrorUtils.handlePlatformError(context, e);
    } catch (e) {
      setState(() {
        mediaUploading = false;
      });
      showError(context, 'Unexpected error, please try again.');
    }
    mediaAttachments.notifyListeners();
  }

  // PDF file picker
  Future getPDF() async {
    try {
      setState(() {
        mediaUploading = true;
      });
      FilePickerResult? pickedFile = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowedExtensions: ['pdf'],
        allowMultiple: true
      );
      if (pickedFile != null) {
        await Future.forEach(
          pickedFile.files, (PlatformFile file) async {
          if (file.extension?.toLowerCase() != 'pdf') {
            showError(context, 'Must be PDF');
          } else if (file.size > 20971520) {
            showError(context, 'PDF size must below 20MB');
          } else {
            await _fileUploadService.taskFileUpload(context, file.path).then((response) {
              setState(() {
                docAttachments.value.add(AttachModel(
                  taskImageID: response['taskImageID'],
                  imagePath: response['url'],
                  imageName: response['filename'],
                  selectedStatus: 1
                ));
              });
            });
          }
        });
      }
      setState(() {
        mediaUploading = false;
      });
    } on PlatformException catch (e) {
      ErrorUtils.handlePlatformError(context, e);
    } catch (e) {
      setState(() {
        mediaUploading = false;
      });
      showError(context, 'Unexpected error, please try again.');
    }
    docAttachments.notifyListeners();
  }

  markAsCompleteValidate() {
    bool isSpecialTask = widget.taskInfo.specialTaskType == "edited-photo" || widget.subtaskInfo?.specialTaskType == "handover-actual-date";
    bool attachmentError = mediaAttachments.value.length == 0 && docAttachments.value.length == 0 && !isSpecialTask && (widget.subtaskInfo?.isFileRequired == 1 || (widget.taskInfo.isFileRequired == 1 && widget.subtaskInfo?.isFileRequired == null));
    bool othersError = (!_formKey.currentState!.validate() && isSpecialTask);
    if (attachmentError || othersError) {
      setState(() {
        fileRequireValidation = false;
      });
    } else {
      if (widget.taskInfo.specialTaskType == "schedule")
        confirmDialog(
          context,
          icon: img_tmog_logo,
          contentMessage: 'Upon completion of this task, the timeline of the milestones will be pushed to Client App. Are you sure?',
          rejectText: 'Cancel',
          confirmText: 'Confirm',
          onConfirm: () { 
            markAsComplete();
          }
        );
      else
        markAsComplete();
    }
  }

  markAsComplete() async {
    setState(() {
      uploading = true;
    });
    List<int> uploadedAttachmentIDs = [];

    docAttachments.value.forEach((file) {
      if (file.taskImageID != null) {
        uploadedAttachmentIDs.add(file.taskImageID!);
      }
    });

    mediaAttachments.value.forEach((image) {
      if (image.taskImageID != null) {
        uploadedAttachmentIDs.add(image.taskImageID!);
      }
    });

    TaskMarkCompleteModel data = TaskMarkCompleteModel(
      projectID: widget.taskInfo.projectID ?? 0,
      taskID: widget.isSubtaskForm ? widget.subtaskInfo?.taskID ?? 0 : widget.taskInfo.taskID ?? 0,
      taskImageID: uploadedAttachmentIDs,
      remark: remarkInputController.text,
      dropboxLink: dropboxInputController.text,
      actualHandoverDate: (widget.subtaskInfo?.specialTaskType == "handover-actual-date") ? invoiceDateController.text : "",
      invoiceDate: (widget.subtaskInfo?.specialTaskType != "handover-actual-date") ? invoiceDateController.text : "",
    );

    try {
      setState(() {
        onlyClickOnce = onlyClickOnce + 1;
      });
      if (onlyClickOnce == 1) {
        await _taskService.postMyTaskMarkComplete(data).then((response) async {
          widget.callback!();
          // setState(() {
          //   completed = true;
          //   if (widget.isSubtaskForm) {
          //     // 16 Dec 2022 (Steph): They accept the completed info direct replace the form then the cronJob with rearrange the taskList
          //     widget.onMarkCompleted!(SubtaskDetailsModel.fromJson(response['response']));
          //   } else {
          //     widget.onMarkCompleted!(TaskDetailsModel.fromJson(response['response']));
          //   }
          // });
          // goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: widget.taskInfo.taskID));
        }).timeout(Duration(seconds: 60), onTimeout: () {
          setState(() {
            onlyClickOnce = 0;
          });
        });
        widget.callback!();
        // goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: widget.taskInfo.taskID));
      }
    } catch (e) {
      setState(() {
        uploading = false;
      });
      // showError(context, 'Unexpected error, please try again.');
    } finally {
      setState(() {
        onlyClickOnce = 0;
      });
    }
  }
}
