import 'dart:math';

import 'package:flutter/material.dart';

class DragScrollListenerView extends StatefulWidget {
  final ScrollController scrollController;
  final Widget child;
  final bool onDragging;

  const DragScrollListenerView({
    Key? key, 
    required this.scrollController, 
    required this.child, 
    this.onDragging = false,
  }) : super(key: key);

  @override
  _DragScrollListenerViewState createState() => _DragScrollListenerViewState();
}

class _DragScrollListenerViewState extends State<DragScrollListenerView> {
  bool _scrolling = false;
  bool _pointerDown = false;
  double? _pointerYPosition;
  double? _pointerXPosition;

  @override
  Widget build(BuildContext context) {
    return Listener(
      onPointerMove: _onPointerMove,
      onPointerDown: _onPointerDown,
      onPointerUp: _onPointerUp,
      child: widget.child,
    );
  }

  _onPointerMove(PointerMoveEvent event) {
    if (_pointerDown && widget.onDragging) {
      _pointerYPosition = event.position.dy;
      _pointerXPosition = event.position.dx;

      _scrollList();
    }
  }

  _onPointerDown(PointerDownEvent event) {
    _pointerDown = true;
    _pointerYPosition = event.position.dy;
    _pointerXPosition = event.position.dx;
  }

  _onPointerUp(PointerUpEvent event) {
    _pointerDown = false;
  }

  final int _duration = 30; // in ms
  final int _scrollAreaSize = 50;
  final double _overDragMax = 20.0;
  final double _overDragCoefficient = 3.3;

  _scrollList() async {
    if (!_scrolling &&
        _pointerDown &&
        _pointerYPosition != null &&
        _pointerXPosition != null) {
      double? newOffset;

      RenderBox rb = context.findRenderObject()! as RenderBox;
      Size size = rb.size;

      Offset topLeftOffset = localToGlobal(rb, Offset.zero);
      Offset bottomRightOffset = localToGlobal(rb, size.bottomRight(Offset.zero));

      newOffset = _scrollListVertical(topLeftOffset, bottomRightOffset);

      if (newOffset != null) {
        _scrolling = true;
          await widget.scrollController.animateTo(newOffset, duration: Duration(milliseconds: _duration), curve: Curves.linear);
        _scrolling = false;
        if (_pointerDown) _scrollList();
      }
    }
  }

  Offset localToGlobal(RenderObject object, Offset point, {RenderObject? ancestor}) {
    return MatrixUtils.transformPoint(object.getTransformTo(ancestor), point);
  }

  double? _scrollListVertical(Offset topLeftOffset, Offset bottomRightOffset) {
    double top = topLeftOffset.dy;
    double bottom = bottomRightOffset.dy;
    double? newOffset;

    var pointerYPosition = _pointerYPosition;
    var scrollController = widget.scrollController;
    if (pointerYPosition != null) {
      if (pointerYPosition < (top + _scrollAreaSize) &&
          scrollController.position.pixels > scrollController.position.minScrollExtent) {
        final overDrag = max((top + _scrollAreaSize) - pointerYPosition, _overDragMax);
        newOffset = max(scrollController.position.minScrollExtent,
          scrollController.position.pixels - overDrag / _overDragCoefficient);
      } else if (pointerYPosition > (bottom - _scrollAreaSize) &&
          scrollController.position.pixels < scrollController.position.maxScrollExtent) {
        final overDrag = max(pointerYPosition - (bottom - _scrollAreaSize), _overDragMax);
        newOffset = min(scrollController.position.maxScrollExtent,
          scrollController.position.pixels + overDrag / _overDragCoefficient);
      }
    }

    return newOffset;
  }
}