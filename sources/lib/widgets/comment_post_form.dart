import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path/path.dart' as p;
import 'package:sources/args/media_slider_args.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/models/config_model.dart';
import 'package:sources/services/app_service.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:video_compress/video_compress.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/error_utils.dart';
import 'package:sources/utils/size_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/widgets/custom_mentions/custom_mention_view.dart';
import 'package:sources/widgets/custom_mentions/mention_model.dart';
import 'package:sources/widgets/video_thumbnail_widget.dart';

import '../view_models/comment_view_model.dart';

class CommentPostForm extends StatefulWidget {
  final GlobalKey<CustomMentionsState> commentTextfieldKey;
  final bool showTopicButton;
  final bool isForward;
  final VoidCallback submitOnTap;
  final VoidCallback updateOnTap;
  final ValueChanged onTextChanged;
  final bool editMode;
  final List<Map<String, dynamic>>? teamMembers;
  final Key? key;
  final FocusNode focusNode;

  const CommentPostForm({
    required this.commentTextfieldKey,
    this.showTopicButton = true,
    this.isForward = false,
    required this.submitOnTap,
    required this.updateOnTap,
    required this.onTextChanged,
    this.editMode = false,
    this.teamMembers = const [],
    this.key,
    required this.focusNode,
  }) : super(key: key);

  @override
  _CommentPostFormState createState() => _CommentPostFormState();
}

class _CommentPostFormState extends State<CommentPostForm> {
  final _picker = ImagePicker();

  final database = locator<AppDatabase>();
  final commentViewModel = locator<CommentViewModel>();
  ConfigDetails? config;

  // Subscription? _subscription;

  @override
  void initState() {
    super.initState();
    config = SpManager.getConfigData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: ValueListenableBuilder(
        valueListenable: commentViewModel.isPostFeedLoading, 
        child: _buildFormLoadingIndicator(),
        builder: (BuildContext context, bool isLoading, Widget? child) {
          return isLoading ? 
          // Show loading indicator
          _buildFormLoadingIndicator() : 
          // Show form textfield
          Container(
            color: darkGrey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Form field & buttons
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(top: 18, bottom: 18, left: 5, right: 5),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Add button
                        if (!widget.isForward)
                        _buildAddMediaButton(),
                        if (widget.isForward)
                        SizedBox(width: 10),
                        // Text input field
                        Flexible(
                          child: _buildTextField(),
                        ),
                        // Submit button
                        if (!widget.editMode)
                        ValueListenableBuilder(
                          valueListenable: commentViewModel.isSending,
                          builder: (BuildContext context, bool sending, Widget? child) {
                            return GestureDetector(
                              behavior: HitTestBehavior.opaque,
                              onTap: () {
                                if (!sending) 
                                  widget.submitOnTap();
                              },
                              child: Opacity(
                                opacity: sending ? 0.5 : 1.0,
                                child: Container(
                                  width: 50,
                                  padding: EdgeInsets.only(top: 13.5, bottom: 13.5),
                                  child: SvgPicture.asset(ic_workplace_chat_send, width: 24.0, color: yellow),
                                ),
                              ),
                            );
                          }
                        ),
                        // Edit save button
                        if (widget.editMode)
                        ValueListenableBuilder(
                          valueListenable: commentViewModel.isSending,
                          builder: (BuildContext context, bool sending, Widget? child) {
                            return GestureDetector(
                              behavior: HitTestBehavior.opaque,
                              onTap: ()  {
                                if (!sending) 
                                  widget.updateOnTap();
                              },
                              child: Opacity(
                                opacity: sending ? 0.5 : 1.0,
                                child: Container(
                                  width: 50,
                                  padding: EdgeInsets.only(top: 13.5, bottom: 13.5),
                                  child: SvgPicture.asset(ic_workplace_chat_tick, width: 24.0, color: yellow),
                                ),
                              )
                            );
                          }
                        ),
                      ],
                    ),
                  ),
                ),
                // Pending to upload media list
                ValueListenableBuilder(
                  valueListenable: commentViewModel.mediaPdfFiles,
                  builder: (BuildContext context, List pdfFiles, Widget? child) {
                    return _buildPdfPreviewSlider(pdfFiles);
                  }
                ),
                ValueListenableBuilder(
                  valueListenable: commentViewModel.mediaImageFiles,
                  builder: (BuildContext context, List mediaFiles, Widget? child) {
                    return _buildMediaPreviewSlider(mediaFiles);
                  }
                ),
              ],
            ),
          );
        }
      ),
    );
  }

  // Feed form loading indicator widget
  Widget _buildFormLoadingIndicator() {
    return Container(
      color: darkGrey,
      height: 85,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Lottie.asset(
            lottie_loading_spinner_white,
            width: 60.0,
            height: 60.0,
            fit: BoxFit.fill,
          ),
        ],
      ),
    );
  }

  // Add media widget
  Widget _buildAddMediaButton() {
    return GestureDetector(
      onTap: () {
        if (commentViewModel.mediaImageFiles.value.length >= 10) {
          showError(context, 'You can select maximum up to 10 images only.');
        }
        else if (commentViewModel.mediaPdfFiles.value.length >= 6) {
          showError(context, 'You can select maximum up to 6 PDF files only.');
        }
        else {
          mediaSelectionDialog(
            context,
            cameraClick: () {
              getCamera();
            },
            photoClick: () {
              getImage();
            },
            takeVideoClick: () {
              takeVideo();
            },
            videoClick: () {
              getVideo();
            },
            fileClick: () {
              getPDF();
            },
          );
        }
      },
      child: Container(
        width: 50,
        padding: EdgeInsets.only(top: 13.5, bottom: 13.5),
        child: SvgPicture.asset(ic_workplace_chat_add, width: 24.0, color: yellow)
      ),
    );
  }

  // Text field widget
  Widget _buildTextField() {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(
        textScaleFactor: MediaQuery.of(context).textScaleFactor > 1.2 ? 1.2 : MediaQuery.of(context).textScaleFactor
      ),
      child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Textfield
            Flexible(
              child: FractionallySizedBox(
                widthFactor: 1.0,
                child: CustomMentions(
                  enabled: !widget.isForward,
                  key: widget.commentTextfieldKey,
                  defaultText: commentViewModel.temporaryTextFieldValue.value,
                  onChanged: widget.onTextChanged,
                  suggestionPosition: CustomSuggestionPosition.Top,
                  maxLines: 3,
                  minLines: 1,
                  focusNode: widget.focusNode,
                  decoration: InputDecoration(
                    hintText: "Reply Here",
                    hintStyle: italicFontsStyle(color: grey, height: 1.32),
                    border: InputBorder.none,
                    fillColor: whiteGrey,
                    contentPadding: EdgeInsets.only(top: 10, bottom: 10),
                  ),
                  style: regularFontsStyle(height: 1.32),
                  suggestionListWidth: SizeUtils.width(context, 100) - (widget.editMode ? 70 : 110),
                  suggestionListHeight: 180,
                  suggestionListDecoration: BoxDecoration(
                    color: whiteGrey,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(12.0), topRight: Radius.circular(12.0)),
                  ),
                  suggestionBorderRadius: BorderRadius.only(topLeft: Radius.circular(12.0), topRight: Radius.circular(12.0)),
                  suggestionListMargin: EdgeInsets.only(bottom: 8),
                  mentions: [
                    CustomMention(
                      trigger: '@',
                      style: regularFontsStyle(color: darkYellow, height: 1.32),
                      data: widget.teamMembers ?? [],
                      matchAll: false,
                      markupBuilder: (trigger, id, value) {
                        // @[teamMemberID]
                        return trigger + "[" + id + "]";
                      },
                      suggestionBuilder: (data) {
                        return Container(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          height: 60,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                child: Text(
                                  data['teamMemberName'] ?? "",
                                  style: regularFontsStyle(fontSize: 15.0, height: 1.27),
                                ),
                              ),
                              Text(
                                data['designation'] ?? "",
                                style: regularFontsStyle(color: grey, fontSize: 12.0, height: 1.25),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ]
                ),
              ),
            ),
          ],
      ),
    );
  }

  // Upload pdf preview slider
  Widget _buildPdfPreviewSlider(List mediaFiles) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: Container(
        margin: EdgeInsets.only(bottom: 10.0),
        child: Row(
          children: [
            if (commentViewModel.isPostFeedLoading.value)
              Opacity(
                opacity: 0.5,
                child: Container(
                  padding: EdgeInsets.only(top: 8.0, right: 28.0, bottom: 8.0, left: 16.0),
                  margin: EdgeInsets.symmetric(horizontal: 5.0),
                  decoration: BoxDecoration(
                    border: Border.all(color: white),
                    borderRadius: BorderRadius.all(Radius.circular(50.0)),
                  ),
                  child: Row(
                    children: [
                      SvgPicture.asset(ic_doc, width: 18.0, color: yellow),
                      SizedBox(width: 8.0),
                      Text(
                        'Uploading...',
                        style: boldFontsStyle(fontSize: 14.0, color: white),
                      ),
                    ],
                  ),
                ),
              ),
            for (var i = 0; i < mediaFiles.length; i++)
              Stack(
                alignment: Alignment.centerRight,
                children: [
                  GestureDetector(
                    onTap: () => OpenFilex.open(commentViewModel.mediaPdfFiles.value[i]),
                    child: Container(
                      padding: EdgeInsets.only(top: 8.0, right: 28.0, bottom: 8.0, left: 16.0),
                      margin: EdgeInsets.symmetric(horizontal: 5.0),
                      decoration: BoxDecoration(
                        border: Border.all(color: white),
                        borderRadius: BorderRadius.all(Radius.circular(50.0)),
                      ),
                      child: Row(
                        children: [
                          SvgPicture.asset(ic_doc, width: 18.0, color: yellow),
                          SizedBox(width: 8.0),
                          Text(
                            commentViewModel.mediaPdfFiles.value[i].substring(commentViewModel.mediaPdfFiles.value[i].lastIndexOf("/") + 1),
                            style: boldFontsStyle(fontSize: 14.0, color: white),
                          ),
                        ],
                      ),
                    ),
                  ),
                  if (!widget.isForward)
                    Positioned(
                      child: GestureDetector(
                        onTap: () {
                          commentViewModel.removePdfFile(i);
                        },
                        child: SvgPicture.asset(ic_media_delete, width: 22),
                      ),
                    ),
                ],
              ),
          ],
        ),
      ),
    );
  }

  // Upload media preview slider
  Widget _buildMediaPreviewSlider(List mediaFiles) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: Row(
        children: [
          for (var i = 0; i < mediaFiles.length; i++)
            Stack(
              children: [
                GestureDetector(
                  onTap: () {
                    List<MediaInfoArguments> mediaArgsList = commentViewModel.mediaImageFiles.value.map((m) => MediaInfoArguments(
                      mediaPath: m ?? '',
                      mediaDescription: '',
                      mediaType: m.substring(m.length - 3) ?? '',
                      datetime: ''
                    )).toList();

                    goToNextNamedRoute(context, mediaSliderScreen, args: MediaSliderArguments(mediaArgsList, i));
                  },
                  child: Container(
                    margin: EdgeInsets.all(10),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: p.extension(commentViewModel.mediaImageFiles.value[i]) == '.MOV' || p.extension(commentViewModel.mediaImageFiles.value[i]) == '.mp4'
                      ? VideoThumbnailWidget(
                        videoPath: commentViewModel.mediaImageFiles.value[i],
                        height: 60,
                        width: 60,
                      ) : commentViewModel.mediaImageFiles.value[i][0] == '/' ? Image.file(
                        File(commentViewModel.mediaImageFiles.value[i]),
                        height: 60,
                        width: 60,
                        fit: BoxFit.cover,
                        cacheWidth: SizeUtils.width(context, 60).toInt(),
                        cacheHeight: SizeUtils.height(context, 30).toInt(),
                      ) : CachedNetworkImage(
                        width: 60,
                        height: 60,
                        maxHeightDiskCache: 120,
                        maxWidthDiskCache: 120,
                        placeholder: (context, url) => Container(color: Colors.grey[50]),
                        imageUrl: commentViewModel.mediaImageFiles.value[i],
                        fit: BoxFit.cover,
                        alignment: Alignment.center,
                      ),
                    ),
                  ),
                ),
                if (!widget.isForward)
                  Positioned(
                    top: 5,
                    right: 5,
                    child: GestureDetector(
                      onTap: () {
                        commentViewModel.removeImageFile(i);

                      },
                      child: SvgPicture.asset(ic_media_delete, width: 22),
                    ),
                  ),
              ],
            ),
        ],
      ),
    );
  }

  // Open camera
  Future getCamera() async {
    commentViewModel.isPostFeedLoading.value = true;
    try {
      await AppService.getConfig().then((response) async {
        final pickedFile = await _picker.pickImage(source: ImageSource.camera, imageQuality: response.data['response']['taskMediaUploadQuality']);
        if (pickedFile != null) {
          if (!commentViewModel.mediaImageFiles.value.contains(pickedFile.path)) {
            commentViewModel.mediaImageFiles.value.add(pickedFile.path);
          }
        }
      });
    }
    on PlatformException catch (e) {
      ErrorUtils.handlePlatformError(context, e);
    }
    catch (e) {
      showError(context, 'Unexpected error, please try again.');
    }
    commentViewModel.isPostFeedLoading.value = false;
  }

  // Image picker
  Future getImage() async {
    var pickedFile;
    commentViewModel.isPostFeedLoading.value = true;
    bool pickSingleImage = false;
    if (Platform.isIOS) {
      var iosData = await DeviceInfoPlugin().iosInfo;
      if ((iosData.systemVersion).compareTo('14.0') < 0) {
        pickSingleImage = true;
      }
    }
    try {
      await AppService.getConfig().then((response) async {
        if (pickSingleImage) {
          await _picker.pickImage(source: ImageSource.gallery, imageQuality: response.data['response']['taskMediaUploadQuality']).then((value) {
            pickedFile = [];
            pickedFile.add(value);
          });
        } else {
          pickedFile = await _picker.pickMultiImage(imageQuality: response.data['response']['taskMediaUploadQuality']);
        }
        if (pickedFile != null) {
          pickedFile.forEach((element) {
            if (!commentViewModel.mediaImageFiles.value.contains(element.path)) {
              commentViewModel.mediaImageFiles.value.add(element.path);
            }
          });
        }
      });
    }
    on PlatformException catch (e) {
      ErrorUtils.handlePlatformError(context, e);
    }
    catch (e) {
      showError(context, 'Unexpected error, please try again.');
    }
    commentViewModel.isPostFeedLoading.value = false;
  }

  // Take Video via Camera
  Future takeVideo() async {
    commentViewModel.isPostFeedLoading.value = true;
    try {
      final pickedFile = await _picker.pickVideo(source: ImageSource.camera);
      if (pickedFile != null) {
        await VideoCompress.setLogLevel(0);
        final MediaInfo? comprossedVideo = await VideoCompress.compressVideo(
          pickedFile.path,
          quality: VideoQuality.Res1280x720Quality,
          deleteOrigin: true,
          includeAudio: true,
        );
        if (!commentViewModel.mediaImageFiles.value.contains(comprossedVideo!.path)) {
          commentViewModel.mediaImageFiles.value.add(comprossedVideo.path);
        }
      }
    }
    on PlatformException catch (e) {
      ErrorUtils.handlePlatformError(context, e);
    }
    catch (e) {
      showError(context, 'Unexpected error, please try again.');
    }
    commentViewModel.isPostFeedLoading.value = false;
  }

  // Video picker
  Future getVideo() async {
    commentViewModel.isPostFeedLoading.value = true;
    try {
      final pickedFile = await _picker.pickVideo(source: ImageSource.gallery);
      if (pickedFile != null) {
        await VideoCompress.setLogLevel(0);
        final MediaInfo? comprossedVideo = await VideoCompress.compressVideo(
          pickedFile.path,
          quality: VideoQuality.Res1280x720Quality,
          deleteOrigin: true,
          includeAudio: true,
        );
        if (!commentViewModel.mediaImageFiles.value.contains(comprossedVideo!.path)) {
          commentViewModel.mediaImageFiles.value.add(comprossedVideo.path);
        }
      }
    }
    on PlatformException catch (e) {
      ErrorUtils.handlePlatformError(context, e);
    }
    catch (e) {
      showError(context, 'Unexpected error, please try again.');
    }
    commentViewModel.isPostFeedLoading.value = false;
  }

  // PDF file picker
  Future getPDF() async {
    commentViewModel.isPostFeedLoading.value = true;
    try {
      FilePickerResult? pickedFile = await FilePicker.platform.pickFiles(
        type: FileType.custom, 
        allowedExtensions: ['pdf'],
        allowMultiple: true
      );

      if (pickedFile != null) {
        pickedFile.files.forEach((file) {
          if (file.extension?.toLowerCase() != 'pdf') {
            showError(context, 'Must be PDF');
          } else if (file.size > 20971520) {
            showError(context, 'PDF size must below 20MB');
          } else {
            // limit up to 20 files
            if (!commentViewModel.mediaPdfFiles.value.contains(file.path)) {
              commentViewModel.mediaPdfFiles.value.add(file.path);
            }
          }
        });
      }
    }
    on PlatformException catch (e) {
      ErrorUtils.handlePlatformError(context, e);
    }
    catch (e) {
      showError(context, 'Unexpected error, please try again.');
    }
    commentViewModel.isPostFeedLoading.value = false;
  }
}
