import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:bubble/bubble.dart';
import 'package:assorted_layout_widgets/assorted_layout_widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/database/feeds.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/view_models/app_view_model.dart';
import 'package:sources/widgets/avatar.dart';
import 'package:sources/widgets/content_widget.dart';
import 'package:sources/widgets/topic_pill.dart';
import 'package:sources/widgets/video_thumbnail_widget.dart';

class ChatBubble extends StatefulWidget {
  final FeedWithDetail feedWithDetail;
  final int index;
  final String type;
  final bool showBubble;
  final bool showTopic;
  final bool showNip;
  final bool showToolbox;
  final bool enableReplyButton;
  final bool enableEditButton;
  final bool enableDeleteButton;
  final String workplaceStatus;
  final void Function(dynamic)? onLongPressCallback;
  final void Function()? onResend;
  final void Function()? onReply;
  final void Function()? onForward;
  final void Function()? onShare;
  final void Function()? onEdit;
  final void Function()? onCopy;
  final void Function()? onDelete;
  final void Function(Media media) onPdfClicked;
  final void Function(List<Media> medias, List<Feed> feeds, int index) onImageClicked;

  ChatBubble({
    required this.feedWithDetail,
    required this.index,
    this.type = 'self', // self or other
    this.showBubble = true,
    this.showTopic = true,
    this.showNip = true,
    this.showToolbox = false,
    this.enableReplyButton = true,
    this.enableEditButton = true,
    this.enableDeleteButton = true,
    this.workplaceStatus = 'read',
    this.onLongPressCallback,
    this.onResend,
    this.onReply,
    this.onForward,
    this.onShare,
    this.onEdit,
    this.onCopy,
    this.onDelete,
    required this.onPdfClicked,
    required this.onImageClicked,
    Key? key,
  }) : super(key: key);

  @override
  _ChatBubbleState createState() => _ChatBubbleState();
}

class _ChatBubbleState extends State<ChatBubble> {
  final AppViewModel appViewModel = locator<AppViewModel>();
  final database = locator<AppDatabase>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Opacity(
        opacity: widget.showBubble ? 1 : 0,
        child: Stack(
          children: [
            GestureDetector(
              onLongPress: () {
                RenderObject object = context.findRenderObject()!;
                var translation = object.getTransformTo(null).getTranslation();
                if (widget.onLongPressCallback != null)
                widget.onLongPressCallback!(translation);
              },
              child: Container(
                margin: EdgeInsets.only(bottom: 16),
                child: widget.type == 'self' ? _buildMyBubble(context) : _buildOthersBubble(context),
              ),
            ),
            // Build message toolbox
            if (widget.showToolbox) _buildFeedTools(context),
          ],
          alignment: Alignment.bottomRight,
        ),
      ),
    );
  }

  // Build current user's message
  Widget _buildMyBubble(BuildContext context) {
    final teamMemberDetail = widget.feedWithDetail.teamMemberDetail;
    final topicDetail = widget.feedWithDetail.topicDetail;
    final feed = widget.feedWithDetail.feedDetail;
    String postTime = formatDatetime(datetime: feed.postDateTime, format: 'dd MMM yyyy • hh:mma');
    List<Media> pdfList = widget.feedWithDetail.mediaList?.where((e) => e.fileExtension == 'pdf').toList() ?? [];
    List<Media> mediaList = widget.feedWithDetail.mediaList?.where((e) => e.fileExtension != 'pdf').toList() ?? [];

    final String feedDesription = feed.description ?? '';

    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        if (feed.sendStatus == 0 && !widget.showToolbox)
          GestureDetector(
            onTap: widget.onResend,
            child: SvgPicture.asset(ic_resend),
          ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Opacity(
              opacity: (feed.sendStatus == 0 && !widget.showToolbox) ? 0.5 : 1,
              child: Bubble(
                stick: true,
                margin: BubbleEdges.only(
                  top: widget.showNip ? 16 : 0,
                  bottom: 0,
                  right: widget.showNip ? 24 : 32,
                  left: 24
                ),
                padding: BubbleEdges.all(16),
                radius: Radius.circular(12),
                borderColor: yellow,
                borderWidth: 1,
                nip: widget.showNip ? BubbleNip.rightTop : BubbleNip.no,
                color: yellow,
                nipHeight: 12,
                alignment: Alignment.centerRight,
                shadowColor: transparent,
                elevation: 0,
                child: Container(
                  constraints: BoxConstraints(
                    minWidth: MediaQuery.of(context).size.width * 0.65,
                    maxWidth: MediaQuery.of(context).size.width * 0.65,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      // User title
                      if (widget.showNip)
                      Text('Me', style: boldFontsStyle(fontSize: 17.0, height: 1.35)),
                      // User designation name
                      if (widget.showNip)
                      Text(
                        teamMemberDetail?.designationName ?? '',
                        style: regularFontsStyle(fontSize: 15.0, height: 1.27),
                      ),
                      // Spacing
                      SizedBox(height: 10),
                      // If content is too long, use expandable widget else use html widget
                      if (feedDesription != '')
                        Padding(
                          padding: const EdgeInsets.only(bottom:16.0),
                          child: ContentWidget(
                            content: feedDesription,
                            readMoreColor: darkerYellow,
                            tagNameColor: darkerYellow,
                            fontSize: 19
                          ),
                        ),
                      // Media list
                      if (mediaList.length > 0)
                        Container(
                          margin: EdgeInsets.only(bottom: 16),
                          child: _buildMediaList(context, mediaList),
                        ),
                      // PDF list
                      if (pdfList.length > 0)
                        Container(
                          margin: EdgeInsets.only(bottom: 16),
                          child: _buildPDFList(context, pdfList),
                        ),
                      // Message category topic & time row
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: feed.sendStatus == 0 || feed.sendStatus == 2 ? [
                            SvgPicture.asset(ic_sending_clock, width: 20)
                          ] : [
                            // Render feed category topic
                            if (widget.showTopic && topicDetail != null)
                            _buildTopicPill(context, topicDetail),
                            // Render feed post time
                            _buildTime(postTime, black),
                          ],
                        ),
                      ),
                      // Add a line separator
                      if (feed.replyCount > 0 || (widget.feedWithDetail.replyFeedFailedCount != null && widget.feedWithDetail.replyFeedFailedCount! > 0)) _buildRepliesSeparator(context),
                      // Replies avatars
                      if (feed.replyCount > 0 || (widget.feedWithDetail.replyFeedFailedCount != null && widget.feedWithDetail.replyFeedFailedCount! > 0)) _buildRepliesButton(context),
                    ]
                  ),
                ),
              ),
            ),
            if (feed.sendStatus == 0 && !widget.showToolbox)
              Container(
                margin: EdgeInsets.only(left: 24, top: 8),
                child: Row(
                  children: [
                    SvgPicture.asset(
                      ic_warning,
                      color: red,
                      width: 25.0,
                    ),
                    SizedBox(width: 10.0),
                    Text(
                      'Not Delivered',
                      style: boldFontsStyle(color: red, fontSize: 15.0, height: 1.27),
                    )
                  ],
                ),
              )
          ],
        ),
      ],
    );
  }

  // Build other users' message
  Widget _buildOthersBubble(BuildContext context) {
    final teamMemberDetail = widget.feedWithDetail.teamMemberDetail;
    final topicDetail = widget.feedWithDetail.topicDetail;
    final feed = widget.feedWithDetail.feedDetail;
    List<Media> pdfList = widget.feedWithDetail.mediaList?.where((e) => e.fileExtension == 'pdf').toList() ?? [];
    List<Media> mediaList = widget.feedWithDetail.mediaList?.where((e) => e.fileExtension != 'pdf').toList() ?? [];

    String postTime = formatDatetime(datetime: feed.postDateTime, format: 'dd MMM yyyy • hh:mma');

    final String feedDesription = feed.description ?? '';

    return Container(
      padding: EdgeInsets.only(left: 16, right: 16),
      child: Stack(
        children: [
          // User avatar
          if (widget.showNip)
          Positioned(
            top: 16,
            left: 5,
            child: Avatar(
              url: teamMemberDetail?.teamMemberAvatar ?? null,
              width: 26,
              borderColor: grey,
            ),
          ),
          // Message bubble
          Bubble(
            stick: false,
            margin: BubbleEdges.only(
                top: widget.showNip ? 16 : 0,
                bottom: 0,
                right: 24,
                left: widget.showNip ? 32 : 40),
            padding: BubbleEdges.all(16),
            radius: Radius.circular(12),
            borderColor: grey,
            borderWidth: 0.5,
            color: white,
            nip: widget.showNip ? BubbleNip.leftTop : BubbleNip.no,
            nipHeight: 12,
            alignment: Alignment.centerLeft,
            shadowColor: transparent,
            elevation: 0,
            child: Container(
              // Set a fix width to bubble size
              constraints: BoxConstraints(
                minWidth: MediaQuery.of(context).size.width * 0.65,
                maxWidth: MediaQuery.of(context).size.width * 0.65,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  // User title
                  if (widget.showNip)
                    Text(
                      (teamMemberDetail?.teamMemberName ?? ''),
                      style: boldFontsStyle(fontSize: 17.0, height: 1.35),
                    ),
                  // User designation name
                  if (widget.showNip)
                    Text(
                      teamMemberDetail?.designationName ?? '',
                      style: regularFontsStyle(color: grey, fontSize: 15.0, height: 1.27),
                    ),
                  SizedBox(height: 10),
                  // If content is too long, use expandable widget else use html widget
                  if (feedDesription != '')
                    Padding(
                      padding: const EdgeInsets.only(bottom:16.0),
                      child: ContentWidget(
                        content: feedDesription,
                        readMoreColor: darkYellow,
                        tagNameColor: darkYellow,
                        fontSize: 19
                      ),
                    ),
                  // Media list
                  if (mediaList.length > 0)
                    Container(
                      margin: EdgeInsets.only(bottom: 16),
                      child: _buildMediaList(context, mediaList),
                    ),
                  // PDF list
                  if (pdfList.length > 0)
                    Container(
                      margin: EdgeInsets.only(bottom: 16),
                      // padding: EdgeInsets.only(right: 24),
                      child: _buildPDFList(context, pdfList),
                    ),
                  // Message category topic & time row
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        // Render feed category topic
                        if (widget.showTopic && topicDetail != null)
                          _buildTopicPill(context, topicDetail),
                        // Render feed post time
                        _buildTime(postTime, grey),
                      ],
                    ),
                  ),
                  // Add a line separator
                  if (feed.replyCount > 0 || (widget.feedWithDetail.replyFeedFailedCount != null && widget.feedWithDetail.replyFeedFailedCount! > 0)) _buildRepliesSeparator(context),
                  // Replies avatars
                  if (feed.replyCount > 0 || (widget.feedWithDetail.replyFeedFailedCount != null && widget.feedWithDetail.replyFeedFailedCount! > 0)) _buildRepliesButton(context),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  // Feed topic pill
  Widget _buildTopicPill(BuildContext context, Topic topicDetail) {
    return TopicPill(
      text: topicDetail.topicName!,
      color: hexToColor(topicDetail.topicColour!),
      borderColor: hexToColor(topicDetail.topicColour!),
      backgroundColor: white,
      margin: EdgeInsets.zero,
    );
  }

  // Feed post time
  Widget _buildTime(String time, Color color) {
    return Text(
      time,
      style: regularFontsStyle(color: color, fontSize: 12.0, height: 1.33),
    );
  }

  // Geraldine contact JC via whatsapp talk about file name is too long at 22 Apr
  // and said can ignore this comment again at 22 Apr (same day)
  Widget _buildPDFList(BuildContext context, List<Media> pdfList) {
    return Wrap(
      children: [
        for (var index = 0; index < (pdfList.length <= 3 ? pdfList.length : 3); index++)
          GestureDetector(
            onTap: () {
              widget.onPdfClicked(pdfList[index]);
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              margin: EdgeInsets.only(right: 10, bottom: 10),
              decoration: BoxDecoration(
                border: Border.all(),
                borderRadius: BorderRadius.all(Radius.circular(50.0)),
              ),
              child: Row(
                children: [
                  SvgPicture.asset(ic_doc, width: 18.0, color: widget.type == 'self' ? darkerYellow : null),
                  SizedBox(width: 8.0),
                  if (pdfList[index].url != null || pdfList[index].localFilePath != null)
                  Expanded(
                    child: Text(
                      (pdfList[index].url ?? pdfList[index].localFilePath)!.substring((pdfList[index].url ?? pdfList[index].localFilePath)!.lastIndexOf("/") + 1),
                      style: regularFontsStyle(fontSize: 14.0),
                    ),
                  ),
                ],
              )
            ),
          ),
        if (pdfList.length > 3)
          ExpandableNotifier(
            child: Expandable(
              collapsed: ExpandableButton(
                child: Text(
                  '${pdfList.length-3} More...',
                  style: boldFontsStyle(fontSize: 14.0, color: darkerYellow, decoration: TextDecoration.underline),
                ),
              ),
              expanded: Wrap(
                children: [
                  for (var index = 3; index < pdfList.length; index++)
                    GestureDetector(
                      onTap: () {
                        widget.onPdfClicked(pdfList[index]);
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                        margin: EdgeInsets.only(right: 10, bottom: 10),
                        decoration: BoxDecoration(
                          border: Border.all(),
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                        ),
                        child: Row(
                          children: [
                            SvgPicture.asset(ic_doc, width: 18.0, color: widget.type == 'self' ? darkerYellow : null),
                            SizedBox(width: 8.0),
                            if (pdfList[index].url != null || pdfList[index].localFilePath != null)
                            Expanded(
                              child: Text(
                                (pdfList[index].url ?? pdfList[index].localFilePath)!.substring((pdfList[index].url ?? pdfList[index].localFilePath)!.lastIndexOf("/") + 1),
                                style: regularFontsStyle(fontSize: 14.0),
                              ),
                            ),
                          ],
                        )
                      ),
                    ),
                ],
              ),
            )
          ),
      ],
    );
  }

  // Stream media list by feedID
  Widget _buildMediaList(BuildContext context, List<Media> medias) {
    List<Feed> feeds = medias.map((e) => widget.feedWithDetail.feedDetail).toList();

    return LayoutBuilder(
      builder: (context, constraints) {
        double imageWidth = (constraints.maxWidth - 30) / 3;

        return Wrap(
          children: [
            for (var index = 0; index < (medias.length <= 3 ? medias.length : 3); index++)
              GestureDetector(
                onTap: () {
                  widget.onImageClicked(medias, feeds, index);
                },
                child: (medias[index].fileExtension == 'MOV' || medias[index].fileExtension == 'mp4') ? Container(
                  width: imageWidth,
                  height: imageWidth,
                  margin: EdgeInsets.only(right: 10, bottom: 10),
                  child: Stack(
                    children: [
                      AspectRatio(
                        aspectRatio: 1,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(12),
                          child: VideoThumbnailWidget(
                            videoPath: medias[index].url  ?? medias[index].localFilePath!,
                            height: imageWidth,
                            width: imageWidth,
                          ),
                        ),
                      ),
                      if (medias.length > 3 && index == 2)
                      Container(
                        decoration: BoxDecoration(
                          color: darkGrey,
                          border: Border.all(color: Color(0x00ffffff)),
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Center(
                          child: Text(
                            '+' + (medias.length - 2).toString(),
                            style: boldFontsStyle(fontFamily: platForm, fontSize: 30.0, letterSpacing: 0.8, height: 1.33, color: white),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ],
                  )
                ) : Container(
                  width: imageWidth,
                  height: imageWidth,
                  margin: EdgeInsets.only(right: 10, bottom: 10),
                  child: Stack(
                    children: [
                      AspectRatio(
                        aspectRatio: 1,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(12),
                          child: (widget.workplaceStatus != 'archive') ? CachedNetworkImage(
                            placeholder: (context, url) => Container(color: Colors.grey[50]),
                            imageUrl: medias[index].url ?? '',
                            fit: BoxFit.cover,
                            alignment: Alignment.center,
                            maxHeightDiskCache: 120,
                            maxWidthDiskCache: 120,
                          ) : Container(
                            color: lightGrey,
                          )
                        ),
                      ),
                      if (medias.length > 3 && index == 2)
                      Container(
                        decoration: BoxDecoration(
                          color: darkGrey,
                          border: Border.all(color: Color(0x00ffffff)),
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Center(
                          child: Text(
                            '+' + (medias.length - 2).toString(),
                            style: boldFontsStyle(fontFamily: platForm, fontSize: 30.0, letterSpacing: 0.8, height: 1.33, color: white),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
          ],
        );
      }
    );
  }

  // Stream downloading progress.
  Future<double> getProgressValue(String keyName) async {
    return SpManager.getInt(keyName).toDouble();
  }

  // Separator line between message and reply button
  Widget _buildRepliesSeparator(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 15, bottom: 15),
      child: Divider(height: 1, thickness: 1),
    );
  }

  // Replies button
  Widget _buildRepliesButton(BuildContext context) {
    final Feed feed = widget.feedWithDetail.feedDetail;
    int totalCount = widget.feedWithDetail.replyFeedFailedCount != null ? widget.feedWithDetail.replyFeedFailedCount! + feed.replyCount : feed.replyCount;

    return Container(
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: widget.onReply,
        child: Row(
          children: [
            // Print avatar of replies member
            if(feed.replyTeamMemberAvatar != null && feed.replyCount > 0)
              // _buildTeamMemberAvatar(context, feed),
            // Spacing
            SizedBox(width: 10),
            // Show number of replies
            Text(
              totalCount.toString() + (totalCount > 1 ? ' Replies' : ' Reply'),
              style: boldFontsStyle(
                color: widget.type == 'self' ? black : grey, 
                fontSize: 15.0, 
                height: 1.27
              ),
            ),
            // Show unread indicator if replies haven't read yet
            if (feed.replyReadStatus == 'unread')
            Container(
              width: 10,
              height: 10,
              margin: EdgeInsets.all(10),
              decoration: BoxDecoration(color: red, shape: BoxShape.circle),
            ),
            if (widget.feedWithDetail.replyFeedFailedCount != null && widget.feedWithDetail.replyFeedFailedCount! > 0)
            SvgPicture.asset(
              ic_warning,
              width: 25,
              height: 25,
              color: red,
            )
          ],
        ),
      ),
    );
  }

  //  Team member avatars
  // Widget _buildTeamMemberAvatar(BuildContext context, Feed feed) {
  //   var distinctTeamMembers = feed.replyTeamMemberAvatar!.split('|');
  //   var itemCount = distinctTeamMembers.length;

  //   return itemCount > 0 ? RowSuper(
  //     innerDistance: -6,
  //     children: distinctTeamMembers.asMap().entries.map((e) {
  //       final index = e.key;
  //       final avatarUrl = e.value;
  //       // Print 3 avatars only
  //       if (index < 3) {
  //         return Avatar(
  //           url: avatarUrl,
  //           width: 20,
  //           borderColor: widget.type == 'self' ? yellow : white,
  //           // If more than 3 avatars, the last one shows dots
  //           dots: (itemCount > 3 && index == 2) ? true : false,
  //         );
  //       }
  //     }).toList()
  //   ) : Container();
  // }

  // Message toolbox
  Widget _buildFeedTools(BuildContext context) {
    return Positioned(
      bottom: 0,
      right: widget.type == 'self' ? 40 : 80,
      child: RowSuper(
        innerDistance: 8,
        children: [
          if (widget.enableReplyButton)
          GestureDetector(
            onTap: widget.onReply,
            child: SvgPicture.asset(ic_toolbox_reply, width: 36),
          ),
          GestureDetector(
            onTap: widget.onForward,
            child: SvgPicture.asset(ic_toolbox_forward, width: 36),
          ),
          GestureDetector(
            onTap: widget.onShare,
            child: SvgPicture.asset(ic_toolbox_share, width: 36),
          ),
          if (widget.type == 'self' && widget.enableEditButton)
          GestureDetector(
            onTap: widget.onEdit,
            child: SvgPicture.asset(ic_toolbox_edit, width: 36),
          ),
          GestureDetector(
            onTap: widget.onCopy,
            child: SvgPicture.asset(ic_toolbox_copy, width: 36),
          ),
          if (widget.type == 'self' && widget.enableDeleteButton)
          GestureDetector(
            onTap: widget.onDelete,
            child: SvgPicture.asset(ic_toolbox_delete, width: 36),
          ),
        ],
      )
    );
  }
}
