import 'dart:ui' as ui;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:expandable/expandable.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:sources/widgets/simple_html_css.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/utils/text_utils.dart';

class ContentWidget extends StatelessWidget {
  final String content;
  final Color contentColor;
  final double fontSize;
  final Color readMoreColor;
  final Color tagNameColor;
  final int maxLines;
  final bool enabledReadMore;
  final Color linkColor;
  final String readmoreText;
  final String readlessText;

  const ContentWidget({
    this.content = '',
    this.contentColor = black,
    this.fontSize = 19.0,
    this.readMoreColor = darkYellow,
    this.tagNameColor = darkYellow,
    this.maxLines = 4,
    this.enabledReadMore = true,
    this.linkColor = linkBlue,
    this.readlessText = 'read less',
    this.readmoreText = 'read more',
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, size) {
        String htmlContent = this.content.replaceAll(' xss=removed', '');
        TextStyle aTagTextStyle = regularFontsStyle(
          color: linkColor, 
          fontSize: this.fontSize, 
          height: 1.32
        );
        TextStyle h6TagTextStyle = boldFontsStyle(
          color: this.tagNameColor, 
          fontSize: this.fontSize, 
          height: 1.32
        );

        return ExpandableNotifier(
          child: Column(
            children: [
              Expandable(
                collapsed: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      RichText(
                        textScaler: MediaQuery.of(context).textScaler,
                        text: HTML.toTextSpan(
                          context, 
                          htmlContent,
                          defaultTextStyle: regularFontsStyle(
                            color: this.contentColor, 
                            fontSize: this.fontSize, 
                            height: 1.32
                          ),
                          overrideStyle: {
                            'a': aTagTextStyle,
                            'h6': h6TagTextStyle,
                          },
                          linksCallback: (link) {
                            launch(link);
                          },
                        ),
                        maxLines: this.maxLines,
                        overflow: TextOverflow.ellipsis,
                      ),
                      // Read more button
                      if (this.enabledReadMore && isTextOverflow(context, htmlContent, aTagTextStyle, h6TagTextStyle, minWidth: size.minWidth, maxWidth: size.maxWidth))
                        ExpandableButton(
                          child: Text(
                            this.readmoreText,
                            textAlign: TextAlign.center,
                            style: boldFontsStyle(
                              color: readMoreColor,
                              fontSize: this.fontSize,
                            ),
                          ),
                        ),
                    ],
                  ),
                ), 
                // After content is expand, show full content with html widget
                expanded: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    RichText(
                      textScaleFactor: MediaQuery.of(context).textScaleFactor,
                      text: HTML.toTextSpan(
                        context, 
                        htmlContent,
                        defaultTextStyle: regularFontsStyle(
                          color: this.contentColor, 
                          fontSize: this.fontSize, 
                          height: 1.32
                        ),
                        overrideStyle: {
                          'a': aTagTextStyle,
                          'h6': h6TagTextStyle,
                        },
                        linksCallback: (link) {
                          launch(link);
                        }
                      ),
                    ),
                    // Read less button
                      ExpandableButton(
                        child: Text(
                          this.readlessText,
                          textAlign: TextAlign.center,
                          style: boldFontsStyle(
                            color: readMoreColor,
                            fontSize: this.fontSize,
                          ),
                        ),
                      ),
                  ]
                ),
              ),
            ],
          ),
        );
      }
    );
  }
  
  bool isTextOverflow(BuildContext context, String html, TextStyle aTagStyle, TextStyle h6TagStyle, {double minWidth = 0, double maxWidth = double.infinity}) {
    final TextPainter textPainter = TextPainter(
      text: HTML.toTextSpan(
        context, 
        html,
        defaultTextStyle: regularFontsStyle(
          color: this.contentColor, 
          fontSize: this.fontSize, 
          height: 1.32
        ),
        overrideStyle: {
          'a': aTagStyle,
          'h6': h6TagStyle,
        },
        linksCallback: (link) {
          launch(link);
        }
      ),
      maxLines: this.maxLines,
      textDirection: ui.TextDirection.ltr,
    )..layout(minWidth: minWidth, maxWidth: maxWidth);

    return textPainter.didExceedMaxLines;
  }
}
