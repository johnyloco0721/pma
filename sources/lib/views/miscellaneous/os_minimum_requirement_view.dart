import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/utils/text_utils.dart';

class OSRequireView extends StatelessWidget {
  final String platform;

  const OSRequireView({Key? key, required this.platform}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String requireVersionMsg = platform == 'ios' ? '12.4 IOS version' : '6.0 Android version';
    String updateOSMsg = platform == 'ios' ? 'update your IOS version' : 'update your Android version';

    return Scaffold(
      backgroundColor: white,
      body: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
                img_os_version_pic,
                width: 150.0,
                height: 150.0,
                fit: BoxFit.fill,
            ),
            Container(
                width: MediaQuery.of(context).size.width * 0.8,
                child: Column(
                    children: [
                      SizedBox(height: 15.0),
                      RichText(
                        text: TextSpan(
                          text: 'Minimum ',
                          style: regularFontsStyle(fontSize: 17.0, height: 1.35),
                          children: [
                            TextSpan(text: requireVersionMsg, style: boldFontsStyle(fontSize: 17.0, height: 1.35)),
                            TextSpan(text: ' is required to support all the features in this app.', style: regularFontsStyle(fontSize: 17.0, height: 1.35)),
                          ]
                        ),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 15.0),
                      RichText(
                        text: TextSpan(
                          text: 'Please ',
                          style: regularFontsStyle(fontSize: 17.0, height: 1.35),
                          children: [
                            TextSpan(text: updateOSMsg, style: boldFontsStyle(fontSize: 17.0, height: 1.35)),
                            TextSpan(text: ' in order to continue to this app.', style: regularFontsStyle(fontSize: 17.0, height: 1.35)),
                          ]
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ]
                )
            ),
          ]
        )
      ),
    );
  }
}
