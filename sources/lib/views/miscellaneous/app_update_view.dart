import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/utils/text_utils.dart';

import '../../constants/app_data_models.dart';

class UpdateView extends StatelessWidget {

  void _launchURL() async => await canLaunch(AppDataModel.getUpdateAppLink()) ? await launch(AppDataModel.getUpdateAppLink()) : throw 'Could not launch ${AppDataModel.getUpdateAppLink()}';

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: white,
      body: Container(
        width: double.infinity,
        child: Column(
          children: [
            SafeArea(
              child: Text("")
            ),
            Image.asset(
                img_makeover_service_logo,
                width: MediaQuery.of(context).size.width * 0.39
            ),
            Lottie.asset(
              'assets/lotties/update.json',
              width: 300.0,
              height: 300.0,
              fit: BoxFit.fill,
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.7,
              child: Column(
                children: [
                  Text(
                    'Hi, please update your app',
                    style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, height: 1.3, letterSpacing: 0.8),
                    textAlign: TextAlign.center,
                  ),
                  Padding(padding: EdgeInsets.only(top: 16)),
                  Text(
                    'Update your app so that we can bring you a better experience!',
                    style: regularFontsStyle(fontSize: 17.0, height: 1.35),
                    textAlign: TextAlign.center,
                  ),
                  Padding(padding: EdgeInsets.only(top: 20)),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(
                        top: 15.0
                    ),
                    decoration: BoxDecoration(
                        color: yellow,
                        borderRadius: BorderRadius.circular(30),
                    ),
                    child: TextButton(
                      child: Text(
                        'Update Now',
                        style: boldFontsStyle(height: 1.32),
                      ),
                      onPressed: () {
                        _launchURL();
                      },
                    ),
                  )
                ]
              )
            ),
          ]
        )
      ),
    );
  }
}
