import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/constants/app_data_models.dart';
import 'package:sources/services/api.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/text_utils.dart';

class MaintenanceView extends StatefulWidget {
  @override
  _MaintenanceViewState createState() => _MaintenanceViewState();
}

class _MaintenanceViewState extends State<MaintenanceView> {
  int _topLeftTapCount = 0;
  int _bottomRightTapCount = 0;
  String _bypassCode = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: Stack(
        children: [
           GestureDetector(
            onDoubleTap: _handleContainer1DoubleTap,
          child: Align(
            alignment: Alignment.topLeft,
            child: Container(
              margin: EdgeInsets.only(top: 50, left: 10),
              width: 150,
              height: 150,
              color: Colors.transparent,
              child: Center(
                child: Text(
                  '',
                  style: Theme.of(context).textTheme.titleLarge!.copyWith(
                        color: Colors.white,
                      ),
                ),
              ),
            ),
            ),
          ),
          GestureDetector(
            onDoubleTap: _handleContainer2DoubleTap,
            child: Align(
              alignment: Alignment.bottomRight,
              child: Container(
                margin: EdgeInsets.only(bottom: 50, right: 10),
                width: 150,
                height: 150,
                color: Colors.transparent,
                child: Center(
                  child: Text(
                    '',
                    style: Theme.of(context).textTheme.titleLarge!.copyWith(
                          color: Colors.white,
                        ),
                  ),
                ),
              ),
            ),
          ),
          Positioned.fill(
            bottom: MediaQuery.of(context).size.height * 0.2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  img_makeover_service_logo,
                  width: MediaQuery.of(context).size.width * 0.39,
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.width * 0.4),
                ),
                Lottie.asset(
                  'assets/lotties/maintenance.json',
                  width: 250.0,
                  height: 250.0,
                  fit: BoxFit.fill,
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: Column(
                    children: [
                      Text(
                        'Sorry, the app is under maintenance now! Please try again later',
                        style: regularFontsStyle(fontSize: 17.0, height: 1.35),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
  
  void _handleContainer1DoubleTap() {
    setState(() {
      _topLeftTapCount=2;
      if (_topLeftTapCount == 2 && _bottomRightTapCount == 2) {
        _bottomRightTapCount=0;
      }
    });
  }

  void _handleContainer2DoubleTap() {
    setState(() {
      _bottomRightTapCount=2;
      if(_topLeftTapCount == 0 && _bottomRightTapCount == 2){
        _bottomRightTapCount=0;
        _topLeftTapCount=0;
      }else if(_topLeftTapCount == 2 && _bottomRightTapCount == 2){
        _bottomRightTapCount=0;
        _topLeftTapCount=0;
        // Execute your desired action after double tap on both containers
        _showBypassCodeDialog();
      }
    });
  }

// ByPassCode Error Message Alert Box
  void _showErrorMessageDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: white,
          contentPadding: EdgeInsets.fromLTRB(40, 45, 40, 75),
          title: Container(
            width: double.maxFinite,
            height: 70,
            child: Text('Invalid Bypass Code'),
          ),
          content: Text('Please enter a valid bypass code.'),
          actions: [
            TextButton(
              child: Text(
                'OK',
                style: TextStyle(color: darkYellow),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

// ByPassCode Alert Box
  void _showBypassCodeDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: white,
          contentPadding: EdgeInsets.fromLTRB(40, 40, 40, 70),
          title: Container(
             width: double.maxFinite, // Expand the width to maximum
             height: 50,
             child: Text('Enter Bypass Code'),
          ),
          content: TextFormField(
            decoration: InputDecoration(
              hintText: 'Bypass Code',
            ),
            onChanged: (value) {
              setState(() {
                _bypassCode = value; 
              });
            },
          ),
          actions: [
            TextButton(
              child: Text('Cancel', style: TextStyle(color: black),),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
                child: Padding(
                padding: EdgeInsets.only(right: 20),
                child: Text('Submit', style: TextStyle(color: darkYellow),),
              ),
              onPressed: () async {
                if (_bypassCode != '') {
                  // Perform the bypass action here
                  await Api().postHTTP('MaintenanceByPass', '{ "password" : "$_bypassCode" }', checking: false).then((response) {
                    AppDataModel.getByPassParameter(response.data['response']['parameter']);
                    AppDataModel.getByPassHash(response.data['response']['hash']);
                    AppDataModel.setByPassCode(true);
                    goAndClearAllPageNamed(context, homeScreen);
                  }).onError((error, stackTrace) {
                    _showErrorMessageDialog();
                  });
                } else {
                  _showErrorMessageDialog();
                }
              },
            ),
          ],
        );
      },
    );
  }
}