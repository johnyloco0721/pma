import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/constants/app_data_models.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/models/loginData.dart';
import 'package:sources/models/profile_model.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/app_service.dart';
import 'package:sources/services/profile_service.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/shared_preference_utils/pref_keys.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/widgets/custom_text_input_widget.dart';

class LoginView extends StatefulWidget {
  final String? errorMessage;
  const LoginView({Key? key, this.errorMessage}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  bool usernameNull = false;
  bool passwordNull = false;
  bool usernameMatching = true;
  bool passwordMatching = true;
  String errMsg = "";
  int onlyClickOnce = 0;

  @override
  void initState() {
    // implement initState
    super.initState();

    // Start listening to changes.
    usernameController.addListener(() {
      usernameMatching = true;
      usernameNull = !checkNull(usernameController.text);
    });
    passwordController.addListener(() {
      passwordMatching = true;
      passwordNull = !checkNull(passwordController.text);
    });

    // Redirect to next route once widget built completed
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (widget.errorMessage != null) {
        SpManager.logoutDeleteData();
        showToastMessage(context, message: widget.errorMessage ?? '');
      }
    });
  }
  
  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  void updateErrMsg(msg) {
    setState(() {
      errMsg = msg;
    });
  }

  bool checkNull(value) {
    bool trueFalse = false;
    setState(() {
      if (value == null || value.isEmpty) {
        trueFalse = true;
      }
    });
    return trueFalse;
  }

  void loginRequest() async {
    FirebaseMessaging.instance
      .getToken()
      .then((token) async {
        if (token != null) {
          LoginData loginData = LoginData(usernameController.text, passwordController.text, token);
          
          try {
            if (AppDataModel.getByPassCode()) {
              setState(() {
                onlyClickOnce = onlyClickOnce + 1;
              });

              if (onlyClickOnce == 1) {
                await Api().postHTTP('Login?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', loginData.toJson()).then((response) {
                  if (response.data['status']) {
                    SpManager.putString(loginKey, response.data['response']['loginDetails']['loginHash']);
                    SpManager.putInt(userKey, response.data['response']['loginDetails']['teamMemberID']);

                    // After login get profile details and store into shared preferences.
                    try {
                      AppService.getProfile().then((response) async {
                        ProfileModel profileModel = ProfileModel.fromJson(response.data);
                        if (profileModel.status == true) {
                          var json = response.data['response']['profileDetails'];
                          locator<AppDatabase>().profileDao.getSingleProfile(json['username']).then((profile) async {
                            if (profile == null) {
                              // If user changed, clean up database and go to loadDataScreen
                              await ProfileService().cleanDatabase();
                              SpManager.removeItem(syncedAPI);
                              goAndClearAllPageNamed(context, loadDataScreen);
                            }
                          });
                          SpManager.saveUser(json);
                          // If user is same, go to homeScreen
                          goAndClearAllPageNamed(context, homeScreen);
                        }
                      });
                    }
                    catch (e) {
                      if (e == 'Token mismatched.') {
                        goAndClearAllPageNamed(context, loginScreen);
                      }
                    }
                  }
                  else{
                    usernameMatching = false;
                    passwordMatching = false;
                    updateErrMsg(response.data['message']);
                  }
                }).timeout(Duration(seconds: 60), onTimeout: () {
                  setState(() {
                    onlyClickOnce = 0;
                  });
                }).whenComplete(() {
                  setState(() {
                    onlyClickOnce = 0;
                  });
                });
              }
            }else{
              setState(() {
                onlyClickOnce = onlyClickOnce + 1;
              });

              if (onlyClickOnce == 1) {
                await Api().postHTTP('Login', loginData.toJson()).then((response) {
                  if (response.data['status']) {
                    SpManager.putString(loginKey, response.data['response']['loginDetails']['loginHash']);
                    SpManager.putInt(userKey, response.data['response']['loginDetails']['teamMemberID']);

                    // After login get profile details and store into shared preferences.
                    try {
                      AppService.getProfile().then((response) async {
                        ProfileModel profileModel = ProfileModel.fromJson(response.data);
                        if (profileModel.status == true) {
                          var json = response.data['response']['profileDetails'];
                          locator<AppDatabase>().profileDao.getSingleProfile(json['username']).then((profile) async {
                            if (profile == null) {
                              // If user changed, clean up database and go to loadDataScreen
                              await ProfileService().cleanDatabase();
                              SpManager.removeItem(syncedAPI);
                              goAndClearAllPageNamed(context, loadDataScreen);
                            }
                          });
                          SpManager.saveUser(json);
                          // If user is same, go to homeScreen
                          goAndClearAllPageNamed(context, homeScreen);
                        }
                      });
                    }
                    catch (e) {
                      if (e == 'Token mismatched.') {
                        goAndClearAllPageNamed(context, loginScreen);
                      }
                    }
                  }
                  else{
                    usernameMatching = false;
                    passwordMatching = false;
                    updateErrMsg(response.data['message']);
                  }
                }).timeout(Duration(seconds: 60), onTimeout: () {
                  setState(() {
                    onlyClickOnce = 0;
                  });
                }).whenComplete(() {
                  setState(() {
                    onlyClickOnce = 0;
                  });
                });
              }
            }
          }
          catch (e) {
            if (e == 'Token mismatched.') {
              goAndClearAllPageNamed(context, loginScreen);
            } else {
              showError(context, 'Something went wrong, please try again!');
            }
          }
        }
        else {
          showError(context, "token is empty.");
        }
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: black,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        automaticallyImplyLeading: false,
        toolbarHeight: 0,
      ),
      backgroundColor: black,
      body: WillPopScope(
        onWillPop: () async => false,
        child: Stack(
          children: <Widget> [
            Container(
              padding: EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Welcome to',
                        style: boldFontsStyle(fontFamily: platForm, fontSize: 30.0, color: white, height: 1.14, letterSpacing: 0.5),
                      ),
                      Text(
                        'Mogster Garage',
                        style: boldFontsStyle(fontFamily: platForm, fontSize: 30.0, color: yellow, height: 1.14, letterSpacing: 0.5),
                      ),// Main Title
                      Container(
                        padding: EdgeInsets.only(bottom: 20.0, top: 10.0),
                        child: Text(
                          'by The Makeover Guys',
                          style: regularFontsStyle(color: white, height: 1.32),
                        ),
                      ),  // Sub Title
                      CustomTextInput(
                        inputType: 'username',
                        hintTextString: 'Username',
                        textEditController: usernameController,
                        themeColor: white,
                        suffixIcon: 'assets/icons/Properties_Owner.svg',
                        isNull: usernameNull,
                        isMatch: usernameMatching, maxLength: null,
                      ), // Username Field
                      CustomTextInput(
                        inputType: 'password',
                        hintTextString: 'Password',
                        textEditController: passwordController,
                        obscureText: true,
                        themeColor: white,
                        suffixIcon: 'assets/icons/Password_Icon.svg',
                        isNull: passwordNull,
                        isMatch: passwordMatching,
                      ), // Password Field
                      Container(
                        height: 60.0,
                        padding: EdgeInsets.only(top: 14.0, left: 20.0),
                        child: Text(
                          errMsg,
                          style: regularFontsStyle(fontSize: 15.0, color: lightRed, height: 1.27),
                        ),
                      ) // errorMsgWidget
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Opacity(
                        opacity: (usernameNull && passwordNull && (onlyClickOnce == 0)) ? 1.0 : 0.5,
                        child: Container(
                          width: double.infinity,
                          margin: EdgeInsets.only(top: 5.0),
                          decoration: BoxDecoration(
                            color: yellow,
                            borderRadius: BorderRadius.circular(30),
                          ),
                          child: TextButton(
                            child: Text(
                              'Login',
                              style: boldFontsStyle(height: 1.32),
                            ),
                            onPressed: !(usernameNull && passwordNull) ? null : () {
                              // API Request to login
                              loginRequest();
                            }
                          ),
                        ),
                      ), // Login Button
                      Container(
                        width: double.infinity,
                        margin: EdgeInsets.only(top: 15.0),
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(
                            color: white,
                            width: 1
                          )
                        ),
                        child: TextButton(
                          child: Text(
                            'Forgot Password',
                            style: boldFontsStyle(color: white, height: 1.32),
                          ),
                          onPressed: () {
                            // Go to reset password page
                            goToNextNamedRoute(context, forgotPasswordScreen);
                          },
                        ),
                      ) // Reset Password Button
                    ],
                  )
                ],
              ),
            ),
            Transform.translate(
              offset: Offset(MediaQuery.of(context).size.width * 0.40, MediaQuery.of(context).size.height * 0.65),
              child: Lottie.asset(
                'assets/lotties/auth.json',
                width: MediaQuery.of(context).size.width * 0.65,
                height: MediaQuery.of(context).size.height * 0.35,
                fit: BoxFit.fill,
              )
            )
          ]
        )
      ),
    );
  }
}