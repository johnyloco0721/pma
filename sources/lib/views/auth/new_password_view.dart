import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/widgets/custom_text_input_widget.dart';

class SetNewPasswordView extends StatefulWidget {
  final String resetPassToken;

  SetNewPasswordView({Key? key, required this.resetPassToken}) : super(key: key);

  @override
  _SetNewPasswordViewState createState() => _SetNewPasswordViewState();
}

class _SetNewPasswordViewState extends State<SetNewPasswordView> {
  final passwordController = TextEditingController();
  final confirmPassController = TextEditingController();
  bool passwordNull = false;
  bool confirmPassNull = false;
  bool passwordMatching = true;
  bool confirmPassMatching = true;
  String errMsg = "";

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    passwordController.dispose();
    confirmPassController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    // Start listening to changes.
    passwordController.addListener(() {
      passwordMatching = true;
      passwordNull = !checkNull(passwordController.text);
    });
    confirmPassController.addListener(() {
      confirmPassMatching = true;
      confirmPassNull = !checkNull(confirmPassController.text);
    });
  }

  void updateErrMsg(msg) {
    setState(() {
      errMsg = msg;
    });
  }

  bool checkNull(value) {
    bool trueFalse = false;
    setState(() {
      if (value == null || value.isEmpty) {
        trueFalse = true;
      }
    });
    return trueFalse;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: black,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        automaticallyImplyLeading: false,
        toolbarHeight: 0,
      ),
      body: Stack(
        children: <Widget> [
          Container(
            padding: EdgeInsets.only(
              top: 20.0,
              left: 20.0,
              right: 20.0
            ),
            width: double.infinity,
            color: Theme.of(context).primaryColor,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                      Text(
                        'Welcome to',
                        style: boldFontsStyle(fontFamily: platForm, fontSize: 30.0, color: white, height: 1.14, letterSpacing: 0.5),
                      ),
                      Text(
                        'Mogster Garage',
                        style: boldFontsStyle(fontFamily: platForm, fontSize: 30.0, color: yellow, height: 1.14, letterSpacing: 0.5),
                      ), // Main Title
                      Container(
                        padding: EdgeInsets.only(bottom: 20.0, top: 5.0),
                        child: Text(
                          'by The Makeover Guys',
                          style: regularFontsStyle(color: white, height: 1.32),
                        ),
                      ),  // Sub Title
                    CustomTextInput(
                      inputType: 'password',
                      hintTextString: 'Password',
                      textEditController: passwordController,
                      obscureText: true,
                      themeColor: Theme.of(context).hintColor,
                      suffixIcon: 'assets/icons/Password_Icon.svg',
                      isNull: passwordNull,
                      isMatch: passwordMatching,
                    ), // Password Field

                    CustomTextInput(
                      inputType: 'password',
                      hintTextString: 'Re-enter Password',
                      textEditController: confirmPassController,
                      obscureText: true,
                      themeColor: Theme.of(context).hintColor,
                      suffixIcon: 'assets/icons/Password_Icon.svg',
                      isNull: passwordNull,
                      isMatch: passwordMatching,
                    ), // Password Field
                    Container(
                      height: 60.0,
                      padding: EdgeInsets.only(
                          top: 14.0,
                          left: 20.0
                      ),
                      child: Text(
                        errMsg,
                        style: regularFontsStyle(fontSize: 15.0, color: lightRed, height: 1.27)
                      ),
                    ) // errorMsgWidget
                  ],
                ),
                Column(
                  children: <Widget>[
                    Opacity(
                      opacity: (passwordNull && confirmPassNull) ? 1.0 : 0.5,
                        child: Container(
                        width: double.infinity,
                        margin: EdgeInsets.only(
                            top: 5.0
                        ),
                        decoration: BoxDecoration(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(30),
                            border: Border.all(
                                color: white,
                                width: 1
                            )
                        ),
                        child: TextButton(
                            child: Text(
                              'Confirm Password',
                              style: boldFontsStyle(color: white),
                            ),
                            onPressed: !(passwordNull && confirmPassNull) ? null : (){
                              // TODO: Call API
                              // Api().postHTTP('PasswordVerification', {"token": widget.resetPassToken, "password": passwordController.text, "confirmPassword": confirmPassController.text}).then((response) {
                              //   if (response.data['status']) {
                              //     Navigator.push(context,MaterialPageRoute(builder: (context) => LoginPage()));
                              //   }
                              //   else {
                              //     passwordMatching = false;
                              //     confirmPassMatching = false;
                              //     updateErrMsg(response.data['message']);
                              //   }
                              // });
                            }
                        ),
                      ) // Reset Password Button
                    )
                  ],
                )
              ],
            ),
          ),
          Transform.translate(
            offset: Offset(MediaQuery.of(context).size.width * 0.40, MediaQuery.of(context).size.height * 0.65),
            child: Image(
              image: AssetImage("assets/images/Asset_Main_Illustration.png"),
              width: MediaQuery.of(context).size.width * 0.65,
            ),
          )
        ]
      ),
    );
  }
}