import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';
import 'package:sources/constants/app_data_models.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/services/api.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/widgets/custom_text_input_widget.dart';


class ResetPasswordView extends StatefulWidget {
  ResetPasswordView({Key? key}) : super(key: key);

  @override
  _ResetPasswordViewState createState() => _ResetPasswordViewState();
}

class _ResetPasswordViewState extends State<ResetPasswordView> {
  final usernameController = TextEditingController();
  bool usernameNull = false;
  bool usernameMatching = true;
  String errMsg = "";
  bool mailSent = false;
  late String emailReceiver;
  late String resetPassToken;
  int onlyClickOnce = 0;

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    usernameController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    // Start listening to changes.
    usernameController.addListener(() {
      usernameMatching = true;
      usernameNull = !checkNull(usernameController.text);
    });
  }

  void updateErrMsg(msg) {
    setState(() {
      errMsg = msg;
    });
  }

  bool checkNull(value) {
    bool trueFalse = false;
    setState(() {
      if (value == null || value.isEmpty) {
        trueFalse = true;
      }
    });
    return trueFalse;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: black,
      appBar: AppBar(
        backgroundColor: black,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        automaticallyImplyLeading: false,
        toolbarHeight: 0,
      ),
      body: GestureDetector(
        behavior: HitTestBehavior.opaque, // Make empty space clickable
        onTap: () {
          // Hide keyboard when unfocus input field
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Stack(
          children: <Widget> [
            Container(
              padding: EdgeInsets.only(
                top: 20.0,
                left: 20.0,
                right: 20.0
              ),
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Welcome to',
                        style: boldFontsStyle(fontFamily: platForm, fontSize: 30.0, color: white, height: 1.14, letterSpacing: 0.5),
                      ),
                      Text(
                        'Mogster Garage',
                        style: boldFontsStyle(fontFamily: platForm, fontSize: 30.0, color: yellow, height: 1.14, letterSpacing: 0.5),
                      ), // Main Title
                      Container(
                        padding: EdgeInsets.only(bottom: 20.0, top: 5.0),
                        child: Text(
                          'by The Makeover Guys',
                          style: regularFontsStyle(color: white, height: 1.32),
                        ),
                      ),  // Sub Title
                      (mailSent) ? Container(
                        padding: EdgeInsets.only( top: 20.0 ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'We sent a link to ',
                              style: regularFontsStyle(color: white, height: 1.32),
                            ),
                            Text(
                              emailReceiver + '.',
                              style: boldFontsStyle(color: white, height: 1.32),
                            ),
                            Text(
                              'Please check your email and click on the link to reset password.',
                              style: regularFontsStyle(color: white, height: 1.32),
                            ),
                          ]
                        )
                      ) : // after mail sent
                      CustomTextInput(
                        inputType: 'username',
                        hintTextString: 'Username',
                        textEditController: usernameController,
                        themeColor: white,
                        suffixIcon: 'assets/icons/Properties_Owner.svg',
                        isNull: usernameNull,
                        isMatch: usernameMatching,
                      ), // Username Field
                      Container(
                        height: mailSent ? 0.0 : 42.0,
                        padding: EdgeInsets.only(
                            top: 14.0,
                            left: 20.0
                        ),
                        child: Text(
                          errMsg,
                          style: regularFontsStyle(fontSize: 15.0, color: lightRed, height: 1.27),
                        ),
                      ) // errorMsgWidget
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Opacity(
                        opacity: usernameNull ? 1.0 : 0.5,
                        child: Container(
                          width: double.infinity,
                          margin: EdgeInsets.only(
                              top: mailSent ? 64.0 : 80.0
                          ),
                          decoration: BoxDecoration(
                              color: mailSent ? yellow : Colors.transparent,
                              borderRadius: BorderRadius.circular(30),
                              border: Border.all(
                                color: white,
                                width: mailSent ? 0 : 1
                              )
                          ),
                          child: Opacity(
                            opacity: (onlyClickOnce >= 1) ? 0.5 : 1.0,
                            child: TextButton(
                              child: Text(
                                mailSent ? 'Login Now' : 'Reset Password',
                                style: boldFontsStyle(
                                  color: mailSent ? black : white
                                ),
                              ),
                              onPressed: mailSent ? (() {
                                // Go back to login page
                                goToNextNamedRoute(context, loginScreen);
                              }) // after mail sent
                              : (!usernameNull ? null : () async {
                                // Call API
                                try{ 
                                  if (AppDataModel.getByPassCode()) {
                                    setState(() {
                                      onlyClickOnce = onlyClickOnce + 1;
                                    });

                                    if (onlyClickOnce == 1) {
                                      await Api().postHTTP('PasswordReset?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', {"loginUsername": usernameController.text}).then((response) {
                                        if (response.data['status']) {
                                          mailSent = true;
                                          updateErrMsg("");
                                          resetPassToken = response.data['response']['token'];
                                          emailReceiver = response.data['response']['teamMemberEmail'];
                                        }
                                        else {
                                          usernameMatching = false;
                                          updateErrMsg(response.data['message']);
                                        }
                                      }).timeout(Duration(seconds: 60), onTimeout: () {
                                        setState(() {
                                          onlyClickOnce = 0;
                                        });
                                      }).whenComplete(() {
                                        setState(() {
                                          onlyClickOnce = 0;
                                        });
                                      });
                                    }
                                  }else{
                                    setState(() {
                                      onlyClickOnce = onlyClickOnce + 1;
                                    });

                                    if (onlyClickOnce == 1) {
                                      await Api().postHTTP('PasswordReset', {"loginUsername": usernameController.text}).then((response) {
                                        if (response.data['status']) {
                                          mailSent = true;
                                          updateErrMsg("");
                                          resetPassToken = response.data['response']['token'];
                                          emailReceiver = response.data['response']['teamMemberEmail'];
                                        }
                                        else {
                                          usernameMatching = false;
                                          updateErrMsg(response.data['message']);
                                        }
                                      }).timeout(Duration(seconds: 60), onTimeout: () {
                                        setState(() {
                                          onlyClickOnce = 0;
                                        });
                                      }).whenComplete(() {
                                        setState(() {
                                          onlyClickOnce = 0;
                                        });
                                      });
                                    }
                                  }
                                } catch(e) {
                                  showError(context, 'Something went wrong, please try again!');
                                }
                              }),
                            ),
                          ),
                        ) // Reset Password Button
                      )
                    ],
                  )
                ],
              ),
            ),
            Transform.translate(
                offset: Offset(MediaQuery.of(context).size.width * 0.40, MediaQuery.of(context).size.height * 0.65),
                child: Lottie.asset(
                  'assets/lotties/auth.json',
                  width: MediaQuery.of(context).size.width * 0.65,
                  height: MediaQuery.of(context).size.height * 0.35,
                  fit: BoxFit.fill,
                )
            )
            ]
        ),
      ),
    );
  }
}