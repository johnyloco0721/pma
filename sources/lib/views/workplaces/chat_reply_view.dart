import 'dart:async';
import 'dart:io';

import 'package:bubble/bubble.dart';
import 'package:clipboard/clipboard.dart';
import 'package:cron/cron.dart';
import 'package:dio/dio.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:drift/drift.dart' as DRIFT;
import 'package:path/path.dart' as p;
import 'package:sources/args/chat_reply_args.dart';
import 'package:sources/args/forward_args.dart';
import 'package:sources/args/media_slider_args.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/services/api.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/widgets/video_thumbnail_widget.dart';
import 'package:uuid/uuid.dart';
import 'package:open_filex/open_filex.dart';

import 'package:sources/constants/app_constants.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/database/feeds.dart';
import 'package:sources/models/feed_post_model.dart';
import 'package:sources/models/general_response_model.dart';
import 'package:sources/services/feed_service.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/size_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/view_models/app_view_model.dart';
import 'package:sources/view_models/workplace_view_model.dart';
import 'package:sources/widgets/avatar.dart';
import 'package:sources/widgets/backdrop.dart';
import 'package:sources/widgets/chat_bubble.dart';
import 'package:sources/widgets/content_widget.dart';
import 'package:sources/widgets/custom_mentions/custom_mention_view.dart';
import 'package:sources/widgets/label_separator.dart';
import 'package:sources/widgets/post_form.dart';
import 'package:sources/widgets/skeleton.dart';
import 'package:sources/widgets/system_bubble.dart';
import 'package:sources/widgets/topic_pill.dart';

class WorkplaceReplyChatView extends StatefulWidget {
  final ChatReplyArguments replyParentArguments;
  const WorkplaceReplyChatView({Key? key, required this.replyParentArguments}) : super(key: key);

  @override
  _WorkplaceReplyChatViewState createState() => _WorkplaceReplyChatViewState();
}

class _WorkplaceReplyChatViewState extends State<WorkplaceReplyChatView> {
  final database = locator<AppDatabase>();
  final AppViewModel appViewModel = locator<AppViewModel>();
  final WorkplaceViewModel workplaceViewModel = locator<WorkplaceViewModel>();
  FeedService _feedService = FeedService();
  FeedWithDetail? replyParentFeedWithDetail;
  bool isLoading = false;
  bool isEmpty = false;
  String workplaceStatus = 'read';
  Workplace? currentWorkplace;
  int? currentProjectID;
  int? currentParentFeedID;
  int replyCount = 0;
  int onlyClickOnce = 0;

  final AutoScrollController autoScrollController = new AutoScrollController(axis: Axis.vertical);
  GlobalKey<CustomMentionsState> replyTextfieldKey = GlobalKey<CustomMentionsState>(debugLabel: 'ReplyChatTextKey');
  List<Map<String, dynamic>> mentionsList = [];
  String lastUpdateDateTime = '';

  Cron? cron;

  @override
  void initState() {
    // implement initState
    super.initState();
    isLoading = true;

    database.workplaceDao.getSingleWorkplace(widget.replyParentArguments.projectID).then((workplace) {
      if (workplace != null) {
        setState(() {
          currentWorkplace = workplace;
          workplaceStatus = workplace.isRead ?? 'read';
        });
      }
    });
    currentProjectID = widget.replyParentArguments.projectID;
    currentParentFeedID = widget.replyParentArguments.parentFeedID;

    database.teamMemberDao.getTeamMembersByProject(currentProjectID!).then((List<TeamMember> teamMembers) {
      teamMembers.forEach((member) {
        mentionsList.add({
          'id': member.teamMemberID.toString(),
          'display': member.teamMemberName,
          'teamMemberName': member.teamMemberName,
          'avatar': member.teamMemberAvatar,
          'designation': member.designationName,
        });
      });
    });

    _feedService.syncDeletedFeeds(currentProjectID!);

    // Fetch reply feeds data from API every 15 seconds
    cron = Cron()..schedule(Schedule.parse('*/5 * * * * *'), () async {
      await database.feedDao.getLatestUpdateReplyFeed(currentParentFeedID!).then((latestUpdateWorkplace) {
        lastUpdateDateTime = latestUpdateWorkplace!.updatedDateTime!;
      }).onError((error, stackTrace) {
        print('Latest Update Datetime no found.');
      });
      _feedService.fetchAndSaveFeedRepliesToDB(projectID: currentProjectID!, feedID: currentParentFeedID!, lastUpdateDateTime: lastUpdateDateTime).then((value) {
        // checkNewUnread();
      })
      .onError((error, stackTrace) {
        print('Syncing data failed!');
      });
      _feedService.syncDeletedFeeds(currentProjectID!);
    });

    initialize();
  }

  void initialize() async {
    setState(() {
      isLoading = true;
    });
    await database.feedDao.getSingleFeed(currentParentFeedID!).then((feed) async {
      await database.workplaceDao.getSingleWorkplace(currentProjectID!).then((workplace) async {
        await database.topicDao.getSingleTopic(feed!.topicID!).then((topic) async {
          await database.teamMemberDao.getSingleTeamMember(feed.projectID, feed.teamMemberID!).then((member) {
            FeedWithDetail feedWithDetail = FeedWithDetail(
              feedDetail: feed, 
              workplaceDetail: workplace, 
              topicDetail: topic,
              teamMemberDetail: member
            );
    
            setState(() {
              database.feedDao.updateFeed(feed.copyWith(replyReadStatus: DRIFT.Value('read')));
              replyParentFeedWithDetail = feedWithDetail;
            });
          });
        });
      });
    });
    await database.feedDao.getLatestUpdateReplyFeed(currentParentFeedID!).then((latestUpdateWorkplace) {
      lastUpdateDateTime = latestUpdateWorkplace!.updatedDateTime!;
    }).onError((error, stackTrace) {
      print('Latest Update Datetime no found.');
    });
    await _feedService.fetchAndSaveFeedRepliesToDB(projectID: currentProjectID!, feedID: currentParentFeedID!, lastUpdateDateTime: lastUpdateDateTime).then((value) {
      // print('Initiate reply data from API to Local DB failed!');
      setState(() {
        isLoading = false;
      });
    });
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      checkNewUnread();
    });
  }

  void checkNewUnread() {
    database.feedDao.getReplyFeeds(currentParentFeedID!).then((feeds) {
      var unreadFeeds = feeds.where((e) => e.isRead == 'unread');
      var selfFeeds = unreadFeeds.where((e) => e.teamMemberID == workplaceViewModel.currentUser!.teamMemberID);
      if (selfFeeds.length > 0) {
        unreadFeeds = unreadFeeds.where((e) => e.feedID > selfFeeds.first.feedID);
      }
      var totalReplyUnreads = unreadFeeds.length;
      var unreadIndex = 0;
      if (totalReplyUnreads > 0) {
        feeds.asMap().forEach((index, feed) { 
          if (feed.feedID == unreadFeeds.last.feedID) {
            unreadIndex = index;
          }
        });
      }
      workplaceViewModel.isShowReplyNewUnreads.value = totalReplyUnreads > 0 ? true : false;
      workplaceViewModel.totalReplyUnreads.value = totalReplyUnreads;
      workplaceViewModel.lastReplyUnreadIndex.value = unreadIndex;
    });
  }

  downloadMediaToCache(List<Media> mediaList) async {
    List<String> tmpLocalURLs = [];
    await Future.forEach(mediaList, (Media media) async {
      String tmpLocalURL = '';
      if (media.localFilePath != null && await File(media.localFilePath ?? '').exists()) {
        tmpLocalURL = media.localFilePath!;
      } else {
        tmpLocalURL = await Api().downloadFile(media.url ?? '', 'img', context, isShow: false, saveToGallery: false);
        await database.mediaDao.insertOrUpdateMedia(media.copyWith(localFilePath: DRIFT.Value(tmpLocalURL)));
      }
      tmpLocalURLs.add(tmpLocalURL);
    });

    return tmpLocalURLs;
  }

  void submitForm(String formValue)  async {
    var newUUID = Uuid().v4();
    int newFeedID = -1;
    int newMediasID = -1;
    List<int> mediaIds = [];
    bool uplaodImageFailed = false;

    await database.teamMemberDao.getSingleTeamMember(currentProjectID!, workplaceViewModel.currentUser!.teamMemberID!).then((teamMember) {
      if (teamMember == null && (workplaceViewModel.currentUser!.access == 'superadmin' || workplaceViewModel.currentUser!.access == 'hod')) {
        database.teamMemberDao.insertTeamMember(TeamMembersCompanion(
          teamMemberID: DRIFT.Value(workplaceViewModel.currentUser!.teamMemberID!),
          teamMemberName: DRIFT.Value(workplaceViewModel.currentUser!.name),
          teamMemberAvatar: DRIFT.Value(workplaceViewModel.currentUser!.avatar),
          designationName: DRIFT.Value(workplaceViewModel.currentUser!.designationName),
          projectID: DRIFT.Value(currentProjectID!),
          show: DRIFT.Value(false),
        ));
      }
    });

    await database.mediaDao.getLowestMediaID().then((mediaData) {
      if(mediaData.length > 0) {
        if(mediaData[0].mediaID < 0)
          newMediasID = mediaData[0].mediaID - 1;
      }
    });

    await database.feedDao.getLowestFeedID().then((feedData) async {
      if(feedData.length > 0) {
        if(feedData[0].feedID < 0)
          newFeedID = feedData[0].feedID - 1;
      }

      if (workplaceViewModel.mediaPdfFiles.value.length > 0) {
        for (var mediaFile in workplaceViewModel.mediaPdfFiles.value) {
          database.mediaDao.insertMedia(MediasCompanion(
            mediaID: DRIFT.Value(newMediasID),
            projectID: DRIFT.Value(currentProjectID!),
            feedID: DRIFT.Value(newFeedID),
            fileExtension: DRIFT.Value(p.extension(mediaFile).substring(1)),
            localFilePath: DRIFT.Value(mediaFile),
            feedUuid: DRIFT.Value(newUUID),
          ));
          
          final formData = FormData.fromMap({'fileUpload': await MultipartFile.fromFile(mediaFile)});
          await _feedService.postMedia(formData).then((response) {
            GeneralResponseModel mediaResponse = GeneralResponseModel.fromJson(response.data);

            database.mediaDao.updateMediaData(newMediasID, mediaResponse.response['mediaID'], mediaResponse.response['url']);
            mediaIds.add(mediaResponse.response['mediaID']);
          }).onError((error, stackTrace) {
            uplaodImageFailed = true;
            showError(context, error.toString());
          }).timeout(Duration(seconds: 60), onTimeout: () {
            setState(() {
              onlyClickOnce = 0;
            });
          }).whenComplete(() {
            setState(() {
              onlyClickOnce = 0;
            });
          });
          newMediasID--;
        }
      }

      if (workplaceViewModel.mediaImageFiles.value.length > 0) {
        for (var mediaFile in workplaceViewModel.mediaImageFiles.value) {
          database.mediaDao.insertMedia(MediasCompanion(
            mediaID: DRIFT.Value(newMediasID),
            projectID: DRIFT.Value(currentProjectID!),
            feedID: DRIFT.Value(newFeedID),
            fileExtension: DRIFT.Value(p.extension(mediaFile).substring(1)),
            localFilePath: DRIFT.Value(mediaFile),
            feedUuid: DRIFT.Value(newUUID),
          ));

          File renamedFile;
          if (Platform.isIOS) {
            String fileExtension = p.extension(mediaFile);
            String uniqueIdentifier = DateTime.now().millisecondsSinceEpoch.toString();
            String shortenedFileName = p.basenameWithoutExtension(mediaFile).substring(0, 10) + '_' + uniqueIdentifier + fileExtension;
            String shortenedFilePath = p.join(p.dirname(mediaFile), shortenedFileName);
            renamedFile = File(mediaFile).renameSync(shortenedFilePath);
          } else {
            renamedFile = File(mediaFile);
          }
          
          final formData = FormData.fromMap({'fileUpload': await MultipartFile.fromFile(renamedFile.path)});
          await _feedService.postMedia(formData).then((response) {
            GeneralResponseModel mediaResponse = GeneralResponseModel.fromJson(response.data);

            database.mediaDao.updateMediaData(newMediasID, mediaResponse.response['mediaID'], mediaResponse.response['url']);
            mediaIds.add(mediaResponse.response['mediaID']);
          }).onError((error, stackTrace) {
            uplaodImageFailed = true;
            showError(context, error.toString());
          }).timeout(Duration(seconds: 60), onTimeout: () {
            setState(() {
              onlyClickOnce = 0;
            });
          }).whenComplete(() {
            setState(() {
              onlyClickOnce = 0;
            });
          });
          newMediasID--;
        }
      }

      FeedPostModel formData = FeedPostModel(
        projectID: currentProjectID!,
        topicID: 1,
        description: formValue,
        replyID: currentParentFeedID,
        mediaID: mediaIds,
        uuid: newUUID
      );

      final convertedDesc = formValue.replaceAllMapped(RegExp(r'@\[\d+\]'), (match) {
        int stringLength = match.group(0)?.length ?? 0;
        if (stringLength > 0) {
          var matchID = match.group(0)?.substring(2, stringLength - 1);
          var matchedMember = mentionsList.firstWhere((member) => member['id'] == matchID);
          String replaceName = matchedMember['teamMemberName'];

          return '<b style="color: #B0782A;">$replaceName</b>';
        }
        return '${match.group(0)}';
      });

      database.feedDao.insertFeed(FeedsCompanion(
        uuid: DRIFT.Value(formData.uuid),
        feedID: DRIFT.Value(newFeedID),
        parentID: DRIFT.Value(formData.replyID),
        projectID: DRIFT.Value(formData.projectID),
        description: DRIFT.Value(convertedDesc),
        feedType: DRIFT.Value("normal"),
        postDateTime: DRIFT.Value(formatDatetime(datetime: DateTime.now().toString(), format: 'yyyy-MM-dd HH:mm:ss')),
        topicID: DRIFT.Value(formData.topicID),
        teamMemberID: DRIFT.Value(workplaceViewModel.currentUser!.teamMemberID),
        replyTeamMemberAvatar: DRIFT.Value(''),
        replyCount: DRIFT.Value(0),
        replyReadStatus: DRIFT.Value("read"),
        sendStatus: DRIFT.Value(uplaodImageFailed ? 0 : 2),
        isRead: DRIFT.Value("read")
      ));
      
      if (!uplaodImageFailed) {
        _feedService.postNewFeed(formData).then((response) {
          GeneralResponseModel result = GeneralResponseModel.fromJson(response.data);
          if (result.status == true) {
            database.feedDao.updateFeedStatus(newUUID, response.data["response"]["feedID"], 1);
            database.mediaDao.updateMediaFeedID(newFeedID, response.data["response"]["feedID"]);
            _feedService.updateParentFeed(formData.replyID!);
          } else {
            if (response.data["response"]["projectStatus"] == "archive") {
              database.feedDao.deleteAllUnsentFeeds();
              database.workplaceDao.updateWorkplace(currentWorkplace!.copyWith(projectStatus: DRIFT.Value("archive")));
              setState(() {
                workplaceStatus = 'archive';
              });
            } else {
              database.feedDao.updateFeedStatus(newUUID, newFeedID, 0);
            }
            database.feedDao.updateFeedStatus(newUUID, newFeedID, 0);
            showError(context, result.message ?? "");
          }
        })
        .onError((error, stackTrace) {
          database.feedDao.updateFeedStatus(newUUID, newFeedID, 0);
          showError(context, error.toString());
        }).timeout(Duration(seconds: 60), onTimeout: () {
          setState(() {
            onlyClickOnce = 0;
          });
        }).whenComplete(() {
          setState(() {
            onlyClickOnce = 0;
          });
        });
      }
    });

    workplaceViewModel.temporaryTextFieldValue.value = null;
    workplaceViewModel.clearForm();
    if (replyTextfieldKey.currentState != null) {
      replyTextfieldKey.currentState!.controller!.text = '';
      replyTextfieldKey.currentState!.controller!.clear();
    }
    workplaceViewModel.isPostFeedLoading.value = false;
  }

  void submitUpdate(String formValue) async {
    List<int> mediaIds = workplaceViewModel.selectedReplyFeedWithDetail.value?.mediaList?.map((e) => e.mediaID).toList() ?? [];
    bool uplaodImageFailed = false;
    await Future.forEach(workplaceViewModel.mediaPdfFiles.value, (dynamic mediaPath) async {
      if (mediaPath[0] == '/') {
        final formData = FormData.fromMap({'fileUpload': await MultipartFile.fromFile(mediaPath)});
        await _feedService.postMedia(formData).then((response) {
          GeneralResponseModel mediaResponse = GeneralResponseModel.fromJson(response.data);
          mediaIds.add(mediaResponse.response['mediaID']);
        })
        .onError((error, stackTrace) {
          uplaodImageFailed = true;
          showError(context, error.toString());
        });
      }
    }).timeout(Duration(seconds: 60), onTimeout: () {
      setState(() {
        onlyClickOnce = 0;
      });
    }).whenComplete(() {
      setState(() {
        onlyClickOnce = 0;
      });
    });

    await Future.forEach(workplaceViewModel.mediaImageFiles.value, (dynamic mediaPath) async {
      if (mediaPath[0] == '/') {
        File renamedFile;
        if (Platform.isIOS) {
          String fileExtension = p.extension(mediaPath);
          String uniqueIdentifier = DateTime.now().millisecondsSinceEpoch.toString();
          String shortenedFileName = p.basenameWithoutExtension(mediaPath).substring(0, 10) + '_' + uniqueIdentifier + fileExtension;
          String shortenedFilePath = p.join(p.dirname(mediaPath), shortenedFileName);
          renamedFile = File(mediaPath).renameSync(shortenedFilePath);
        } else {
          renamedFile = File(mediaPath);
        }

        final formData = FormData.fromMap({'fileUpload': await MultipartFile.fromFile(renamedFile.path)});
        await _feedService.postMedia(formData).then((response) {
          GeneralResponseModel mediaResponse = GeneralResponseModel.fromJson(response.data);
          mediaIds.add(mediaResponse.response['mediaID']);
        })
        .onError((error, stackTrace) {
          uplaodImageFailed = true;
          showError(context, error.toString());
        });
      }
    }).timeout(Duration(seconds: 60), onTimeout: () {
      setState(() {
        onlyClickOnce = 0;
      });
    }).whenComplete(() {
      setState(() {
        onlyClickOnce = 0;
      });
    });

    FeedPutModel formData = FeedPutModel(
      feedID: workplaceViewModel.selectedReplyFeedWithDetail.value!.feedDetail.feedID,
      topicID: 1,
      mediaID: mediaIds,
      description: formValue,
    );

    if (!uplaodImageFailed) {
      _feedService.updateFeed(formData).then((response) {
        GeneralResponseModel result = GeneralResponseModel.fromJson(response.data);
        if (result.status == true) {
          workplaceViewModel.temporaryTextFieldValue.value = null;
          workplaceViewModel.clearForm();
          hideToolbox();
          _feedService.fetchAndSaveFeedsToDB(projectID: currentProjectID!);
          _feedService.fetchAndSaveFeedRepliesToDB(projectID: currentProjectID!, feedID: currentParentFeedID!);
        } else {
          showError(context, result.message ?? "");
        }
        workplaceViewModel.isPostFeedLoading.value = false;
      })
      .onError((error, stackTrace) {
        showError(context, error.toString());
        workplaceViewModel.isPostFeedLoading.value = false;
      }).timeout(Duration(seconds: 60), onTimeout: () {
      setState(() {
        onlyClickOnce = 0;
      });
    }).whenComplete(() {
      setState(() {
        onlyClickOnce = 0;
      });
    });
    }
    workplaceViewModel.isPostFeedLoading.value = false;
  }

  void showToolbox(FeedWithDetail feedWithDetail) {
    workplaceViewModel.replytoolboxMode.value = 'show';
    workplaceViewModel.selectedReplyFeedWithDetail.value = feedWithDetail;
  }

  void hideToolbox() {
    workplaceViewModel.replytoolboxMode.value = 'none';
    workplaceViewModel.replyFeedDescriptionNode.unfocus();
    workplaceViewModel.mediaImageFiles.value = [];
    workplaceViewModel.mediaPdfFiles.value = [];

    if (replyTextfieldKey.currentState != null) {
      replyTextfieldKey.currentState!.controller!.text = '';
      replyTextfieldKey.currentState!.controller!.clear();
    }
  }

  @override
  void dispose() async {
    // implement dispose
    super.dispose();
    // update all feeds read status when back button is clicked.
    database.feedDao.updateReadStatus(currentProjectID!, currentParentFeedID);
    // call feeds list API to make sure latest feeds list will be loaded when back fromm reply chat viewZ
    _feedService.fetchAndSaveFeedsToDB(projectID: currentProjectID!, lastUpdateDateTime: lastUpdateDateTime);

    cron?.close();
    autoScrollController.dispose();
    workplaceViewModel.isShowReplyNewUnreads.value = false;
    workplaceViewModel.isPostFeedLoading.value = false;
    workplaceViewModel.replytoolboxMode.value = 'none';
    workplaceViewModel.isShowReplyScrollBottomButton.value = false;
    mentionsList.clear();
  }

  @override
  Widget build(BuildContext context) {

    autoScrollController.addListener(() {
      if (autoScrollController.position.pixels > 100) {
        workplaceViewModel.isShowReplyScrollBottomButton.value = true;
      } else {
        workplaceViewModel.isShowReplyScrollBottomButton.value = false;
      }
    });

    return Scaffold(
      key: PageStorageKey<String>('WorkplaceChatReply'),
      backgroundColor: white,
      appBar: _buildAppBar(context),
      body: RefreshIndicator(
        color: yellow,
        backgroundColor: white,
        onRefresh: () {
          return _feedService.fetchAndSaveFeedRepliesToDB(projectID: currentProjectID!, feedID: currentParentFeedID!, lastUpdateDateTime: lastUpdateDateTime);
        },
        child: Stack(
          alignment: Alignment.center,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                // Feeds list, Scroll to Bottom & backdrop
                Expanded(
                  child: Stack(
                    children: [
                      // Build feed listing
                      _buildFeedList(context),
                      // Build scroll to bottom button
                      Positioned(
                        bottom: 15,
                        right: 15,
                        child: _scrollToBottomButton(),
                      ),
                      // Build backdrop here when it is edit mode
                      ValueListenableBuilder(
                        valueListenable: workplaceViewModel.replytoolboxMode,
                        builder: (context, String replytoolboxMode, Widget? child) {
                          return replytoolboxMode == 'edit' ? ClipPath(child: Backdrop(onTap: () => hideToolbox())) : Container();
                        }
                      ),
                      ValueListenableBuilder(
                        valueListenable: workplaceViewModel.replytoolboxMode, 
                        builder: (context, String replytoolboxMode, Widget? child) {
                          return replytoolboxMode == 'edit' ? _buildSelectedBubbleWithToolbox() : Container();
                        }
                      ),
                    ],
                  ),
                ),
                // Form, if workplace archive will hide
                if (workplaceStatus != 'archive')
                  Container(
                    child: ValueListenableBuilder(
                      valueListenable: workplaceViewModel.replytoolboxMode, 
                      builder: (BuildContext context, String replytoolboxMode, Widget? child) {
                        return PostForm(
                          textfieldKey: replyTextfieldKey,
                          editMode: (replytoolboxMode == 'edit') ? true : false,
                          showTopicButton: false,
                          teamMembers: mentionsList,
                          focusNode: workplaceViewModel.replyFeedDescriptionNode,
                          onTextChanged: (text) {
                            workplaceViewModel.temporaryTextFieldValue.value = text;
                          },
                          submitOnTap: () {
                            String formValue = replyTextfieldKey.currentState!.controller!.markupText.trim();
                            if (formValue != '' || workplaceViewModel.mediaPdfFiles.value.length > 0 || workplaceViewModel.mediaImageFiles.value.length > 0) {
                              setState(() {
                                onlyClickOnce = onlyClickOnce + 1;
                              });
                              workplaceViewModel.isPostFeedLoading.value = true;
                              if (onlyClickOnce == 1)
                              submitForm(formValue);
                              // update all feeds read status when send button is clicked.
                              database.feedDao.updateReadStatus(currentProjectID!, currentParentFeedID);
                              workplaceViewModel.isShowReplyNewUnreads.value = false;
                              autoScrollController.jumpTo(0);
                            }
                          },
                          updateOnTap: () {
                            String formValue = replyTextfieldKey.currentState!.controller!.markupText;
                            if (formValue != '' || workplaceViewModel.mediaPdfFiles.value.length > 0 || workplaceViewModel.mediaImageFiles.value.length > 0) {
                              setState(() {
                                onlyClickOnce = onlyClickOnce + 1;
                              });
                              workplaceViewModel.isPostFeedLoading.value = true;
                              if (onlyClickOnce == 1)
                              submitUpdate(formValue);
                              // update all feeds read status when send button is clicked.
                              database.feedDao.updateReadStatus(currentProjectID!, currentParentFeedID);
                            }
                          },
                          isDisable: onlyClickOnce >= 1,
                        );
                      }
                    ),
                  ),
              ],
            ),
            // Build backdrop when it's not edit mode
            ValueListenableBuilder(
              valueListenable: workplaceViewModel.replytoolboxMode, 
              builder: (context, String replytoolboxMode, Widget? child) {
                return replytoolboxMode == 'show' ? ClipPath(child: Backdrop(onTap: () => hideToolbox())) : Container();
              }
            ),
            // Build selected bubble with toolbox
            ValueListenableBuilder(
              valueListenable: workplaceViewModel.replytoolboxMode, 
              builder: (context, String replytoolboxMode, Widget? child) {
                return replytoolboxMode != 'none' && replytoolboxMode != 'edit' ? _buildSelectedBubbleWithToolbox() : Container();
              }
            ),
          ],
        ),
      ),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.light,
      centerTitle: false,
      backgroundColor: black,
      leading: IconButton(
        onPressed: () {
          hideToolbox();
          Navigator.pop(context);
        },
        icon: SvgPicture.asset(ic_chevron_left, width: 10.0),
      ),
      title: Text(
        'Reply In Thread',
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, color: white, letterSpacing: 0.8),
      ),
      flexibleSpace: GestureDetector(
        onTap: () {
          // onTap app bar empty space then scroll up a bit
          if (autoScrollController.positions.isNotEmpty) {
            if (autoScrollController.offset + 3000.0 < autoScrollController.position.maxScrollExtent)
              autoScrollController.animateTo(autoScrollController.offset + 3000.0, duration: Duration(milliseconds: 1000), curve: Curves.ease);
            else
              autoScrollController.animateTo(autoScrollController.position.maxScrollExtent, duration: Duration(milliseconds: 1000), curve: Curves.ease);
          }
        },
      ),
      // only show when workplace is archive
      bottom: (workplaceStatus == 'archive') ? PreferredSize(
        child: Container(
          color: yellow,
          height: 42.0,
          width: double.infinity,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'This project is archived. ',
                style: regularFontsStyle(fontSize: 14.0),
              ),
            ],
          ),
        ),
        preferredSize: Size.fromHeight(42.0)
      ) : PreferredSize(child: Container(), preferredSize:  Size.fromHeight(0.0)),
    );
  }

  // Workplace feeds stream builder
  StreamBuilder<List<FeedWithDetail>> _buildFeedList(BuildContext context) {
    return StreamBuilder(
      stream: database.feedDao.watchReplyFeedsWithDetail(currentProjectID!, currentParentFeedID!),
      builder: (BuildContext context, AsyncSnapshot<List<FeedWithDetail>> snapshot) {
        List<FeedWithDetail> feeds = snapshot.data ?? []; 
        int itemCount = snapshot.hasData ? snapshot.data?.length as int : 0;
        int totalItems = itemCount + 1;
        var unreadFeedID = -1;
        var unreadFeeds = feeds.where((e) => e.feedDetail.isRead == "unread");
        var selfFeeds = unreadFeeds.where((e) => e.feedDetail.teamMemberID == workplaceViewModel.currentUser!.teamMemberID);
        if (selfFeeds.length > 0) {
          unreadFeeds = unreadFeeds.where((e) => e.feedDetail.feedID > selfFeeds.first.feedDetail.feedID);
        }
        if (unreadFeeds.length > 0) {
          unreadFeedID = unreadFeeds.last.feedDetail.feedID;
        }

        if (isLoading) {
          return _buildSkeleton(context);
        }
        // else if (!isLoading && itemCount == 0) {
        //   return _buildEmptyResult(context);
        // }
        else {
          return Stack(
            alignment: Alignment.topCenter,
            children: [
              ListView.builder(
                addAutomaticKeepAlives: true,
                controller: autoScrollController,
                physics: workplaceViewModel.replytoolboxMode.value != 'none' ? const NeverScrollableScrollPhysics() : const AlwaysScrollableScrollPhysics(),
                key: PageStorageKey<String>('WorkplaceChatReply' + currentParentFeedID.toString()),
                itemCount: totalItems,
                // Sort newest chat item at the bottom & make the list always stick to bottom
                reverse: true,
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  if (index == (itemCount)) {
                    return _buildParentFeed(context, replyParentFeedWithDetail!);
                  }
                  else {
                    bool showDate = false;
                    bool showUnreadLabel = false;
                    final FeedWithDetail feedWithDetail = feeds[index];
                    FeedWithDetail? nextfeedWithDetail;
                    DateTime now = DateTime.now();
                    var currentDate = formatDatetime(datetime: feedWithDetail.feedDetail.postDateTime ?? '');
                    var todayDate = formatDatetime(datetime: now.toString());
                    if (index < (itemCount - 1)) {
                      int nextIndex = index + 1;
                      nextfeedWithDetail = feeds[nextIndex];
                      var nextDate = formatDatetime(datetime: nextfeedWithDetail.feedDetail.postDateTime ?? '', format: 'd MMM y');
                      if (currentDate != nextDate) {
                        showDate = true;
                      }
                    }
                    // If the feed is the oldest feed
                    else if (index == (itemCount - 1)) {
                      showDate = true;
                    }
                    if (unreadFeedID == feedWithDetail.feedDetail.feedID && feedWithDetail.teamMemberDetail!.teamMemberID != workplaceViewModel.currentUser!.teamMemberID) {
                      showUnreadLabel = true;
                      workplaceViewModel.lastReplyUnreadIndex.value = index;
                    }
                    Topic? topic = feedWithDetail.topicDetail;
                  
                    bool showLabel = false;
                    if (showDate || showUnreadLabel) {
                      showLabel = true;
                    }

                    return Container(
                      padding: EdgeInsets.only(bottom: (index == 0) ? 30 : 0),
                      child: Column(
                        children: [
                          // Date separator
                          if (showDate)
                          LabelSeparator(label: (currentDate == todayDate) ? 'Today' : currentDate, color: grey),
                          // New label
                          if (showUnreadLabel)
                          LabelSeparator(label: (workplaceViewModel.lastReplyUnreadIndex.value + 1).toString() + ' New Unread', color: darkYellow),
                          // List item
                          (topic != null && topic.topicName == 'System') ? 
                          SystemBubble(feedWithDetail: feedWithDetail, index: index,showToolbox: false,) :
                          _buildListItem(context, feedWithDetail, nextfeedWithDetail, index, showLabel),
                        ],
                      ),
                    );
                  }
                }
              ),
              // Build New Unreads Button
              ValueListenableBuilder(
                valueListenable: workplaceViewModel.isShowReplyNewUnreads,
                builder: (context, bool isShowReplyNewUnreads, Widget? child) {
                  return isShowReplyNewUnreads ? Positioned(
                    top: 20,
                    child: GestureDetector(
                      onTap: () {
                        autoScrollController.scrollToIndex(
                          workplaceViewModel.lastReplyUnreadIndex.value, 
                          preferPosition: AutoScrollPosition.middle
                        ).then((value) {
                          // After user click, get latest parent feed from database
                          _feedService.updateParentFeed(currentParentFeedID!);
                          // Hide unread button
                          workplaceViewModel.isShowReplyNewUnreads.value = false;
                        });
                      },
                      child: _buildUnreadButton(),
                    ),
                  ) : Container();
                },
              ),
            ],
          );
        }
      }
    );
  }

  // Feed item widget
  Widget _buildListItem(BuildContext context, FeedWithDetail feedWithDetail, FeedWithDetail? nextfeedWithDetail, int index, bool showLabel) {
    bool showNip = true;
    String type = 'other';
    if (feedWithDetail.teamMemberDetail!.teamMemberID == workplaceViewModel.currentUser!.teamMemberID) {
      type = 'self';
    }
    if (nextfeedWithDetail != null && !showLabel) {
      if (nextfeedWithDetail.feedDetail.teamMemberID == feedWithDetail.feedDetail.teamMemberID) {
        showNip = false;
      }
    }
    return AutoScrollTag(
      key: ValueKey(index),
      controller: autoScrollController,
      index: index,
      child: ValueListenableBuilder(
        valueListenable: workplaceViewModel.replytoolboxMode,
        builder: (context, String replytoolboxMode, Widget? child) {
          bool showBubble = (workplaceViewModel.replytoolboxMode.value != 'none' && workplaceViewModel.selectedReplyFeedWithDetail.value!.feedDetail.feedID == feedWithDetail.feedDetail.feedID) ? false : true;
          return ChatBubble(
            key: ValueKey(index), 
            feedWithDetail: feedWithDetail, 
            index: index, 
            type: type,
            showBubble: showBubble, 
            showTopic: false,
            showNip: showNip,
            showToolbox: false,
            enableReplyButton: false,
            onResend: () async {
              List<int> mediaIds = [];
              bool uplaodImageFailed = false;
              database.feedDao.updateFeedStatus(feedWithDetail.feedDetail.uuid!, feedWithDetail.feedDetail.feedID, 2);

              var mediasList = await database.mediaDao.getMedias(feedWithDetail.feedDetail.feedID);
              for(var mediaData in mediasList) {
                if (mediaData.mediaID < 0) {
                  final formData = FormData.fromMap({'fileUpload': await MultipartFile.fromFile(mediaData.localFilePath!)});
                  await _feedService.postMedia(formData).then((response) {
                    GeneralResponseModel mediaResponse = GeneralResponseModel.fromJson(response.data);

                    database.mediaDao.updateMediaData(mediaData.mediaID, mediaResponse.response['mediaID'], mediaResponse.response['url']);
                    mediaIds.add(mediaResponse.response['mediaID']);
                  })
                  .onError((error, stackTrace) {
                    uplaodImageFailed = true;
                    showError(context, error.toString());
                  });
                }
                else {
                  mediaIds.add(mediaData.mediaID);
                }
              }

              FeedPostModel formData = FeedPostModel(
                projectID: feedWithDetail.feedDetail.projectID,
                topicID: feedWithDetail.feedDetail.topicID!,
                description: feedWithDetail.feedDetail.description!,
                replyID: feedWithDetail.feedDetail.parentID,
                mediaID: mediaIds,
                uuid: feedWithDetail.feedDetail.uuid!
              );

              if (!uplaodImageFailed) {
                _feedService.postNewFeed(formData).then((response) {
                  GeneralResponseModel result = GeneralResponseModel.fromJson(response.data);
                  if (result.status == true) {
                    database.feedDao.updateFeedStatus(feedWithDetail.feedDetail.uuid!, response.data["response"]["feedID"], 1);
                    database.mediaDao.updateMediaFeedID(feedWithDetail.feedDetail.feedID, response.data["response"]["feedID"]);
                  } else {
                    database.feedDao.updateFeedStatus(feedWithDetail.feedDetail.uuid!, feedWithDetail.feedDetail.feedID, 0);
                    showError(context, result.message ?? "");
                  }
                })
                .onError((error, stackTrace) {
                  database.feedDao.updateFeedStatus(feedWithDetail.feedDetail.uuid!, feedWithDetail.feedDetail.feedID, 0);
                  showError(context, error.toString());
                });
              }
            },
            onLongPressCallback: (offset) {
              if (workplaceStatus != 'archive') {
                RenderBox bubbleSize = context.findRenderObject() as RenderBox;
                double statusBarHeight = MediaQuery.of(context).padding.top;
                double appBarHeight = _buildAppBar(context).preferredSize.height;
                double maxOffsetY = MediaQuery.of(context).size.height - statusBarHeight - appBarHeight - 64;
                double originalOffsetY = offset.y;
                double minOffset = maxOffsetY / 6;
                // offset Y checking
                if ((originalOffsetY + bubbleSize.size.height) > maxOffsetY) {
                  offset.y = minOffset;
                } else {
                  offset.y = maxOffsetY - (originalOffsetY + bubbleSize.size.height);
                }
                // min offset Y checking
                if (offset.y < 0.0) {
                  offset.y = minOffset;
                }
                workplaceViewModel.selectedReplyBubbleOffset.value = offset.y;
                workplaceViewModel.selectedReplyFeedWithDetail.value = feedWithDetail;
                showToolbox(feedWithDetail);
              }
            },
            onPdfClicked: (Media media) async {
              try {
                late String file;
                if (media.localFilePath != null && await File(media.localFilePath ?? '').exists()){
                  file = media.localFilePath!;
                } else {
                  await SpManager.putInt(media.url.toString(), 50);
                  showToastMessage(context, message: 'Downloading', duration: 1);
                  await Api().downloadFile(media.url ?? '', 'pdf', context).then((localSavePath) {
                    file = localSavePath;
                    final newMediaData = media.copyWith(
                      localFilePath: DRIFT.Value(file)
                    );
                    database.mediaDao.insertOrUpdateMedia(newMediaData);
                  });
                }

                OpenFilex.open(file);
              } 
              on Exception catch (_) {
                showError(context, 'Unexpected error, please try again.');
              }
            },
            onImageClicked: (List<Media> medias, List<Feed> feeds, int index) {
              List<MediaInfoArguments> mediaArgsList = medias.map((m) => MediaInfoArguments(
                mediaPath: m.url ?? '',
                mediaDescription: feeds[index].description,
                mediaType: m.fileExtension ?? '',
                datetime: feeds[index].postDateTime
              )).toList();

              goToNextNamedRoute(context, mediaSliderScreen, args: MediaSliderArguments(mediaArgsList, index));
            },
          );
        }
      ),
    );
  }
  
  // Parent feed widget
  Widget _buildParentFeed(BuildContext context, FeedWithDetail feedWithDetail) {
    Feed feed = feedWithDetail.feedDetail;
    Topic topic = feedWithDetail.topicDetail!;
    var currentDate = formatDatetime(datetime: feed.postDateTime ?? '', format: 'd MMM y');
    var currentTime = formatDatetime(datetime: feed.postDateTime ?? '', format: 'h:mma');
    
    return Container(
      padding: EdgeInsets.all(24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Reply to title
          Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Text(
              'Reply to',
              style: regularFontsStyle(color: grey, fontSize: 15.0, height: 1.27),
            ),
          ),
          // User avatar & name
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              // User avatar
              Avatar(
                url: feedWithDetail.teamMemberDetail?.teamMemberAvatar ?? '',
                width: 26,
              ),
              // User name & designation
              Container(
                margin: EdgeInsets.only(top: 15),
                padding: EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      feedWithDetail.teamMemberDetail?.teamMemberName ?? '',
                      style: boldFontsStyle(fontSize: 17.0, height: 1.35),
                    ),
                    Text(
                      feedWithDetail.teamMemberDetail?.designationName ?? '',
                      style: regularFontsStyle(color: grey, fontSize: 15.0, height: 1.27),
                    ),
                  ],
                ),
              )
            ],
          ),
          // Feed message
          Container(
            padding: EdgeInsets.only(top: 16),
            child: ContentWidget(
              content: feed.description ?? '',
              readMoreColor: darkYellow,
              tagNameColor: darkYellow,
              fontSize: 19,
            ),
          ),
          // Empty space
          SizedBox(height: 10),
          // Media list
          Container(
            margin: EdgeInsets.only(bottom: 16),
            padding: EdgeInsets.only(right: 24),
            child: _buildMediaList(context, feedWithDetail, feed.feedID),
          ),
          SizedBox(height: 16),
          // Feed topic
          Row(
            children: [
              TopicPill(
                text: topic.topicName ?? '',
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: hexToColor(topic.topicColour!),
                borderColor: hexToColor(topic.topicColour!),
                margin: EdgeInsets.zero,
              ),
            ]
          ),
          // Empty space
          SizedBox(height: 8),
          // Date & time
          Row(
            children: [
              Text(
                currentDate,
                style: regularFontsStyle(color: grey, fontSize: 15.0, height: 1.27),
              ),
              Padding(
                padding: EdgeInsets.only(left: 8, right: 8),
                child: Icon(
                  Icons.circle,
                  color: lightGrey,
                  size: 4,
                ),
              ),
              Text(
                currentTime.toLowerCase(),
                style: regularFontsStyle(color: grey, fontSize: 15.0, height: 1.27),
              ),
            ],
          ),
          // Empty space
          SizedBox(height: 24),
          // Separator line
          Divider(thickness: 0.5, color: lightGrey),
          // Total replies
          StreamBuilder(
            stream: database.feedDao.watchReplyFeeds(feed.feedID),
            builder: (context, AsyncSnapshot<List<Feed>> snapshot) {
              int itemCount = snapshot.hasData ? snapshot.data?.length as int : 0;
              return Text(
                itemCount.toString() + ((itemCount > 1) ? ' Replies' : ' Reply'),
                style: boldFontsStyle(color: grey, fontSize: 15.0, height: 1.27),
              );
            }
          ),
          // Separator line
          Divider(thickness: 0.5, color: lightGrey),
        ],
      ),
    );
  }
  
  // Stream media list by feedID
  FutureBuilder<List<Media>> _buildMediaList(BuildContext context, FeedWithDetail feedWithDetail, int feedID) {

    return FutureBuilder(
      future: database.mediaDao.getMedias(feedID),
      builder: (BuildContext context, AsyncSnapshot<List<Media>> snapshot) {
        final dataList = snapshot.data ?? [];
        final medias = dataList.where((e) => e.fileExtension != 'pdf').toList();
        final docs = dataList.where((e) => e.fileExtension == 'pdf').toList();

        return LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            double imageWidth = (constraints.maxWidth - 30) / 3;
            return Column(
              children: [
                Wrap(
                  children: [
                    for (var index = 0; index < (medias.length <= 3 ? medias.length : 3); index++)
                    _buildMediaThumbnail(context, feedWithDetail, medias, index, imageWidth, false, medias.length),
                  ],
                ),
                Wrap(
                  children: [
                    for (var index = 0; index < (docs.length <= 3 ? docs.length : 3); index++)
                    _buildMediaThumbnail(context, feedWithDetail, docs, index, imageWidth, true, docs.length),
                  ],
                ),
                if (docs.length > 3)
                  ExpandableNotifier(
                    child: Expandable(
                      collapsed: ExpandableButton(
                        child: Text(
                          '${docs.length-3} More...',
                          style: boldFontsStyle(fontSize: 14.0, color: darkerYellow, decoration: TextDecoration.underline),
                        ),
                      ),
                      expanded: Wrap(
                        children: [
                          for (var index = 3; index < docs.length; index++)
                            _buildMediaThumbnail(
                              context,
                              feedWithDetail,
                              docs,
                              index,
                              imageWidth,
                              true,
                              docs.length
                            ),
                        ],
                      ),
                    )
                  ),
              ],
            );
          }
        );
      }
    );
  }

  Widget _buildMediaThumbnail(BuildContext context, FeedWithDetail feedWithDetail, List<Media> medias, int index, double imageWidth, bool isPdf, int itemCount) {
    bool showMore = false;
    if (!isPdf && itemCount > 3 && index == 2) {
      showMore = true;
    }
    List<Feed> feeds = medias.map((e) => feedWithDetail.feedDetail).toList();

    return GestureDetector(
      onTap: () async {
        if (medias[index].fileExtension == 'pdf') {
          try {
            late String file;
            if (medias[index].localFilePath != null && await File(medias[index].localFilePath ?? '').exists()){
              file = medias[index].localFilePath!;
            } else {
              await SpManager.putInt(medias[index].url.toString(), 50);
              showToastMessage(context, message: 'Downloading', duration: 1);
              await Api().downloadFile(medias[index].url ?? '', 'pdf', context).then((localSavePath) {
                file = localSavePath;
                final newMediaData = medias[index].copyWith(
                  localFilePath: DRIFT.Value(file)
                );
                database.mediaDao.insertOrUpdateMedia(newMediaData);
              });
            }

            OpenFilex.open(file);
          } 
          on Exception catch (_) {
            showError(context, 'Unexpected error, please try again.');
          }
        }
        else {
          List<MediaInfoArguments> mediaArgsList = medias.map((m) => MediaInfoArguments(
            mediaPath: m.url ?? '',
            mediaDescription: feeds[index].description,
            mediaType: m.fileExtension ?? '',
            datetime: feeds[index].postDateTime
          )).toList();

          goToNextNamedRoute(context, mediaSliderScreen, args: MediaSliderArguments(mediaArgsList, index));
        }
      },
      child: (medias[index].fileExtension == 'pdf') ? Container(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        margin: EdgeInsets.only(right: 10, bottom: 10),
        decoration: BoxDecoration(
          border: Border.all(),
          borderRadius: BorderRadius.all(Radius.circular(50.0)),
        ),
        child: Row(
          children: [
            SvgPicture.asset(ic_doc, width: 18.0),
            SizedBox(width: 8.0),
            if (medias[index].url != null || medias[index].localFilePath != null)
            Container(
              width: MediaQuery.of(context).size.width * 0.399,
              child: Text(
                (medias[index].url ?? medias[index].localFilePath)!.substring((medias[index].url ?? medias[index].localFilePath)!.lastIndexOf("/") + 1),
                style: regularFontsStyle(fontSize: 14.0),
              ),
            ),
          ],
        )
      )
      : Container(
        width: imageWidth,
        height: imageWidth,
        margin: EdgeInsets.only(right: 10, bottom: 10),
        child: Stack(
          children: [
            AspectRatio(
              aspectRatio: 1,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: (medias[index].fileExtension == 'mp4') ? VideoThumbnailWidget(
                  videoPath: medias[index].url  ?? medias[index].localFilePath!,
                  height: imageWidth,
                  width: imageWidth,
                ) : CachedNetworkImage(
                  placeholder: (context, url) => Container(color: Colors.grey[50],),
                  imageUrl: medias[index].url ?? '',
                  fit: BoxFit.cover,
                  alignment: Alignment.center,
                  maxHeightDiskCache: 120,
                  maxWidthDiskCache: 120,
                )
              ),
            ),
            if (showMore)
            Container(
              decoration: BoxDecoration(
                color: darkGrey,
                border: Border.all(color: Color(0x00ffffff)),
                borderRadius: BorderRadius.circular(12),
              ),
              child: Center(
                child: Text(
                  '+' + (itemCount - 2).toString() + ' more',
                  style: boldFontsStyle(color: white, fontSize: 15.0, height: 1.27),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Duplicated Selected Bubble
  Widget _buildSelectedBubbleWithToolbox() {
    String type = 'other';
    if (workplaceViewModel.selectedReplyFeedWithDetail.value!.teamMemberDetail!.teamMemberID == workplaceViewModel.currentUser!.teamMemberID) {
      type = 'self';
    }

    return ValueListenableBuilder(
      valueListenable: workplaceViewModel.replytoolboxMode, 
      builder: (context, String replytoolboxMode, Widget? child) {
        FeedWithDetail selectedReplyFeedWithDetail = workplaceViewModel.selectedReplyFeedWithDetail.value!;
        return Positioned(
          width: SizeUtils.width(context, 100),
          bottom: replytoolboxMode == 'edit' ? 135 : workplaceViewModel.selectedReplyBubbleOffset.value,
          left: 0,
          child: ChatBubble(
            key: ValueKey(selectedReplyFeedWithDetail.feedDetail.feedID),
            feedWithDetail: selectedReplyFeedWithDetail,
            index: 0, 
            type: type,
            showBubble: true, 
            showTopic: false,
            showNip: true,
            showToolbox: replytoolboxMode != 'none'  && replytoolboxMode != 'edit',
            enableReplyButton: false,
            onForward: () async {
              List<String> tmpLocalURLs = await downloadMediaToCache(selectedReplyFeedWithDetail.mediaList ?? []);

              goToNextNamedRoute(
                context, 'workplaceForward', 
                args: ForwardArguments(
                  removeAllHtmlTags(selectedReplyFeedWithDetail.feedDetail.description ?? ''),
                  tmpLocalURLs
                )
              );
              hideToolbox();
            },
            onShare: () async {
              workplaceShareDialog(
                context, 
                selectedReplyFeedWithDetail.feedDetail.description ?? '',
                selectedReplyFeedWithDetail.mediaList ?? []
              );
              hideToolbox();
            },
            onEdit: () {
              String putSymbol = (selectedReplyFeedWithDetail.feedDetail.description ?? '').replaceAll(RegExp(r'<h6[^>]*>'), '@');
              replyTextfieldKey.currentState!.controller!.text = Bidi.stripHtmlIfNeeded(putSymbol).trim();
              workplaceViewModel.replytoolboxMode.value = 'edit';
              workplaceViewModel.replyFeedDescriptionNode.requestFocus();
              if (selectedReplyFeedWithDetail.mediaList!.length > 0) {
                workplaceViewModel.mediaPdfFiles.value = selectedReplyFeedWithDetail.mediaList!.where((e) => e.fileExtension == 'pdf').map((e) => e.url ?? e.localFilePath).toList();
                workplaceViewModel.mediaImageFiles.value = selectedReplyFeedWithDetail.mediaList!.where((e) => e.fileExtension != 'pdf').map((e) => e.url ?? e.localFilePath).toList();
              }
            },
            onCopy: () async {
              List<String> tmpLocalURLs = await downloadMediaToCache(selectedReplyFeedWithDetail.mediaList ?? []);
              hideToolbox();
              FlutterClipboard.copy(
                removeAllHtmlTags(
                  reverseMarkupText(selectedReplyFeedWithDetail.feedDetail.description ?? '')
                )
              );
              workplaceViewModel.temporaryCopiesMediaFiles.value = tmpLocalURLs;

              showMessage(context, message: 'Copied!');
            },
            onDelete: () {
              hideToolbox();
              workplaceViewModel.isPostFeedLoading.value = true;
              _feedService.deleteFeed(selectedReplyFeedWithDetail.feedDetail)
              .then((value) {
                workplaceViewModel.isPostFeedLoading.value = false;
                _feedService.updateParentFeed(selectedReplyFeedWithDetail.feedDetail.parentID!);
              })
              .onError((error, stackTrace) {
                print('Something went wrong, please try again!');
              });
              workplaceViewModel.isPostFeedLoading.value = false;
            },
            onPdfClicked: (Media media) async {
              try {
                late String file;
                if (media.localFilePath != null && await File(media.localFilePath ?? '').exists()){
                  file = media.localFilePath!;
                } else {
                  await SpManager.putInt(media.url.toString(), 50);
                  showToastMessage(context, message: 'Downloading', duration: 1);
                  await Api().downloadFile(media.url ?? '', 'pdf', context).then((localSavePath) {
                    file = localSavePath;
                    final newMediaData = media.copyWith(
                      localFilePath: DRIFT.Value(file)
                    );
                    database.mediaDao.insertOrUpdateMedia(newMediaData);
                  });
                }

                OpenFilex.open(file);
              } 
              on Exception catch (_) {
                showError(context, 'Unexpected error, please try again.');
              }
            },
            onImageClicked: (List<Media> medias, List<Feed> feeds, int index) {
              List<MediaInfoArguments> mediaArgsList = medias.map((m) => MediaInfoArguments(
                mediaPath: m.url ?? '',
                mediaDescription: feeds[index].description,
                mediaType: m.fileExtension ?? '',
                datetime: feeds[index].postDateTime
              )).toList();

              goToNextNamedRoute(context, mediaSliderScreen, args: MediaSliderArguments(mediaArgsList, index));
            },
          ),
        );
      }
    );
  }

  // Build Unread Button
  Widget _buildUnreadButton() {
    return Container(
      padding: EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
      decoration: BoxDecoration(
        color: darkGrey,
        border: Border.all(color: darkGrey),
        borderRadius: BorderRadius.circular(30)
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            (workplaceViewModel.lastReplyUnreadIndex.value + 1).toString() + ' New Unread',
            style: boldFontsStyle(color: white, fontSize: 15.0),
            key: ValueKey(workplaceViewModel.lastReplyUnreadIndex.value),
          ),
          SizedBox(width: 10),
          GestureDetector(
            onTap: () {
              // After user click, get latest parent feed from database
              _feedService.updateParentFeed(currentParentFeedID!);
              // Hide unread button
              workplaceViewModel.isShowReplyNewUnreads.value = false;
            },
            child: SvgPicture.asset(ic_close, width: 12.0, color: white),
          ),
        ],
      ),
    );
  }

  // Parent feed skeleton widget
  Widget _buildParentFeedSkeleton(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Reply to title skeleton
          SkeletonContainer.rounded(width: 80, height: 15),
          // User avatar & name skeleton
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              // User avatar skeleton
              SkeletonContainer.circular(width: 26, height: 26),
              // User name & designation skeleton
              Container(
                margin: EdgeInsets.only(top: 15),
                padding: EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10),
                    SkeletonContainer.rounded(width: 100, height: 15),
                    SizedBox(height: 5),
                    SkeletonContainer.rounded(width: 120, height: 15),
                    SizedBox(height: 25),
                  ],
                ),
              )
            ],
          ),
          // Feed message
          SkeletonContainer.rounded(width: 300, height: 15),
          SizedBox(height: 10),
          SkeletonContainer.rounded(width: 250, height: 15),
          // Empty space
          SizedBox(height: 10),
          // Media list
          SkeletonContainer.rounded(width: 80, height: 80),
          SizedBox(height: 24),
          // Feed topic
          Container(
            height: 22,
            child: Row(
              children: [
                TopicPill(
                  text: '         ',
                  color: black,
                  borderColor: shimmerGrey,
                ),
              ]
            ),
          ),
          // Empty space
          SizedBox(height: 8),
          // Date & time
          Row(
            children: [
              SkeletonContainer.rounded(width: 100, height: 20),
              Padding(
                padding: EdgeInsets.only(left: 8, right: 8),
                child: Icon(
                  Icons.circle,
                  color: lightGrey,
                  size: 4,
                ),
              ),
              SkeletonContainer.rounded(width: 80, height: 20),
            ],
          ),
          // Empty space
          SizedBox(height: 24),
          // Separator line
          Divider(thickness: 0.5, color: lightGrey),
          // Total replies
          SkeletonContainer.rounded(width: 80, height: 20),
          // Separator line
          Divider(thickness: 0.5, color: lightGrey),
        ],
      ),
    );
  }

  // Skeleton widget
  Widget _buildSkeleton(BuildContext context) {
    return Container(
      color: white,
      child: ListView(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        children: [
          _buildParentFeedSkeleton(context),
          Column(
            children: [
              Stack(
                children: [
                  Bubble(
                    margin: BubbleEdges.only(top: 20, bottom: 16, right: 60, left: 50),
                    padding: BubbleEdges.all(0),
                    radius: Radius.circular(12),
                    color: shimmerGrey,
                    nip: BubbleNip.leftTop,
                    nipHeight: 12,
                    alignment: Alignment.centerLeft,
                    shadowColor: transparent,
                    elevation: 0,
                    child: SkeletonContainer.rounded(width: 250, height: 300),
                  ),
                  Positioned(top: 20, left: 20, child: SkeletonContainer.circular(width: 25, height: 25)),
                ],
              ),
              Stack(
                children: [
                  Bubble(
                    margin: BubbleEdges.only(top: 20, bottom: 94, left: 60, right: 20),
                    padding: BubbleEdges.all(0),
                    radius: Radius.circular(12),
                    nip: BubbleNip.rightBottom,
                    color: shimmerGrey,
                    nipHeight: 12,
                    alignment: Alignment.centerRight,
                    shadowColor: transparent,
                    elevation: 0,
                    child: SkeletonContainer.rounded(width: 250, height: 188),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(top: 16, bottom: 24, left: 20, right: 20),
                decoration: BoxDecoration(color: darkGrey),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Column(
                            children: [
                              Container(
                                width: 24,
                                height: 24,
                                child: SkeletonContainer.circular(width: 24, height: 24),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(width: 15.0),
                        Expanded(
                          flex: 15,
                          child: Column(
                            children: [
                              Container(
                                child: SkeletonContainer.rounded(width: MediaQuery.of(context).size.width * 0.65, height: 51),
                              ),
                            ],
                          )
                        ),
                        SizedBox(width: 15.0),
                        Expanded(
                          flex: 2, 
                          child: Column(
                            children: [
                              Container(
                                width: 24,
                                height: 24,
                                child: SkeletonContainer.circular(width: 24, height: 24),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  // Empty result widget
  Widget _buildEmptyResult(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.75,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Lottie.asset(
              lottie_empty_chat,
              width: 250.0,
              height: 250.0,
              fit: BoxFit.fill,
            ),
            Container(
              width: 250.0,
              child: Text(
                text_empty_chat,
                style: regularFontsStyle(fontSize: 17.0, height: 1.35),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Scroll To Bottom Floating Button
  Widget _scrollToBottomButton() {
    return ValueListenableBuilder(
      valueListenable: workplaceViewModel.isShowReplyScrollBottomButton,
      builder: (context, bool isShowReplyScrollBottomButton, Widget? child) {
        return isShowReplyScrollBottomButton ? Container(
          width: 40.0,
          height: 40.0,
          child: RawMaterialButton(
            fillColor: darkGrey,
            shape: CircleBorder(),
            child: SvgPicture.asset(
              ic_arrow_down,
              width: 26.0,
            ),
            onPressed: () {
              autoScrollController.jumpTo(0);
            },
          )
        ) : Container();
      }
    );
  }
}
