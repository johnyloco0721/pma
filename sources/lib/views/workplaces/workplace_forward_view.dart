import 'package:assorted_layout_widgets/assorted_layout_widgets.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:getwidget/getwidget.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:lottie/lottie.dart';
import 'package:drift/drift.dart' as MOOR;
import 'package:path/path.dart' as p;
import 'package:sources/models/feed_post_model.dart';
import 'package:sources/models/general_response_model.dart';
import 'package:sources/services/feed_service.dart';
import 'package:sources/utils/debouncer_utlis.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/scroll.utils.dart';
import 'package:substring_highlight/substring_highlight.dart';
import 'package:uuid/uuid.dart';

import 'package:sources/args/forward_args.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_constants.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/size_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/view_models/workplace_view_model.dart';
import 'package:sources/widgets/avatar.dart';
import 'package:sources/widgets/custom_mentions/custom_mention_view.dart';
import 'package:sources/widgets/post_form.dart';
import 'package:sources/widgets/topic_pill.dart';

class WorkplaceForwardView extends StatefulWidget {
  final ForwardArguments? forwardArguments;
  const WorkplaceForwardView({Key? key, this.forwardArguments}) : super(key: key);

  @override
  _WorkplaceForwardState createState() => _WorkplaceForwardState();
}

class _WorkplaceForwardState extends State<WorkplaceForwardView> {
  FeedService _feedService = FeedService();
  final database = locator<AppDatabase>();
  final WorkplaceViewModel workplaceViewModel = locator<WorkplaceViewModel>();

  bool isLoading = true;
  ScrollController _scrollController = ScrollController();
  final pagingController = PagingController<int, Workplace>(firstPageKey: 1);
  final _debouncer = Debouncer(milliseconds: 500);
  GlobalKey<CustomMentionsState> forwardTextfieldKey = GlobalKey<CustomMentionsState>(debugLabel: 'ForwardTextKey');
  final searchTextFieldController = TextEditingController();
  String searchKeyword = '';
  List<int> _selectedWorkplaceID = [];
  int onlyClickOnce = 0;

  @override
  void initState() {
    super.initState();
    pagingController.addPageRequestListener((pageKey) {
      searchWithPagination();
    });
    initialize();
  }

  void initialize() async {
    if (widget.forwardArguments != null) {
      workplaceViewModel.temporaryTextFieldValue.value = widget.forwardArguments!.text;
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        setState(() {
          workplaceViewModel.mediaPdfFiles.value = widget.forwardArguments!.filePaths?.where((e) => e.substring((e.lastIndexOf(".") + 1)) == 'pdf').toList() ?? [];
          workplaceViewModel.mediaImageFiles.value = widget.forwardArguments!.filePaths?.where((e) => e.substring((e.lastIndexOf(".") + 1)) != 'pdf').toList() ?? [];
        });
      });
    }

    database.topicDao.getTopics().then((List<Topic> topics) {
      // Remove system topic
      topics.removeWhere((element) => element.topicName == 'System');
      workplaceViewModel.topicList.value = topics;
      if (topics.length > 0) {
        workplaceViewModel.defaultTopic = topics.firstWhere((element) => element.topicID == 1);
        workplaceViewModel.selectedTopic.value = workplaceViewModel.defaultTopic;
      }
    });
  }
  
  submitForm(String formValue, String displayValue) async {
    await Future.forEach(_selectedWorkplaceID, (int projectID) async {
      var newUUID = Uuid().v4();
      int newFeedID = -projectID;
      int newMediasID = -projectID;
      List<int> mediaIds = [];
      bool uplaodImageFailed = false;

      await database.teamMemberDao.getSingleTeamMember(projectID, workplaceViewModel.currentUser!.teamMemberID!).then((teamMember) {
        if (teamMember == null) {
          database.teamMemberDao.insertTeamMember(TeamMembersCompanion(
            teamMemberID: MOOR.Value(workplaceViewModel.currentUser!.teamMemberID!),
            teamMemberName: MOOR.Value(workplaceViewModel.currentUser!.name),
            teamMemberAvatar: MOOR.Value(workplaceViewModel.currentUser!.avatar),
            designationName: MOOR.Value(workplaceViewModel.currentUser!.designationName),
            projectID: MOOR.Value(projectID),
            show: MOOR.Value(false),
          ));
        }
      });

      await database.mediaDao.getLowestMediaID().then((mediaData) {
        if(mediaData.length > 0) {
          if(mediaData[0].mediaID < 0)
            newMediasID = mediaData[0].mediaID - 1;
        }
      });

      await database.feedDao.getLowestFeedID().then((feedData) async {
        if(feedData.length > 0) {
          if(feedData[0].feedID < 0)
            newFeedID = feedData[0].feedID - 1;
        }

        if (workplaceViewModel.mediaPdfFiles.value.length > 0) {
          for (var mediaFile in workplaceViewModel.mediaPdfFiles.value) {
            database.mediaDao.insertMedia(MediasCompanion(
              mediaID: MOOR.Value(newMediasID),
              projectID: MOOR.Value(projectID),
              feedID: MOOR.Value(newFeedID),
              fileExtension: MOOR.Value(p.extension(mediaFile).substring(1)),
              localFilePath: MOOR.Value(mediaFile),
              feedUuid: MOOR.Value(newUUID),
            ));
            
            final formData = FormData.fromMap({'fileUpload': await MultipartFile.fromFile(mediaFile)});
            await _feedService.postMedia(formData).then((response) {
              GeneralResponseModel mediaResponse = GeneralResponseModel.fromJson(response.data);

              if (mediaResponse.status == true) {
                database.mediaDao.updateMediaData(newMediasID, mediaResponse.response['mediaID'], mediaResponse.response['url']);
                mediaIds.add(mediaResponse.response['mediaID']);
              } else {
                uplaodImageFailed = true;
              }
            })
            .onError((error, stackTrace) {
              uplaodImageFailed = true;
            }).timeout(Duration(seconds: 60), onTimeout: () {
              setState(() {
                onlyClickOnce = 0;
              });
            }).whenComplete(() {
              setState(() {
                onlyClickOnce = 0;
              });
            });
            newMediasID--;
          }
        }

        if (workplaceViewModel.mediaImageFiles.value.length > 0) {
          for (var mediaFile in workplaceViewModel.mediaImageFiles.value) {
            database.mediaDao.insertMedia(MediasCompanion(
              mediaID: MOOR.Value(newMediasID),
              projectID: MOOR.Value(projectID),
              feedID: MOOR.Value(newFeedID),
              fileExtension: MOOR.Value(p.extension(mediaFile).substring(1)),
              localFilePath: MOOR.Value(mediaFile),
              feedUuid: MOOR.Value(newUUID),
            ));
            
            final formData = FormData.fromMap({'fileUpload': await MultipartFile.fromFile(mediaFile)});
            await _feedService.postMedia(formData).then((response) {
              GeneralResponseModel mediaResponse = GeneralResponseModel.fromJson(response.data);

              if (mediaResponse.status == true) {
                database.mediaDao.updateMediaData(newMediasID, mediaResponse.response['mediaID'], mediaResponse.response['url']);
                mediaIds.add(mediaResponse.response['mediaID']);
              } else {
                uplaodImageFailed = true;
              }
            })
            .onError((error, stackTrace) {
              uplaodImageFailed = true;
            }).timeout(Duration(seconds: 60), onTimeout: () {
              setState(() {
                onlyClickOnce = 0;
              });
            }).whenComplete(() {
              setState(() {
                onlyClickOnce = 0;
              });
            });
            newMediasID--;
          }
        }

        FeedPostModel formData = FeedPostModel(
          projectID: projectID,
          topicID: workplaceViewModel.selectedTopic.value!.topicID,
          description: formValue,
          replyID: null,
          mediaID: mediaIds,
          uuid: newUUID,
          feedType: "forward"
        );

        database.feedDao.insertFeed(FeedsCompanion(
          uuid: MOOR.Value(formData.uuid),
          feedID: MOOR.Value(newFeedID),
          parentID: MOOR.Value(null),
          projectID: MOOR.Value(formData.projectID),
          description: MOOR.Value(formData.description),
          originDesc: MOOR.Value(displayValue),
          feedType: MOOR.Value("forward"),
          postDateTime: MOOR.Value(formatDatetime(datetime: DateTime.now().toString(), format: 'yyyy-MM-dd HH:mm:ss')),
          topicID: MOOR.Value(formData.topicID),
          teamMemberID: MOOR.Value(workplaceViewModel.currentUser!.teamMemberID),
          replyTeamMemberAvatar: MOOR.Value(''),
          replyCount: MOOR.Value(0),
          replyReadStatus: MOOR.Value("read"),
          sendStatus: MOOR.Value(uplaodImageFailed ? 0 : 2)
        ));
        
        if (!uplaodImageFailed) {
          _feedService.postNewFeed(formData).then((response) {
            GeneralResponseModel result = GeneralResponseModel.fromJson(response.data);
            if (result.status == true) {
              database.feedDao.updateFeedStatus(newUUID, response.data["response"]["feedID"], 1);
              database.mediaDao.updateMediaFeedID(newFeedID, response.data["response"]["feedID"]);
            } else {
              database.feedDao.updateFeedStatus(newUUID, newFeedID, 0);
              showError(context, result.message ?? "");
            }
          })
          .onError((error, stackTrace) {
            database.feedDao.updateFeedStatus(newUUID, newFeedID, 0);
            showError(context, error.toString());
          }).timeout(Duration(seconds: 60), onTimeout: () {
            setState(() {
              onlyClickOnce = 0;
            });
          }).whenComplete(() {
            setState(() {
              onlyClickOnce = 0;
            });
          });
        }
      });
    });
    workplaceViewModel.temporaryTextFieldValue.value = null;
    workplaceViewModel.clearForm();
    if (forwardTextfieldKey.currentState != null) {
      forwardTextfieldKey.currentState!.controller!.text = '';
      forwardTextfieldKey.currentState!.controller!.clear();
    }
    workplaceViewModel.isPostFeedLoading.value = false;
  }

  @override
  void dispose() {
    super.dispose();
    searchKeyword = '';
    pagingController.dispose();
    searchTextFieldController.dispose();
  }

  void searchWithPagination() async {
    database.workplaceDao.getAllWorkplaces(keyword: searchKeyword).then((workplaces) {
      isLoading = true;
      final List<Workplace>? items = workplaces;
      pagingController.appendLastPage(items ?? []);
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: PageStorageKey<String>('Forward'),
      appBar: _buildAppBar(context),
      backgroundColor: white,
      body: Stack(
          alignment: Alignment.center,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                // Feeds list, Scroll to Bottom & backdrop
                Expanded(
                  child: _buildForwardList(context),
                ),
                // Build Topic Selector
                ValueListenableBuilder(
                  valueListenable: workplaceViewModel.isShowTopicSelector, 
                  builder: (BuildContext context, bool isShowTopicSelector, Widget? child) {
                    return isShowTopicSelector ? _buildTopicSelector(context) : Container();
                  }
                ),
                // Form
                Container(
                  child: ValueListenableBuilder(
                    valueListenable: workplaceViewModel.toolboxMode, 
                    builder: (BuildContext context, String toolboxMode, Widget? child) {
                      return PostForm(
                        textfieldKey: forwardTextfieldKey,
                        editMode: false,
                        isForward: true,
                        teamMembers: [],
                        focusNode: workplaceViewModel.forwardDescriptionNode,
                        onTextChanged: (text) {
                          workplaceViewModel.temporaryTextFieldValue.value = text;
                        },
                        submitOnTap: () async {
                          String formValue = forwardTextfieldKey.currentState!.controller!.markupText;
                          String displayValue = forwardTextfieldKey.currentState!.controller!.text;
                          if (formValue != '' || workplaceViewModel.mediaPdfFiles.value.length > 0 || workplaceViewModel.mediaImageFiles.value.length > 0) {
                            workplaceViewModel.isPostFeedLoading.value = true;
                            setState(() {
                              onlyClickOnce = onlyClickOnce + 1;
                            });
                            if (onlyClickOnce == 1)
                            await submitForm(formValue, displayValue);
                          }
                          Navigator.pop(context);
                        },
                        updateOnTap: () {},
                        isDisable: onlyClickOnce >= 1,
                      );
                    }
                  ),
                ),
              ],
            ),
          ],
        ),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      toolbarHeight: 90,
      systemOverlayStyle: SystemUiOverlayStyle.light,
      title: TextField(
        autofocus: true,
        controller: searchTextFieldController,
        onChanged: (text) {
          // Use debouncer to do api search once user stop typing for 500ms
          _debouncer.run(() {
            setState(() {
              searchKeyword = text;
              pagingController.refresh();
            });
          });
        },
        onEditingComplete: () {},
        style: regularFontsStyle(height: 1.32),
        decoration: InputDecoration(
          suffixIconConstraints: BoxConstraints(minHeight: 10, minWidth: 10),
          hintText: 'Forward to...',
          hintStyle: italicFontsStyle(color: grey, height: 1.32),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: yellow, width: 2.0),
            borderRadius: BorderRadius.circular(12),
          ),
          fillColor: white,
          filled: true,
          contentPadding: EdgeInsets.only(top: 10, bottom: 10, left: 24, right: 24),
          suffixIcon: Padding(
            padding: EdgeInsets.only(right: 25.0),
            child: GestureDetector(
              onTap: () {
                searchKeyword = searchTextFieldController.text;
                pagingController.refresh();
              },
              child: Container(
                child: SvgPicture.asset(ic_search, color: black),
              ),
            ),
          ),
        ),
      ),
      flexibleSpace: GestureDetector(
        onTap: () {
          // onTap app bar empty space then reset list scroll position
          scrollToTop(_scrollController);
        },
      ),
      leading: IconButton(
        onPressed: () {
          workplaceViewModel.mediaPdfFiles.value = [];
          workplaceViewModel.mediaImageFiles.value = [];
          Navigator.pop(context);
        },
        icon: SvgPicture.asset(ic_chevron_left, width: 10 ),
      ),
      backgroundColor: black,
      centerTitle: false,
      elevation: 0,
    );
  }

  // Workplace list stream builder
  Widget _buildForwardList(BuildContext context) {
    return Container(
      child: RefreshIndicator(
        color: yellow,
        backgroundColor: white,
        onRefresh: () => Future.sync(() => pagingController),
        child: PagedListView.separated(
          scrollController: _scrollController,
          pagingController: pagingController,
          separatorBuilder: (context, index) => Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Divider(thickness: 1),
          ),
          builderDelegate: PagedChildBuilderDelegate<Workplace>(
            itemBuilder: (context, element, index) => isLoading ? 
              _buildLoadingIndicator(context) : 
              _buildListItem(context, element),
            firstPageProgressIndicatorBuilder: (context) {
              return _buildLoadingIndicator(context);
            },
            noItemsFoundIndicatorBuilder: (context) => _buildEmptyResult(context),
          ),
        ),
      ),
    );
  }

  // Search result item widget
  Widget _buildListItem(BuildContext context, Workplace item) {
    final teamMemberDao = database.teamMemberDao;

    return Padding(
      padding: EdgeInsets.all(20),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 8),
                  child: SubstringHighlight(
                    text: (item.propertyName != null ? item.propertyName.toString() + ' ' : '') + (item.projectUnit ?? ''),
                    term: searchKeyword,
                    textStyle: regularFontsStyle(height: 1.32),
                    textStyleHighlight: boldFontsStyle(color: darkYellow, height: 1.32),
                  ),
                ),
                Row(
                  children: [
                    Text('Team member : '),
                    FutureBuilder(
                      future: teamMemberDao.getTeamMembersByProject(item.projectID),
                      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
                        final teamMembers = snapshot.data ?? [];
                        var itemCount = snapshot.hasData ? snapshot.data?.length : 0;
                        if (snapshot.hasData && (itemCount != null && itemCount > 0)) {
                          return RowSuper(
                            innerDistance: -6,
                            children: teamMembers.asMap().entries.map((e) {
                              if (e.key < 5)
                                return Avatar(
                                  url: e.value.teamMemberAvatar ?? '',
                                  width: 20,
                                  borderColor: white,
                                  dots: (itemCount > 5 && e.key == 4) ? true : false,
                                );
                            }).toList(),
                          );
                        }
                        return Text('-');
                      },
                    ),
                  ],
                )
              ],
            ),
          ),
          // Review - R4: Steph request change the checkbox UI
          GFCheckbox(
            size: 26.0,
            type: GFCheckboxType.circle,
            activeBgColor: yellow,
            inactiveBorderColor: lightGrey,
            activeBorderColor: black,
            activeIcon: Icon(Icons.check, size: 16, color: black),
            onChanged: (value) {
              setState(() {
                if (_selectedWorkplaceID.contains(item.projectID)) {
                  _selectedWorkplaceID.remove(item.projectID);
                } else {
                  _selectedWorkplaceID.add(item.projectID);
                }
              });
            },
            value: _selectedWorkplaceID.contains(item.projectID),
            inactiveIcon: null,
          ),
          // Checkbox(
          //   onChanged: (value){
          //     setState(() {
          //       if (_selectedWorkplaceID.contains(item.projectID)) {
          //         _selectedWorkplaceID.remove(item.projectID);
          //       } else {
          //         _selectedWorkplaceID.add(item.projectID);
          //       }
          //     });
          //     print(_selectedWorkplaceID);
          //   },
          //   value: _selectedWorkplaceID.contains(item.projectID)
          // )
        ],
      ),
    );
  }
  // Topic selector widget
  Widget _buildTopicSelector(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: workplaceViewModel.topicList, 
      builder: (BuildContext context, List<Topic> topics, Widget? child) {
        var btnMore = Topic(
          topicID: 0, 
          topicName: 'More',
          topicColour: '#FFD200',
          topicIcon: '',
          weight: (topics.length + 1),
        );
        return Container(
          padding: EdgeInsets.only(top: 10, bottom: 10),
          height: 45,
          child: ListView.builder(
            key: PageStorageKey<String>('TopicSelector'),
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: 6,
            itemBuilder: (context, index) {
              Topic topic = (index == 5) ? btnMore : topics[index];
              return TopicPill(
                text: topic.topicName!,
                fontSize: 15.0,
                fontWeight: (topic.topicID == 0) ? FontWeight.w700 : FontWeight.w400,
                color: (topic.topicID == 0) ? black : hexToColor(topics[index].topicColour!),
                backgroundColor: (topic.topicID == 0) ? hexToColor(btnMore.topicColour!) : white,
                borderColor: (topic.topicID == 0) ? hexToColor(btnMore.topicColour!) : hexToColor(topics[index].topicColour!),
                margin: EdgeInsets.only(left: (index == 0) ? 15 : 6, right: (index == 5) ? 15 : 6),
                onTap: () {
                  if (topic.topicID == 0) {
                    Navigator.pushNamed(context, 'topicSelector').then((value) => initialize());
                  }
                  else {
                    workplaceViewModel.selectedTopic.value = topic;
                  }
                  workplaceViewModel.isShowTopicSelector.value = false;
                },
              );
            }
          )
        );
      }
    );
  }

  // Empty result widget
  Widget _buildEmptyResult(BuildContext context) {
    return Container(
      height: SizeUtils.height(context, 75),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Lottie.asset(lottie_empty_search, width: 250.0, height: 250.0, fit: BoxFit.fill),
            Container(
              width: 250.0,
              child: Text(
                text_empty_search,
                style: regularFontsStyle(fontSize: 17.0, height: 1.35),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Loading indicator widget
  Widget _buildLoadingIndicator(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 30),
            child: Lottie.asset(
              lottie_loading_spinner,
              width: 60.0,
              height: 60.0,
              fit: BoxFit.fill,
            ),
          )
        ],
      )
    );
  }
}