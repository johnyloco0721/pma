import 'dart:convert';
import 'dart:io';

import 'package:assorted_layout_widgets/assorted_layout_widgets.dart';
import 'package:cron/cron.dart';
import 'package:drift/drift.dart' as DRIFT;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:sources/args/forward_args.dart';
import 'package:sources/args/task_args.dart';
import 'package:sources/args/workplace_args.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_constants.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/home_view.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/services/workplace_service.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/scroll.utils.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/utils/size_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/view_models/app_view_model.dart';
import 'package:sources/view_models/workplace_view_model.dart';
import 'package:sources/widgets/avatar.dart';
import 'package:sources/widgets/skeleton.dart';

class WorkplaceListView extends StatefulWidget {
  final WorkplaceArguments? workplaceArguments;
  const WorkplaceListView({Key? key, this.workplaceArguments}) : super(key: key);

  @override
  _WorkplaceListState createState() => _WorkplaceListState();
}

class _WorkplaceListState extends State<WorkplaceListView> {
  AppViewModel appViewModel = locator<AppViewModel>();
  final database = locator<AppDatabase>();
  final workplaceViewModel = locator<WorkplaceViewModel>();
  WorkplaceService _workplaceService = WorkplaceService();
  ScrollController _scrollController = ScrollController();
  bool isLoading = false;
  bool isEmpty = false;
  List<String> filterList = ['attention', 'unread', 'read']; // if change this initial filter, need change in workplace_view_model also
  String lastUpdateDateTime = '';

  Cron? cron;

  @override
  void initState() {
    // implement initState
    super.initState();
    // isLoading = true;
    _workplaceService.syncDeletedWorkplaces();

    // Fetch workplaces data from API every 15 seconds
    cron = Cron()..schedule(Schedule.parse('*/10 * * * * *'), () async {
      await database.workplaceDao.getLatestUpdateWorkplace().then((latestUpdateWorkplace) {
        lastUpdateDateTime = latestUpdateWorkplace!.updatedDateTime!;
      }).onError((error, stackTrace) {
        print('Latest Update Datetime no found.');
      });
      _workplaceService.fetchWorkplaceListAndSaveToDB(lastUpdateDateTime: lastUpdateDateTime);
      _workplaceService.syncDeletedWorkplaces();
    });

    initialize();
  }

  void initialize() async {
    // Initialize view model once getting in workplace module
    workplaceViewModel.initialize();
    await database.workplaceDao.getLatestUpdateWorkplace().then((latestUpdateWorkplace) {
      lastUpdateDateTime = latestUpdateWorkplace!.updatedDateTime!;
    }).onError((error, stackTrace) {
      print('Latest Update Datetime no found.');
    });
    await _workplaceService.fetchWorkplaceListAndSaveToDB(lastUpdateDateTime: lastUpdateDateTime).then((value) {
      // setState(() {
      //   isLoading = false;
      // });
      toggleFilter();
    });

    // Redirect to next route once widget built completed
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (widget.workplaceArguments?.projectID != null) {
        goToNextNamedRoute(context, 'workplaceChat', args: widget.workplaceArguments);
      } else if (widget.workplaceArguments?.forwardText != null) {
        goToNextNamedRoute(context, 'workplaceForward', args: ForwardArguments(widget.workplaceArguments?.forwardText, null));
      } else if (widget.workplaceArguments?.forwardFilePaths != null) {
        goToNextNamedRoute(context, 'workplaceForward', args: ForwardArguments(null, widget.workplaceArguments?.forwardFilePaths));
      }

      // Check for push notification coming from terminated state.
      var message = SpManager.getFcmPayload();
      SpManager.removeItem('fcmPayload');
      if (message != null) {
        var data = jsonDecode(message);
        switch (data['moduleName']) {
          case 'workplace':
            goToNextNamedRoute(context, 'workplaceChat', args: WorkplaceArguments(int.parse(data['projectID']), null, null, null, null));
            break;
          case 'workplace-reply':
            goToNextNamedRoute(context, 'workplaceChat', args: WorkplaceArguments(int.parse(data['projectID']), int.parse(data['replyToID']), null, null, null));
            break;
          case "task-by-project--list-all":
          case "task-by-project--list-incomplete":
          case "task-by-project--list-complete":
            // go to chat page
            notificationRedirection(
              'workplaceChat', 
              appViewModel.bottomBarCurrentIndex.value, 
              toIndex: 0, 
              args: WorkplaceArguments(int.parse(data['projectID']), null, data['moduleName'], null, null)
            );
            break;
          case 'task-details':
            notificationRedirection(taskMainScreen, appViewModel.bottomBarCurrentIndex.value, toIndex: 1, args: TaskArguments(projectID: int.parse(data['projectID']), taskID: int.parse(data['affectedModuleID'])));
            break;
          case 'task-milestone':
            notificationRedirection(taskMainScreen, appViewModel.bottomBarCurrentIndex.value, toIndex: 1, args: TaskArguments(projectID: int.parse(data['affectedModuleID'])));
            break;
          case 'task-comment':
            notificationRedirection(taskMainScreen, appViewModel.bottomBarCurrentIndex.value, toIndex: 1, args: TaskArguments(projectID: int.parse(data['projectID']), taskID: int.parse(data['affectedModuleID']), moduleName: data['moduleName']));
            break;
          default:
            HomeView.of(appViewModel.tabsNavigatorKey[appViewModel.bottomBarCurrentIndex.value].currentContext!)!.navigateFromRootToPage('alertList', null, null, null, null);
            break;
        }
      }
    });
  }

  void temporaryMakeWorkplacesToArchived() {
    // update updated workplace to archive
    database.workplaceDao.getPeriodAgoWorkplace(DateTime.now().subtract(Duration(days: 0)).toString()).then((workplaces) {
      workplaces.forEach((Workplace workplace) {
        // update the older workplace to archive
        database.workplaceDao.updateWorkplace(
          workplace.copyWith(isRead: DRIFT.Value('archive'))
        );
        // delete media files
        database.mediaDao.getMediasByProjectID(workplace.projectID).then((medias) {
          medias.forEach((Media media) {
            File(media.localFilePath ?? '').delete();
          });
        });
      });
    });
  }

  void toggleFilter() {
  if(mounted)
    setState(() {
      filterList = workplaceViewModel.workplaceFilterList.value;
    });
  }

  @override
  void dispose() {
    print('dispose workplace list');
    cron?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: PageStorageKey<String>('WorkplaceList'),
      appBar: _buildAppBar(context),
      backgroundColor: white,
      body: RefreshIndicator(
        color: yellow,
        backgroundColor: white,
        onRefresh: () {
          return _workplaceService.fetchWorkplaceListAndSaveToDB(lastUpdateDateTime: lastUpdateDateTime);
        },
        child: _buildWorkplaceList(context),
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     temporaryMakeWorkplacesToArchived();
      //   },
      //   backgroundColor: yellow,
      //   child: SvgPicture.asset(ic_workplace_archived),
      // ),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.dark,
      title: Text(
        title_workplace_page,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: boldFontsStyle(fontFamily: platForm, fontSize: 35.0, letterSpacing: 0.5),
      ),
      flexibleSpace: GestureDetector(
        onTap: () {
          // onTap app bar empty space then reset list scroll position
          scrollToTop(_scrollController);
        },
      ),
      actions: [
        // Workplace Search
        GestureDetector(
          onTap: () {
            goToNextNamedRoute(context, 'workplaceSearch');
          },
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: SvgPicture.asset(ic_search, width: 20.0, color: black),
          ),
        ),
        // Workplace Filter
        GestureDetector(
          onTap: () {
            // Pass callback function to workplaceFilter
            Navigator.pushNamed(context, 'workplaceFilter').then((value) => toggleFilter());
          },
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: filterList.length == 0 || filterList.length == 3 ? 
              SvgPicture.asset(ic_filter, width: 20.0, color: black) : 
              SvgPicture.asset(ic_filter_active, width: 20.0),
          ),
        ),
        SizedBox(width: 10.0),
      ],
      backgroundColor: white,
      surfaceTintColor: white,
      centerTitle: false,
      automaticallyImplyLeading: false,
      elevation: 0,
    );
  }

  // Workplace list stream builder
  StreamBuilder<List<Workplace>> _buildWorkplaceList(BuildContext context) {
    final workplaceDao = database.workplaceDao;

    return StreamBuilder(
      stream: workplaceDao.watchAllWorkplaces(filterBy: filterList),
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        final workplaces = snapshot.data ?? [];
        int itemCount = snapshot.hasData ? snapshot.data!.length : 0;
        if (isLoading) {
          return _buildSkeleton(context);
        }
        else if (!isLoading && itemCount == 0) {
          return _buildEmptyResult(context);
        }
        else {
          return ListView.separated(
            physics: const AlwaysScrollableScrollPhysics(),
            controller: _scrollController,
            key: PageStorageKey<String>('WorkplaceList'),
            separatorBuilder: (context, index) => Padding(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Divider(thickness: 1),
            ), 
            itemCount: itemCount,
            itemBuilder: (BuildContext context, int index) {
              final workplaceItem = workplaces[index];
              return _buildListItem(context, workplaceItem);
            }
          );
        }
      }
    );
  }

  // Workplace list item widget
  Widget _buildListItem(BuildContext context, Workplace workplace) {
    final teamMemberDao = database.teamMemberDao;

    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        goToNextNamedRoute(context, 'workplaceChat', args: WorkplaceArguments(workplace.projectID, null, null, null, null));
      },
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Row(
          children: [
            _svgPicture(workplace.isRead),
            Expanded(
              flex: 5,
              child: Padding(
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Project Name & Unit
                    Padding(
                      padding: const EdgeInsets.only(bottom: 3),
                      child: Text(
                        (workplace.propertyName != null ? workplace.propertyName.toString() + ' ' : '') + (workplace.projectUnit ?? ''),
                        style: boldFontsStyle(height: 1.32),
                      ),
                    ),
                    // Owner Name
                    Padding(
                      padding: const EdgeInsets.only(bottom: 6),
                      child: Text(
                        'Owner: ' + (workplace.ownerName ?? ''),
                        style: regularFontsStyle(color: grey, fontSize: 15.0, height: 1.27),
                      ),
                    ),
                    // Team members avatars
                    FutureBuilder(
                      future: teamMemberDao.getTeamMembersByProject(workplace.projectID),
                      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
                        final teamMembers = snapshot.data ?? [];
                        var itemCount = snapshot.hasData ? snapshot.data?.length : 0;
                        if (snapshot.hasData && (itemCount != null && itemCount > 0)) {
                          return RowSuper(
                            innerDistance: -6,
                            children: teamMembers.asMap().entries.map((e) {
                              if (e.key < 5)
                                return Avatar(
                                  url: e.value.teamMemberAvatar ?? '',
                                  width: 20,
                                  borderColor: white,
                                  dots: (itemCount > 5 && e.key == 4) ? true : false,
                                );
                            }).toList(),
                          );
                        }
                        return Container();
                      },
                    ),
                  ],
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SvgPicture.asset(ic_arrow_right_workplace, width: 24.0, color: black),
                Padding(padding: EdgeInsets.only(top: 22.5)),
                if (workplace.updatedDateTime != null)
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                    decoration: BoxDecoration(
                      color: white,
                      border: Border.all(color: lightGrey),
                      borderRadius: BorderRadius.circular(30)
                    ),
                    child: Text(
                      showTimestamp(workplace.displayDateTime!),
                      style: regularFontsStyle(color: grey, fontSize: 12.0, height: 1.25),
                    ),
                  )
              ]
            ),
          ],
        ),
      ),
    );
  }

  String showTimestamp(String datetime) {
    String result;
    String today = formatDatetime(datetime: DateTime.now().toString(), format: "y-MM-dd 00:00:00");
    String yesterday = formatDatetime(datetime: DateTime.now().subtract(Duration(days: 1)).toString(), format: "y-MM-dd 00:00:00");

    if (today.compareTo(datetime) <= 0) {
      result = formatDatetime(datetime: datetime, format: 'hh:mma').toLowerCase();
    } else if ((today.compareTo(datetime) > 0) && (yesterday.compareTo(datetime) <= 0)) {
      result = "Yesterday";
    } else {
      result = formatDatetime(datetime: datetime, format: 'd MMM y');
    }
    return result;
  }

  // Skeleton widget
  Widget _buildSkeleton(BuildContext context) {
    return Container(
      color: white,
      padding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
      width: SizeUtils.width(context, 100),
      child: ListView(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        children: [
          for (int i = 0; i < 8; i++)
            Column(
              children: [
                Row(
                  children: [
                    SkeletonContainer.rounded(width: 20, height: 20),
                    Expanded(
                      flex: 5,
                      child: Padding(
                        padding: EdgeInsets.only(left: 20, right: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                              SkeletonContainer.rounded(
                                width: SizeUtils.width(context, 40),
                                height: SizeUtils.height(context, 1.5),
                              ),
                              SizedBox(
                                height: 12,
                              ),
                              SkeletonContainer.rounded(
                                width: SizeUtils.width(context, 30),
                                height: SizeUtils.height(context, 1.5),
                              ),
                              SizedBox(
                                height: 12,
                              ),
                              SkeletonContainer.rounded(
                                width: SizeUtils.width(context, 20),
                                height: SizeUtils.height(context, 2),
                              )
                            ]
                        ),
                      ),
                    ),
                    SvgPicture.asset(ic_arrow_right_workplace, width: 24.0, color: shimmerGrey),
                  ],
                ),
                SizedBox(height: 20),
                Divider(color: shimmerGrey, thickness: 1),
                SizedBox(height: 20),
              ],
            )
        ],
      ),
    );
  }

  // Empty result widget
  Widget _buildEmptyResult(BuildContext context) {
    return LayoutBuilder(
      builder: (context, size) {
        return ListView(
          children: [
            Container(
              height: size.maxHeight,
              color: white,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Lottie.asset(lottie_empty_workplace, width: 250.0, height: 250.0, fit: BoxFit.fill),
                    Container(
                      width: 250.0,
                      child: Text(
                        text_empty_workplace,
                        style: regularFontsStyle(fontSize: 17.0, height: 1.35),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        );
      }
    );
  }

  // Workplace read status icons
  SvgPicture _svgPicture(status) {
    switch (status) {
      case 'read':
        return SvgPicture.asset(ic_workplace_read, width: 24.0);
      case 'unread':
        return SvgPicture.asset(ic_workplace_unread, width: 24.0, color: darkYellow);
      case 'attention':
        return SvgPicture.asset(ic_workplace_attention, width: 24.0);
      case 'archive':
        return SvgPicture.asset(ic_workplace_archived, width: 24.0, color: darkBlue);
      default:
        return SvgPicture.asset(ic_workplace_read, width: 24.0);
    }
  }

}