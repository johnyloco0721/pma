import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';

import 'package:sources/constants/app_fonts.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/services/workplace_service.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/widgets/avatar.dart';

class ProjectDetailsView extends StatefulWidget {
  final int projectId;
  const ProjectDetailsView({Key? key, required this.projectId}) : super(key: key);

  @override
  _ProjectDetailsViewState createState() => _ProjectDetailsViewState();
}

class _ProjectDetailsViewState extends State<ProjectDetailsView> {
  final database = locator<AppDatabase>();
  List<WorkplaceInfo> infoList = [];
  List<TeamMember> teamMemberList = [];

  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    // implement initState
    setInitialValue();
    loadWorkplaceDetails();
  }
  
  Future loadWorkplaceDetails() async {
    WorkplaceService _workplaceService = WorkplaceService();
    setState(() {
      isLoading = true;
    });

    await _workplaceService.getWorkplaceDetail(widget.projectId);
    setInitialValue();

    setState(() {
      isLoading = false;
    });
  }

  setInitialValue() {
    database.workplaceInfoDao.getWorkplaceInfosByProject(widget.projectId).then((workplaceInfos) {
      setState(() {
        infoList = workplaceInfos;
      });
    });
    
    database.teamMemberDao.getTeamMembersByProject(widget.projectId).then((teamMembers) {
      if (teamMembers != []) {
        setState(() {
          teamMemberList = teamMembers;
        });
      }
    });
  }

  @override
  void dispose() {
    // implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      appBar: _buildAppBar(context),
      body: _buildProjectDetails(context),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.light,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: SvgPicture.asset(ic_chevron_left, width: 10.0),
      ),
      title: Text(
        'Project Details',
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, color: white, letterSpacing: 0.8),
      ),
      centerTitle: false,
      backgroundColor: black,
    );
  }

  // Project details widget
  Widget _buildProjectDetails(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Column(
        children: <Widget>[
          // Tab bar
          Container(
            padding: EdgeInsets.all(24),
            constraints: BoxConstraints(maxHeight: 150.0),
            child: Material(
              color: transparent,
              child: TabBar(
                labelColor: black,
                labelStyle: boldFontsStyle(fontSize: 17.0, height: 1.35),
                unselectedLabelColor: grey,
                unselectedLabelStyle: regularFontsStyle(fontSize: 17.0, height: 1.35),
                indicatorColor: darkYellow,
                indicatorSize: TabBarIndicatorSize.tab,
                labelPadding: EdgeInsets.only(top: 10.0, bottom: 4),
                dividerColor: transparent,
                tabs: [
                  Text('Info'),
                  Text('Team'),
                ],
              ),
            ),
          ),
          // Tab content view
          Expanded(
            child: TabBarView(
              children: [
                Container(
                  padding: EdgeInsets.only(left: 24, right: 24),
                  child: RefreshIndicator(
                    color: yellow,
                    backgroundColor: white,
                    onRefresh: loadWorkplaceDetails,
                    child: infoList.length == 0 && isLoading ? Center(
                      child: CircularProgressIndicator(color: yellow)
                    ) : _buildInfoTabView(context),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 24, right: 24),
                  child: RefreshIndicator(
                    color: yellow,
                    backgroundColor: white,
                    onRefresh: loadWorkplaceDetails,
                    child: teamMemberList.length == 0 && isLoading ? Center(
                      child: CircularProgressIndicator(color: yellow)
                    ) : _buildTeamTabView(context),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  // Project info tab page
  Widget _buildInfoTabView(BuildContext context) {
    return ListView(
      children: [
        Wrap(
          spacing: 16.0,
          children: [
            for (int i=0; i<infoList.length; i+=2)
              if (infoList[i].infoDisplayValue != null && infoList[i].infoDisplayValue != '')
              LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
                  return Container(
                    width: constraints.maxWidth,
                    padding: EdgeInsets.only(bottom: 32.0),
                    child: _buildInfoItem(context, infoList, i, constraints.maxWidth / 2 - 8.0)
                  );
                }
              ),
          ],
        ),
      ],
    );
  }

  // Project info item widget
  Widget _buildInfoItem(BuildContext context, List<WorkplaceInfo> info, int index, double width) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: width,
                child: Text(
                  info[index].infoLabel ?? '',
                  style: regularFontsStyle(color: grey, fontSize: 17.0, height: 1.35),
                ),
              ),
              if (index+1 < infoList.length)
                Container(
                  width: width,
                  child: Text(
                    info[index+1].infoLabel ?? '',
                    style: regularFontsStyle(color: grey, fontSize: 17.0, height: 1.35),
                  ),
                ),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: width,
              child: Text(
                info[index].infoDisplayValue ?? '',
                style: regularFontsStyle(fontSize: 19.0, height: 1.32),
              ),
            ),
            if (index+1 < infoList.length)
              Container(
                width: width,
                child: Text(
                  info[index+1].infoDisplayValue ?? '',
                  style: regularFontsStyle(fontSize: 19.0, height: 1.32),
                ),
              ),
          ],
        ),
      ],
    );
  }

  // Team tab page
  Widget _buildTeamTabView(BuildContext context) {
    var itemCount = teamMemberList.length;
    if (itemCount > 0) {
      return ListView.builder(
        physics: const AlwaysScrollableScrollPhysics(),
        key: PageStorageKey<String>('TeamMemberList'),
        itemCount: itemCount,
        itemBuilder: (BuildContext context, int index) {
          final memberAvatar = teamMemberList[index].teamMemberAvatar;
          final memberName = teamMemberList[index].teamMemberName;
          return _buildTeamItem(context, memberAvatar!, memberName!);
        }
      );
    } else {
      return Container(
        height: MediaQuery.of(context).size.height * 0.6,
        child: Center(
          child: Text(
            'There is no team members\nassigned at the moment',
            textAlign: TextAlign.center,
            style: regularFontsStyle(fontSize: 17.0, height: 1.35),
          ),
        ),
      );
    }
  }

  // Team item widget
  Widget _buildTeamItem(BuildContext context, String avatar, String name) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 16),
      child: Row(
        children: [
          Avatar(url: avatar, width: 30),
          SizedBox(width: 16),
          Expanded(
            child: Text(
              name,
              style: regularFontsStyle(fontSize: 19.0, height: 1.32),
            ),
          )
        ],
      ),
    );
  }
}
