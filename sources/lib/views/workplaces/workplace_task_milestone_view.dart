import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:lottie/lottie.dart';
import 'package:sources/args/task_form_args.dart';
import 'package:sources/args/workplace_task_milestone_args.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/models/task_milestone_model.dart';
import 'package:sources/models/task_project_item_model.dart';
import 'package:sources/models/team_member_model.dart';
import 'package:sources/services/task_services.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/widgets/milestone_widget.dart';

import '../../models/permissions_model.dart';

class WorkplaceTaskMilestoneView extends StatefulWidget {
  final WorkplaceTaskMilestoneArguments milestoneArgs;
  const WorkplaceTaskMilestoneView({Key? key, required this.milestoneArgs}) : super(key: key);

  @override
  _WorkplaceTaskMilestoneViewState createState() => _WorkplaceTaskMilestoneViewState();
}

class _WorkplaceTaskMilestoneViewState extends State<WorkplaceTaskMilestoneView> {
  TaskService _taskService = TaskService();
  
  ScrollController _scrollController = ScrollController();
  PagingController<int, StageDetail> _milestonePagingController = PagingController(firstPageKey: 1);

  String milestoneCurrentFliterValue = 'Incomplete';
  String? propertyName = '';
  bool isLoading = false;

  int totalCount = 0;
  int incompleteCount = 0;
  int completedCount = 0;
  int onlyClickOnce = 0;

  PermissionsModel? permissions;
  List<TeamMemberDetails>? teamMemberList;
  List<TaskMilestoneOption>? milestoneList;
  List<DisclaimerList>? disclaimerList;

  @override
  void initState() {
    super.initState();
    setState(() {
      isLoading = true;
      
      switch(widget.milestoneArgs.goToAllTaskMilestone) {
        case "task-by-project--list-all":
          milestoneCurrentFliterValue = 'All';
          break;
        case "task-by-project--list-incomplete":
          milestoneCurrentFliterValue = 'Incomplete';
          break;
        case "task-by-project--list-complete":
          milestoneCurrentFliterValue = 'Completed';
          break;
        default:
          milestoneCurrentFliterValue = 'Incomplete';
          break;
      }
    });
    _fetchMyTaskListByMilestonePage(1);
    _milestonePagingController.addPageRequestListener((pageKey) {
      _fetchMyTaskListByMilestonePage(pageKey);
    });
    propertyName = widget.milestoneArgs.projectName;
  }

  Future<void> _fetchMyTaskListByMilestonePage(int pageKey) async {
    try {
      await _taskService.fetchAllTaskListByMilestone(widget.milestoneArgs.projectID ?? 0, pageKey: pageKey, filterBy: milestoneCurrentFliterValue.toLowerCase()).then((response) {
            TaskMilestoneModel taskProjectListResponse = response;
            TaskProjectItemModel? projectDetail = taskProjectListResponse.response?.projectDetail;
            int? total = projectDetail?.totalTasks;
            int? completed = projectDetail?.completedTasks;
            if (total != null && completed != null)
              setState(() {
                propertyName = projectDetail?.propertyName;
                totalCount = total;
                incompleteCount = total - completed;
                completedCount = completed;
                permissions = taskProjectListResponse.response?.permissions;
                teamMemberList = taskProjectListResponse.response?.teamMemberList;
                milestoneList = taskProjectListResponse.response?.milestoneList;
                disclaimerList = taskProjectListResponse.response?.disclaimerArr;
              });
            List<StageDetail>? StageList = taskProjectListResponse.response?.stageArray;
            StageList?.forEach((stageDetail) {
              stageDetail.taskList?.forEach((task) {
                task.isExpanded = task.milestoneStatus != 'completed';
              });
            });
            final isLastPage = (StageList?.length ?? 0)  < (taskProjectListResponse.limitPerPage ?? 50);
            if (isLastPage) {
              _milestonePagingController.appendLastPage(StageList ?? []);
              setState(() {
                isLoading = false;
              });
            } else {
              final nextPageKey = pageKey + 1;
              _milestonePagingController.appendPage(StageList ?? [], nextPageKey);
            }

            if (totalCount == 0) {
              setState(() {
                isLoading = false;
                permissions = taskProjectListResponse.response?.permissions;
              });
            }
      });
    } catch (error) {      
      _milestonePagingController.error = error;
      setState(() {
        isLoading = false;
      });
    }
  }

  void setLoadingState(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
    _milestonePagingController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      appBar: _buildAppBar(context),
      body: isLoading ? 
        Container(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Lottie.asset(
                lottie_loading_spinner,
                width: 50.0,
                height: 50.0,
                fit: BoxFit.fill,
              ),
              Container(
                width: 250.0,
                child: Text(
                  'just a few secs...',
                  style: regularFontsStyle(fontSize: 14.0, color: grey),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        )
        : totalCount == 0 ? _buildEmptyWorkplaceTaskMilestoneView() : _buildWorkplaceTaskMilestoneView() ,
      floatingActionButton: (permissions?.canAccessEditorMode == true) ? 
      FloatingActionButton(
        onPressed: () {
          goToNextNamedRoute(context, taskFormScreen,
              args: TaskFormArguments(
                teamMemberList: teamMemberList,
                milestoneList: milestoneList,
                projectID: widget.milestoneArgs.projectID ,
                milestonePagingController: _milestonePagingController
              ), callback: () {
            _milestonePagingController.refresh();
          });
        },
        shape: CircleBorder(),
        backgroundColor: yellow,
        child: SvgPicture.asset(ic_add, width: 26.0),
      ) : Container(),
    ) ;
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      toolbarHeight: 90,
      systemOverlayStyle: SystemUiOverlayStyle.light,
      title: Text(
        propertyName != null ? "Tasks For $propertyName" : "Tasks For ",
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, color: white, letterSpacing: 0.8, height: 1.25),
      ),
      flexibleSpace: GestureDetector(
        onTap: () {
          // onTap app bar empty space then reset list scroll position
        },
      ),
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: SvgPicture.asset(ic_chevron_left, width: 10 ),
      ),
      backgroundColor: black,
      centerTitle: false,
    );
  }

  Widget _buildEmptyWorkplaceTaskMilestoneView() {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Lottie.asset(
            lottie_empty_docs,
            width: 250.0,
            height: 250.0,
            fit: BoxFit.fill,
          ),
          Container(
            width: 250.0,
            child: Text(
              'There’s no tasks yet',
              style: regularFontsStyle(fontSize: 17.0, height: 1.35),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(height: 10,),
          if (permissions?.canGenerateTask == true)
          ElevatedButton (
            style: ElevatedButton.styleFrom(
              backgroundColor: onlyClickOnce == 1 ? yellow.withOpacity(0.5) : yellow,
              padding: EdgeInsets.symmetric(vertical: 10.0),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
              maximumSize: Size(200.0, 50.0)
            ),
            onPressed: () async {
              var confirmGenerate = await showDecision(
                context, 
                'Are you sure you want to generate tasks without dates or assignees?', 
                'Cancel', 
                'Yes',
                buttonColor: yellow,
                textColor: black,
              );
              if (confirmGenerate) {
                      setState(() {
                        onlyClickOnce = onlyClickOnce + 1;
                      });

                      if (onlyClickOnce == 1) {
                          isLoading = true;

                        try {
                          // Show the dialog using a Builder widget to get the right context
                          var overlay = Overlay.of(context);
                          OverlayEntry overlayEntry;
                          // ignore: unused_local_variable
                          var response = await _taskService.manualGenerateTask(widget.milestoneArgs.projectID);

                          overlayEntry = OverlayEntry(
                            builder: (context) => Stack(
                              children: [
                                Positioned.fill(
                                  child: Container(
                                    color: Colors.black.withOpacity(0.5), // Dark background with opacity
                                  ),
                                ),
                                Center(
                                  child: Container(
                                    margin: EdgeInsets.only(top: 50.0),
                                    width: MediaQuery.of(context).size.width * 0.8,
                                    height: MediaQuery.of(context).size.height * 0.2,
                                    child: Material(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(12),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          SvgPicture.asset(ic_completed, width: 45),
                                          SizedBox(height: 12.0),
                                          Text(
                                            'Tasks Generated Successfully!',
                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Colors.black),
                                            textAlign: TextAlign.center,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );

                          overlay.insert(overlayEntry);

                          await Future.delayed(Duration(seconds:2));

                          // Remove the overlay
                          overlayEntry.remove();
                          
                                                      
                        } catch (error) {
                          showError(context, "Something went wrong, please try again!");
                          _milestonePagingController.refresh();
                          isLoading = false;
                          // Handle errors
                        } finally {
                          // Set loading state to false regardless of success or failure
                          goToReplacementNamedRoute(context, workplaceTaskMilestoneScreen, args: WorkplaceTaskMilestoneArguments(widget.milestoneArgs.projectID ?? 0, null, propertyName),
                          callback: () {
                              _milestonePagingController.refresh();
                            }
                          );
                          isLoading = false;
                        }
                          _milestonePagingController.refresh();
                          isLoading = false;
                      }
                    }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(ic_add, width: 16.0),
                SizedBox(width: 8.0),
                Text (
                  "Generate Tasks",
                  style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: black),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  // workplace task milestone view
  Widget _buildWorkplaceTaskMilestoneView() {
    return Column(
      children: [
        Container(
          height: 36.0,
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          margin: EdgeInsets.only(top: 8.0),
          decoration: BoxDecoration(
            border: Border(bottom: BorderSide(width: 1.0, color: shimmerGrey))
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              filterItemLayout('All', milestoneCurrentFliterValue == 'All', totalCount),
              filterItemLayout('Incomplete', milestoneCurrentFliterValue == 'Incomplete', incompleteCount),
              filterItemLayout('Completed', milestoneCurrentFliterValue == 'Completed', completedCount),
            ],
          ),
        ),
        Expanded(
          child: RefreshIndicator(
            color: yellow,
            backgroundColor: white,
            onRefresh: () async {
              _milestonePagingController.refresh();
            },
            child: MilestoneWidget(
              disclamerList: disclaimerList,
              scrollController: _scrollController, 
              milestonePagingController: _milestonePagingController,
              isWorkplace: true,
              permissions: permissions,
              projectID: widget.milestoneArgs.projectID,
              setLoadingState: setLoadingState,
            ),
          ),
        ),
      ],
    );
  }

  // filter item layout
  Widget filterItemLayout(String label, bool selected, int count) => Expanded(
    child: GestureDetector(
      onTap: () {
        setState(() {
          milestoneCurrentFliterValue = label;
          _milestonePagingController.refresh();
        });
      },
      child: Container(
        padding: EdgeInsets.only(bottom: 8.0),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              width: selected ? 2.0 : 0.0,
              color: selected ? darkYellow : transparent
            )
          )
        ),
        child: Center(
          child: Text(
            label + (!isLoading ? ' ($count)' : ''),
            style: selected ? boldFontsStyle(
              fontSize: 14.0, color: black
            ) : regularFontsStyle(
              fontSize: 14.0, color: grey
            ),
          )
        ),
      ),
    ),
  );
}