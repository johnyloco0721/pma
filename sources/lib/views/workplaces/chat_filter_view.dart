import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/view_models/workplace_view_model.dart';

class ChatFilter extends StatefulWidget {
  const ChatFilter({Key? key}) : super(key: key);

  @override
  _ChatFilterState createState() => _ChatFilterState();
}

class _ChatFilterState extends State<ChatFilter> {
  final AppDatabase database = locator<AppDatabase>();
  final WorkplaceViewModel workplaceViewModel = locator<WorkplaceViewModel>();
  List<int> filterList = [];

  final localIconPath = {
    "General" : ic_category_generic,
    "Site Visit" : ic_category_site_visit,
    "Design/Planning" : ic_category_design_planning,
    "Order / Delivery" : ic_category_order_delivery,
    "Site Work" : ic_category_site_work,
    "Defects / Complaints" : ic_category_defects_complains,
    "Finale" : ic_category_finale,
    "System" : ic_category_sytem,
    "Notes" : ic_category_notes
  };

  @override
  void initState() {
    super.initState();
    filterList = workplaceViewModel.chatFilterList.value;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteGrey,
      appBar: _buildAppBar(context),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Subtitle
            Text(
              'Filtered By:',
              style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, height: 1.3, letterSpacing: 0.8),
            ),
            SizedBox(height: 15),
            Expanded(
              child: _buildSwitch(context),
            ),
            // Reset button
            Container(
              height: 60,
              width: double.infinity,
              child: OutlinedButton (
                style: OutlinedButton.styleFrom(
                  foregroundColor: black,
                  textStyle: boldFontsStyle(height: 1.3),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(100.0),
                  ),
                  side: BorderSide(width: 1, color: black),
                ),
                onPressed: () {
                  resetFilter();
                },
                child: Text ("Reset to Default"),
              ),
            )
          ],
        ),
      ),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.light,
      actions: [
        GestureDetector(
          onTap: () {
            workplaceViewModel.chatFilterList.value = filterList;
            Navigator.pop(context);
          },
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: SvgPicture.asset(ic_close, width: 20.0, color: white),
          ),
        ),
        SizedBox(width: 10.0),
      ],
      backgroundColor: black,
      automaticallyImplyLeading: false,
    );
  }

  // Topic filter switchers
  FutureBuilder<List<Topic>> _buildSwitch(BuildContext context) {
    return FutureBuilder(
      future: database.topicDao.getTopics(),
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        final topics = snapshot.data ?? [];
        var itemCount = snapshot.hasData ? snapshot.data?.length : 0;
        return ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: itemCount,
          itemBuilder: (BuildContext context, int index) {
            Topic topic = topics[index];

            return Padding(
              padding: EdgeInsets.only(bottom: 15),
              child: Row(
                children: [
                  SvgPicture.network(
                    topic.topicIcon!,
                    width: 20.0,
                    color: isTopicSelected(topic.topicID) ? hexToColor(topic.topicColour!) : grey,
                    placeholderBuilder: (context) => SvgPicture.asset(
                      localIconPath[topic.topicName] ?? '',
                      width: 20.0,
                      color: isTopicSelected(topic.topicID) ? hexToColor(topic.topicColour!) : grey,
                    ),
                  ),
                  SizedBox(width: 20),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          topic.topicName!,
                          style: isTopicSelected(topic.topicID) 
                            ? boldFontsStyle(height: 1.32) 
                            : regularFontsStyle(color: grey, height: 1.32),
                        ),
                        Row()
                      ],
                    ),
                  ),
                  SizedBox(width: 20),
                  Transform.scale(
                    scale: 0.7,
                    alignment: Alignment.centerRight,
                    child: CupertinoSwitch(
                      value: isTopicSelected(topic.topicID),
                      onChanged: (value) {
                        isTopicSelected(topic.topicID) ? removeFilter(topic.topicID) : addFilter(topic.topicID);
                      },
                      activeColor: yellow,
                      trackColor: grey,
                    ),
                  ),
                ],
              )
            );
          }
        );
      },
    );
  }

  // Check if topic is selected
  bool isTopicSelected(int topicID) {
    return this.filterList.contains(topicID) ? true : false;
  }

  // Add topic to filterList
  void addFilter(int topicID) {
    if (!this.filterList.contains(topicID)) {
      setState(() {
        this.filterList.add(topicID);
      });
    }
  }

  // Remove topic from filterList
  void removeFilter(int topicID) {
    if (this.filterList.contains(topicID)) {
      setState(() {
        this.filterList.remove(topicID);
      });
    }
  }

  // Clear filterList
  void resetFilter() {
    database.topicDao.getTopics().then((topics) {
      setState(() {
        workplaceViewModel.chatFilterList.value = topics.map((e) => e.topicID).toList();
        this.filterList = workplaceViewModel.chatFilterList.value;
      });
    });
  }
}