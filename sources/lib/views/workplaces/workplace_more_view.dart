import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sources/args/workplace_more_args.dart';
import 'package:sources/args/workplace_task_milestone_args.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_routes.dart';

import 'package:sources/database/app_database.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';

import '../../models/workplace_details_model.dart';
import '../../services/workplace_service.dart';

class ProjectMoreView extends StatefulWidget {
  final WorkplaceMoreArguments workplaceMoreArgs;
  const ProjectMoreView({Key? key, required this.workplaceMoreArgs}) : super(key: key);

  @override
  _ProjectMoreViewState createState() => _ProjectMoreViewState();
}

class _ProjectMoreViewState extends State<ProjectMoreView> {
  final database = locator<AppDatabase>();
  WorkplaceService _workplaceService = WorkplaceService();
  WorkplaceDetailsModel? taskDetailsResponseModel;
  List<TagsModel> tag = [];
  int totalTasks = 0;
  int completedTasks = 0;

  @override
  void initState() {
    // implement initState
    super.initState();
    _fetchWorkplaceDetails();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _fetchWorkplaceDetails() async {
    try {
      await _workplaceService.getWorkplaceDetail(widget.workplaceMoreArgs.projectID!).then((response) {
        taskDetailsResponseModel = response;
        setState(() {
          totalTasks = taskDetailsResponseModel?.response?.projectArr?.totalTasks ?? 0;
          completedTasks = taskDetailsResponseModel?.response?.projectArr?.completedTasks ?? 0;
          tag = taskDetailsResponseModel?.response?.projectArr?.tags ?? [];
        });
      });
    } catch (error) {
      print('Workplace List API error!');
    }
    // Redirect to next route once widget built completed
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (widget.workplaceMoreArgs.goToAllTaskMilestone != null) {
        goToNextNamedRoute(context, workplaceTaskMilestoneScreen, args: WorkplaceTaskMilestoneArguments(widget.workplaceMoreArgs.projectID ?? 0, widget.workplaceMoreArgs.goToAllTaskMilestone, null));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      appBar: _buildAppBar(context),
      body: _buildChatMore(context),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.light,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: SvgPicture.asset(ic_chevron_left, width: 10.0),
      ),
      title: Text(
        widget.workplaceMoreArgs.propertyName ?? '',
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, color: white, letterSpacing: 0.8),
      ),
      centerTitle: false,
      backgroundColor: black,
    );
  }

  // Chat more widget
  Widget _buildChatMore(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 24.0, left: 24.0, right: 24.0),
      child: ListView(
        children: [
          Column(
            children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        for(int i = 0 ; i < tag.length ;i++ )
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(vertical: 8.0,horizontal: 16.0),
                                decoration: BoxDecoration(
                                  color: tag[i].backgroundColor != "" && tag[i].backgroundColor != "transparent" ? hexToColor(tag[i].backgroundColor ?? ""):transparent,
                                  border: Border.all(color: tag[i].borderColor != "" ? hexToColor(tag[i].borderColor ?? ""):transparent),
                                  borderRadius: BorderRadius.circular(500)
                                ),
                                child: Row(
                                  children: [
                                    if (tag[i].icon != null && tag[i].icon != '')
                                      Padding(
                                        padding: EdgeInsets.only(right: 8.0),
                                        child: SvgPicture.network(tag[i].icon ?? ""),
                                      ),             
                                    Text(
                                      tag[i].label ?? '',
                                      style: boldFontsStyle(fontSize: 14.0, color: tag[i].textColor != "" ? hexToColor(tag[i].textColor ?? ""):grey),),
                                  ],
                                ),
                          ),
                          if (tag.length-1 > 0)
                          SizedBox(width:6.0),
                          ],
                        ),
                      ],
                    ),   
                  ],
                ),
              SizedBox(height:16.0),
              Container(
                width: MediaQuery.of(context).size.width * 0.7,
                child: Text(
                  widget.workplaceMoreArgs.propertyName ?? '',
                  style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, height: 1.25, letterSpacing: 0.8),
                  textAlign: TextAlign.center
                ),
              ),
              SizedBox(height: 8.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(ic_people, width: 18.0, color: grey),
                  SizedBox(width: 8.0),
                  Text(
                    widget.workplaceMoreArgs.ownerName ?? '',
                    style: regularFontsStyle(fontSize: 14.0, color: grey),
                  ),
                ],
              ),
              SizedBox(height: 24.0),
            ],
          ),
          _buildListItem(
            context,
            () => goToNextNamedRoute(context, 'workplaceDetails', args: widget.workplaceMoreArgs.projectID),
            'Project Details'
          ),
          _buildListItem(
            context,
            () => goToNextNamedRoute(context, 'workplaceMedias', args: widget.workplaceMoreArgs.projectID),
            'View All Medias & Files'
          ),
          _buildListItem(
            context,
            () => goToNextNamedRoute(context, 'costEstimation', args: widget.workplaceMoreArgs.projectID),
            'Project Costs'
          ),
          if (totalTasks > 0)
            _buildListItem(
              context,
              () => goToNextNamedRoute(
                context, 
                workplaceTaskMilestoneScreen, 
                args: WorkplaceTaskMilestoneArguments(widget.workplaceMoreArgs.projectID ?? 0, null, null), 
                callback: () => _fetchWorkplaceDetails()
              ),
              '$completedTasks / $totalTasks Project Tasks Completed'
            ),
          if (totalTasks == 0)
            _buildListItem(
                context,
                () => goToNextNamedRoute(
                  context, 
                  workplaceTaskMilestoneScreen, 
                  args: WorkplaceTaskMilestoneArguments(widget.workplaceMoreArgs.projectID ?? 0, null, widget.workplaceMoreArgs.propertyName), 
                  callback: () => _fetchWorkplaceDetails()
                ),
                'Project Tasks'
              ),
        ],
      ),
    );
  }

  Widget _buildListItem(BuildContext context, void Function()? onTapFunction, String label) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque, // Make empty space clickable
      onTap: onTapFunction,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 20, bottom: 20),
            child: Row(
              children: [
                Expanded(
                  flex: 10,
                  child: Text(
                    label,
                    style: boldFontsStyle(height: 1.32)
                  )
                ),
                SvgPicture.asset(
                  ic_arrow_right_workplace,
                  width: 24.0,
                  color: black,
                ),
              ],
            ),
          ),
          Divider(thickness: 1,),
        ],
      ),
    );
  }
}