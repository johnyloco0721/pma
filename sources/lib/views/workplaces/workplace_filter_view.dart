import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter/cupertino.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/view_models/workplace_view_model.dart';

class WorkplaceFilter extends StatefulWidget {
  const WorkplaceFilter({Key? key}) : super(key: key);

  @override
  _WorkplaceFilterState createState() => _WorkplaceFilterState();
}

class _WorkplaceFilterState extends State<WorkplaceFilter> {
  WorkplaceViewModel viewModel = locator<WorkplaceViewModel>();
  List<String> filterList = [];
  bool allSwitch = true;
  bool attentionSwitch = true;
  bool unreadSwitch = true;
  bool readSwitch = true;
  bool archivedSwitch = false;

  @override
  void initState() {
    super.initState();
    setState(() {
      filterList = viewModel.workplaceFilterList.value;
      attentionSwitch = filterList.length == 0 ? true : (filterList.contains('attention') ? true : false);
      unreadSwitch = filterList.length == 0 ? true : (filterList.contains('unread') ? true : false);
      readSwitch = filterList.length == 0 ? true : (filterList.contains('read') ? true : false);
      archivedSwitch = filterList.length == 0 ? false : (filterList.contains('archive') ? true : false);
      allSwitch = this.attentionSwitch && this.unreadSwitch && this.readSwitch && this.archivedSwitch;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteGrey,
      appBar: _buildAppBar(context),
      body: _buildFilterList(context),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      actions: [
        GestureDetector(
          onTap: () {
            viewModel.workplaceFilterList.value = filterList;
            Navigator.pop(context);
          },
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: SvgPicture.asset(ic_close, width: 24.0, color: black),
          ),
        ),
        SizedBox(width: 10.0),
      ],
      actionsIconTheme: IconThemeData(color: black, size: 30),
      backgroundColor: whiteGrey,
      automaticallyImplyLeading: false,
      elevation: 0,
    );
  }

  // Filter list widget
  Widget _buildFilterList(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 30, bottom: 30, left: 20, right: 20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Filter title "Show:"
            Text(
              'Show :',
              style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, height: 1.3, letterSpacing: 0.8),
            ),
            SizedBox(height: 20),
            // Filter 1: All
            _buildSwitch(context, type: 'all', label: 'All', switchValue: allSwitch, icon: null),
            // Filter 2: Attention
            _buildSwitch(context, type: 'attention', label: 'Attention', switchValue: attentionSwitch, icon: ic_workplace_filter_attention, activeColor: red, inactiveColor: grey),
            // Filter 3: New Unreads
            _buildSwitch(context, type: 'unread', label: 'New Unreads', switchValue: unreadSwitch, icon: ic_workplace_filter_unread, activeColor: darkYellow, inactiveColor: grey),
            // Filter 4: Read
            _buildSwitch(context, type: 'read', label: 'Read', switchValue: readSwitch, icon: ic_workplace_filter_read, activeColor: black, inactiveColor: grey),
            // Filter 4: Read
            _buildSwitch(context, type: 'archive', label: 'Archived', switchValue: archivedSwitch, icon: ic_workplace_filter_archived, activeColor: black, inactiveColor: grey),
            SizedBox(height: 50),
            Container(
              height: 56,
              width: double.infinity,
              child: OutlinedButton (
                style: OutlinedButton.styleFrom(
                  foregroundColor: black,
                  textStyle: boldFontsStyle(height: 1.32),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.0)),
                  side: BorderSide(width: 1, color: black),
                ),
                onPressed: () {
                  resetFilter();
                },
                child: Text ("Reset to Default"),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Build filter switch
  Widget _buildSwitch(BuildContext context, {String type = 'all', String label = '', bool switchValue = true, String? icon, Color activeColor = yellow, Color inactiveColor = grey}) {
    return Padding(
      padding: EdgeInsets.only(bottom: 10),
      child: Row(
        children: [
          if (icon != null)
          SvgPicture.asset(
            icon,
            width: 20.0,
            color: switchValue ? activeColor : inactiveColor,
          ),
          if (icon != null)
          SizedBox(width: 20),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  label,
                  style: switchValue 
                  ? boldFontsStyle(height: 1.32) 
                  : regularFontsStyle(color: grey, height: 1.32),
                ),
              ],
            ),
          ),
          SizedBox(width: 20),
          Transform.scale(
            scale: 0.7,
            alignment: Alignment.centerRight,
            child:CupertinoSwitch(
              value: switchValue,
              onChanged: (value) {
                updateFilterSwitch(type, value);
              },
              trackColor: grey,
              activeColor: darkYellow,
            ),
          ),
        ],
      ),
    );
  }

  // Update switch value
  void updateFilterSwitch(String type, bool value) {
    switch (type) {
      case 'all':
        setState(() {
          allSwitch = value;
          attentionSwitch = value;
          unreadSwitch = value;
          readSwitch = value;
          archivedSwitch = value;
        });
        updateFilterList();
        break;
      case 'attention':
        setState(() {
          attentionSwitch = value;
          allSwitch = (attentionSwitch && unreadSwitch && readSwitch && archivedSwitch);
        });
        updateFilterList();
        break;
      case 'read':
        setState(() {
          readSwitch = value;
          allSwitch = (attentionSwitch && unreadSwitch && readSwitch && archivedSwitch);
        });
        updateFilterList();
        break;
      case 'unread':
        setState(() {
          unreadSwitch = value;
          allSwitch = (attentionSwitch && unreadSwitch && readSwitch && archivedSwitch);
        });
        updateFilterList();
        break;
      case 'archive':
        setState(() {
          archivedSwitch = value;
          allSwitch = (attentionSwitch && unreadSwitch && readSwitch && archivedSwitch);
        });
        updateFilterList();
        break;
    }
  }

  // Update filterList array
  void updateFilterList() {
    if (attentionSwitch) {
      if (!filterList.contains('attention')) filterList.add('attention');
    } else {
      if (filterList.contains('attention')) filterList.remove('attention');
    }

    if (unreadSwitch) {
      if (!filterList.contains('unread')) filterList.add('unread');
    } else {
      if (filterList.contains('unread')) filterList.remove('unread');
    }

    if (readSwitch) {
      if (!filterList.contains('read')) filterList.add('read');
    } else {
      if (filterList.contains('read')) filterList.remove('read');
    }

    if (archivedSwitch) {
      if (!filterList.contains('archive')) filterList.add('archive');
    } else {
      if (filterList.contains('archive')) filterList.remove('archive');
    }
  }

  // Reset filter switch
  void resetFilter() {
    setState(() {
      allSwitch = false;
      attentionSwitch = true;
      unreadSwitch = true;
      readSwitch = true;
      archivedSwitch = false;
    });
    allSwitch = (attentionSwitch && unreadSwitch && readSwitch && archivedSwitch);
    filterList = ['attention', 'unread', 'read'];
  }
}