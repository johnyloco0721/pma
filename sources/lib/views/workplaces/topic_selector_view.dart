import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/services/topic_service.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/view_models/workplace_view_model.dart';

class TopicSelector extends StatefulWidget {
  const TopicSelector({Key? key}) : super(key: key);

  @override
  _TopicSelectorState createState() => _TopicSelectorState();
}

class _TopicSelectorState extends State<TopicSelector> {
  WorkplaceViewModel workplaceViewModel = locator<WorkplaceViewModel>();
  TopicService _topicService = TopicService();
  List<Topic> topics = [];

  final localIconPath = {
    "General" : ic_category_generic,
    "Site Visit" : ic_category_site_visit,
    "Design/Planning" : ic_category_design_planning,
    "Order / Delivery" : ic_category_order_delivery,
    "Site Work" : ic_category_site_work,
    "Defects / Complaints" : ic_category_defects_complains,
    "Finale" : ic_category_finale,
    "System" : ic_category_sytem,
    "Notes" : ic_category_notes
  };

  @override
  void initState() {
    super.initState();

    topics = workplaceViewModel.topicList.value;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteGrey,
      appBar: _buildAppBar(context),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.all(20),
            child: Text(
              'Select Category:',
              style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, height: 1.3, letterSpacing: 0.8),
            ),
          ),
          Expanded(
            child: ReorderableListView.builder(
              padding: EdgeInsets.only(left: 24, right: 24),
              itemCount: topics.length,
              onReorder: (int oldIndex, int newIndex) {
                setOrder(oldIndex, newIndex);
              },
              itemBuilder: (context, index) {
                return _buildListItem(context, topics[index], index);
              },
            ),
          ),
        ],
      ),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      systemOverlayStyle: SystemUiOverlayStyle.light,
      backgroundColor: black,
      actions: [
        GestureDetector(
          onTap: () {
            // Save topic order
            _topicService.saveTopicOrder(this.topics);
            // Back to chat
            Navigator.pop(context);
          },
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: SvgPicture.asset(ic_close, width: 20.0, color: white),
          ),
        ),
        SizedBox(width: 10.0),
      ],
    );
  }
    
  // Topic list item
  Widget _buildListItem(BuildContext context, Topic topic, int index) {
    return ListTile(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
      contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      horizontalTitleGap: 0,
      key: Key('$index'),
      leading: SvgPicture.network(
        topic.topicIcon ?? '',
        color: hexToColor(topic.topicColour ?? ''),
        placeholderBuilder: (context) => SvgPicture.asset(
          localIconPath[topic.topicName] ?? '',
          color: hexToColor(topic.topicColour ?? ''),
        ),
      ),
      title: Text(
        topic.topicName ?? '',
        style: regularFontsStyle(color: hexToColor(topic.topicColour ?? ''), height: 1.32),
      ),
      trailing: SvgPicture.asset(ic_drag, width: 24.0),
      onTap: () {
        // Set selected topic
        workplaceViewModel.selectedTopic.value = topic;
        // Save topic order
        _topicService.saveTopicOrder(this.topics);
        // Back to chat
        Navigator.pop(context);
      },
    );
  }

  // Update topic order
  void setOrder(oldIndex, newIndex) {
    setState(() {
      if (oldIndex < newIndex) {
        newIndex -= 1;
      }
      final item = topics.removeAt(oldIndex);
      topics.insert(newIndex, item);
    });
  }
}