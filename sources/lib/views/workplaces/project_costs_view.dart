import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:open_filex/open_filex.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:sources/args/media_slider_args.dart';

import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/models/costs_media_model.dart';
import 'package:sources/models/project_costs_model.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/services/workplace_service.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/scroll.utils.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/widgets/content_widget.dart';
import 'package:sources/widgets/html_widget/html_stylist.dart';
import 'package:sources/widgets/video_thumbnail_widget.dart';

import '../../constants/app_lottie_jsons.dart';

class ProjectCostsView extends StatefulWidget {
  final int projectId;
  const ProjectCostsView({Key? key, required this.projectId})
      : super(key: key);

  @override
  _ProjectCostsViewState createState() => _ProjectCostsViewState();
}

class _ProjectCostsViewState extends State<ProjectCostsView> {
  final database = locator<AppDatabase>();
  WorkplaceService _workplaceService = WorkplaceService();
  ScrollController _scrollController = ScrollController();
  List<ExpandableController> expandableControllers = [];
  List<ProjectCostsDetails>? projectCostsList = [];
  ProjectCostsModel? projectCostsModel;
  bool is_loading = false;

  @override
  void initState() {
    // implement initState
    super.initState();
    fetchProjectCosts();
  }

  fetchProjectCosts() async {
    setState(() {
      is_loading = true;
    });

    await _workplaceService.getMyProjectCost(projectID: widget.projectId).then((response) {
      projectCostsModel = response;
      projectCostsList = projectCostsModel?.response;

      for (int i = 0; i < (projectCostsList?.length ?? 0); i++) {
        expandableControllers.add(ExpandableController());
      }
    }); 

    setState(() {
      is_loading = false;
    });
  }

  @override
  void dispose() {
    // implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      appBar: _buildAppBar(context),
      body: is_loading 
        ? _loading(context) 
        : RefreshIndicator(
          color: yellow,
          backgroundColor: white,
          onRefresh: () async {
            fetchProjectCosts();
          },
          child: Container(
            padding: EdgeInsets.only(left: 24.0, right: 24.0, top: 24.0, bottom: 24.0),
            child: projectCostsList?.length != 0 
              ? _buildCostListItem(context) 
              : _buildEmptyResult(context)
            ),
        ),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.light,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: SvgPicture.asset(ic_chevron_left, width: 10.0),
      ),
      title: Text(
        'Project Costs',
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: boldFontsStyle(
            fontFamily: platForm,
            fontSize: 20.0,
            color: white,
            letterSpacing: 0.8),
      ),
      flexibleSpace: GestureDetector(
        onTap: () {
          // onTap app bar empty space then reset list scroll position
          scrollToTop(_scrollController);
        },
      ),
      centerTitle: false,
      backgroundColor: black,
    );
  }

  Widget _loading(context) {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Lottie.asset(
            lottie_loading_spinner,
            width: 50.0,
            height: 50.0,
            fit: BoxFit.fill,
          ),
          SizedBox(height: 16.0,),
          Container(
            // width: 250.0,
            child: Text(
              'just a few secs...',
              style: regularFontsStyle(fontSize: 14.0, color: grey),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }

  // Empty result widget
  Widget _buildEmptyResult(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Lottie.asset(
            lottie_project_costs,
            width: 250.0,
            height: 250.0,
            fit: BoxFit.fill,
          ),
          Container(
            width: 250.0,
            child: Text(
              'There’s no quotation yet',
              style: regularFontsStyle(fontSize: 17.0, height: 1.35),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCostListItem(BuildContext context) {
    return ListView.separated(
        physics: const AlwaysScrollableScrollPhysics(),
        controller: _scrollController,
        itemCount: projectCostsList?.length ?? 0,
        itemBuilder: (BuildContext context, int index) { 
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  RichText(
                    text: HTML.toTextSpan(
                      context,
                      '${projectCostsList?[index].costName}:',
                      defaultTextStyle: boldFontsStyle(fontSize: 14.0, color: grey),
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(
                    width: 8.0,
                  ),
                  RichText(
                    text: HTML.toTextSpan(
                      context,
                       'RM ${projectCostsList?[index].costAmount ?? ''}',
                      defaultTextStyle: boldFontsStyle(fontSize: 14.0, color: black),
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
              if (projectCostsList?[index].costIndication != '' && projectCostsList?[index].costIndication != null)
              RichText(
                text: HTML.toTextSpan(
                  context,
                  projectCostsList?[index].costIndication ?? '',
                  defaultTextStyle: italicFontsStyle(
                    fontSize: 14.0, color: grey, fontWeight: FontWeight.w400),
                ),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              if (projectCostsList?[index].bookingFeeTxt != '' && projectCostsList?[index].bookingFeeTxt != null)
              RichText(
                text: HTML.toTextSpan(
                  context,
                  projectCostsList?[index].bookingFeeTxt ?? '',
                  defaultTextStyle: italicFontsStyle(
                    fontSize: 14.0, color: grey, fontWeight: FontWeight.w400),
                ),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(height: 8.0,),
              _buildMediaList(context, projectCostsList?[index].media ?? []),
              _buildPDFList(context, projectCostsList?[index].document ?? [], index),
              SizedBox(height: 8.0,),
              if (projectCostsList?[index].costRemark != '' && projectCostsList?[index].costRemark != null)
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                decoration: BoxDecoration(
                  color: whiteGrey,
                  border: Border.all(color: lightGrey),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: ContentWidget(
                  content: projectCostsList?[index].costRemark ?? '',
                  linkColor: linkYellow,
                  maxLines: 1,
                  fontSize: 14,
                  readmoreText: 'Read More',
                  readlessText: 'Collapse',
                ),
              ),
              SizedBox(height: 8.0,),
              Row(
                children: [
                  Text(
                    formatDatetime(datetime: projectCostsList?[index].postDateTime ?? '', format: 'd MMM y'),
                    style: regularFontsStyle(color: grey, fontSize: 12.0, height: 1.27),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: Icon(
                      Icons.circle,
                      color: lightGrey,
                      size: 4,
                    ),
                  ),
                  Text(
                    formatDatetime(datetime: projectCostsList?[index].postDateTime ?? '', format: 'h:mma'),
                    style: regularFontsStyle(color: grey, fontSize: 12.0, height: 1.27),
                  ),
                ],
              ),
              SizedBox(height: 10.0,),
            ],
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(
          color: grey
        ),
    );
  }

  Widget _buildMediaList(BuildContext context, List<CostsMediaModel> mediaList) {
    List<CostsMediaModel> imageList = mediaList.where((e) => e.fileExtension != 'pdf').toList();
    return LayoutBuilder(
      builder: (context, constraints) {
        double imageWidth = (constraints.maxWidth) / 6;

        return Wrap(
          children: [
            for (var index = 0; index < (imageList.length <= 5 ? imageList.length : 5); index++)
              GestureDetector(
                onTap: () {
                  List<MediaInfoArguments> mediaArgsList = imageList.map((m) => MediaInfoArguments(
                    mediaPath: m.url ?? '',
                    mediaType: m.fileExtension ?? '',
                  )).toList();

                  goToNextNamedRoute(context, mediaSliderScreen, args: MediaSliderArguments(mediaArgsList, index));
                  // widget.onImageClicked(imageList, feeds, index);
                },
                child: (imageList[index].fileExtension == 'MOV' || imageList[index].fileExtension == 'mp4') ? Container(
                  width: imageWidth,
                  height: imageWidth,
                  margin: EdgeInsets.only(right: 10, bottom: 10),
                  child: Stack(
                    children: [
                      AspectRatio(
                        aspectRatio: 1,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: VideoThumbnailWidget(
                            videoPath: imageList[index].url ?? '',
                            height: imageWidth,
                            width: imageWidth,
                          ),
                        ),
                      ),
                      if (imageList.length > 5 && index == 4)
                      Positioned.fill(
                        child: Container(
                          decoration: BoxDecoration(
                            color: darkGrey.withOpacity(0.7),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Center(
                            child: Text(
                              '+' + (imageList.length - 4).toString(),
                              style: boldFontsStyle(fontFamily: platForm, fontSize: 30.0, letterSpacing: 0.8, height: 1.33, color: white),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ) : Container(
                  width: imageWidth,
                  height: imageWidth,
                  margin: EdgeInsets.only(right: 10, bottom: 10),
                  child: Stack(
                    children: [
                      AspectRatio(
                        aspectRatio: 1,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: CachedNetworkImage(
                            placeholder: (context, url) => Container(color: Colors.grey[50]),
                            imageUrl: imageList[index].url ?? '',
                            fit: BoxFit.cover,
                            alignment: Alignment.center,
                            maxHeightDiskCache: 120,
                            maxWidthDiskCache: 120,
                          )
                        ),
                      ),
                      if (imageList.length > 5 && index == 4)
                      Positioned.fill(
                        child: Container(
                          decoration: BoxDecoration(
                            color: darkGrey.withOpacity(0.7),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Center(
                            child: Text(
                              '+' + (imageList.length - 4).toString(),
                              style: boldFontsStyle(fontFamily: platForm, fontSize: 30.0, letterSpacing: 0.8, height: 1.33, color: white),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
          ],
        );
      }
    );
  }

  Widget _buildPDFList(BuildContext context, List<CostsMediaModel> mediaList, indexCost) {
    List<CostsMediaModel> pdfList = mediaList.where((e) => e.fileExtension == 'pdf').toList();

    return Wrap(
      children: [
        for (var index = 0; index < (pdfList.length <= 3 ? pdfList.length : 3); index++)
          GestureDetector(
            onTap: () async {
              try {
                late String file;
                // if (pdfList[index].localFilePath != null && await File(media.localFilePath ?? '').exists()) {
                //   file = media.localFilePath!;
                // } else {
                  await SpManager.putInt(pdfList[index].url.toString(), 50);
                  showToastMessage(context, message: 'Downloading', duration: 1);
                  await Api().downloadFile(pdfList[index].url ?? '', 'pdf', context).then((localSavePath) {
                    file = localSavePath;
                    // final newMediaData = pdfList.copyWith(localFilePath: file);
                    // database.mediaDao.insertOrUpdateMedia(newMediaData);
                  });
                // }

                OpenFilex.open(file);
              } on Exception catch (_) {
                showError(context, 'Unexpected error, please try again.');
              }
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              margin: EdgeInsets.only(right: 10, bottom: 10),
              decoration: BoxDecoration(
                border: Border.all(),
                borderRadius: BorderRadius.all(Radius.circular(50.0)),
              ),
              child: Row(
                children: [
                  SvgPicture.asset(ic_doc, width: 18.0, color: darkYellow),
                  SizedBox(width: 8.0),
                  if (pdfList[index].url != null)
                  Expanded(
                    child: Text(
                      (pdfList[index].url )?.substring((pdfList[index].url )!.lastIndexOf("/") + 1) ?? "",
                      style: regularFontsStyle(fontSize: 14.0),
                    ),
                  ),
                ],
              )
            ),
          ),
        if (pdfList.length > 3)
          ExpandableNotifier(
            controller: expandableControllers[indexCost],
            child: Expandable(
              collapsed: ExpandableButton(
                child: GestureDetector(
                  onTap: () {
                    expandableControllers[indexCost].toggle();
                  },
                  child: Text(
                    '${pdfList.length-3} More...',
                    style: boldFontsStyle(fontSize: 14.0, color: darkYellow, decoration: TextDecoration.underline),
                  ),
                ),
              ),
              expanded: Wrap(
                children: [
                  for (var index = 3; index < pdfList.length; index++)
                    GestureDetector(
                      onTap: () async {
                        try {
                          late String file;
                          // if (pdfList[index].localFilePath != null && await File(media.localFilePath ?? '').exists()) {
                          //   file = media.localFilePath!;
                          // } else {
                            await SpManager.putInt(pdfList[index].url.toString(), 50);
                            showToastMessage(context, message: 'Downloading', duration: 1);
                            await Api().downloadFile(pdfList[index].url ?? '', 'pdf', context).then((localSavePath) {
                              file = localSavePath;
                              // final newMediaData = pdfList.copyWith(localFilePath: file);
                              // database.mediaDao.insertOrUpdateMedia(newMediaData);
                            });
                          // }

                          OpenFilex.open(file);
                        } on Exception catch (_) {
                          showError(context, 'Unexpected error, please try again.');
                        }
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                        margin: EdgeInsets.only(right: 10, bottom: 10),
                        decoration: BoxDecoration(
                          border: Border.all(),
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                        ),
                        child: Row(
                          children: [
                            SvgPicture.asset(ic_doc, width: 18.0, color: darkYellow),
                            SizedBox(width: 8.0),
                            if (pdfList[index].url != null )
                            Expanded(
                              child: Text(
                                (pdfList[index].url)?.substring((pdfList[index].url )!.lastIndexOf("/") + 1) ?? "",
                                style: regularFontsStyle(fontSize: 14.0),
                              ),
                            ),
                          ],
                        )
                      ),
                    ),
                  ExpandableButton(
                    child: GestureDetector(
                      onTap: () {
                        expandableControllers[indexCost].toggle();
                      },
                      child: Container(
                        margin: EdgeInsets.only(bottom: 12.0),
                        child: Text(
                          'Collapse...',
                          style: boldFontsStyle(
                            fontSize: 14.0, color: darkYellow),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ),
      ],
    );
  }

}
