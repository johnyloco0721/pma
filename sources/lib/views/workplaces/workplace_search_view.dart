import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:lottie/lottie.dart';
import 'package:sources/args/workplace_args.dart';
import 'package:sources/utils/debouncer_utlis.dart';
import 'package:sources/utils/scroll.utils.dart';
import 'package:substring_highlight/substring_highlight.dart';

import 'package:sources/constants/app_constants.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/services/workplace_service.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/size_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/models/workplace_list_model.dart';

class WorkplaceSearchView extends StatefulWidget {
  const WorkplaceSearchView({Key? key}) : super(key: key);

  @override
  _WorkplaceSearchViewState createState() => _WorkplaceSearchViewState();
}

class _WorkplaceSearchViewState extends State<WorkplaceSearchView> {
  WorkplaceService _workplaceService = WorkplaceService();
  bool isLoading = true;
  ScrollController _scrollController = ScrollController();
  final pagingController = PagingController<int, WorkplaceListDetails>(firstPageKey: 1);
  final searchTextFieldController = TextEditingController();
  String searchKeyword = '';
  final _debouncer = Debouncer(milliseconds: 500);

  @override
  void initState() {
    super.initState();
    pagingController.addPageRequestListener((pageKey) {
      searchWithPagination(pageKey);
    });
  }

  @override
  void dispose() {
    super.dispose();
    searchKeyword = '';
    pagingController.dispose();
    searchTextFieldController.dispose();
  }

  void searchWithPagination(int pageKey) async {
    if (searchKeyword != '') {
      try {
        isLoading = true;
        WorkplaceListModel response = await _workplaceService.searchWorkplace(keyword: searchKeyword, pageKey: pageKey);

        int totalItems = response.totalItems ?? 0;
        int limitPerPage = response.limitPerPage ?? 50;
        int currentPageNo = response.currentPageNo ?? 1;
        int totalPages = (totalItems/limitPerPage).ceil();
        final List<WorkplaceListDetails>? newItems = response.response;

        if (totalItems > 0 && totalPages > (response.currentPageNo ?? 1)) {
          final nextPageKey = currentPageNo + 1;
          pagingController.appendPage(newItems!, nextPageKey);
        } else {
          pagingController.appendLastPage(newItems!);
        }

        isLoading = false;
      }
      catch (error) {
        showError(context, 'Unexpected error, please try again.');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteGrey,
      appBar: _buildAppBar(context),
      body: Container(
        child: searchKeyword == '' ? _buildNoSearchScreen(context) : Container(
          child: RefreshIndicator(
            color: yellow,
            backgroundColor: white,
            onRefresh: () => Future.sync(() => pagingController),
            child: PagedListView.separated(
              scrollController: _scrollController,
              pagingController: pagingController,
              separatorBuilder: (context, index) => Padding(
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Divider(thickness: 1),
              ),
              builderDelegate: PagedChildBuilderDelegate<WorkplaceListDetails>(
                itemBuilder: (context, element, index) => isLoading ? 
                  _buildLoadingIndicator(context) : 
                  _buildListItem(context, element),
                firstPageProgressIndicatorBuilder: (context) {
                  return _buildLoadingIndicator(context);
                },
                noItemsFoundIndicatorBuilder: (context) => _buildEmptyResult(context),
              ),
            ),
          ),
        ),
      ),
    ) ;
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      toolbarHeight: 90,
      systemOverlayStyle: SystemUiOverlayStyle.light,
      title: TextField(
        autofocus: true,
        controller: searchTextFieldController,
        onChanged: (text) {
          // Use debouncer to do api search once user stop typing for 500ms
          _debouncer.run(() {
            setState(() {
              searchKeyword = text;
              pagingController.refresh();
            });
          });
        },
        onEditingComplete: () {},
        style: regularFontsStyle(height: 1.32),
        decoration: InputDecoration(
          suffixIconConstraints: BoxConstraints(minHeight: 10, minWidth: 10),
          hintText: 'Search',
          hintStyle: italicFontsStyle(color: grey, height: 1.32),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: yellow, width: 2.0),
            borderRadius: BorderRadius.circular(12),
          ),
          fillColor: white,
          filled: true,
          contentPadding: EdgeInsets.only(top: 10, bottom: 10, left: 24, right: 24),
          suffixIcon: Padding(
            padding: EdgeInsets.only(right: 25.0),
            child: GestureDetector(
              onTap: () {
                searchKeyword = searchTextFieldController.text;
                pagingController.refresh();
              },
              child: Container(
                child: SvgPicture.asset(ic_search, color: black),
              ),
            ),
          ),
        ),
      ),
      flexibleSpace: GestureDetector(
        onTap: () {
          // onTap app bar empty space then reset list scroll position
          scrollToTop(_scrollController);
        },
      ),
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: SvgPicture.asset(ic_chevron_left, width: 10 ),
      ),
      backgroundColor: black,
      centerTitle: false,
      elevation: 0,
    );
  }

  // Search result item widget
  Widget _buildListItem(BuildContext context, WorkplaceListDetails item) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque, // Make empty space clickable
      onTap: () {
        goToNextNamedRoute(context, 'workplaceChat', args: WorkplaceArguments(item.projectID, null, null, null, null));
      },
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: 8),
              child: SubstringHighlight(
                text: (item.propertyName != null ? item.propertyName.toString() + ' ' : '') + (item.projectUnit ?? ''),
                term: searchKeyword,
                textStyle: regularFontsStyle(height: 1.32),
                textStyleHighlight: boldFontsStyle(color: darkYellow, height: 1.32),
              ),
            ),
            SubstringHighlight(
              text: 'Owner: ' + (item.ownerName ?? '-'),
              term: searchKeyword,
              textStyle: regularFontsStyle(color: grey, fontSize: 15.0, height: 1.27),
              textStyleHighlight: boldFontsStyle(color: darkYellow, fontSize: 15.0, height: 1.27),
            ),
          ],
        ),
      ),
    );
  }

  // Build no search screen
  Widget _buildNoSearchScreen(BuildContext context) {
    return Container(
      color: whiteGrey,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
    );
  }

  // Empty result widget
  Widget _buildEmptyResult(BuildContext context) {
    return Container(
      height: SizeUtils.height(context, 75),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Lottie.asset(lottie_empty_search, width: 250.0, height: 250.0, fit: BoxFit.fill),
            Container(
              width: 250.0,
              child: Text(
                text_empty_search,
                style: regularFontsStyle(fontSize: 17.0, height: 1.35),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Loading indicator widget
  Widget _buildLoadingIndicator(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 30),
            child: Lottie.asset(
              lottie_loading_spinner,
              width: 60.0,
              height: 60.0,
              fit: BoxFit.fill,
            ),
          )
        ],
      )
    );
  }
}