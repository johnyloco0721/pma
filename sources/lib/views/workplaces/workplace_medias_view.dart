import 'dart:io';

import 'package:drift/drift.dart' as DRIFT;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:open_filex/open_filex.dart';
import 'package:sources/args/media_slider_args.dart';

import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/feed_service.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/utils/size_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/widgets/video_thumbnail_widget.dart';

class ProjectMediasView extends StatefulWidget {
  final int projectId;
  const ProjectMediasView({Key? key, required this.projectId}) : super(key: key);

  @override
  _ProjectMediasViewState createState() => _ProjectMediasViewState();
}

class _ProjectMediasViewState extends State<ProjectMediasView> {
  final database = locator<AppDatabase>();
  FeedService _feedService = FeedService();
  int mediaTabCurrentIndex = 0;

  @override
  void initState() {
    // implement initState
    super.initState();
    initialize();
  }

  @override
  void dispose() {
    // implement dispose
    super.dispose();
  }

  void initialize() async {
    await _feedService.fetchAndSaveAssociateMediasToDB(projectID: widget.projectId).then((value) {
      // setState(() {
      //   isLoading = false;
      // });
    });
  }

  void setMediaGallery(int index) {
    setState(() {
      mediaTabCurrentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      appBar: _buildAppBar(context),
      body: _buildProjectMedias(context),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.light,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: SvgPicture.asset(ic_chevron_left, width: 10.0),
      ),
      title: Text(
        'View All Medias & Docs',
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, color: white, letterSpacing: 0.8),
      ),
      centerTitle: false,
      backgroundColor: black,
    );
  }

  // Project media widget
  Widget _buildProjectMedias(BuildContext context) {
    return DefaultTabController(
      key: PageStorageKey<String>('MediaGalleryTabController'),
      initialIndex: mediaTabCurrentIndex,
      length: 2,
      child: NotificationListener(
        onNotification: (ScrollNotification scrollNotification) {
          if (scrollNotification is ScrollEndNotification) {
            if (scrollNotification.metrics.pixels == 0) {
              setMediaGallery(0);
            } else {
              setMediaGallery(1);
            }
          }
          return false;
        },
        child: Column(
          children: <Widget>[
            // Tab bar
            Container(
              padding: EdgeInsets.all(24),
              constraints: BoxConstraints(maxHeight: 150.0),
              child: Material(
                color: transparent,
                child: TabBar(
                  key: PageStorageKey<String>('MediaGalleryTabBar'),
                  labelColor: black,
                  labelStyle: boldFontsStyle(fontSize: 17.0, height: 1.35),
                  unselectedLabelColor: grey,
                  unselectedLabelStyle: regularFontsStyle(fontSize: 17.0, color: grey),
                  // Line below active tab indicator
                  indicatorSize: TabBarIndicatorSize.tab,
                  indicatorColor: darkYellow,
                  dividerColor: transparent,
                  labelPadding: EdgeInsets.only(top: 0.0, bottom: 5.0),
                  tabs: [
                    Text('Medias'),
                    Text('Docs'),
                  ],
                ),
              ),
            ),
            // Tabs content view
            Expanded(
              child: TabBarView(
                key: PageStorageKey<String>('MediaGalleryTabView'),
                children: [
                  _buildMediaTabView(context),
                  _buildDocTabView(context)
                ],
              ),
            )
          ],
        ),
      )
    );
  }

  // Images grid list
  StreamBuilder<List<AssociateMedia>> _buildMediaTabView(BuildContext context) {
    final database = locator<AppDatabase>();
    final associateMediaDao = database.associateMediaDao;

    return StreamBuilder(
      stream: associateMediaDao.watchAssociateImagesWithFeedByProjectID(widget.projectId),
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        final List results = snapshot.data ?? [];
        List<AssociateMedia> medias = List.castFrom(results);

        int itemCount = snapshot.hasData ? snapshot.data!.length : 0;
        
        if (snapshot.hasData && itemCount > 0) {
          return GridView.builder(
            key: PageStorageKey<String>('MediaImages'),
            padding: EdgeInsets.only(left: 24, right: 24, bottom: 24),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisSpacing: 16,
              crossAxisCount: 3,
              childAspectRatio: 1,
              mainAxisSpacing: 16
            ),
            itemCount: itemCount,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: () {
                  List<MediaInfoArguments> mediaArgsList = medias.map((m) => MediaInfoArguments(
                    mediaPath: m.projectFeedsMedia ?? '',
                    mediaDescription: m.caption,
                    mediaType: m.projectFeedsMediaType?? '',
                    datetime: m.createdDateTime
                  )).toList();

                  goToNextNamedRoute(context, mediaSliderScreen, args: MediaSliderArguments(mediaArgsList, index));
                },
                child: Container(
                  child: AspectRatio(
                    aspectRatio: 1,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: (medias[index].projectFeedsMediaType == 'mp4') ? LayoutBuilder(
                        builder: (context, boxConstraint) {
                          return VideoThumbnailWidget(
                            videoPath: medias[index].projectFeedsMedia ?? medias[index].localFilePath!,
                            height: boxConstraint.maxHeight,
                            width: boxConstraint.maxWidth,
                          );
                        }
                      ) : medias[index].projectFeedsMedia == null 
                      ? Image.file(
                        File(medias[index].localFilePath!),
                        alignment: Alignment.center,
                        fit: BoxFit.cover,
                        cacheHeight: SizeUtils.height(context, 30).toInt(),
                        cacheWidth: SizeUtils.width(context, 60).toInt(),
                      ) : CachedNetworkImage(
                        placeholder: (context, url) => Container(color: Colors.grey[50]),
                        imageUrl: medias[index].projectFeedsMedia ?? '',
                        fit: BoxFit.cover,
                        alignment: Alignment.center,
                        maxHeightDiskCache: 120,
                        maxWidthDiskCache: 120,
                      )
                    ),
                  ),
                ),
              );
            },
          );
        } else {
          return _buildImagesEmtpyResult(context);
        }
      }
    );
  }

  // Empty result for images
  Widget _buildImagesEmtpyResult(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 24),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Lottie.asset(
              lottie_empty_media,
              width: 250.0,
              height: 250.0,
              fit: BoxFit.fill,
            ),
            Container(
              width: 250.0,
              child: Text(
                'There is no media\nat the moment',
                style: regularFontsStyle(fontSize: 17.0, height: 1.35), 
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Documents list widget
  StreamBuilder<List<AssociateMedia>> _buildDocTabView(BuildContext context) {
    final database = locator<AppDatabase>();
    final associateMediaDao = database.associateMediaDao;

    return StreamBuilder(
      stream: associateMediaDao.watchAssociateDocumentsWithFeedByProjectID(widget.projectId),
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        final List results = snapshot.data ?? [];
        List<AssociateMedia> medias = List.castFrom(results);

        int itemCount = snapshot.hasData ? snapshot.data!.length : 0;
        
        if (snapshot.hasData && itemCount > 0) {
          return ListView.builder(
            physics: const AlwaysScrollableScrollPhysics(),
            key: PageStorageKey<String>('MediaDocuments'),
            padding: EdgeInsets.only(left: 24, right: 24, bottom: 24),
            itemCount: itemCount,
            itemBuilder: (BuildContext context, int index) {
              return _buildDocItem(context, medias[index]);
            }
          );
        } else {
          // TO DO: show empty result
          return _buildDocsEmtpyResult(context);
        }
      },
    );
  }

  // Document list item
  Widget _buildDocItem(BuildContext context, AssociateMedia media) {
    String url = media.projectFeedsMedia ?? '';
    List arr = url.split('/').toList();
    String filename = arr.last.toString();

    return GestureDetector(
      behavior: HitTestBehavior.opaque, // Make empty space clickable
      onTap: () async {
        // go to pdf viewer
        try {
          late String file;
          if (media.localFilePath != null && await File(media.localFilePath ?? '').exists()){
            file = media.localFilePath!;
          } else {
            await SpManager.putInt(media.projectFeedsMedia.toString(), 50);
            showToastMessage(context, message: 'Downloading', duration: 1);
            await Api().downloadFile(media.projectFeedsMedia ?? '', 'pdf', context).then((localSavePath) {
              file = localSavePath;
              final newMediaData = media.copyWith(
                localFilePath: DRIFT.Value(file)
              );
              database.associateMediaDao.insertOrUpdateAssociateMedia(newMediaData);
            });
          }

          OpenFilex.open(file);
        } 
        on Exception catch (_) {
          showError(context, 'Unexpected error, please try again.');
        }
      },
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 24),
            child: Row(
              children: [
                Container(
                  child: SvgPicture.asset(
                    ic_pdf_document,
                    color: black,
                  ),
                ),
                SizedBox(
                  width: 16,
                ),
                Expanded(
                  child: Text(
                    filename,
                    style: boldFontsStyle(height: 1.32),
                  ),
                ),
              ],
            ),
          ),
          Divider(thickness: 1),
        ],
      ),
    );
  }

  // Empty result for documents
  Widget _buildDocsEmtpyResult(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 24),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Lottie.asset(
              lottie_empty_docs,
              width: 250.0,
              height: 250.0,
              fit: BoxFit.fill,
            ),
            Container(
              width: 250.0,
              child: Text(
                'There is no document\nat the moment',
                style: regularFontsStyle(fontSize: 17.0, height: 1.35),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
