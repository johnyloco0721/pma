import 'dart:async';
import 'dart:io';

import 'package:bubble/bubble.dart';
import 'package:clipboard/clipboard.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:drift/drift.dart' as MOOR;
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:cron/cron.dart';
import 'package:path/path.dart' as p;
import 'package:sources/args/chat_reply_args.dart';
import 'package:sources/args/forward_args.dart';
import 'package:sources/args/media_slider_args.dart';
import 'package:sources/args/workplace_args.dart';
import 'package:sources/args/workplace_more_args.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/models/recover_workplace_model.dart';
import 'package:sources/services/api.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:uuid/uuid.dart';
import 'package:open_filex/open_filex.dart';

import 'package:sources/constants/app_constants.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/database/feeds.dart';
import 'package:sources/models/feed_post_model.dart';
import 'package:sources/models/general_response_model.dart';
import 'package:sources/services/feed_service.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/services/workplace_service.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/size_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/view_models/app_view_model.dart';
import 'package:sources/view_models/workplace_view_model.dart';
import 'package:sources/widgets/backdrop.dart';
import 'package:sources/widgets/chat_bubble.dart';
import 'package:sources/widgets/custom_mentions/custom_mention_view.dart';
import 'package:sources/widgets/label_separator.dart';
import 'package:sources/widgets/post_form.dart';
import 'package:sources/widgets/skeleton.dart';
import 'package:sources/widgets/system_bubble.dart';
import 'package:sources/widgets/topic_pill.dart';

import '../../models/workplace_details_model.dart';

class WorkplaceChatView extends StatefulWidget {
  final WorkplaceArguments workplaceArguments;
  const WorkplaceChatView({Key? key, required this.workplaceArguments}) : super(key: key);

  @override
  _WorkplaceChatViewState createState() => _WorkplaceChatViewState();
}

class _WorkplaceChatViewState extends State<WorkplaceChatView> {
  final database = locator<AppDatabase>();
  final AppViewModel appViewModel = locator<AppViewModel>();
  final WorkplaceViewModel workplaceViewModel = locator<WorkplaceViewModel>();
  FeedService _feedService = FeedService();
  WorkplaceService _workplaceService = WorkplaceService();
  bool isLoading = false;
  bool isEmpty = false;
  String workplaceStatus = 'read';
  Workplace? currentWorkplace;
  int onlyClickOnce = 0;

  List<int> filterList = [];
  int totalTopics = 0;
  final AutoScrollController autoScrollController = new AutoScrollController(axis: Axis.vertical);
  bool isShowTopicSelector = false;
  GlobalKey<CustomMentionsState> textfieldKey = GlobalKey<CustomMentionsState>(debugLabel: 'ChatTextKey');
  List<Map<String, dynamic>> mentionsList = [];
  String lastUpdateDateTime = '';

  Cron? cron;
  List<String> avatarList = [];
  TagsModel? tags;

  @override
  void initState() {
    // implement initState
    super.initState();
    isLoading = true;

    _workplaceService.getWorkplaceDetail(widget.workplaceArguments.projectID!).then((value) {
      database.workplaceDao.getSingleWorkplace(widget.workplaceArguments.projectID!).then((workplace) {
        if (workplace != null) {
          setState(() {
            currentWorkplace = workplace;
            workplaceStatus = workplace.isRead ?? 'read';
          });
        }
      });
    });

    _feedService.syncDeletedFeeds(widget.workplaceArguments.projectID!);

    database.topicDao.getTopics().then((List<Topic> topics) {
      // Remove system topic
      topics.removeWhere((element) => element.topicName == 'System');
      workplaceViewModel.topicList.value = topics;
      if (topics.length > 0) {
        workplaceViewModel.defaultTopic = topics.firstWhere((element) => element.topicID == 1);
        workplaceViewModel.selectedTopic.value = workplaceViewModel.defaultTopic;
      }
    });

    database.teamMemberDao.getTeamMembersByProject(widget.workplaceArguments.projectID!).then((List<TeamMember> teamMembers) {
      teamMembers.forEach((member) {
        mentionsList.add({
          'id': member.teamMemberID.toString(),
          'display': member.teamMemberName,
          'teamMemberName': member.teamMemberName,
          'avatar': member.teamMemberAvatar,
          'designation': member.designationName,
        });
      });
    });

    database.topicDao.getTopics().then((topics) {
      workplaceViewModel.chatFilterList.value = topics.map((e) => e.topicID).toList();
      totalTopics = topics.length;
      filterList = workplaceViewModel.chatFilterList.value;
    });

    // Fetch feeds data from API every 15 seconds
    cron = Cron()
      ..schedule(Schedule.parse('*/5 * * * * *'), () async {
        await database.feedDao.getLatestUpdateFeed(widget.workplaceArguments.projectID!).then((latestUpdateWorkplace) {
          lastUpdateDateTime = latestUpdateWorkplace!.updatedDateTime!;
        }).onError((error, stackTrace) {
          print('Latest Update Datetime no found.');
        });
        await _feedService.fetchAndSaveFeedsToDB(projectID: widget.workplaceArguments.projectID!, lastUpdateDateTime: lastUpdateDateTime).then((value) {
          // checkNewUnread();
        });
        await _feedService.syncDeletedFeeds(widget.workplaceArguments.projectID!);
      });

    initialize();
  }

  void initialize() async {
    await database.feedDao.getLatestUpdateFeed(widget.workplaceArguments.projectID!).then((latestUpdateWorkplace) {
      lastUpdateDateTime = latestUpdateWorkplace!.updatedDateTime!;
    }).onError((error, stackTrace) {
      print('Latest Update Datetime no found.');
    });
    await _feedService.fetchAndSaveFeedsToDB(projectID: widget.workplaceArguments.projectID!, lastUpdateDateTime: '').then((value) {
      setState(() {
        isLoading = false;
      });
    });
    // Redirect to next route once widget built completed
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      checkNewUnread();
      jumpToNextView();
    });
  }

  void jumpToNextView() {
    if (widget.workplaceArguments.feedID != null) {
      database.feedDao.getSingleFeed(widget.workplaceArguments.feedID!).then((feed) {
        if (feed != null) {
          workplaceViewModel.isShowNewUnreads.value = false;
          goToNextNamedRoute(context, 'replyChat', args: ChatReplyArguments(widget.workplaceArguments.projectID!, widget.workplaceArguments.feedID!));
        } else {
          jumpToNextView();
        }
      });
    } else if (widget.workplaceArguments.goToAllTaskMilestone != null) {
      goToWorkplaceMore();
    }
  }

  void goToWorkplaceMore() {
    database.workplaceDao.getSingleWorkplace(currentWorkplace!.projectID).then((workplace) {
      goToNextNamedRoute(context, 'workplaceMore', args: WorkplaceMoreArguments(workplace?.projectID, (workplace?.propertyName ?? '') + ' ' + (workplace?.projectUnit ?? ''), workplace?.ownerName, workplace?.completedTasks, workplace?.totalTasks, widget.workplaceArguments.goToAllTaskMilestone));
    });
  }

  void checkNewUnread() {
    if (currentWorkplace?.projectID != null) {
      database.feedDao.getFeeds(currentWorkplace!.projectID).then((feeds) {
        var unreadFeeds = feeds.where((e) => e.isRead == 'unread');
        var selfFeeds = unreadFeeds.where((e) => e.teamMemberID == workplaceViewModel.currentUser!.teamMemberID);
        if (selfFeeds.length > 0) {
          unreadFeeds = unreadFeeds.where((e) => e.feedID > selfFeeds.first.feedID);
        }
        var totalUnreads = unreadFeeds.length;
        var unreadIndex = 0;
        if (totalUnreads > 0) {
          feeds.asMap().forEach((index, feed) {
            if (feed.feedID == unreadFeeds.last.feedID) {
              unreadIndex = index;
            }
          });
        }
        workplaceViewModel.isShowNewUnreads.value = totalUnreads > 0 ? true : false;
        workplaceViewModel.totalUnreads.value = totalUnreads;
        workplaceViewModel.lastUnreadIndex.value = unreadIndex;
      });
    }
  }

  void toggleFilter() {
    setState(() {
      filterList = workplaceViewModel.chatFilterList.value;
    });
  }

  void submitForm(String formValue) async {
    var newUUID = Uuid().v4();
    int newFeedID = -1;
    int newMediasID = -1;
    List<int> mediaIds = [];
    bool uplaodImageFailed = false;

    await database.teamMemberDao.getSingleTeamMember(currentWorkplace!.projectID, workplaceViewModel.currentUser!.teamMemberID!).then((teamMember) {
      if (teamMember == null) {
        database.teamMemberDao.insertTeamMember(TeamMembersCompanion(
          teamMemberID: MOOR.Value(workplaceViewModel.currentUser!.teamMemberID!),
          teamMemberName: MOOR.Value(workplaceViewModel.currentUser!.name),
          teamMemberAvatar: MOOR.Value(workplaceViewModel.currentUser!.avatar),
          designationName: MOOR.Value(workplaceViewModel.currentUser!.designationName),
          projectID: MOOR.Value(currentWorkplace!.projectID),
          show: MOOR.Value(false),
        ));
      }
    });

    await database.mediaDao.getLowestMediaID().then((mediaData) {
      if (mediaData.length > 0) {
        if (mediaData[0].mediaID < 0) newMediasID = mediaData[0].mediaID - 1;
      }
    });

    await database.feedDao.getLowestFeedID().then((feedData) async {
      if (feedData.length > 0) {
        if (feedData[0].feedID < 0) newFeedID = feedData[0].feedID - 1;
      }

      if (workplaceViewModel.mediaPdfFiles.value.length > 0) {
        for (var mediaFile in workplaceViewModel.mediaPdfFiles.value) {
          database.mediaDao.insertMedia(MediasCompanion(
            mediaID: MOOR.Value(newMediasID),
            projectID: MOOR.Value(currentWorkplace!.projectID),
            feedID: MOOR.Value(newFeedID),
            fileExtension: MOOR.Value(p.extension(mediaFile).substring(1)),
            localFilePath: MOOR.Value(mediaFile),
            feedUuid: MOOR.Value(newUUID),
          ));

          final formData = FormData.fromMap({'fileUpload': await MultipartFile.fromFile(mediaFile)});
          await _feedService.postMedia(formData).then((response) {
            GeneralResponseModel mediaResponse = GeneralResponseModel.fromJson(response.data);

            database.mediaDao.updateMediaData(newMediasID, mediaResponse.response['mediaID'], mediaResponse.response['url']);
            mediaIds.add(mediaResponse.response['mediaID']);
          }).onError((error, stackTrace) {
            uplaodImageFailed = true;
            showError(context, error.toString());
          }).timeout(Duration(seconds: 60), onTimeout: () {
            setState(() {
              onlyClickOnce = 0;
            });
          }).whenComplete(() {
            setState(() {
              onlyClickOnce = 0;
            });
          });
          newMediasID--;
        }
      }

      if (workplaceViewModel.mediaImageFiles.value.length > 0) {
        for (var mediaFile in workplaceViewModel.mediaImageFiles.value) {
          database.mediaDao.insertMedia(MediasCompanion(
            mediaID: MOOR.Value(newMediasID),
            projectID: MOOR.Value(currentWorkplace!.projectID),
            feedID: MOOR.Value(newFeedID),
            fileExtension: MOOR.Value(p.extension(mediaFile).substring(1)),
            localFilePath: MOOR.Value(mediaFile),
            feedUuid: MOOR.Value(newUUID),
          ));
          File renamedFile;
          if (Platform.isIOS) {
            String fileExtension = p.extension(mediaFile);
            String uniqueIdentifier = DateTime.now().millisecondsSinceEpoch.toString();
            String shortenedFileName = p.basenameWithoutExtension(mediaFile).substring(0, 10) + '_' + uniqueIdentifier + fileExtension;
            String shortenedFilePath = p.join(p.dirname(mediaFile), shortenedFileName);
            renamedFile = File(mediaFile).renameSync(shortenedFilePath);
          } else {
            renamedFile = File(mediaFile);
          }

          final formData = FormData.fromMap({'fileUpload': await MultipartFile.fromFile(renamedFile.path)});
          await _feedService.postMedia(formData).then((response) {
            GeneralResponseModel mediaResponse = GeneralResponseModel.fromJson(response.data);

            database.mediaDao.updateMediaData(newMediasID, mediaResponse.response['mediaID'], mediaResponse.response['url']);
            mediaIds.add(mediaResponse.response['mediaID']);
          }).onError((error, stackTrace) {
            uplaodImageFailed = true;
            showError(context, error.toString());
          }).timeout(Duration(seconds: 60), onTimeout: () {
            setState(() {
              onlyClickOnce = 0;
            });
          }).whenComplete(() {
            setState(() {
              onlyClickOnce = 0;
            });
          });
          newMediasID--;
        }
      }

      FeedPostModel formData = FeedPostModel(projectID: currentWorkplace!.projectID, topicID: workplaceViewModel.selectedTopic.value!.topicID, description: formValue, replyID: null, mediaID: mediaIds, uuid: newUUID);

      final convertedDesc = formValue.replaceAllMapped(RegExp(r'@\[\d+\]'), (match) {
        int stringLength = match.group(0)?.length ?? 0;
        if (stringLength > 0) {
          var matchID = match.group(0)?.substring(2, stringLength - 1);
          var matchedMember = mentionsList.firstWhere((member) => member['id'] == matchID);
          String replaceName = matchedMember['teamMemberName'];

          return '<b style="color: #B0782A;">$replaceName</b>';
        }
        return '${match.group(0)}';
      });

      database.feedDao.insertFeed(FeedsCompanion(uuid: MOOR.Value(formData.uuid), feedID: MOOR.Value(newFeedID), parentID: MOOR.Value(null), projectID: MOOR.Value(formData.projectID), description: MOOR.Value(convertedDesc), feedType: MOOR.Value("normal"), postDateTime: MOOR.Value(formatDatetime(datetime: DateTime.now().toString(), format: 'yyyy-MM-dd HH:mm:ss')), topicID: MOOR.Value(formData.topicID), teamMemberID: MOOR.Value(workplaceViewModel.currentUser!.teamMemberID), replyTeamMemberAvatar: MOOR.Value(''), replyCount: MOOR.Value(0), replyReadStatus: MOOR.Value("read"), sendStatus: MOOR.Value(uplaodImageFailed ? 0 : 2), isRead: MOOR.Value("read")));

      if (!uplaodImageFailed) {
        _feedService.postNewFeed(formData).then((response) {
          GeneralResponseModel result = GeneralResponseModel.fromJson(response.data);
          if (result.status == true) {
            database.feedDao.updateFeedStatus(newUUID, response.data["response"]["feedID"], 1);
            database.mediaDao.updateMediaFeedID(newFeedID, response.data["response"]["feedID"]);
          } else {
            if (response.data["response"]["projectStatus"] == "archive") {
              database.feedDao.deleteAllUnsentFeeds();
              database.workplaceDao.updateWorkplace(currentWorkplace!.copyWith(projectStatus: MOOR.Value("archive")));
              setState(() {
                workplaceStatus = 'archive';
              });
            } else {
              database.feedDao.updateFeedStatus(newUUID, newFeedID, 0);
            }
            showError(context, result.message ?? "");
          }
        }).onError((error, stackTrace) {
          database.feedDao.updateFeedStatus(newUUID, newFeedID, 0);
          showError(context, error.toString());
        }).timeout(Duration(seconds: 60), onTimeout: () {
          setState(() {
            onlyClickOnce = 0;
          });
        }).whenComplete(() {
          setState(() {
            onlyClickOnce = 0;
          });
        });
      }
    });

    workplaceViewModel.temporaryTextFieldValue.value = null;
    workplaceViewModel.clearForm();
    if (textfieldKey.currentState != null) {
      textfieldKey.currentState!.controller!.text = '';
      textfieldKey.currentState!.controller!.clear();
    }
    workplaceViewModel.isPostFeedLoading.value = false;
  }

  void submitUpdate(String formValue) async {
    List<int> mediaIds = workplaceViewModel.selectedFeedWithDetail.value?.mediaList?.map((e) => e.mediaID).toList() ?? [];
    bool uplaodImageFailed = false;
    await Future.forEach(workplaceViewModel.mediaPdfFiles.value, (dynamic mediaPath) async {
      if (mediaPath[0] == '/') {
        final formData = FormData.fromMap({'fileUpload': await MultipartFile.fromFile(mediaPath)});
        await _feedService.postMedia(formData).then((response) {
          GeneralResponseModel mediaResponse = GeneralResponseModel.fromJson(response.data);
          mediaIds.add(mediaResponse.response['mediaID']);
        }).onError((error, stackTrace) {
          uplaodImageFailed = true;
          showError(context, error.toString());
        });
      }
    }).timeout(Duration(seconds: 60), onTimeout: () {
      setState(() {
        onlyClickOnce = 0;
      });
    }).whenComplete(() {
      setState(() {
        onlyClickOnce = 0;
      });
    });

    await Future.forEach(workplaceViewModel.mediaImageFiles.value, (dynamic mediaPath) async {
      if (mediaPath[0] == '/') {
        File renamedFile;
        if (Platform.isIOS) {
          String fileExtension = p.extension(mediaPath);
          String uniqueIdentifier = DateTime.now().millisecondsSinceEpoch.toString();
          String shortenedFileName = p.basenameWithoutExtension(mediaPath).substring(0, 10) + '_' + uniqueIdentifier + fileExtension;
          String shortenedFilePath = p.join(p.dirname(mediaPath), shortenedFileName);
          renamedFile = File(mediaPath).renameSync(shortenedFilePath);
        } else {
          renamedFile = File(mediaPath);
        }
        final formData = FormData.fromMap({'fileUpload': await MultipartFile.fromFile(renamedFile.path)});
        await _feedService.postMedia(formData).then((response) {
          GeneralResponseModel mediaResponse = GeneralResponseModel.fromJson(response.data);
          mediaIds.add(mediaResponse.response['mediaID']);
        }).onError((error, stackTrace) {
          uplaodImageFailed = true;
          showError(context, error.toString());
        });
      }
    }).timeout(Duration(seconds: 60), onTimeout: () {
      setState(() {
        onlyClickOnce = 0;
      });
    }).whenComplete(() {
      setState(() {
        onlyClickOnce = 0;
      });
    });

    FeedPutModel formData = FeedPutModel(
      feedID: workplaceViewModel.selectedFeedWithDetail.value!.feedDetail.feedID,
      topicID: workplaceViewModel.selectedTopic.value!.topicID,
      mediaID: mediaIds,
      description: formValue,
    );

    if (!uplaodImageFailed) {
      _feedService.updateFeed(formData).then((response) {
        GeneralResponseModel result = GeneralResponseModel.fromJson(response.data);
        if (result.status == true) {
          workplaceViewModel.temporaryTextFieldValue.value = null;
          workplaceViewModel.clearForm();
          hideToolbox();
          _feedService.fetchAndSaveFeedsToDB(projectID: currentWorkplace!.projectID);
        } else {
          showError(context, result.message ?? "");
        }
        workplaceViewModel.isPostFeedLoading.value = false;
      }).onError((error, stackTrace) {
        showError(context, error.toString());
        workplaceViewModel.isPostFeedLoading.value = false;
      }).timeout(Duration(seconds: 60), onTimeout: () {
        setState(() {
          onlyClickOnce = 0;
        });
      }).whenComplete(() {
      setState(() {
        onlyClickOnce = 0;
      });
    });
    }
    workplaceViewModel.isPostFeedLoading.value = false;
  }

  void showToolbox(FeedWithDetail feedWithDetail) {
    workplaceViewModel.toolboxMode.value = 'show';
    workplaceViewModel.selectedFeedWithDetail.value = feedWithDetail;
  }

  void hideToolbox() {
    workplaceViewModel.toolboxMode.value = 'none';

    workplaceViewModel.feedDescriptionNode.unfocus();
    workplaceViewModel.selectedTopic.value = workplaceViewModel.defaultTopic;
    workplaceViewModel.mediaImageFiles.value = [];
    workplaceViewModel.mediaPdfFiles.value = [];

    if (textfieldKey.currentState != null) {
      textfieldKey.currentState!.controller!.text = '';
      textfieldKey.currentState!.controller!.clear();
    }
  }

  @override
  void dispose() async {
    // implement dispose
    super.dispose();
    // update all feeds read status when back button is clicked.
    database.feedDao.updateReadStatus(currentWorkplace!.projectID, null);
    // update workplace read status when back button is clicked.
    if (workplaceStatus != 'archive') {
      currentWorkplace = currentWorkplace?.copyWith(isRead: MOOR.Value('read'));
      database.workplaceDao.updateWorkplace(currentWorkplace!);
    }
    // call workplace API to make sure latest workplace will be loaded when back fromm chat view
    await database.workplaceDao.getLatestUpdateWorkplace().then((latestUpdateWorkplace) {
      lastUpdateDateTime = latestUpdateWorkplace!.updatedDateTime!;
    }).onError((error, stackTrace) {
      print('Latest Update Datetime no found.');
    });
    _workplaceService.fetchWorkplaceListAndSaveToDB(lastUpdateDateTime: lastUpdateDateTime);

    cron?.close();
    autoScrollController.dispose();
    filterList.clear();
    workplaceViewModel.isShowNewUnreads.value = false;
    workplaceViewModel.isShowTopicSelector.value = false;
    workplaceViewModel.isPostFeedLoading.value = false;
    workplaceViewModel.toolboxMode.value = 'none';
    workplaceViewModel.isShowScrollBottomButton.value = false;
    mentionsList.clear();
  }

  @override
  Widget build(BuildContext context) {
    autoScrollController.addListener(() {
      if (autoScrollController.position.pixels > 100) {
        workplaceViewModel.isShowScrollBottomButton.value = true;
      } else {
        workplaceViewModel.isShowScrollBottomButton.value = false;
      }
    });

    return Scaffold(
      key: PageStorageKey<String>('WorkplaceChat'),
      backgroundColor: white,
      appBar: _buildAppBar(context),
      body: RefreshIndicator(
        color: yellow,
        backgroundColor: white,
        onRefresh: () async {
          await _feedService.fetchAndSaveFeedsToDB(projectID: widget.workplaceArguments.projectID!, lastUpdateDateTime: lastUpdateDateTime);

          database.workplaceDao.getSingleWorkplace(widget.workplaceArguments.projectID!).then((workplace) {
            if (workplace != null) {
              setState(() {
                currentWorkplace = workplace;
                workplaceStatus = workplace.isRead ?? 'read';
              });
            }
          });
        },
        child: Stack(
          alignment: Alignment.center,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                // Feeds list, Scroll to Bottom & backdrop
                Expanded(
                  child: Stack(
                    children: [
                      // Build feed listing
                      _buildFeedList(context),
                      // Build scroll to bottom button
                      Positioned(
                        bottom: 15,
                        right: 15,
                        child: _scrollToBottomButton(),
                      ),
                      // Build backdrop here when it is edit mode
                      ValueListenableBuilder(
                          valueListenable: workplaceViewModel.toolboxMode,
                          builder: (context, String toolboxMode, Widget? child) {
                            return toolboxMode == 'edit' ? ClipPath(child: Backdrop(onTap: () => hideToolbox())) : Container();
                          }),
                      ValueListenableBuilder(
                          valueListenable: workplaceViewModel.toolboxMode,
                          builder: (context, String toolboxMode, Widget? child) {
                            return toolboxMode == 'edit' ? _buildSelectedBubbleWithToolbox() : Container();
                          }),
                    ],
                  ),
                ),
                // Build Topic Selector
                ValueListenableBuilder(
                    valueListenable: workplaceViewModel.isShowTopicSelector,
                    builder: (BuildContext context, bool isShowTopicSelector, Widget? child) {
                      return isShowTopicSelector ? _buildTopicSelector(context) : Container();
                    }),
                // Form, if workplace archive will hide
                if (workplaceStatus != 'archive')
                  Container(
                    child: ValueListenableBuilder(
                        valueListenable: workplaceViewModel.toolboxMode,
                        builder: (BuildContext context, String toolboxMode, Widget? child) {
                          return PostForm(
                            textfieldKey: textfieldKey,
                            editMode: (toolboxMode == 'edit') ? true : false,
                            teamMembers: mentionsList,
                            focusNode: workplaceViewModel.feedDescriptionNode,
                            onTextChanged: (text) {
                              workplaceViewModel.temporaryTextFieldValue.value = text;
                            },
                            submitOnTap: () {
                              String formValue = textfieldKey.currentState!.controller!.markupText.trim();
                              if (formValue != '' || workplaceViewModel.mediaPdfFiles.value.length > 0 || workplaceViewModel.mediaImageFiles.value.length > 0) {
                                setState(() {
                                  onlyClickOnce = onlyClickOnce + 1;
                                });
                                workplaceViewModel.isPostFeedLoading.value = true;
                                if (onlyClickOnce == 1)
                                submitForm(formValue);
                                // update all feeds read status when send button is clicked.
                                database.feedDao.updateReadStatus(currentWorkplace!.projectID, null);
                                workplaceViewModel.isShowNewUnreads.value = false;
                                autoScrollController.jumpTo(0);
                              }
                            },
                            updateOnTap: () {
                              String formValue = textfieldKey.currentState!.controller!.markupText;
                              if (formValue != '' || workplaceViewModel.mediaPdfFiles.value.length > 0 || workplaceViewModel.mediaImageFiles.value.length > 0) {
                                workplaceViewModel.isPostFeedLoading.value = true;
                                setState(() {
                                  onlyClickOnce = onlyClickOnce + 1;
                                });
                                if (onlyClickOnce == 1)
                                submitUpdate(formValue);
                                // update all feeds read status when send button is clicked.
                                database.feedDao.updateReadStatus(currentWorkplace!.projectID, null);
                              }
                            },
                            isDisable: onlyClickOnce >= 1
                          );
                        }),
                  ),
              ],
            ),
            // Build backdrop when it's not edit mode
            ValueListenableBuilder(
                valueListenable: workplaceViewModel.toolboxMode,
                builder: (context, String toolboxMode, Widget? child) {
                  return toolboxMode == 'show'
                      ? ClipPath(child: Backdrop(onTap: () => hideToolbox()))
                      : toolboxMode == 'comment'
                          ? ClipPath(child: Backdrop(onTap: () => hideToolbox()))
                          : Container();
                }),
            // Build selected bubble with toolbox
            ValueListenableBuilder(
                valueListenable: workplaceViewModel.toolboxMode,
                builder: (context, String toolboxMode, Widget? child) {
                  return toolboxMode != 'none' && toolboxMode != 'edit' && toolboxMode != 'comment'
                      ? _buildSelectedBubbleWithToolbox()
                      : toolboxMode == 'comment'
                          ? _buildSelectedSystemBubbleWithToolbox()
                          : Container();
                }),
          ],
        ),
      ),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.light,
      centerTitle: false,
      backgroundColor: black,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: SvgPicture.asset(ic_chevron_left, width: 10.0),
      ),
      flexibleSpace: GestureDetector(
        onTap: () {
          // onTap app bar empty space then scroll up a bit
          if (autoScrollController.positions.isNotEmpty) {
            if (autoScrollController.offset + 3000.0 < autoScrollController.position.maxScrollExtent)
              autoScrollController.animateTo(autoScrollController.offset + 3000.0, duration: Duration(milliseconds: 1000), curve: Curves.ease);
            else
              autoScrollController.animateTo(autoScrollController.position.maxScrollExtent, duration: Duration(milliseconds: 1000), curve: Curves.ease);
          }
        },
      ),
      actions: [
        // Chat filter
        GestureDetector(
          onTap: () {
            Navigator.pushNamed(context, 'chatFilter').then((value) => toggleFilter());
          },
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: filterList.length == totalTopics
                ?
                // Icon filter inactive
                SvgPicture.asset(ic_filter, width: 20.0, color: white)
                :
                // Icon filter active
                SvgPicture.asset(ic_workplace_chat_filter_active, width: 20.0),
          ),
        ),
        // Workplace details
        GestureDetector(
          onTap: () {
            goToWorkplaceMore();
          },
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: SvgPicture.asset(ic_workplace_more, width: 20.0, color: white),
          ),
        ),
        SizedBox(width: 10.0),
      ],
      title: Text(
        (currentWorkplace?.propertyName ?? '') + ' ' + (currentWorkplace?.projectUnit ?? ''),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, color: white, letterSpacing: 0.8),
      ),
      // only show when workplace is archive
      bottom: (workplaceStatus == 'archive')
          ? PreferredSize(
              child: Container(
                color: yellow,
                height: 42.0,
                width: double.infinity,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'This project is archived. ',
                      style: regularFontsStyle(fontSize: 14.0),
                    ),
                    // Only Consultants will have this function shown on their phone.
                    if (workplaceViewModel.currentUser?.designationName == 'Strategy Consultant')
                      GestureDetector(
                        onTap: () {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                              backgroundColor: white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12),
                              ),
                              content: Text(
                                'Are you sure you want to recover this project - Fera Residence, B-10-06?',
                                style: boldFontsStyle(fontSize: 17.0, height: 1.35),
                                textAlign: TextAlign.center,
                              ),
                              actions: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        minimumSize: Size(132.0, 58.0),
                                        backgroundColor: yellow,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(30.0),
                                        ),
                                      ),
                                      onPressed: () {
                                        Navigator.pop(context);
                                        setState(() {
                                          workplaceStatus = 'read';
                                        });
                                        _feedService.recoverWorkplace(RecoverWorkplaceModel(projectID: currentWorkplace!.projectID, isArchived: 0));
                                        showMessage(context, message: 'Recovered!');
                                      },
                                      child: Text("Recover", style: boldFontsStyle(height: 1.32)),
                                    ),
                                    OutlinedButton(
                                      style: OutlinedButton.styleFrom(
                                        minimumSize: Size(132.0, 58.0),
                                        backgroundColor: black,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(30.0),
                                        ),
                                        side: BorderSide(width: 1, color: black),
                                      ),
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: Text("Cancel", style: boldFontsStyle(height: 1.32)),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          );
                        },
                        child: Text(
                          'Click to recover project.',
                          style: boldFontsStyle(fontSize: 14.0),
                        ),
                      ),
                  ],
                ),
              ),
              preferredSize: Size.fromHeight(42.0))
          : PreferredSize(child: Container(), preferredSize: Size.fromHeight(0.0)),
    );
  }

  // Workplace feeds stream builder
  StreamBuilder<List<FeedWithDetail>> _buildFeedList(BuildContext context) {
    return StreamBuilder(
        stream: database.feedDao.watchFeedsWithDetail(widget.workplaceArguments.projectID!, filterList),
        builder: (BuildContext context, AsyncSnapshot<List<FeedWithDetail>> snapshot) {
          List<FeedWithDetail> feeds = snapshot.data ?? [];
          var itemCount = snapshot.hasData ? snapshot.data?.length : 0;
          var unreadFeedID = -1;
          var unreadFeeds = feeds.where((e) => e.feedDetail.isRead == "unread");
          var selfFeeds = unreadFeeds.where((e) => e.feedDetail.teamMemberID == workplaceViewModel.currentUser!.teamMemberID);
          if (selfFeeds.length > 0) {
            unreadFeeds = unreadFeeds.where((e) => e.feedDetail.feedID > selfFeeds.first.feedDetail.feedID);
          }
          if (unreadFeeds.length > 0) {
            unreadFeedID = unreadFeeds.last.feedDetail.feedID;
          }

          if (isLoading) {
            return _buildSkeleton(context);
          } else {
            return Stack(
              alignment: Alignment.center,
              children: [
                ListView.builder(
                    addAutomaticKeepAlives: true,
                    controller: autoScrollController,
                    physics: workplaceViewModel.toolboxMode.value != 'none' ? const NeverScrollableScrollPhysics() : const AlwaysScrollableScrollPhysics(),
                    key: PageStorageKey<String>('WorkplaceChat' + (currentWorkplace?.projectID ?? 0).toString()),
                    // key: _listKey,
                    itemCount: itemCount,
                    // Sort newest chat item at the bottom & make the list always stick to bottom
                    reverse: true,
                    shrinkWrap: false,
                    itemBuilder: (BuildContext context, int index) {
                      bool showDate = false;
                      bool showUnreadLabel = false;
                      final FeedWithDetail feedWithDetail = feeds[index];
                      FeedWithDetail? nextfeedWithDetail;
                      DateTime now = DateTime.now();
                      var currentDate = formatDatetime(datetime: feedWithDetail.feedDetail.postDateTime ?? '');
                      var todayDate = formatDatetime(datetime: now.toString());
                      if (index < (itemCount! - 1)) {
                        nextfeedWithDetail = feeds[index + 1];
                        var nextDate = formatDatetime(datetime: nextfeedWithDetail.feedDetail.postDateTime ?? '', format: 'd MMM y');
                        if (currentDate != nextDate) {
                          showDate = true;
                        }
                      }
                      // If the feed is the oldest feed
                      else if (index == (itemCount - 1)) {
                        showDate = true;
                      }
                      if (unreadFeedID == feedWithDetail.feedDetail.feedID && feedWithDetail.teamMemberDetail!.teamMemberID != workplaceViewModel.currentUser!.teamMemberID) {
                        showUnreadLabel = true;
                        workplaceViewModel.lastUnreadIndex.value = index;
                      }
                      Topic? topic = feedWithDetail.topicDetail;

                      bool showLabel = false;
                      if (showDate || showUnreadLabel) {
                        showLabel = true;
                      }

                      return Container(
                        padding: EdgeInsets.only(top: (index == (itemCount - 1)) ? 30 : 0, bottom: (index == 0) ? 30 : 0),
                        child: Column(
                          children: [
                            // Date separator
                            if (showDate) LabelSeparator(label: (currentDate == todayDate) ? 'Today' : currentDate, color: grey),
                            // New label
                            if (showUnreadLabel) LabelSeparator(label: (workplaceViewModel.lastUnreadIndex.value + 1).toString() + ' New Unread', color: darkYellow),
                            // List item
                            AutoScrollTag(
                              key: ValueKey(feedWithDetail.feedDetail.feedID),
                              controller: autoScrollController,
                              index: index,
                              child: (topic != null && topic.topicName == 'System') ? _buildSystemItem(context, feedWithDetail, index) : _buildListItem(context, feedWithDetail, nextfeedWithDetail, index, showLabel),
                            ),
                          ],
                        ),
                      );
                    }),
                // Build New Unreads Button
                ValueListenableBuilder(
                  valueListenable: workplaceViewModel.isShowNewUnreads,
                  builder: (context, bool isShowNewUnreads, Widget? child) {
                    return isShowNewUnreads
                        ? Positioned(
                            top: 20,
                            child: GestureDetector(
                              onTap: () {
                                autoScrollController.scrollToIndex(workplaceViewModel.lastUnreadIndex.value, preferPosition: AutoScrollPosition.middle).then((value) {
                                  // After user click, get latest workplace from database
                                  database.workplaceDao.getSingleWorkplace(currentWorkplace!.projectID).then((workplace) {
                                    currentWorkplace = workplace;
                                  });
                                  // Hide unread button
                                  workplaceViewModel.isShowNewUnreads.value = false;
                                });
                              },
                              child: _buildUnreadButton(),
                            ),
                          )
                        : Container();
                  },
                ),
                if (itemCount == 0) _buildEmptyResult(context),
              ],
            );
          }
        });
  }

  // Feed item widget
  Widget _buildListItem(BuildContext context, FeedWithDetail feedWithDetail, FeedWithDetail? nextfeedWithDetail, int index, bool showLabel) {
    bool showNip = true;
    String type = 'other';
    if (feedWithDetail.teamMemberDetail!.teamMemberID == workplaceViewModel.currentUser!.teamMemberID) {
      type = 'self';
    }
    if (nextfeedWithDetail != null) {
      if (nextfeedWithDetail.feedDetail.teamMemberID == feedWithDetail.feedDetail.teamMemberID && nextfeedWithDetail.feedDetail.feedType != "system") {
        showNip = false;
      }
    }
    return ValueListenableBuilder(
        valueListenable: workplaceViewModel.toolboxMode,
        builder: (context, String toolboxMode, Widget? child) {
          bool showBubble = (workplaceViewModel.toolboxMode.value != 'none' && workplaceViewModel.toolboxMode.value != 'comment' && workplaceViewModel.selectedFeedWithDetail.value!.feedDetail.feedID == feedWithDetail.feedDetail.feedID) ? false : true;
          return ChatBubble(
            key: ValueKey(feedWithDetail.feedDetail.feedID),
            feedWithDetail: feedWithDetail,
            index: index,
            type: type,
            showBubble: showBubble,
            showTopic: true,
            showNip: showNip,
            showToolbox: false,
            enableReplyButton: true,
            workplaceStatus: workplaceStatus,
            onResend: () async {
              List<int> mediaIds = [];
              bool uplaodImageFailed = false;
              database.feedDao.updateFeedStatus(feedWithDetail.feedDetail.uuid!, feedWithDetail.feedDetail.feedID, 2);

              var mediasList = await database.mediaDao.getMedias(feedWithDetail.feedDetail.feedID);
              for (var mediaData in mediasList) {
                if (mediaData.mediaID < 0) {
                  final formData = FormData.fromMap({'fileUpload': await MultipartFile.fromFile(mediaData.localFilePath!)});
                  await _feedService.postMedia(formData).then((response) {
                    GeneralResponseModel mediaResponse = GeneralResponseModel.fromJson(response.data);

                    database.mediaDao.updateMediaData(mediaData.mediaID, mediaResponse.response['mediaID'], mediaResponse.response['url']);
                    mediaIds.add(mediaResponse.response['mediaID']);
                  }).onError((error, stackTrace) {
                    uplaodImageFailed = true;
                    showError(context, error.toString());
                  });
                } else {
                  mediaIds.add(mediaData.mediaID);
                }
              }

              FeedPostModel formData = FeedPostModel(projectID: feedWithDetail.feedDetail.projectID, topicID: feedWithDetail.feedDetail.topicID!, description: feedWithDetail.feedDetail.description!, replyID: feedWithDetail.feedDetail.parentID, mediaID: mediaIds, uuid: feedWithDetail.feedDetail.uuid!);

              if (!uplaodImageFailed) {
                _feedService.postNewFeed(formData).then((response) {
                  GeneralResponseModel result = GeneralResponseModel.fromJson(response.data);
                  if (result.status == true) {
                    database.feedDao.updateFeedStatus(feedWithDetail.feedDetail.uuid!, response.data["response"]["feedID"], 1);
                    database.mediaDao.updateMediaFeedID(feedWithDetail.feedDetail.feedID, response.data["response"]["feedID"]);
                  } else {
                    if (response.data["response"]["projectStatus"] == "archive") {
                      database.feedDao.deleteAllUnsentFeeds();
                      database.workplaceDao.updateWorkplace(currentWorkplace!.copyWith(projectStatus: MOOR.Value("archive")));
                      setState(() {
                        workplaceStatus = 'archive';
                      });
                    } else {
                      database.feedDao.updateFeedStatus(feedWithDetail.feedDetail.uuid!, feedWithDetail.feedDetail.feedID, 0);
                    }
                    showError(context, result.message ?? "");
                  }
                }).onError((error, stackTrace) {
                  database.feedDao.updateFeedStatus(feedWithDetail.feedDetail.uuid!, feedWithDetail.feedDetail.feedID, 0);
                  showError(context, error.toString());
                });
              } else {
                database.feedDao.updateFeedStatus(feedWithDetail.feedDetail.uuid!, feedWithDetail.feedDetail.feedID, 0);
              }
            },
            onLongPressCallback: (offset) {
              final viewInsets = EdgeInsets.fromWindowPadding(WidgetsBinding.instance.window.viewInsets,WidgetsBinding.instance.window.devicePixelRatio);
                RenderBox bubbleSize = context.findRenderObject() as RenderBox;
                double statusBarHeight = MediaQuery.of(context).padding.top;
                double appBarHeight = _buildAppBar(context).preferredSize.height;
                double maxOffsetY = MediaQuery.of(context).size.height - statusBarHeight - appBarHeight - 64;
                double originalOffsetY = offset.y;
                double minOffset = maxOffsetY / 6;
                // offset Y checking
                if ((originalOffsetY + bubbleSize.size.height) > maxOffsetY) {
                  offset.y = minOffset - viewInsets.bottom;
                } else {
                  offset.y = maxOffsetY - (originalOffsetY + bubbleSize.size.height) - viewInsets.bottom;
                }
                // min offset Y checking
                if (offset.y < 0.0) {
                  offset.y = minOffset;
                }
                workplaceViewModel.selectedBubbleOffset.value = offset.y;
                workplaceViewModel.selectedFeedWithDetail.value = feedWithDetail;
                showToolbox(feedWithDetail);
            },
            onReply: () {
              workplaceViewModel.isShowNewUnreads.value = false;
              goToNextNamedRoute(context, 'replyChat', args: ChatReplyArguments(feedWithDetail.feedDetail.projectID, feedWithDetail.feedDetail.feedID));
              hideToolbox();
            },
            onPdfClicked: (Media media) async {
              try {
                late String file;
                if (media.localFilePath != null && await File(media.localFilePath ?? '').exists()) {
                  file = media.localFilePath!;
                } else {
                  await SpManager.putInt(media.url.toString(), 50);
                  showToastMessage(context, message: 'Downloading', duration: 1);
                  await Api().downloadFile(media.url ?? '', 'pdf', context).then((localSavePath) {
                    file = localSavePath;
                    final newMediaData = media.copyWith(localFilePath: MOOR.Value(file));
                    database.mediaDao.insertOrUpdateMedia(newMediaData);
                  });
                }

                OpenFilex.open(file);
              } on Exception catch (_) {
                showError(context, 'Unexpected error, please try again.');
              }
            },
            onImageClicked: (List<Media> medias, List<Feed> feeds, int index) {
              List<MediaInfoArguments> mediaArgsList = medias.map((m) => MediaInfoArguments(mediaPath: m.url ?? '', mediaDescription: feeds[index].description, mediaType: m.fileExtension ?? '', datetime: feeds[index].postDateTime)).toList();

              goToNextNamedRoute(context, mediaSliderScreen, args: MediaSliderArguments(mediaArgsList, index));
            },
          );
        });
  }

// Feed item widget
  Widget _buildSystemItem(BuildContext context, FeedWithDetail feedWithDetail, int index) {
    return ValueListenableBuilder(
        valueListenable: workplaceViewModel.toolboxMode,
        builder: (context, String toolboxMode, Widget? child) {
          return SystemBubble(
            key: ValueKey(feedWithDetail.feedDetail.feedID),
            feedWithDetail: feedWithDetail,
            index: index,
            showToolbox: false,
            showTopic: true,
            onLongPressCallback: (offset) async {
              RenderBox bubbleSize = context.findRenderObject() as RenderBox;
              double statusBarHeight = MediaQuery.of(context).padding.top;
              double appBarHeight = _buildAppBar(context).preferredSize.height;
              double maxOffsetY = MediaQuery.of(context).size.height - statusBarHeight - appBarHeight - 64;
              // set a min offset Y checking
              if (offset.y < (statusBarHeight + appBarHeight + 64)) {
                offset.y = statusBarHeight + appBarHeight + 64;
              }
              // set a max offset Y checking
              if ((offset.y + bubbleSize.size.height) > maxOffsetY) {
                offset.y = maxOffsetY - bubbleSize.size.height;
              }
              workplaceViewModel.selectedBubbleOffset.value = offset.y;
              workplaceViewModel.selectedFeedWithDetail.value = feedWithDetail;
              workplaceViewModel.toolboxMode.value = 'comment';
            },
          );
        });
  }

  // Topic selector widget
  Widget _buildTopicSelector(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: workplaceViewModel.topicList,
        builder: (BuildContext context, List<Topic> topics, Widget? child) {
          var btnMore = Topic(
            topicID: 0,
            topicName: 'More',
            topicColour: '#FFD200',
            topicIcon: '',
            weight: (topics.length + 1),
          );
          return Container(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              height: 50,
              child: ListView.builder(
                  key: PageStorageKey<String>('TopicSelector'),
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: 6,
                  itemBuilder: (context, index) {
                    Topic topic = (index == 5) ? btnMore : topics[index];
                    return TopicPill(
                      text: topic.topicName!,
                      fontSize: 15.0,
                      fontWeight: (topic.topicID == 0) ? FontWeight.w700 : FontWeight.w400,
                      color: (topic.topicID == 0) ? black : hexToColor(topics[index].topicColour!),
                      backgroundColor: (topic.topicID == 0) ? hexToColor(btnMore.topicColour!) : white,
                      borderColor: (topic.topicID == 0) ? hexToColor(btnMore.topicColour!) : hexToColor(topics[index].topicColour!),
                      margin: EdgeInsets.only(left: (index == 0) ? 15 : 6, right: (index == 5) ? 15 : 6),
                      onTap: () {
                        if (topic.topicID == 0) {
                          Navigator.pushNamed(context, 'topicSelector').then((value) => initialize());
                        } else {
                          workplaceViewModel.selectedTopic.value = topic;
                        }
                        workplaceViewModel.isShowTopicSelector.value = false;
                      },
                    );
                  }));
        });
  }

  // Duplicated Selected Bubble
  Widget _buildSelectedBubbleWithToolbox() {
    String type = 'other';
    if (workplaceViewModel.selectedFeedWithDetail.value!.teamMemberDetail!.teamMemberID == workplaceViewModel.currentUser!.teamMemberID) {
      type = 'self';
    }

    return ValueListenableBuilder(
        valueListenable: workplaceViewModel.toolboxMode,
        builder: (context, String toolboxMode, Widget? child) {
          FeedWithDetail selectedFeedWithDetail = workplaceViewModel.selectedFeedWithDetail.value!;
          return Positioned(
            width: SizeUtils.width(context, 100),
            bottom: toolboxMode == 'edit' ? (isShowTopicSelector ? 50 + 50 : 50) : workplaceViewModel.selectedBubbleOffset.value,
            left: 0,
            child: ChatBubble(
              key: ValueKey(selectedFeedWithDetail.feedDetail.feedID),
              feedWithDetail: selectedFeedWithDetail,
              index: 0,
              type: type,
              showBubble: true,
              showTopic: true,
              showNip: true,
              showToolbox: toolboxMode != 'none' && toolboxMode != 'edit',
              enableReplyButton: workplaceStatus != 'archive', //reply button
              enableEditButton: workplaceStatus != 'archive', //edit button
              enableDeleteButton: workplaceStatus != 'archive', //delete button
              workplaceStatus: workplaceStatus,
              onReply: () {
                workplaceViewModel.isShowNewUnreads.value = false;
                goToNextNamedRoute(context, 'replyChat', args: ChatReplyArguments(selectedFeedWithDetail.feedDetail.projectID, selectedFeedWithDetail.feedDetail.feedID));
                hideToolbox();
              },
              onForward: () async {
                List<String> tmpLocalURLs = await downloadMediaToCache(context, selectedFeedWithDetail.mediaList ?? []);

                goToNextNamedRoute(context, 'workplaceForward', args: ForwardArguments(removeAllHtmlTags(selectedFeedWithDetail.feedDetail.description ?? ''), tmpLocalURLs));
                hideToolbox();
              },
              onShare: () {
                workplaceShareDialog(
                  context, 
                  selectedFeedWithDetail.feedDetail.description ?? '',
                  selectedFeedWithDetail.mediaList ?? []
                );
                hideToolbox();
              },
              onEdit: () {
                Topic editTopic = workplaceViewModel.topicList.value.firstWhere((element) => element.topicID == selectedFeedWithDetail.feedDetail.topicID);
                workplaceViewModel.selectedTopic.value = editTopic;
                String putSymbol = (selectedFeedWithDetail.feedDetail.description ?? '').replaceAll(RegExp(r'<h6[^>]*>'), '@');
                textfieldKey.currentState!.controller!.text = Bidi.stripHtmlIfNeeded(putSymbol).trim();
                if (selectedFeedWithDetail.mediaList!.length > 0) {
                  workplaceViewModel.mediaPdfFiles.value = selectedFeedWithDetail.mediaList!.where((e) => e.fileExtension == 'pdf').map((e) => e.url ?? e.localFilePath).toList();
                  workplaceViewModel.mediaImageFiles.value = selectedFeedWithDetail.mediaList!.where((e) => e.fileExtension != 'pdf').map((e) => e.url ?? e.localFilePath).toList();
                }
                workplaceViewModel.toolboxMode.value = 'edit';
                workplaceViewModel.feedDescriptionNode.requestFocus();
              },
              onCopy: () async {
                List<String> tmpLocalURLs = await downloadMediaToCache(context, selectedFeedWithDetail.mediaList ?? []);

                hideToolbox();
                FlutterClipboard.copy(
                  removeAllHtmlTags(
                    reverseMarkupText(selectedFeedWithDetail.feedDetail.description ?? '')
                  )
                );
                workplaceViewModel.temporaryCopiesMediaFiles.value = tmpLocalURLs;

                showMessage(context, message: 'Copied!');
              },
              onDelete: () {
                hideToolbox();
                workplaceViewModel.isPostFeedLoading.value = true;
                _feedService.deleteFeed(selectedFeedWithDetail.feedDetail).then((value) {
                  workplaceViewModel.isPostFeedLoading.value = false;
                }).onError((error, stackTrace) {
                  print('Something went wrong, please try again!');
                });
                workplaceViewModel.isPostFeedLoading.value = false;
              },
              onPdfClicked: (Media media) async {
                try {
                  late String file;
                  if (media.localFilePath != null && await File(media.localFilePath ?? '').exists()) {
                    file = media.localFilePath!;
                  } else {
                    await SpManager.putInt(media.url.toString(), 50);
                    showToastMessage(context, message: 'Downloading', duration: 1);
                    await Api().downloadFile(media.url ?? '', 'pdf', context).then((localSavePath) {
                      file = localSavePath;
                      final newMediaData = media.copyWith(localFilePath: MOOR.Value(file));
                      database.mediaDao.insertOrUpdateMedia(newMediaData);
                    });
                  }

                  OpenFilex.open(file);
                } on Exception catch (_) {
                  showError(context, 'Unexpected error, please try again.');
                }
              },
              onImageClicked: (List<Media> medias, List<Feed> feeds, int index) {
                List<MediaInfoArguments> mediaArgsList = medias.map((m) => MediaInfoArguments(mediaPath: m.url ?? '', mediaDescription: feeds[index].description, mediaType: m.fileExtension ?? '', datetime: feeds[index].postDateTime)).toList();

                goToNextNamedRoute(context, mediaSliderScreen, args: MediaSliderArguments(mediaArgsList, index));
              },
            ),
          );
        });
  }

// Duplicated Selected System Bubble
  Widget _buildSelectedSystemBubbleWithToolbox() {
    double statusBarHeight = MediaQuery.of(context).padding.top;
    double appBarHeight = _buildAppBar(context).preferredSize.height;

    return ValueListenableBuilder(
        valueListenable: workplaceViewModel.toolboxMode,
        builder: (context, String toolboxMode, Widget? child) {
          FeedWithDetail selectedFeedWithDetail = workplaceViewModel.selectedFeedWithDetail.value!;
          return Positioned(
            width: SizeUtils.width(context, 100),
            top: toolboxMode == 'edit' ? null : workplaceViewModel.selectedBubbleOffset.value - statusBarHeight - appBarHeight,
            bottom: toolboxMode == 'edit' ? (isShowTopicSelector ? 135 + 50 : 135) : null,
            left: 0,
            child: Column(
              children: [
                SystemBubble(
                  key: ValueKey(selectedFeedWithDetail.feedDetail.feedID),
                  feedWithDetail: selectedFeedWithDetail,
                  index: 0,
                  showToolbox: true,
                  showTopic: true,
                ),
                // Container( // 4 July 2023 TMOG request hide this feature temporary
                //   transform: Matrix4.translationValues(-45.0, -35.0, 0.0),
                //   alignment: Alignment.bottomRight,
                //   child: GestureDetector(
                //     onTap: () {
                //       int taskID = int.parse(selectedFeedWithDetail.feedDetail.moduleID!);
                //       goToNextNamedRoute(context, taskCommentScreen, args: taskID, callback: () {
                //         database.feedDao.updateFeed(selectedFeedWithDetail.feedDetail.copyWith(replyReadStatus: 'read'));
                //       });
                //       hideToolbox();
                //     },
                //     child: SvgPicture.asset(ic_toolbox_reply, width: 35),
                //   ),
                // ),
              ],
            ),
          );
        });
  }

  // Build Unread Button
  Widget _buildUnreadButton() {
    return Container(
      padding: EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
      decoration: BoxDecoration(color: darkGrey, border: Border.all(color: darkGrey), borderRadius: BorderRadius.circular(30)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            (workplaceViewModel.lastUnreadIndex.value + 1).toString() + ' New Unread',
            style: boldFontsStyle(color: white, fontSize: 15.0),
            key: ValueKey(workplaceViewModel.lastUnreadIndex.value),
          ),
          SizedBox(width: 10),
          GestureDetector(
            onTap: () {
              // After user click, get latest workplace from database
              database.workplaceDao.getSingleWorkplace(currentWorkplace!.projectID).then((workplace) {
                currentWorkplace = workplace;
              });
              // Hide unread button
              workplaceViewModel.isShowNewUnreads.value = false;
            },
            child: SvgPicture.asset(ic_close, width: 12.0, color: white),
          ),
        ],
      ),
    );
  }

  // Skeleton widget
  Widget _buildSkeleton(BuildContext context) {
    return Container(
      color: white,
      child: ListView(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        children: [
          Column(
            children: [
              Stack(
                children: [
                  Bubble(
                    margin: const BubbleEdges.only(top: 20, bottom: 16, right: 60, left: 50),
                    padding: const BubbleEdges.all(0),
                    radius: const Radius.circular(12),
                    color: shimmerGrey,
                    nip: BubbleNip.leftTop,
                    nipHeight: 12,
                    alignment: Alignment.centerLeft,
                    shadowColor: transparent,
                    elevation: 0,
                    child: const SkeletonContainer.rounded(width: 250, height: 300),
                  ),
                  const Positioned(top: 20, left: 20, child: SkeletonContainer.circular(width: 25, height: 25)),
                ],
              ),
              Stack(
                children: [
                  Bubble(
                    margin: const BubbleEdges.only(top: 20, bottom: 94, left: 60, right: 20),
                    padding: const BubbleEdges.all(0),
                    radius: const Radius.circular(12),
                    nip: BubbleNip.rightBottom,
                    color: shimmerGrey,
                    nipHeight: 12,
                    alignment: Alignment.centerRight,
                    shadowColor: transparent,
                    elevation: 0,
                    child: const SkeletonContainer.rounded(width: 250, height: 188),
                  ),
                ],
              ),
              Container(
                padding: const EdgeInsets.only(top: 16, bottom: 24, left: 20, right: 20),
                decoration: const BoxDecoration(color: darkGrey),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Column(
                            children: [
                              Container(
                                width: 24,
                                height: 24,
                                child: const SkeletonContainer.circular(width: 24, height: 24),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(width: 15.0),
                        Expanded(
                            flex: 15,
                            child: Column(
                              children: [
                                Container(
                                  child: SkeletonContainer.rounded(width: MediaQuery.of(context).size.width * 0.65, height: 51),
                                ),
                              ],
                            )),
                        const SizedBox(width: 15.0),
                        Expanded(
                          flex: 2,
                          child: Column(
                            children: [
                              Container(
                                width: 24,
                                height: 24,
                                child: const SkeletonContainer.circular(width: 24, height: 24),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  // Empty result widget
  Widget _buildEmptyResult(BuildContext context) {
    return LayoutBuilder(builder: (context, size) {
      return ListView(
        children: [
          Container(
            height: size.maxHeight,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Lottie.asset(
                    filterList.length < totalTopics ? lottie_empty_chat_filter : lottie_empty_chat,
                    width: 250.0,
                    height: 250.0,
                    fit: BoxFit.fill,
                  ),
                  Container(
                    width: 250.0,
                    child: Text(
                      filterList.length < totalTopics ? text_empty_chat_filter : text_empty_chat,
                      style: regularFontsStyle(fontSize: 17.0, height: 1.35),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  if (filterList.length < totalTopics) SizedBox(height: 30),
                  if (filterList.length < totalTopics)
                    OutlinedButton(
                      style: OutlinedButton.styleFrom(
                        foregroundColor: black,
                        textStyle: boldFontsStyle(height: 1.32),
                        padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 36.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100.0),
                        ),
                        side: BorderSide(width: 1, color: black),
                      ),
                      onPressed: () {
                        setState(() {
                          filterList.clear();
                        });
                      },
                      child: Text("Reset to Default"),
                    ),
                ],
              ),
            ),
          )
        ],
      );
    });
  }

  // Scroll To Bottom Floating Button
  Widget _scrollToBottomButton() {
    return ValueListenableBuilder(
        valueListenable: workplaceViewModel.isShowScrollBottomButton,
        builder: (context, bool isShowScrollBottomButton, Widget? child) {
          return isShowScrollBottomButton
              ? Container(
                  width: 40.0,
                  height: 40.0,
                  child: RawMaterialButton(
                    fillColor: darkGrey,
                    shape: CircleBorder(),
                    child: SvgPicture.asset(
                      ic_arrow_down,
                      width: 26.0,
                    ),
                    onPressed: () {
                      autoScrollController.jumpTo(0);
                    },
                  ))
              : Container();
        });
  }
}
