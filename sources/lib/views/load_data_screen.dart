import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:sources/constants/app_box_shadow.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/services/data_sync_service.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/view_models/workplace_view_model.dart';
import 'package:wakelock_plus/wakelock_plus.dart';

class LoadDataScreen extends StatefulWidget {
  const LoadDataScreen({Key? key}) : super(key: key);

  @override
  _LoadDataScreenState createState() => _LoadDataScreenState();
}

class _LoadDataScreenState extends State<LoadDataScreen> {
  final workplaceViewModel = locator<WorkplaceViewModel>();
  DataSyncService _dataSyncService = DataSyncService();
  final database = locator<AppDatabase>();
  double precentages = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    WakelockPlus.enable();
    initialize();
  }

  void initialize() async {
    // Initialize view model once getting in workplace module
    await workplaceViewModel.initialize();
    // Step 1: Get all topics
    if (!checkSyncedAPI('topics API')) {
      await _dataSyncService.fetchTopicListAndSaveToDB();
      SpManager.setSyncedAPI('topics API');
    }
    increaseProgress(5.0);
    // Step 2: Get profile list
    if (!checkSyncedAPI('profile API')) {
      await _dataSyncService.getProfileList();
      SpManager.setSyncedAPI('profile API');
    }
    increaseProgress(5.0);
    // Step 3: Get all workplaces
    if (!checkSyncedAPI('workplaces API')) {
      await _dataSyncService.fetchWorkplaceListAndSaveToDB(callback: increaseProgress);
      SpManager.setSyncedAPI('workplaces API');
    }
    increaseProgress(30.0, max: 40.0);
    // Step 4: Get all feeds
    if (!checkSyncedAPI('feeds API')) {
      await database.workplaceDao.getPeriodAgoWorkplace(DateTime.now().subtract(Duration(days: 90)).toString()).then((List<Workplace> workplaceList) async {
        await Future.forEach(workplaceList, (Workplace workplace) async {
          try {
            await _dataSyncService.fetchAndSaveFeedsToDB(projectID: workplace.projectID);
          } catch(e) {
            print(e);
            print('Called Feeds API failed!');
            // await showError(context, 'Called API failed!');
          }

          // increase progress when all feeds data for a workplace's is loaded.
          double progress = (1 / workplaceList.length) * 30;
          increaseProgress(progress);
        });
      });
      SpManager.setSyncedAPI('feeds API');
    }
    increaseProgress(30.0, max: 70.0);
    // Step 5: Get all alerts
    if (!checkSyncedAPI('alerts API')) {
      await _dataSyncService.getAlertList(callback: increaseProgress);
      SpManager.setSyncedAPI('alerts API');
    }
    increaseProgress(30.0, max: 100.0);
    await Future.delayed(Duration(seconds: 1));
    
    // Unregister foreground connection
    await database.close();
    await locator.unregister<AppDatabase>();
    // Register background connection
    await setupBackgroundDB();

    await Future.delayed(Duration(seconds: 1));

    // Redirect to next route once widget built completed
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      WakelockPlus.disable();
      goAndClearAllPageNamed(context, homeScreen);
    });
  }

  void increaseProgress(double value, {double max = 100}) {
    setState(() {
      if (precentages+value <= max)
        precentages += value;
      else
        precentages = max;
    });
  }

  bool checkSyncedAPI(String apiName) {
    List<dynamic> syncedAPIList = SpManager.getSyncedAPI();
    return syncedAPIList.contains(apiName);
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: white,
      body: Container(
        width: double.infinity,
        child: Column(
          children: [
            SafeArea(
              child: Text("")
            ),
            Image.asset(
              img_makeover_service_logo,
              width: MediaQuery.of(context).size.width * 0.39
            ),
            Padding(padding: EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.1)),
            Lottie.asset(
              lottie_load_data,
              width: 300.0,
              height: 300.0,
              fit: BoxFit.fill,
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.7,
              child: Column(
                children: [
                  Text(
                    "Syncing Information...",
                    style: boldFontsStyle(fontSize: 20.0, height: 1.35),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 10),
                  Text(
                    "Please wait a moment while we download information from Mogster Garage",
                    style: regularFontsStyle(fontSize: 17.0, height: 1.35),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 20.0),
                  Container(
                    padding: EdgeInsets.all(16.0),
                    decoration: BoxDecoration(
                      color: whiteGrey,
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      boxShadow: app_box_shadow
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        LinearPercentIndicator(
                          lineHeight: 8.0,
                          animationDuration: 1500,
                          animation: true,
                          animateFromLastPercent: true,
                          percent: precentages / 100,
                          progressColor: yellow,
                          backgroundColor: lightGrey,
                          padding: EdgeInsets.zero,
                        ),
                        SizedBox(height: 8),
                        Text(
                          precentages.round().toString() + '%',
                          style: boldFontsStyle(fontSize:17.0, height: 1.35),
                        )
                      ],
                    ),
                  ),
                ]
              )
            ),
          ]
        )
      ),
    );
  }
}