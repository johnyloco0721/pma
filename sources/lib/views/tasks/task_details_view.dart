import 'package:cron/cron.dart';
import 'package:flutter/material.dart';

import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lottie/lottie.dart';

import 'package:sources/args/push_update_info_args.dart';
import 'package:sources/args/task_args.dart';
import 'package:sources/args/task_form_args.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/models/profile_model.dart';
import 'package:sources/models/raise_invoice_model.dart';
import 'package:sources/models/task_details_model.dart';
import 'package:sources/services/task_services.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/widgets/comment_box_widget.dart';
import 'package:sources/widgets/completed_info_widget.dart';
import 'package:sources/widgets/content_widget.dart';
import 'package:sources/widgets/html_widget/html_stylist.dart';
import 'package:sources/widgets/mark_as_complete_widget.dart';

class TaskDetailView extends StatefulWidget {
  final TaskArguments taskArguments;
  const TaskDetailView({Key? key, required this.taskArguments}) : super(key: key);

  @override
  _TaskDetailsViewState createState() => _TaskDetailsViewState();
}

class _TaskDetailsViewState extends State<TaskDetailView> {
  TaskService _taskService = TaskService();
  late bool isLoading;
  late bool isCron;
  late bool hasSubtask;
  TaskDetailsModel? taskInfo;
  List<SubtaskDetailsModel>? subtasks;
  bool autoUpdateCA = false;
  bool imageUpdateCA = false;
  ProfileDetails loggedUserInfo = SpManager.getUserdata();
  TaskDetailsResponseModel? taskDetailsResponseModel;
  final TextEditingController _remarksController = TextEditingController();
  Cron? cron;
  int? projectID;
  int onlyClickOnce = 0;

  bool redirected = false;

  @override
  void initState() {
    super.initState();
    fetchTaskDetials();

    cron = Cron()
      ..schedule(Schedule.parse('*/5 * * * * *'), () async {
          await _taskService.fetchMyTaskDetails(widget.taskArguments.taskID ?? 0).then((response) {
            setState(() {
              taskDetailsResponseModel = response;
              taskInfo = taskDetailsResponseModel?.taskArr;
              subtasks = taskDetailsResponseModel?.subTaskArr;
              autoUpdateCA = taskDetailsResponseModel?.clientAppRequirements?.autoUpdateCA == 1;
              imageUpdateCA = taskDetailsResponseModel?.clientAppRequirements?.imageUpdateCA == 1;
            });
          });
      });
  }

  fetchTaskDetials() async {
    setState(() {
      isLoading = true;
    });
    await _taskService.fetchMyTaskDetails(widget.taskArguments.taskID ?? 0).then((response) {
      taskDetailsResponseModel = response;
      taskInfo = taskDetailsResponseModel?.taskArr;
      subtasks = taskDetailsResponseModel?.subTaskArr;
      autoUpdateCA = taskDetailsResponseModel?.clientAppRequirements?.autoUpdateCA == 1;
      imageUpdateCA = taskDetailsResponseModel?.clientAppRequirements?.imageUpdateCA == 1;
    });
    setState(() {
      isLoading = false;
    });

    // Redirect to next route once widget built completed
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (widget.taskArguments.moduleName != null && widget.taskArguments.moduleName == "task-comment" && !redirected) {
        redirected = true;
        goToNextNamedRoute(context, taskCommentScreen, args: widget.taskArguments.taskID);
      }
    });
  }

  loading() {
    if (mounted) {
      setState(() {
        isLoading = true;
      });
    }
    Future.delayed(Duration(seconds: 5), () {
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    });
  }

  void setLoadingState(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }

  void stopLoading() {
    setLoadingState(false);
  }

  @override
  void dispose() {
    super.dispose();
    cron?.close();
  }

  @override
  Widget build(BuildContext context) {
    hasSubtask = (subtasks?.length ?? 0) > 0;
    taskInfo = taskDetailsResponseModel?.taskArr;
    subtasks = taskDetailsResponseModel?.subTaskArr;
    // taskDetailsResponseModel?.permissions?.canAccessEditorMode = false;
    return Scaffold(
      key: PageStorageKey<String>('TaskDetail'),
      backgroundColor: whiteGrey,
      appBar: _buildAppBar(context),
      body: isLoading
        ? Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Lottie.asset(
                lottie_loading_spinner,
                width: 50.0,
                height: 50.0,
                fit: BoxFit.fill,
              ),
              SizedBox(height: 16.0),
              Text(
                'just a few secs...',
                style: regularFontsStyle(fontSize: 14.0, color: grey),
              ),
            ],
          ),
        ) : RefreshIndicator(
          color: yellow,
          backgroundColor: white,
          onRefresh: () async {
            // call fetchTaskDetials function again to refresh page
            fetchTaskDetials();
          },
          child: SingleChildScrollView(
            child: Container(
                child: _buildTaskDetailView(context)),
          ),
        ),
        floatingActionButton: (taskDetailsResponseModel?.permissions?.canAccessEditorMode == true) ?  FloatingActionButton(
          onPressed: () async {
            // go to editor mode view route (remember add route in home_view.dart)
            // ignore: unused_local_variable
            final dynamic _response = await goToNextNamedRoute(
                context,
                taskFormScreen,
                args: TaskFormArguments(
                  teamMemberList: taskDetailsResponseModel?.assigneeList,
                  milestoneList: taskDetailsResponseModel?.milestoneList,
                  projectID: taskDetailsResponseModel!.taskArr!.projectID!,
                  taskDetail: taskDetailsResponseModel?.taskArr,
                  milestonePagingController: widget.taskArguments.milestonePagingController
                ), callback: () {
                  fetchTaskDetials;
                }
              );
          },
          shape: CircleBorder(),
          backgroundColor: yellow,
          child: SvgPicture.asset(ic_edit),
        ) : Container(),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.light,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: SvgPicture.asset(ic_chevron_left, width: 10.0),
      ),
      title: !isLoading
        ? Text(
            loggedUserInfo.teamMemberID == taskInfo!.assignTo ? 'My Task' : 'Tasks For ${taskInfo?.propertyName}',
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, color: white, letterSpacing: 0.8),
          )
        : Container(),
      centerTitle: false,
      backgroundColor: black,
    );
  }

  // task detail view
  //edit update info same with the grey box details by choonxiang
  Widget _buildTaskDetailView(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(24.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // task details layout
          taskDetailsLayout(),
          // leave comment button, (outline layout)
          (hasSubtask)
            ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CommentOutlineButton(
                  taskID: widget.taskArguments.taskID ?? 0,
                  taskInfo: taskInfo,
                  callback: () {
                    fetchTaskDetials();
                  }),
                  SizedBox(height: 20.0),
                  if (taskDetailsResponseModel?.permissions?.canRaiseInvoice == true && taskInfo?.isRaiseInvoiceButtonEditable == true)
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: yellow,
                      padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                      minimumSize: Size(230.0, 40)
                    ),
                    onPressed: () async {
                      var notifyFinance = await showDecisionWithFields(
                        context, 
                        'Are you sure you want to notify Finance Team to issue invoice for: ${taskInfo?.paymentTaskName}', 
                        'Cancel', 
                        'Notify Finance',
                        _remarksController,
                        buttonColor: yellow,
                        textColor: black,
                      );

                      if (notifyFinance) {
                        setState(() {
                          onlyClickOnce = onlyClickOnce + 1;
                        });
                        if (onlyClickOnce == 1) {
                          try {
                            var response = await _taskService.postRaiseInvoice(
                              RaiseInvoiceModel(
                                projectID: taskInfo?.projectID,
                                taskID: taskInfo?.taskID,
                                remarks: _remarksController.text,
                              ),
                            );
                            if (response["status"] == true) {
                              // If successful, show the success dialog
                              showDialog(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context) {
                                  final dialogContext = context;
                                  Future.delayed(Duration(seconds: 2), () {
                                    Navigator.of(dialogContext).pop();
                                  });
                                  return AlertDialog(
                                    backgroundColor: white,
                                    title: SvgPicture.asset(ic_completed, width: 45),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(12),
                                    ),
                                    content: Text(
                                      "Success! \n We've notified Finance Team!",
                                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: black),
                                      textAlign: TextAlign.center,
                                    ),
                                  );
                                },
                              );

                              // Fetch task details after 2 seconds only when success
                              Future.delayed(Duration(seconds: 2), () {
                                fetchTaskDetials();
                              });
                            } else {
                              showError(context, response["message"] ?? "Unexpected error, please try again.");
                              return;
                            }

                          } catch (error) {
                            showError(context, 'Unexpected error, please try again.');
                          } finally {
                            setState(() {
                              onlyClickOnce = 0;
                            });
                          }
                        }
                      }
                    },
                    child: Text(
                      "Raise Invoice",
                      style: boldFontsStyle(fontSize: 16.0, height: 1.5),
                    ),
                  ),
                SizedBox(height: 24.0),
                Divider(color: grey),
                SizedBox(height: 16.0),
                Text(
                  'Sub Tasks',
                  style: boldFontsStyle(fontSize: 14.0, color: grey),
                ),
              ],
            ) : CommentOutlineButton(
              taskID: widget.taskArguments.taskID ?? 0,
              taskInfo: taskInfo,
              callback: () {
                fetchTaskDetials();
              }
            ),
          SizedBox(height: 24.0),
          // mark as completed Form or update info layout (no subtask form)
          if (!hasSubtask)
            taskInfo?.taskStatus != 'completed' && taskInfo?.taskStatus != 'flagged'
              ? !(taskInfo?.hideCompletedButton ?? false)
                ? MarkAsCompleteForm(
                    isFileRequire: taskInfo?.isFileRequired == 1,
                    isSubtaskForm: hasSubtask,
                    taskInfo: taskInfo!,
                    autoUpdateCA: autoUpdateCA,
                    imageUpdateCA: imageUpdateCA,
                    callback: () {
                      fetchTaskDetials();
                      goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: taskInfo?.taskID));
                    },
                    onMarkCompleted: (completedTask) {
                      setState(() {
                        taskInfo = completedTask;
                      });
                    })
                : Container()
              : CompletedInfo(
                taskInfo: taskInfo,
                autoUpdateCA: autoUpdateCA,
                isloadingTrue: () {
                  loading();
                },
                setLoadingState: setLoadingState,
                stopLoading: stopLoading,
                onManualPush: () => onManualPush(
                  context,
                  PushUpdateInfoArguments(
                    taskID: taskInfo?.taskID,
                    action: "push",
                    remark: taskInfo?.taskUpdates,
                    imageUpdateCA: imageUpdateCA,
                    updatesImages: taskInfo?.forInternal?.updatesImages,
                    updatesFiles: taskInfo?.forInternal?.updatesFiles,
                    isFileRequired: taskInfo?.isFileRequired == 1,
                    specialTaskType: taskInfo?.specialTaskType,
                    linkValue: taskInfo?.forInternal?.dropboxLink
                  )),
                callback: () {
                  fetchTaskDetials();
                  goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: taskInfo?.taskID));
                }),
          // mark as completed Form or update info layout (subtask form)
          if (hasSubtask)
            for (int i = 0; i < (subtasks?.length ?? 0); i++)
              subtasks?[i].taskStatus != 'completed' && subtasks?[i].taskStatus != 'flagged'
                ? MarkAsCompleteForm(
                    key: ValueKey(subtasks?[i].taskID),
                    isFileRequire: subtasks?[i].isFileRequired == 1,
                    isSubtaskForm: hasSubtask,
                    taskInfo: taskInfo!,
                    subtaskInfo: subtasks?[i],
                    autoUpdateCA: autoUpdateCA,
                    imageUpdateCA: imageUpdateCA,
                    callback: () {
                      fetchTaskDetials();
                      goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: taskInfo?.taskID));
                    },
                    onMarkCompleted: (completedSubtask) {
                      setState(() {
                        subtasks?[i] = completedSubtask;
                      });
                    })
                : 
                CompletedInfo(
                      taskInfo: taskInfo!,
                      subtaskInfo: subtasks?[i],
                      isloadingTrue: () {
                        loading();
                      },
                      setLoadingState: setLoadingState,
                      stopLoading: stopLoading,
                      isSubtask: true,
                      autoUpdateCA: autoUpdateCA,
                      onManualPush: () => onManualPush(
                        context,
                        PushUpdateInfoArguments(
                          taskID: subtasks?[i].taskID,
                          action: "push",
                          remark: subtasks?[i].forInternal?.taskUpdates,
                          imageUpdateCA: imageUpdateCA,
                          updatesImages: subtasks?[i].forInternal?.updatesImages,
                          updatesFiles: subtasks?[i].forInternal?.updatesFiles,
                          isFileRequired: taskInfo?.isFileRequired == 1,
                          specialTaskType: taskInfo?.specialTaskType,
                          linkValue: taskInfo?.forInternal?.dropboxLink,
                          taskRelatedDate: subtasks?[i].taskRelatedDate
                        )),
                      callback: () {
                        fetchTaskDetials();
                        goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: taskInfo?.taskID));
                      }),
        ],
      ),
    );
  }

  // task details layout
  Widget taskDetailsLayout() => Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      // task's due date, (outline layout)
      if (!hasSubtask)
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
            decoration: BoxDecoration(
                color: taskInfo?.taskStatus == 'completed'
                    ? green
                    : taskInfo?.taskStatus == 'flagged'
                        ? lightPink
                        : transparent,
                border: Border.all(
                    color: taskInfo?.taskStatus == 'completed'
                        ? green
                        : taskInfo?.taskStatus == 'flagged'
                            ? lightPink
                            : taskInfo?.taskEndDateTime != null ? lightGrey : red),
                borderRadius: BorderRadius.circular(500.0)),
            child: Text(
              taskInfo?.taskStatus == 'completed'
                  ? 'Completed'
                  : taskInfo?.taskStatus == 'flagged'
                      ? 'Flagged'
                      : taskInfo?.taskEndDateTime != null 
                        ? formatDatetime(datetime: taskInfo?.taskEndDateTime ?? '') 
                        : 'No Due Date',
              style: (taskInfo?.taskStatus == 'completed' || taskInfo?.taskStatus == 'flagged' || taskInfo?.taskEndDateTime != null ) ? boldFontsStyle(
                  fontSize: 14.0,
                  color: taskInfo?.taskStatus == 'completed'
                      ? white
                      : taskInfo?.taskStatus == 'flagged'
                          ? red
                          : grey) : italicFontsStyle(fontSize: 14.0, color: red),
            ),
          ),
          if (taskInfo?.taskStatus == 'completed') SvgPicture.asset(ic_task_complete, width: 26.0),
          if (taskInfo?.taskStatus == 'flagged') SvgPicture.asset(ic_task_flag, width: 26.0),
        ],
      ),
      SizedBox(height: 4.0),
      // task Disclaimer
      if (taskInfo?.taskDisclaimer != null && taskInfo?.taskDisclaimer != '')
        Container(
          width:double.infinity,
          margin: EdgeInsets.only(top: 8.0),
          padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 8.0),
          decoration: BoxDecoration(
            color: (taskInfo?.taskDisclaimerBgColor ?? '') != '' ? hexToColor(taskInfo?.taskDisclaimerBgColor ?? '') : lightorange,
            border: Border.all(color: (taskInfo?.taskDisclaimerBorderColor ?? '') != '' ? hexToColor(taskInfo?.taskDisclaimerBorderColor ?? '') : darkYellow),
            borderRadius: BorderRadius.circular(6.0)
          ),
          child: ContentWidget(
            content: taskInfo?.taskDisclaimer ?? "",
            linkColor: linkYellow,
          ),
        ),
      SizedBox(height: 16.0),
      // task's title
      RichText(
        text: TextSpan(
          children: [
            if (taskInfo?.isSpecial ?? false)
              WidgetSpan(
                child: Padding(
                  padding: EdgeInsets.only(right: 8.0),
                  child: SvgPicture.asset(ic_star, width: 20.0),
                ),
              ),
            TextSpan(
              text: taskInfo?.paymentTaskName ?? '',
              style: boldFontsStyle(fontSize: 20.0, height: 1.4),
            ),
          ]
        ),
      ),
      // task's description
      if (taskInfo?.taskDescription != null && taskInfo?.taskDescription != '')
        Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: Text(
            taskInfo?.taskDescription ?? '',
            style: regularFontsStyle(fontSize: 16.0, height: 1.5),
          ),
        ),
      SizedBox(height: 16.0),
      // task's project labels
      Container(
        margin: EdgeInsets.only(bottom: 8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // project icon
            SvgPicture.asset(ic_home, width: 18.0),
            SizedBox(width: 8.0),
            // project label
            Expanded(
              child: Text(
                taskInfo?.propertyName ?? '',
                style: regularFontsStyle(fontSize: 14.0, color: grey),
              ),
            ),
          ],
        ),
      ),
      // task's assigner labels
      Container(
        margin: EdgeInsets.only(bottom: 8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // assigner icon
            SvgPicture.asset(ic_stage, width: 18.0, color: darkYellow,),
            SizedBox(width: 8.0),
            // stage label
            Container(
              width: 75.0,
              child: Text(
                'Stage:',
                style: regularFontsStyle(fontSize: 14.0, color: grey),
              ),
            ),
            // stage value
            Text(
              taskInfo?.stage?.label ?? '',
              style: regularFontsStyle(fontSize: 14.0, color: black),
            ),
          ],
        ),
      ),
      // milestone label
      Container(
        margin: EdgeInsets.only(bottom: 8.0),
        child: Row(crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // milestone icon
            SvgPicture.asset(ic_milestone, width: 18.0),
            SizedBox(width: 8.0),
            // milestone label
            Container(
              width: 75.0,
              child: Text(
                'Milestone:',
                style: regularFontsStyle(fontSize: 14.0, color: grey),
              ),
            ),
            // milestone value
            Text(
              taskInfo?.milestoneCategoryName ?? '',
              style: regularFontsStyle(fontSize: 14.0, color: black),
            ),
          ],
        ),
      ),
      if(taskInfo?.teamMemberName != null && taskInfo!.teamMemberName != '')
      // task's assigner labels
      Container(
        margin: EdgeInsets.only(bottom: 8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // assigner icon
            SvgPicture.asset(ic_people, width: 18.0),
            SizedBox(width: 8.0),
            // assigner label
            Text(
              'Assigned by ',
              style: regularFontsStyle(fontSize: 14.0, color: grey),
            ),
            Expanded(
              child: GestureDetector(
                onTap: () {
                  showContactDialog(context, contactName: taskInfo?.teamMemberName ?? '', number: taskInfo?.teamMemberContactNo ?? '');
                },
                child: Container(
                  child: Text(
                    taskInfo?.teamMemberName ?? '',
                    style: boldFontsStyle(fontSize: 14.0, color: darkYellow),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      if(taskInfo?.assignToName != null && taskInfo!.assignToName != '')
      // task's assignTo labels
      Container(
       margin: EdgeInsets.only(bottom: 8.0),
       child: Row(
         crossAxisAlignment: CrossAxisAlignment.start,
         children: [
           // assigner icon
           SvgPicture.asset(ic_assign, width: 18.0),
           SizedBox(width: 8.0),
           // assigner label
           Text(
             'Assigned to ',
             style: regularFontsStyle(fontSize: 14.0, color: grey),
           ),
           Expanded(
             child: GestureDetector(
               onTap: () {
                 showContactDialog(context, contactName: taskInfo?.assignToName ?? '', number: taskInfo?.assignToContactNumber ?? '');
               },
               child: Container(
                 child: Text(
                   taskInfo?.assignToName ?? '',
                   style: boldFontsStyle(fontSize: 14.0, color: darkYellow),
                 ),
               ),
             ),
           ),
         ],
       ),
      ),
      // task's file require labels
      if (taskInfo?.isFileRequiredLabel != '')
        Container(
          margin: EdgeInsets.only(bottom: 8.0),
          child: Row(
            children: [
              // attach icon
              SvgPicture.asset(ic_attach, width: 18.0),
              SizedBox(width: 8.0),
              // attach label
              RichText(
                  text: HTML.toTextSpan(
                    context,
                    taskDetailsResponseModel?.taskArr?.isFileRequiredLabel ?? "",
                    defaultTextStyle: regularFontsStyle(fontSize: 14.0, color: grey),
                  ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),

            ],
          ),
        ),
      // task's subtask labels
      if (hasSubtask)
        Container(
          margin: EdgeInsets.only(bottom: 8.0),
          child: Row(
            children: [
              // attach icon
              SvgPicture.asset(ic_career, width: 18.0),
              SizedBox(width: 8.0),
              // attach label
              Text(
                '${subtasks?.where((e) => e.taskStatus == 'completed').length} / ${subtasks?.length} Sub Tasks',
                style: regularFontsStyle(fontSize: 14.0, color: grey),
              ),
            ],
          ),
        ),
      SizedBox(height: 8.0),
    ],
  );
}
