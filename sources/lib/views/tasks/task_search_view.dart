import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:lottie/lottie.dart';
import 'package:sources/args/task_args.dart';
import 'package:sources/args/task_search_args.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_constants.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/models/task_item_model.dart';
import 'package:sources/models/task_list_model.dart';
import 'package:sources/models/task_project_item_model.dart';
import 'package:sources/services/task_services.dart';
import 'package:sources/utils/debouncer_utlis.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:substring_highlight/substring_highlight.dart';

class TaskSearchView extends StatefulWidget {
  final TaskSearchArguments taskSearchArgs;
  const TaskSearchView({Key? key, required this.taskSearchArgs}) : super(key: key);

  @override
  _TaskSearchViewState createState() => _TaskSearchViewState();
}

class _TaskSearchViewState extends State<TaskSearchView> {
  TaskService _taskService = TaskService();
  TextEditingController searchTextFieldController = TextEditingController();
  ScrollController scrollController = ScrollController();
  PagingController pagingController = PagingController<int, dynamic>(firstPageKey: 1);
  Debouncer _debouncer = Debouncer(milliseconds: 500);

  String searchKeyword = '';
  String filterDate = '';

  @override
  void initState() {
    super.initState();
    filterDate = widget.taskSearchArgs.filterDatetime ?? '';
    if (widget.taskSearchArgs.fromIndex == 0) {
      pagingController.addPageRequestListener((pageKey) {
        _fetchMyTaskProjectPage(pageKey);
      });
    } else {
      pagingController.addPageRequestListener((pageKey) {
        _fetchMyTaskPage(pageKey);
      });
    }
  }

  Future<void> _fetchMyTaskProjectPage(int pageKey) async {
    try {
      await _taskService.fetchMyTaskProjectList(pageKey: pageKey, keyword: searchKeyword,date:filterDate).then((response) {
        TaskListModel taskProjectListResponse = response;
        List<TaskProjectItemModel>? projectList = taskProjectListResponse.response?.map((e) => TaskProjectItemModel.fromJson(e)).toList();
        final isLastPage = (projectList?.length ?? 0)  < (taskProjectListResponse.limitPerPage ?? 50);
        if (isLastPage) {
          pagingController.appendLastPage(projectList ?? []);
        } else {
          final nextPageKey = pageKey + 1;
          pagingController.appendPage(projectList ?? [], nextPageKey);
        }
      });
    } catch (error) {
      pagingController.error = error;
    }
  }

  Future<void> _fetchMyTaskPage(int pageKey) async {
    try {
      await _taskService.fetchMyTaskList(pageKey: pageKey, keyword: searchKeyword, date: filterDate).then((response) {
        TaskListModel taskListResponse = response;
        List<TaskItemModel>? taskList = taskListResponse.response?.map((e) => TaskItemModel.fromJson(e)).toList();
        final isLastPage = (taskList?.length ?? 0)  < (taskListResponse.limitPerPage ?? 50);
        if (isLastPage) {
          pagingController.appendLastPage(taskList ?? []);
        } else {
          final nextPageKey = pageKey + 1;
          pagingController.appendPage(taskList ?? [], nextPageKey);
        }
      });
    } catch (error) {
      pagingController.error = error;
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: PageStorageKey<String>('TaskSearch'),
      backgroundColor: whiteGrey,
      appBar: _buildAppBar(context),
      body: RefreshIndicator(
        color: yellow,
        backgroundColor: white,
        onRefresh: () async {
          // call fetchTaskDetials function again to refresh page
          setState(() {
            pagingController.refresh();
          });
        },
        child: _buildTaskSearchView(context),
      ),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.light,
      toolbarHeight: 120.0,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: SvgPicture.asset(ic_chevron_left, width: 10.0),
        // padding: EdgeInsets.only(bottom: 65.0),
      ),
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          searchInputLayout(),
          if (filterDate != '')
            filterDateLayout(),
        ]
      ),
      centerTitle: false,
      backgroundColor: black,
    );
  }

  Widget searchInputLayout() => TextField(
    autofocus: true,
    controller: searchTextFieldController,
    onChanged: (text) {
      // Use debouncer to do api search once user stop typing for 500ms
      _debouncer.run(() {
        setState(() {
          searchKeyword = text;
          pagingController.refresh();
        });
      });
    },
    onEditingComplete: () {},
    style: regularFontsStyle(height: 1.32),
    decoration: InputDecoration(
      suffixIconConstraints: BoxConstraints(minHeight: 10, minWidth: 10),
      hintText: 'Search',
      hintStyle: italicFontsStyle(color: grey, height: 1.32),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: yellow, width: 2.0),
        borderRadius: BorderRadius.circular(12),
      ),
      fillColor: white,
      filled: true,
      contentPadding: EdgeInsets.only(top: 10, bottom: 10, left: 24, right: 24),
      suffixIcon: Padding(
        padding: EdgeInsets.only(right: 25.0),
        child: GestureDetector(
          onTap: () {
            searchKeyword = searchTextFieldController.text;
            pagingController.refresh();
          },
          child: Container(
            child: SvgPicture.asset(ic_search, color: black),
          ),
        ),
      ),
    ),
  );

  Widget filterDateLayout() => Container(
    height: 30.0,
    padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 6.0),
    margin: EdgeInsets.only(bottom: 24.0, top: 16.0),
    decoration: BoxDecoration(
      border: Border.all(color: white),
      borderRadius: BorderRadius.all(Radius.circular(6.0)),
    ),
    child: Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          'Filter:',
          style: regularFontsStyle(fontSize: 14.0, color: white),
        ),
        SizedBox(width: 8.0),
        Text(
          formatDatetime(datetime: filterDate),
          style: boldFontsStyle(fontSize: 14.0, color: white),
        ),
        SizedBox(width: 16.0),
        InkWell(
          onTap: () {
            setState(() {
              filterDate = '';
              pagingController.refresh();
            });
          },
          child: SvgPicture.asset(ic_close, width: 18.0, color: white)
        ),
      ],
    ),
  );

  Widget _buildTaskSearchView(BuildContext context) {
    return PagedListView.separated(
      physics: AlwaysScrollableScrollPhysics(),
      scrollController: scrollController,
      pagingController: pagingController,
      builderDelegate: PagedChildBuilderDelegate<dynamic>(
        firstPageProgressIndicatorBuilder: (context) => Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Lottie.asset(
              lottie_loading_spinner,
              width: 50.0,
              height: 50.0,
              fit: BoxFit.fill,
            ),
            SizedBox(height: 16.0),
            Text(
              'just a few secs...',
              style: regularFontsStyle(fontSize: 14.0, color: grey),
            ),
          ],
        ),
        newPageProgressIndicatorBuilder: (context) => Container(
          padding: EdgeInsets.all(8.0),
          child: Center(
            child: Lottie.asset(
              lottie_loading_spinner,
              width: 50.0,
              height: 50.0,
              fit: BoxFit.fill,
            ),
          ),
        ),
        noMoreItemsIndicatorBuilder: (context) => Container(
          padding: EdgeInsets.all(8.0),
          child: Center(
            child: Text(
              'You’ve reach the end of the list.',
              style: regularFontsStyle(fontSize: 12.0, height: 1.33, color: grey),
            )
          ),
        ),
        noItemsFoundIndicatorBuilder: (context) => Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              child: Lottie.asset(
                lottie_empty_search,
                width: 250.0,
                height: 250.0,
                fit: BoxFit.fill,
              ),
            ),
            Container(
              width: 250.0,
              child: Text(
                text_empty_search,
                style: regularFontsStyle(fontSize: 17.0, height: 1.35), 
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
        itemBuilder: (context, project, index) => widget.taskSearchArgs.fromIndex == 0 ? InkWell(
          onTap: () => goToNextNamedRoute(context, taskMilestoneScreen, args: TaskArguments(projectID: project.projectID ?? 0)),
          child: Container(
            padding: EdgeInsets.all(24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SubstringHighlight(
                  text: project.propertyName ?? '',
                  term: searchKeyword,
                  textStyle: regularFontsStyle(fontSize: 16.0, height: 1.5),
                  textStyleHighlight: boldFontsStyle(fontSize: 16.0, color: darkYellow, height: 1.5),
                ),
                SizedBox(height: 4.0),
                Row(
                  children: [
                    SvgPicture.asset(ic_people, width: 18.0, color: grey),
                    SizedBox(width: 8.0),
                    SubstringHighlight(
                      text: project.ownerName ?? '',
                      term: searchKeyword,
                      textStyle: regularFontsStyle(fontSize: 14.0),
                      textStyleHighlight: boldFontsStyle(fontSize: 14.0, color: darkYellow),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ) : InkWell(
          onTap: () => goToNextNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: project.taskID)),
          child: Container(
            padding: EdgeInsets.all(24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SubstringHighlight(
                  text: project.taskName ?? '',
                  term: searchKeyword,
                  textStyle: regularFontsStyle(fontSize: 16.0, height: 1.5),
                  textStyleHighlight: boldFontsStyle(fontSize: 16.0, color: darkYellow, height: 1.5),
                ),
                SizedBox(height: 4.0),
                SubstringHighlight(
                  text: project.propertyName ?? '',
                  term: searchKeyword,
                  textStyle: regularFontsStyle(fontSize: 14.0),
                  textStyleHighlight: boldFontsStyle(fontSize: 14.0, color: darkYellow),
                ),
              ],
            ),
          ),
        ),
      ),
      separatorBuilder: (context, index) => Divider(height: 0.5, color: lightGrey),
    );
  }
}