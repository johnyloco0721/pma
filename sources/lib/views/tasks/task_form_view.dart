import 'package:deleteable_tile/deleteable_tile.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:getwidget/components/checkbox/gf_checkbox.dart';
import 'package:getwidget/types/gf_checkbox_type.dart';
import 'package:intl/intl.dart';

import 'package:lottie/lottie.dart';
import 'package:sources/args/task_args.dart';

import 'package:sources/args/task_form_args.dart';
import 'package:sources/constants/app_box_shadow.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/models/special_task_model.dart';
import 'package:sources/models/task_details_model.dart';
import 'package:sources/models/task_milestone_model.dart';
import 'package:sources/models/task_post_model.dart';
import 'package:sources/models/task_requirements.dart';
import 'package:sources/models/team_member_model.dart';
import 'package:sources/services/task_services.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:substring_highlight/substring_highlight.dart';

class TaskFormView extends StatefulWidget {
  final TaskFormArguments? taskEditArgs;
  const TaskFormView({Key? key, this.taskEditArgs}) : super(key: key);

  @override
  _TaskFormViewState createState() => _TaskFormViewState();
}

class _TaskFormViewState extends State<TaskFormView> {
  TaskService _taskService = TaskService();
  final _formKey = GlobalKey<FormState>();
  final _milestoneKey = GlobalKey();
  final _mainTitleKey = GlobalKey();
  final _assignToKey = GlobalKey();
  final TextEditingController _taskController = TextEditingController();
  final TextEditingController _taskDescController = TextEditingController();
  final TextEditingController _assignController = TextEditingController();
  DateFormat formatter = DateFormat('d MMM yyyy');

  TaskMilestoneOption? milestoneValue;
  TaskDetailsResponseModel? taskDetailsResponseModel;
  TaskDetailsModel? taskInfo;
  List<SubTasksPostModel> subTaskList = [];
  List<SubTasksPostModel> oldSubTaskList = [];
  List<TeamMemberDetails> teamMember = [];
  List<SpecialTaskDetails> specialTaskList = [];
  List<SubtaskDetailsModel>? subTaskInfo = [];
  List<int?>? _fileRequiredCheckedList = [];
  List<bool> fileRequiredBoolList = [];
  List<int?>? _autoUpdateCACheckedList = [];
  List<bool> autoUpdateCABoolList = [];
  List<int?>? _imageUpdateCheckedList = [];
  List<bool> imageUpdateBoolList = [];
  List<int?>? isCompletedList = [];
  List<int?>? isFlaggedList = [];
  List? taskPercentage = [];
  bool? canDeleteTask;
  List<String>? paymentPercentageArr = [];
  List<bool>? isDuedateEmpty = [];

  bool _isLoading = false;
  bool _isSelectedUploadFiles = true;
  bool _isSelectedShownInApp = true;
  bool _isSelectedIncludeFilesAttached = true;
  bool isDelay = false;
  int completedSubtaskLength = 0;
  int? subtaskLength;
  bool? duedateApplyToAll = false;
  var sameDueDate;
  bool? requirementApplyToAll = false;
  bool? sameRequirement;
  bool? clientSettingApplyToAll = false;
  bool? sameautoUpdateCA;
  bool? sameimageUpdate;
  bool? isDueDateClear = false;
  int onlyClickOnce = 0;

  String? earlierDateTime;
  String? latestDateTime;
  var currentMessage;
  int show = 0;
  var currentUser;
  DateTime? currentUserEditingTime;
  int? fetchSubtaskLength;
  TaskPostPreviousModel? oldFormData;

  @override
  void initState() {
    super.initState();
    fetchTaskDetials();
    getSpecialTaskList();
    getTeamMemberList();
  }

  fetchTaskDetials() async {
    _isLoading = true;
    if (widget.taskEditArgs?.taskDetail != null) {
      await _taskService.fetchMyTaskDetails(widget.taskEditArgs!.taskDetail!.taskID!).then((response) {
        taskDetailsResponseModel = response;
        setState(() {
          _isSelectedUploadFiles = taskDetailsResponseModel?.requirements?.fileRequired == 1;
          _isSelectedShownInApp = taskDetailsResponseModel?.clientAppRequirements?.autoUpdateCA == 1;
          _isSelectedIncludeFilesAttached = taskDetailsResponseModel?.clientAppRequirements?.imageUpdateCA == 1;
          taskInfo = taskDetailsResponseModel?.taskArr;
          subTaskInfo = taskDetailsResponseModel?.subTaskArr;
          canDeleteTask = taskDetailsResponseModel?.permissions?.canDeleteTask;
          paymentPercentageArr = taskDetailsResponseModel?.paymentPercentageArr;
          if (isNullDatetime(taskInfo?.taskTargetDateTime))
            taskInfo?.taskTargetDateTime = null;
          if (isNullDatetime(taskInfo?.taskEndDateTime))
            taskInfo?.taskEndDateTime = null;
          subTaskList = taskDetailsResponseModel?.subTaskArr
                  ?.map((e) => SubTasksPostModel(
                        taskID: e.taskID ?? 0,
                        title: e.taskName,
                        templateTaskID: e.templateTaskID ?? 0,
                        description: e.taskDescription,
                        targetDate: e.taskTargetDateTime,
                        dueDate: e.taskEndDateTime,
                        requirements: e.requirements,
                        clientAppRequirements: e.clientAppRequirements
                      ))
                  .toList() ??
              [];
          if (taskInfo?.milestoneCategoryID != null)
            milestoneValue = widget.taskEditArgs?.milestoneList!.firstWhere((e) => e.milestoneCategoryID == taskInfo?.milestoneCategoryID);
          _taskController.text = taskInfo?.taskName ?? '';
          _taskDescController.text = taskInfo?.taskDescription ?? '';
          _assignController.text = taskInfo?.assignToName ?? '';
          completedSubtaskLength = taskDetailsResponseModel?.subTaskArr?.where((subtask) => subtask.taskStatus == "completed").length ?? 0;
        });
        // for the checkbox list on subtask
        subtaskLength = subTaskList.length;
        if (subtaskLength != 0) {
          for (int index = 0; index < subTaskList.length; index++) {
            _fileRequiredCheckedList = taskDetailsResponseModel?.subTaskArr?.map((e) => e.requirements?.fileRequired).toList();
            fileRequiredBoolList = _fileRequiredCheckedList!.map((item) => item == 1).toList();
            _autoUpdateCACheckedList = taskDetailsResponseModel?.subTaskArr?.map((e) => e.clientAppRequirements?.autoUpdateCA).toList();
            autoUpdateCABoolList = _autoUpdateCACheckedList!.map((item) => item == 1).toList();
            _imageUpdateCheckedList= taskDetailsResponseModel?.subTaskArr?.map((e) => e.clientAppRequirements?.imageUpdateCA).toList();
            imageUpdateBoolList = _imageUpdateCheckedList!.map((item) => item == 1).toList();
            isCompletedList = taskDetailsResponseModel?.subTaskArr?.map((e) => e.isCompleted).toList();
            isFlaggedList = taskDetailsResponseModel?.subTaskArr?.map((e) => e.isFlag).toList();
            isDuedateEmpty?.insert(index, false);
          }
        }
        // store prevoius form data 
        oldSubTaskList= taskDetailsResponseModel?.subTaskArr
          ?.map((e) => SubTasksPostModel(
            taskID: e.taskID ?? 0,
            title: e.taskName,
            templateTaskID: e.templateTaskID ?? 0,
            description: e.taskDescription,
            targetDate: e.taskTargetDateTime,
            dueDate: e.taskEndDateTime,
            requirements: Requirements (fileRequired: e.requirements?.fileRequired),
            clientAppRequirements: ClientAppRequirements(
              autoUpdateCA: e.clientAppRequirements?.autoUpdateCA,
              imageUpdateCA: e.clientAppRequirements?.imageUpdateCA,
            ),
          ))
        .toList() ??
        [];
        oldFormData = TaskPostPreviousModel(
            taskID: taskInfo?.taskID,
            projectID: widget.taskEditArgs!.projectID,
            milestoneID: taskInfo?.milestoneCategoryID,
            templateTaskID: taskInfo?.templateTaskID,
            description: taskInfo?.taskDescription,
            taskPaymentPercentage: taskInfo?.taskPercentage ?? '',
            title: taskInfo?.taskName,
            targetDate: taskInfo?.taskTargetDateTime,
            dueDate: taskInfo?.taskEndDateTime,
            assignTo: taskInfo?.assignTo,
            requirements: Requirements(fileRequired: taskDetailsResponseModel?.requirements?.fileRequired),
            clientAppRequirements: ClientAppRequirements(
              autoUpdateCA: taskDetailsResponseModel?.clientAppRequirements?.autoUpdateCA,
              imageUpdateCA: taskDetailsResponseModel?.clientAppRequirements?.imageUpdateCA,
            ),
            subTasks: oldSubTaskList,
          );
      });
    } else {
      setState(() {
        taskInfo = TaskDetailsModel();
      });
    }
    _isLoading = false;
  }

  getSpecialTaskList() async {
    await _taskService.fetchSpecialTaskList(widget.taskEditArgs?.projectID ?? 0).then((response) {
      specialTaskList = response.response;
    });
  }

  getTeamMemberList() async {
    await _taskService.fetchTeamMemberList().then((response) {
      teamMember = response.response;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: PageStorageKey<String>('TaskForm'),
      backgroundColor: whiteGrey,
      appBar: _buildAppBar(context),
      body: !_isLoading
          ? GestureDetector(
            onTap: () {
              int? teamMemberID = teamMember.firstWhere((e) => e.teamMemberName!.compareTo(_assignController.text) == 0, orElse: () => TeamMemberDetails()).teamMemberID;
              if (teamMemberID == null) {
                _assignController.clear();
              }
            },
            child: _buildTaskFormView(context)
            )
          : Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Lottie.asset(
                    lottie_loading_spinner,
                    width: 50.0,
                    height: 50.0,
                    fit: BoxFit.fill,
                  ),
                  SizedBox(height: 16.0),
                  Text(
                    'just a few secs...',
                    style: regularFontsStyle(fontSize: 14.0, color: grey),
                  ),
                ],
              ),
            ),
      bottomNavigationBar: _buildSubmitButton(context),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.light,
      automaticallyImplyLeading: false,
      title: Text(
        widget.taskEditArgs?.taskDetail == null ? 'Add A New Task' : 'Edit Task',
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, color: white, letterSpacing: 0.8),
      ),
      actions: <Widget>[
        IconButton(
          onPressed: () {
            Navigator.pop(context, taskRescheduleArguments(null, false, null, null, null, null));
          },
          icon: SvgPicture.asset(ic_close, width: 15.0, color: white),
        ),
      ],
      centerTitle: false,
      backgroundColor: black,
    );
  }

  Widget _buildTaskFormView(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(24.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              // show task status 
              if ((taskInfo?.taskStatus == 'completed' || taskInfo?.taskStatus == 'flagged') && subtaskLength == 0)
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                    decoration: BoxDecoration(
                        color: taskInfo?.taskStatus == 'completed'
                            ? green
                            : taskInfo?.taskStatus == 'flagged'
                                ? lightPink
                                : transparent,
                        border: Border.all(
                            color: taskInfo?.taskStatus == 'completed'
                                ? green
                                : taskInfo?.taskStatus == 'flagged'
                                    ? lightPink
                                    : transparent),
                        borderRadius: BorderRadius.circular(500.0)),
                    child: Text(
                      taskInfo?.taskStatus == 'completed'
                          ? 'Completed'
                          : taskInfo?.taskStatus == 'flagged'
                              ? 'Flagged'
                              : '',
                      style: boldFontsStyle(
                          fontSize: 14.0,
                          color: taskInfo?.taskStatus == 'completed'
                              ? white
                              : taskInfo?.taskStatus == 'flagged'
                                  ? red
                                  : transparent),
                    ),
                  ),
                  if (taskInfo?.taskStatus == 'completed') SvgPicture.asset(ic_task_complete, width: 26.0),
                  if (taskInfo?.taskStatus == 'flagged') SvgPicture.asset(ic_task_flag, width: 26.0),
                ],
              ),
              if (taskInfo?.taskStatus == 'completed' || taskInfo?.taskStatus == 'flagged')
              SizedBox(height: 16.0),
              // Parent Task Info Widget
              _buildTaskInfoWidget(context),
              // Subtask Label & SubTask Form
              // temporary condition
              if (_taskController.text != 'Edited Photos' && (subTaskList.length != 0 || (taskInfo?.isAddSubTaskAllowed == 1 || taskInfo?.isAddSubTaskAllowed == null)  && !(taskInfo?.isSpecial ?? false))) 
                Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          'Sub Tasks',
                          style: boldFontsStyle(fontSize: 14.0, height: 1.29, color: grey),
                        ),
                        Text(
                          ' (If there`s any)',
                          style: italicFontsStyle(fontSize: 14.0, height: 1.29, color: grey),
                        ),
                      ],
                    ),
                    SizedBox(height: 16.0),
                    // Add SubTask Dorder Widget
                    // temporary condition
                    if ((taskInfo?.isAddSubTaskAllowed == 1 || taskInfo?.isAddSubTaskAllowed == null) && !(taskInfo?.isSpecial ?? false))
                    Padding(
                      padding: const EdgeInsets.only(bottom: 16.0),
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            subTaskList.insert(0, SubTasksPostModel(
                              taskID: 0, 
                              // temporary condition
                              title: (taskInfo?.isSpecial ?? false) ? "${subTaskList[0].title} Batch ${subTaskList.length+1}" : '',
                              targetDate: taskInfo?.taskTargetDateTime, 
                              dueDate: taskInfo?.taskEndDateTime,
                              requirements: taskDetailsResponseModel?.requirements,
                              clientAppRequirements: taskDetailsResponseModel?.clientAppRequirements
                            ));
                            fileRequiredBoolList.insert(0, _isSelectedUploadFiles);
                            autoUpdateCABoolList.insert(0, _isSelectedShownInApp);
                            imageUpdateBoolList.insert(0, _isSelectedIncludeFilesAttached);
                            isCompletedList?.insert(0, 0);
                            isFlaggedList?.insert(0, 0);
                            isDuedateEmpty?.insert(0, false);
                            duedateApplyToAll = false;
                            requirementApplyToAll = false;
                            clientSettingApplyToAll = false;
                            for (int i = 0; i < subTaskList.length; i++) {
                              subTaskList[i].requirements = Requirements(fileRequired: fileRequiredBoolList[i] ? 1 : 0);
                              subTaskList[i].clientAppRequirements = ClientAppRequirements(
                                autoUpdateCA: autoUpdateCABoolList[i] ? 1 : 0,
                                imageUpdateCA: imageUpdateBoolList[i] ? 1 : 0,
                              );
                            }
                          });
                        },
                        child: DottedBorder(
                          padding: EdgeInsets.all(16.0),
                          borderType: BorderType.RRect,
                          dashPattern: [6],
                          color: lightGrey,
                          child: Row(
                            children: [
                              SvgPicture.asset(ic_add, width: 24.0),
                              SizedBox(width: 16.0),
                              Text(
                                'Add A Sub Task',
                                style: TextStyle(fontSize: 16.0, height: 1.5),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    // SubTask Form List
                    Wrap(
                      children: [
                        for (int index = 0; index < subTaskList.length; index++) 
                          subTaskFormWidget(context, index)
                      ],
                    ),
                  ],
                ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildTaskInfoWidget(BuildContext context) {
    // ignore: unused_local_variable
    final List<String>? items = paymentPercentageArr;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Milestone Label
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Text(
            'Milestone*',
            style: boldFontsStyle(fontSize: 14.0, color: grey),
          ),
        ),
        // Milestone Drop Down
        if ((taskInfo?.isMilestoneEditable == 1 || taskInfo?.isMilestoneEditable == null))
        DropdownButtonFormField2<TaskMilestoneOption>(
          key: _milestoneKey,
          decoration: InputDecoration(
            isDense: true,
            contentPadding: EdgeInsets.zero,
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: darkYellow),
              borderRadius: BorderRadius.circular(6.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: lightGrey),
              borderRadius: BorderRadius.circular(6.0),
            ),
            border: OutlineInputBorder(),
            fillColor: white,
            filled: true,
          ),
          isExpanded: true,
          buttonStyleData: ButtonStyleData(
            height: 40,
            padding: EdgeInsets.only(top: 8.0, bottom: 8.0, right: 16.0),
            elevation: 2,
          ),
          iconStyleData: IconStyleData(
            icon: SvgPicture.asset(ic_dropdown, color: grey),
          ),
          dropdownStyleData: DropdownStyleData(
            maxHeight: 200.0,
            decoration: BoxDecoration(
              color: white
            )
          ),
          hint: Text(
            'Select Milestone',
            style: italicFontsStyle(fontSize: 16.0, color: grey),
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
          items: widget.taskEditArgs?.milestoneList
            ?.map((option) => DropdownMenuItem<TaskMilestoneOption>(
              value: option,
              child: Text(
                option.milestoneCategoryName ?? '',
                style: regularFontsStyle(fontSize: 16.0, height: 1.5),
                overflow: TextOverflow.ellipsis,
              ),
            ))
            .toList(),
          value: milestoneValue,
          onChanged: (value) {
            taskInfo?.milestoneCategoryID = value?.milestoneCategoryID;
          },
          onSaved: (value) {},
          validator: (value) {
            if (taskInfo?.milestoneCategoryID == null) {
              Scrollable.ensureVisible(_milestoneKey.currentContext!);
              return '*Please select milestone';
            }
            return null;
          }),
        if (taskInfo?.isMilestoneEditable == 0 )
        Container(
          child: Column(
            children: [
              SizedBox(height: 8.0),
              Text(
                taskInfo?.milestoneCategoryName ?? '',
                style: regularFontsStyle(fontSize: 16.0, height: 1.5)
              ),
            ],
          ),
        ),
        Divider(
          color: lightGrey,
          height: 32.0,
        ),
        // Task Label & Text Input Field
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Text(
            "Task Title*",
            style: boldFontsStyle(fontSize: 14.0, color: grey),
          ),
        ),
        if (taskInfo?.isTaskTitleEditable == 1 || taskInfo?.isTaskTitleEditable == null)
        TypeAheadField<SpecialTaskDetails> (
          key: _mainTitleKey, 
          controller: _taskController,
          builder: (context, controller, focusNode) {
            return TextFormField(
              controller: controller,
              focusNode: focusNode,
              onChanged: (value) {
                setState(() {
                  if (taskInfo?.isSpecial ?? false) {
                    taskInfo?.isSpecial = false;
                    taskInfo?.taskPercentage = null;
                    _taskController.clear();
                    taskInfo?.isAddSubTaskAllowed = 1;
                    taskInfo?.templateTaskID = 0;
                    subTaskList.clear();
                    taskInfo?.isRequirementEditable = 1;
                    taskInfo?.isClientAppPhotoEditable = 1;
                    taskInfo?.isClientAppUpdateEditable = 1;
                  }
                });
              },
              decoration: InputDecoration(
                hintText: 'Fill In Task Title',
                hintStyle: italicFontsStyle(fontSize: 16.0, height: 1.5, color: grey),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: darkYellow),
                  borderRadius: BorderRadius.circular(6.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: lightGrey),
                  borderRadius: BorderRadius.circular(6.0),
                ),
                border: OutlineInputBorder(),
                contentPadding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                fillColor: white,
                filled: true,
                prefixIcon: (taskInfo?.isSpecial ?? false) ? Container(
                  padding: EdgeInsets.only(left: 16.0, right: 8.0),
                  child: SvgPicture.asset(ic_star, width: 20.0),
                ) : null,
                prefixIconConstraints: BoxConstraints(maxHeight: 20.0),
                suffixIcon: (_taskController.text.isNotEmpty) ?
                Padding(
                  padding: EdgeInsets.only(right: 16.0),
                  child: Material(
                    color: transparent,
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          if (taskInfo?.isSpecial ?? false) {
                            taskInfo?.isSpecial = false;
                            taskInfo?.taskPercentage = null;
                            taskInfo?.isAddSubTaskAllowed = 1;
                            taskInfo?.templateTaskID = 0;
                            subTaskList.clear();
                            taskInfo?.isRequirementEditable = 1;
                            taskInfo?.isClientAppPhotoEditable = 1;
                            taskInfo?.isClientAppUpdateEditable = 1;
                          }
                        });
                        _taskController.clear();
                      },
                      child: SvgPicture.asset(ic_close, width: 18.0, color: black),
                    ),
                  ),
                ) : Padding(
                  padding: EdgeInsets.only(),
                ),
                suffixIconConstraints: BoxConstraints(maxHeight: 18.0),
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  Scrollable.ensureVisible(_mainTitleKey.currentContext!);
                  return '*Please fill in task title';
                }
                return null;
              }
            );
          },
          itemBuilder: (BuildContext context, SpecialTaskDetails suggestion) { 
            if (suggestion.templateTaskID == 0) {
              return ListTile(
                shape: Border(bottom: BorderSide(color: shimmerGrey)),
                contentPadding: EdgeInsets.only(left: 16.0),
                tileColor: white,
                title: Row(
                  children: [
                    SvgPicture.asset(ic_add, width: 20.0, color: grey),
                    SizedBox(width: 8.0),
                    Text(
                      "Add As New Task",
                      style: italicFontsStyle(fontSize: 14.0, color: grey),
                    ),
                  ],
                ),
              );
            }
            return ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 16.0),
              tileColor: white,
              title: SubstringHighlight(
                text: suggestion.templateTaskName ?? '',
                term: _taskController.text,
                textStyle: suggestion.disabled ?? true ? italicFontsStyle(color: lightGrey, fontSize: 16.0, height: 1.5) : regularFontsStyle(color: black, fontSize: 16.0, height: 1.5),
                textStyleHighlight: suggestion.disabled ?? true ? regularFontsStyle(color: lightGrey, fontSize: 16.0, height: 1.5) : boldFontsStyle(color: black, fontSize: 16.0, height: 1.5),
              ),
            );
          }, 
          onSelected: (SpecialTaskDetails suggestion) async { 
            if (suggestion.disabled == false) {
              bool replace = true;
              if (subTaskList.length > 0) {
                replace = await showDecision(
                  context, 
                  "Choosing this task will overide all existing sub tasks. Your sub tasks will be all removed. Are you sure?",
                  "Cancel",
                  "I’m Sure",
                  titleIcon: SvgPicture.asset(ic_task_overtime, width: 60, color: red),
                );
              }
              if (replace) {
                _taskController.text = suggestion.templateTaskName ?? '';
                setState(() {
                  taskInfo?.templateTaskID = suggestion.templateTaskID ?? 0;
                  if ((taskInfo?.templateTaskID ?? 0) > 0)
                    taskInfo?.isSpecial = true;
                  _isSelectedUploadFiles = suggestion.requirements?.fileRequired == 1;
                  _isSelectedShownInApp = suggestion.clientAppRequirements?.autoUpdateCA == 1;
                  _isSelectedIncludeFilesAttached = suggestion.clientAppRequirements?.imageUpdateCA == 1;
                  milestoneValue = widget.taskEditArgs?.milestoneList!.firstWhere((e) => e.milestoneCategoryID == suggestion.milestoneCategoryID);
                  taskInfo?.milestoneCategoryID = suggestion.milestoneCategoryID;
                  taskInfo?.taskPercentage = suggestion.taskPercentage;
                  _taskDescController.text = suggestion.templateTaskDescription ?? '';
                  paymentPercentageArr = ['10', '20', '30', '40', '50', '60', '70', '80', '90', '100'];
                  _assignController.text = teamMember.firstWhere((e) => e.teamMemberID == suggestion.teamMemberID, orElse: () => TeamMemberDetails()).teamMemberName ?? '';
                  subTaskList = suggestion.defaultSubtask
                    ?.map((e) => SubTasksPostModel(
                          taskID: 0,
                          title: e.templateTaskName,
                          description: e.templateTaskDescription,
                          templateTaskID: e.templateTaskID,
                          targetDate: taskInfo?.taskEndDateTime,
                          dueDate: taskInfo?.taskEndDateTime,
                          requirements: e.requirements,
                          clientAppRequirements: e.clientAppRequirements
                        ))
                    .toList() ??
                  [];
                });
                subtaskLength = subTaskList.length;
                if (subtaskLength != 0) {
                  for (int index = 0; index < subTaskList.length; index++) {
                    _fileRequiredCheckedList = suggestion.defaultSubtask?.map((e) => e.requirements?.fileRequired).toList();
                    fileRequiredBoolList = _fileRequiredCheckedList!.map((item) => item == 1).toList();
                    _autoUpdateCACheckedList = suggestion.defaultSubtask?.map((e) => e.clientAppRequirements?.autoUpdateCA).toList();
                    autoUpdateCABoolList = _autoUpdateCACheckedList!.map((item) => item == 1).toList();
                    _imageUpdateCheckedList= suggestion.defaultSubtask?.map((e) => e.clientAppRequirements?.imageUpdateCA).toList();
                    imageUpdateBoolList = _imageUpdateCheckedList!.map((item) => item == 1).toList();
                    isDuedateEmpty?.insert(index, false);
                    isCompletedList?.insert(index, 0);
                    isFlaggedList?.insert(index, 0);
                  }
                }
              }
            }
          }, 
          suggestionsCallback: (pattern) { 
            return Future.delayed(Duration(milliseconds: 350), () => getSpecialTaskOptions(pattern));
          },
          loadingBuilder: (context) {
            return Container(height: 0);
          },
          decorationBuilder: (context, child) => Material(
            color: white,
            child: child,
          ),
        ),
        if (taskInfo?.isTaskTitleEditable == 0 && !(taskInfo?.isSpecial ?? false))
        Text (
          taskInfo?.paymentTaskName ?? '',
          style: regularFontsStyle(fontSize: 16.0, height: 1.5),
        ),
        if (taskInfo?.isTaskTitleEditable == 0 && (taskInfo?.isSpecial ?? false))
        RichText(
            text: TextSpan(
              children: [
                WidgetSpan(
                  alignment: PlaceholderAlignment.middle,
                  child: Padding(
                    padding: EdgeInsets.only(right: 8.0),
                    child: SvgPicture.asset(ic_star, width: 20.0),
                  ),
                ),
                TextSpan(
                  text: taskInfo?.paymentTaskName,
                  style: regularFontsStyle(fontSize: 16.0, height: 1.5),
                ),
              ]
            ),
          ),
        SizedBox(height: 16.0),
        // Description Label & Text Input Field
        if (taskInfo?.isTaskDescriptionEditable == 1 || taskInfo?.isTaskDescriptionEditable == null)
        textFieldWidget(
            label: "Task Description ",
            optional: '(optional)',
            hint: "Fill In Description If There's Any",
            required: false,
            onChanged: (text) {
              taskInfo?.taskDescription = text;
            },
            controller: _taskDescController,
          ),
        if (taskInfo?.isTaskDescriptionEditable == 0)
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Text(
                      "Task Description ",
                      style: boldFontsStyle(fontSize: 14.0, color: grey),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Text(
                      '(optional)',
                      style: italicFontsStyle(fontSize: 14.0, color: grey),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8.0,),
              Text (
                taskInfo?.taskDescription ?? '',
                style: regularFontsStyle(fontSize: 16.0, height: 1.5),
              ),
              SizedBox(height: 20.0,)
            ]
          ),
        // Target & Due Date Label & Date Picker
        assignTo(),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
            if (subTaskList.length == 0)
            Expanded(
              child: datePickerWidget('Due Date', 'Choose A Date', 
              onChanged: (text) {
                setState(() {
                  isDueDateClear = false;
                });
                var prevTaskEndDate = taskInfo?.taskEndDateTime;
                if (prevTaskEndDate != text) {
                  isDelay = true;
                }
                taskInfo?.taskEndDateTime = text.toString();
                taskInfo?.taskTargetDateTime = text.toString();
              }, 
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return '*Please choose a\n  date';
                }
                return null;
              },
              editable: (taskInfo?.isCompleted == 1 || taskInfo?.isFlag == 1 ) ? false : true,
              prevDate: taskInfo?.taskEndDateTime,
              closeButton: Padding(
                padding: EdgeInsets.only(left: 10),
                child: Material(
                  color: transparent,
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        isDueDateClear = true;
                      });
                    },
                    child: Container(
                      transform: Matrix4.translationValues(2, 3, 5),
                      child: SvgPicture.asset(ic_close, color: black)
                    ),
                  ),
                ),
              ),
              controller: (isDueDateClear ?? false) ? TextEditingController(text: null) : TextEditingController(text: taskInfo?.taskEndDateTime) 
              ),
            ),
            const SizedBox(width: 140.0),
          ]), 
        if (subTaskList.length == 0)         
        requirement(
          onChanged: (value) {
            setState(() {
              if ((taskInfo?.isRequirementEditable == null || taskInfo?.isRequirementEditable == 1) && !(taskInfo?.isSpecial ?? false))
                _isSelectedUploadFiles = value;
            });
          },
          value: _isSelectedUploadFiles,
          opacity: ((taskInfo?.isRequirementEditable == null || taskInfo?.isRequirementEditable == 1) && !(taskInfo?.isSpecial ?? false)) ? 1.0 : 0.5,
        ),
        if (subTaskList.length == 0)
        clientAppSetting(
          onAutoPublishChanged: (value) {
            setState(() {
              if ((taskInfo?.isClientAppUpdateEditable == null || taskInfo?.isClientAppUpdateEditable == 1) && !(taskInfo?.isSpecial ?? false))
                _isSelectedShownInApp = value;
            });
          },
          onFileRequiredChanged: (value) {
            setState(() {
              if ((taskInfo?.isClientAppPhotoEditable == null || taskInfo?.isClientAppPhotoEditable == 1) && !(taskInfo?.isSpecial ?? false))
                _isSelectedIncludeFilesAttached = value;
            });
          },
          value1: _isSelectedShownInApp,
          value2: _isSelectedIncludeFilesAttached,
          opacityAutoUpdate: ((taskInfo?.isClientAppUpdateEditable == null || taskInfo?.isClientAppUpdateEditable == 1) && !(taskInfo?.isSpecial ?? false)) ? 1.0 : 0.5,
          opacityImageUpdate: ((taskInfo?.isClientAppPhotoEditable == null || taskInfo?.isClientAppPhotoEditable == 1) && !(taskInfo?.isSpecial ?? false)) ? 1.0 : 0.5,
        ),
        if ((subTaskList.length != 0) || (taskInfo?.isAddSubTaskAllowed == 1 || taskInfo?.isAddSubTaskAllowed == null) && !(taskInfo?.isSpecial ?? false))
        Divider(
          color: lightGrey,
          height: 32.0,
        ),
      ],
    );
  }

  Widget subTaskFormWidget(BuildContext context, int index) {
    var subTask = subTaskList[index];
    bool isCompleted = false;
    bool isFlagged = false;
    bool? isSpecial = false;
    // ignore: unused_local_variable
    bool? isAbleToRemove = false;
    TextEditingController _duaDateController = TextEditingController();
    List<GlobalObjectKey<FormState>> formKeyList = List.generate(subTaskList.length, (index) => GlobalObjectKey<FormState>(subTaskList[index]));

    if ((taskDetailsResponseModel?.subTaskArr?.where((e) => e.taskID == subTaskList[index].taskID).length ?? 0) > 0) {
      isCompleted = taskDetailsResponseModel?.subTaskArr?.firstWhere((e) => e.taskID == subTaskList[index].taskID).taskStatus == "completed";
      isFlagged = taskDetailsResponseModel?.subTaskArr?.firstWhere((e) => e.taskID == subTaskList[index].taskID).taskStatus == "flagged";
      isSpecial = taskDetailsResponseModel?.subTaskArr?.firstWhere((e) => e.taskID == subTaskList[index].taskID).isSpecial == true;
      isAbleToRemove = taskDetailsResponseModel?.subTaskArr?.firstWhere((e) => e.taskID == subTaskList[index].taskID).isRemoveTaskAllowed == 1;
    }

    void _onCategorySelected(bool selected, List<bool> item, index) {
      if (selected == true) {
        setState(() {
          item[index] = true;
        });
      } else {
        setState(() {
          item[index] = false;
        });
      }
    }

    return Deleteable(
      key: ValueKey(subTask),
      builder: (context, delete) {
        return Container(
          padding: EdgeInsets.all(16.0),
          margin: EdgeInsets.only(bottom: 16.0),
          decoration: BoxDecoration(
            color: white,
            border: Border.all(color: Color(0xFFC0BFBB)),
            borderRadius: BorderRadius.circular(12.0),
            boxShadow: subtask_box_shadow,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: 
                [
                  if (isCompleted)
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                        decoration: BoxDecoration(
                          color: green,
                          border: Border.all(
                            color: green,
                          ),
                          borderRadius: BorderRadius.circular(500.0),
                        ),
                        child: Text(
                          'Completed',
                          style: boldFontsStyle(fontSize: 14.0, color: white),
                        ),
                      ),
                      SvgPicture.asset(ic_task_complete, width: 26.0),
                    ],
                  ),
                  if (isFlagged)
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                        decoration: BoxDecoration(
                          color: lightPink,
                          border: Border.all(
                            color: lightPink,
                          ),
                          borderRadius: BorderRadius.circular(500.0),
                        ),
                        child: Text(
                          'Flagged',
                          style: boldFontsStyle(fontSize: 14.0, color: red),
                        ),
                      ),
                      SvgPicture.asset(ic_task_flag, width: 26.0),
                    ],
                  ),
                  if (isCompleted || isFlagged)
                  SizedBox(height: 16.0),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: textFieldWidget(
                            label: 'Sub Task Title*',
                            hint: 'Fill In Sub Task Title',
                            required: true,
                            initialValue: subTask.title,
                            onChanged: (text) {
                              subTask.title = text;
                            },
                            editable: ((!isCompleted && !isFlagged && !(isSpecial ?? false)) && !(taskInfo?.isSpecial ?? false)),
                            subTaskIsSpecial: (isSpecial ?? false) || (taskInfo?.isSpecial ?? false)
                          )
                        ),
                        SizedBox(width: 16.0),
                        if (((!isCompleted && !isFlagged && !(isSpecial ?? false)) || widget.taskEditArgs?.taskDetail == null) && !(taskInfo?.isSpecial ?? false))
                          GestureDetector(
                            onTap: () async {
                              await delete();
                              setState(() {
                                subTaskList.remove(subTask);
                                fileRequiredBoolList.removeAt(index);
                                autoUpdateCABoolList.removeAt(index);
                                imageUpdateBoolList.removeAt(index);
                                isCompletedList?.removeAt(index);
                                isFlaggedList?.removeAt(index);
                                isDuedateEmpty?.removeAt(index);
                              });
                            },
                            child: SvgPicture.asset(
                              ic_task_remove_subtask,
                              width: 40.0,
                            )
                          )
                      ],
                    ),
                    //Expandable Columns
                    ExpandableNotifier(
                      controller: ExpandableController(initialExpanded: isDuedateEmpty?[index]),
                      child: Expandable(
                        collapsed: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            textFieldWidget(
                              label: 'Sub Task Description ',
                              optional: '(optional)',
                              hint: "Fill In Description If There's Any",
                              required: false,
                              initialValue: subTask.description,
                              onChanged: (text) {
                                subTask.description = text;
                              }),
                            Row(children: [
                              Expanded(
                                key:  formKeyList.isNotEmpty ? formKeyList[index] : null,
                                child: datePickerWidget('Due Date', 'Choose A Date', 
                                  // dateTime: (duedateApplyToAll == false) ? _duaDateController.text = subTask.dueDate ?? '' : null, 
                                  controller: _duaDateController = TextEditingController (text: subTask.dueDate),
                                  onChanged: (text) {
                                    var prevSubDueDate = subTask.dueDate;
                                    if (prevSubDueDate != text) {
                                      isDelay = true;
                                    }
                                    _duaDateController.text = text.toString();
                                    subTask.dueDate = text.toString();
                                    subTask.targetDate = text.toString();
                                  }, 
                                  validator: (value) {                                  
                                    for (int i = 0; i < subTaskList.length; i++) {
                                      if(subTaskList[i].dueDate == null) {
                                        setState(() {
                                          isDuedateEmpty?[i] = false;
                                        });
                                      }
                                    }
                                    if (value == null || value.isEmpty) {
                                      Scrollable.ensureVisible(formKeyList[index].currentContext!);
                                      return '*Please choose a date';
                                    }
                                    // if (DateTime.parse(subTask.targetDate).isAfter(DateTime.parse(subTask.dueDate!))) {
                                    //   return 'Invalid Date';
                                    // }
                                    return null;
                                  },
                                  editable: (!isCompleted && !isFlagged),
                                  prevDate: subTask.dueDate,
                                  closeButton: Padding(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Material(
                                    color: transparent,
                                    child: InkWell(
                                      onTap: () {
                                        setState(() {
                                          subTask.dueDate = null;
                                          duedateApplyToAll = true;
                                        });
                                      },
                                      child: Container(
                                        transform: Matrix4.translationValues(2, 3, 5),
                                        child: SvgPicture.asset(ic_close, color: black)
                                      ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 100.0),
                            ]),
                            if (!isCompleted && !isFlagged)
                            applyToAllButton(
                              onTap: () {
                                setState(() {
                                  // duedateApplyToAll = true;
                                  subTask.dueDate = (_duaDateController.text != '') ? _duaDateController.text : null;
                                  sameDueDate = subTask.dueDate;
                                  var formattedSameDueDate = DateFormat('d MMM yyyy').parse(sameDueDate);
                                  var formattedOutput = DateFormat('yyyy-MM-dd HH:mm:ss.SSS').format(formattedSameDueDate);
                                  for (int i = 0; i < subTaskList.length; i++) {
                                    if(isCompletedList?[i] != 1 && isFlaggedList?[i] != 1)
                                    subTaskList[i].dueDate = formattedOutput;
                                    subTaskList[i].targetDate = formattedOutput;
                                  }
                                });
                                RefreshIndicator(
                                  backgroundColor: white,
                                  child: Container(),
                                  onRefresh:() async {
                                    datePickerWidget('Due Date', 'Choose A Date',
                                      dateTime: (duedateApplyToAll == false) ? subTask.dueDate : null, 
                                      controller: (duedateApplyToAll == true) ? TextEditingController(text: subTask.dueDate) : null,
                                    );
                                  },
                                );
                              }
                            ),
                            SizedBox(height: 20.0),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      requirement(
                                        onChanged: (value) {
                                          setState(() {
                                            if (!isCompleted && !isFlagged && !(isSpecial ?? false) && !(taskInfo?.isSpecial ?? false))
                                            _onCategorySelected(value, fileRequiredBoolList, index);
                                            subTask.requirements?.fileRequired = fileRequiredBoolList[index] ? 1 : 0;
                                          });
                                        },
                                        value: fileRequiredBoolList[index],
                                        opacity: (!isCompleted && !isFlagged&& !(isSpecial ?? false) && !(taskInfo?.isSpecial ?? false)) ? 1.0 : 0.5,
                                      ),
                                      if (!isCompleted && !isFlagged && !(isSpecial ?? false) && !(taskInfo?.isSpecial ?? false))
                                      applyToAllButton(
                                        onTap: () {
                                          setState(() {
                                            requirementApplyToAll = true;
                                            sameRequirement = fileRequiredBoolList[index];
                                            for (int i = 0; i < subTaskList.length; i++) {
                                              if (isCompletedList?[i] != 1 && isFlaggedList?[i] != 1)
                                              fileRequiredBoolList[i] = sameRequirement!;
                                            }
                                            for (int i = 0; i < subTaskList.length; i++) {
                                              subTaskList[i].requirements?.fileRequired = fileRequiredBoolList[i] ? 1 : 0;
                                            }
                                          });
                                        }
                                      )
                                    ],
                                  ),
                                  
                                ),
                                SizedBox(width: 8.0),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      clientAppSetting(
                                        onAutoPublishChanged: (value) {
                                          setState(() {
                                            if (!isCompleted && !isFlagged && !(isSpecial ?? false) && !(taskInfo?.isSpecial ?? false))
                                              _onCategorySelected(value, autoUpdateCABoolList, index);
                                              subTask.clientAppRequirements?.autoUpdateCA =  autoUpdateCABoolList[index] ? 1 : 0;
                                          });
                                        },
                                        onFileRequiredChanged: (value) {
                                          setState(() {
                                            if (!isCompleted && !isFlagged && !(isSpecial ?? false) && !(taskInfo?.isSpecial ?? false))
                                              _onCategorySelected(value, imageUpdateBoolList, index);
                                              subTask.clientAppRequirements?.imageUpdateCA = imageUpdateBoolList[index] ? 1 : 0;
                                          });
                                        },
                                        value1: autoUpdateCABoolList[index],
                                        value2: imageUpdateBoolList[index],
                                        opacityAutoUpdate: (!isCompleted && !isFlagged && !(isSpecial ?? false) && !(taskInfo?.isSpecial ?? false)) ? 1 : 0.5,
                                        opacityImageUpdate: (!isCompleted && !isFlagged && !(isSpecial ?? false) && !(taskInfo?.isSpecial ?? false)) ? 1 : 0.5
                                      ),
                                      if (!isCompleted && !isFlagged && !(isSpecial ?? false) && !(taskInfo?.isSpecial ?? false))
                                      applyToAllButton(
                                        onTap: () {
                                          setState(() {
                                            clientSettingApplyToAll = true;
                                            sameautoUpdateCA = autoUpdateCABoolList[index];
                                            sameimageUpdate = imageUpdateBoolList[index];
                                            for (int i = 0; i < subTaskList.length; i++) {
                                              if (isCompletedList?[i] != 1 && isFlaggedList?[i] != 1) {
                                                autoUpdateCABoolList[i] = sameautoUpdateCA!;
                                                imageUpdateBoolList[i] = sameimageUpdate!;
                                              }
                                            }
                                            for (int i = 0; i < subTaskList.length; i++) {
                                              subTaskList[i].clientAppRequirements = ClientAppRequirements(autoUpdateCA: autoUpdateCABoolList[i] ? 1 : 0, imageUpdateCA: imageUpdateBoolList[i] ? 1 : 0);
                                            }
                                          });
                                        }
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Divider(
                              color: lightGrey,
                              height: 32.0,
                            ),
                            Align(
                                alignment: Alignment.centerRight,
                                child: ExpandableButton(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            isDuedateEmpty?[index] = true;
                                          });
                                        },
                                        child: Row(
                                          children: [
                                            Text(
                                              'Collapse',
                                              style: boldFontsStyle(fontSize: 14.0, height: 1.29, color: grey),
                                            ),
                                            SizedBox(width: 8.0),
                                            SvgPicture.asset(ic_dropdown, width: 24.0, color: grey),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ))
                          ],
                        ),
                        expanded: ExpandableButton(
                            child: Column(
                              children: [
                                Divider(
                                  color: lightGrey,
                                  height: 32.0,
                                ),
                                Align(
                                    alignment: Alignment.centerRight,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              isDuedateEmpty?[index] = false;
                                            });
                                          },
                                          child: Row(
                                            children: [
                                              Text(
                                                'Edit',
                                                style: boldFontsStyle(fontSize: 14.0, height: 1.29, color: grey),
                                              ),
                                              SizedBox(width: 8.0),
                                              RotatedBox(
                                                quarterTurns:2,
                                                child: SvgPicture.asset(ic_dropdown, width: 24.0, color: grey)
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    )),
                              ],
                            )),
                      ),
                    ),
                  ]
          ),
        );
      });
  }

  Widget textFieldWidget({
    required String label, 
    required String hint, 
    required bool required, 
    String? optional,
    String? initialValue, 
    ValueChanged? onChanged, 
    bool editable = true,
    bool subTaskIsSpecial = false,
    TextEditingController? controller
  }) {
    final dataKey = new GlobalKey();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Text(
                label,
                style: boldFontsStyle(fontSize: 14.0, color: grey),
              ),
            ),
            Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Text(
            optional ?? '',
            style: italicFontsStyle(fontSize: 14.0, color: grey),
          ),
        ),
          ],
        ),
        editable ? TextFormField(
          key: dataKey,
          controller: controller,
          initialValue: initialValue,
          onChanged: onChanged,
          validator: (value) {
            if ((value == null || value.isEmpty || value == '') && required) {
              Scrollable.ensureVisible(dataKey.currentContext!);
              return '*Please fill in sub task title';
            }
            return null;
          },
          decoration: InputDecoration(
            hintText: hint,
            hintStyle: italicFontsStyle(fontSize: 16.0, height: 1.5, color: grey),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: darkYellow),
              borderRadius: BorderRadius.circular(6.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: lightGrey),
              borderRadius: BorderRadius.circular(6.0),
            ),
            border: OutlineInputBorder(),
            contentPadding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
            fillColor: white,
            filled: true,
          ),
          style: regularFontsStyle(fontSize: 16.0, height: 1.5),
        ) : Row(
          children: [
            if (subTaskIsSpecial)
            SvgPicture.asset(ic_star, width: 20.0),
            if (subTaskIsSpecial)
            SizedBox(width: 8.0),
            Container(
              padding: EdgeInsets.symmetric(vertical: 8.0),
              child: Text(
                initialValue ?? '',
                style: regularFontsStyle(fontSize: 16.0, height: 1.5),
              ),
            ),
          ],
        ),
        SizedBox(height: 16.0),
      ],
    );
  }

  Widget datePickerWidget(String label, String hint, {String? dateTime, TextEditingController? controller, ValueChanged? onChanged, String? Function(String?)? validator, bool editable = true, var prevDate, Padding? closeButton}) {
    DateFormat formatter = DateFormat('d MMM yyyy');
    var dateTimeText;
    var formattedDate;
    if (prevDate != null && controller?.text != '') {
      dateTimeText = DateTime.parse(prevDate);
      formattedDate = formatter.format(dateTimeText);
      controller?.text = formattedDate;
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Text(
            label,
            style: boldFontsStyle(fontSize: 14.0, color: grey),
          ),
        ),
        editable ? TextFormField(
          controller: controller,
          onChanged: onChanged,
          validator: validator,
          onTap: () async {
            formattedDate = await showDatePicker(
              context: context,
              initialDate: prevDate != null ? DateTime.parse(prevDate) : DateTime.now(),
              firstDate: DateTime.now().subtract(Duration(days: 365)),
              lastDate: DateTime.now().add(Duration(days: 3650)),
              builder: (context, child) {
                return Theme(
                  data: ThemeData.light().copyWith(
                    datePickerTheme: DatePickerThemeData(
                      backgroundColor: white,
                      surfaceTintColor: Colors.transparent,
                      headerForegroundColor: Colors.black54,
                      dayBackgroundColor: WidgetStateProperty.resolveWith((states) {
                        if (states.contains(WidgetState.selected)) {
                          return darkYellow;
                        }
                        return white;
                      }),
                      todayBackgroundColor: WidgetStateProperty.resolveWith((states) {
                        if (states.contains(WidgetState.selected)) {
                          return darkYellow;
                        }
                        return white;
                      }),
                      todayForegroundColor: WidgetStateProperty.resolveWith((states) {
                        if (states.contains(WidgetState.selected)) {
                          return white;
                        }
                        return darkYellow;
                      }),
                      yearOverlayColor: WidgetStateProperty.resolveWith((states) {
                        if (states.contains(WidgetState.selected)) {
                          return darkYellow;
                        }
                        return white;
                      }),
                      yearForegroundColor: WidgetStateProperty.resolveWith((states) {
                        if (states.contains(WidgetState.selected)) {
                          return white;
                        }
                        return black;
                      }),
                      yearBackgroundColor: WidgetStateProperty.resolveWith((states) {
                        if (states.contains(WidgetState.selected)) {
                          return darkYellow;
                        }
                        return white;
                      }),
                      confirmButtonStyle: ButtonStyle(
                        backgroundColor: WidgetStateProperty.resolveWith((states) {
                          return white;
                        }),
                        foregroundColor: WidgetStateProperty.resolveWith((states) {
                          return darkYellow;
                        }),
                      ),
                      cancelButtonStyle: ButtonStyle(
                        backgroundColor: WidgetStateProperty.resolveWith((states) {
                          return white;
                        }),
                        foregroundColor: WidgetStateProperty.resolveWith((states) {
                          return grey;
                        }),
                      ),
                    ),
                  ),
                  child: child!,
                );
              },
            );
            if (formattedDate != null) {
              var text = formattedDate;
              if (onChanged != null) {
                onChanged(text);
              }
              controller?.text = formatter.format(formattedDate);;
            }
          },
          decoration: InputDecoration(
            hintText: hint,
            hintStyle: italicFontsStyle(fontSize: 16.0, height: 1.5, color: grey),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: darkYellow),
              borderRadius: BorderRadius.circular(6.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: lightGrey),
              borderRadius: BorderRadius.circular(6.0),
            ),
            border: OutlineInputBorder(),
            contentPadding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
            fillColor: white,
            filled: true,
            suffix: closeButton,
          ),
          style: regularFontsStyle(fontSize: 16.0, height: 1.5),
          onSaved: (val) => {},
        ) : Container(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            formattedDate ?? formattedDate.toString(),
            style: regularFontsStyle(fontSize: 16.0, height: 1.5),
          ),
        ),
        if (subTaskList.length == 0)
        SizedBox(height: 16.0),
        if (subTaskList.length != 0)
        SizedBox(height: 8.0),
      ],
    );
  }

  String? customValidator(String? targetDate, String? endDate, String? value) {
    //<-- add String? as a return type
    if (value == null || value.isEmpty) {
      return 'This is a required field';
    }
    DateTime targetDateTime = DateTime.parse(taskInfo!.taskTargetDateTime!);
    DateTime endDateTime = DateTime.parse(taskInfo!.taskEndDateTime!);
    if (targetDateTime.isAfter(endDateTime)) {
      return 'Invalid Date';
    }
    return null;
  }

  Widget assignTo() {
    // Assign To Label & Text Input Field
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Text(
            "Assign To*",
            style: boldFontsStyle(fontSize: 14.0, color: grey),
          ),
        ),
        if (taskInfo?.isAssigneeEditable == 1 || taskInfo?.isAssigneeEditable == null)
        TypeAheadField<TeamMemberDetails>(
          key: _assignToKey,
          controller: _assignController,
          builder: (context, controller, focusNode) {
            return TextFormField(
              controller: controller,
              focusNode: focusNode,
              decoration: InputDecoration(
                hintText: 'Search Name',
                hintStyle: italicFontsStyle(fontSize: 16.0, height: 1.5, color: grey),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: darkYellow),
                  borderRadius: BorderRadius.circular(6.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: lightGrey),
                  borderRadius: BorderRadius.circular(6.0),
                ),
                border: OutlineInputBorder(),
                contentPadding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                fillColor: white,
                filled: true,
                suffix: Padding(
                  padding: EdgeInsets.only(left: 10),
                  child: Material(
                    color: transparent,
                    child: InkWell(
                      onTap: () {
                        _assignController.clear();
                      },
                      child: Container(
                        transform: Matrix4.translationValues(2, 3, 5),
                        child: SvgPicture.asset(ic_close, color: black)
                      ),
                    ),
                  ),
                ),
              ),
              style: regularFontsStyle(fontSize: 16.0, height: 1.5),
              textInputAction: TextInputAction.done, // Set the input action to "done"
              onEditingComplete: _onEditingComplete, // Handle the "Done" button press
              validator: (value) {
                int? teamMemberID = teamMember.firstWhere((e) => e.teamMemberName!.compareTo(_assignController.text) == 0, orElse: () => TeamMemberDetails()).teamMemberID;
                if (value == null || value.isEmpty) {
                  Scrollable.ensureVisible(_assignToKey.currentContext!);
                  return '*Please select';
                } else if (teamMemberID == null) {
                  _assignController.clear();
                  Scrollable.ensureVisible(_assignToKey.currentContext!);
                  return 'Team Member Name \nis incorrect';
                }
                return null;
              },
            );
          },
          decorationBuilder: (context, child) {
            return Material(
              color: white,
              child: child,
            );
          },
          suggestionsCallback: (pattern) {
            return Future.delayed(Duration(milliseconds: 350), () => getTeamMemberOptions(pattern));
          },
          itemBuilder: (context, TeamMemberDetails suggestion) {
            return Container(
              child: ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 16.0),
                tileColor: white,
                title: Text(suggestion.teamMemberName ?? ''),
              ),
            );
          },
          onSelected: (TeamMemberDetails suggestion) {
            _assignController.text = suggestion.teamMemberName ?? '';
          },
          loadingBuilder: (context) {
            return Container(height: 0);
          },
        ),
        if (taskInfo?.isAssigneeEditable == 0)
          Container(
            child: Text(
              _assignController.text,
              style: regularFontsStyle(fontSize: 16.0, height: 1.5),
            ),
          ),
          SizedBox(height: 16.0,)
      ],
    );
  }

  Widget requirement({ValueChanged? onChanged, bool? value, double? opacity}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Requirements Label & Checkbox
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Text(
            "Requirements",
            style: boldFontsStyle(fontSize: 14.0, color: grey),
          ),
        ),
        Opacity(
          opacity: opacity ?? 1.0,
          // (completedSubtaskLength == 0 && !(taskInfo?.isSpecial ?? false)) ? 1.0 : 0.5,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GFCheckbox(
                size: 26.0,
                type: GFCheckboxType.circle,
                activeBgColor: yellow,
                inactiveBorderColor: lightGrey,
                activeBorderColor: black,
                activeIcon: Icon(Icons.check, size: 16, color: black),
                onChanged: onChanged,
                value: value ?? true,
                inactiveIcon: null,
              ),
              SizedBox(width: 8.0),
              Flexible(
                child: Text(
                  "Complete With Files",
                  style: regularFontsStyle(fontSize: 16.0, height: 1.5),
                ),
              ),
            ],
          ),
        ),
        if (subTaskList.length == 0)
        SizedBox(height: 16.0),
        if (subTaskList.length != 0)
        SizedBox(height: 8.0),
      ],
    );
  }

  Widget clientAppSetting({ValueChanged? onAutoPublishChanged, ValueChanged? onFileRequiredChanged, bool? value1, bool? value2, double? opacityAutoUpdate, double? opacityImageUpdate}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Text(
            "Client App Settings",
            style: boldFontsStyle(fontSize: 14.0, color: grey),
          ),
        ),
        Opacity(
          opacity: opacityAutoUpdate ?? 1.0,
          // (completedSubtaskLength == 0 && !(taskInfo?.isSpecial ?? false)) ? 1.0 : 0.5,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GFCheckbox(
                size: 26.0,
                type: GFCheckboxType.circle,
                activeBgColor: yellow,
                inactiveBorderColor: lightGrey,
                activeBorderColor: black,
                activeIcon: Icon(Icons.check, size: 16, color: black),
                onChanged: onAutoPublishChanged,
                value: value1 ?? true,
                inactiveIcon: null,
              ),
              SizedBox(width: 8.0),
              Flexible(
                child: Text(
                  "Auto-Publish To Client",
                  style: regularFontsStyle(fontSize: 16.0, height: 1.5),
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 8.0),
          Opacity(
            opacity: opacityImageUpdate ?? 1.0,
            // (completedSubtaskLength == 0 && !(taskInfo?.isSpecial ?? false)) ? 1.0 : 0.5,
            child: Row(
              children: [
                GFCheckbox(
                  size: 26.0,
                  type: GFCheckboxType.circle,
                  activeBgColor: yellow,
                  inactiveBorderColor: lightGrey,
                  activeBorderColor: black,
                  activeIcon: Icon(Icons.check, size: 16, color: black),
                  onChanged: onFileRequiredChanged,
                  value: value2 ?? true,
                  inactiveIcon: null,
                ),
                SizedBox(width: 8.0),
                Flexible(
                  child: Text(
                    "Include Files If Published",
                    style: regularFontsStyle(fontSize: 16.0, height: 1.5),
                  ),
                ),
              ],
            ),
          ),
          if (subTaskList.length == 0)
          SizedBox(height: 16.0),
          if (subTaskList.length != 0)
          SizedBox(height: 8.0),
      ],
    );
  }

  Widget applyToAllButton({Function()? onTap}) {
    return GestureDetector(
      onTap: onTap,
      child: Text(
        'Apply This To All',
        style: boldFontsStyle(fontSize: 16.0, height: 1.5, fontWeight: FontWeight.w700, decoration: TextDecoration.underline, color: darkYellow),
      ),
    );
  }

  Widget _buildSubmitButton(BuildContext context) {
    bool enableRemoveButton = (widget.taskEditArgs?.taskDetail == null || ((canDeleteTask ?? false) && taskInfo?.isRemoveTaskAllowed == 1) ) && !_isLoading;

    return Container(
        padding: EdgeInsets.all(24.0),
        decoration: BoxDecoration(
          color: darkGrey,
          borderRadius: BorderRadius.vertical(top: Radius.circular(30.0)),
        ),
        child: Row(
          children: [
            Expanded(
              child: Opacity(
                opacity: enableRemoveButton ? 1.0 : 0.5,
                child: ElevatedButton(
                  onPressed: () async {
                    if (widget.taskEditArgs?.taskDetail == null) {
                      Navigator.pop(context, taskRescheduleArguments(null, false, null, null, null, null));
                    } else {
                      if (enableRemoveButton) {
                        bool decision = await showDecision(
                          context, 
                          'Are you sure you want to remove this task?',
                          'Cancel',
                          'Remove',
                        );
                        if(decision) {
                          setState(() {
                            onlyClickOnce = onlyClickOnce + 1;
                          });

                          if (onlyClickOnce == 1)
                          _taskService.taskDelete(taskInfo!.taskID!).then((response) {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                // Store the dialog context to access it later
                                final dialogContext = context;

                                // Delayed execution after 2 seconds
                                Future.delayed(Duration(seconds: 2), () {
                                  // Dismiss the dialog and navigate to another page
                                  Navigator.of(dialogContext).pop();
                                  // Navigator.of(dialogContext).pop();
                                });
                                return AlertDialog(
                                  backgroundColor: white,
                                  title: SvgPicture.asset(ic_completed, width: 45),
                                  content: Text(
                                    'Removed Successfully!',
                                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: black),
                                    textAlign: TextAlign.center,
                                  ),
                                );
                              }
                            ).then((value) {
                                Navigator.pop(context, taskRescheduleArguments(null, false, null, null, null, null));
                                Navigator.pop(context, taskRescheduleArguments(null, false, null, null, null, null));
                            });
                          }).onError((error, stackTrace) {
                            showToastMessage(context, message: error.toString());                            
                          }).timeout(Duration(seconds: 60), onTimeout: () {
                            setState(() {
                              onlyClickOnce = 0;
                            });
                          }).whenComplete(() {
                            setState(() {
                              onlyClickOnce = 0;
                            });
                          });
                        }
                      }
                    }
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      if (widget.taskEditArgs?.taskDetail != null)
                        Padding(
                          padding: EdgeInsets.only(right: 8.0),
                          child: SvgPicture.asset(ic_task_remove, width: 24),
                        ),
                      Text(
                        widget.taskEditArgs?.taskDetail == null ? 'Cancel' : 'Remove Task',
                        style: boldFontsStyle(fontSize: 16.0, color: white, height: 1.5),
                      ),
                    ],
                  ),
                  style: widget.taskEditArgs?.taskDetail == null
                      ? OutlinedButton.styleFrom(
                          backgroundColor: darkGrey,
                          side: BorderSide(color: white),
                          padding: EdgeInsets.symmetric(vertical: 12.0),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                        )
                      : ElevatedButton.styleFrom(
                          backgroundColor: red,
                          padding: EdgeInsets.symmetric(vertical: 12.0),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                        ),
                ),
              ),
            ),
            SizedBox(width: 16.0),
            Expanded(
              child: Opacity(
                opacity: (onlyClickOnce >= 1) ? 0.5 : 1.0,
                child: ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      if (subTaskList.length == 1) {
                        earlierDateTime = subTaskList[0].targetDate;
                        latestDateTime = subTaskList[0].dueDate;
                      } else if (subTaskList.where((e) => e.targetDate != null).length > 1) {
                        List<SubTasksPostModel> tempSubTaskList = subTaskList.where((e) => e.targetDate != null).toList();
                        DateTime earlierDate = DateTime.parse(tempSubTaskList[0].targetDate.toString());
                        DateTime latestDate = DateTime.parse(tempSubTaskList[0].dueDate.toString());
              
                        for (int i = 1; i < tempSubTaskList.length; i++) {
                          bool skip = i >= (taskDetailsResponseModel?.subTaskArr?.length ?? 0) || taskDetailsResponseModel?.subTaskArr?[i].taskStatus == "completed";
                          if (!skip) {
                            if (earlierDate.isBefore(DateTime.parse(tempSubTaskList[i].targetDate.toString()))) {
                              earlierDate = earlierDate;
                            } else {
                              earlierDate = DateTime.parse(tempSubTaskList[i].targetDate.toString());
                            }
              
                            if (latestDate.isAfter(DateTime.parse(tempSubTaskList[i].dueDate.toString()))) {
                              latestDate = latestDate;
                            } else {
                              latestDate = DateTime.parse(tempSubTaskList[i].dueDate.toString());
                            }
                          }
                        }
              
                        earlierDateTime = DateFormat('yyyy-MM-dd HH:mm:ss').format(earlierDate);
                        latestDateTime = DateFormat('yyyy-MM-dd HH:mm:ss').format(latestDate);
                      } else {
                        earlierDateTime = taskInfo?.taskTargetDateTime;
                        latestDateTime = taskInfo?.taskEndDateTime;
                      }
              
                      TaskPostModel formDataUpdate = TaskPostModel(
                        taskID: taskInfo?.taskID,
                        projectID: widget.taskEditArgs!.projectID,
                        milestoneID: taskInfo?.milestoneCategoryID,
                        templateTaskID: taskInfo?.templateTaskID,
                        description: taskInfo?.taskDescription,
                        taskPaymentPercentage: taskInfo?.taskPercentage ?? '',
                        title: _taskController.text,
                        targetDate: subTaskList.length == 0 ? taskInfo?.taskTargetDateTime : earlierDateTime,
                        dueDate: subTaskList.length == 0 ? taskInfo?.taskEndDateTime : latestDateTime,
                        assignTo: teamMember.firstWhere((e) => e.teamMemberName!.compareTo(_assignController.text) == 0).teamMemberID!,
                        requirements: Requirements(fileRequired: _isSelectedUploadFiles ? 1 : 0),
                        clientAppRequirements: ClientAppRequirements(
                          autoUpdateCA: _isSelectedShownInApp ? 1 : 0,
                          imageUpdateCA: _isSelectedIncludeFilesAttached ? 1 : 0,
                        ),
                        subTasks: subTaskList,
                        oldTaskData: oldFormData,
                      );
                      TaskPostModel formData = TaskPostModel(
                        taskID: taskInfo?.taskID,
                        projectID: widget.taskEditArgs!.projectID,
                        milestoneID: taskInfo?.milestoneCategoryID,
                        templateTaskID: taskInfo?.templateTaskID,
                        description: taskInfo?.taskDescription,
                        taskPaymentPercentage: taskInfo?.taskPercentage ?? '',
                        title: _taskController.text,
                        targetDate: subTaskList.length == 0 ? taskInfo?.taskTargetDateTime : earlierDateTime,
                        dueDate: subTaskList.length == 0 ? taskInfo?.taskEndDateTime : latestDateTime,
                        assignTo: teamMember.firstWhere((e) => e.teamMemberName!.compareTo(_assignController.text) == 0).teamMemberID!,
                        requirements: Requirements(fileRequired: _isSelectedUploadFiles ? 1 : 0),
                        clientAppRequirements: ClientAppRequirements(
                          autoUpdateCA: _isSelectedShownInApp ? 1 : 0,
                          imageUpdateCA: _isSelectedIncludeFilesAttached ? 1 : 0,
                        ),
                        subTasks: subTaskList,
                      );

                      if (widget.taskEditArgs?.taskDetail != null) {
                        setState(() {
                          onlyClickOnce = onlyClickOnce + 1;
                        });
                        if (onlyClickOnce == 1)
                        _taskService.postTaskUpdate(formDataUpdate).then((response) {
                          //get response from taskUpdate API
                          // ignore: unused_local_variable
                          taskRescheduleArguments responseTaskUpdate = taskRescheduleArguments(response["response"], response["response"]["taskRescheduled"], response["response"]["taskList"], response["response"]["rescheduledTaskIDArr"], response["response"]["rescheduledAmount"], response["response"]["rescheduledConfig"]);
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {               
                              final dialogContext = context; // Store the dialog context to access it later
                              
                              Future.delayed(Duration(seconds: 2), () { // Delayed execution after 2 seconds
                                Navigator.of(dialogContext).pop(); // Dismiss the dialog and navigate to another page
                              });
              
                              // Return the AlertDialog widget
                              return AlertDialog(
                                backgroundColor: white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                title: SvgPicture.asset(ic_completed, width: 45),
                                content: Text(
                                  'Saved Successfully!',
                                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Colors.black),
                                  textAlign: TextAlign.center,
                                ),
                              );
                            },
                          ).then((value) {
                                Navigator.pop(context, taskRescheduleArguments(null, false, null, null, null, null));
                                goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: response['response']['taskArr']['taskID'], projectID: response['response']['taskArr']['projectID']),
                                  callback: () {
                                    widget.taskEditArgs!.milestonePagingController!.refresh();
                                  } 
                                );
                          });
                        }).onError((error, stackTrace) {
                          // if someone edited the task
                          if (error == 'task-update-conflict') {
                            showDialog(
                              barrierDismissible: false,
                              context: context,
                              builder: (context) => WillPopScope(
                                onWillPop: () async {
                                  // Dismiss the dialog when the user tries to pop it
                                  Navigator.pop(context);
                                  return false; // Prevent the dialog from being popped by the back button
                                },
                                child: AlertDialog(
                                  backgroundColor: white,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  title: SvgPicture.asset(ic_error, width: 45),
                                  content: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      SizedBox(height: 12.0),
                                      Text(
                                        'Someone has edited the task, the changes you made will not be saved.',
                                        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, height: 1.5),
                                        textAlign: TextAlign.center,
                                      ),
                                      SizedBox(height: 32.0),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: OutlinedButton (
                                              style: OutlinedButton.styleFrom(
                                                foregroundColor: black,
                                                side: BorderSide(width: 1, color: black),
                                                padding: EdgeInsets.symmetric(vertical: 12.0),
                                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                                              ),
                                              onPressed: () {
                                                Navigator.pop(context, true);
                                              },
                                              child: Text (
                                                'Back To Task Page',
                                                style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: black),
                                              ),
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  )
                                ),
                              )
                            ).then((value) {
                              setState(() {
                                _isLoading = true;
                              });
                              Navigator.pop(context, taskRescheduleArguments(null, false, null, null, null, null)); 
                            });
                          }
              
                          // if someone deleted the task
                          if (error == 'task-deleted') {
                            showDialog(
                              barrierDismissible: false,
                              context: context,
                              builder: (context) => WillPopScope(
                                onWillPop: () async {
                                  // Dismiss the dialog when the user tries to pop it
                                  Navigator.pop(context);
                                  return false; // Prevent the dialog from being popped by the back button
                                },
                                child: AlertDialog(
                                  backgroundColor: white,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  title: SvgPicture.asset(ic_error, width: 45),
                                  content: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      SizedBox(height: 12.0),
                                      Text(
                                        'Opps! Looks like someone has removed this task, the changes you made will not be saved.',
                                        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, height: 1.5),
                                        textAlign: TextAlign.center,
                                      ),
                                      SizedBox(height: 32.0),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: OutlinedButton (
                                              style: OutlinedButton.styleFrom(
                                                foregroundColor: black,
                                                side: BorderSide(width: 1, color: black),
                                                padding: EdgeInsets.symmetric(vertical: 12.0),
                                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                                              ),
                                              onPressed: () {
                                                Navigator.pop(context);
                                              },
                                              child: Text (
                                                'Back To Project Task List',
                                                style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: black),
                                              ),
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  )
                                ),
                              )
                            ).then((value) {
                              Navigator.pop(context, taskRescheduleArguments(null, false, null, null, null, null)); 
                              Navigator.pop(context, taskRescheduleArguments(null, false, null, null, null, null)); 
                            });
                          }
                        }).timeout(Duration(seconds: 60), onTimeout: () {
                          setState(() {
                            onlyClickOnce = 0;
                          });
                        }).whenComplete(() {
                          onlyClickOnce = 0;
                        });
                      } else {
                        setState(() {
                          onlyClickOnce = onlyClickOnce + 1;
                        });
                        if (onlyClickOnce == 1)
                        _taskService.postTaskCreate(formData).then((response) {
                          // ignore: unused_local_variable
                          taskRescheduleArguments responseTaskUpdate = taskRescheduleArguments(response["response"], response["response"]["taskRescheduled"], response["response"]["taskList"], response["response"]["rescheduledTaskIDArr"], response["response"]["rescheduledAmount"], response["response"]["rescheduledConfig"]);
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              // Store the dialog context to access it later
                              final dialogContext = context;
              
                              // Delayed execution after 2 seconds
                              Future.delayed(Duration(seconds: 2), () {
                                // Dismiss the dialog and navigate to another page
                                Navigator.of(dialogContext).pop();
                              });
                              return AlertDialog(
                                backgroundColor: white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                title: SvgPicture.asset(ic_completed, width: 45),
                                content: Text(
                                  'Added Successfully!',
                                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: black),
                                  textAlign: TextAlign.center,
                                ),
                              );
                            }
                          ).then((value) {
                             goToReplacementNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: response['response']['taskArr']['taskID'], projectID: response['response']['taskArr']['projectID']),
                              callback: () {
                                widget.taskEditArgs!.milestonePagingController!.refresh();
                              }  
                            );                    
                          });
                        }).onError((error, stackTrace) {
                          showToastMessage(context, message: error.toString());
                        }).timeout(Duration(seconds: 60), onTimeout: () {
                          setState(() {
                            onlyClickOnce = 0;
                          });
                        }).whenComplete(() {
                          onlyClickOnce = 0;
                        });
                      }
                    }
                  },
                  child: Text(
                    widget.taskEditArgs?.taskDetail == null ? 'Submit' : 'Save Task',
                    style: boldFontsStyle(fontSize: 16.0, height: 1.5),
                  ),
                  style: ElevatedButton.styleFrom(
                    backgroundColor: yellow,
                    padding: EdgeInsets.symmetric(vertical: 12.0),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                  ),
                ),
              ),
            )
          ],
        ));
  }

  void _onEditingComplete() {
      int? teamMemberID = teamMember.firstWhere((e) => e.teamMemberName!.compareTo(_assignController.text) == 0, orElse: () => TeamMemberDetails()).teamMemberID;
      if (teamMemberID == null) {
        _assignController.clear();
      }else{
        FocusScope.of(context).unfocus();
      }
  }

  getSpecialTaskOptions(String pattern) {
    List<SpecialTaskDetails> matches = [];
    // matches.add(SpecialTaskDetails(
    //   templateTaskID: 2,
    //   milestoneCategoryID: 22,
    //   specialTaskOrder: 2,
    //   templateTaskName: "Product Orders & Purchase Round 1",
    //   templateTaskDescription: "Designer’s part",
    //   teamMemberID: 269,
    //   requirements: Requirements(
    //     fileRequired: 1
    //   ),
    //   clientAppRequirements: ClientAppRequirements(
    //     autoUpdateCA: 1,
    //     imageUpdateCA: 0
    //   ),
    //   disabled: (widget.taskEditArgs?.taskDetail?.completedSubTask ?? 0) == 0 ? false : true,
    //   enabledSubtask: false,
    //   defaultSubtask: [
    //     SubTasksPostModel(
    //       taskID: 0,
    //       title: "Orders", 
    //       description: null,
    //       targetDate: taskInfo?.taskTargetDateTime,
    //       dueDate: taskInfo?.taskEndDateTime,
    //     )
    //   ],
    // ));
    matches.addAll(specialTaskList);

    matches.retainWhere((e) => e.templateTaskName!.toLowerCase().contains(pattern.toLowerCase()));
    matches.insert(0, SpecialTaskDetails(
      templateTaskID: 0,
      templateTaskName: _taskController.text,
    ));
    return matches;
  }
  
  getTeamMemberOptions(String pattern) {
    List<TeamMemberDetails> matches = [];
    matches.addAll(teamMember);

    matches.retainWhere((e) => e.teamMemberName!.toLowerCase().contains(pattern.toLowerCase()));
    return matches;
  }
}

class taskRescheduleArguments {
  Map? response;
  bool? taskRescheduled;
  List? taskList;
  List? rescheduledTaskIDArr;
  int? rescheduledAmount;
  String? rescheduledConfig;

  taskRescheduleArguments(this.response, this.taskRescheduled, this.taskList, this.rescheduledTaskIDArr, this.rescheduledAmount, this.rescheduledConfig);
}
