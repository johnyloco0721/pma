import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:percent_indicator/percent_indicator.dart';
// import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:sources/args/task_args.dart';
import 'package:sources/args/task_search_args.dart';
import 'package:sources/constants/app_box_shadow.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_constants.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/models/task_item_model.dart';
import 'package:sources/models/task_list_model.dart';
import 'package:sources/models/task_project_item_model.dart';
import 'package:sources/services/task_services.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/scroll.utils.dart';
import 'package:sources/utils/size_utils.dart';
import 'package:sources/utils/status_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/widgets/task_label_widget.dart';
import '../../models/reddot_list_model.dart';
import '../../widgets/calendar_widget.dart';

class TaskMainView extends StatefulWidget {
  final TaskArguments? taskArguments;
  const TaskMainView({Key? key, this.taskArguments}) : super(key: key);

  @override
  _TaskMainViewState createState() => _TaskMainViewState();
}

class _TaskMainViewState extends State<TaskMainView> with SingleTickerProviderStateMixin {
  int projectItemCount = 0;
  int taskItemCount = 0;
  String projectCurrentFliterValue = "Incomplete";
  String taskCurrentFliterValue = "Incomplete";
  bool isShowTopTabView = true;
  TabController? _tabController;
  ScrollController projectScrollController = ScrollController();
  ScrollController taskScrollController = ScrollController();
  TaskService _taskService = TaskService();
  double previousScrollPosition = 0.0;

  final PagingController<int, TaskProjectItemModel> _projectPagingController = PagingController(firstPageKey: 1);
  final PagingController<int, TaskItemModel> _taskPagingController = PagingController(firstPageKey: 1);

  bool showCalendar = false;
  bool calendarFilterActive = false;
  List<DateTime> hasProjectTask = [];
  String selectedView = 'ProjectView';
  DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss.SSS'Z'");
  String filterDateTime = "";
  // PanelController panelController = PanelController();

  bool redirected = false;
  ValueNotifier<bool> projectRequesting = ValueNotifier(false);
  ValueNotifier<bool> taskRequesting = ValueNotifier(false);

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);

    _projectPagingController.addPageRequestListener((pageKey) {
      if (!projectRequesting.value)
        _fetchMyTaskProjectPage(pageKey);
    });
    _taskPagingController.addPageRequestListener((pageKey) {
      if (!taskRequesting.value)
        _fetchMyTaskPage(pageKey);
    });

    projectScrollController.addListener(() => scrollListener(projectScrollController));
    taskScrollController.addListener(() => scrollListener(taskScrollController));

    _tabController?.addListener(() {
      if (_tabController?.index == 0) _projectPagingController.refresh();
      if (_tabController?.index == 1) _taskPagingController.refresh();
    });

    _fetchRedDotList();
  }

  Future<void> _fetchMyTaskProjectPage(int pageKey) async {
    try {
      projectRequesting.value = true;
      await _taskService.fetchMyTaskProjectList(pageKey: pageKey, filterBy: projectCurrentFliterValue.toLowerCase(),date:filterDateTime).then((response) {
        TaskListModel taskProjectListResponse = response;
        List<TaskProjectItemModel>? projectList = taskProjectListResponse.response?.map((e) => TaskProjectItemModel.fromJson(e)).toList();
        final isLastPage = (projectList?.length ?? 0) < (taskProjectListResponse.limitPerPage ?? 50);
        setState(() {
          projectItemCount = taskProjectListResponse.totalItems ?? 0;
        });
        if (isLastPage) {
          _projectPagingController.appendLastPage(projectList ?? []);
        } else {
          final nextPageKey = pageKey + 1;
          _projectPagingController.appendPage(projectList ?? [], nextPageKey);
        }
        projectRequesting.value = false;
      });
    } catch (error) {
      _projectPagingController.error = error;
      projectRequesting.value = false;
    }

    // Redirect to next route once widget built completed
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (widget.taskArguments?.projectID != null && !redirected) {
        redirected = true;
        goToNextNamedRoute(context, taskMilestoneScreen, args: widget.taskArguments, callback: () {
          _taskPagingController.refresh();
          _projectPagingController.refresh();
        });
      }
    });
  }

  Future<void> _fetchMyTaskPage(int pageKey) async {
    try {
      taskRequesting.value = true;
      await _taskService.fetchMyTaskList(pageKey: pageKey, filterBy: taskCurrentFliterValue.toLowerCase(),date:filterDateTime).then((response) {
        TaskListModel taskListResponse = response;
        List<TaskItemModel>? taskList = taskListResponse.response?.map((e) => TaskItemModel.fromJson(e)).toList();
        final isLastPage = (taskList?.length ?? 0) < (taskListResponse.limitPerPage ?? 50);
        setState(() {
          taskItemCount = taskListResponse.totalItems ?? 0;
        });
        if (isLastPage) {
          _taskPagingController.appendLastPage(taskList ?? []);
        } else {
          final nextPageKey = pageKey + 1;
          _taskPagingController.appendPage(taskList ?? [], nextPageKey);
        }
        taskRequesting.value = false;
      });
    } catch (error) {
      _taskPagingController.error = error;
      taskRequesting.value = false;
    }
  }

    Future<void> _fetchRedDotList() async {
    try {
      await _taskService.fetchRedDotList().then((response) {
        RedDotListModel redDotList = response;
        RedDotListModelResponse redDotListResponse = redDotList.response!;
        List<String>? redDotDateTimeList = redDotListResponse.taskDateTime;
        setState(() {
          hasProjectTask.clear();
          for (var e in redDotDateTimeList ?? []) {
            DateTime dt = DateTime.parse(e);
            String getDate = dateFormat.format(dt);
            DateTime dt2 = DateTime.parse(getDate);
            hasProjectTask.add(dt2);
          };
        });
      });
    } catch (error) {
      _taskPagingController.error = error;
    }
  }

  @override
  void dispose() {
    super.dispose();
    _tabController?.dispose();
    _projectPagingController.dispose();
    _taskPagingController.dispose();
    projectScrollController.removeListener(() => scrollListener(projectScrollController));
    projectScrollController.dispose();
    taskScrollController.removeListener(() => scrollListener(taskScrollController));
    taskScrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: PageStorageKey<String>('TaskMain'),
      backgroundColor: white,
      appBar: _buildAppBar(context),
      body: WillPopScope(
        onWillPop: () async => false,
        child: _buildTaskMainView(context)
      ),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.dark,
      title: Text(
        title_task_page,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: boldFontsStyle(fontFamily: platForm, fontSize: 35.0, letterSpacing: 0.5, color: showCalendar ? white : black),
      ),
      flexibleSpace: GestureDetector(
        onTap: () {
          // onTap app bar empty space then reset list scroll position
          scrollToTop(projectScrollController);
          scrollToTop(taskScrollController);
        },
      ),
      actions: [
        Row(
          children: [
            // Calendar
            GestureDetector(
              onTap: () {
                // show calander
                setState(() {
                  showCalendar = !showCalendar;
                });
              },
             child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                child: showCalendar 
                ? calendarFilterActive ? SvgPicture.asset(ic_calendar_yellow_active, width: 20.0) : SvgPicture.asset(ic_calendar_yellow, width: 20.0)
                : calendarFilterActive ? SvgPicture.asset(ic_calendar_active, width: 20.0) : SvgPicture.asset(ic_calendar, width: 20.0),
              ), 
            ),
            // Task Search
            GestureDetector(
              onTap: () {
                // search
                goToNextNamedRoute(context, taskSearchScreen, args: TaskSearchArguments(_tabController?.index, filterDateTime), callback: () {
                  _taskPagingController.refresh();
                  _projectPagingController.refresh();
                });
              },
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                child: SvgPicture.asset(ic_search, width: 20.0, color: showCalendar ? white : black),
              ),
            ),
            SizedBox(width: 10.0),
          ],
        ),
      ],
      backgroundColor: showCalendar ? darkGrey : white,
      centerTitle: false,
      automaticallyImplyLeading: false,
      elevation: 0,
    );
  }

  Widget _buildTaskMainView(BuildContext context) {
    return Column(
      children: [
        if (showCalendar)
          CustomTableCalender(
            calendarTopPadding: 263.0,
            hasProjectTask: hasProjectTask,
            dateFilter: (value) { 
              setState(() {
                calendarFilterActive = value != '';
                filterDateTime = value;
                _projectPagingController.refresh();
                _taskPagingController.refresh();
              });
            },
            dateSelected: filterDateTime
          ),
        // Tab bar
        AnimatedContainer(
          duration: Duration(milliseconds: 150),
          margin: EdgeInsets.symmetric(vertical: isShowTopTabView ? 16.0 : 0.0, horizontal: 24),
          height: isShowTopTabView ? 46.0 : 0.0,
          child: Material(
            color: whiteGrey,
            borderRadius: BorderRadius.circular(12.0),
            child: TabBar(
              controller: _tabController,
              padding: EdgeInsets.all(6.0),
              indicator: BoxDecoration(color: white, border: Border.all(color: shimmerGrey), borderRadius: BorderRadius.circular(6.0), boxShadow: app_box_shadow),
              labelColor: darkYellow,
              labelStyle: boldFontsStyle(fontSize: 14.0, height: 1.35),
              indicatorSize: TabBarIndicatorSize.tab,
              unselectedLabelColor: grey,
              unselectedLabelStyle: regularFontsStyle(fontSize: 14.0, height: 1.35),
              indicatorColor: transparent,
              dividerColor: transparent,
              labelPadding: EdgeInsets.all(8.0),
              tabs: [
                Text('Project View'),
                Text('Task View'),
              ],
            ),
          ),
        ),
        // Tab content view
        Expanded(
          child: TabBarView(
            controller: _tabController,
            children: [
              RefreshIndicator(
                color: yellow,
                backgroundColor: white,
                onRefresh: () async {
                  _projectPagingController.refresh();
                },
                child: projectViewLayout(),
              ),
              RefreshIndicator(
                color: yellow,
                backgroundColor: white,
                onRefresh: () async {
                  _taskPagingController.refresh();
                },
                child: taskViewLayout(),
              ),
            ],
          ),
        )
      ],
    );
  }

  // project view tab content
  Widget projectViewLayout() {
    return Column(
      children: [
        // filter layout
        filterLayout(projectCurrentFliterValue, 'Project'),
        // filtered item listing layout
        Expanded(
          flex: 20,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 24.0),
            child: PagedListView(
              physics: AlwaysScrollableScrollPhysics(),
              scrollController: projectScrollController,
              pagingController: _projectPagingController,
              builderDelegate: PagedChildBuilderDelegate<TaskProjectItemModel>(
                firstPageProgressIndicatorBuilder: (context) => Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Lottie.asset(
                      lottie_loading_spinner,
                      width: 50.0,
                      height: 50.0,
                      fit: BoxFit.fill,
                    ),
                    SizedBox(height: 16.0),
                    Text(
                      'just a few secs...',
                      style: regularFontsStyle(fontSize: 14.0, color: grey),
                    ),
                  ],
                ),
                newPageProgressIndicatorBuilder: (context) => Container(
                  padding: EdgeInsets.all(8.0),
                  child: Center(
                    child: Lottie.asset(
                      lottie_loading_spinner,
                      width: 50.0,
                      height: 50.0,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                noMoreItemsIndicatorBuilder: (context) => Container(
                  padding: EdgeInsets.all(8.0),
                  child: Center(
                      child: Text(
                    'You’ve reach the end of the list.',
                    style: regularFontsStyle(fontSize: 12.0, height: 1.33, color: grey),
                  )),
                ),
                noItemsFoundIndicatorBuilder: (context) => Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      child: Lottie.asset(
                        lottie_empty_docs,
                        width: 250.0,
                        height: 250.0,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Container(
                      width: 250.0,
                      child: Text(
                        text_empty_task,
                        style: regularFontsStyle(fontSize: 17.0, height: 1.35),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
                itemBuilder: (context, project, index) {
                  Status _projectStatus = statusConfig(project.taskStatus ?? '');
                  // item layout
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // total item count
                      if (index == 0)
                        Container(
                          padding: EdgeInsets.only(top: 16.0),
                          child: Text(
                            '$projectItemCount projects found.',
                            style: italicFontsStyle(fontSize: 14.0, color: grey),
                          ),
                        ),
                      GestureDetector(
                        onTap: () {
                          showCalendar = false;
                          // go to Task Project page
                          goToNextNamedRoute(context, taskMilestoneScreen, args: TaskArguments(projectID: project.projectID ?? 0), callback: () {
                            _taskPagingController.refresh();
                            _projectPagingController.refresh();
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.all(16.0),
                          margin: EdgeInsets.only(top: index == 0 ? 8.0 : 16.0, bottom: 0.0),
                          decoration: BoxDecoration(
                            color: _projectStatus.secondaryColor,
                            border: Border.all(color: _projectStatus.primaryColor),
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              // item's leading layout
                              Expanded(
                                flex: 2,
                                child: project.taskStatus != 'completed'
                                  ? CircularPercentIndicator(
                                    radius: 20.0,
                                    lineWidth: 8.0,
                                    percent: (project.completedTasks ?? 0) / (project.totalTasks ?? 0),
                                    progressColor: yellow,
                                    backgroundColor: grey,
                                    animation: true,
                                    animationDuration: 1000,
                                  )
                                  : project.taskStatus == 'flagged'
                                    ? SvgPicture.asset(ic_task_flag, width: 36.0, color: red)
                                    : SvgPicture.asset(ic_completed, width: 36.0, color: green),
                              ),
                              SizedBox(width: 12.0),
                              // item's content layout
                              Expanded(
                                flex: 13,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    // project title
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                            project.propertyName ?? '',
                                            style: boldFontsStyle(fontSize: 16.0, color: black),
                                          ),
                                        ),
                                        // item's trailing layout
                                        SvgPicture.asset(
                                          ic_task_right_arrow,
                                          width: 20.0,
                                          color: black,
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 4.0),
                                    // project createdBy
                                    Row(
                                      children: [
                                        SvgPicture.asset(ic_people, color: grey, width: 18.0),
                                        SizedBox(width: 8.0),
                                        Text(
                                          project.ownerName ?? '',
                                          style: regularFontsStyle(fontSize: 14.0, color: grey),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 8.0),
                                    // stage
                                    Row(
                                      children: [
                                        SvgPicture.asset(ic_stage, color: grey, width: 18.0),
                                        SizedBox(width: 8.0),
                                        Text(
                                          project.stage?.label ?? "",
                                          style: regularFontsStyle(fontSize: 14.0, color: grey),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 8.0),
                                    // project progress
                                    RichText(
                                      text: TextSpan(children: [
                                        TextSpan(
                                          text: '${project.completedTasks} / ${project.totalTasks}',
                                          style: boldFontsStyle(fontSize: 14.0, color: black),
                                        ),
                                        TextSpan(
                                          text: ' tasks completed',
                                          style: regularFontsStyle(fontSize: 14.0, color: black),
                                        )
                                      ]),
                                    ),
                                    SizedBox(height: 8.0),
                                    // project due date
                                    if (project.taskStatus != 'completed')
                                    Row(
                                      children: [
                                        // due date label
                                        Text(
                                          'Upcoming Date:',
                                          style: boldFontsStyle(fontSize: 12.0, color: grey),
                                        ),
                                        SizedBox(width: 8.0),
                                        // status icon
                                        _projectStatus.icon,
                                        SizedBox(width: 8.0),
                                        // due date, (outline layout)
                                        project.taskTargetDateTime == null
                                        ? Container(
                                          padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                                          decoration: BoxDecoration(color: transparent, border: Border.all(color: red), borderRadius: BorderRadius.circular(500.0)),
                                          child: Text(
                                            'No Date',
                                            style: italicFontsStyle(color: red, fontSize: 12.0, height: 1.33),
                                          ),
                                        )
                                        : Container(
                                          padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                                          decoration: BoxDecoration(color: transparent, border: Border.all(color: _projectStatus.primaryColor), borderRadius: BorderRadius.circular(500.0)),
                                          child: Text(
                                            formatDatetime(datetime: project.taskTargetDateTime ?? ''),
                                            style: boldFontsStyle(color: _projectStatus.textColor, fontSize: 12.0, height: 1.33),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
        )
      ],
    );
  }

  // task view tab content
  Widget taskViewLayout() {
    return Column(
      children: [
        // filter layout
        filterLayout(taskCurrentFliterValue, 'Task'),
        // filtered item listing layout
        Expanded(
          flex: 20,
          child: PagedListView.separated(
            physics: AlwaysScrollableScrollPhysics(),
            scrollController: taskScrollController,
            pagingController: _taskPagingController,
            separatorBuilder: (context, index) => Container(),
            builderDelegate: PagedChildBuilderDelegate<TaskItemModel>(
            firstPageProgressIndicatorBuilder: (context) => Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Lottie.asset(
                  lottie_loading_spinner,
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.fill,
                ),
                SizedBox(height: 16.0),
                Text(
                  'just a few secs...',
                  style: regularFontsStyle(fontSize: 14.0, color: grey),
                ),
              ],
            ),
            newPageProgressIndicatorBuilder: (context) => Container(
              padding: EdgeInsets.all(8.0),
              child: Center(
                child: Lottie.asset(
                  lottie_loading_spinner,
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.fill,
                ),
              ),
            ),
            noMoreItemsIndicatorBuilder: (context) => Container(
              padding: EdgeInsets.all(8.0),
              child: Center(
                  child: Text(
                'You’ve reach the end of the list.',
                style: regularFontsStyle(fontSize: 12.0, height: 1.33, color: grey),
              )),
            ),
            noItemsFoundIndicatorBuilder: (context) => Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: Lottie.asset(
                    lottie_empty_docs,
                    width: 250.0,
                    height: 250.0,
                    fit: BoxFit.fill,
                  ),
                ),
                Container(
                  width: 250.0,
                  child: Text(
                    text_empty_task,
                    style: regularFontsStyle(fontSize: 17.0, height: 1.35),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
            itemBuilder: (context, task, index) {
              Status _taskStatus = statusConfig(task.taskStatus ?? '', iconWidth: 24.0);
              // item layout
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // total item count
                  if (index == 0)
                    Container(
                      padding: EdgeInsets.only(top: 16.0, left: 24.0, right: 24.0, bottom: 8.0),
                      child: Text(
                        '$taskItemCount tasks found.',
                        style: italicFontsStyle(fontSize: 14.0, color: grey),
                      ),
                    ),
                  GestureDetector(
                    onTap: () {
                      showCalendar = false;
                      // go to Task Details page
                      goToNextNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: task.taskID), callback: () {
                        _taskPagingController.refresh();
                        _projectPagingController.refresh();
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.all(24.0),
                      decoration: BoxDecoration(
                        color: _taskStatus.secondaryColor,
                        border: Border(
                          top: index == 0 ? BorderSide(color: shimmerGrey, width: 1.0) : BorderSide.none,
                          bottom: BorderSide(color: shimmerGrey, width: 1.0),
                        )),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          // item's leading layout
                          _taskStatus.icon,
                          SizedBox(width: 12.0),
                          // item's content layout
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                // task due date, (outline layout)
                                Container(
                                  padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                                  decoration: BoxDecoration(color: task.taskStatus == 'completed' ? shimmerGrey : transparent, border: Border.all(color: task.taskStatus == 'completed' ? shimmerGrey : task.taskTargetDateTime != null ? _taskStatus.primaryColor : red), borderRadius: BorderRadius.circular(500)),
                                  child: RichText(
                                      text: TextSpan(children: [
                                    if (task.taskStatus == 'completed') TextSpan(text: "Completed: ", style: regularFontsStyle(color: grey, fontSize: 12.0, height: 1.33)),
                                    if (task.taskStatus == 'completed') TextSpan(text: formatDatetime(datetime: task.completedDateTime ?? ''), style: boldFontsStyle(color: task.taskStatus == 'completed' ? grey : _taskStatus.textColor, fontSize: 12.0, height: 1.33)),
                                    if (task.taskStatus != 'completed') TextSpan(
                                      text: task.taskTargetDateTime != null ? formatDatetime(datetime: task.taskTargetDateTime ?? '') :  'No Due Date', 
                                      style: task.taskTargetDateTime != null ? boldFontsStyle(color: _taskStatus.textColor, fontSize: 12.0, height: 1.33) : italicFontsStyle(color: red, fontSize: 12.0, height: 1.33)
                                    ),
                                  ])),
                                ),
                                SizedBox(height: 8.0),
                                // task title
                                RichText(
                                  text: TextSpan(
                                    children: [
                                      if (task.isSpecial ?? false)
                                        WidgetSpan(
                                          child: Padding(
                                            padding: EdgeInsets.only(right: 8.0),
                                            child: SvgPicture.asset(ic_star, width: 20.0),
                                          ),
                                        ),
                                      TextSpan(
                                        text: task.taskName ?? '',
                                        style: boldFontsStyle(fontSize: 16.0, height: 1.4),
                                      ),
                                    ]
                                  ),
                                ),
                                SizedBox(height: 8.0),
                                // task project name
                                Text(
                                  task.propertyName ?? '',
                                  style: regularFontsStyle(fontSize: 14.0, color: grey),
                                ),
                                SizedBox(height: 8.0),
                                // task label
                                TaskLabels(
                                  isShowSubtaskLabel: (task.totalSubTask ?? 0) > 0,
                                  totalSubTask: task.totalSubTask ?? 0,
                                  completedSubTask: task.completedSubTask ?? 0,
                                  isShowFileRequireLabel: (task.isFileRequired ?? 0) == 1,
                                  isFileRequiredLabel: task.isFileRequiredLabel ?? '',
                                  replyCount: task.replyCount ?? 0,
                                  taskID: task.taskID!,
                                  isRead: task.isRead ?? '',
                                ),
                                SizedBox(width: 8.0)
                              ],
                            ),
                          ),
                          // item's trailing layout
                          SvgPicture.asset(ic_task_right_arrow, width: 20.0, color: _taskStatus.primaryColor),
                        ],
                      ),
                    ),
                  ),
                ],
              );
            }),
          ),
        )
      ],
    );
  }

  // filter layout
  Widget filterLayout(String currentFilter, String viewName) {
    return Container(
      height: 36.0,
      padding: EdgeInsets.symmetric(horizontal: 24.0),
      decoration: BoxDecoration(border: Border(bottom: BorderSide(width: 1.0, color: shimmerGrey))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          filterItemLayout('All', currentFilter == 'All', viewName),
          filterItemLayout('Incomplete', currentFilter == 'Incomplete', viewName),
          filterItemLayout('Completed', currentFilter == 'Completed', viewName),
        ],
      ),
    );
  }

  // filter item layout
  Widget filterItemLayout(String label, bool selected, String viewName) => Expanded(
    child: GestureDetector(
      onTap: () {
        setState(() {
          if (viewName == "Project") {
            projectCurrentFliterValue = label;
            _projectPagingController.refresh();
          } else {
            taskCurrentFliterValue = label;
            _taskPagingController.refresh();
          }
        });
      },
      child: Container(
        padding: EdgeInsets.only(bottom: 8.0),
        decoration: BoxDecoration(border: Border(bottom: BorderSide(width: selected ? 2.0 : 0.0, color: selected ? darkYellow : transparent))),
        child: Center(
            child: Text(
          label,
          style: selected ? boldFontsStyle(fontSize: 14.0, color: black) : regularFontsStyle(fontSize: 14.0, color: grey),
        )),
      ),
    ),
  );

  // content scroll listener
  scrollListener(ScrollController controller) {
    if (controller.offset >= previousScrollPosition && isShowTopTabView == true && controller.offset >= (controller.position.minScrollExtent + SizeUtils.height(context, 5))) {
      setState(() {
        if (!((_tabController?.index == 0 && (_projectPagingController.itemList?.length ?? 0) <= 3) || (_tabController?.index == 1 && (_taskPagingController.itemList?.length ?? 0) <= 3)))
          isShowTopTabView = false;
      });
    }

    if (controller.offset < previousScrollPosition && isShowTopTabView == false && controller.offset < controller.position.maxScrollExtent - SizeUtils.height(context, 5)) {
      setState(() {
        isShowTopTabView = true;
      });
    }
    previousScrollPosition = controller.offset;
  }
}
