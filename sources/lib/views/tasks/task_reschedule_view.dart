import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:sources/args/delay_args.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/models/task_delay_model.dart';
import 'package:sources/models/task_live_preview_post_model.dart';
import 'package:sources/models/task_milestone_model.dart';
import 'package:sources/models/task_reschedule_model.dart';
import 'package:sources/services/task_services.dart';
import 'package:sources/utils/debouncer_utlis.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/scroll.utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/widgets/milestone_widget.dart';

class TaskRescheduleView extends StatefulWidget {
  final LivePreviewPostModel data;
  const TaskRescheduleView({Key? key, required this.data}) : super(key: key);

  @override
  _TaskRescheduleViewState createState() => _TaskRescheduleViewState();
}

class _TaskRescheduleViewState extends State<TaskRescheduleView> {
  TaskService _taskService = TaskService();
  
  ScrollController _scrollController = ScrollController();
  PagingController<int, StageDetail> _milestonePagingController = PagingController(firstPageKey: 1);

  TaskRescheduleModel? taskPreviewResponse;

  // for rescheduled function
  bool isLoading = false;
  int delay = 0;
  int delayDateIndex = 0;
  List<TaskMilestoneDetail>? taskList;
  Debouncer _debouncer = Debouncer(milliseconds: 500);

  @override
  void initState() {
    super.initState();
    delay = widget.data.delayDuration ?? 0;
    _milestonePagingController.addPageRequestListener((pageKey) {
      _fetchMyTaskListByMilestonePage(pageKey);
    });
  }

  Future<void> _fetchMyTaskListByMilestonePage(int pageKey) async {
    try {
      widget.data.delayDuration = delay;

      await _taskService.postLivePreviewReschedule(widget.data).then((response) {
        taskPreviewResponse = response;
        taskList = taskPreviewResponse?.response?.taskList;
        delay = taskPreviewResponse?.response?.rescheduledAmount ?? widget.data.delayDuration ?? 0;

        setState(() {
          delayDateIndex = 0;
          isLoading = true;
        });

        taskList?.forEach((project) {
          project.isExpanded = project.milestoneStatus != 'completed';
        });
        // _milestonePagingController.appendLastPage(taskList ?? []);
      });
    } catch (error) {
      _milestonePagingController.error = error;
    }

    setState(() {
      isLoading = false;
    });
  }

  void setLoadingState(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
    _milestonePagingController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: PageStorageKey<String>('TaskReschedule'),
      backgroundColor: shimmerGrey,
      appBar: _buildAppBar(context),
      body: RefreshIndicator(
        color: yellow,
        backgroundColor: white,
        onRefresh: () async {
          _milestonePagingController.refresh();
        },
        child: _buildTaskProjectView(context),
      ),
      bottomNavigationBar: _buildBottomNavigationBar(context),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.light,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: SvgPicture.asset(ic_chevron_left, width: 10.0),
      ),
      title: Text(
        'My Task for ' + (''),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, color: white, letterSpacing: 0.8),
      ),
      flexibleSpace: GestureDetector(
        onTap: () {
          // onTap app bar empty space then reset list scroll position
          scrollToTop(_scrollController);
        },
      ),
      centerTitle: false,
      backgroundColor: black,
      bottom: appBarBottom(),
    );
  }

  // app bar bottom widget
  PreferredSize appBarBottom() {
    return PreferredSize(
      child: Container(
        color: white,
        height: 90.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Text(
                    "Here’s a preview of delay by ",
                    style: boldFontsStyle(fontSize: 14.0),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 99.0,
                  child: Row(
                    children: [
                      Expanded(
                        child: TextButton(
                          onPressed: isLoading ? (){} : () {
                            if (delay > 1) {
                              setState(() {
                                delay--;
                                if (delayDateIndex <= 0) {
                                  _debouncer.run(() {
                                    _milestonePagingController.refresh();
                                  });
                                } else {
                                  delayDateIndex--;
                                }
                              });
                            }
                          },
                          child: Text("-"),
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                            textStyle: boldFontsStyle(fontSize: 16.0,),
                            backgroundColor: grey,
                            side: BorderSide(color: lightGrey),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.horizontal(left: Radius.circular(12.0)),
                            ), // foreground
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border(
                              top: BorderSide(color: lightGrey),
                              bottom: BorderSide(color: lightGrey),
                            )
                          ),
                          child: TextField(
                            controller: TextEditingController()..text = "$delay",
                            textAlign: TextAlign.center,
                            style: boldFontsStyle(fontSize: 16.0),
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.symmetric(vertical: 6.5, horizontal: 0.0),
                              isDense: true,
                            ),
                            keyboardType: TextInputType.number,
                            inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly], // Only numbers can be entered
                            onChanged: (text) {
                              if (text != '-') {
                                _debouncer.run(() {
                                  setState(() {
                                    try {
                                      delay = int.parse(text);
                                    } catch(e) {
                                      delay = 1;
                                    }
                                    _milestonePagingController.refresh();
                                  });
                                });
                              }
                            },
                          ),
                        ),
                      ),
                      Expanded(
                        child: TextButton(
                          onPressed: isLoading ? (){} : () {
                            setState(() {
                              delay++;
                              if (delayDateIndex >= 4) {
                                _debouncer.run(() {
                                  _milestonePagingController.refresh();
                                });
                              } else {
                                delayDateIndex++;
                              }
                            });
                          },
                          child: Text('+'),
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                            textStyle: boldFontsStyle(fontSize: 14.0),
                            backgroundColor: grey,
                            side: BorderSide(color: lightGrey),
                            shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.horizontal(right: Radius.circular(12.0)),
                            ), // foreground
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Text(
                    "days",
                    style: boldFontsStyle(fontSize: 14.0),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
      preferredSize: Size.fromHeight(90.0),
    );
  }


  // build button submit button
  Widget _buildBottomNavigationBar(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(24.0),
      decoration: BoxDecoration(
        color: darkGrey,
        borderRadius: BorderRadius.vertical(top: Radius.circular(30.0)),
      ),
      child: Row(
        children: [
          Expanded(
            child: OutlinedButton(
              onPressed: () {
                setState(() {
                  delay = 0;
                });
                _milestonePagingController.refresh();
              },
              child: Text(
                'Cancel',
                style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: white),
              ),
              style: OutlinedButton.styleFrom(
                backgroundColor: transparent,
                padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
                side: BorderSide(color: white),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
              ),
            ),
          ),
          SizedBox(width: 16.0),
          Expanded(
            child: ElevatedButton(
              onPressed: () async {
                taskList?.forEach((milestone) {
                  milestone.tasks?.forEach((task) {
                    if (task.taskStartDateTime != null)
                      task.taskStartDateTime = task.taskStartDateTimeArr?[delayDateIndex];
                    task.taskTargetDateTime = task.taskTargetDateTimeArr?[delayDateIndex];
                    task.taskEndDateTime = task.taskEndDateTimeArr?[delayDateIndex];
                  });
                });

                //call delay API
                TaskDelayModel data = TaskDelayModel(projectID: widget.data.projectID, taskIDs: widget.data.taskIDs, delayDuration: delay, taskList: taskList);
                await _taskService.postTaskDelay(data).then((response) {
                  showToastMessage(context, message: 'Update Successfully');
                  if (response["status"] == true) {
                    Navigator.pop(context);
                  } else {
                    _rescheduledDialog();
                  }
                  _milestonePagingController.refresh();
                }).onError((error, stackTrace) {
                  showToastMessage(context, message: error.toString());
                });
              },
              child: Text(
                'Confirm',
                style: boldFontsStyle(fontSize: 16.0, height: 1.5),
              ),
              style: ElevatedButton.styleFrom(
                backgroundColor: yellow,
                padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
              ),
            ),
          ),
        ],
      ),
    );
  }

  // task project view
  Widget _buildTaskProjectView(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: MilestoneWidget(
            scrollController: _scrollController, 
            milestonePagingController: _milestonePagingController,
            livePreviewArgs: LivePreviewArguments(
              delay: delay, 
              delayDateIndex: delayDateIndex,
            ),
            setLoadingState: setLoadingState,
          ),
        ),
      ],
    );
  }

  _rescheduledDialog() async {
    showDialog(
      context: this.context,
      builder: (BuildContext context) => AlertDialog(
        backgroundColor: white,
        title: Stack(
          children: [
            Positioned(
              child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                  color: transparent,
                  splashColor: transparent,
                  focusColor: transparent,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: SvgPicture.asset(ic_close, width: 20.0, color: black),
                ),
              ],
            ))
          ],
        ),
        titlePadding: EdgeInsets.fromLTRB(5.0, 10.0, 10.0, 0.0),
        content: Text('There is new update on task date. Please reschedule again.', textAlign: TextAlign.center, style: boldFontsStyle(fontSize: 16.0)),
        contentPadding: EdgeInsets.fromLTRB(26.0, 0, 35.0, 0),
        actions: <Widget>[
          ListTile(
            title: Row(
              children: [
                Expanded(
                  flex: 1,
                  child: TextButton(
                    style: TextButton.styleFrom(
                      padding: EdgeInsets.symmetric(vertical: 12.0),
                      textStyle: boldFontsStyle(
                        fontSize: 16.0,
                      ),
                      backgroundColor: black,
                      side: BorderSide(color: black),
                      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))), // foreground
                    ),
                    onPressed: () {
                      Navigator.pop(context, false);
                      _milestonePagingController.refresh();
                    },
                    child: Text('Nope'),
                  ),
                ),
                SizedBox(width: 8.0),
                Expanded(
                  flex: 1,
                  child: TextButton(
                    style: TextButton.styleFrom(
                      padding: EdgeInsets.symmetric(vertical: 12.0),
                      textStyle: boldFontsStyle(
                        fontSize: 16.0,
                      ),
                      backgroundColor: black,
                      foregroundColor: yellow,
                      side: BorderSide(color: yellow),
                      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))), // foreground
                    ),
                    onPressed: () async {
                      Navigator.pop(context, false);
                      _milestonePagingController.refresh();
                    },
                    child: Text('Okay'),
                  ),
                ),
              ],
            ),
          ),
        ],
        actionsAlignment: MainAxisAlignment.center,
        actionsPadding: EdgeInsets.symmetric(vertical: 12.0),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      )
    ).then((delay) {
      if (delay == null) return;
      if (delay) Navigator.pop(context);
    });
  }
}