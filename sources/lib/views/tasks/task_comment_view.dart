import 'dart:async';
import 'dart:io';
import 'package:bubble/bubble.dart';
import 'package:clipboard/clipboard.dart';
import 'package:cron/cron.dart';
import 'package:dio/dio.dart';
import 'package:drift/drift.dart' as DRIFT;
import 'package:path/path.dart' as p;
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:open_filex/open_filex.dart';
import 'package:sources/models/comment_details_model.dart';
import 'package:sources/services/comment_service.dart';
import 'package:sources/utils/size_utils.dart';
import 'package:sources/view_models/comment_view_model.dart';

import '../../args/forward_args.dart';
import '../../args/media_slider_args.dart';
import '../../constants/app_colors.dart';
import '../../constants/app_constants.dart';
import '../../constants/app_fonts.dart';
import '../../constants/app_images.dart';
import '../../constants/app_lottie_jsons.dart';
import '../../constants/app_routes.dart';
import '../../database/app_database.dart';
import '../../models/attach_model.dart';
import '../../models/comment_post_model.dart';
import '../../models/comment_update_model.dart';
import '../../models/feed_item_model.dart';
import '../../models/general_response_model.dart';
import '../../models/media_item_model.dart';
import '../../models/task_details_model.dart';
import '../../services/api.dart';
import '../../services/feed_service.dart';
import '../../services/service_locator.dart';
import '../../utils/dialog_utils.dart';
import '../../utils/route_utils.dart';
import '../../utils/shared_preference_utils/sp_manager.dart';
import '../../utils/text_utils.dart';
import '../../utils/util_helper.dart';
import '../../view_models/workplace_view_model.dart';
import '../../widgets/backdrop.dart';
import '../../widgets/comment_bubble.dart';
import '../../widgets/comment_post_form.dart';
import '../../widgets/comment_system_bubble.dart';
import '../../widgets/custom_mentions/custom_mention_view.dart';
import '../../widgets/skeleton.dart';
import '../../widgets/task_comments_info.dart';

class TaskCommentView extends StatefulWidget {
  final int taskID;
  const TaskCommentView({Key? key, required this.taskID}) : super(key: key);
  @override
  _TaskCommentViewState createState() => _TaskCommentViewState();
}

class _TaskCommentViewState extends State<TaskCommentView> {
  final database = locator<AppDatabase>();
  CommentService _commentService = CommentService();
  FeedService _feedService = FeedService();
  late bool isLoading;

  TaskDetailsModel taskInfo = TaskDetailsModel();
  List<FeedItemModel> commentList = [];
  CommentDetailsModel? commentDetailsResponse;
  CommentDetailsResponse taskInfoResponse = CommentDetailsResponse(taskDetails: null, commentList: []);
  CommentDetailsModel commentDetail = CommentDetailsModel(response: CommentDetailsResponse(taskDetails: null, commentList: []));
  List<SubtaskDetailsModel>? subtasks;
  bool autoUpdateCA = false;
  bool imageUpdateCA = false;

  bool hasUnread = true;
  GlobalKey<CustomMentionsState> commentTextfieldKey = GlobalKey<CustomMentionsState>(debugLabel: 'commentTextKey');
  List<Map<String, dynamic>> mentionsList = [];
  FocusNode commentDescriptionNode = FocusNode();
  final CommentViewModel commentViewModel = locator<CommentViewModel>();
  final WorkplaceViewModel workplaceViewModel = locator<WorkplaceViewModel>();
  String taskStartDateTime = '';
  String taskCreatedDateTime = '';
  String taskUpdatedDateTime = '';
  String taskName = '';
  String parentTaskName = '';
  String taskDescription = '';
  int commentCount = 0;
  String taskStatus = '';
  bool editMode = false;
  final ScrollController _scrollController = ScrollController();
  final ScrollController _noCommentScrollController = ScrollController();
  bool isCollapsed = true;

  ExpandableController additionalInfoController = ExpandableController();

  String systemStatus = '';
  int? currentProjectID;
  List<MediaInfoArguments> mediaArgsList = [];
  List<AttachModel> imageList = [];
  List<AttachModel> docList = [];

  List<int> mediaIds = [];
  int? toolboxFeedID;
  bool showToolbox = false;
  bool noTaskDetails = false;
  late Timer _timer;
  String errorMessage = '';

  Cron? cron;

  @override
  void initState() {
    fetchCommentList();

    additionalInfoController.addListener(() {
      isCollapsed = !isCollapsed;
    });
    super.initState();
    // Fetch comments data from API every 15 seconds
    cron = Cron()
      ..schedule(Schedule.parse('*/5 * * * * *'), () async {
        await _commentService
            .fetchTaskCommentDetails(widget.taskID)
            .then((response) {
          setState(() {
            commentDetailsResponse = response;
            taskInfoResponse = commentDetailsResponse!.response;
            taskInfo = taskInfoResponse.taskDetails!;
            commentList = taskInfoResponse.commentList ?? [];
            taskStartDateTime = taskInfo.taskStartDateTime ?? '';
            taskCreatedDateTime = taskInfo.createdDateTime ?? '';
            taskUpdatedDateTime = taskInfo.updatedDateTime ?? '';
            taskName = taskInfo.taskName ?? '';
            parentTaskName = taskInfo.parentTaskName ?? '';
            taskDescription = taskInfo.taskDescription ?? '';
            commentCount = commentDetailsResponse?.totalItems ?? 0;

            taskStatus = taskInfo.taskStatus ?? '';

            imageList = taskInfo.forInternal?.updatesImages ?? [];
            docList = taskInfo.forInternal?.updatesFiles ?? [];
            mediaArgsList = imageList
                .map((m) => MediaInfoArguments(
                    mediaPath: m.imagePath ?? '',
                    thumbnailPath: m.thumbnailPath ?? '',
                    mediaDescription: taskDescription,
                    mediaType: m.imageType ?? '',
                    datetime: taskInfo.taskStartDateTime))
                .toList();
          });
        });
      });
  }

  //collapsed task deatail first and setHeight
  doCollapsedAndSetHeight() {
    setState(() {
      isCollapsed = false;
      additionalInfoController = ExpandableController(
        initialExpanded: isCollapsed,
      );
    });
  }

  fetchCommentList({bool syncNewdataOnly = true}) async {
    setState(() {
      if (syncNewdataOnly) isLoading = true;
    });
    try {
      List<Map<String, dynamic>> temporaryList = [];
      await _commentService.fetchTaskCommentDetails(widget.taskID).then((response) {
        commentDetailsResponse = response;
        taskInfoResponse = commentDetailsResponse!.response;
        taskInfo = taskInfoResponse.taskDetails!;
        commentList = taskInfoResponse.commentList ?? [];

        taskStartDateTime = taskInfo.taskStartDateTime ?? '';
        taskCreatedDateTime = taskInfo.createdDateTime ?? '';
        taskUpdatedDateTime = taskInfo.updatedDateTime ?? '';
        taskName = taskInfo.taskName ?? '';
        parentTaskName = taskInfo.parentTaskName ?? '';
        taskDescription = taskInfo.taskDescription ?? '';
        commentCount = commentDetailsResponse?.totalItems ?? 0;
        taskStatus = taskInfo.taskStatus ?? '';

        imageList = taskInfo.forInternal?.updatesImages ?? [];
        docList = taskInfo.forInternal?.updatesFiles ?? [];
        mediaArgsList = imageList
            .map((m) => MediaInfoArguments(
                mediaPath: m.imagePath ?? '',
                thumbnailPath: m.thumbnailPath ?? '',
                mediaDescription: taskDescription,
                mediaType: m.imageType ?? '',
                datetime: taskInfo.taskStartDateTime))
            .toList();

        database.teamMemberDao.getTeamMembersByProject(taskInfoResponse.taskDetails!.projectID!).then((List<TeamMember> teamMembers) {
          teamMembers.forEach((member) {
            temporaryList.add({
              'id': member.teamMemberID.toString(),
              'display': member.teamMemberName,
              'teamMemberName': member.teamMemberName,
              'avatar': member.teamMemberAvatar,
              'designation': member.designationName,
            });
          });
        });
      });
      setState(() {
        mentionsList = temporaryList;
      });
      Future.delayed(Duration(milliseconds: 1000), () => doCollapsedAndSetHeight());
    } catch (e) {
      setState(() {
        // if don't have this task,then popup and goback to previous page
        noTaskDetails = true;
        _commentService.fetchErrorMessage(widget.taskID).then((response) {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              _timer = Timer(Duration(seconds: 3), () {
                Navigator.pop(context, true);
              });
              return AlertDialog(
                backgroundColor: white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
                title: SvgPicture.asset(
                  ic_warning,
                  width: 40,
                ),
                content: Text(
                  response,
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      color: black),
                  textAlign: TextAlign.center,
                ),
              );
            }
          ).then((val) {
            if (_timer.isActive) {
              _timer.cancel();
            }
            if (val == null || _timer.isActive == false)
              Navigator.pop(context, true);
          });
        });
      });
    }
    setState(() {
      if (syncNewdataOnly) isLoading = false;
    });
  }

  @override
  void dispose() {
    super.dispose();
    cron?.close();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          if (toolboxFeedID != null) hideToolbox();
          if (noTaskDetails) Navigator.pop(context, true);

          //collapse text keyboard in mobile
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        });
      },
      child: Scaffold(
        key: PageStorageKey<String>('TaskComments'),
        backgroundColor: whiteGrey,
        appBar: _buildAppBar(context),
        body: isLoading
            ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Lottie.asset(
                      lottie_loading_spinner,
                      width: 50.0,
                      height: 50.0,
                      fit: BoxFit.fill,
                    ),
                    SizedBox(height: 16.0),
                    Text(
                      'just a few secs...',
                      style: regularFontsStyle(fontSize: 14.0, color: grey),
                    ),
                  ],
                ),
              )
            : NotificationListener<ScrollNotification>(
              onNotification: (scrolling) {
                if (_noCommentScrollController.position.userScrollDirection == ScrollDirection.reverse) {
                  setState(() {
                    isCollapsed = false;
                    additionalInfoController = ExpandableController(
                      initialExpanded: isCollapsed,
                    );
                  });
                }
                return true;
              },
              child: RefreshIndicator(
                  color: yellow,
                  backgroundColor: white,
                  onRefresh: () async {
                    // call fetchTaskDetials function again to refresh page
                    fetchCommentList();
                  },
                  child: noTaskDetails
                      ? Container()
                      : Stack(
                        alignment: Alignment.center,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              // Feeds list, Scroll to Bottom & backdrop
                              Expanded(
                                child: Stack(
                                  children: [
                                    // Build feed listing
                                    _buildTaskDetailView(context),
                                    ValueListenableBuilder(
                                        valueListenable: commentViewModel.toolboxMode,
                                        builder: (context, String toolboxMode, Widget? child) {
                                          return toolboxMode == 'edit' && toolboxMode != 'comment'
                                              ? ClipPath(child: Backdrop(onTap: () => hideToolbox()))
                                              : Container();
                                        }),
                                    // Build selected bubble with toolbox
                                    ValueListenableBuilder(
                                        valueListenable: commentViewModel.toolboxMode,
                                        builder: (context, String toolboxMode, Widget? child) {
                                          return toolboxMode == 'edit' && toolboxMode != 'comment'
                                              ? _buildSelectedBubbleWithToolbox()
                                              : Container();
                                        }),
                                  ],
                                ),
                              ),
                              _buildCommentsSend(), 
                            ],
                          ),
                          // Build backdrop when it's not edit mode
                          ValueListenableBuilder(
                              valueListenable: commentViewModel.toolboxMode,
                              builder: (context, String toolboxMode, Widget? child) {
                                return toolboxMode == 'comment' && toolboxMode != 'edit'
                                    ? ClipPath(child: Backdrop(onTap: () => hideToolbox()))
                                    : Container();
                              }),
                          // Build selected bubble with toolbox
                          ValueListenableBuilder(
                              valueListenable: commentViewModel.toolboxMode,
                              builder: (context, String toolboxMode, Widget? child) {
                                return toolboxMode == 'comment' && toolboxMode != 'edit'
                                    ? _buildSelectedBubbleWithToolbox()
                                    : Container();
                              }),
                        ],
                      )),
            ),
      ),
    );
  }

  void hideToolbox() {
    setState(() {
      toolboxFeedID = null;
      commentViewModel.toolboxMode.value = 'none';
      commentViewModel.commentDescriptionNode.unfocus();
      commentViewModel.mediaImageFiles.value = [];
      commentViewModel.mediaPdfFiles.value = [];

      commentTextfieldKey.currentState!.controller!.text = '';
      commentTextfieldKey.currentState!.controller!.clear();

      editMode = false;
      fetchCommentList(syncNewdataOnly: false);
    });
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.light,
      leading: IconButton(
        onPressed: () {
          hideToolbox();
          Navigator.pop(context, true);
        },
        icon: SvgPicture.asset(ic_chevron_left, width: 10.0),
      ),
      title: Column(
        children: [
          Text(
            'Reply In Thread',
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, color: white, letterSpacing: 0.8),
          ),
        ],
      ),
      centerTitle: false,
      backgroundColor: black,
    );
  }

// task detail view
  Widget _buildTaskDetailView(BuildContext context) {
    taskInfo = taskInfoResponse.taskDetails!;
    return ListView(
        controller: _noCommentScrollController,
        padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 24.0),
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Text(
                  'Reply To',
                  style: regularFontsStyle(
                      fontSize: 15.0, height: 1.8, color: grey),
                ),
                SizedBox(height: 8.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                      decoration: BoxDecoration(
                          color: taskInfo.taskStatus == 'completed'
                              ? green
                              : taskInfo.taskStatus == 'flagged'
                                  ? lightPink
                                  : transparent,
                          border: Border.all(
                              color: taskInfo.taskStatus == 'completed'
                                  ? green
                                  : taskInfo.taskStatus == 'flagged'
                                      ? lightPink
                                      : taskInfo.taskEndDateTime != null ? lightGrey : red),
                          borderRadius: BorderRadius.circular(500.0)),
                      child: Text(
                        taskInfo.taskStatus == 'completed'
                            ? 'Completed'
                            : taskInfo.taskStatus == 'flagged'
                                ? 'Flagged'
                                : taskInfo.taskEndDateTime != null 
                                  ? formatDatetime(datetime: taskInfo.taskEndDateTime ?? '') 
                                  : 'No Due Date',
                        style:  (taskInfo.taskStatus == 'completed' || taskInfo.taskStatus == 'flagged' || taskInfo.taskEndDateTime != null ) ? boldFontsStyle(
                            fontSize: 14.0,
                            color: taskInfo.taskStatus == 'completed'
                                ? white
                                : taskInfo.taskStatus == 'flagged'
                                    ? red
                                    : grey) 
                                    : italicFontsStyle(fontSize: 14.0, color: red),
                      ),
                    ),
                    if (taskInfo.taskStatus == 'completed')
                      SvgPicture.asset(ic_task_complete, width: 26.0),
                    if (taskInfo.taskStatus == 'flagged')
                      SvgPicture.asset(ic_task_flag, width: 26.0),
                  ],
                ),
                SizedBox(height: 16.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(taskName, style: boldFontsStyle(fontSize: 16.0)),
                    taskInfo.parentTaskName != null
                        ? Text(parentTaskName,
                            style:
                                italicFontsStyle(fontSize: 12.0, color: grey))
                        : Container(),
                    SizedBox(
                        height: taskInfo.taskDescription == '' ||
                                taskInfo.taskDescription == null
                            ? 0.0
                            : 8.0),
                    taskInfo.taskDescription == '' ||
                            taskInfo.taskDescription == null
                        ? Container()
                        : Text(taskInfo.taskDescription ?? ''),
                    SizedBox(height: 8.0),
                  ],
                ),
                taskStatus == 'completed' || taskStatus == 'flagged'
                    ? Row(
                        children: [
                          Expanded(
                            child: ExpandableNotifier(
                              child: Column(children: [
                                ExpandablePanel(
                                  controller: additionalInfoController,
                                  expanded: TaskCommentsInfo(
                                      taskInfo: taskInfo,
                                      autoUpdateCA: autoUpdateCA,
                                      isPushedToClientApp: taskInfo.isPushedToClientApp,
                                      isCollapsed: false,
                                      callback: () {
                                        fetchCommentList();
                                      }),
                                  collapsed: TaskCommentsInfo(
                                      taskInfo: taskInfo,
                                      autoUpdateCA: autoUpdateCA,
                                      isPushedToClientApp: taskInfo.isPushedToClientApp,
                                      isCollapsed: true,
                                      callback: () {
                                        fetchCommentList();
                                      }),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Builder(
                                      builder: (context) {
                                        return Expanded(
                                            child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisSize:
                                                    MainAxisSize.min,
                                                children: [
                                              Text(
                                                'Last edited on: ${formatDatetime(datetime: taskInfo.updatedDateTime != '' && taskInfo.updatedDateTime != null ? taskInfo.updatedDateTime : taskInfo.createdDateTime)}',
                                                style: regularFontsStyle(
                                                    fontSize: 12.0,
                                                    height: 1.5,
                                                    color: grey),
                                              ),
                                              Container(
                                                width: 4.0,
                                                height: 4.0,
                                                margin: EdgeInsets.all(8.0),
                                                decoration: BoxDecoration(
                                                  color: grey,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          500.0),
                                                ),
                                              ),
                                              Text(
                                                formatDatetime(
                                                        datetime: taskInfo
                                                                        .updatedDateTime !=
                                                                    '' &&
                                                                taskInfo.updatedDateTime !=
                                                                    null
                                                            ? taskInfo
                                                                .updatedDateTime
                                                            : taskInfo
                                                                .createdDateTime,
                                                        format: 'hh:mma')
                                                    .toLowerCase(),
                                                style: regularFontsStyle(
                                                    fontSize: 12.0,
                                                    height: 1.5,
                                                    color: grey),
                                              ),
                                              Spacer(),
                                              GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    isCollapsed =
                                                        !isCollapsed;
                                                    additionalInfoController =
                                                        ExpandableController(
                                                      initialExpanded:
                                                          isCollapsed,
                                                    );
                                                  });
                                                },
                                                child: AnimatedRotation(
                                                    turns: isCollapsed
                                                        ? 0.5
                                                        : 1.0,
                                                    duration: Duration(
                                                        milliseconds: 450),
                                                    child: SvgPicture.asset(
                                                        ic_dropdown,
                                                        color: grey)),
                                              )
                                            ]));
                                      },
                                    ),
                                  ],
                                ),
                                SizedBox(height: 8.0),
                              ]),
                            ),
                          ),
                        ],
                      )
                    : Container(),
              ]),
              _buildCommentsList()
            ],
          ),
        ]);
  }

  Widget _buildCommentsList() {
    return Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(vertical: 8.0),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 4.0),
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(color: lightGrey),
                  bottom: BorderSide(color: lightGrey),
                ),
              ),
              child: Row(
                children: [
                  Text('$commentCount ',
                      style: boldFontsStyle(
                          fontSize: 14.0, height: 1.8, color: grey)),
                  Text(commentCount > 1 ? 'Replies' : 'Reply',
                      style: boldFontsStyle(
                          fontSize: 14.0, height: 1.8, color: grey)),
                  SizedBox(width: 8.0),
                  if (taskInfoResponse.taskDetails?.isRead == "unread")
                    Container(
                      width: 10.0,
                      height: 10.0,
                      margin: EdgeInsets.symmetric(horizontal: 4.0),
                      decoration: BoxDecoration(
                        color: red,
                        borderRadius: BorderRadius.circular(500.0),
                      ),
                    ),
                ],
              ),
            ),
            SizedBox(height: 8.0),
            if (isLoading) _buildSkeleton(context),
            Row(
              children: [
                Expanded(
                  child: Container(
                    child: NotificationListener<ScrollNotification>(
                        onNotification: (scrolling) {
                          if (_scrollController.position.userScrollDirection == ScrollDirection.reverse) {
                            setState(() {
                              isCollapsed = false;
                              additionalInfoController = ExpandableController(
                                initialExpanded: isCollapsed,
                              );
                            });
                          }
                          final metrics = scrolling.metrics;
                          if (metrics.atEdge) {
                            bool isTop = metrics.pixels == 0;
                            if (isTop) {
                              setState(() {
                                isCollapsed = false;
                                additionalInfoController = ExpandableController(
                                  initialExpanded: isCollapsed,
                                );
                              });
                            } else {
                              setState(() {
                                isCollapsed = false;
                                additionalInfoController = ExpandableController(
                                  initialExpanded: isCollapsed,
                                );
                              });
                            }
                          }
                          return true;
                        },
                        child: commentCount != 0 && isLoading == false
                            ? ListView.separated(
                                separatorBuilder: (c, index) {
                                  return SizedBox(
                                    height: 0.0,
                                  );
                                },
                                addAutomaticKeepAlives: true,
                                controller: _scrollController,
                                physics: NeverScrollableScrollPhysics(),
                                key: PageStorageKey<String>(
                                    'TaskComments' + widget.taskID.toString()),
                                itemCount: commentList.length,
                                // Sort newest chat item at the bottom & make the list always stick to bottom
                                reverse: true,
                                shrinkWrap: true,
                                itemBuilder: (BuildContext context, int index) {
                                  bool showDate = false;
                                  bool showUnread = false;
                                  var unreadFeeds =
                                      taskInfoResponse.taskDetails?.isRead;

                                  final FeedItemModel commentWithDetails =
                                      commentList[index];
                                  FeedItemModel? nextcommentWithDetail;
                                  DateTime now = DateTime.now();
                                  var currentDate = formatDatetime(
                                      datetime:
                                          commentWithDetails.postDateTime ??
                                              '');
                                  var todayDate =
                                      formatDatetime(datetime: now.toString());
                                  if (index < (commentList.length - 1)) {
                                    nextcommentWithDetail =
                                        commentList[index + 1];
                                    var nextDate = formatDatetime(
                                        datetime: nextcommentWithDetail
                                                .postDateTime ??
                                            '',
                                        format: 'd MMM y');
                                    if (currentDate != nextDate) {
                                      showDate = true;
                                    }
                                  }
                                  // If the feed is the oldest feed
                                  else if (index == (commentCount - 1)) {
                                    showDate = true;
                                  }
                                  // new label
                                  if (unreadFeeds == "unread" && index == 0) {
                                    showUnread = true;
                                  }

                                  String? commentType =
                                      commentWithDetails.feedType;

                                  bool showLabel = false;
                                  if (showDate || showUnread) {
                                    showLabel = true;
                                  }

                                  return Container(
                                    padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                                    child: Column(
                                      children: [
                                        if (showDate)
                                          Padding(
                                            padding: EdgeInsets.only(
                                                top: 0, bottom: 8.0),
                                            child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Expanded(
                                                    child: Container(
                                                      padding: EdgeInsets.only(
                                                          right: 8, left: 0),
                                                      child: Divider(
                                                          thickness: 0.5,
                                                          color: grey),
                                                    ),
                                                  ),
                                                  Text(
                                                    (currentDate == todayDate)
                                                        ? 'Today'
                                                        : currentDate,
                                                    style:
                                                        TextStyle(color: grey),
                                                  ),
                                                  Expanded(
                                                    child: Container(
                                                      padding: EdgeInsets.only(
                                                          right: 0, left: 8),
                                                      child: Divider(
                                                          thickness: 0.5,
                                                          color: grey),
                                                    ),
                                                  ),
                                                ]),
                                          ),
                                        if (showUnread)
                                          Padding(
                                            padding: EdgeInsets.only(
                                                top: 0, bottom: 8.0),
                                            child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Expanded(
                                                    child: Container(
                                                      padding: EdgeInsets.only(
                                                          right: 8, left: 0),
                                                      child: Divider(
                                                          thickness: 0.5,
                                                          color: darkYellow),
                                                    ),
                                                  ),
                                                  Text(
                                                    'New',
                                                    style: TextStyle(
                                                        color: darkYellow),
                                                  ),
                                                  Expanded(
                                                    child: Container(
                                                      padding: EdgeInsets.only(
                                                          right: 0, left: 8),
                                                      child: Divider(
                                                          thickness: 0.5,
                                                          color: darkYellow),
                                                    ),
                                                  ),
                                                ]),
                                          ),
                                        // List item
                                        (commentType != 'normal' &&
                                                commentType == 'system')
                                            ? CommentSystemBubble(
                                                key: ValueKey(commentList[index].feedID),
                                                commentWithDetails: taskInfoResponse,
                                                comments: commentList[index],
                                                index: index)
                                            : _buildCommentItem(
                                                context,
                                                taskInfoResponse,
                                                commentWithDetails,
                                                nextcommentWithDetail,
                                                index,
                                                showLabel),
                                      ],
                                    ),
                                  );
                                })
                            : _buildEmptyResult(context)),
                  ),
                ),
              ],
            ),
          ],
        ));
  }

// Comment item widget
  Widget _buildCommentItem(
      BuildContext context,
      CommentDetailsResponse taskInfoResponse,
      FeedItemModel comments,
      FeedItemModel? nextcommentWithDetail,
      int index,
      bool showLabel) {
    bool showNip = true;
    String type = 'other';
    if (comments.teamMemberID == workplaceViewModel.currentUser!.teamMemberID) {
      type = 'self';
    }
    if (nextcommentWithDetail != null) {
      if (nextcommentWithDetail.teamMemberID == comments.teamMemberID &&
          nextcommentWithDetail.feedType != "system") {
        showNip = false;
      }
    }
    return ValueListenableBuilder(
        valueListenable: commentViewModel.toolboxMode,
        builder: (context, String toolboxMode, Widget? child) {
          return CommentBubble(
            key: ValueKey(comments.feedID),
            commentWithDetails: taskInfoResponse,
            commentList: comments,
            index: index,
            type: type,
            showBubble: true,
            showTopic: false,
            showNip: showNip,
            showToolbox: false,
            enableReplyButton: false,
            onLongPressCallback: (offset) {
              setState(() {
                RenderBox bubbleSize = context.findRenderObject() as RenderBox;
                double statusBarHeight = MediaQuery.of(context).padding.top;
                double appBarHeight = _buildAppBar(context).preferredSize.height;
                double maxOffsetY = MediaQuery.of(context).size.height - statusBarHeight - appBarHeight - 64;
                double originalOffsetY = offset.y;
                double minOffset = maxOffsetY / 6;
                // offset Y checking
                if ((originalOffsetY + bubbleSize.size.height) > maxOffsetY) {
                  offset.y = minOffset;
                } else {
                  offset.y = maxOffsetY - (originalOffsetY + bubbleSize.size.height);
                }
                // min offset Y checking
                if (offset.y < 0.0) {
                  offset.y = minOffset;
                }
                toolboxFeedID = comments.feedID;
                commentViewModel.selectedBubbleOffset.value = offset.y;
                commentViewModel.selectedCommentWithDetail.value = comments;
                commentViewModel.toolboxMode.value = "comment";
                editMode = true;
              });
            },
            onPdfClicked: (Media media) async {
              try {
                late String file;
                if (media.localFilePath != null &&
                    await File(media.localFilePath ?? '').exists()) {
                  file = media.localFilePath!;
                } else {
                  await SpManager.putInt(media.url.toString(), 50);
                  showToastMessage(context,
                      message: 'Downloading', duration: 1);
                  await Api()
                      .downloadFile(media.url ?? '', 'pdf', context)
                      .then((localSavePath) {
                    file = localSavePath;
                    final newMediaData = media.copyWith(localFilePath: DRIFT.Value(file));
                    database.mediaDao.insertOrUpdateMedia(newMediaData);
                  });
                }
                OpenFilex.open(file);
              } on Exception catch (_) {
                showError(context, 'Unexpected error, please try again.');
              }
            },
            onImageClicked:
                (List<Media> medias, List<FeedItemModel> feeds, int index) {
              List<MediaInfoArguments> mediaArgsList = medias
                  .map((m) => MediaInfoArguments(
                      mediaPath: m.url ?? '',
                      mediaDescription: feeds[index].description,
                      mediaType: m.fileExtension ?? '',
                      datetime: feeds[index].postDateTime))
                  .toList();
              goToNextNamedRoute(context, mediaSliderScreen,
                  args: MediaSliderArguments(mediaArgsList, index));
            },
          );
        });
  }

  downloadMediaToCache(List<MediaItemModel> mediaList) async {
    List<String> tmpLocalURLs = [];
    await Future.forEach(mediaList, (MediaItemModel media) async {
      String tmpLocalURL = '';
      if (media.url != null && await File(media.url ?? '').exists()) {
        tmpLocalURL = media.url!;
      } else {
        tmpLocalURL = await Api().downloadFile(media.url ?? '', 'img', context, isShow: false, saveToGallery: false);
        // await database.mediaDao.insertOrUpdateMedia(media.copyWith(localFilePath: tmpLocalURL));
      }
      tmpLocalURLs.add(tmpLocalURL);
    });

    return tmpLocalURLs;
  }

// Skeleton widget
  Widget _buildSkeleton(BuildContext context) {
    return Container(
      child: ListView(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        children: [
          Column(
            children: [
              Stack(
                children: [
                  Bubble(
                    margin: BubbleEdges.only(top: 20, bottom: 16, right: 60, left: 50),
                    padding: BubbleEdges.all(0),
                    radius: Radius.circular(12),
                    color: shimmerGrey,
                    nip: BubbleNip.leftTop,
                    nipHeight: 12,
                    alignment: Alignment.centerLeft,
                    shadowColor: transparent,
                    elevation: 0,
                    child: SkeletonContainer.rounded(width: 250, height: 300),
                  ),
                  Positioned(
                      top: 20,
                      left: 20,
                      child: SkeletonContainer.circular(width: 25, height: 25)),
                ],
              ),
              Stack(
                children: [
                  Bubble(
                    margin: BubbleEdges.only(
                        top: 20, bottom: 94, left: 60, right: 20),
                    padding: BubbleEdges.all(0),
                    radius: Radius.circular(12),
                    nip: BubbleNip.rightBottom,
                    color: shimmerGrey,
                    nipHeight: 12,
                    alignment: Alignment.centerRight,
                    shadowColor: transparent,
                    elevation: 0,
                    child: SkeletonContainer.rounded(width: 250, height: 188),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildSelectedBubbleWithToolbox() {
    String type = 'other';
    if (commentViewModel.selectedCommentWithDetail.value?.teamMemberID == workplaceViewModel.currentUser!.teamMemberID) {
      type = 'self';
    }
    return ValueListenableBuilder(
        valueListenable: commentViewModel.toolboxMode,
        builder: (context, String toolboxMode, Widget? child) {
          FeedItemModel selectedCommentWithDetail = commentViewModel.selectedCommentWithDetail.value!;
          return Positioned(
            width: SizeUtils.width(context, 100),
            bottom: toolboxMode == 'edit' ? 135 : commentViewModel.selectedBubbleOffset.value,
            left: 0,
            child: CommentBubble(
              key: ValueKey(selectedCommentWithDetail.feedID),
              commentWithDetails: taskInfoResponse,
              commentList: selectedCommentWithDetail,
              index: 0,
              type: type,
              showBubble: true,
              showTopic: false,
              showNip: true,
              showToolbox: toolboxMode != 'none' && toolboxMode != 'edit',
              enableReplyButton: false,
              onForward: () async {
                List<String> tmpLocalURLs = await downloadMediaToCache(
                    selectedCommentWithDetail.mediaList ?? []);
          
                goToNextNamedRoute(context, 'workplaceForward',
                    args: ForwardArguments(
                        removeAllHtmlTags(
                            selectedCommentWithDetail.description ?? ''),
                        tmpLocalURLs));
                hideToolbox();
              },
              onShare: () async {
                workplaceShareDialog(
                  context, 
                  selectedCommentWithDetail.description ?? '',
                  selectedCommentWithDetail.mediaList?.map((e) => 
                    Media(
                      mediaID: e.mediaID!, 
                      projectID: selectedCommentWithDetail.projectID!, 
                      feedID: selectedCommentWithDetail.feedID!,
                      url: e.url,
                      fileExtension: e.fileExtension
                    )
                  ).toList() ?? []
                );
                hideToolbox();
              },
              onEdit: () {
              String putSymbol = (selectedCommentWithDetail.description ?? '').replaceAll(RegExp(r'<h6[^>]*>'), '@');
                commentTextfieldKey.currentState!.controller!.text = Bidi.stripHtmlIfNeeded(putSymbol).trim();
                if (selectedCommentWithDetail.mediaList!.length > 0) {
                  commentViewModel.mediaPdfFiles.value =
                      selectedCommentWithDetail.mediaList!
                          .where((e) => e.fileExtension == 'pdf')
                          .map((e) => e.url)
                          .toList();
                  commentViewModel.mediaImageFiles.value =
                      selectedCommentWithDetail.mediaList!
                          .where((e) => e.fileExtension != 'pdf')
                          .map((e) => e.url)
                          .toList();
                }
                commentViewModel.toolboxMode.value = "edit";
              },
              onCopy: () async {
                List<String> tmpLocalURLs = await downloadMediaToCache(
                    selectedCommentWithDetail.mediaList ?? []);
          
                hideToolbox();
                FlutterClipboard.copy(removeAllHtmlTags(reverseMarkupText(
                    selectedCommentWithDetail.description ?? '')));
                workplaceViewModel.temporaryCopiesMediaFiles.value =
                    tmpLocalURLs;
          
                showMessage(context, message: 'Copied!');
              },
              onDelete: () async {
                await _commentService
                    .deleteComment(taskInfoResponse.taskDetails!.projectID!,
                        widget.taskID, selectedCommentWithDetail.feedID!)
                    .then((response) {});
                hideToolbox();
              },
              onPdfClicked: (Media media) async {
                try {
                  late String file;
                  if (media.localFilePath != null &&
                      await File(media.localFilePath ?? '').exists()) {
                    file = media.localFilePath!;
                  } else {
                    await SpManager.putInt(media.url.toString(), 50);
                    showToastMessage(context,
                        message: 'Downloading', duration: 1);
                    await Api()
                        .downloadFile(media.url ?? '', 'pdf', context)
                        .then((localSavePath) {
                      file = localSavePath;
                      final newMediaData =
                          media.copyWith(localFilePath: DRIFT.Value(file));
                      database.mediaDao.insertOrUpdateMedia(newMediaData);
                    });
                  }
                  OpenFilex.open(file);
                } on Exception catch (_) {
                  showError(context, 'Unexpected error, please try again.');
                }
              },
              onImageClicked: (List<Media> medias, List<FeedItemModel> feeds, int index) {
                List<MediaInfoArguments> mediaArgsList = medias
                    .map((m) => MediaInfoArguments(
                        mediaPath: m.url ?? '',
                        mediaDescription: feeds[index].description,
                        mediaType: m.fileExtension ?? '',
                        datetime: feeds[index].postDateTime))
                    .toList();
                goToNextNamedRoute(context, mediaSliderScreen,
                    args: MediaSliderArguments(mediaArgsList, index));
              },
            ),
          );
        });
  }

  Widget _buildEmptyResult(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.75,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Lottie.asset(
                lottie_empty_chat,
                width: 250.0,
                height: 250.0,
                fit: BoxFit.contain,
              ),
            ),
            Container(
              width: 250.0,
              child: Text(
                text_empty_chat,
                style: regularFontsStyle(fontSize: 17.0, height: 1.35),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildCommentsSend() {
    return Container(
      width: double.infinity,
      child: Row(
        children: [
          Expanded(
            child: CommentPostForm(
              commentTextfieldKey: commentTextfieldKey,
              showTopicButton: false,
              focusNode: commentDescriptionNode,
              teamMembers: mentionsList,
              editMode: editMode,
              onTextChanged: (text) {
                commentViewModel.temporaryTextFieldValue.value = text;
              },
              submitOnTap: () async {
                commentViewModel.isSending.value = true;
                String formValue = commentTextfieldKey.currentState!.controller!.markupText;

                if (formValue != '' ||
                    commentViewModel.mediaImageFiles.value.length > 0 ||
                    commentViewModel.mediaPdfFiles.value.length > 0) {
                  int newMediasID = -1;
                  List<int> mediaIds = [];
                  // ignore: unused_local_variable
                  bool uplaodImageFailed = false;

                  if (commentViewModel.mediaPdfFiles.value.length > 0) {
                    for (var mediaFile in commentViewModel.mediaPdfFiles.value) {
                      final formData = FormData.fromMap({
                        'fileUpload': await MultipartFile.fromFile(mediaFile)
                      });
                      await _feedService.postMedia(formData).then((response) {
                        GeneralResponseModel mediaResponse =
                            GeneralResponseModel.fromJson(response.data);

                        if (mediaResponse.status == true) {
                          database.mediaDao.updateMediaData(
                              newMediasID,
                              mediaResponse.response['mediaID'],
                              mediaResponse.response['url']);
                          mediaIds.add(mediaResponse.response['mediaID']);
                        } else {
                          uplaodImageFailed = true;
                        }
                      }).onError((error, stackTrace) {
                        uplaodImageFailed = true;
                      });
                      newMediasID--;
                    }
                  }

                  if (commentViewModel.mediaImageFiles.value.length > 0) {
                    for (var mediaFile
                        in commentViewModel.mediaImageFiles.value) {

                      File renamedFile;
                      if (Platform.isIOS) {
                        String fileExtension = p.extension(mediaFile);
                        String uniqueIdentifier = DateTime.now().millisecondsSinceEpoch.toString();
                        String shortenedFileName = p.basenameWithoutExtension(mediaFile).substring(0, 10) + '_' + uniqueIdentifier + fileExtension;
                        String shortenedFilePath = p.join(p.dirname(mediaFile), shortenedFileName);
                        renamedFile = File(mediaFile).renameSync(shortenedFilePath);
                      } else {
                        renamedFile = File(mediaFile);
                      }

                      final formData = FormData.fromMap({
                        'fileUpload': await MultipartFile.fromFile(renamedFile.path)
                      });
                      await _feedService.postMedia(formData).then((response) {
                        GeneralResponseModel mediaResponse =
                            GeneralResponseModel.fromJson(response.data);

                        if (mediaResponse.status == true) {
                          database.mediaDao.updateMediaData(
                              newMediasID,
                              mediaResponse.response['mediaID'],
                              mediaResponse.response['url']);
                          mediaIds.add(mediaResponse.response['mediaID']);
                        } else {
                          uplaodImageFailed = true;
                        }
                      }).onError((error, stackTrace) {
                        uplaodImageFailed = true;
                      });
                      newMediasID--;
                    }
                  }

                  CommentPostModel data = CommentPostModel(
                      projectID: taskInfoResponse.taskDetails!.projectID!,
                      taskID: widget.taskID,
                      description: formValue,
                      mediaID: mediaIds);
                  await _commentService.postComment(data).then((response) {
                    FocusScopeNode currentFocus = FocusScope.of(context);
                    if (!currentFocus.hasPrimaryFocus) {
                      currentFocus.unfocus();
                    }
                    hideToolbox();
                  });
                }

                commentViewModel.isSending.value = false;
                // _noCommentScrollController.jumpTo(_noCommentScrollController.position.maxScrollExtent);
              },
              updateOnTap: () async {
                commentViewModel.isSending.value = true;
                String formValue = commentTextfieldKey.currentState!.controller!.markupText;

                List<int?> mediaIds = commentViewModel.selectedCommentWithDetail.value?.mediaList?.map((e) => e.mediaID).toList() ?? [];

                await Future.forEach(commentViewModel.mediaPdfFiles.value, (dynamic mediaPath) async {
                  if (mediaPath[0] == '/') {
                    final formData = FormData.fromMap({'fileUpload': await MultipartFile.fromFile(mediaPath)});
                    await _feedService.postMedia(formData).then((response) {
                      GeneralResponseModel mediaResponse = GeneralResponseModel.fromJson(response.data);
                      mediaIds.add(mediaResponse.response['mediaID']);
                    });
                  }
                });

                await Future.forEach(commentViewModel.mediaImageFiles.value, (dynamic mediaPath) async {
                  if (mediaPath[0] == '/') {
                    File renamedFile;
                    if (Platform.isIOS) {
                      String fileExtension = p.extension(mediaPath);
                      String uniqueIdentifier = DateTime.now().millisecondsSinceEpoch.toString();
                      String shortenedFileName = p.basenameWithoutExtension(mediaPath).substring(0, 10) + '_' + uniqueIdentifier + fileExtension;
                      String shortenedFilePath = p.join(p.dirname(mediaPath), shortenedFileName);
                      renamedFile = File(mediaPath).renameSync(shortenedFilePath);
                    } else {
                      renamedFile = File(mediaPath);
                    }

                    final formData = FormData.fromMap({'fileUpload': await MultipartFile.fromFile(renamedFile.path)});
                    await _feedService.postMedia(formData).then((response) {
                      GeneralResponseModel mediaResponse = GeneralResponseModel.fromJson(response.data);
                      mediaIds.add(mediaResponse.response['mediaID']);
                    });
                  }
                });

                CommentUpdateModel formData = CommentUpdateModel(
                  projectID: taskInfoResponse.taskDetails!.projectID!,
                  taskID: widget.taskID,
                  feedID: commentViewModel.selectedCommentWithDetail.value!.feedID!,
                  mediaID: mediaIds,
                  description: formValue,
                );

                await _commentService.postCommentUpdate(formData).then((response) {
                  hideToolbox();
                  FocusScopeNode currentFocus = FocusScope.of(context);
                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                });
                fetchCommentList();
                commentViewModel.isSending.value = false;
              },
            )
          ),
        ]
      )
    );
  }
}
