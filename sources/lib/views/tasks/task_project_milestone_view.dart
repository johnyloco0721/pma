import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
// import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:sources/args/task_args.dart';
import 'package:sources/args/task_form_args.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/models/permissions_model.dart';
import 'package:sources/models/task_milestone_model.dart';
import 'package:sources/models/task_project_item_model.dart';
import 'package:sources/services/task_services.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/scroll.utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/widgets/milestone_widget.dart';

import '../../models/task_item_model.dart';
import '../../widgets/calendar_widget.dart';

class TaskProjectMilestoneView extends StatefulWidget {
  final TaskArguments taskArguments;
  const TaskProjectMilestoneView({Key? key, required this.taskArguments}) : super(key: key);

  @override
  _TaskProjectMilestoneViewState createState() => _TaskProjectMilestoneViewState();
}

class _TaskProjectMilestoneViewState extends State<TaskProjectMilestoneView> {
  TaskService _taskService = TaskService();
  
  ScrollController _scrollController = ScrollController();
  PagingController<int, StageDetail> _milestonePagingController = PagingController(firstPageKey: 1);

  PermissionsModel? permissions;
  TaskMilestoneModel? taskProjectListResponse;
  TaskProjectItemModel? projectDetail;

  bool showCalendar = false;
  bool calendarFilterActive = false;
  bool isLoading = false;
  List<DateTime> hasProjectTask = [];
  DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss.SSS'Z'");
  String filterDateTime = "";
  List<StageDetail>? defaultStageList;
  // PanelController panelController = PanelController();
  List<DisclaimerList>? disclamerList;

  bool redirected = false;

  @override
  void initState() {
    super.initState();
    _milestonePagingController.addPageRequestListener((pageKey) {
      _fetchMyTaskListByMilestonePage(pageKey);
    });
  }

  Future<void> _fetchMyTaskListByMilestonePage(int pageKey) async {
    try {
      await _taskService.fetchMyTaskListByMilestone(widget.taskArguments.projectID ?? 0, pageKey, filterDateTime).then((response) {
        taskProjectListResponse = response;
        projectDetail = taskProjectListResponse?.response?.projectDetail;
        List<StageDetail>? stageList = taskProjectListResponse?.response?.stageArray;
        setState(() {
          if(defaultStageList == null) {
            hasProjectTask.clear();
            defaultStageList = stageList;
            defaultStageList?.forEach((defaultStage) {
              defaultStage.taskList?.forEach((taskDetail) {
                List<TaskItemModel> taskList = taskDetail.tasks ?? [];
                for (var e in taskList) {
                  if(e.taskTargetDateTime != null){
                    DateTime dt = DateTime.parse(e.taskTargetDateTime ?? '');
                    String getDate = dateFormat.format(dt);
                    DateTime dt2 = DateTime.parse(getDate);
                    hasProjectTask.add(dt2);
                  }
                }
              });
            });
          }
          permissions = taskProjectListResponse?.response?.permissions;
          disclamerList = taskProjectListResponse?.response?.disclaimerArr;
        });

        stageList?.forEach((stageDetail) {
          stageDetail.taskList?.forEach((project) {
            project.isExpanded = project.milestoneStatus != 'completed';
          });
        });
        final isLastPage = (stageList?.length ?? 0)  < (taskProjectListResponse?.limitPerPage ?? 50);
        if (isLastPage) {
          _milestonePagingController.appendLastPage(stageList ?? []);
        } else {
          final nextPageKey = pageKey + 1;
          _milestonePagingController.appendPage(stageList ?? [], nextPageKey);
        }
      });
    } catch (error) {
      _milestonePagingController.error = error;
    }

    // Redirect to next route once widget built completed
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (widget.taskArguments.taskID != null && !redirected) {
        redirected = true;
        goToNextNamedRoute(context, taskDetailsScreen, args: widget.taskArguments, callback: () {
          _milestonePagingController.refresh();
        });
      }
    });
  }

  void setLoadingState(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
    _milestonePagingController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: PageStorageKey<String>('TaskProjectMilestone'),
      backgroundColor: shimmerGrey,
      appBar: _buildAppBar(context),
      body: isLoading ? 
        Container(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Lottie.asset(
                lottie_loading_spinner,
                width: 50.0,
                height: 50.0,
                fit: BoxFit.fill,
              ),
              Container(
                width: 250.0,
                child: Text(
                  'just a few secs...',
                  style: regularFontsStyle(fontSize: 14.0, color: grey),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ) : RefreshIndicator(
        color: yellow,
        backgroundColor: white,
        onRefresh: () async {
          _milestonePagingController.refresh();
        },
        child: _buildTaskProjectView(context),
      ),
      floatingActionButton: (permissions?.canAccessEditorMode == true) ? 
      FloatingActionButton(
        onPressed: () async {
          // go to editor mode view route (remember add route in home_view.dart)
          goToNextNamedRoute(context, taskFormScreen,
              args: TaskFormArguments(
                teamMemberList: taskProjectListResponse?.response?.teamMemberList,
                milestoneList: taskProjectListResponse?.response?.milestoneList,
                projectID: widget.taskArguments.projectID,
              ), callback: () {
            _milestonePagingController.refresh();
          });
        },
        shape: CircleBorder(),
        backgroundColor: yellow,
        child: SvgPicture.asset(ic_add),
      ) : Container(),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.light,
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: SvgPicture.asset(ic_chevron_left, width: 10.0),
      ),
      title: Text(
        'My Task for ' + (projectDetail?.propertyName ?? ''),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, color: white, letterSpacing: 0.8),
      ),
      flexibleSpace: GestureDetector(
        onTap: () {
          // onTap app bar empty space then reset list scroll position
          scrollToTop(_scrollController);
        },
      ),
      actions: [
        // Calendar
        GestureDetector(
          onTap: () {
            // show calander
            setState(() {
              showCalendar = !showCalendar;
            });
          },
             child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                child: showCalendar 
                ? calendarFilterActive ? SvgPicture.asset(ic_calendar_yellow_active, width: 20.0) : SvgPicture.asset(ic_calendar_yellow, width: 20.0)
                : calendarFilterActive ? SvgPicture.asset(ic_calendar_white_active, width: 20.0) : SvgPicture.asset(ic_calendar, width: 20.0, color: white),
              ),
        ),
        SizedBox(width: 10.0),
      ],
      centerTitle: false,
      backgroundColor: black,
    );
  }

  // task project view
  Widget _buildTaskProjectView(BuildContext context) {
    return Column(
      children: [
        if(showCalendar)
          CustomTableCalender(
            calendarTopPadding: 270.0,
            hasProjectTask: hasProjectTask,
            dateFilter: (value) { 
              setState(() {
                calendarFilterActive = value != '';
                filterDateTime = value;
                _milestonePagingController.refresh(); 
              });
            },
            dateSelected: filterDateTime
          ),
        Expanded(
          child: MilestoneWidget(
            disclamerList: disclamerList,
            scrollController: _scrollController, 
            milestonePagingController:_milestonePagingController,
            permissions: permissions,
            projectID: widget.taskArguments.projectID,
            setLoadingState: setLoadingState,
          ),
        ),
      ],
    );
  }
}