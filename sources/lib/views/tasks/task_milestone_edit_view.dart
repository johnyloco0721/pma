import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:lottie/lottie.dart';
import 'package:sources/args/task_args.dart';
import 'package:sources/args/task_form_args.dart';
import 'package:sources/constants/app_box_shadow.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_constants.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_lottie_jsons.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/models/task_item_model.dart';
import 'package:sources/models/task_live_preview_post_model.dart';
import 'package:sources/models/task_milestone_model.dart';
import 'package:sources/models/task_project_item_model.dart';
import 'package:sources/services/task_services.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/scroll.utils.dart';
import 'package:sources/utils/status_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/widgets/drag_scroll_listener_widget.dart';
import 'package:sources/widgets/task_label_widget.dart';
import 'package:sticky_headers/sticky_headers.dart';

import '../../models/task_milestone_update_model.dart';
import '../../utils/dialog_utils.dart';

class TaskMilestoneEditorView extends StatefulWidget {
  final int projectID;
  const TaskMilestoneEditorView({Key? key, required this.projectID}) : super(key: key);

  @override
  _TaskMilestoneEditorViewState createState() => _TaskMilestoneEditorViewState();
}

class _TaskMilestoneEditorViewState extends State<TaskMilestoneEditorView> {
  TaskService _taskService = TaskService();

  GlobalKey _listViewKey = GlobalKey();
  ScrollController _scrollController = ScrollController();
  PagingController<int, TaskMilestoneDetail> _milestonePagingController = PagingController(firstPageKey: 1);
  late TaskMilestoneModel taskProjectListResponse;

  bool onDragging = false;

  // for changeColorBorder
  int? fromList;
  int? toList;
  bool _changeBorderColor = false;

  List<TaskMilestoneDetail>? projectList;
  TaskProjectItemModel? projectDetail;
  List<TaskMilestoneOption>? milestoneOption;

  @override
  void initState() {
    super.initState();
    _milestonePagingController.addPageRequestListener((pageKey) {
      _fetchMyTaskListByMilestonePage(pageKey);
    });
  }

  Future<void> _fetchMyTaskListByMilestonePage(int pageKey) async {
    try {
      await _taskService.fetchAllTaskListByMilestone(widget.projectID, pageKey: pageKey).then((response) {
        taskProjectListResponse = response;
        projectList = taskProjectListResponse.response?.stageArray?[0].taskList;
        
        setState(() {
          projectDetail = taskProjectListResponse.response?.projectDetail;
        });
        final isLastPage = (projectList?.length ?? 0) < (taskProjectListResponse.limitPerPage ?? 50);
        if (isLastPage) {
          _milestonePagingController.appendLastPage(projectList ?? []);
        } else {
          final nextPageKey = pageKey + 1;
          _milestonePagingController.appendPage(projectList ?? [], nextPageKey);
        }
      });
    } catch (error) {
      _milestonePagingController.error = error;
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: PageStorageKey<String>('TaskProjectMilestone'),
      backgroundColor: shimmerGrey,
      appBar: _buildAppBar(context),
      body: WillPopScope(
        onWillPop: () async => false,
        child: _buildTaskProjectView(context)
      ),
      // remove refresh function, requested by Steph [Review - R4]
      // RefreshIndicator(
      //     color: yellow,
      //     onRefresh: () async {
      //       _milestonePagingController.refresh();
      //     },
      // ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          // go to editor mode view route (remember add route in home_view.dart)
          goToNextNamedRoute(context, taskFormScreen,
              args: TaskFormArguments(
                teamMemberList: taskProjectListResponse.response?.teamMemberList,
                milestoneList: taskProjectListResponse.response?.milestoneList,
                projectID: widget.projectID,
              ), callback: () {
            _milestonePagingController.refresh();
          });
        },
        shape: CircleBorder(),
        backgroundColor: yellow,
        child: SvgPicture.asset(ic_add),
      ),
      bottomNavigationBar: _buildSubmitButton(context),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.light,
      automaticallyImplyLeading: false,
      // remove back button, requested by Steph [Review - R4]
      // leading: IconButton(
      //   onPressed: () {
      //     Navigator.pop(context);
      //   },
      //   icon: SvgPicture.asset(ic_chevron_left, width: 10.0),
      // ),
      title: Text(
        'My Task for ' + (projectDetail?.propertyName ?? ''),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, color: white, letterSpacing: 0.8),
      ),
      flexibleSpace: GestureDetector(
        onTap: () {
          // onTap app bar empty space then reset list scroll position
          scrollToTop(_scrollController);
        },
      ),
      centerTitle: false,
      backgroundColor: black,
      bottom: PreferredSize(
        child: Container(
          color: yellow,
          height: 42.0,
          width: double.infinity,
          child: Center(
            child: Text(
              "You're in editor mode.",
              style: regularFontsStyle(fontSize: 14.0, height: 1.29),
            ),
          ),
        ),
        preferredSize: Size.fromHeight(42.0)
      ),
    );
  }

  // task project view
  Widget _buildTaskProjectView(BuildContext context) {
    return DragScrollListenerView(
      scrollController: _scrollController,
      onDragging: onDragging,
      child: PagedListView.separated(
        key: _listViewKey,
        scrollController: _scrollController,
        pagingController: _milestonePagingController,
        separatorBuilder: (context, index) => Container(
          color: shimmerGrey,
          height: 8.0,
        ),
        builderDelegate: PagedChildBuilderDelegate<TaskMilestoneDetail>(
          firstPageProgressIndicatorBuilder: (context) => Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Lottie.asset(
                lottie_loading_spinner,
                width: 50.0,
                height: 50.0,
                fit: BoxFit.fill,
              ),
              SizedBox(height: 16.0),
              Text(
                'just a few secs...',
                style: regularFontsStyle(fontSize: 14.0, height: 1.29, color: grey),
              ),
            ],
          ),
          newPageProgressIndicatorBuilder: (context) => Container(
            padding: EdgeInsets.all(8.0),
            child: Center(
              child: Lottie.asset(
                lottie_loading_spinner,
                width: 50.0,
                height: 50.0,
                fit: BoxFit.fill,
              ),
            ),
          ),
          noMoreItemsIndicatorBuilder: (context) => Container(
            padding: EdgeInsets.all(8.0),
            child: Center(
              child: Text(
                'You’ve reach the end of the list.',
                style: regularFontsStyle(fontSize: 12.0, height: 1.33, color: grey),
              ),
            ),
          ),
          noItemsFoundIndicatorBuilder: (context) => Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                child: Lottie.asset(
                  lottie_empty_docs,
                  width: 250.0,
                  height: 250.0,
                  fit: BoxFit.fill,
                ),
              ),
              Container(
                width: 250.0,
                child: Text(
                  text_empty_task,
                  style: regularFontsStyle(fontSize: 17.0, height: 1.35),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
          itemBuilder: (context, milestone, index) {
            bool showStage = _milestonePagingController.itemList?.firstWhere((e) => e.stage?.label == milestone.stage?.label) == milestone;
            bool isStageLastItem = _milestonePagingController.itemList?.lastWhere((e) => e.stage?.label == milestone.stage?.label) == milestone;
    
            return DragTarget<DragArguments>(
              builder: (BuildContext context, List<dynamic> accepted, List<dynamic> rejected) {
                return Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: _changeBorderColor && (milestone.milestoneCategoryID == toList) ? darkYellow : transparent,
                      width: _changeBorderColor && (milestone.milestoneCategoryID == toList) ? 4 : 0,
                    ),
                  ),
                  child: Column(
                    children: [
                      StickyHeader(
                        header: taskItemLayout(milestone, showStage),
                        content: (milestone.isExpanded!) ? subtaskListView(context, milestone) : Container(),
                      ),
                      if (isStageLastItem)
                        SizedBox(height:8.0),
                    ],
                  ),
                );
              },
              onAccept: (args) {
                _changeBorderColor = false;
                if (milestone != args.milestone) {
                  setState(() {
                    milestone.tasks?.add(args.task);
                    milestone.tasks?.sort((a, b) {
                      return (a.taskTargetDateTime ?? a.completedDateTime ?? '').compareTo((b.taskTargetDateTime ?? b.completedDateTime ?? ''));
                    });
                    args.milestone.tasks?.remove(args.task);
                  });
                }
              },
              onWillAccept: (args) {
                setState(() {
                  if (fromList != milestone.milestoneCategoryID) {
                    _changeBorderColor = true;
                    toList = milestone.milestoneCategoryID;
                  } else {
                    _changeBorderColor = false;
                    toList = fromList;
                  }
                });
                return milestone != args?.milestone;
              },
            );
          },
        )),
    );
  }

  // task item layout
  Widget taskItemLayout(TaskMilestoneDetail milestone, bool showStage) {
    List<int>? existingMilestone = _milestonePagingController.itemList?.map((e) => e.milestoneCategoryID ?? 0).toList();
    milestoneOption = taskProjectListResponse.response?.milestoneList?.where((option) => !(existingMilestone?.contains(option.milestoneCategoryID) ?? false)).toList();

    return GestureDetector(
      onTap: () {
        setState(() {
          milestone.isExpanded = !milestone.isExpanded!;
        });
        // avoid sticky header UI bug
        _scrollController.animateTo(_scrollController.offset - 0.1, duration: Duration(milliseconds: 100), curve: Curves.ease);
      },
      child: Column(
        children: [
          if (showStage)
            Row(
              children: [
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
                    width: double.infinity,color:white,
                    child: Row(
                      children: [
                        Container(
                          height: 10.0,
                          width: 10.0,
                          decoration: new BoxDecoration(
                            color: hexToColor(milestone.stage?.iconColor ?? '#ffffff'),
                            shape: BoxShape.circle,
                          ),
                        ),
                        SizedBox(width:8.0), 
                        Text(milestone.stage?.label ?? '',style:boldFontsStyle(color:grey,fontSize: 14.0)),
                      ],
                    )
                  )
                ),
              ],
            ),
          Row(
            children: [
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 24.0),
                  width: double.infinity,
                  color: whiteGrey,
                  child: Row(
                    children: [
                      // task content layout
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // milestone label layout
                            Text(
                              'Milestone',
                              style: boldFontsStyle(fontSize: 14.0, color: grey),
                            ),
                            SizedBox(height: 8.0),
                            // milestoneCategoryName drop down selection widget
                            DropdownButtonFormField2<TaskMilestoneOption>(
                              decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.zero,
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: darkYellow),
                                  borderRadius: BorderRadius.circular(6.0),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: lightGrey), 
                                  borderRadius: BorderRadius.circular(6.0)
                                ),
                                fillColor: white,
                                filled: true,
                              ),
                              isExpanded: true,
                              buttonStyleData: ButtonStyleData(
                                height: 40,
                                padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                                width: double.infinity,
                                elevation: 2,
                              ),
                              iconStyleData: IconStyleData(
                                icon: SvgPicture.asset(ic_dropdown, width: 24.0, color: grey),
                              ),
                              // buttonHeight: 40,
                              // buttonPadding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                              // buttonWidth: double.infinity,
                              // dropdownMaxHeight: 200,
                              // icon: SvgPicture.asset(ic_dropdown, width: 24.0, color: grey),
                              hint: Text(
                                milestone.milestoneCategoryName ?? '',
                                style: regularFontsStyle(fontSize: 16.0),
                                overflow: TextOverflow.ellipsis,
                              ),
                              items: milestoneOption?.map((options) {
                                return DropdownMenuItem<TaskMilestoneOption>(
                                  value: options,
                                  child: Text(
                                    options.milestoneCategoryName ?? '',
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                  ),
                                );
                              }).toList(),
                              onChanged: (TaskMilestoneOption? value) {
                                milestone.milestoneCategoryName = value?.milestoneCategoryName;
                                milestone.milestoneCategoryID = value?.milestoneCategoryID;
                              },
                            )
                          ],
                        ),
                      ),
                      SizedBox(width: 16.0),
                      // subtask count & dropdown layout
                      Row(
                        children: [
                          // subtask count
                          Text(
                            (milestone.tasks?.length ?? 0).toString(),
                            style: boldFontsStyle(fontSize: 14.0, height: 1.29, color: milestone.isExpanded! ? darkYellow : grey),
                          ),
                          // dropdown icon
                          AnimatedRotation(turns: milestone.isExpanded! ? 1.0 : 0.5, duration: Duration(milliseconds: 450), child: SvgPicture.asset(ic_dropdown, color: milestone.isExpanded! ? darkYellow : grey))
                        ],
                      ),
                      SizedBox(height:16.0)
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  // subtask List View
  Widget subtaskListView(BuildContext context, TaskMilestoneDetail milestone) => ListView.separated(
    itemCount: milestone.tasks!.length,
    physics: NeverScrollableScrollPhysics(),
    shrinkWrap: true,
    separatorBuilder: (context, index) => Divider(height: 1.0, color: shimmerGrey),
    itemBuilder: (context, index) {
      Status _subtaskStatus = statusConfig(milestone.tasks![index].taskStatus ?? '', iconWidth: 24.0);

      return GestureDetector(
        onTap: () async {
          // TODO: go to edit task page
          if ((milestone.tasks![index].taskStatus == 'completed' && (milestone.tasks![index].totalSubTask ?? 0) == 0) || milestone.tasks![index].taskStatus == 'flagged') {
            goToNextNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: milestone.tasks![index].taskID));
          } else {
            final dynamic _response = await Navigator.pushNamed(
              context,
              taskFormScreen,
              arguments: TaskFormArguments(
                teamMemberList: taskProjectListResponse.response?.teamMemberList,
                milestoneList: taskProjectListResponse.response?.milestoneList,
                projectID: widget.projectID,
              ),
            );
            // show delay pop
            if (_response.taskRescheduled == true && _response.taskList != []) {
              bool decision = await showDecision(
                context, 
                "Do you want the following tasks to be delayed accordingly?", 
                "Nope", 
                "Yes, Delay"
              );
              if (decision) {
                goToNextNamedRoute(
                  context, 
                  taskRescheduleScreen, 
                  args: LivePreviewPostModel(
                    projectID: widget.projectID, 
                    taskIDs: _response.rescheduledTaskIDArr, 
                    delayDuration: _response.rescheduledAmount,
                  )
                );
              }
            } else {
              _milestonePagingController.refresh();
            }
          }
        },
        child: milestone.tasks![index].taskStatus == 'completed'
          ? Opacity(
            opacity: (milestone.tasks![index].totalSubTask ?? 0) > 0 ? 1.0 : 0.7,
            child: Container(
                padding: EdgeInsets.all(20.0),
                color: white,
                child: subTaskItemContent(milestone.tasks![index], _subtaskStatus),
              ),
          )
          : LongPressDraggable<DragArguments>(
            data: DragArguments(
              milestone: milestone,
              task: milestone.tasks![index],
            ),
            child: Opacity(
              opacity: milestone.tasks![index].taskStatus != 'flagged' ? 1.0 : 0.7,
              child: Container(
                padding: EdgeInsets.all(20.0),
                color: white,
                child: subTaskItemContent(milestone.tasks![index], _subtaskStatus),
              ),
            ),
            axis: Axis.vertical,
            feedback: Container(
              width: MediaQuery.of(context).size.width,
              color: transparent,
              child: Center(
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 12.0),
                  decoration: BoxDecoration(
                    color: white,
                    borderRadius: BorderRadius.circular(12.0),
                    boxShadow: drag_box_shadow,
                  ),
                  child: subTaskItemContent(milestone.tasks![index], _subtaskStatus),
                ),
              ),
            ),
            childWhenDragging: Container(
              height: 100.0,
              color: whiteGrey,
            ),
            onDragStarted: () {
              setState(() {
                onDragging = true;
                fromList = milestone.milestoneCategoryID;
                _changeBorderColor = false;
              });
            },
            onDragEnd: (details) {
              setState(() {
                onDragging = false;
              });
            },
            onDraggableCanceled: (velocity, offset) {
              setState(() {
                onDragging = false;
              });
            },
          ),
      );
    },
  );

  Widget subTaskItemContent(TaskItemModel item, Status config) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        // subtask's leading icon
        item.taskStatus == 'completed' ? config.icon : SvgPicture.asset(ic_drag, width: 24.0),
        SizedBox(width: 16.0),
        // subtask's content
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                decoration: BoxDecoration(color: item.taskStatus == 'completed' ? shimmerGrey : transparent, border: Border.all(color: shimmerGrey), borderRadius: BorderRadius.circular(500)),
                child: RichText(
                    text: TextSpan(children: [
                  if (item.taskStatus == 'completed') TextSpan(text: "Completed: ", style: regularFontsStyle(color: grey, fontSize: 12.0, height: 1.33)),
                  TextSpan(
                      text: item.taskStatus == 'completed'
                        ? formatDatetime(datetime: item.completedDateTime ?? '')
                        : formatDatetime(datetime: item.taskTargetDateTime ?? ''),
                      style: boldFontsStyle(color: grey, fontSize: 12.0, height: 1.33)),
                ])),
              ),
              SizedBox(height: 8.0),
              RichText(
                text: TextSpan(
                  children: [
                    if (item.isSpecial ?? false)
                      WidgetSpan(
                        alignment: PlaceholderAlignment.middle,
                        child: Padding(
                          padding: EdgeInsets.only(right: 8.0),
                          child: SvgPicture.asset(ic_star, width: 18.0),
                        ),
                      ),
                    TextSpan(
                      text: item.taskName ?? '',
                      style: boldFontsStyle(fontSize: 16.0, height: 1.5),
                    ),
                  ]
                ),
              ),
              SizedBox(height: 8.0),
              TaskLabels(
                isShowSubtaskLabel: (item.totalSubTask ?? 0) > 0,
                isFileRequiredLabel: item.isFileRequiredLabel ?? '',
                totalSubTask: item.totalSubTask ?? 0,
                completedSubTask: item.completedSubTask ?? 0,
                isShowFileRequireLabel: (item.isFileRequired ?? 0) == 1 && item.taskStatus != 'completed',
                replyCount: item.replyCount ?? 0,
                taskID: item.taskID!, 
                isRead: item.isRead ?? ''
              ),
            ],
          ),
        ),
      ],
    );
  }

  // build button submit button
  Widget _buildSubmitButton(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(24.0),
      decoration: BoxDecoration(
        color: darkGrey,
        borderRadius: BorderRadius.vertical(top: Radius.circular(30.0)),
      ),
      child: Row(
        children: [
          Expanded(
            child: OutlinedButton(
              onPressed: () async {
                bool decision = await showDecision(
                  context, 
                  'Are you sure you want to quit editor mode without saving.', 
                  'Cancel', 
                  "Don't Save",
                );
                if (decision) {
                  Navigator.pop(context);
                }
              },
              child: Text(
                'Cancel',
                style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: white),
              ),
              style: OutlinedButton.styleFrom(
                backgroundColor: transparent,
                padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
                side: BorderSide(color: white),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
              ),
            ),
          ),
          SizedBox(width: 16.0),
          Expanded(
            child: ElevatedButton(
              onPressed: () async {
                // call edit API
                MilestoneListUpdateModel data = MilestoneListUpdateModel(taskList: projectList);
                await _taskService.postMilestoneListUpdate(data).then((response) {
                  showToastMessage(context, message: 'Update Successful');
                  _milestonePagingController.refresh();
                }).onError((error, stackTrace) {
                  showToastMessage(context, message: error.toString());
                });

                Navigator.pop(context);
              },
              child: Text(
                'Submit',
                style: boldFontsStyle(fontSize: 16.0, height: 1.5),
              ),
              style: ElevatedButton.styleFrom(
                backgroundColor: yellow,
                padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DragArguments {
  final TaskMilestoneDetail milestone;
  final TaskItemModel task;

  DragArguments({required this.milestone, required this.task});
}
