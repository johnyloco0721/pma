import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/models/profile_model.dart';
import 'package:sources/services/app_service.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/utils/size_utils.dart';
import 'package:sources/utils/util_helper.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with TickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;
  bool isMaintenance = false;

  @override
  void didUpdateWidget(SplashScreen oldWidget) {
    _controller.reset();
    _controller.forward();
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = AnimationController(duration: const Duration(milliseconds: 2000), vsync: this, value: 0.5);
    _animation = CurvedAnimation(parent: _controller, curve: Curves.bounceInOut);
    _controller.forward();
    Future.delayed(Duration(milliseconds: 2000)).then((value) {
      checkOSVersion();
      goToNext();
    });
  }

  @override
  dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    setStatusBarIconColor(iconColor: Brightness.light);

    return Scaffold(
      backgroundColor: yellow,
      body: Center(
        child: ScaleTransition(
          scale: _animation,
          alignment: Alignment.center,
          child: Container(
            width: SizeUtils.width(context, 60),
            child: AspectRatio(
              aspectRatio: 245.58 / 69.05,
              child: Image.asset(
                img_makeover_logo,
                fit: BoxFit.fill,
              ),
            ),
          ),
        ),
      ),
    );
  }

  checkOSVersion() async {
    if (Platform.isAndroid) {
      var androidData = await DeviceInfoPlugin().androidInfo;
      var splitVersionNumber = (androidData.version.release).split(".");
      String temp = "";
      for (int i=1; i < splitVersionNumber[0].length; i++) {
        temp += "0";
      }
      if ((androidData.version.release).compareTo(temp+'6.0') < 0) {
        goAndClearAllPageNamed(context, AndroidosRequireScreen);
      }
    }
    if (Platform.isIOS) {
      var iosData = await DeviceInfoPlugin().iosInfo;
      var splitVersionNumber = (iosData.systemVersion).split(".");
      String temp = "";
      for (int i=2; i < splitVersionNumber[0].length; i++) {
        temp += "0";
      }
      if ((iosData.systemVersion).compareTo(temp+'12.4') < 0) {
        goAndClearAllPageNamed(context, IOSosRequireScreen);
      }
    }
  }

  void goToNext() async {
    bool result = await appVersionComparison();
    if (result) {
      try {
        await AppService.getProfile().then((response) {
          ProfileModel profileModel = ProfileModel.fromJson(response.data);
          if (profileModel.status == true) {
            var json = response.data['response']['profileDetails'];
            SpManager.saveUser(json);
          }
        });
        List<dynamic> syncedAPIList = SpManager.getSyncedAPI();
        if (syncedAPIList.length < 5) {
          goAndClearAllPageNamed(context, loadDataScreen);
        } else {
          goAndClearAllPageNamed(context, homeScreen);
        }
      }
      catch (e) {
        if (e == 'Token mismatched.') {
          goAndClearAllPageNamed(context, loginScreen);
        }
        print('failed Profile API');
      }
    }
  }
}