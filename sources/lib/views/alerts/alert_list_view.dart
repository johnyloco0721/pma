import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:lottie/lottie.dart';
import 'package:sources/args/chat_reply_args.dart';
import 'package:sources/args/task_args.dart';
import 'package:sources/args/workplace_args.dart';
import 'package:sources/args/workplace_task_milestone_args.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_routes.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/models/alert_post_model.dart';
import 'package:sources/models/task_live_preview_post_model.dart';
import 'package:sources/services/alert_service.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/scroll.utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/view_models/app_view_model.dart';
import 'package:sources/widgets/skeleton.dart';

import '../../models/alert_response_model.dart';

class AlertView extends StatefulWidget {
  const AlertView({Key? key}) : super(key: key);

  @override
  _AlertViewContent createState() => _AlertViewContent();
}

class _AlertViewContent extends State<AlertView> {
  final database = locator<AppDatabase>();
  final AppViewModel appViewModel = locator<AppViewModel>();
  ScrollController _scrollController = ScrollController();
  bool isAlertListLoading = false;
  AlertService _alertService = AlertService();
  String lastUpdateDateTime = '';

  int lastAlertIndex = 0;
  int todayFirstItemID = -1;
  int pastFirstItemID = -1;
  bool isTodayAllRead = false;
  bool isPastAllRead = false;

  int taskID = 0;

  @override
  void initState() {
    // implement initState
    super.initState();
    initialize();
  }

  void initialize() async {
    await database.alertDao.getLatestUpdateAlert().then((latestUpdateAlert) {
      lastUpdateDateTime = latestUpdateAlert!.updatedDateTime!;
    }).onError((error, stackTrace) {
      print('Latest Update Datetime no found.');
    });
    await _alertService.getAlertList(lastUpdateDateTime: lastUpdateDateTime);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: PageStorageKey<String>('AlertList'),
      backgroundColor: white,
      appBar: _buildAppBar(context),
      body: RefreshIndicator(
        color: yellow,
        backgroundColor: white,
        onRefresh: () async {
          await database.alertDao.getLatestUpdateAlert().then((latestUpdateAlert) {
            lastUpdateDateTime = latestUpdateAlert!.updatedDateTime!;
          }).onError((error, stackTrace) {
            print('Latest Update Datetime no found.');
          });
          return _alertService.getAlertList(lastUpdateDateTime: lastUpdateDateTime);
        },
        child: _buildAlertList(context),
      ),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.dark,
      title: Text('Alert', maxLines: 1, overflow: TextOverflow.ellipsis, style: boldFontsStyle(fontFamily: platForm, fontSize: 35.0, letterSpacing: 0.5)),
      flexibleSpace: GestureDetector(
        onTap: () {
          // onTap app bar empty space then reset list scroll position
          scrollToTop(_scrollController);
        },
      ),
      backgroundColor: white,
      surfaceTintColor: white,
      centerTitle: false,
      automaticallyImplyLeading: false,
      elevation: 0,
    );
  }

  // Alert list stream builder
  StreamBuilder<List<Alert>> _buildAlertList(BuildContext context) {
    final alertDao = database.alertDao;

    return StreamBuilder(
      stream: alertDao.watchAllAlerts(),
      builder: (BuildContext context, AsyncSnapshot<List<Alert>> snapshot) {
        resetData();
        List<Alert> alerts = snapshot.data ?? [];
        var itemCount = snapshot.hasData ? snapshot.data?.length : 0;
        // used to separate today and past alert data
        String today = formatDatetime(datetime: DateTime.now().toString(), format: "y-MM-dd 00:00:00");
        List<Alert> todayItemList = alerts.where((e) => today.compareTo(e.createdDateTime ?? '') <= 0).toList();
        int todayItemCount = todayItemList.length;

        // get today & past first alert ID
        if (alerts.length > 0) {
          if (todayItemCount > 0) todayFirstItemID = todayItemList[0].notificationID;
          if (alerts.length > todayItemCount) pastFirstItemID = alerts[todayItemCount].notificationID;
        }

        // used to hide/show 'Read All' button
        isTodayAllRead = todayItemList.where((e) => e.isRead == 'unread').length == 0;
        isPastAllRead = alerts.where((e) => today.compareTo(e.createdDateTime ?? '') > 0).toList().where((e) => e.isRead == 'unread').length == 0;

        if (isAlertListLoading) {
          return _buildSkeleton(context);
        } else if (snapshot.hasData && (itemCount != null && itemCount > 0)) {
          return ListView.separated(
              physics: const AlwaysScrollableScrollPhysics(),
              controller: _scrollController,
              key: PageStorageKey<String>('AlertList'),
              itemCount: itemCount,
              separatorBuilder: (context, index) => Padding(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Divider(thickness: 0.5, height: 0, color: lightGrey),
                  ),
              itemBuilder: (BuildContext context, int index) {
                final alertItem = alerts[index];
                lastAlertIndex = itemCount - 1;
                return _buildListItem(context, alertItem, index, index < todayItemCount);
              });
        } else {
          // Use list view so that user can still refresh the list if no items
          return ListView.builder(
              itemCount: 1,
              itemBuilder: (BuildContext context, int index) {
                return _buildEmptyResult(context);
              });
        }
      },
    );
  }

  // Alert list item widget
  Widget _buildListItem(BuildContext context, Alert alertItem, int index, bool isToday) {
    return Column(
      children: [
        if (todayFirstItemID == alertItem.notificationID || pastFirstItemID == alertItem.notificationID) buildTodayOrPastSeparated(isToday),
        Opacity(
          opacity: alertItem.isRead == 'read' ? 0.5 : 1.0,
          child: ElevatedButton(
            onPressed: () async {
              _alertService.updateNotificationStatus(AlertPostModel(alertItem.notificationID.toString(), alertItem.allNotificationTxt)).then((response) {
                if(alertItem.moduleName == 'task-comment'){
                  setState(() {
                    String affectedModuleInfoJSON = response.data['response'][0]['affectedModuleInfoJSON'];
                    AlertResponseModel alertInfo = AlertResponseModel.fromJson(jsonDecode(affectedModuleInfoJSON));
                    taskID = alertInfo.taskID ?? 0;
                    goToNextNamedRoute(context, taskCommentScreen, args: taskID);
                  });
                }
                else{
                  Alert alert = Alert.fromJson(response.data['response'][0]);
                  database.alertDao.updateAlertByID(alert.notificationID);
                }
              }).onError((error, stackTrace) {
                print('Called NotificationDetail API Failed!');
              });

              if (alertItem.moduleName == 'task-details' || alertItem.moduleName == 'task') {
                goToNextNamedRoute(context, taskDetailsScreen, args: TaskArguments(taskID: int.parse(alertItem.affectedModuleID ?? '')));
              } else if (alertItem.moduleName == 'task-milestone') {
                goToNextNamedRoute(context, taskMilestoneScreen, args: TaskArguments(projectID: int.parse(alertItem.affectedModuleID ?? '')));
              } else if (alertItem.moduleName == 'workplace') {
                // navigate to chat
                goToNextNamedRoute(context, 'workplaceChat', args: WorkplaceArguments(alertItem.projectID, null, null, null, null));
              } else if (alertItem.moduleName == 'workplace-reply') {
                goToNextNamedRoute(context, 'replyChat', args: ChatReplyArguments(alertItem.projectID, alertItem.replyToID!));
              } else if (alertItem.moduleName == 'project-date-change') {
                goToNextNamedRoute(context, taskRescheduleScreen, args: LivePreviewPostModel(projectID: alertItem.projectID, taskIDs: alertItem.delayTaskIDs?.split("|"), delayDuration: alertItem.delayDuration));
                // goToNextNamedRoute(context, taskRescheduleScreen, args: LivePreviewPostModel(projectID: 999, taskIDs: [31959, 32095], delayDuration: 3));
              } else if ((alertItem.moduleName ?? "").length >= 21 && (alertItem.moduleName ?? "").substring(0, 21) == "task-by-project--list") {
                goToNextNamedRoute(context, workplaceTaskMilestoneScreen, args: WorkplaceTaskMilestoneArguments(int.parse(alertItem.affectedModuleID ?? ''), alertItem.moduleName, null));
              } 
              // else {
              //   HomeView.of(context)!.navigateFromRootToPage('workplaceList', alertItem.projectID, alertItem.replyToID, null, null);
              // }
            },
            style: ElevatedButton.styleFrom(
              backgroundColor: white,
              elevation: 0,
              foregroundColor: lightGrey,
              padding: EdgeInsets.symmetric(horizontal: 20),
            ),
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 22),
              width: double.infinity,
              child: Stack(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width * 0.82,
                        child: HtmlWidget(alertItem.description ?? '', // - Jonathan request change alert description show full sentence at 27 Apr 2022
                            textStyle: regularFontsStyle(height: 1.32)),
                      ),
                      Padding(padding: EdgeInsets.only(top: 12)),
                      Row(
                        children: [
                          Text(pastTime(alertItem.createdDateTime ?? '', isToday), style: regularFontsStyle(fontSize: 15.0, color: grey, height: 1.27)),
                          if (!isToday)
                            Row(
                              children: [
                                Container(
                                  width: 4.0,
                                  height: 4.0,
                                  margin: EdgeInsets.all(8.0),
                                  decoration: BoxDecoration(
                                    color: grey,
                                    borderRadius: BorderRadius.circular(500.0),
                                  ),
                                ),
                                Text(formatDatetime(datetime: alertItem.createdDateTime ?? '', format: 'hh:mma').toLowerCase(), style: regularFontsStyle(fontSize: 15.0, color: grey, height: 1.27)),
                              ],
                            ),
                        ],
                      ),
                    ],
                  ),
                  if (alertItem.isRead != 'read')
                    Positioned(
                        right: 0,
                        child: Container(
                          width: 10,
                          height: 10,
                          decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(5)), color: red),
                        ))
                ],
              ),
            ),
          ),
        ),
        if (index == lastAlertIndex) ...[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Divider(
              height: 0.5,
              thickness: 0.5,
              color: lightGrey,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, bottom: 30),
            child: Text(
              'You have caught up with all the notifications!',
              style: regularFontsStyle(fontSize: 15.0, height: 1.27),
            ),
          )
        ]
      ],
    );
  }

  Widget buildTodayOrPastSeparated(bool isToday) {
    return Container(
      padding: EdgeInsets.only(top: 20, left: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(isToday ? 'Today' : 'Past Notifications', style: boldFontsStyle(height: 1.32)),
          if ((!isTodayAllRead && isToday) || (!isPastAllRead && !isToday))
            GestureDetector(
              onTap: () {
                _alertService.postNotificationReadAll(context, isToday).then((value) async {
                  await database.alertDao.getLatestUpdateAlert().then((latestUpdateAlert) {
                    lastUpdateDateTime = latestUpdateAlert!.updatedDateTime!;
                  }).onError((error, stackTrace) {
                    print('Latest Update Datetime no found.');
                  });
                  return _alertService.getAlertList(lastUpdateDateTime: lastUpdateDateTime);
                });
              },
              child: Container(
                margin: EdgeInsets.only(right: 24.0),
                padding: EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                  border: Border.all(),
                  borderRadius: BorderRadius.all(Radius.circular(12.0)),
                ),
                child: Row(
                  children: [
                    SvgPicture.asset(ic_close_small, width: 20.0),
                    SizedBox(width: 4.0),
                    Text(
                      'Read All',
                      style: regularFontsStyle(fontSize: 17.0, height: 1.35, color: black),
                    )
                  ],
                ),
              ),
            )
        ],
      ),
    );
  }

  resetData() {
    todayFirstItemID = -1;
    pastFirstItemID = -1;
    isTodayAllRead = false;
    isPastAllRead = false;
  }

  Duration checkDifferent(String stringDate) {
    DateTime date = DateTime.parse(stringDate + 'Z');
    return DateTime.now().difference(date.subtract(Duration(hours: 8)));
  }

  String pastTime(String stringDate, bool isToday) {
    Duration difference = checkDifferent(stringDate);
    String result;

    if (!isToday) {
      result = formatDatetime(datetime: stringDate);
    } else if (difference.inHours == 1) {
      result = difference.inHours.toString() + " hour ago";
    } else if (difference.inHours > 1) {
      result = difference.inHours.toString() + " hours ago";
    } else if (difference.inMinutes > 1) {
      result = difference.inMinutes.toString() + " mins ago";
    } else {
      result = "Just now";
    }
    return result;
  }

  // Skeleton widget
  Widget _buildSkeleton(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 50),
      width: MediaQuery.of(context).size.width,
      child: ListView(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        children: [
          for (int i = 0; i < 8; i++)
            Column(
              children: [
                if (i == 0)
                  Container(
                    height: MediaQuery.of(context).size.height * 0.04,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SkeletonContainer.rounded(width: MediaQuery.of(context).size.width * 0.11, height: 12),
                      ],
                    ),
                  ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 22),
                  width: double.infinity,
                  height: 125,
                  child: Stack(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SkeletonContainer.rounded(width: MediaQuery.of(context).size.width * 0.55, height: 12),
                          Padding(padding: EdgeInsets.only(top: 10)),
                          SkeletonContainer.rounded(width: MediaQuery.of(context).size.width * 0.69, height: 12),
                          Padding(padding: EdgeInsets.only(top: 20)),
                          SkeletonContainer.rounded(width: MediaQuery.of(context).size.width * 0.18, height: 12),
                        ],
                      )
                    ],
                  ),
                )
              ],
            )
        ],
      ),
    );
  }

  // Empty result widget
  Widget _buildEmptyResult(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.75,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Lottie.asset(
              'assets/lotties/empty_alert.json',
              width: 250.0,
              height: 250.0,
              fit: BoxFit.fill,
            ),
            Container(
              width: 250.0,
              child: Text(
                'There’s no notifications yet.',
                style: regularFontsStyle(fontSize: 17.0, height: 1.35),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

