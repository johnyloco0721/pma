import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/utils/text_utils.dart';

import 'package:sources/database/app_database.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/services/profile_service.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:sources/widgets/skeleton.dart';

class ProfileInfoView extends StatefulWidget {
  const ProfileInfoView({ Key? key }) : super(key: key);

  @override
  _ProfileViewContent createState() => _ProfileViewContent();
}

class _ProfileViewContent extends State<ProfileInfoView> {
  ProfileService _profileService = ProfileService();
  bool isProfileLoading = false;

  @override
  void initState() {
    // implement initState
    super.initState();
    // isProfileLoading = true;

    _profileService.getProfileList().then((value) {
      // setState(() {
      //   isProfileLoading = false;
      // });
    });
  }

  @override
  Widget build(BuildContext context) {
    ProfileService _profileService = ProfileService();

    return Scaffold(
      key: PageStorageKey<String>('ProfileInfo'),
      backgroundColor: white,
      appBar: _buildAppBar(context),
      body: RefreshIndicator(
        color: yellow,
        backgroundColor: white,
        onRefresh: _profileService.getProfileList,
        child: SingleChildScrollView(
          child: isProfileLoading
            ? _buildSkeleton(context)
            : _buildProfileList(context),
        ),
      ),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: black,
      systemOverlayStyle: SystemUiOverlayStyle.light,
      title: Text(
        'Personal Information',
        style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, color: white, letterSpacing: 0.8)
      ),
      centerTitle: false,
      automaticallyImplyLeading: false,
      elevation: 0,
      leading: Builder(
        builder: (BuildContext context) {
          return IconButton(
            icon: SvgPicture.asset(
              'assets/icons/Arrow_Back.svg',
              fit: BoxFit.scaleDown,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          );
        },
      ),
    );
  }

  // Profile info stream builder
  StreamBuilder<Profile> _buildProfileList(BuildContext context) {
    final database = locator<AppDatabase>();
    final profileDao = database.profileDao;

    final List<String> label = <String>['Branch', 'Email', 'Gmail', 'Address'];
    List<String> info = ['', '', '', ''];
    bool loading = true;

    return StreamBuilder(
      stream: profileDao.watchProfile(),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        final profile = snapshot.data ?? [];
        if (snapshot.hasData) {
          info = <String>[profile.branch, profile.email, profile.gmail, profile.address];
          loading = false;
          return Container(
            padding: EdgeInsets.all(24),
            child: Column(
              children: [
                SizedBox(height: 25),
                ClipOval(
                  child: CachedNetworkImage(
                    width: MediaQuery.of(context).size.width * 0.34,
                    height: MediaQuery.of(context).size.width * 0.34,
                    placeholder: (context, url) => Container(color: Colors.grey[50]),
                    imageUrl: profile.avatar ?? '',
                    fit: BoxFit.cover,
                    alignment: Alignment.center,
                    maxHeightDiskCache: 120,
                    maxWidthDiskCache: 120,
                    errorWidget: (context, string, dynamic) {
                      return Image.asset(img_default_avatar, width: MediaQuery.of(context).size.width * 0.34);
                    }
                  )
                ),
                SizedBox(height: 16),
                Text(
                  profile.name,
                  style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, height: 1.3, letterSpacing: 0.8)
                ),
                SizedBox(height: 8),
                Text(
                  profile.designationName,
                  style: regularFontsStyle(color: grey, fontSize: 17.0, height: 1.35)
                ),
                SizedBox(height: 24),
                Container(
                  child: ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) => buildListTile(label[index], info[index], context, loading),
                    itemCount: 4
                  ),
                ),
              ],
            ),
          );
        } else {
          loading = true;
          return _buildSkeleton(context);
        }
      },
    );
  }

  Widget buildListTile(String label, String info, BuildContext context, bool loading) {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      child: ListTile(
        contentPadding: EdgeInsets.zero,
        title: loading ?
        SkeletonContainer.rounded(
          width: MediaQuery.of(context).size.width * 0.18,
          height: MediaQuery.of(context).size.height * 0.015
        ) : Text(
          label != '' ? label : '-',
          style: regularFontsStyle(color: grey, fontSize: 17.0, height: 1.35)
        ),
        subtitle: Container(
          padding: EdgeInsets.symmetric(vertical: loading ? 16 : 8),
          child: loading ? 
          SkeletonContainer.rounded(
            width: MediaQuery.of(context).size.width * 0.6,
            height: MediaQuery.of(context).size.height * 0.015
          ) : 
          Text(
            info != '' ? info : '-',
            style: regularFontsStyle(height: 1.32)
          ),
        )
      )
    );
  }

  // Skeleton widget
  Widget _buildSkeleton(BuildContext context) {
    final List<String> label = <String>['Branch', 'Email', 'Gmail', 'Address'];
    List<String> info = ['', '', '', ''];

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 24.0),
      child: Column(
        children: [
          Padding(padding: EdgeInsets.only(top: 25.0)),
          SkeletonContainer.circular(
              width: MediaQuery.of(context).size.width * 0.34,
              height: MediaQuery.of(context).size.height * 0.17
          ),
          Padding(padding: EdgeInsets.only(top: 16.0)),
          SkeletonContainer.rounded(
              width: MediaQuery.of(context).size.width * 0.18,
              height: MediaQuery.of(context).size.height * 0.015
          ),
          Padding(padding: EdgeInsets.only(top: 16.0)),
          SkeletonContainer.rounded(
              width: MediaQuery.of(context).size.width * 0.33,
              height: MediaQuery.of(context).size.height * 0.015
          ),
          Padding(padding: EdgeInsets.only(top: 24.0)),
          Container(
            height: MediaQuery.of(context).size.height * 0.4,
            child: ListView.builder(
                itemBuilder: (BuildContext context, int index) => buildListTile(label[index], info[index], context, true),
                itemCount: info.length
            ),
          ),
        ],
      ),
    );
  }
}
