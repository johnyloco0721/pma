import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:page_transition/page_transition.dart';

import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/views/profiles/profile_main_view.dart';

StreamBuilder supportSuccessView({bool loading = true}) {
  return StreamBuilder(
    stream: null,
      builder: (BuildContext context, loading) {
        return Scaffold(
          backgroundColor: white,
          appBar: AppBar(
            backgroundColor: black,
            systemOverlayStyle: SystemUiOverlayStyle.light,
            title: Text(
              'Support',
              style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, color: white, height: 1.3, letterSpacing: 0.8)
            ),
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  icon: SvgPicture.asset(
                    'assets/icons/Arrow_Back.svg',
                    fit: BoxFit.scaleDown,
                  ),
                  onPressed: () {
                    goToNormalPageWithTransition(context, ProfileView(), transitionType: PageTransitionType.leftToRight);
                  },
                );
              },
            ),
          ),
          body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Lottie.asset(
                    'assets/lotties/thank_you.json',
                    width: 300.0,
                    height: 300.0,
                    fit: BoxFit.fill,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.6,
                    child: Text(
                      'Thanks you! Your request / feedback is sent to our team',
                      style: regularFontsStyle(fontSize: 17.0, height: 1.35),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              )
          )
        );
      }
  );
}
