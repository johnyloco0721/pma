import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sources/constants/app_box_shadow.dart';
import 'package:sources/constants/app_data_models.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/models/config_model.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/view_models/app_view_model.dart';
import 'package:sources/views/profiles/support_success_view.dart';
import 'package:sources/widgets/skeleton.dart';
import 'package:sources/services/api.dart';

class SupportView extends StatefulWidget {
  @override
  _SupportFormState createState() => _SupportFormState();
}

class _SupportFormState extends State<SupportView> {
  AppViewModel appViewModel = locator<AppViewModel>();
  bool loading = true;
  GlobalKey dropdownKey = GlobalKey();
  final enquiryController = TextEditingController();
  bool enquiryNull = false;
  late double width, height, xPosition, yPosition;
  bool _isDropdownOpened = false;
  String selectedValue = "";
  late SupportTopic topic;
  int onlyClickOnce = 0;

  @override
  void initState() {
    loading = true;

    loadData();
    enquiryController.addListener(() {
      enquiryNull = !checkNull(enquiryController.text);
    });
    super.initState();
  }

  Future loadData() async {
    ConfigDetails? config = SpManager.getConfigData();
    if (config != null) {
      topic = config.support!.topic!;
    } else {
      if (AppDataModel.getByPassCode()) {
        await Api().getHTTP('Config?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}').then((response) {
          topic = SupportTopic.fromJson(response.data['response']['support']['topic']);
        }).onError((error, stackTrace) {
          print('Called Config API Failed!');
        }); 
      }else{
        await Api().getHTTP('Config').then((response) {
          topic = SupportTopic.fromJson(response.data['response']['support']['topic']);
        }).onError((error, stackTrace) {
          print('Called Config API Failed!');
        }); 
      }
    }

    setState(() {
      loading = false;
    });
  }

  bool checkNull(value) {
    bool trueFalse = false;
    setState(() {
      if (value == null || value.isEmpty) {
        trueFalse = true;
      }
    });
    return trueFalse;
  }

  void findDropdownData() {
    final RenderBox renderBox = dropdownKey.currentContext?.findRenderObject() as RenderBox;
    height = renderBox.size.height;
    width = renderBox.size.width;
    Offset offset = renderBox.localToGlobal(Offset.zero);
    xPosition = offset.dx;
    yPosition = offset.dy;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: PageStorageKey<String>('ProfileMain'),
      backgroundColor: white,
      appBar: _buildAppBar(context),
      body: SingleChildScrollView(
        child: loading
          ? _buildSkeleton(context)
          : _buildSupportForm(context),
      ),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: black,
      systemOverlayStyle: SystemUiOverlayStyle.light,
      title: Text(
        'Support',
        style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, color: white, letterSpacing: 0.8),
      ),
      centerTitle: false,
      automaticallyImplyLeading: false,
      elevation: 0,
      leading: Builder(
        builder: (BuildContext context) {
          return IconButton(
            icon: SvgPicture.asset(
              'assets/icons/Arrow_Back.svg',
              fit: BoxFit.scaleDown,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          );
        },
      ),
    );
  }

  Widget _buildSupportForm(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 25),
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.6,
          child: Text(
            'Hello, tell us what can we help you with:',
            style: boldFontsStyle(height: 1.32),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: 24),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                key: dropdownKey,
                onTap: () {
                  setState(() {
                    FocusScope.of(context).unfocus();

                    if (!_isDropdownOpened) {
                      findDropdownData();
                    }

                    _isDropdownOpened = !_isDropdownOpened;
                  });
                },
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 14, horizontal: 24),
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: _isDropdownOpened
                              ? darkYellow
                              : grey,
                          width: _isDropdownOpened ? 1.5 : 0.5),
                      borderRadius: BorderRadius.all(Radius.circular(12.0))),
                  child: Row(
                    children: [
                      Text(
                        selectedValue != '' ? selectedValue : 'Select Topic',
                        style: selectedValue != '' ? regularFontsStyle(height: 1.32) : italicFontsStyle(color: grey, height: 1.32)
                      ),
                      Spacer(),
                      SvgPicture.asset(
                        ic_dropdown,
                        color: black,
                        fit: BoxFit.scaleDown,
                      ),
                    ],
                  ),
                ),
              ),
              if (_isDropdownOpened) buildDropdown(height),
              SizedBox(height: 16),
              TextFormField(
                onTap: () {
                  if (_isDropdownOpened) {
                    _isDropdownOpened = false;
                  }
                },
                controller: enquiryController,
                cursorColor: black,
                maxLines: 3,
                minLines: 3,
                style: regularFontsStyle(height: 1.32),
                decoration: InputDecoration(
                    hintText: 'Write your message here',
                    hintStyle: italicFontsStyle(color: grey, height: 1.32),
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 14, horizontal: 24),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        borderSide: BorderSide(
                            color: darkYellow,
                            width: 1.5)),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        borderSide: BorderSide(
                            color: grey, width: 0.5))),
              ),
              Padding(padding: EdgeInsets.only(top: 48.0)),
              Opacity(
                opacity: (enquiryNull && selectedValue != '' && onlyClickOnce == 0 ) ? 1.0 : 0.3,
                child: TextButton(
                  child: Text(
                    'Submit',
                    style: boldFontsStyle(height: 1.32),
                  ),
                  style: ButtonStyle(
                    minimumSize: MaterialStateProperty.all<Size>(
                        Size(double.infinity, 60.0)),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    )),
                    backgroundColor: MaterialStateProperty.all(yellow),
                  ),
                  onPressed: enquiryNull && selectedValue != '' ?  () {
                    late String temp;
                    switch(selectedValue) {
                      case 'Ask for help': temp = 'help'; break;
                      case 'Give us feedback': temp = 'feedback'; break;
                      case 'Others': temp = 'others'; break;
                    }
                    if (AppDataModel.getByPassCode()) {
                      setState(() {
                        onlyClickOnce = onlyClickOnce + 1;
                      });

                      if (onlyClickOnce == 1) {
                        Api().postHTTP('Support?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', {"supportTopic": temp, "supportDescription": enquiryController.text}).then((response) {
                          if(response.data['status']){
                            goToNormalPage(context, supportSuccessView());
                          }
                        }).onError((error, stackTrace) {
                          showError(context, 'Something went wrong, please try again!');
                        }).timeout(Duration(seconds: 60), onTimeout: () {
                          setState(() {
                            onlyClickOnce = 0;
                          });
                        }).whenComplete(() {
                          setState(() {
                            onlyClickOnce = 0;
                          });
                        });
                      }
                      
                    }else{
                      setState(() {
                        onlyClickOnce = onlyClickOnce + 1;
                      });

                      if (onlyClickOnce == 1) {
                        Api().postHTTP('Support', {"supportTopic": temp, "supportDescription": enquiryController.text}).then((response) {
                          if(response.data['status']){
                            goToNormalPage(context, supportSuccessView());
                          }
                        }).onError((error, stackTrace) {
                          showError(context, 'Something went wrong, please try again!');
                        }).timeout(Duration(seconds: 60), onTimeout: () {
                          setState(() {
                            onlyClickOnce = 0;
                          });
                        }).whenComplete(() {
                          setState(() {
                            onlyClickOnce = 0;
                          });
                        });
                      }
                    }
                  } : null,
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget buildDropdown(double itemsHeight) {
    return Container(
      margin: EdgeInsets.only(top: 16),
      height: 3 * itemsHeight,
      decoration: BoxDecoration(
        color: white,
        border: Border.all(color: grey, width: 0.5),
        borderRadius: BorderRadius.circular(12),
        boxShadow: app_box_shadow
      ),
      child: Column(
        children: [
          buildDropdownItem(topic.help!),
          buildDropdownItem(topic.feedback!),
          buildDropdownItem(topic.others!),
        ],
      ),
    );
  }

  Widget buildDropdownItem(String text) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedValue = text;
          _isDropdownOpened = !_isDropdownOpened;
        });
      },
      child: Container(
        color: Colors.transparent,
        padding: EdgeInsets.symmetric(vertical: 14, horizontal: 24),
        child: Row(
          children: [
            Text(
              text,
              style: regularFontsStyle(height: 1.32),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSkeleton(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 24.0),
      child: Column(
        children: [
          SizedBox(height: 25),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SkeletonContainer.rounded(
                width: MediaQuery.of(context).size.width * 0.6,
                height: MediaQuery.of(context).size.height * 0.015
              ),
              Padding(padding: EdgeInsets.only(top: 13)),
              SkeletonContainer.rounded(
                width: MediaQuery.of(context).size.width * 0.5,
                height: MediaQuery.of(context).size.height * 0.015
              ),
            ]
          ),
          SizedBox(height: 30),
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 0.063,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: grey,
                      width: 0.5
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(12.0))
                  ),
                ),
                SizedBox(height: _isDropdownOpened ? 16 + (height * 3) : 16),
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 0.14,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: grey,
                      width: 0.5
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(12.0))
                  ),
                ),
                SizedBox(height: 48),
                SkeletonContainer.rounded(
                    width: double.infinity,
                    height: MediaQuery.of(context).size.height * 0.073
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
