import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:sources/constants/app_data_models.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/profile_service.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/view_models/app_view_model.dart';
import 'package:sources/views/profiles/profile_info_view.dart';
import 'package:sources/views/profiles/support_view.dart';
import 'package:sources/widgets/skeleton.dart';

class ProfileView extends StatefulWidget {
  const ProfileView({ Key? key }) : super(key: key);

  @override
  _ProfileViewContent createState() => _ProfileViewContent();
}

class _ProfileViewContent extends State<ProfileView> {
  AppViewModel appViewModel = locator<AppViewModel>();
  ProfileService _profileService = ProfileService();
  bool isProfileLoading = false;

  @override
  void initState() {
    // implement initState
    super.initState();
    // isProfileLoading = true;

    _profileService.getProfileList().then((value) {
      // setState(() {
      //   isProfileLoading = false;
      // });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: PageStorageKey<String>('ProfileMain'),
      backgroundColor: white,
      appBar: _buildAppBar(context),
      body: RefreshIndicator(
        color: yellow,
        backgroundColor: white,
        onRefresh: _profileService.getProfileList,
        child: SingleChildScrollView(
          child: isProfileLoading
            ? _buildSkeleton(context)
            : _buildProfileList(context),
        ),
      ),
    );
  }

  // App bar widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle.dark,
      title: Text(
        'Profile',
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: boldFontsStyle(fontFamily: platForm, fontSize: 35.0, letterSpacing: 0.5)
      ),
      backgroundColor: white,
      surfaceTintColor: white,
      centerTitle: false,
      automaticallyImplyLeading: false,
      elevation: 0,
    );
  }

  // Profile main stream builder
  StreamBuilder<Profile> _buildProfileList(BuildContext context) {    
    final database = locator<AppDatabase>();
    final profileDao = database.profileDao;

    return StreamBuilder(
      stream: profileDao.watchProfile(),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        final profile = snapshot.data ?? [];
        if (snapshot.hasData) {
          return Container(
            padding: EdgeInsets.all(24.0),
            child: Column(
              children: [
                SizedBox(height: 12),
                ClipOval(
                  child: CachedNetworkImage(
                    width: MediaQuery.of(context).size.width * 0.34,
                    height: MediaQuery.of(context).size.width * 0.34,
                    placeholder: (context, url) => Container(color: Colors.grey[50]),
                    imageUrl: profile.avatar ?? '',
                    fit: BoxFit.cover,
                    alignment: Alignment.center,
                    maxHeightDiskCache: 120,
                    maxWidthDiskCache: 120,
                    errorWidget: (context, string, dynamic) {
                      return Image.asset(img_default_avatar, width: MediaQuery.of(context).size.width * 0.34);
                    }
                  )
                ),
                SizedBox(height: 16),
                Text(
                  profile.name,
                  style: boldFontsStyle(fontFamily: platForm, fontSize: 20.0, height: 1.3, letterSpacing: 0.8)
                ),
                SizedBox(height: 8),
                Text(
                  profile.designationName,
                  style: regularFontsStyle(color: grey, fontSize: 17.0, height: 1.35)
                ),
                SizedBox(height: 24),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ListTile(
                        contentPadding: EdgeInsets.only(top: 8, bottom: 8),
                        title: Text(
                          'Personal Information',
                          style: boldFontsStyle(height: 1.32)
                        ),
                        trailing: SvgPicture.asset(
                          'assets/icons/Arrow_Forward.svg'
                        ),
                        onTap: () {
                          goToNormalPage(context, ProfileInfoView());
                        },
                      ),
                      Divider(height: 0.5, color: lightGrey),
                      ListTile(
                        contentPadding: EdgeInsets.only(top: 8, bottom: 8),
                        title: Text(
                          'Support',
                          style: boldFontsStyle(height: 1.32)
                        ),
                        trailing: SvgPicture.asset(
                          'assets/icons/Arrow_Forward.svg'
                        ),
                        onTap: () {
                          goToNormalPage(context, SupportView());
                        },
                      ),
                      Divider(height: 0.5, color: lightGrey),
                    ],
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 32.0)),
                TextButton(
                  child: Text(
                    'Log Out',
                    style: boldFontsStyle(height: 1.32)
                  ),
                  style: ButtonStyle(
                      minimumSize: WidgetStateProperty.all<Size>(
                          Size(double.infinity, 60.0)
                      ),
                      shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0),
                              side: BorderSide(color: black)
                          )
                      )
                  ),
                  onPressed: () {
                    if (AppDataModel.getByPassCode()) {
                      Api().getHTTP('Logout?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}').then((response) { 
                        SpManager.logoutDeleteData();
                        goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, loginScreen);
                      }).onError((error, stackTrace){
                        showError(context, 'Something went wrong, please try again!');
                      });
                    }else{
                      Api().getHTTP('Logout').then((response) { 
                        SpManager.logoutDeleteData();
                        goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, loginScreen);
                      }).onError((error, stackTrace){
                        showError(context, 'Something went wrong, please try again!');
                      });
                    }

                  },
                ),
                SizedBox(height: 12),
                FutureBuilder(
                  future: PackageInfo.fromPlatform(),
                  builder: (context, AsyncSnapshot<PackageInfo> snapdata){
                    String? version = snapdata.data?.version;
                    String? buildNumber = snapdata.data?.buildNumber;
                    String? dateTime = formatDatetime(datetime: DateTime.now().toString(), format: 'yyyyMMdd');
                    return Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(bottom: 15.0),
                      child: Text(
                        'Build: $version.$dateTime.$buildNumber',
                        style: regularFontsStyle(fontSize: 12.0, color: lightGrey),
                      ),
                    );
                  }
                ),
              ],
            ),
          );
        } else {
          return _buildSkeleton(context);
        }
      },
    );
  }

  // Skeleton widget
  Widget _buildSkeleton(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Column(
        children: [
          Padding(padding: EdgeInsets.only(top: 25.0)),
          SkeletonContainer.circular(
              width: MediaQuery.of(context).size.width * 0.34,
              height: MediaQuery.of(context).size.width * 0.34
          ),
          Padding(padding: EdgeInsets.only(top: 16.0)),
          SkeletonContainer.rounded(
              width: MediaQuery.of(context).size.width * 0.18,
              height: MediaQuery.of(context).size.height * 0.015
          ),
          Padding(padding: EdgeInsets.only(top: 16.0)),
          SkeletonContainer.rounded(
              width: MediaQuery.of(context).size.width * 0.33,
              height: MediaQuery.of(context).size.height * 0.015
          ),
          Padding(padding: EdgeInsets.only(top: 24.0)),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ListTile(
                  title: SkeletonContainer.rounded(
                      width: MediaQuery.of(context).size.width * 0.35,
                      height: MediaQuery.of(context).size.height * 0.015
                  ),
                  trailing: SkeletonContainer.circular(
                      width: MediaQuery.of(context).size.width * 0.03,
                      height: MediaQuery.of(context).size.height * 0.015
                  ),
                ),
                Divider(height: 0.5, color: lightGrey),
                ListTile(
                  title: SkeletonContainer.rounded(
                      width: MediaQuery.of(context).size.width * 0.35,
                      height: MediaQuery.of(context).size.height * 0.015
                  ),
                  trailing: SkeletonContainer.circular(
                      width: MediaQuery.of(context).size.width * 0.03,
                      height: MediaQuery.of(context).size.height * 0.015
                  ),
                ),
                Divider(height: 0.5, color: lightGrey),
              ],
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 32.0)),
          TextButton(
            child: Text(''),
            style: ButtonStyle(
                minimumSize: WidgetStateProperty.all<Size>(
                    Size(double.infinity, 60.0)
                ),
                shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        side: BorderSide(color: shimmerGrey)
                    )
                )
            ), onPressed: () {},
          )
        ],
      ),
    );
  }
}