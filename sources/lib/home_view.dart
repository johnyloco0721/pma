import 'package:animations/animations.dart';
import 'package:cron/cron.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:sources/args/chat_reply_args.dart';
import 'package:sources/args/forward_args.dart';
import 'package:sources/args/media_slider_args.dart';
import 'package:sources/args/task_args.dart';
import 'package:sources/args/task_form_args.dart';
import 'package:sources/args/task_search_args.dart';
import 'package:sources/args/workplace_args.dart';
import 'package:sources/args/workplace_more_args.dart';
import 'package:sources/args/workplace_task_milestone_args.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/models/task_live_preview_post_model.dart';
import 'package:sources/services/alert_service.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/services/topic_service.dart';
import 'package:sources/view_models/app_view_model.dart';
import 'package:sources/views/profiles/profile_main_view.dart';
import 'package:sources/views/tasks/task_comment_view.dart';
import 'package:sources/views/tasks/task_details_view.dart';
import 'package:sources/views/tasks/task_main_view.dart';
import 'package:sources/views/tasks/task_milestone_edit_view.dart';
import 'package:sources/views/tasks/task_project_milestone_view.dart';
import 'package:sources/views/tasks/task_reschedule_view.dart';
import 'package:sources/views/tasks/task_search_view.dart';
import 'package:sources/views/tasks/task_form_view.dart';
import 'package:sources/views/workplaces/chat_filter_view.dart';
import 'package:sources/views/workplaces/chat_reply_view.dart';
import 'package:sources/views/workplaces/project_costs_view.dart';
import 'package:sources/views/workplaces/workplace_details_view.dart';
import 'package:sources/views/workplaces/workplace_forward_view.dart';
import 'package:sources/views/workplaces/workplace_medias_view.dart';
import 'package:sources/views/workplaces/topic_selector_view.dart';
import 'package:sources/views/workplaces/workplace_more_view.dart';
import 'package:sources/views/workplaces/workplace_filter_view.dart';
import 'package:sources/views/workplaces/workplace_search_view.dart';
import 'package:sources/views/alerts/alert_list_view.dart';
import 'package:sources/views/workplaces/chat_view.dart';
import 'package:sources/views/workplaces/workplace_list_view.dart';
import 'package:sources/views/workplaces/workplace_task_milestone_view.dart';
import 'package:sources/widgets/connection_status.dart';
import 'package:sources/widgets/media_slider_view.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);
  static _HomeViewState? of(BuildContext context) =>
      context.findAncestorStateOfType<_HomeViewState>();

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final database = locator<AppDatabase>();
  WorkplaceArguments? workplaceArguments;
  AppViewModel appViewModel = locator<AppViewModel>();
  AlertService _alertService = AlertService();
  String lastUpdateDateTime = '';

  Cron? cron;

  @override
  void dispose() {
    super.dispose();
    cron?.close();
  }

  @override
  void initState() {
    super.initState();
    appViewModel.bottomBarCurrentIndex.value = 0;

    // Fetch alerts data from API every 15 seconds
    cron = Cron()
      ..schedule(Schedule.parse('*/15 * * * * *'), () async {
        await database.alertDao
            .getLatestUpdateAlert()
            .then((latestUpdateAlert) {
          lastUpdateDateTime = latestUpdateAlert!.updatedDateTime ??
              DateTime.now().subtract(Duration(minutes: 5)).toString();
        }).onError((error, stackTrace) {
          print('Latest Update Datetime no found.');
        });
        _alertService.getAlertList(lastUpdateDateTime: lastUpdateDateTime);
      });

    initialize();
  }

  void initialize() async {
    TopicService _topicService = TopicService();
    try {
      await _topicService.fetchTopicListAndSaveToDB();
    } on Exception catch (_) {
      print(_);
      return null;
    }
    await database.alertDao.getLatestUpdateAlert().then((latestUpdateAlert) {
      lastUpdateDateTime = latestUpdateAlert!.updatedDateTime!;
    }).onError((error, stackTrace) {
      print('Latest Update Datetime no found.');
    });
    _alertService.getAlertList(lastUpdateDateTime: lastUpdateDateTime);
  }

  void setIndex(int index) {
    if (appViewModel.bottomBarCurrentIndex.value == index) {
      appViewModel.tabsNavigatorKey[index].currentState!
          .popUntil((route) => route.isFirst);
    } else {
      setState(() => appViewModel.bottomBarCurrentIndex.value = index);
    }
  }

  void navigateFromRootToPage(String route, int? projectID, int? feedID,
      String? forwardText, List<String>? filePaths) {
    if (route == 'workplaceList') {
      setState(() {
        workplaceArguments =
            WorkplaceArguments(projectID, feedID, null, forwardText, filePaths);
      });
      setIndex(0);
    } else if (route == 'taskMain') {
      setIndex(1);
    } else if (route == 'alertList') {
      setIndex(2);
    }
  }

  Widget navigatorPages(RouteSettings settings, String from) {
    late Widget page;

    switch (from) {
      case 'task':
        page = TaskMainView();
        break;
      case 'alert':
        page = AlertView();
        break;
      case 'profile':
        page = ProfileView();
        break;
      default:
        page = WorkplaceListView(workplaceArguments: workplaceArguments);
        // Clear workplace argument after redirect
        workplaceArguments = null;
        break;
    }

    if (settings.name == 'workplaceList') {
      final args = settings.arguments as WorkplaceArguments;
      page = WorkplaceListView(workplaceArguments: args);
    }
    if (settings.name == 'workplaceChat') {
      // final args = settings.arguments as int;
      final args = settings.arguments as WorkplaceArguments;
      page = WorkplaceChatView(workplaceArguments: args);
    }
    if (settings.name == 'workplaceSearch') page = WorkplaceSearchView();
    if (settings.name == 'workplaceForward') {
      final args = settings.arguments as ForwardArguments;
      page = WorkplaceForwardView(forwardArguments: args);
    }
    if (settings.name == 'workplaceFilter') page = WorkplaceFilter();
    if (settings.name == 'chatFilter') page = ChatFilter();
    if (settings.name == 'topicSelector') page = TopicSelector();
    if (settings.name == 'replyChat') {
      final args = settings.arguments as ChatReplyArguments;
      page = WorkplaceReplyChatView(replyParentArguments: args);
    }
    if (settings.name == 'profileMore') page = WorkplaceSearchView();
    if (settings.name == 'workplaceMore') {
      final args = settings.arguments as WorkplaceMoreArguments;
      page = ProjectMoreView(workplaceMoreArgs: args);
    }
    if (settings.name == 'workplaceDetails') {
      final args = settings.arguments as int;
      page = ProjectDetailsView(projectId: args);
    }
    if (settings.name == 'costEstimation') {
      final args = settings.arguments as int;
      page = ProjectCostsView(projectId: args);
    }
    if (settings.name == 'workplaceMedias') {
      final args = settings.arguments as int;
      page = ProjectMediasView(projectId: args);
    }
    if (settings.name == workplaceTaskMilestoneScreen) {
      final args = settings.arguments as WorkplaceTaskMilestoneArguments;
      page = WorkplaceTaskMilestoneView(milestoneArgs: args);
    }
    if (settings.name == taskMainScreen) {
      final args = settings.arguments as TaskArguments;
      page = TaskMainView(taskArguments: args);
    }
    if (settings.name == taskMilestoneScreen) {
      final args = settings.arguments as TaskArguments;
      page = TaskProjectMilestoneView(taskArguments: args);
    }
    if (settings.name == taskDetailsScreen) {
      final args = settings.arguments as TaskArguments;
      page = TaskDetailView(taskArguments: args);
    }
    if (settings.name == taskSearchScreen) {
      final args = settings.arguments as TaskSearchArguments;
      page = TaskSearchView(taskSearchArgs: args);
    }
    if (settings.name == taskMilestoneEditScreen) {
      final args = settings.arguments as int;
      page = TaskMilestoneEditorView(projectID: args);
    }
    if (settings.name == taskRescheduleScreen) {
      final args = settings.arguments as LivePreviewPostModel;
      page = TaskRescheduleView(data: args);
    }
    if (settings.name == taskFormScreen) {
      final args = settings.arguments as TaskFormArguments?;
      page = TaskFormView(taskEditArgs: args);
    }
    if (settings.name == mediaSliderScreen) {
      final args = settings.arguments as MediaSliderArguments;
      page = MediaSliderView(mediaSliderArgs: args);
    }
    if (settings.name == 'alert') {
      page = AlertView();
    }
    if (settings.name == taskCommentScreen) {
      final args = settings.arguments as int;
      page = TaskCommentView(taskID: args);
    }

    return page;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        final isFirstRouteInCurrentTab = !await appViewModel
            .tabsNavigatorKey[appViewModel.bottomBarCurrentIndex.value]
            .currentState!
            .maybePop();
        if (isFirstRouteInCurrentTab) {
          // if not on the 'main' tab
          if (appViewModel.bottomBarCurrentIndex.value == 0) {
            // select 'main' tab
            setIndex(0);
            // back button handled by app
            return false;
          }
        }
        // let system handle back button if we're on the first route
        return isFirstRouteInCurrentTab;
      },
      child: Scaffold(
        body: PageTransitionSwitcher(
          child: getViewForIndex(appViewModel.bottomBarCurrentIndex.value),
          duration: const Duration(milliseconds: 300),
          reverse: false,
          transitionBuilder: (Widget child, Animation<double> animation,
              Animation<double> secondaryAnimation) {
            return SharedAxisTransition(
              child: child,
              animation: animation,
              secondaryAnimation: secondaryAnimation,
              transitionType: SharedAxisTransitionType.horizontal,
            );
          },
        ),
        bottomNavigationBar: Container(
          color: black,
          child: SafeArea(
            top: false,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                _bottomNavigationBar(context),
                ConnectionStatusWidget(
                  bgColor: red,
                  msgText: 'No Internet Connection!',
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getViewForIndex(int index) {
    Widget page = WorkplaceListView();

    switch (index) {
      case 0:
        return Offstage(
          offstage: appViewModel.bottomBarCurrentIndex.value != index,
          child: Navigator(
            key: appViewModel.tabsNavigatorKey[index],
            onGenerateRoute: (settings) {
              page = navigatorPages(settings, 'workplace');
              return MaterialPageRoute(builder: (_) => page);
            },
          ),
        );
      case 1:
        return Navigator(
          key: appViewModel.tabsNavigatorKey[index],
          onGenerateRoute: (settings) {
            page = navigatorPages(settings, 'task');
            return MaterialPageRoute(builder: (_) => page);
          },
        );
      case 2:
        return Navigator(
          key: appViewModel.tabsNavigatorKey[index],
          onGenerateRoute: (settings) {
            page = navigatorPages(settings, 'alert');
            return MaterialPageRoute(builder: (_) => page);
          },
        );
      case 3:
        return Navigator(
          key: appViewModel.tabsNavigatorKey[index],
          onGenerateRoute: (settings) {
            page = navigatorPages(settings, 'profile');
            return MaterialPageRoute(builder: (_) => page);
          },
        );
      default:
        page = Container();
        return page;
    }
  }

  Widget _bottomNavigationBar(BuildContext context) {
    final database = locator<AppDatabase>();
    final alertDao = database.alertDao;
    bool allAlertRead = true;

    return StreamBuilder(
      stream: alertDao.watchCountUnreadAlerts(),
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        var itemCount = snapshot.hasData ? snapshot.data?.length : 0;
        allAlertRead =
            !(snapshot.hasData && (itemCount != null && itemCount > 0));
        return Container(
            color: black,
            padding: EdgeInsets.only(top: 4),
            child: Row(
              children: <Widget>[
                _bottomNavigationBarItem(context, ic_workplace_inactive,
                    ic_workplace_active, 0),
                _bottomNavigationBarItem(
                    context, ic_task_inactive, ic_task_active, 1),
                _bottomNavigationBarItem(
                    context,
                    allAlertRead ? ic_alert_inactive : ic_new_alert_inactive,
                    allAlertRead ? ic_alert_active : ic_new_alert_active,
                    2),
                _bottomNavigationBarItem(context, ic_profile_inactive,
                    ic_profile_active, 3),
              ]
            ));
      },
    );
  }

  Widget _bottomNavigationBarItem(BuildContext context, String inactiveIcon,
      String activeIcon, int index) {
    return InkWell(
        onTap: () {
          setIndex(index);
        },
        child: Container(
          height: 60,
          width: MediaQuery.of(context).size.width / 6,
          margin: EdgeInsets.only(
            left: MediaQuery.of(context).size.width / 24,
            right: MediaQuery.of(context).size.width / 24,
          ),
          child: Column(
            children: [
              Container(
                height: 2.0,
                margin: EdgeInsets.only(
                    bottom: 8.0, top: 4.0, left: 5.0, right: 5.0),
                decoration: BoxDecoration(
                    color: index == appViewModel.bottomBarCurrentIndex.value
                        ? yellow
                        : black,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10.0),
                      bottomRight: Radius.circular(10.0),
                    )),
              ),
              SvgPicture.asset(
                  index == appViewModel.bottomBarCurrentIndex.value
                      ? activeIcon
                      : inactiveIcon,
                  fit: BoxFit.none,
                  placeholderBuilder: (context) => SvgPicture.asset(
                        index == appViewModel.bottomBarCurrentIndex.value
                            ? inactiveIcon
                            : activeIcon,
                        fit: BoxFit.none,
                      )),
            ],
          ),
        ));
  }
}
