const String splashScreen = "/splashScreen";
const String loginScreen = "/loginScreen";
const String errorLoginScreen = "/errorLoginScreen";
const String forgotPasswordScreen = "/forgotPasswordScreen";
const String homeScreen = "/homeScreen";
const String loadDataScreen = "/loadDataScreen";
const String appMaintenanceScreen = "/appMaintenanceScreen";
const String appUpdateScreen = "/appUpdateScreen";
const String IOSosRequireScreen = "/IOSosRequireScreen";
const String AndroidosRequireScreen = "/AndroidosRequireScreen";

const String workplaceListScreen = "/workplaceListScreen";
const String workplaceSearchScreen = "/workplaceSearchScreen";
const String workplaceChatScreen = "/workplaceChatScreen";
const String workplaceReplyScreen = "/workplaceReplyScreen";
const String workplaceMoreInfoScreen = "/workplaceMoreInfoScreen";
const String workplaceProjectDetailsScreen = "/workplaceProjectDetailsScreen";
const String workplaceCostEstimationScreen = "/workplaceCostEstimationScreen";
const String workplaceTaskMilestoneScreen = "/workplaceTaskMilestoneScreen";

const String profileMainScreen = '/profileMainScreen';
const String profilePersonalInfoScreen = '/profilePersonalInfoScreen';
const String profileSupportScreen = '/profileSupportScreen';
const String profileSupportSuccessScreen = '/profileSupportSuccessScreen';

const String mediaSliderScreen = "/mediaSliderScreen";

const String taskMainScreen = "/taskMainScreen";
const String taskMilestoneScreen = "/taskMilestoneScreen";
const String taskDetailsScreen = "/taskDetailsScreen";
const String taskSearchScreen = "/taskSearchScreen";
const String taskMilestoneEditScreen = "/taskMilestoneEditScreen";
const String taskRescheduleScreen = "/taskRescheduleScreen";
const String taskFormScreen = "/taskFormScreen";
const String taskCommentScreen = "/taskCommentScreen";
