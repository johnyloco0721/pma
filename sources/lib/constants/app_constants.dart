const String title_workplace_page = "Posts";
const String title_task_page = "Tasks";
const String title_alert_page = "Alert";
const String title_profile_page = "Profile";

const String text_empty_workplace = "Sorry, there’s no projects yet.";
const String text_empty_search = "Sorry! No result shown\nbased on your search.";
const String text_empty_chat = "It’s empty here,\nbe the first one to post!";
const String text_empty_chat_filter = "No result shown with\nyour filter option.";
const String text_empty_task = "There is no task at the moment";