class AppDataModel {
  static String? updateAppLink;
  static bool? byPassCode;
  static String? byPassParameter;
  static String? byPassHash;

  static setByPassCode(bool? code){
    byPassCode = code;
  }
 
  static getByPassCode(){
    return byPassCode ?? false;
  }

  static setUpdateAppLink(String? data) {
    updateAppLink = data;
  }

  static String getUpdateAppLink() {
    return updateAppLink ?? "invalid link";
  }

  static getByPassParameter(String? pass) {
    byPassParameter = pass;
  }

  static String setByPassParameter() {
    return byPassParameter ?? 'invalid parameter';
  }

  static getByPassHash(String? pass) {
    byPassHash = pass;
  }

  static String setByPassHash() {
    return byPassHash ?? 'invalid hash';
  }
}