import 'package:flutter/material.dart';

const List<BoxShadow> app_box_shadow = [
  BoxShadow(
    color: Color.fromRGBO(75, 54, 0, 0.05),
    offset: Offset(0, 7),
    blurRadius: 27),
  BoxShadow(
    color: Color.fromRGBO(75, 54, 0, 0.0359427),
    offset: Offset(0, 2.92443),
    blurRadius: 11.28),
  BoxShadow(
    color: Color.fromRGBO(75, 54, 0, 0.0298054),
    offset: Offset(0, 1.56354),
    blurRadius: 6.03),
  BoxShadow(
    color: Color.fromRGBO(75, 54, 0, 0.025),
    offset: Offset(0, 0.876509),
    blurRadius: 3.38082),
  BoxShadow(
    color: Color.fromRGBO(75, 54, 0, 0.0201946),
    offset: Offset(0, 0.465507),
    blurRadius: 1.79553),
  BoxShadow(
    color: Color.fromRGBO(75, 54, 0, 0.0140573),
    offset: Offset(0, 0.193708),
    blurRadius: 0.747159),
];

const List<BoxShadow> drag_box_shadow = [
  BoxShadow(
    color: Color.fromRGBO(75, 54, 0, 0.12),
    offset: Offset(0, 17),
    blurRadius: 34),
  BoxShadow(
    color: Color.fromRGBO(75, 54, 0, 0.0827978),
    offset: Offset(0, 6.20528),
    blurRadius: 12.4106),
  BoxShadow(
    color: Color.fromRGBO(75, 54, 0, 0.0667549),
    offset: Offset(0, 3.01255),
    blurRadius: 6.0251),
  BoxShadow(
    color: Color.fromRGBO(75, 54, 0, 0.0532451),
    offset: Offset(0, 1.47681),
    blurRadius: 2.95361),
  BoxShadow(
    color: Color.fromRGBO(75, 54, 0, 0.0372022),
    offset: Offset(0, 0.583932),
    blurRadius: 1.16786),
];

const List<BoxShadow> subtask_box_shadow = [
  BoxShadow(
    color: Color.fromRGBO(75, 54, 0, 0.01),
    offset: Offset(0, 0.19),
    blurRadius: 0.75),
  BoxShadow(
    color: Color.fromRGBO(75, 54, 0, 0.02),
    offset: Offset(0, 0.47),
    blurRadius: 1.8),
  BoxShadow(
    color: Color.fromRGBO(48, 34, 0, 0.03),
    offset: Offset(0, 0.88),
    blurRadius: 3.38),
  BoxShadow(
    color: Color.fromRGBO(75, 54, 0, 0.03),
    offset: Offset(0, 1.56),
    blurRadius: 6.03),
  BoxShadow(
    color: Color.fromRGBO(75, 54, 0, 0.04),
    offset: Offset(0, 2.92),
    blurRadius: 11.28),
  BoxShadow(
    color: Color.fromRGBO(75, 54, 0, 0.05),
    offset: Offset(0, 7),
    blurRadius: 27),
];
