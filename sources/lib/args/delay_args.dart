class LivePreviewArguments {
  final int delay;
  final int delayDateIndex;

  LivePreviewArguments({required this.delay, required this.delayDateIndex});
}