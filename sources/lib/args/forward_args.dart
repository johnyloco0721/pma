class ForwardArguments {
  final String? text;
  final List<String>? filePaths;

  ForwardArguments(this.text, this.filePaths);
}