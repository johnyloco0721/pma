class WorkplaceArguments {
  final int? projectID;
  final int? feedID;
  final String? goToAllTaskMilestone;
  final String? forwardText;
  final List<String>? forwardFilePaths;

  WorkplaceArguments(this.projectID, this.feedID, this.goToAllTaskMilestone, this.forwardText, this.forwardFilePaths);
}