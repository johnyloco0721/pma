class TaskDetailsArguments {
  int taskID;
  String? propertyName;

  TaskDetailsArguments({required this.taskID, this.propertyName});
}