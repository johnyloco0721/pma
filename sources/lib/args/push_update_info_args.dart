import 'package:sources/models/attach_model.dart';
import 'package:sources/models/task_details_model.dart';
import 'package:sources/models/task_requirements.dart';

class PushUpdateInfoArguments {
  final int? taskID;
  final int? projectID;
  final String? action;
  final String? remark;
  final bool? isShownMediasInCA;
  final bool? imageUpdateCA;
  final List<AttachModel>? updatesImages;
  final List<AttachModel>? updatesFiles;
  final String? updateScreen;
  final bool? isFileRequired;
  final String? specialTaskType;
  final String? linkValue;
  final String? taskRelatedDate;
  final ExtraDatesDisplayArr? extraDatesDisplayArr;
  ForClient? forClient;
  ForInternal? forInternal;

  PushUpdateInfoArguments({
    this.taskID,
    this.projectID,
    this.action,
    this.remark,
    this.isShownMediasInCA,
    this.imageUpdateCA,
    this.updatesImages,
    this.updatesFiles,
    this.updateScreen,
    this.isFileRequired,
    this.specialTaskType,
    this.linkValue,
    this.taskRelatedDate,
    this.extraDatesDisplayArr,
    this.forClient,
    this.forInternal,
  });
}
