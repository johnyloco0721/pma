import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:sources/models/task_milestone_model.dart';

class TaskArguments {
  final int? projectID;
  final int? taskID;
  final String? moduleName;
  final PagingController<int, StageDetail>? milestonePagingController;

  TaskArguments({this.projectID, this.taskID, this.moduleName, this.milestonePagingController});
}