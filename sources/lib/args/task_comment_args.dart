class TaskCommentArguments {
  final int projectID;
  final int taskID;

  TaskCommentArguments({required this.projectID,required this.taskID});
}
