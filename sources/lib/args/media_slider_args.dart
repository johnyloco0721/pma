class MediaSliderArguments {
  final List<MediaInfoArguments> mediaInfoList;
  final int initialIndex;

  MediaSliderArguments(this.mediaInfoList, this.initialIndex);
}

class MediaInfoArguments {
  final String mediaPath;
  final String? thumbnailPath;
  final String mediaType;
  final String? mediaDescription;
  final String? datetime;

  MediaInfoArguments({
    required this.mediaPath, 
    this.thumbnailPath, 
    required this.mediaType, 
    this.mediaDescription, 
    this.datetime
  });
}