import 'package:sources/database/app_database.dart';

class MediasListArguments {
  final List<Media> mediaList;
  final List<Feed> mediaFeedList;
  final int initialIndex;

  MediasListArguments(this.mediaList, this.mediaFeedList, this.initialIndex);
}