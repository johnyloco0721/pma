class ChatReplyArguments {
  final int projectID;
  final int parentFeedID;

  ChatReplyArguments(this.projectID, this.parentFeedID);
}