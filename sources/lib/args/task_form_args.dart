import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:sources/models/task_details_model.dart';
import 'package:sources/models/task_milestone_model.dart';
import 'package:sources/models/team_member_model.dart';

class TaskFormArguments {
  List<TeamMemberDetails>? teamMemberList;
  List<TaskMilestoneOption>? milestoneList;
  TaskDetailsModel? taskDetail;
  int? projectID;
  final PagingController<int, StageDetail>? milestonePagingController;

  TaskFormArguments({this.teamMemberList, this.milestoneList, this.taskDetail, required this.projectID, this.milestonePagingController});
}