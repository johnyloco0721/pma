class WorkplaceTaskMilestoneArguments {
  final int? projectID;
  final String? goToAllTaskMilestone;
  final String? projectName;

  WorkplaceTaskMilestoneArguments(this.projectID, this.goToAllTaskMilestone, this.projectName);
}