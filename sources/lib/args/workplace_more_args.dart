class WorkplaceMoreArguments {
  final int? projectID;
  final String? propertyName;
  final String? ownerName;
  final int? completedTasks;
  final int? totalTasks;
  final String? goToAllTaskMilestone;

  WorkplaceMoreArguments(this.projectID, this.propertyName, 
          this.ownerName, this.completedTasks, this.totalTasks, this.goToAllTaskMilestone);
}