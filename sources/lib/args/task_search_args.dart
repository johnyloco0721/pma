class TaskSearchArguments {
  final int? fromIndex;
  final String? filterDatetime;

  TaskSearchArguments(this.fromIndex, this.filterDatetime);
}