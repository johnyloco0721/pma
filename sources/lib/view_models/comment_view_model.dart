import 'package:flutter/material.dart';

import 'package:sources/database/app_database.dart';
import 'package:sources/models/profile_model.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';

import '../models/feed_item_model.dart';

class CommentViewModel extends ChangeNotifier {
  ProfileDetails? currentUser;

  ValueNotifier<bool> isShowScrollBottomButton = ValueNotifier(false);

  // Workplace Chat Form
  Workplace? currentProject;
  ValueNotifier<String> toolboxMode = ValueNotifier('none');
  ValueNotifier<bool> isShowTopicSelector = ValueNotifier(false);
  ValueNotifier<FeedItemModel?> selectedCommentWithDetail = ValueNotifier(null);
  ValueNotifier<double> selectedBubbleOffset = ValueNotifier(0.0);
  ValueNotifier<String?> temporaryTextFieldValue = ValueNotifier(null);

  FocusNode commentDescriptionNode = FocusNode();
  ValueNotifier<bool> isPostFeedLoading = ValueNotifier(false);
  ValueNotifier<bool> isSending = ValueNotifier(false);
  ValueNotifier<List> mediaPdfFiles = ValueNotifier([]);
  ValueNotifier<List> mediaImageFiles = ValueNotifier([]);
  ValueNotifier<List<int>> selectedDeleteMediaIDs = ValueNotifier([]);
  ValueNotifier<List> temporaryCopiesMediaFiles = ValueNotifier([]);

  ValueNotifier<Topic?> selectedTopic = ValueNotifier(null);

  ValueNotifier<bool> isShowNewUnreads = ValueNotifier(false);
  ValueNotifier<int> totalUnreads = ValueNotifier(0);
  ValueNotifier<int> lastUnreadIndex = ValueNotifier(0);

  initialize() {
    this.currentUser = SpManager.getUserdata();
  }

  disposeChat() {
    isPostFeedLoading.value = false;
    temporaryTextFieldValue.value = null;
    mediaImageFiles.value.clear();
    mediaPdfFiles.value.clear();
  }

  clearForm() {
    mediaImageFiles.value.clear();
    mediaPdfFiles.value.clear();
    mediaImageFiles.notifyListeners();
    mediaPdfFiles.notifyListeners();
  }

  removePdfFile(index) {
    String path = mediaPdfFiles.value[index];
    if (path[0] != '/') {
      selectedDeleteMediaIDs.value.addAll(selectedCommentWithDetail.value!.mediaList!.where((e) => e.url == path).map((e) => e.mediaID!));
      selectedCommentWithDetail.value!.mediaList!.removeWhere((e) => e.url == path);
    } 
    mediaPdfFiles.value.removeAt(index);
    mediaPdfFiles.notifyListeners();
  }

  removeImageFile(index) {
    String path = mediaImageFiles.value[index];
    if (path[0] != '/') {
      selectedDeleteMediaIDs.value.addAll(selectedCommentWithDetail.value!.mediaList!.where((e) => e.url == path).map((e) => e.mediaID!));
      selectedCommentWithDetail.value!.mediaList!.removeWhere((e) => e.url == path);
    }
    mediaImageFiles.value.removeAt(index);
    mediaImageFiles.notifyListeners();
  }
}
