import 'package:flutter/material.dart';

import 'package:sources/database/app_database.dart';
import 'package:sources/database/feeds.dart';
import 'package:sources/models/profile_model.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';

class WorkplaceViewModel extends ChangeNotifier {
  ProfileDetails? currentUser;

  // Workplace List
  ValueNotifier<List<String>> workplaceFilterList = ValueNotifier(['attention', 'unread', 'read']); // if change this initial filter, need change in workplace_list_view also
  ValueNotifier<bool> isShowScrollBottomButton = ValueNotifier(false);

  // Workplace Chat Form
  ValueNotifier<List<int>> chatFilterList = ValueNotifier([]);
  Workplace? currentProject;
  ValueNotifier<String> toolboxMode = ValueNotifier('none');
  ValueNotifier<bool> isShowTopicSelector = ValueNotifier(false);
  ValueNotifier<FeedWithDetail?> selectedFeedWithDetail = ValueNotifier(null);
  ValueNotifier<double> selectedBubbleOffset = ValueNotifier(0.0);
  ValueNotifier<String?> temporaryTextFieldValue = ValueNotifier(null);

  FocusNode feedDescriptionNode = FocusNode();
  ValueNotifier<bool> isPostFeedLoading = ValueNotifier(false);
  ValueNotifier<List> mediaPdfFiles = ValueNotifier([]);
  ValueNotifier<List> mediaImageFiles = ValueNotifier([]);
  ValueNotifier<List<int>> selectedDeleteMediaIDs = ValueNotifier([]);
  ValueNotifier<List> temporaryCopiesMediaFiles = ValueNotifier([]);

  ValueNotifier<List<Topic>> topicList = ValueNotifier([]);
  Topic? defaultTopic;
  ValueNotifier<Topic?> selectedTopic = ValueNotifier(null);

  ValueNotifier<bool> isShowNewUnreads = ValueNotifier(false);
  ValueNotifier<int> totalUnreads = ValueNotifier(0);
  ValueNotifier<int> lastUnreadIndex = ValueNotifier(0);

  // Workplace Reply Chat
  FocusNode replyFeedDescriptionNode = FocusNode();
  ValueNotifier<String> replytoolboxMode = ValueNotifier('none');
  ValueNotifier<FeedWithDetail?> selectedReplyFeedWithDetail = ValueNotifier(null);
  ValueNotifier<double> selectedReplyBubbleOffset = ValueNotifier(0.0);
  ValueNotifier<bool> isShowReplyScrollBottomButton = ValueNotifier(false);
  ValueNotifier<bool> isShowReplyNewUnreads = ValueNotifier(false);
  ValueNotifier<int> totalReplyUnreads = ValueNotifier(0);
  ValueNotifier<int> lastReplyUnreadIndex = ValueNotifier(0);

  // Workplace Reply Chat
  FocusNode forwardDescriptionNode = FocusNode();

  // Task List
  ValueNotifier<List<String>> taskFilterList = ValueNotifier([]);

  initialize() {
    this.currentUser = SpManager.getUserdata();
  }

  disposeChat() {
    isPostFeedLoading.value = false;
    selectedTopic.value = defaultTopic;
    temporaryTextFieldValue.value = null;
    mediaImageFiles.value.clear();
    mediaPdfFiles.value.clear();
  }

  clearForm() {
    mediaImageFiles.value.clear();
    mediaPdfFiles.value.clear();
    mediaImageFiles.notifyListeners();
    mediaPdfFiles.notifyListeners();
  }

  removePdfFile(index, isChatView) {
    String path = mediaPdfFiles.value[index];
    if (path[0] != '/' && isChatView) {
      selectedDeleteMediaIDs.value.addAll(selectedFeedWithDetail.value!.mediaList!.where((e) => e.url == path).map((e) => e.mediaID));
      selectedFeedWithDetail.value?.mediaList?.removeWhere((e) => e.url == path);
    } else if (path[0] != '/' && !isChatView) {
      selectedDeleteMediaIDs.value.addAll(selectedReplyFeedWithDetail.value!.mediaList!.where((e) => e.url == path).map((e) => e.mediaID));
      selectedReplyFeedWithDetail.value!.mediaList!.removeWhere((e) => e.url == path);
    }
    mediaPdfFiles.value.removeAt(index);
    mediaPdfFiles.notifyListeners();
  }

  removeImageFile(index, isChatView) {
    String path = mediaImageFiles.value[index];
    if (path[0] != '/' && isChatView) {
      selectedDeleteMediaIDs.value.addAll(selectedFeedWithDetail.value!.mediaList!.where((e) => e.url == path).map((e) => e.mediaID));
      selectedFeedWithDetail.value!.mediaList!.removeWhere((e) => e.url == path);
    } else if (path[0] != '/' && !isChatView) {
      selectedDeleteMediaIDs.value.addAll(selectedReplyFeedWithDetail.value!.mediaList!.where((e) => e.url == path).map((e) => e.mediaID));
      selectedReplyFeedWithDetail.value!.mediaList!.removeWhere((e) => e.url == path);
    }
    mediaImageFiles.value.removeAt(index);
    mediaImageFiles.notifyListeners();
  }
}
