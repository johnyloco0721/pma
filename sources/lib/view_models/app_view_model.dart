// import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mute/flutter_mute.dart';

// import 'package:sources/constants/app_audios.dart';
class AppViewModel {
  GlobalKey<NavigatorState> mainNavigatorKey = GlobalKey<NavigatorState>();
  final tabsNavigatorKey = [
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
  ];
  ValueNotifier<int> bottomBarCurrentIndex = ValueNotifier(0);
  ValueNotifier<bool> isConnectionFailed = ValueNotifier(false);
  ValueNotifier<RingerMode> ringerMode = ValueNotifier(RingerMode.Normal);

  // final AudioCache _soundEffectCache = AudioCache(
  //     prefix: 'assets/audios/',
  //     fixedPlayer: AudioPlayer()..setReleaseMode(ReleaseMode.STOP),
  //   );

  // buttonSoundEffect() async {
  //   ringerMode.value = await FlutterMute.getRingerMode();

  //   print(ringerMode.value);
  //   if (ringerMode.value == RingerMode.Normal) {
  //     _soundEffectCache.play(button_click_soundclip);
  //   }
  // }
}
