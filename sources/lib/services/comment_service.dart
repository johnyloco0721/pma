import 'package:dio/dio.dart';
import 'package:sources/models/task_edit_model.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/service_locator.dart';

import '../constants/app_routes.dart';
import '../database/app_database.dart';
import '../models/comment_details_model.dart';
import '../models/comment_post_model.dart';
import '../models/comment_update_model.dart';
import '../models/task_flagging_model.dart';
import '../models/task_resolve_model.dart';
import '../utils/error_utils.dart';
import '../utils/route_utils.dart';
import '../view_models/app_view_model.dart';
import 'package:sources/constants/app_data_models.dart';

class CommentService {
  final Api api = Api();
  final AppViewModel appViewModel = locator<AppViewModel>();
  final database = locator<AppDatabase>();

  // Fetch comments from API
  Future fetchErrorMessage(int taskID) async {
    if (!appViewModel.isConnectionFailed.value) {
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().getHTTP('/TaskComments?taskID=$taskID&currentPageNo=1&limitPerPage=10&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
          return response.data["message"];
        }else{
          Response response = await Api().getHTTP('/TaskComments?taskID=$taskID&currentPageNo=1&limitPerPage=10');
          return response.data["message"];
        }
      } on DioException catch (e) {
        print("debug TaskComments API error: $taskID , $e");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called TaskComments API Failed!');
    }
  }

  // Fetch comments from API
  Future fetchTaskCommentDetails(int taskID, {int pageKey = 1, CommentDetailsModel? oldReponseModel}) async {
    if (!appViewModel.isConnectionFailed.value) {
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().getHTTP('/TaskComments?taskID=$taskID&currentPageNo=$pageKey&limitPerPage=10&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
          CommentDetailsModel newReponseModel = CommentDetailsModel.fromJson(response.data);

          int totalItems = response.data['totalItems'] ?? 0;
          int limitPerPage = response.data['limitPerPage'] ?? 50;
          int currentPageNo = response.data['currentPageNo'] ?? 1;
          int totalPages = (totalItems/limitPerPage).ceil();

          if (oldReponseModel != null) {
            oldReponseModel.response.commentList?.addAll(newReponseModel.response.commentList ?? []);
            newReponseModel = oldReponseModel;
          }

          // If more than 1 page, then continue calling API
          if (totalItems > 0 && totalPages > (response.data['currentPageNo'] ?? 1)) {
            currentPageNo++;
            return fetchTaskCommentDetails(taskID, pageKey: currentPageNo, oldReponseModel: newReponseModel);
          } else {
            return newReponseModel;
          }
        } else {
          Response response = await Api().getHTTP('/TaskComments?taskID=$taskID&currentPageNo=$pageKey&limitPerPage=10');
          CommentDetailsModel newReponseModel = CommentDetailsModel.fromJson(response.data);

          int totalItems = response.data['totalItems'] ?? 0;
          int limitPerPage = response.data['limitPerPage'] ?? 50;
          int currentPageNo = response.data['currentPageNo'] ?? 1;
          int totalPages = (totalItems/limitPerPage).ceil();

          if (oldReponseModel != null) {
            oldReponseModel.response.commentList?.addAll(newReponseModel.response.commentList ?? []);
            newReponseModel = oldReponseModel;
          }

          // If more than 1 page, then continue calling API
          if (totalItems > 0 && totalPages > (response.data['currentPageNo'] ?? 1)) {
            currentPageNo++;
            return fetchTaskCommentDetails(taskID, pageKey: currentPageNo, oldReponseModel: newReponseModel);
          } else {
            return newReponseModel;
          }
        }
      } on DioException catch (e) {
        print("debug TaskComments API error: $taskID , $e");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called TaskComments API Failed!');
    }
  }

  Future postComment(CommentPostModel data) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().postHTTP('/TaskCommentCreate?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', data.toJson());
        return CommentDetailsResponse.fromJson(response.data['response']);
      }else{
        Response response = await Api().postHTTP('/TaskCommentCreate', data.toJson());
        return CommentDetailsResponse.fromJson(response.data['response']);
      }
    } on DioException catch (e) {
      print("debug TaskCommentCreate API error: ${data.taskID}");
      if (ErrorUtils.handleError(e) == 'Token mismatched.') {
        goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
      } else {
        print('No Internet Connection! Called TaskCommentCreate API Failed!');
      }
    }
  }

  // Update Task
  Future postCommentUpdate(CommentUpdateModel data) async {
    if (!appViewModel.isConnectionFailed.value) {
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().postHTTP('/TaskCommentUpdate?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', data.toJson());
          return response.data;
        }else{
          Response response = await Api().postHTTP('/TaskCommentUpdate', data.toJson());
          return response.data;
        }
      } on DioException catch (e) {
        print("debug TaskCommentUpdate API error: ${data.toJson()}");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
        throw e;
      }
    } else {
      print('No Internet Connection! Called TaskCommentUpdate API Failed!');
    }
  }

  Future deleteComment(int projectID, int taskID, int feedID) async {
    if (!appViewModel.isConnectionFailed.value) {
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().deleteHTTP('/TaskCommentDelete?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', {"projectID": "$projectID", "taskID": "$taskID", "feedID": "$feedID"});
          return response.data;
        }else{
          Response response = await Api().deleteHTTP('/TaskCommentDelete', {"projectID": "$projectID", "taskID": "$taskID", "feedID": "$feedID"});
          return response.data;
        }
      } on DioException catch (e) {
        print("debug TaskCommentDelete API error: $taskID");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
        throw e;
      }
    } else {
      print('No Internet Connection! Called TaskCommentDelete API Failed!');
    }
  }

  Future postMyTaskEdit(MyTaskEditModel data) async {
    if (!appViewModel.isConnectionFailed.value) {
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().postHTTP('/MyTaskEdit?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', data.toJson());
          return response;
        }else{
          Response response = await Api().postHTTP('/MyTaskEdit', data.toJson());
          return response;
        }
      } on DioException catch (e) {
        print("debug MyTaskEdit API error: ${data.taskID}");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called MyTaskEdit API Failed!');
    }
  }

  Future deleteMyTaskFile(dynamic taskImageID) async {
    if (taskImageID >= 0) {
      try {
        if (AppDataModel.getByPassCode()) {
          return await api.deleteHTTP('/MyTaskFileDelete?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', {'taskImageID': taskImageID })
          .then((response) {
            return response;
          });
        }else{
          return await api.deleteHTTP('/MyTaskFileDelete', {'taskImageID': taskImageID })
            .then((response) {
              return response;
            });
        }
      } on DioException catch (e) {
        print("debug MyTaskFileDelete API error: ");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    }
  }

  Future postTaskFlagging(TaskFlaggingModel data) async {
    if (!appViewModel.isConnectionFailed.value) {
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().postHTTP('/TaskFlagging?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', data.toJson());
          return response;
        }else{
          Response response = await Api().postHTTP('/TaskFlagging', data.toJson());
          return response;
        }
      } on DioException catch (e) {
        print("debug TaskFlagging API error: ${data.taskID}");
        print(e);
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called TaskFlagging API Failed!');
    }
  }

  Future postMyTaskResolve(TaskResolveModel data) async {
    if (!appViewModel.isConnectionFailed.value) {
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().postHTTP('/MyTaskResolve?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', data.toJson());
          return response;
        }else{
          Response response = await Api().postHTTP('/MyTaskResolve', data.toJson());
          return response;
        }
      } on DioException catch (e) {
        print("debug MyTaskResolve API error: ${data.taskID}");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called MyTaskResolve API Failed!');
    }
  }

}
