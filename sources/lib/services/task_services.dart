import 'package:dio/dio.dart';
import 'package:sources/constants/app_data_models.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/models/push_update_model.dart';
import 'package:sources/models/raise_invoice_model.dart';
import 'package:sources/models/special_task_model.dart';
import 'package:sources/models/task_details_model.dart';
import 'package:sources/models/task_list_model.dart';
import 'package:sources/models/task_live_preview_post_model.dart';
import 'package:sources/models/task_mark_complete_model.dart';
import 'package:sources/models/task_milestone_model.dart';
import 'package:sources/models/task_reschedule_model.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/error_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/view_models/app_view_model.dart';

import '../models/reddot_list_model.dart';
import '../models/task_delay_model.dart';
import '../models/task_milestone_update_model.dart';
import '../models/task_post_model.dart';
import '../models/team_member_list_model.dart';

class TaskService {
  final Api api = Api();
  final AppViewModel appViewModel = locator<AppViewModel>();

  static getMyTaskProjectList({int pageKey = 1, String filterBy = '', String keyword = '', String date =''}) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('/MyTaskProjectList?currentPageNo=$pageKey&filterBy=$filterBy&searchKey=$keyword&date=$date&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
        return response;
      } else {
        Response response = await Api().getHTTP('/MyTaskProjectList?currentPageNo=$pageKey&filterBy=$filterBy&searchKey=$keyword&date=$date');
        return response;
      }
    }
    on DioException catch (e) {
      print("debug MyTaskProjectList API error: $pageKey $filterBy $keyword");
      throw ErrorUtils.handleError(e);
    }
  }
  
  static getMyTaskList({int pageKey = 1, String filterBy = '', String keyword = '', String date =''}) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('/MyTaskList?currentPageNo=$pageKey&filterBy=$filterBy&searchKey=$keyword&date=$date&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
        return response;
      } else {
        Response response = await Api().getHTTP('/MyTaskList?currentPageNo=$pageKey&filterBy=$filterBy&searchKey=$keyword&date=$date');
        return response;
      }
    }
    on DioException catch (e) {
      print("debug MyTaskList API error: $pageKey $filterBy $keyword");
      throw ErrorUtils.handleError(e);
    }
  }
  
  static getMyTaskListByMilestone(int projectID, int pageKey, String date) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('/MyTaskListByMilestone2?currentPageNo=$pageKey&projectID=$projectID&date=$date&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
        return response;
      } else {
        Response response = await Api().getHTTP('/MyTaskListByMilestone2?currentPageNo=$pageKey&projectID=$projectID&date=$date');
        return response;
      }
    }
    on DioException catch (e) {
      print("debug MyTaskListByMilestone API error: $projectID $pageKey");
      throw ErrorUtils.handleError(e);
    }
  }
  
  static getMyTaskDetails(int taskID) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('/MyTaskListByMilestone?taskID=$taskID&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
        return response;
      } else {
        Response response = await Api().getHTTP('/MyTaskListByMilestone?taskID=$taskID');
        return response;
      }
    }
    on DioException catch (e) {
      print("debug MyTaskListByMilestone API error: $taskID");
      throw ErrorUtils.handleError(e);
    }
  }

  static getAllTaskListByMilestone(int projectID, {int pageKey = 1, String filterBy = ''}) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('/AllTaskListByMilestone2?currentPageNo=$pageKey&projectID=$projectID&filterBy=$filterBy&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
        return response;
      } else {
        Response response = await Api().getHTTP('/AllTaskListByMilestone2?currentPageNo=$pageKey&projectID=$projectID&filterBy=$filterBy');
        return response;
      }
    }
    on DioException catch (e) {
      print("debug AllTaskListByMilestone API error: $projectID $pageKey");
      throw ErrorUtils.handleError(e);
    }
  }

  // Fetch task projects from API
  Future fetchMyTaskProjectList({int pageKey = 1, String filterBy = '', String keyword = '', String date = ''}) async {
    if (!appViewModel.isConnectionFailed.value){
      try{
        Response response = await getMyTaskProjectList(pageKey: pageKey, filterBy: filterBy, keyword: keyword,date :date);
        return TaskListModel.fromJson(response.data);
      } catch (e) {
        if (e == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called MyTaskProjectList API Failed!');
    }
  }

  // Fetch tasks from API
  Future fetchMyTaskList({int pageKey = 1, String filterBy = '', String keyword = '',String date = ''}) async {
    if (!appViewModel.isConnectionFailed.value){
      try{
        Response response = await getMyTaskList(pageKey: pageKey, filterBy: filterBy, keyword: keyword, date: date);
        return TaskListModel.fromJson(response.data);
      } catch (e) {
        if (e == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called MyTaskList API Failed!');
    }
  }

  // Fetch tasks by milestone from API
  Future fetchMyTaskListByMilestone(int projectID, int pageKey, String date) async {
    if (!appViewModel.isConnectionFailed.value){
      try{
        Response response = await getMyTaskListByMilestone(projectID, pageKey, date);
        return TaskMilestoneModel.fromJson(response.data);
      } catch (e) {
        if (e == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called MyTaskListByMilestone API Failed!');
    }
  }

  // Fetch tasks details from API
  Future fetchMyTaskDetails(int taskID) async {
    if (!appViewModel.isConnectionFailed.value){
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().getHTTP('/MyTaskDetails?taskID=$taskID&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
          return TaskDetailsResponseModel.fromJson(response.data['response']);
        } else {
          Response response = await Api().getHTTP('/MyTaskDetails?taskID=$taskID');
          return TaskDetailsResponseModel.fromJson(response.data['response']);
        }
      }
      on DioException catch (e) {
        print("debug MyTaskDetails API error: $taskID");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called MyTaskDetails API Failed!');
    }
  }

  // Upload attachment via API
  Future postMyTaskFileUpload(dynamic formData) async {
    if (!appViewModel.isConnectionFailed.value){
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().postHTTP('/MyTaskFileUpload?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', formData);
          return response.data;
        } else {
          Response response = await Api().postHTTP('/MyTaskFileUpload', formData);
          return response.data;
        }
      }
      on DioException catch (e) {
        print("debug MyTaskFileUpload API error: ");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called MyTaskFileUpload API Failed!');
    }
  }

  // Upload attachment via API
  Future postMyTaskMarkComplete(TaskMarkCompleteModel data) async {
    if (!appViewModel.isConnectionFailed.value){
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().postHTTP('/MyTaskMarkComplete?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', data.toJson());
          return response.data;
        } else {
          Response response = await Api().postHTTP('/MyTaskMarkComplete', data.toJson());
          return response.data;
        }
      }
      on DioException catch (e) {
        print("debug MyTaskMarkComplete API error: ${data.toJson()}");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called MyTaskMarkComplete API Failed!');
    }
  }

  // Create Task
  Future postTaskCreate(TaskPostModel data) async {
    if (!appViewModel.isConnectionFailed.value){
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().postHTTP('/TaskCreate?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', data.toJson());
          return response.data;
        } else {
          Response response = await Api().postHTTP('/TaskCreate', data.toJson());
          return response.data;
        }
      }
      on DioException catch (e) {
        print("debug TaskCreate API error: ${data.toJson()}");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
        throw e;
      }
    } else {
      print('No Internet Connection! Called TaskCreate API Failed!');
    }
  }

  // Update Task
  Future postTaskUpdate(TaskPostModel data) async {
    if (!appViewModel.isConnectionFailed.value){
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().postHTTP('/TaskUpdate2?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', data.toJson());
          return response.data;
        } else {
          Response response = await Api().postHTTP('/TaskUpdate2', data.toJson());
          return response.data;
        }
      }
      on DioException catch (e) {
        print("debug TaskUpdate API error: ${data.toJson()}");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
        throw e;
      }
    } else {
      print('No Internet Connection! Called TaskUpdate API Failed!');
    }
  }

  // Delete Task
  Future taskDelete(int taskID) async {
    if (!appViewModel.isConnectionFailed.value){
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().deleteHTTP('/TaskDelete?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', {"taskID": "$taskID"});
          return response.data;
        } else {
          Response response = await Api().deleteHTTP('/TaskDelete', {"taskID": "$taskID"});
          return response.data;
        }
      }
      on DioException catch (e) {
        print("debug TaskDelete API error: $taskID");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
        throw e;
      }
    } else {
      print('No Internet Connection! Called TaskDelete API Failed!');
    }
  }

  // Fetch all tasks by milestone from API
  Future fetchAllTaskListByMilestone(int projectID, {int pageKey = 1, String filterBy = ''}) async {
    if (!appViewModel.isConnectionFailed.value){
      try{
        Response response = await getAllTaskListByMilestone(projectID, pageKey: pageKey, filterBy: filterBy);
        return TaskMilestoneModel.fromJson(response.data);
      } catch (e) {
        if (e == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
        print(e);
      }
    } else {
      print('No Internet Connection! Called AllTaskListByMilestone API Failed!');
    }
  }

  // Update Milestone Edit View List
  Future postMilestoneListUpdate(MilestoneListUpdateModel data) async {
    if (!appViewModel.isConnectionFailed.value) {
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().postHTTP('/TaskListByMilestoneUpdate?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', data.toJson());
          return response.data;
        } else {
          Response response = await Api().postHTTP('/TaskListByMilestoneUpdate', data.toJson());
          return response.data;
        }
        
      } on DioException catch (e) {
        print("debug TaskListByMilestoneUpdate API error: ${data.toJson()}");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(
              appViewModel.mainNavigatorKey, errorLoginScreen,
              args: e);
        }
      }
    } else {
      print('No Internet Connection! Called TaskListByMilestoneUpdate API Failed!');
    }
  }

// Update Milestone Edit View List
  Future postTaskDelay(TaskDelayModel data) async {
    if (!appViewModel.isConnectionFailed.value) {
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().postHTTP('/TaskReschedule?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', data.toJson());
          return response.data;
        } else {
          Response response = await Api().postHTTP('/TaskReschedule', data.toJson());
          return response.data;
        }
      } on DioException catch (e) {
        print("debug TaskDelay API error: ${data.toJson()}");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(
              appViewModel.mainNavigatorKey, errorLoginScreen,
              args: e);
        }
      }
    } else {
      print('No Internet Connection! Called TaskDelay API Failed!');
    }
  }

  //fetch Live Preview List
  Future postLivePreviewReschedule(LivePreviewPostModel data) async {
    if (!appViewModel.isConnectionFailed.value){
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().postHTTP('/LivePreviewReschedule?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', data.toJson());
          return TaskRescheduleModel.fromJson(response.data);
        } else {
          Response response = await Api().postHTTP('/LivePreviewReschedule', data.toJson());
          return TaskRescheduleModel.fromJson(response.data);
        }
      }
      on DioException catch (e) {
        print("debug TaskPushClientApp API error: ${data.toJson()}");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
        throw e;
      }
    } else {
      print('No Internet Connection! Called TaskPushClientApp API Failed!');
    }
  }
  // Push Task To Client App
  Future postTaskPushClientApp(PushUpdateModel data) async {
    if (!appViewModel.isConnectionFailed.value){
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().postHTTP('/TaskPushClientApp?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', data.toJson());
          return response.data;
        } else {
          Response response = await Api().postHTTP('/TaskPushClientApp', data.toJson());
          return response.data;
        }
      }
      on DioException catch (e) {
        print("debug TaskPushClientApp API error: ${data.toJson()}");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
        throw ErrorUtils.handleError(e);
      }
    } else {
      print('No Internet Connection! Called TaskPushClientApp API Failed!');
    }
  }

  // unPusheded Task To Client App
  Future postUnpushedTaskPushClientApp(taskID) async {
    if (!appViewModel.isConnectionFailed.value){
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().postHTTP('/TaskUnpublishClientApp?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', {'taskID': taskID});
          return response.data;
        } else {
          Response response = await Api().postHTTP('/TaskUnpublishClientApp', {'taskID': taskID});
          return response.data;
        }
      }
      on DioException catch (e) {
        print("debug TaskUnpublishClientApp API error: {'taskID': $taskID}");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
        throw ErrorUtils.handleError(e);
      }
    } else {
      print('No Internet Connection! Called TaskUnpublishClientApp API Failed!');
    }
  }

  // Fetch manual generate task
  Future manualGenerateTask(projectID) async {
    if (!appViewModel.isConnectionFailed.value){
      try{
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().postHTTP('/ManualGenerateTask?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}',  {'projectID': projectID});
          return response.data;
        } else {
          Response response = await Api().postHTTP('/ManualGenerateTask', {'projectID': projectID});
          return response.data;
        }
      } catch (e) {
        if (e == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
        print(e);
      }
    } else {
      print('No Internet Connection! Called ManualGenerateTask API Failed!');
    }
  }

  // Post Raise Invoice
  Future postRaiseInvoice(RaiseInvoiceModel data) async {
    if (!appViewModel.isConnectionFailed.value){
      try{
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().postHTTP('/RaiseInvoice?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', data.toJson());
          return response.data;
        } else {
          Response response = await Api().postHTTP('/RaiseInvoice', data.toJson());
          return response.data;
        }
      } catch (e) {
        if (e == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
        print(e);
      }
    } else {
      print('No Internet Connection! Called AllTaskListByMilestone API Failed!');
    }
  }

  Future fetchSpecialTaskList(int projectID) async {
    if (!appViewModel.isConnectionFailed.value){
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().getHTTP('/GetSpecialTaskList?projectID=$projectID&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
          return SpecialTaskModel.fromJson(response.data);
        } else {
          Response response = await Api().getHTTP('/GetSpecialTaskList?projectID=$projectID');
          return SpecialTaskModel.fromJson(response.data);
        }
      }
      on DioException catch (e) {
        print("debug GetSpecialTaskList API error");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
        throw ErrorUtils.handleError(e);
      }
    } else {
      print('No Internet Connection! Called GetSpecialTaskList API Failed!');
    }
  }

  Future fetchTeamMemberList() async {
    if (!appViewModel.isConnectionFailed.value){
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().getHTTP('/GetTeamMemberList?currentPageNo=1&itemPerpage=10000&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
          return TeamMemberListModel.fromJson(response.data);
        } else {
          Response response = await Api().getHTTP('/GetTeamMemberList?currentPageNo=1&itemPerpage=10000');
          return TeamMemberListModel.fromJson(response.data);
        }
      }
      on DioException catch (e) {
        print("debug GetTeamMemberList API error");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
        throw ErrorUtils.handleError(e);
      }
    } else {
      print('No Internet Connection! Called GetTeamMemberList API Failed!');
    }
  }

  Future fetchRedDotList() async {
    if (!appViewModel.isConnectionFailed.value){
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await Api().getHTTP('/redDotList?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
          return RedDotListModel.fromJson(response.data);
        } else {
          Response response = await Api().getHTTP('/redDotList');
          return RedDotListModel.fromJson(response.data);
        }
      }
      on DioException catch (e) {
        print("debug redDotList API error");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
        throw ErrorUtils.handleError(e);
      }
    } else {
      print('No Internet Connection! Called redDotList API Failed!');
    }
  }

}

