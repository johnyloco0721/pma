import 'package:dio/dio.dart';
import 'package:drift/drift.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/constants/app_data_models.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/models/cost_estimation_model.dart';
import 'package:sources/models/general_response_model.dart';
import 'package:sources/models/project_costs_model.dart';
import 'package:sources/models/workplace_details_model.dart';
import 'package:sources/models/workplace_list_model.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/error_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/view_models/app_view_model.dart';

// Workplace service for API calling
class WorkplaceService {
  final Api api = Api();
  final AppViewModel appViewModel = locator<AppViewModel>();
  final database = locator<AppDatabase>();

  static getWorkplaceList({String keyword = '', int pageKey = 1, String lastUpdateDateTime = ''}) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('WorkplaceList?currentPageNo=$pageKey&searchKey=$keyword&updateDateTime=$lastUpdateDateTime&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
        return response;
      }else{
        Response response = await Api().getHTTP('WorkplaceList?currentPageNo=$pageKey&searchKey=$keyword&updateDateTime=$lastUpdateDateTime');
        return response;
      }
    }
    on DioException catch (e) {
      print("debug WorkplaceList API error: $keyword $pageKey $lastUpdateDateTime");
      throw ErrorUtils.handleError(e);
    }
  }

  // Search workplace from API
  Future<WorkplaceListModel> searchWorkplace({String keyword = '', int pageKey = 1}) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('WorkplaceList?currentPageNo=$pageKey&searchKey=$keyword&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
        WorkplaceListModel result = WorkplaceListModel.fromJson(response.data);
        return result;
      }else{
        Response response = await Api().getHTTP('WorkplaceList?currentPageNo=$pageKey&searchKey=$keyword');
        WorkplaceListModel result = WorkplaceListModel.fromJson(response.data);
        
        return result;
      }
    }
    on DioException catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  // Fetch workplace list from API and store into local DB
  Future fetchWorkplaceListAndSaveToDB({int pageKey = 1, String lastUpdateDateTime = ''}) async {
    if (!appViewModel.isConnectionFailed.value) {
      try {
        await getWorkplaceList(pageKey: pageKey, lastUpdateDateTime: lastUpdateDateTime).then((response) {
          WorkplaceListModel workplaceResponse = WorkplaceListModel.fromJson(response.data);
          List<WorkplaceListDetails> workplaceList = workplaceResponse.response!.length == 0 ? [] : workplaceResponse.response!;

          workplaceList.forEach((WorkplaceListDetails item) {
            database.workplaceDao.getSingleWorkplace(item.projectID!).then((matchedWorkplace) {
              bool updated = false;

              if (matchedWorkplace != null && matchedWorkplace.updatedDateTime != null && item.updatedDateTime != null) {
                DateTime serverUpdateDateTime = DateTime.parse(item.updatedDateTime!);
                DateTime localUpdateDateTime = DateTime.parse(matchedWorkplace.updatedDateTime!);

                Duration difference = serverUpdateDateTime.difference(localUpdateDateTime);

                if (difference.inSeconds <= 0) {
                  updated = true;
                }
              }

              if (!updated) {
                // Insert or update workplace list
                Workplace workplace = Workplace.fromJson(item.toJson()).copyWith(
                  isRead: Value(item.isArchived ?? false ? 'archive' : item.isRead)
                );
                database.workplaceDao.insertOrUpdateWorkplace(workplace);
                // Insert or update team member
                item.includedMemberArr?.forEach((member) {
                  var teamMember = TeamMembersCompanion(
                    projectID: Value(item.projectID!), 
                    teamMemberID: Value(member.teamMemberID!),
                    teamMemberName: Value(member.teamMemberName),
                    teamMemberAvatar: Value(member.teamMemberAvatar),
                    designationName: Value(member.designationName),
                    show: Value(true),
                  );
                  database.teamMemberDao.insertOrUpdateTeamMember(teamMember);
                });
                List<int> teamMemberIDs = item.includedMemberArr?.map((e) => e.teamMemberID ?? 0).toList() ?? [];
                database.teamMemberDao.deleteTeamMemberByExcludedIds(item.projectID!, teamMemberIDs);
              }
            });
          });

          int totalItems = workplaceResponse.totalItems ?? 0;
          int limitPerPage = workplaceResponse.limitPerPage ?? 50;
          int currentPageNo = workplaceResponse.currentPageNo ?? 1;
          int totalPages = (totalItems/limitPerPage).ceil();

          // If more than 1 page, then continue calling API
          if (totalItems > 0 && totalPages > (workplaceResponse.currentPageNo ?? 1)) {
            currentPageNo++;
            fetchWorkplaceListAndSaveToDB(pageKey: currentPageNo, lastUpdateDateTime: lastUpdateDateTime);
          }
        });
      } catch (e) {
        if (e == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called Workplace List API Failed!');
    }
  }

  Future getWorkplaceDetail(int projectID) async {
    if (!appViewModel.isConnectionFailed.value) {
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await api.getHTTP('WorkplaceDetail?projectID=$projectID&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
          WorkplaceDetailsModel workplaceResponse = WorkplaceDetailsModel.fromJson(response.data);
          Workplace workplace = Workplace.fromJson(workplaceResponse.response!.projectArr!.toJson());
          database.workplaceDao.insertOrUpdateWorkplace(workplace);
          workplaceResponse.response?.includedMemberArr?.forEach((item) {
            var teamMember = TeamMembersCompanion(
              projectID: Value(workplace.projectID), 
              teamMemberID: Value(item.teamMemberID!),
              teamMemberName: Value(item.teamMemberName),
              teamMemberAvatar: Value(item.teamMemberAvatar),
              designationName: Value(item.designationName),
            );
            database.teamMemberDao.insertOrUpdateTeamMember(teamMember);
          });
          List<int> teamMemberIDs = workplaceResponse.response?.includedMemberArr?.map((e) => e.teamMemberID ?? 0).toList() ?? [];
          database.teamMemberDao.deleteTeamMemberByExcludedIds(projectID, teamMemberIDs);
          workplaceResponse.response?.projectArr?.infoList?.asMap().forEach((index, item) {
            var workplaceInfo = WorkplaceInfosCompanion(
              projectID: Value(workplace.projectID), 
              infoLabel: Value(item.infoLabel),
              infoDisplayValue: Value(item.infoDisplayValue),
              sequance: Value(index),
            );
            database.workplaceInfoDao.insertOrUpdateWorkplaceInfo(workplaceInfo);
          });
          return workplaceResponse;
        }else{
          Response response = await api.getHTTP('WorkplaceDetail?projectID=$projectID');
          WorkplaceDetailsModel workplaceResponse = WorkplaceDetailsModel.fromJson(response.data);
          Workplace workplace = Workplace.fromJson(workplaceResponse.response!.projectArr!.toJson());
          database.workplaceDao.insertOrUpdateWorkplace(workplace);
          workplaceResponse.response?.includedMemberArr?.forEach((item) {
            var teamMember = TeamMembersCompanion(
              projectID: Value(workplace.projectID), 
              teamMemberID: Value(item.teamMemberID!),
              teamMemberName: Value(item.teamMemberName),
              teamMemberAvatar: Value(item.teamMemberAvatar),
              designationName: Value(item.designationName),
            );
            database.teamMemberDao.insertOrUpdateTeamMember(teamMember);
          });
          List<int> teamMemberIDs = workplaceResponse.response?.includedMemberArr?.map((e) => e.teamMemberID ?? 0).toList() ?? [];
          database.teamMemberDao.deleteTeamMemberByExcludedIds(projectID, teamMemberIDs);
          workplaceResponse.response?.projectArr?.infoList?.asMap().forEach((index, item) {
            var workplaceInfo = WorkplaceInfosCompanion(
              projectID: Value(workplace.projectID), 
              infoLabel: Value(item.infoLabel),
              infoDisplayValue: Value(item.infoDisplayValue),
              sequance: Value(index),
            );
            database.workplaceInfoDao.insertOrUpdateWorkplaceInfo(workplaceInfo);
          });
          return workplaceResponse;
        }
      } catch (e) {
        if (e == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called Workplace Details API Failed!');
    }
  }

  Future getTags(int projectID) async {
    if (!appViewModel.isConnectionFailed.value) {
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await api.getHTTP('WorkplaceDetail?projectID=$projectID&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
          WorkplaceDetailsModel workplaceResponse = WorkplaceDetailsModel.fromJson(response.data);
          List<TagsModel>? tags = workplaceResponse.response?.projectArr?.tags;
          return tags;
        }else{
          Response response = await api.getHTTP('WorkplaceDetail?projectID=$projectID');
          WorkplaceDetailsModel workplaceResponse = WorkplaceDetailsModel.fromJson(response.data);
          List<TagsModel>? tags = workplaceResponse.response?.projectArr?.tags;
          return tags;
        }
      } catch (e) {
      if (e == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
      } else {
        print('No Internet Connection! Called Workplace Details API Failed!');
      }
  }

  // Call API to get all cost estimation and store into local db
  Future getCostEstimation({int pageKey = 1, required int projectID}) async {
    if (!appViewModel.isConnectionFailed.value) {
      try {
        if (AppDataModel.getByPassCode()) {
          await api.getHTTP('WorkplaceCostEstimation?projectID=$projectID&currentPageNo=$pageKey&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}')
          .then((response) {
            CostEstimationModel costEstimationReponse = CostEstimationModel.fromJson(response.data);
            List<CostEstimationDetails> costEstimationList = costEstimationReponse.response!.length == 0 ? [] : costEstimationReponse.response!;
            costEstimationList.forEach((CostEstimationDetails item) {
              CostEstimation costEstimation = CostEstimation.fromJson(item.toJson());
              database.costEstimationDao.insertOrUpdateCostEstimation(costEstimation);
            });

            int totalItems = costEstimationReponse.totalItems ?? 0;
            int limitPerPage = costEstimationReponse.limitPerPage ?? 50;
            int currentPageNo = costEstimationReponse.currentPageNo ?? 1;
            int totalPages = (totalItems/limitPerPage).ceil();
            
            // If more than 1 page, then continue calling API
            if (totalItems > 0 && totalPages > (costEstimationReponse.currentPageNo ?? 1)) {
              currentPageNo++;
              getCostEstimation(pageKey: currentPageNo, projectID: projectID);
            }
          });
        }else{
          await api.getHTTP('WorkplaceCostEstimation?projectID=$projectID&currentPageNo=$pageKey')
          .then((response) {
            CostEstimationModel costEstimationReponse = CostEstimationModel.fromJson(response.data);
            List<CostEstimationDetails> costEstimationList = costEstimationReponse.response!.length == 0 ? [] : costEstimationReponse.response!;
            costEstimationList.forEach((CostEstimationDetails item) {
              CostEstimation costEstimation = CostEstimation.fromJson(item.toJson());
              database.costEstimationDao.insertOrUpdateCostEstimation(costEstimation);
            });

            int totalItems = costEstimationReponse.totalItems ?? 0;
            int limitPerPage = costEstimationReponse.limitPerPage ?? 50;
            int currentPageNo = costEstimationReponse.currentPageNo ?? 1;
            int totalPages = (totalItems/limitPerPage).ceil();
            
            // If more than 1 page, then continue calling API
            if (totalItems > 0 && totalPages > (costEstimationReponse.currentPageNo ?? 1)) {
              currentPageNo++;
              getCostEstimation(pageKey: currentPageNo, projectID: projectID);
            }
          });
        }
      } catch (e) {
        if (e == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called Cost Estimation API Failed!');
    }
  }

  Future getMyProjectCost({required int projectID}) async {
    if (!appViewModel.isConnectionFailed.value) {
      try {
        if (AppDataModel.getByPassCode()) {
          Response response = await api.getHTTP('MyProjectCost?projectID=$projectID&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
          var projectCostsModel = ProjectCostsModel.fromJson(response.data);

          return projectCostsModel;
        }else{
          Response response = await api.getHTTP('MyProjectCost?projectID=$projectID');
          var projectCostsModel = ProjectCostsModel.fromJson(response.data);

          return projectCostsModel;
        }
      } catch (e) {
        if (e == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called Cost Estimation API Failed!');
    }
  }

  // Synchronize workplace list, delete workplace if workplaceID is not exists
  Future syncDeletedWorkplaces() async {
    if (!appViewModel.isConnectionFailed.value){
      try {
        if (AppDataModel.getByPassCode()) {
          return await api.getHTTP('WorkplaceIDList?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}')
          .then((response) {
            GeneralResponseModel responseModel = GeneralResponseModel.fromJson(response.data);
            
            List<int> activeWorkplaceIds = [];
            activeWorkplaceIds = List<int>.from(responseModel.response);
            // activeWorkplaceIds.removeWhere((id) => id == 1504); // used for do deleted project testing
            // Remove workplaces's alerts data if projectID is not in the array
            database.workplaceDao.getWorkplacesByExcludedIds(activeWorkplaceIds).then((workplaces) => {
              workplaces.forEach((workplace) {
                database.alertDao.deleteAlertsByProjectID(workplace.projectID);
              })
            });
            // Remove workplaces if projectID is not in the array
            database.workplaceDao.deleteWorkplacesByExcludedIds(activeWorkplaceIds);
          });
        }else{
          return await api.getHTTP('WorkplaceIDList')
          .then((response) {
            GeneralResponseModel responseModel = GeneralResponseModel.fromJson(response.data);
            
            List<int> activeWorkplaceIds = [];
            activeWorkplaceIds = List<int>.from(responseModel.response);
            // activeWorkplaceIds.removeWhere((id) => id == 1504); // used for do deleted project testing
            // Remove workplaces's alerts data if projectID is not in the array
            database.workplaceDao.getWorkplacesByExcludedIds(activeWorkplaceIds).then((workplaces) => {
              workplaces.forEach((workplace) {
                database.alertDao.deleteAlertsByProjectID(workplace.projectID);
              })
            });
            // Remove workplaces if projectID is not in the array
            database.workplaceDao.deleteWorkplacesByExcludedIds(activeWorkplaceIds);
          });
        }
      } on DioError catch (e) {
        print("debug WorkplaceIDList API error: ");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called Workplace ID List API Failed!');
    }
  }
}
