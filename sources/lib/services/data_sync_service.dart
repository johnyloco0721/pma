import 'package:dio/dio.dart';
import 'package:drift/drift.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/models/alert_model.dart';
import 'package:sources/models/attach_model.dart';
import 'package:sources/models/feed_item_model.dart';
import 'package:sources/models/feed_model.dart';
import 'package:sources/models/feed_topic_model.dart';
import 'package:sources/models/media_item_model.dart';
import 'package:sources/models/reply_feed_model.dart';
import 'package:sources/models/team_member_model.dart';
import 'package:sources/models/workplace_list_model.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/error_utils.dart';
import 'package:sources/view_models/app_view_model.dart';
import 'package:sources/constants/app_data_models.dart';
import 'package:uuid/uuid.dart';

class DataSyncService {
  final Api api = Api();
  final AppViewModel appViewModel = locator<AppViewModel>();
  final database = locator<AppDatabase>();

  // Step 1: Get all topics
  static getTopicList() async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('/FeedTopic?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
        return response;
      }else{
        Response response = await Api().getHTTP('/FeedTopic');
        return response;
      }
    }
    on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  Future fetchTopicListAndSaveToDB() async {
    if (!appViewModel.isConnectionFailed.value) {
      return await getTopicList().then((response) async {
        FeedTopicModel topicResponse = FeedTopicModel.fromJson(response.data);
        if (topicResponse.response?.topicList != null) {
          int index = 0;
          List<FeedTopic> topicList = topicResponse.response?.topicList ?? [];
          await Future.forEach(topicList, (FeedTopic item) async {
            var json = item.toJson();
            json['weight'] = index;
            index++;
            Topic topic = Topic.fromJson(json);
            await database.topicDao.insertOrUpdateTopic(topic);
          });
        }
      });
    } else {
      print('No Internet Connection! Called Topic List API Failed!');
    }
    return null;
  }

  // Step 2: Get profile list
  Future getProfileList() async {
    try {
      if (AppDataModel.getByPassCode()) {
        return await api.getHTTP('Profile?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}').then((response) async {
            Profile profile = Profile.fromJson(response.data['response']['profileDetails']);
            await database.profileDao.insertOrUpdateProfile(profile);
        }
        )
        .onError((error, stackTrace) {
          print('Called Profile List API Failed!');
          // print(error);
        });
      }else{
        return await api.getHTTP('Profile').then((response) async {
            Profile profile = Profile.fromJson(response.data['response']['profileDetails']);
            await database.profileDao.insertOrUpdateProfile(profile);
        }
        )
        .onError((error, stackTrace) {
          print('Called Profile List API Failed!');
          // print(error);
        });
      }
    } 
    on Exception catch (_) {
      // print('calling profile API');
      // print(_);
      // throw FetchDataException;
    }
    return null;
  }

  // Step 3: Get all workplaces
  static getWorkplaceList({String keyword = '', int pageKey = 1}) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('WorkplaceList?currentPageNo=$pageKey&searchKey=$keyword&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
        return response;
      }else{
        Response response = await Api().getHTTP('WorkplaceList?currentPageNo=$pageKey&searchKey=$keyword');
        return response;
      }
    }
    on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  Future fetchWorkplaceListAndSaveToDB({int pageKey = 1, Function? callback}) async {
    if (!appViewModel.isConnectionFailed.value) {
      return await getWorkplaceList(pageKey: pageKey).then((response) async {
        WorkplaceListModel workplaceResponse = WorkplaceListModel.fromJson(response.data);
        List<WorkplaceListDetails> workplaceList = workplaceResponse.response!.length == 0 ? [] : workplaceResponse.response!;

        await Future.forEach(workplaceList, (WorkplaceListDetails item) async {
          await database.workplaceDao.getSingleWorkplace(item.projectID!).then((matchedWorkplace) async {
            bool updated = false;

            if (matchedWorkplace != null && matchedWorkplace.updatedDateTime != null) {
              DateTime serverUpdateDateTime = DateTime.parse(item.updatedDateTime!);
              DateTime localUpdateDateTime = DateTime.parse(matchedWorkplace.updatedDateTime!);

              Duration difference = serverUpdateDateTime.difference(localUpdateDateTime);

              if (difference.inSeconds <= 0) {
                updated = true;
              }
            }

            if (!updated) {
              // Insert or update workplace list
              Workplace workplace = Workplace.fromJson(item.toJson()).copyWith(
                isRead: Value(item.isArchived ?? false ? 'archive' : item.isRead)
              );
              await database.workplaceDao.insertOrUpdateWorkplace(workplace);

              // Insert or update team member
              List<TeamMemberDetails> members = item.includedMemberArr ?? [];
              await Future.forEach(members, (TeamMemberDetails member) async {
                var teamMember = TeamMembersCompanion(
                  projectID: Value(item.projectID!), 
                  teamMemberID: Value(member.teamMemberID!),
                  teamMemberName: Value(member.teamMemberName),
                  teamMemberAvatar: Value(member.teamMemberAvatar),
                  designationName: Value(member.designationName),
                  show: Value(true),
                );
                await database.teamMemberDao.insertOrUpdateTeamMember(teamMember);
              });
            }
          });
        });

        int totalItems = workplaceResponse.totalItems ?? 0;
        int limitPerPage = workplaceResponse.limitPerPage ?? 50;
        int currentPageNo = workplaceResponse.currentPageNo ?? 1;
        int totalPages = (totalItems/limitPerPage).ceil();

        double progress = (1 / totalPages) * 30;
        if (callback != null) callback(progress);

        // If more than 1 page, then continue calling API
        if (totalItems > 0 && totalPages > (workplaceResponse.currentPageNo ?? 1)) {
          currentPageNo++;
          await fetchWorkplaceListAndSaveToDB(pageKey: currentPageNo, callback: callback);
        }
      });
    } else {
      print('No Internet Connection! Called Workplace List API Failed!');
    }
    return null;
  }

  // Step 4: Get all feeds
  static getFeedList({required int projectID, int pageKey = 1}) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('/FeedList?projectID=$projectID&currentPageNo=$pageKey&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
        return response;
      }else{
        Response response = await Api().getHTTP('/FeedList?projectID=$projectID&currentPageNo=$pageKey');
        return response;
      }
    }
    on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static getReplyFeedList({required int projectID, required int feedID, int pageKey = 1}) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('/ReplyFeedList?projectID=$projectID&feedID=$feedID&currentPageNo=$pageKey&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
        return response;
      }else{
        Response response = await Api().getHTTP('/ReplyFeedList?projectID=$projectID&feedID=$feedID&currentPageNo=$pageKey');
        return response;
      }
    }
    on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }
  
  Future fetchAndSaveFeedsToDB({required int projectID, int pageKey = 1, bool fetchAll = true, Function? callback}) async {
    if (!appViewModel.isConnectionFailed.value){
      return await getFeedList(projectID: projectID, pageKey: pageKey).then((response) async {
        FeedModel feedResponse = FeedModel.fromJson(response.data);
        
        int? lastReadFeedID;
        if (pageKey == 1) {
          lastReadFeedID = feedResponse.response?.lastReadFeedID ?? null;
          // Update workplace last read feed ID
          await database.workplaceDao.getSingleWorkplace(projectID).then((Workplace? workplace) async {
            if (workplace != null && workplace.lastReadFeedID != lastReadFeedID) {
              workplace = workplace.copyWith(
                lastReadFeedID: Value(lastReadFeedID),
                completedTasks: Value(feedResponse.response?.projectDetail?[0].completedTasks),
                totalTasks: Value(feedResponse.response?.projectDetail?[0].totalTasks),
              );
              await database.workplaceDao.updateWorkplace(workplace);
            }
          });
        }
        
        List<FeedItemModel> feedList = feedResponse.response?.feedList != null ? feedResponse.response!.feedList! : [];

        await Future.forEach(feedList, (FeedItemModel feed) async {
          var newUUID = Uuid().v4();
          // Insert/update feeds into database
          await database.feedDao.getFeedbyUuid(feed.uuid!).then((matchedFeed) async {
            bool updated = false;

            if (matchedFeed != null) {
              if (matchedFeed.feedID != feed.feedID) {
                await database.feedDao.updateFeedStatus(feed.uuid!, feed.feedID!, 1);
                await database.mediaDao.updateMediaFeedID(matchedFeed.feedID, feed.feedID!);
              }
              if (matchedFeed.updatedDateTime != null) {
                DateTime serverUpdateDateTime = DateTime.parse(feed.updatedDateTime!);
                DateTime localUpdateDateTime = DateTime.parse(matchedFeed.updatedDateTime!);

                Duration difference = serverUpdateDateTime.difference(localUpdateDateTime);

                if (difference.inSeconds <= 0) {
                  updated = true;
                }
              }
            }

            if (!updated || matchedFeed?.replyCount != feed.replyCount) {
              var isRead = 'read';
              if (lastReadFeedID != null) {
                if (feed.feedID! > lastReadFeedID) {
                  isRead = 'unread';
                }
              }
              await database.feedDao.insertOrUpdateFeeds(FeedsCompanion(
                uuid: Value(feed.uuid != '' ? feed.uuid : newUUID),
                feedID: Value(feed.feedID!),
                parentID: Value(null),
                projectID: Value(feed.projectID!),
                description: Value(feed.description ?? null),
                feedType: Value(feed.feedType ?? null),
                postDateTime: Value(feed.postDateTime ?? null),
                updatedDateTime: Value(feed.updatedDateTime ?? null),
                topicID: Value(feed.topicID ?? null),
                teamMemberID: Value(feed.teamMemberID ?? null),
                moduleID: Value(feed.moduleID ?? null),
                moduleName: Value(feed.moduleName ?? null),
                updatesDescription: Value(feed.updatesInfo?.updatesDescription ?? null),
                replyTeamMemberAvatar: Value(feed.replyTeamMemberAvatar!.join('|')),
                replyCount: Value(feed.replyCount ?? 0),
                replyReadStatus: Value(feed.replyReadStatus ?? null),
                backgroundColor: Value(feed.backgroundColor ?? null),
                outlineColor: Value(feed.outlineColor ?? null),
                fontNameColor: Value(feed.fontNameColor ?? null),
                fontPositionColor: Value(feed.fontPositionColor ?? null),
                fontDescriptionColor: Value(feed.fontDescriptionColor ?? null),
                fontDateColor: Value(feed.fontDateColor!),
                sendStatus: Value(1),
                isRead: Value(isRead),
              ));

              List<AttachModel> attachmentList = feed.updatesInfo?.updatesImages ?? [];
              attachmentList.addAll(feed.updatesInfo?.updatesFiles ?? []);
              await Future.forEach(attachmentList, (AttachModel value) async {
                var taskMedia = TaskMediasCompanion(
                  taskMediaID: Value(value.taskImageID!),
                  mediaName: Value(value.imageName),
                  mediaType: Value(value.imageType),
                  taskID: Value(feed.updatesInfo!.taskID!),
                  feedID: Value(feed.feedID!),
                  projectID: Value(feed.projectID!),
                  teamMemberID: Value(feed.teamMemberID!),
                  mediaPath: Value(value.imagePath),
                );
                var associateMedia = AssociateMediasCompanion(
                  mediaID: Value(value.taskImageID!),
                  projectID: Value(feed.projectID!),
                  projectFeedsMedia: Value(value.imagePath),
                  projectFeedsMediaType: Value(value.imageType),
                  createdDateTime: Value(value.createdDateTime!),
                  caption: Value(feed.updatesInfo!.updatesDescription!),
                  module: Value("task"),
                );
                database.taskMediaDao.insertOrUpdateTaskMedia(taskMedia);
                database.associateMediaDao.insertOrUpdateAssociateMedia(associateMedia);
              });
            }
          });

          // Insert/update team member into database
          await database.teamMemberDao.getTeamMembersByProject(feed.projectID!).then((value) async {
            var includedTeamMemberIds = value.map((e) => e.show ? e.teamMemberID : null);
            var teamMember = TeamMembersCompanion(
              projectID: Value(feed.projectID!), 
              teamMemberID: Value(feed.teamMemberID!),
              teamMemberName: Value(feed.teamMemberName ?? null),
              teamMemberAvatar: Value(feed.teamMemberAvatar ?? null),
              designationName: Value(feed.designationName ?? null),
              show: includedTeamMemberIds.contains(feed.teamMemberID) ? Value(true) : Value(false),
            );
            await database.teamMemberDao.insertOrUpdateTeamMember(teamMember);
          });

          // Insert feed medias into database
          List<MediaItemModel> mediaList = feed.mediaList ?? [];
          await Future.forEach(mediaList, (MediaItemModel value) async {
            var media = MediasCompanion(
              mediaID: Value(value.mediaID!),
              projectID: Value(feed.projectID!),
              feedID: Value(feed.feedID!),
              url: Value(value.url ?? null),
              fileExtension: Value(value.fileExtension ?? null),
              feedUuid: Value(feed.uuid != '' ? feed.uuid : newUUID)
            );
            var associateMedia = AssociateMediasCompanion(
              mediaID: Value(value.mediaID!),
              projectID: Value(feed.projectID!),
              projectFeedsMedia: Value(value.url),
              projectFeedsMediaType: Value(value.fileExtension),
              createdDateTime: Value(feed.postDateTime!),
              caption: Value(feed.description!),
              module: Value("workplace"),
            );
            await database.mediaDao.insertOrUpdateMedia(media);
            await database.associateMediaDao.insertOrUpdateAssociateMedia(associateMedia);
          });

          // called feedReplies API when replyCount is not null and greater than 0
          if (feed.replyCount != null && feed.replyCount! > 0) {
            // print(feed.replyCount);
            fetchAndSaveFeedRepliesToDB(projectID: feed.projectID!, feedID: feed.feedID!);
          }
        });

        if (fetchAll) {
          int totalItems = feedResponse.totalItems ?? 0;
          int limitPerPage = feedResponse.limitPerPage ?? 50;
          int currentPageNo = feedResponse.currentPageNo ?? 1;
          int totalPages = (totalItems/limitPerPage).ceil();

          double progress = (1 / totalPages) * 30;
          if (callback != null) callback(progress);

          // If more than 1 page, then continue calling API
          if (totalItems > 0 && totalPages > (feedResponse.currentPageNo ?? 1)) {
            currentPageNo++;
            await fetchAndSaveFeedsToDB(projectID: projectID, pageKey: currentPageNo, callback: callback);
          }
        }
      });
    } else {
      print('No Internet Connection! Called Feeds API Failed!');
    }
    return null;
  }

  Future fetchAndSaveFeedRepliesToDB({required int projectID, required int feedID, int pageKey = 1}) async {
    if (!appViewModel.isConnectionFailed.value){
      await getReplyFeedList(projectID: projectID, feedID: feedID, pageKey: pageKey).then((response) async {
        ReplyFeedModel replyResponse = ReplyFeedModel.fromJson(response.data);
        
        int? lastReadFeedID;
        if (pageKey == 1) {
          lastReadFeedID = replyResponse.response?.lastReadFeedID ?? null;
          // Update parent feed lastReadFeedID
          await database.feedDao.getSingleFeed(feedID).then((Feed? parentFeed) async {
            if (parentFeed != null) {
              parentFeed = parentFeed.copyWith(lastReadFeedID: Value(lastReadFeedID));
              await database.feedDao.updateFeed(parentFeed);
            }
          });
        }

        List<FeedItemModel> replyFeedList = replyResponse.response?.feedList != null ? replyResponse.response!.feedList! : [];
        await Future.forEach(replyFeedList, (FeedItemModel replyFeed) async {
          var newUUID = Uuid().v4();
          // Insert/update reply feeds into database
          await database.feedDao.getFeedbyUuid(replyFeed.uuid!).then((matchedFeed) async {
            bool updated = false;

            if (matchedFeed != null) {
              if (matchedFeed.feedID <= -1) {
                await database.feedDao.updateFeedStatus(replyFeed.uuid!, replyFeed.feedID!, 1);
                await database.mediaDao.updateMediaFeedID(matchedFeed.feedID, replyFeed.feedID!);
              }

              if (matchedFeed.updatedDateTime != null) {
                DateTime serverUpdateDateTime = DateTime.parse(replyFeed.updatedDateTime!);
                DateTime localUpdateDateTime = DateTime.parse(matchedFeed.updatedDateTime!);

                Duration difference = serverUpdateDateTime.difference(localUpdateDateTime);

                if (difference.inSeconds <= 0) {
                  updated = true;
                }
              }
            }

            if (!updated) {
              var isRead = 'read';
              if (lastReadFeedID != null) {
                if (replyFeed.feedID! > lastReadFeedID) {
                  isRead = 'unread';
                }
              }
              await database.feedDao.insertOrUpdateFeeds(FeedsCompanion(
                uuid: Value(replyFeed.uuid != '' ? replyFeed.uuid : newUUID),
                feedID: Value(replyFeed.feedID!),
                parentID: Value(feedID),
                projectID: Value(replyFeed.projectID!),
                description: Value(replyFeed.description ?? null),
                feedType: Value(replyFeed.feedType ?? null),
                postDateTime: Value(replyFeed.postDateTime ?? null),
                topicID: Value(replyFeed.topicID ?? null),
                teamMemberID: Value(replyFeed.teamMemberID ?? null),
                replyCount: Value(replyFeed.replyCount ?? 0),
                replyReadStatus: Value(replyFeed.replyReadStatus ?? null),
                backgroundColor: Value(replyFeed.backgroundColor ?? null),
                outlineColor: Value(replyFeed.outlineColor ?? null),
                fontNameColor: Value(replyFeed.fontNameColor ?? null),
                fontPositionColor: Value(replyFeed.fontPositionColor ?? null),
                fontDescriptionColor: Value(replyFeed.fontDescriptionColor ?? null),
                fontDateColor: Value(replyFeed.fontDateColor ?? null),
                sendStatus: Value(1),
                isRead: Value(isRead),
              ));
            }
          });

          // Insert/update team member into database
          await database.teamMemberDao.getTeamMembersByProject(replyFeed.projectID!).then((value) {
            var includedTeamMemberIds = value.map((e) => e.show ? e.teamMemberID : null);
            var teamMember = TeamMembersCompanion(
              projectID: Value(replyFeed.projectID!), 
              teamMemberID: Value(replyFeed.teamMemberID!),
              teamMemberName: Value(replyFeed.teamMemberName ?? null),
              teamMemberAvatar: Value(replyFeed.teamMemberAvatar ?? null),
              designationName: Value(replyFeed.designationName ?? null),
              show: includedTeamMemberIds.contains(replyFeed.teamMemberID) ? Value(true) : Value(false),
            );
            database.teamMemberDao.insertOrUpdateTeamMember(teamMember);
          });

          // Insert feed medias into database
          List<MediaItemModel> mediaList = replyFeed.mediaList ?? [];
          await Future.forEach(mediaList, (MediaItemModel value) async {
            var media = MediasCompanion(
              mediaID: Value(value.mediaID!),
              projectID: Value(replyFeed.projectID!),
              feedID: Value(replyFeed.feedID!),
              url: Value(value.url ?? null),
              fileExtension: Value(value.fileExtension ?? null),
              feedUuid: Value(replyFeed.uuid != '' ? replyFeed.uuid : newUUID)
            );
            database.mediaDao.insertOrUpdateMedia(media);
          });

          int totalItems = replyResponse.totalItems ?? 0;
          int limitPerPage = replyResponse.limitPerPage ?? 50;
          int currentPageNo = replyResponse.currentPageNo ?? 1;
          int totalPages = (totalItems/limitPerPage).ceil();

          // If more than 1 page, then continue calling API
          if (totalItems > 0 && totalPages > (replyResponse.currentPageNo ?? 1)) {
            currentPageNo++;
            fetchAndSaveFeedRepliesToDB(projectID: projectID, feedID: feedID, pageKey: currentPageNo);
          }
        });
      });
    } else {
      print('No Internet Connection! Called Reply Feeds API Failed!');
    }
  }

  // Step 5: Get all alerts
  Future getAlertList({int pageKey = 1, bool fetchAll = true, Function? callback}) async {
    try {
      if (AppDataModel.getByPassCode()) {
        return await api.getHTTP('NotificationList?currentPageNo=$pageKey&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}').then((response) async {
            AlertModel alertModel = AlertModel.fromJson(response.data);
            List<AlertDetails> alertList = alertModel.response ?? [];
            await Future.forEach(alertList, (AlertDetails item) async {
              item.delayTaskIDs = item.taskIDs?.join("|");
              Alert alert = Alert.fromJson(item.toJson());
              await database.alertDao.insertOrUpdateAlert(alert);
            });

            if (fetchAll) {
              int totalItems = alertModel.totalItems ?? 0;
              int limitPerPage = alertModel.limitPerPage ?? 50;
              int currentPageNo = alertModel.currentPageNo ?? 1;
              int totalPages = (totalItems/limitPerPage).ceil();

              double progress = (1 / totalPages) * 30;
              if (callback != null) callback(progress);
              
              // If more than 1 page, then continue calling API
              if (totalItems > 0 && totalPages > (alertModel.currentPageNo ?? 1)) {
                currentPageNo++;
                await getAlertList(pageKey: currentPageNo, callback: callback);
              }
            }
          }
        )
        .onError((error, stackTrace) {
          print('Called Notification List API Failed!');
        });

      }else{
          return await api.getHTTP('NotificationList?currentPageNo=$pageKey').then((response) async {
            AlertModel alertModel = AlertModel.fromJson(response.data);
            List<AlertDetails> alertList = alertModel.response ?? [];
            await Future.forEach(alertList, (AlertDetails item) async {
              item.delayTaskIDs = item.taskIDs?.join("|");
              Alert alert = Alert.fromJson(item.toJson());
              await database.alertDao.insertOrUpdateAlert(alert);
            });

            if (fetchAll) {
              int totalItems = alertModel.totalItems ?? 0;
              int limitPerPage = alertModel.limitPerPage ?? 50;
              int currentPageNo = alertModel.currentPageNo ?? 1;
              int totalPages = (totalItems/limitPerPage).ceil();

              double progress = (1 / totalPages) * 30;
              if (callback != null) callback(progress);
              
              // If more than 1 page, then continue calling API
              if (totalItems > 0 && totalPages > (alertModel.currentPageNo ?? 1)) {
                currentPageNo++;
                await getAlertList(pageKey: currentPageNo, callback: callback);
              }
            }
          }
        )
        .onError((error, stackTrace) {
          print('Called Notification List API Failed!');
        });
      }
    }

    on Exception catch (_) {
      // print('calling alert API');
      // print(_);
      // throw FetchDataException;
    }
    return null;
  }
}
