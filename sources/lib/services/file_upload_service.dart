import 'package:dio/dio.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/constants/app_data_models.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/models/feed_post_model.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/services/task_services.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/error_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/view_models/app_view_model.dart';

class FileUploadService {
  final Api api = Api();
  final AppViewModel appViewModel = locator<AppViewModel>();
  final database = locator<AppDatabase>();
  TaskService _taskService = TaskService();

  static getFeedList({required int projectID, int pageKey = 1, String lastUpdateDateTime = ''}) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('/FeedList?projectID=$projectID&currentPageNo=$pageKey&updateDateTime=$lastUpdateDateTime&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
        return response;
      }else{
        Response response = await Api().getHTTP('/FeedList?projectID=$projectID&currentPageNo=$pageKey&updateDateTime=$lastUpdateDateTime');
        return response;
      }
    }
    on DioException catch (e) {
      print("debug FeedList API error: $projectID $pageKey $lastUpdateDateTime");
      throw ErrorUtils.handleError(e);
    }
  }

  static postFeed(FeedPostModel data) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().postHTTP('/Feed?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', data.toJson());
        return response;
      }else{
        Response response = await Api().postHTTP('/Feed', data.toJson());
        return response;
      }
    }
    on DioException catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  Future taskFileUpload(context, mediaPath) async {
    try {
      final formData = FormData.fromMap({'fileUpload': await MultipartFile.fromFile(mediaPath)});
      try {
        return await _taskService.postMyTaskFileUpload(formData);
      } catch (e) {
        showError(context, e.toString());
      }
    } catch (e) {
      if (e == 'Token mismatched.') {
        goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
      }
    }
  }

  Future postMedia(dynamic formData) async {
    try {
      if (AppDataModel.getByPassCode()) {
        return await api.postHTTP('FeedFileUpload?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', formData);
      }else{
        return await api.postHTTP('FeedFileUpload', formData);
      }
    } on DioException catch (e) {
      print("debug FeedFileUpload API error: $e");
      if (ErrorUtils.handleError(e) == 'Token mismatched.') {
        goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
      }
    }
  }
}