import 'package:dio/dio.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/constants/app_data_models.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/models/feed_topic_model.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/error_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/view_models/app_view_model.dart';

class TopicService {
  final Api api = Api();
  final database = locator<AppDatabase>();
  final AppViewModel appViewModel = locator<AppViewModel>();

  static getTopicList() async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('/FeedTopic?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
        return response;
      }else{
        Response response = await Api().getHTTP('/FeedTopic');
        return response;
      }
    }
    on DioException catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static potTopicList(Map<String, List> formData) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().postHTTP('FeedTopic?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', formData);
        return response;
      }else{
        Response response = await Api().postHTTP('FeedTopic', formData);
        return response;
      }
    }
    on DioException catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  // Fetch feed topic from API and save to DB
  Future fetchTopicListAndSaveToDB() async {
    if (!appViewModel.isConnectionFailed.value) {
      try{
        getTopicList().then((response) {
          FeedTopicModel topicResponse = FeedTopicModel.fromJson(response.data);
          if (topicResponse.response?.topicList != null) {
            int index = 0;
            topicResponse.response?.topicList!.forEach((FeedTopic item) {
              var json = item.toJson();
              json['weight'] = index;
              index++;
              Topic topic = Topic.fromJson(json);
              database.topicDao.insertOrUpdateTopic(topic);
            });
          }
        });
      } catch (e) {
        if (e == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called Topic List API Failed!');
    }
  }

  Future saveTopicOrder(List<Topic> topics) async {
    // Save into local db
    topics.asMap().forEach((index, topic) { 
      topic = topic.copyWith(weight: index);
      database.topicDao.insertOrUpdateTopic(topic);
    });

    // Update to remote server
    final Map<String, List> formData = {
      "topicList": topics
    };
    try {
      potTopicList(formData);
    } catch (e) {
      if (e == 'Token mismatched.') {
        goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
      }
    }
  }
}
