import 'package:dio/dio.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/constants/app_data_models.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/error_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/view_models/app_view_model.dart';

// Workplace service for API calling
class ProfileService {
  final Api api = Api();
  final AppViewModel appViewModel = locator<AppViewModel>();
  final database = locator<AppDatabase>();

  // Call API to get profile details and store into local db
  Future getProfileList() async {
    try {
      if (AppDataModel.getByPassCode()) {
        await api.getHTTP('Profile?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}')
        .then((response) {
            Profile profile = Profile.fromJson(response.data['response']['profileDetails']);
            database.profileDao.insertOrUpdateProfile(profile);
          }
        );
      }else{
        await api.getHTTP('Profile')
        .then((response) {
            Profile profile = Profile.fromJson(response.data['response']['profileDetails']);
            database.profileDao.insertOrUpdateProfile(profile);
          }
        );
      }
    } on DioException catch (e) {
      print("debug Profile API error: ");
      if (ErrorUtils.handleError(e) == 'Token mismatched.') {
        goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
      }
    }
  }

  cleanDatabase() async {
    database.profileDao.deleteAllProfiles();
    database.teamMemberDao.deleteAllTeamMember();
    database.costEstimationDao.deleteAllCostEstimation();
    database.mediaDao.deleteAllMedias();
    database.taskMediaDao.deleteAllTaskMedias();
    database.associateMediaDao.deleteAllAssociateMedias();
    database.feedDao.deleteAllFeeds();
    database.workplaceDao.deleteAllWorkplaces();
    database.alertDao.deleteAllAlerts();
  }
}