import 'package:dio/dio.dart';
import 'package:sources/services/api.dart';
import 'package:sources/utils/error_utils.dart';
import 'package:sources/constants/app_data_models.dart';

class AppService {
  static getConfig() async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('Config?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', checking: false);
        return response;
      }else{
        Response response = await Api().getHTTP('Config', checking: false);
        return response;
      }
    } 
    on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static getProfile() async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('Profile?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
        return response;
      }else{
        Response response = await Api().getHTTP('Profile');
        return response;
      }
    }
    on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }
}