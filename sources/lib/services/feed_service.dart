import 'package:dio/dio.dart';
import 'package:drift/drift.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/constants/app_data_models.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/models/attach_model.dart';
import 'package:sources/models/feed_item_model.dart';
import 'package:sources/models/feed_model.dart';
import 'package:sources/models/feed_post_model.dart';
import 'package:sources/models/general_response_model.dart';
import 'package:sources/models/recover_workplace_model.dart';
import 'package:sources/models/reply_feed_model.dart';
import 'package:sources/models/workplace_media_model.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/error_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/view_models/app_view_model.dart';
import 'package:uuid/uuid.dart';

class FeedService {
  final Api api = Api();
  final AppViewModel appViewModel = locator<AppViewModel>();
  final database = locator<AppDatabase>();

  static getFeedList({required int projectID, int pageKey = 1, String lastUpdateDateTime = ''}) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('/FeedList?projectID=$projectID&currentPageNo=$pageKey&updateDateTime=$lastUpdateDateTime&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
        return response;
      }else{
        Response response = await Api().getHTTP('/FeedList?projectID=$projectID&currentPageNo=$pageKey&updateDateTime=$lastUpdateDateTime');
        return response;
      }
    }
    on DioException catch (e) {
      print("debug FeedList API error: $projectID $pageKey $lastUpdateDateTime");
      throw ErrorUtils.handleError(e);
    }
  }

  static getReplyFeedList({required int projectID, required int feedID, int pageKey = 1, String lastUpdateDateTime = ''}) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('/ReplyFeedList?projectID=$projectID&feedID=$feedID&currentPageNo=$pageKey&updateDateTime=$lastUpdateDateTime&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
        return response;
      }else{
        Response response = await Api().getHTTP('/ReplyFeedList?projectID=$projectID&feedID=$feedID&currentPageNo=$pageKey&updateDateTime=$lastUpdateDateTime');
        return response;
      }
    }
    on DioException catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static getFeedDetails(int feedID) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('/FeedDetail?feedID=$feedID&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
        return response;
      }else{
        Response response = await Api().getHTTP('/FeedDetail?feedID=$feedID');
        return response;
      }
    }
    on DioException catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static getWorkplaceMedia(int projectID) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().getHTTP('/WorkplaceMedia?projectID=$projectID&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}');
        return response;
      }else{
        Response response = await Api().getHTTP('/WorkplaceMedia?projectID=$projectID');
        return response;
      }
    }
    on DioException catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static postFeed(FeedPostModel data) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().postHTTP('/Feed?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', data.toJson());
        return response;
      }else{
        Response response = await Api().postHTTP('/Feed', data.toJson());
        return response;
      }
    }
    on DioException catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static putFeed(FeedPutModel data) async {
    try {
      if (AppDataModel.getByPassCode()) {
        Response response = await Api().putHTTP('/Feed?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', data.toJson());
        return response;
      }else{
        Response response = await Api().putHTTP('/Feed', data.toJson());
        return response;
      }
    }
    on DioException catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  Future fetchAndSaveFeedsToDB({required int projectID, int pageKey = 1, String lastUpdateDateTime = ''}) async {
    if (!appViewModel.isConnectionFailed.value){
      try {
        await getFeedList(projectID: projectID, pageKey: pageKey, lastUpdateDateTime: lastUpdateDateTime).then((response) async {
          FeedModel feedResponse = FeedModel.fromJson(response.data);
          
          int? lastReadFeedID;
          if (pageKey == 1) {
            lastReadFeedID = feedResponse.response?.lastReadFeedID ?? null;
            // Update workplace last read feed ID
            await database.workplaceDao.getSingleWorkplace(projectID).then((Workplace? workplace) async {
              if (workplace != null) {
                workplace = workplace.copyWith(
                  lastReadFeedID: Value(lastReadFeedID),
                  completedTasks: Value(feedResponse.response?.projectDetail?[0].completedTasks),
                  totalTasks: Value(feedResponse.response?.projectDetail?[0].totalTasks),
                );
                await database.workplaceDao.updateWorkplace(workplace);
              }
            });
          }
          
          List<FeedItemModel> feedList = feedResponse.response!.feedList ?? [];
          feedList.forEach((FeedItemModel feed) async {
            var newUUID = Uuid().v4();
            // Insert/update feeds into database
            await database.feedDao.getFeedbyUuid(feed.uuid!).then((matchedFeed) async {
              bool updated = false;

              if (matchedFeed != null) {
                if (matchedFeed.feedID <= -1) {
                  await database.feedDao.updateFeedStatus(feed.uuid!, feed.feedID!, 1);
                  await database.mediaDao.updateMediaFeedID(matchedFeed.feedID, feed.feedID!);
                }
                if (matchedFeed.updatedDateTime != null) {
                  DateTime serverUpdateDateTime = DateTime.parse(feed.updatedDateTime!);
                  DateTime localUpdateDateTime = DateTime.parse(matchedFeed.updatedDateTime!);

                  Duration difference = serverUpdateDateTime.difference(localUpdateDateTime);

                  if (difference.inSeconds <= 0) {
                    updated = true;
                  }
                }
              }

              if (!updated || matchedFeed?.replyCount != feed.replyCount) {
                var isRead = 'read';
                if (lastReadFeedID != null) {
                  if (feed.feedID! > lastReadFeedID) {
                    isRead = 'unread';
                  }
                }
                await database.feedDao.insertOrUpdateFeeds(FeedsCompanion(
                  uuid: Value(feed.uuid != '' ? feed.uuid : newUUID),
                  feedID: Value(feed.feedID!),
                  parentID: Value(null),
                  projectID: Value(feed.projectID!),
                  description: Value(feed.description ?? null),
                  feedType: Value(feed.feedType ?? null),
                  postDateTime: Value(feed.postDateTime ?? null),
                  updatedDateTime: Value(feed.updatedDateTime ?? null),
                  topicID: Value(feed.topicID ?? null),
                  teamMemberID: Value(feed.teamMemberID ?? null),
                  moduleID: Value(feed.moduleID ?? null),
                  moduleName: Value(feed.moduleName ?? null),
                  updatesDescription: Value(feed.updatesInfo?.updatesDescription ?? null),
                  replyTeamMemberAvatar: Value(feed.replyTeamMemberAvatar!.join('|')),
                  replyCount: Value(feed.replyCount ?? 0),
                  replyReadStatus: Value(feed.replyReadStatus ?? null),
                  backgroundColor: Value(feed.backgroundColor ?? null),
                  outlineColor: Value(feed.outlineColor ?? null),
                  fontNameColor: Value(feed.fontNameColor ?? null),
                  fontPositionColor: Value(feed.fontPositionColor ?? null),
                  fontDescriptionColor: Value(feed.fontDescriptionColor ?? null),
                  fontDateColor: Value(feed.fontDateColor!),
                  sendStatus: Value(1),
                  isRead: Value(isRead),
                ));

                feed.updatesInfo?.updatesImages?.forEach((value) async {
                  await saveMediaToDB(feed, value);
                });

                feed.updatesInfo?.updatesFiles?.forEach((value) async {
                  await saveMediaToDB(feed, value);
                });
                List<int> imageIDs = await feed.updatesInfo?.updatesImages?.map((e) => e.taskImageID ?? 0).toList() ?? [];
                imageIDs.addAll(feed.updatesInfo?.updatesFiles?.map((e) => e.taskImageID ?? 0).toList() ?? []);
                await database.taskMediaDao.getTaskMediasByExcludedIds(feed.feedID!, imageIDs).then((taskMedias) {
                  taskMedias.forEach((taskMedia) async {
                    await database.associateMediaDao.deleteAssociateMedia(projectID, taskMedia.taskMediaID);
                  });
                });
                await database.taskMediaDao.deleteTaskMediasByExcludedIds(feed.feedID!, imageIDs);
              }
            });

            // Insert/update team member into database
            await database.teamMemberDao.getTeamMembersByProject(feed.projectID!).then((value) async {
              var includedTeamMemberIds = value.map((e) => e.show ? e.teamMemberID : null);
              var teamMember = TeamMembersCompanion(
                projectID: Value(feed.projectID!), 
                teamMemberID: Value(feed.teamMemberID!),
                teamMemberName: Value(feed.teamMemberName ?? null),
                teamMemberAvatar: Value(feed.teamMemberAvatar ?? null),
                designationName: Value(feed.designationName ?? null),
                show: includedTeamMemberIds.contains(feed.teamMemberID) ? Value(true) : Value(false),
              );
              await database.teamMemberDao.insertOrUpdateTeamMember(teamMember);
            });

            // remove deleted media from local DB
            await database.mediaDao.deleteMediasByExcludedIds(feed.feedID!, feed.mediaList?.map((e) => e.mediaID!).toList() ?? []);
            
            // Insert feed medias into database
            feed.mediaList?.forEach((value) async {
              var media = MediasCompanion(
                mediaID: Value(value.mediaID!),
                projectID: Value(feed.projectID!),
                feedID: Value(feed.feedID!),
                url: Value(value.url ?? null),
                fileExtension: Value(value.fileExtension ?? null),
                // localFilePath: Value(savedLocalFilePath ?? null),
                feedUuid: Value(feed.uuid != '' ? feed.uuid : newUUID)
              );
              var associateMedia = AssociateMediasCompanion(
                mediaID: Value(value.mediaID!),
                projectID: Value(feed.projectID!),
                projectFeedsMedia: Value(value.url),
                projectFeedsMediaType: Value(value.fileExtension),
                createdDateTime: Value(feed.postDateTime!),
                caption: Value(feed.description!),
                module: Value("workplace"),
              );
              await database.mediaDao.insertOrUpdateMedia(media);
              await database.associateMediaDao.insertOrUpdateAssociateMedia(associateMedia);
            });
          });

          int totalItems = feedResponse.totalItems ?? 0;
          int limitPerPage = feedResponse.limitPerPage ?? 50;
          int currentPageNo = feedResponse.currentPageNo ?? 1;
          int totalPages = (totalItems/limitPerPage).ceil();

          // If more than 1 page, then continue calling API
          if (totalItems > 0 && totalPages > (feedResponse.currentPageNo ?? 1)) {
            currentPageNo++;
            await fetchAndSaveFeedsToDB(projectID: projectID, pageKey: currentPageNo, lastUpdateDateTime: lastUpdateDateTime);
          }
        });
      } catch (e) {
        if (e == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called Feeds API Failed!');
    }
  }

  Future fetchAndSaveFeedRepliesToDB({required int projectID, required int feedID, int pageKey = 1, String lastUpdateDateTime = ''}) async {
    if (!appViewModel.isConnectionFailed.value){
      try {
        await getReplyFeedList(projectID: projectID, feedID: feedID, pageKey: pageKey, lastUpdateDateTime: lastUpdateDateTime).then((response) {
          ReplyFeedModel replyResponse = ReplyFeedModel.fromJson(response.data);

          int? lastReadFeedID;
          if (pageKey == 1) {
            lastReadFeedID = replyResponse.response?.lastReadFeedID ?? null;
            // Update parent feed lastReadFeedID
            database.feedDao.getSingleFeed(feedID).then((Feed? parentFeed) {
              if (parentFeed != null) {
                parentFeed = parentFeed.copyWith(lastReadFeedID: Value(lastReadFeedID));
                database.feedDao.updateFeed(parentFeed);
              }
            });
          }

          List<FeedItemModel> replyFeedList = replyResponse.response?.feedList != null ? replyResponse.response!.feedList! : [];
          replyFeedList.forEach((FeedItemModel replyFeed) {
            var newUUID = Uuid().v4();
            // Insert/update reply feeds into database
            database.feedDao.getFeedbyUuid(replyFeed.uuid!).then((matchedFeed) {
              bool updated = false;

              if (matchedFeed != null) {
                if (matchedFeed.feedID <= -1) {
                  database.feedDao.updateFeedStatus(replyFeed.uuid!, replyFeed.feedID!, 1);
                  database.mediaDao.updateMediaFeedID(matchedFeed.feedID, replyFeed.feedID!);
                }

                if (matchedFeed.updatedDateTime != null) {
                  DateTime serverUpdateDateTime = DateTime.parse(replyFeed.updatedDateTime!);
                  DateTime localUpdateDateTime = DateTime.parse(matchedFeed.updatedDateTime!);

                  Duration difference = serverUpdateDateTime.difference(localUpdateDateTime);

                  if (difference.inSeconds <= 0) {
                    updated = true;
                  }
                }
              }

              if (!updated) {
                var isRead = 'read';
                if (lastReadFeedID != null) {
                  if (replyFeed.feedID! > lastReadFeedID) {
                    isRead = 'unread';
                  }
                }
                database.feedDao.insertOrUpdateFeeds(FeedsCompanion(
                  uuid: Value(replyFeed.uuid != '' ? replyFeed.uuid : newUUID),
                  feedID: Value(replyFeed.feedID!),
                  parentID: Value(feedID),
                  projectID: Value(replyFeed.projectID!),
                  description: Value(replyFeed.description ?? null),
                  feedType: Value(replyFeed.feedType ?? null),
                  postDateTime: Value(replyFeed.postDateTime ?? null),
                  topicID: Value(replyFeed.topicID ?? null),
                  teamMemberID: Value(replyFeed.teamMemberID ?? null),
                  replyCount: Value(replyFeed.replyCount ?? 0),
                  replyReadStatus: Value(replyFeed.replyReadStatus ?? null),
                  backgroundColor: Value(replyFeed.backgroundColor ?? null),
                  outlineColor: Value(replyFeed.outlineColor ?? null),
                  fontNameColor: Value(replyFeed.fontNameColor ?? null),
                  fontPositionColor: Value(replyFeed.fontPositionColor ?? null),
                  fontDescriptionColor: Value(replyFeed.fontDescriptionColor ?? null),
                  fontDateColor: Value(replyFeed.fontDateColor ?? null),
                  sendStatus: Value(1),
                  isRead: Value(isRead),
                ));
              }
            });

            // Insert/update team member into database
            database.teamMemberDao.getTeamMembersByProject(replyFeed.projectID!).then((value) {
              var includedTeamMemberIds = value.map((e) => e.show ? e.teamMemberID : null);
              var teamMember = TeamMembersCompanion(
                projectID: Value(replyFeed.projectID!), 
                teamMemberID: Value(replyFeed.teamMemberID!),
                teamMemberName: Value(replyFeed.teamMemberName ?? null),
                teamMemberAvatar: Value(replyFeed.teamMemberAvatar ?? null),
                designationName: Value(replyFeed.designationName ?? null),
                show: includedTeamMemberIds.contains(replyFeed.teamMemberID) ? Value(true) : Value(false),
              );
              database.teamMemberDao.insertOrUpdateTeamMember(teamMember);
            });

            // remove deleted media from local DB
            database.mediaDao.deleteMediasByExcludedIds(replyFeed.feedID!, replyFeed.mediaList?.map((e) => e.mediaID!).toList() ?? []);
            
            // Insert feed medias into database
            replyFeed.mediaList?.forEach((value) {
              var media = MediasCompanion(
                mediaID: Value(value.mediaID!),
                projectID: Value(replyFeed.projectID!),
                feedID: Value(replyFeed.feedID!),
                url: Value(value.url ?? null),
                fileExtension: Value(value.fileExtension ?? null),
                feedUuid: Value(replyFeed.uuid != '' ? replyFeed.uuid : newUUID)
              );
              database.mediaDao.insertOrUpdateMedia(media);
            });

            int totalItems = replyResponse.totalItems ?? 0;
            int limitPerPage = replyResponse.limitPerPage ?? 50;
            int currentPageNo = replyResponse.currentPageNo ?? 1;
            int totalPages = (totalItems/limitPerPage).ceil();

            // If more than 1 page, then continue calling API
            if (totalItems > 0 && totalPages > (replyResponse.currentPageNo ?? 1)) {
              currentPageNo++;
              fetchAndSaveFeedRepliesToDB(projectID: projectID, feedID: feedID, pageKey: currentPageNo, lastUpdateDateTime: lastUpdateDateTime);
            }
          });
        });
      } catch (e) {
        if (e == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called Reply Feeds API Failed!');
    }
  }

  Future fetchAndSaveAssociateMediasToDB({required int projectID}) async {
    if (!appViewModel.isConnectionFailed.value){
      try {
        await getWorkplaceMedia(projectID).then((response) {
          WorkplaceMediaModel workplaceMediaModel = WorkplaceMediaModel.fromJson(response.data);
          database.associateMediaDao.deleteAllAssociateMedias();
          workplaceMediaModel.response?.media?.forEach((media) {
            var associateMedia = AssociateMediasCompanion(
              mediaID: Value(media.mediaID!),
              projectID: Value(projectID),
              projectFeedsMedia: Value(media.projectFeedsMedia),
              projectFeedsMediaType: Value(media.projectFeedsMediaType),
              createdDateTime: Value(media.createdDateTime!),
              caption: Value(media.caption!),
              module: Value(media.module!),
            );
            database.associateMediaDao.insertOrUpdateAssociateMedia(associateMedia);
          });
          
          workplaceMediaModel.response?.document?.forEach((media) {
            var associateMedia = AssociateMediasCompanion(
              mediaID: Value(media.mediaID!),
              projectID: Value(projectID),
              projectFeedsMedia: Value(media.projectFeedsMedia),
              projectFeedsMediaType: Value(media.projectFeedsMediaType),
              createdDateTime: Value(media.createdDateTime!),
              caption: Value(media.caption!),
              module: Value(media.module!),
            );
            database.associateMediaDao.insertOrUpdateAssociateMedia(associateMedia);
          });
        });
      } catch (e) {
        if (e == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called WorkplaceMedia API Failed!');
    }
  }

  Future recoverWorkplace(RecoverWorkplaceModel formData) async {
    try {
      if (AppDataModel.getByPassCode()) {
        return await api.postHTTP('recoverProjectFromArchive?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', formData.toJson());
      }else{
        return await api.postHTTP('recoverProjectFromArchive', formData.toJson());
      }
    } on DioException catch (e) {
      print("debug recoverProjectFromArchive API error: ");
      if (ErrorUtils.handleError(e) == 'Token mismatched.') {
        goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
      }
    }
  }

  Future postNewFeed(FeedPostModel formData) async {
    try {
      return await postFeed(formData);
    } catch (e) {
      if (e == 'Token mismatched.') {
        goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
      }
    }
  }

  Future postMedia(dynamic formData) async {
    try {
      if (AppDataModel.getByPassCode()) {
        return await api.postHTTP('FeedFileUpload?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', formData);
      }else{
        return await api.postHTTP('FeedFileUpload', formData);
      }
    } on DioException catch (e) {
      print("debug FeedFileUpload API error: ${e}");
      if (ErrorUtils.handleError(e) == 'Token mismatched.') {
        goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
      }
    }
  }

  Future updateFeed(FeedPutModel formData) async {
    try {
      return await putFeed(formData);
    } catch (e) {
      if (e == 'Token mismatched.') {
        goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
      }
    }
  }

  Future deleteFeed(Feed feed) async {
    if (feed.feedID >= 0) {
      try {
        if (AppDataModel.getByPassCode()) {
          return await api.deleteHTTP('/Feed?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', {'projectID': feed.projectID, 'feedID': feed.feedID })
          .then((response) {
            GeneralResponseModel responseModel = GeneralResponseModel.fromJson(response.data);
            if (responseModel.status == true) {
              database.feedDao.deleteFeeds(feed);
              database.mediaDao.deleteMediasByFeedID(feed.feedID);
            }
            return responseModel;
          });
        }else{
          return await api.deleteHTTP('/Feed', {'projectID': feed.projectID, 'feedID': feed.feedID })
          .then((response) {
            GeneralResponseModel responseModel = GeneralResponseModel.fromJson(response.data);
            if (responseModel.status == true) {
              database.feedDao.deleteFeeds(feed);
              database.mediaDao.deleteMediasByFeedID(feed.feedID);
            }
            return responseModel;
          });
        }
      } on DioException catch (e) {
        print("debug Feed API error: ");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      database.feedDao.deleteFeeds(feed);
      database.mediaDao.deleteMediasByFeedID(feed.feedID);
    }
  }

  // Synchronize feed list, delete feed if feedID is not exists
  Future syncDeletedFeeds(int projectID) async {
    if (!appViewModel.isConnectionFailed.value){
      try {
        if (AppDataModel.getByPassCode()) {
          return await api.getHTTP('FeedIDList?projectID=$projectID&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}')
          .then((response) {
            GeneralResponseModel responseModel = GeneralResponseModel.fromJson(response.data);
            
            List<int> activeFeedIds = [];
            activeFeedIds = List<int>.from(responseModel.response);
            // Remove workplaces if projectID is not in the array
            database.feedDao.deleteFeedsByExcludedIds(projectID, activeFeedIds);
          });
        }else{
          return await api.getHTTP('FeedIDList?projectID=$projectID')
          .then((response) {
            GeneralResponseModel responseModel = GeneralResponseModel.fromJson(response.data);
            
            List<int> activeFeedIds = [];
            activeFeedIds = List<int>.from(responseModel.response);
            // Remove workplaces if projectID is not in the array
            database.feedDao.deleteFeedsByExcludedIds(projectID, activeFeedIds);
          });
        }
      } on DioException catch (e) {
        print("debug FeedIDList API error: ");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
    } else {
      print('No Internet Connection! Called Feeds ID List API Failed!');
    }
  }

  Future updateParentFeed(int parentFeedID) async {
    try {
      await getFeedDetails(parentFeedID).then((response) {
        FeedItemModel feedModel = FeedItemModel.fromJson(response.data['response'][0]);
        database.feedDao.insertOrUpdateFeeds(FeedsCompanion(
          uuid: Value(feedModel.uuid),
          feedID: Value(feedModel.feedID!),
          projectID: Value(feedModel.projectID!),
          description: Value(feedModel.description ?? null),
          feedType: Value(feedModel.feedType ?? null),
          postDateTime: Value(feedModel.postDateTime ?? null),
          topicID: Value(feedModel.topicID ?? null),
          teamMemberID: Value(feedModel.teamMemberID ?? null),
          replyCount: Value(feedModel.replyCount ?? 0),
          replyReadStatus: Value(feedModel.replyReadStatus ?? null),
          backgroundColor: Value(feedModel.backgroundColor ?? null),
          outlineColor: Value(feedModel.outlineColor ?? null),
          fontNameColor: Value(feedModel.fontNameColor ?? null),
          fontPositionColor: Value(feedModel.fontPositionColor ?? null),
          fontDescriptionColor: Value(feedModel.fontDescriptionColor ?? null),
          fontDateColor: Value(feedModel.fontDateColor ?? null),
          sendStatus: Value(1)
        ));
      });
    } catch (e) {
      if (e == 'Token mismatched.') {
        goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
      }
    }
  }

  static getnthPrime() {
    locator.allReady().then((value) => print('done'));
    // bool testBool = locator.isRegistered(instance: AppDatabase());
    // print(testBool);
    // locator.registerSingleton(AppDatabase());
    // AppDatabase database = locator<AppDatabase>();
  }

  saveMediaToDB(FeedItemModel feed, AttachModel media) {
    var taskMedia = TaskMediasCompanion(
      taskMediaID: Value(media.taskImageID!),
      mediaName: Value(media.imageName),
      mediaType: Value(media.imageType),
      taskID: Value(feed.updatesInfo!.taskID!),
      feedID: Value(feed.feedID!),
      projectID: Value(feed.projectID!),
      teamMemberID: Value(feed.teamMemberID!),
      mediaPath: Value(media.imagePath),
    );
    var associateMedia = AssociateMediasCompanion(
      mediaID: Value(media.taskImageID!),
      projectID: Value(feed.projectID!),
      projectFeedsMedia: Value(media.imagePath),
      projectFeedsMediaType: Value(media.imageType),
      createdDateTime: Value(media.createdDateTime!),
      caption: Value(feed.updatesInfo!.updatesDescription!),
      module: Value("task"),
    );
    database.taskMediaDao.insertOrUpdateTaskMedia(taskMedia);
    database.associateMediaDao.insertOrUpdateAssociateMedia(associateMedia);
  }
}