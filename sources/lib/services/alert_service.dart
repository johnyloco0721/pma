import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/constants/app_data_models.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/models/alert_model.dart';
import 'package:sources/models/alert_post_model.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/error_utils.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/view_models/app_view_model.dart';

// Workplace service for API calling
class AlertService {
  final Api api = Api();
  var database = locator<AppDatabase>();
  final AppViewModel appViewModel = locator<AppViewModel>();

  Future<AppDatabase> getDatabase() async {
    return locator<AppDatabase>();
  }

  // Call API to get all workplace and store into local db
  Future getAlertList({int pageKey = 1, String lastUpdateDateTime = ''}) async {
    try {
      if(AppDataModel.getByPassCode()){
        await api.getHTTP('NotificationList?currentPageNo=$pageKey&updateDateTime=$lastUpdateDateTime&${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}')
        .then((response) {
            AlertModel alertModel = AlertModel.fromJson(response.data);
            alertModel.response!.forEach((AlertDetails item) {
              item.delayTaskIDs = item.taskIDs?.join("|");
              Alert alert = Alert.fromJson(item.toJson());
              database.alertDao.insertOrUpdateAlert(alert);
            });

            int totalItems = alertModel.totalItems ?? 0;
            int limitPerPage = alertModel.limitPerPage ?? 50;
            int currentPageNo = alertModel.currentPageNo ?? 1;
            int totalPages = (totalItems/limitPerPage).ceil();
            
            // If more than 1 page, then continue calling API
            if (totalItems > 0 && totalPages > (alertModel.currentPageNo ?? 1)) {
              currentPageNo++;
              getAlertList(pageKey: currentPageNo, lastUpdateDateTime: lastUpdateDateTime);
            }
          }
        )
        .onError((error, stackTrace) {
          print("Alert API error debug: $error");
          print('Called Notification List API Failed! $pageKey $lastUpdateDateTime');
        });
      }else{
        await api.getHTTP('NotificationList?currentPageNo=$pageKey&updateDateTime=$lastUpdateDateTime')
        .then((response) {
            AlertModel alertModel = AlertModel.fromJson(response.data);
            alertModel.response!.forEach((AlertDetails item) {
              item.delayTaskIDs = item.taskIDs?.join("|");
              Alert alert = Alert.fromJson(item.toJson());
              database.alertDao.insertOrUpdateAlert(alert);
            });

            int totalItems = alertModel.totalItems ?? 0;
            int limitPerPage = alertModel.limitPerPage ?? 50;
            int currentPageNo = alertModel.currentPageNo ?? 1;
            int totalPages = (totalItems/limitPerPage).ceil();
            
            // If more than 1 page, then continue calling API
            if (totalItems > 0 && totalPages > (alertModel.currentPageNo ?? 1)) {
              currentPageNo++;
              getAlertList(pageKey: currentPageNo, lastUpdateDateTime: lastUpdateDateTime);
            }
          }
        )
        .onError((error, stackTrace) {
          print("Alert API error debug: $error");
          print('Called Notification List API Failed! $pageKey $lastUpdateDateTime');
        });
      }
      } on DioError catch (e) {
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
      }
  }

  Future updateNotificationStatus(AlertPostModel formData) async {
    try {
      if(AppDataModel.getByPassCode()){
        return await api.postHTTP('NotificationDetail?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', formData.toJson());
      }else{
        return await api.postHTTP('NotificationDetail', formData.toJson());
      }
    } on DioError catch (e) {
      if (ErrorUtils.handleError(e) == 'Token mismatched.') {
        goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
      }
    }
  }

  // // Todo: Synchronize feed list, delete feed if feedID is not exists
  // Future syncDeletedAlerts() async {
  //   try {
  //     return await api.getHTTP('NotificationList')
  //     .then((response) {
  //       // Sample projectIDs array
  //       List<int> activeNotificationIds = [191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 208, 209, 210, 211, 212, 213, 214, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237];

  //       AlertDao alertDao = database.alertDao;
  //       // Remove workplaces if projectID is not in the array
  //       alertDao.deleteAlertsByExcludedIds(activeNotificationIds);
  //     });
  //   } on DioError catch (e) {
  //     if (ErrorUtils.handleError(e) == 'Token mismatched.') {
  //       goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
  //     }
  //   }
  // }

  // read all alert via API
  Future postNotificationReadAll(BuildContext context, bool isToday) async {
    String filterType = isToday ? "today" : "yesterday";
    String dateTime = formatDatetime(datetime: DateTime.now().toString(), format: "y-MM-dd 00:00:00");
    if (!appViewModel.isConnectionFailed.value){
      try {
        if (AppDataModel.getByPassCode()) {
          await Api().postHTTP('/NotificationReadAll?${AppDataModel.setByPassParameter()}=${AppDataModel.setByPassHash()}', '{ "filterType" : "$filterType" }').then((value) => print(value));
        }else{
          await Api().postHTTP('/NotificationReadAll', '{ "filterType" : "$filterType" }').then((value) => print(value));
        }
        if (isToday) {
          database.alertDao.readAllTodayUnreadAlerts(dateTime);
        }
        else {
          database.alertDao.readAllPastUnreadAlerts(dateTime);
        }
      } on DioError catch (e) {
        print("debug NotificationReadAll API error.");
        if (ErrorUtils.handleError(e) == 'Token mismatched.') {
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
        }
        showError(context, ErrorUtils.handleError(e));
      }
    } else {
      print('No Internet Connection! Called NotificationReadAll API Failed!');
    }
  }
}