import 'dart:io';
import 'dart:isolate';

import 'package:get_it/get_it.dart';
import 'package:drift/native.dart';
import 'package:drift/isolate.dart';
import 'package:drift/drift.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/view_models/app_view_model.dart';
import 'package:sources/view_models/workplace_view_model.dart';
import 'package:sqflite/sqflite.dart';

import '../view_models/comment_view_model.dart';

GetIt locator = GetIt.instance;

Future setupLocator() async {
  // create a moor executor in a new background isolate. If you want to start the isolate yourself, you
  // can also call MoorIsolate.inCurrent() from the background isolate
  // MoorIsolate isolate = await MoorIsolate.spawn(_backgroundConnection);

  // we can now create a database connection that will use the isolate internally. This is NOT what's
  // returned from _backgroundConnection, moor uses an internal proxy class for isolate communication.
  // DatabaseConnection connection = await isolate.connect();

  locator.registerSingleton(AppViewModel());
  locator.registerSingleton(CommentViewModel());
  locator.registerSingleton(WorkplaceViewModel());
}

Future setupForegroundDB() async {
  locator.registerSingleton(AppDatabase(_foregroundConnection()));
}

Future setupBackgroundDB() async {
  final connection = await _createDriftIsolateAndConnect();
  locator.registerSingleton(AppDatabase.connect(connection));
}

Future<DriftIsolate> _createDriftIsolate() async {
  final dir = await getApplicationDocumentsDirectory();
  removeOldPathSQL(dir);
  final path = p.join(dir.path, '.db_uat_v1.5.sqlite');
  final receivePort = ReceivePort();

  print(path);

  await Isolate.spawn(
    _startBackground,
    _IsolateStartRequest(receivePort.sendPort, path),
  );

  return await receivePort.first as DriftIsolate;
}

void _startBackground(_IsolateStartRequest request) {
  // this is the entry point from the background isolate! Let's create
  // the database from the path we received
  final executor = NativeDatabase(File(request.targetPath));
  // we're using DriftIsolate.inCurrent here as this method already runs on a
  // background isolate. If we used DriftIsolate.spawn, a third isolate would be
  // started which is not what we want!
  final driftIsolate = DriftIsolate.inCurrent(
    () => DatabaseConnection.fromExecutor(executor),
  );
  // inform the starting isolate about this, so that it can call .connect()
  request.sendDriftIsolate.send(driftIsolate);
}

class _IsolateStartRequest {
  final SendPort sendDriftIsolate;
  final String targetPath;

  _IsolateStartRequest(this.sendDriftIsolate, this.targetPath);
}

Future<DatabaseConnection> _createDriftIsolateAndConnect() async {
  final isolate = await _createDriftIsolate();
  return await isolate.connect();
}

// This needs to be a top-level method because it's run on a background isolate
DatabaseConnection _backgroundConnection() {
  // Construct the database to use. This example uses a non-persistent in-memory database each
  // time. You can use your existing VmDatabase with a file as well, or a `LazyDatabase` if you
  // need to construct it asynchronously.
  // When using a Flutter plugin like `path_provider` to determine the path, also see the
  // "Initialization on the main thread" section below!
  final database = NativeDatabase.memory();
  return DatabaseConnection.fromExecutor(database);
}

LazyDatabase _foregroundConnection() {
  return LazyDatabase(() async {
    final dbFolder = await getApplicationDocumentsDirectory();
    removeOldPathSQL(dbFolder);

    final file = File(p.join(dbFolder.path, '.db_uat_v1.5.sqlite'));
    print(dbFolder);
    return NativeDatabase(file);
  });
}

removeOldPathSQL(Directory dbFolder) {
  final old_1_path = p.join(dbFolder.path, 'db.sqlite');
  final old_2_path = p.join(dbFolder.path, 'db_uat_v1.0.sqlite');
  final old_3_path = p.join(dbFolder.path, 'db_uat_v1.1.sqlite');
  final old_4_path = p.join(dbFolder.path, 'db_uat_v1.2.sqlite');
  final old_5_path = p.join(dbFolder.path, 'db_uat_v1.3.sqlite');
  final old_6_path = p.join(dbFolder.path, '.db_uat_v1.4.sqlite');
  deleteDatabase(old_1_path);
  deleteDatabase(old_2_path);
  deleteDatabase(old_3_path);
  deleteDatabase(old_4_path);
  deleteDatabase(old_5_path);
  deleteDatabase(old_6_path);
}
