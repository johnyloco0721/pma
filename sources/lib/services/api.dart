import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sources/utils/dialog_utils.dart';
import 'package:sources/utils/shared_preference_utils/pref_keys.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';
import 'package:sources/utils/util_helper.dart';

class Api {
  // static final String baseUrl = 'https://test3.themakeover.my/api/v1.1.5/GarageMobile/'; // test3 environment
  // static final String baseUrl = 'https://prelive.themakeover.my/api/v1.1.5/GarageMobile/'; // prelive environment
  static final String baseUrl = 'https://garage.themakeover.my/api/v1.1.5/GarageMobile/'; // live environment
  
  static BaseOptions opts = BaseOptions(
    baseUrl: baseUrl,
    responseType: ResponseType.json,
    connectTimeout: Duration(milliseconds: 30000),
    receiveTimeout: Duration(milliseconds: 30000),
  );

  getStringValuesSF() async {
    //Return String
    String? stringValue = SpManager.getString(loginKey);
    return stringValue;
  }

  String getBaseUrl() {
    return baseUrl;
  }

  static Dio createDio() {
    var dio = Dio(opts);
    // const token = '60bf80dfa4b1579552253';
    // dio.options.headers["Authorization"] = "Bearer $token";
    dio.options.headers["contentTypeHeader"] = "application/json";
    return dio;
  }

  static final baseAPI = createDio();

  Future<Response> getHTTP(String url, {bool checking = true}) async {
    bool result = true;
    if (checking)
      result = await appVersionComparison();
    if (result) {
      await getStringValuesSF().then((val) {
        baseAPI.options.headers["Authorization"] = "Bearer $val";
      });
      return await baseAPI.get(url);
    } else {
      print(url + ": cancel");
      throw "Please update your app";
    }
  }

  Future<Response> postHTTP(String url, dynamic data, {bool checking = true}) async {
    bool result = true;
    if (checking)
      result = await appVersionComparison();
    if (result) {
      await getStringValuesSF().then((val) {
        baseAPI.options.headers["Authorization"] = "Bearer $val";
      });
      Response response = await baseAPI.post(url, data: data);
      List<String> whitelist = ['Login', 'PasswordReset'];
      if (response.data["status"] == true || whitelist.contains(url)) {
        return response;
      } else if (response.data["status"] == 'task-deleted' || response.data["status"] == 'task-update-conflict'|| whitelist.contains(url)) {
        throw response.data['status'];
      } else {
        throw response.data["message"];
      }
    } else {
      print(url + ": cancel");
      throw "Please update your app";
    }
  }

  Future<Response> putHTTP(String url, dynamic data) async {
    bool result = await appVersionComparison();
    if (result) {
      await getStringValuesSF().then((val) {
        baseAPI.options.headers["Authorization"] = "Bearer $val";
      });
      Response response = await baseAPI.put(url, data: data);
      if (response.data["status"] == true) {
        return response;
      } else {
        throw response.data["message"];
      }
    } else {
      print(url + ": cancel");
      throw "Please update your app";
    }
  }

  Future<Response> deleteHTTP(String url, dynamic data) async {
    bool result = await appVersionComparison();
    if (result) {
      await getStringValuesSF().then((val) {
        baseAPI.options.headers["Authorization"] = "Bearer $val";
      });
      Response response = await baseAPI.delete(url, data: data);
      if (response.data["status"] == true) {
        return response;
      } else {
        throw response.data["message"];
      }
    } else {
      print(url + ": cancel");
      throw "Please update your app";
    }
  }

  Future downloadFile(String url, String type, BuildContext context, {bool isShow = true, bool saveToGallery = true}) async {
    try {
      await getStringValuesSF().then((val) {
        baseAPI.options.headers["Authorization"] = "Bearer $val";
      });

      var dir;
      if (Platform.isAndroid) {
        if (await requestPermission(Permission.storage)) {
          dir = await getTemporaryDirectory(); // Only for android
        } else {
          dir = await getTemporaryDirectory(); //for android 13
        }
      } else if (Platform.isIOS) {
        if (await requestPermission(Permission.storage)) {
          dir = await getTemporaryDirectory(); // Should be working for both ios and android, but don't know why android not working
        } else {
          showDialog(
            context: context,
            builder: (BuildContext context) => CupertinoAlertDialog(
              title: new Text('Error'),
              content: new Text('Access to storage has been prohibited, please enable it in the Settings app to continue.'),
              actions: [
                CupertinoDialogAction(child: Text('Cancel'), onPressed: () => Navigator.of(context).pop(),),
                CupertinoDialogAction(child: Text('Settings'), onPressed: () => openAppSettings()),
              ],
            )
          );
          return false;
        }
      }

      if (!await dir.exists()) {
        await dir.create(recursive: true);
      }

      String fileName = '';

      // 23 May 2022 - jonathan said don't want show timestamp as pdf file name
      // final timestamp = DateTime.now().millisecondsSinceEpoch.toString();
      fileName = url.substring(url.lastIndexOf("/") + 1);

      if (await dir.exists()) {
        String saveFilePath = File('${dir!.path}/$fileName').path;
        if (type == 'img') {
          if (url[0] == '/') {
            saveFilePath = url;
            if (isShow)
              showToastMessage(context, message: 'Download Successfully!', duration: 1);
          } else {
            await baseAPI
              .download(url, '${dir!.path}/$fileName')
              .then((value) { 
                if (isShow)
                  showToastMessage(context, message: 'Download Successfully!', duration: 1);
              })
              .catchError((onError) {
                if (isShow)
                  showError(context, "Something went wrong, please try again!");
              });
          }
          if (saveToGallery) {
            if (Platform.isIOS) {
              await ImageGallerySaver.saveFile(saveFilePath, isReturnPathOfIOS: true).then((fileInfo) {
                File(saveFilePath).delete();
                saveFilePath = fileInfo['filePath'].substring(7);
                print(saveFilePath);
              });
            } else {
              await ImageGallerySaver.saveFile(saveFilePath);
            }
          }

          return saveFilePath;
        }
        if (type == 'pdf') {
          try {
            Response response = await baseAPI.get(
              url,
              options: Options(
                responseType: ResponseType.bytes,
                followRedirects: false,
              )
            );
            File file = File('${dir!.path}/$fileName');
            print(file.path);
            var raf = file.openSync(mode: FileMode.write);
            print(file.path);
            raf.writeFromSync(response.data);
            await raf.close();
            await SpManager.removeItem(url);
            
            if (isShow)
              showToastMessage(context, message: 'Download Successfully!', duration: 1);

            return file.path;
          }
          catch (e) {
            if (isShow)
              showError(context, "Something went wrong, please try again!");
          }
        }
      }
    } on DioException catch (e) {
      // Handle error
      return e.response!;
    }
  }

  Future<bool> requestPermission(Permission permission) async {
    if (await permission.isGranted) {
      return true;
    } else {
      var result = await permission.request();
      if (result == PermissionStatus.granted) {
        return true;
      }
    }
    return false;
  }
}
