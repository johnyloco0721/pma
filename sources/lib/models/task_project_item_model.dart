import 'package:json_annotation/json_annotation.dart';
import 'package:sources/models/stage_model.dart';

part 'task_project_item_model.g.dart';

@JsonSerializable()
class TaskProjectItemModel {
  int? projectID;
  String? propertyName;
  String? projectUnit;
  String? ownerName;
  String? ownerContactNo;
  String? taskTargetDateTime;
  int? completedTasks;
  int? totalTasks;
  String? projectStatus;
  String? taskStatus;
  int? teamMemberID;
  String? teamMemberName;
  StageModel? stage;

  TaskProjectItemModel({
    this.projectID,
    this.propertyName,
    this.projectUnit,
    this.ownerName,
    this.ownerContactNo,
    this.taskTargetDateTime,
    this.completedTasks,
    this.totalTasks,
    this.projectStatus,
    this.teamMemberID,
    this.teamMemberName,
    this.stage,
  });

  factory TaskProjectItemModel.fromJson(Map<String, dynamic> json) => _$TaskProjectItemModelFromJson(json);

  Map<String, dynamic> toJson() => _$TaskProjectItemModelToJson(this);
}
