import 'package:json_annotation/json_annotation.dart';

part 'comment_post_model.g.dart';

@JsonSerializable()
class CommentPostModel {
  final int projectID;
  final int taskID;
  final String description;
  final List<int> mediaID;
  
  CommentPostModel({
    required this.projectID,
    required this.taskID,
    required this.description,
    required this.mediaID,
  });

  factory CommentPostModel.fromJson(Map<String, dynamic> json) => _$CommentPostModelFromJson(json);

  Map<String, dynamic> toJson() => _$CommentPostModelToJson(this);
}

