// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reddot_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RedDotListModel _$RedDotListModelFromJson(Map<String, dynamic> json) =>
    RedDotListModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: json['response'] == null
          ? null
          : RedDotListModelResponse.fromJson(
              json['response'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$RedDotListModelToJson(RedDotListModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
    };

RedDotListModelResponse _$RedDotListModelResponseFromJson(
        Map<String, dynamic> json) =>
    RedDotListModelResponse(
      taskDateTime: (json['taskDateTime'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$RedDotListModelResponseToJson(
        RedDotListModelResponse instance) =>
    <String, dynamic>{
      'taskDateTime': instance.taskDateTime,
    };
