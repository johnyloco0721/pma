// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recover_workplace_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RecoverWorkplaceModel _$RecoverWorkplaceModelFromJson(
        Map<String, dynamic> json) =>
    RecoverWorkplaceModel(
      projectID: (json['projectID'] as num?)?.toInt(),
      isArchived: (json['isArchived'] as num?)?.toInt(),
    );

Map<String, dynamic> _$RecoverWorkplaceModelToJson(
        RecoverWorkplaceModel instance) =>
    <String, dynamic>{
      'projectID': instance.projectID,
      'isArchived': instance.isArchived,
    };
