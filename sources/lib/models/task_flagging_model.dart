import 'package:json_annotation/json_annotation.dart';

part 'task_flagging_model.g.dart';

@JsonSerializable()
class TaskFlaggingModel {
  int? taskID;
  int? projectID;
  String? reason;

  TaskFlaggingModel({this.taskID,this.projectID,this.reason});

  factory TaskFlaggingModel.fromJson(Map<String, dynamic> json) => _$TaskFlaggingModelFromJson(json);

  Map<String, dynamic> toJson() => _$TaskFlaggingModelToJson(this);
}