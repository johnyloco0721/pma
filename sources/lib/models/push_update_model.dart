import 'package:json_annotation/json_annotation.dart';

part 'push_update_model.g.dart';

@JsonSerializable()
class PushUpdateModel {
  final int? taskID;
  final String? action;
  final String? remark;
  final List? taskImageID;

  PushUpdateModel({
    this.taskID, 
    this.action, 
    this.remark, 
    this.taskImageID, 
  });

  factory PushUpdateModel.fromJson(Map<String, dynamic> json) => _$PushUpdateModelFromJson(json);

  Map<String, dynamic> toJson() => _$PushUpdateModelToJson(this);
}
