import 'package:json_annotation/json_annotation.dart';

part 'stage_model.g.dart';

@JsonSerializable()
class StageModel {
  String? label;
  String? iconColor;

  StageModel({this.label, this.iconColor});

  factory StageModel.fromJson(Map<String, dynamic> json) => _$StageModelFromJson(json);

  Map<String, dynamic> toJson() => _$StageModelToJson(this);
}