import 'package:json_annotation/json_annotation.dart';

part 'attach_model.g.dart';

@JsonSerializable()
class AttachModel {
  int? taskImageID;
  String? imageName;
  String? imageType;
  int? taskID;
  int? projectID;
  int? teamMemberID;
  int? selectedStatus;
  String? createdDateTime;
  String? imagePath;
  String? thumbnailPath;

  AttachModel({
    this.taskImageID,
    this.imageName,
    this.imageType,
    this.taskID,
    this.projectID,
    this.teamMemberID,
    this.selectedStatus,
    this.createdDateTime,
    this.imagePath,
    this.thumbnailPath,
  });

  factory AttachModel.fromJson(Map<String, dynamic> json) => _$AttachModelFromJson(json);

  Map<String, dynamic> toJson() => _$AttachModelToJson(this);
}