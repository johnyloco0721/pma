import 'package:json_annotation/json_annotation.dart';

part 'general_response_model.g.dart';

@JsonSerializable()
class GeneralResponseModel {
  bool? status;
  String? message;
  dynamic response;

  GeneralResponseModel({
    this.status,
    this.message,
    this.response,
  });

  factory GeneralResponseModel.fromJson(Map<String, dynamic> json) => _$GeneralResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$GeneralResponseModelToJson(this);
}
