// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_post_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskPostModel _$TaskPostModelFromJson(Map<String, dynamic> json) =>
    TaskPostModel(
      taskID: (json['taskID'] as num?)?.toInt(),
      projectID: (json['projectID'] as num?)?.toInt(),
      milestoneID: (json['milestoneID'] as num?)?.toInt(),
      templateTaskID: (json['templateTaskID'] as num?)?.toInt(),
      description: json['description'] as String?,
      taskPaymentPercentage: json['taskPaymentPercentage'] as String?,
      title: json['title'] as String?,
      targetDate: json['targetDate'] as String?,
      dueDate: json['dueDate'] as String?,
      assignTo: (json['assignTo'] as num?)?.toInt(),
      requirements: json['requirements'] == null
          ? null
          : Requirements.fromJson(json['requirements'] as Map<String, dynamic>),
      clientAppRequirements: json['clientAppRequirements'] == null
          ? null
          : ClientAppRequirements.fromJson(
              json['clientAppRequirements'] as Map<String, dynamic>),
      subTasks: (json['subTasks'] as List<dynamic>)
          .map((e) => SubTasksPostModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      oldTaskData: json['oldTaskData'] == null
          ? null
          : TaskPostPreviousModel.fromJson(
              json['oldTaskData'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TaskPostModelToJson(TaskPostModel instance) =>
    <String, dynamic>{
      'taskID': instance.taskID,
      'projectID': instance.projectID,
      'milestoneID': instance.milestoneID,
      'templateTaskID': instance.templateTaskID,
      'title': instance.title,
      'description': instance.description,
      'taskPaymentPercentage': instance.taskPaymentPercentage,
      'targetDate': instance.targetDate,
      'dueDate': instance.dueDate,
      'assignTo': instance.assignTo,
      'requirements': instance.requirements,
      'clientAppRequirements': instance.clientAppRequirements,
      'subTasks': instance.subTasks,
      'oldTaskData': instance.oldTaskData,
    };

TaskPostPreviousModel _$TaskPostPreviousModelFromJson(
        Map<String, dynamic> json) =>
    TaskPostPreviousModel(
      taskID: (json['taskID'] as num?)?.toInt(),
      projectID: (json['projectID'] as num?)?.toInt(),
      milestoneID: (json['milestoneID'] as num?)?.toInt(),
      templateTaskID: (json['templateTaskID'] as num?)?.toInt(),
      description: json['description'] as String?,
      taskPaymentPercentage: json['taskPaymentPercentage'] as String?,
      title: json['title'] as String?,
      targetDate: json['targetDate'] as String?,
      dueDate: json['dueDate'] as String?,
      assignTo: (json['assignTo'] as num?)?.toInt(),
      requirements: json['requirements'] == null
          ? null
          : Requirements.fromJson(json['requirements'] as Map<String, dynamic>),
      clientAppRequirements: json['clientAppRequirements'] == null
          ? null
          : ClientAppRequirements.fromJson(
              json['clientAppRequirements'] as Map<String, dynamic>),
      subTasks: (json['subTasks'] as List<dynamic>)
          .map((e) => SubTasksPostModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$TaskPostPreviousModelToJson(
        TaskPostPreviousModel instance) =>
    <String, dynamic>{
      'taskID': instance.taskID,
      'projectID': instance.projectID,
      'milestoneID': instance.milestoneID,
      'templateTaskID': instance.templateTaskID,
      'title': instance.title,
      'description': instance.description,
      'taskPaymentPercentage': instance.taskPaymentPercentage,
      'targetDate': instance.targetDate,
      'dueDate': instance.dueDate,
      'assignTo': instance.assignTo,
      'requirements': instance.requirements,
      'clientAppRequirements': instance.clientAppRequirements,
      'subTasks': instance.subTasks,
    };

SubTasksPostModel _$SubTasksPostModelFromJson(Map<String, dynamic> json) =>
    SubTasksPostModel(
      taskID: (json['taskID'] as num?)?.toInt() ?? 0,
      title: json['title'] as String?,
      description: json['description'] as String?,
      targetDate: json['targetDate'] as String?,
      dueDate: json['dueDate'] as String?,
      requirements: json['requirements'] == null
          ? null
          : Requirements.fromJson(json['requirements'] as Map<String, dynamic>),
      clientAppRequirements: json['clientAppRequirements'] == null
          ? null
          : ClientAppRequirements.fromJson(
              json['clientAppRequirements'] as Map<String, dynamic>),
      templateTaskID: (json['templateTaskID'] as num?)?.toInt(),
    );

Map<String, dynamic> _$SubTasksPostModelToJson(SubTasksPostModel instance) =>
    <String, dynamic>{
      'taskID': instance.taskID,
      'title': instance.title,
      'description': instance.description,
      'targetDate': instance.targetDate,
      'dueDate': instance.dueDate,
      'requirements': instance.requirements,
      'clientAppRequirements': instance.clientAppRequirements,
      'templateTaskID': instance.templateTaskID,
    };
