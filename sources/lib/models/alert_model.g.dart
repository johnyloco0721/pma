// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'alert_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AlertModel _$AlertModelFromJson(Map<String, dynamic> json) => AlertModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: (json['response'] as List<dynamic>?)
          ?.map((e) => AlertDetails.fromJson(e as Map<String, dynamic>))
          .toList(),
      totalBadge: (json['totalBadge'] as num?)?.toInt(),
      totalItems: (json['totalItems'] as num?)?.toInt(),
      currentPageNo: (json['currentPageNo'] as num?)?.toInt(),
      limitPerPage: (json['limitPerPage'] as num?)?.toInt(),
    );

Map<String, dynamic> _$AlertModelToJson(AlertModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
      'totalBadge': instance.totalBadge,
      'totalItems': instance.totalItems,
      'currentPageNo': instance.currentPageNo,
      'limitPerPage': instance.limitPerPage,
    };

AlertDetails _$AlertDetailsFromJson(Map<String, dynamic> json) => AlertDetails(
      notificationID: (json['notificationID'] as num?)?.toInt(),
      projectID: (json['projectID'] as num?)?.toInt(),
      affectedModuleID: json['affectedModuleID'] as String?,
      moduleName: json['moduleName'] as String?,
      isRead: json['isRead'] as String?,
      description: json['description'] as String?,
      createdDateTime: json['createdDateTime'] as String?,
      updatedDateTime: json['updatedDateTime'] as String?,
      assignedByTeamMemberID: (json['assignedByTeamMemberID'] as num?)?.toInt(),
      assignedByTeamMemberName: json['assignedByTeamMemberName'] as String?,
      teamMemberAvatar: json['teamMemberAvatar'] as String?,
      replyToID: (json['replyToID'] as num?)?.toInt(),
      taskIDs: json['taskIDs'] as List<dynamic>?,
      delayTaskIDs: json['delayTaskIDs'] as String?,
      delayDuration: (json['delayDuration'] as num?)?.toInt(),
      allNotificationTxt: json['allNotificationTxt'] as String?,
    );

Map<String, dynamic> _$AlertDetailsToJson(AlertDetails instance) =>
    <String, dynamic>{
      'notificationID': instance.notificationID,
      'projectID': instance.projectID,
      'affectedModuleID': instance.affectedModuleID,
      'moduleName': instance.moduleName,
      'isRead': instance.isRead,
      'description': instance.description,
      'createdDateTime': instance.createdDateTime,
      'updatedDateTime': instance.updatedDateTime,
      'assignedByTeamMemberID': instance.assignedByTeamMemberID,
      'assignedByTeamMemberName': instance.assignedByTeamMemberName,
      'teamMemberAvatar': instance.teamMemberAvatar,
      'replyToID': instance.replyToID,
      'taskIDs': instance.taskIDs,
      'delayTaskIDs': instance.delayTaskIDs,
      'delayDuration': instance.delayDuration,
      'allNotificationTxt': instance.allNotificationTxt,
    };
