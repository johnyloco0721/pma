// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_details_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskDetailsResponseModel _$TaskDetailsResponseModelFromJson(
        Map<String, dynamic> json) =>
    TaskDetailsResponseModel(
      permissions: json['permissions'] == null
          ? null
          : PermissionsModel.fromJson(
              json['permissions'] as Map<String, dynamic>),
      requirements: json['requirements'] == null
          ? null
          : Requirements.fromJson(json['requirements'] as Map<String, dynamic>),
      clientAppRequirements: json['clientAppRequirements'] == null
          ? null
          : ClientAppRequirements.fromJson(
              json['clientAppRequirements'] as Map<String, dynamic>),
      extraDatesDisplayArr: json['extraDatesDisplayArr'] == null
          ? null
          : ExtraDatesDisplayArr.fromJson(
              json['extraDatesDisplayArr'] as Map<String, dynamic>),
      taskArr: json['taskArr'] == null
          ? null
          : TaskDetailsModel.fromJson(json['taskArr'] as Map<String, dynamic>),
      subTaskArr: (json['subTaskArr'] as List<dynamic>?)
          ?.map((e) => SubtaskDetailsModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      paymentPercentageArr: (json['paymentPercentageArr'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    )
      ..assigneeList = (json['assigneeList'] as List<dynamic>?)
          ?.map((e) => TeamMemberDetails.fromJson(e as Map<String, dynamic>))
          .toList()
      ..milestoneList = (json['milestoneList'] as List<dynamic>?)
          ?.map((e) => TaskMilestoneOption.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$TaskDetailsResponseModelToJson(
        TaskDetailsResponseModel instance) =>
    <String, dynamic>{
      'permissions': instance.permissions,
      'requirements': instance.requirements,
      'clientAppRequirements': instance.clientAppRequirements,
      'extraDatesDisplayArr': instance.extraDatesDisplayArr,
      'taskArr': instance.taskArr,
      'subTaskArr': instance.subTaskArr,
      'assigneeList': instance.assigneeList,
      'milestoneList': instance.milestoneList,
      'paymentPercentageArr': instance.paymentPercentageArr,
    };

TaskDetailsModel _$TaskDetailsModelFromJson(Map<String, dynamic> json) =>
    TaskDetailsModel(
      taskID: (json['taskID'] as num?)?.toInt(),
      taskName: json['taskName'] as String?,
      taskDescription: json['taskDescription'] as String?,
      templateTaskID: (json['templateTaskID'] as num?)?.toInt(),
      isSpecial: json['isSpecial'] as bool?,
      specialTaskType: json['specialTaskType'] as String?,
      taskRelatedDate: json['taskRelatedDate'] as String?,
      taskStatus: json['taskStatus'] as String?,
      projectID: (json['projectID'] as num?)?.toInt(),
      parentTaskID: (json['parentTaskID'] as num?)?.toInt(),
      parentTaskName: json['parentTaskName'] as String?,
      isCompleted: (json['isCompleted'] as num?)?.toInt(),
      propertyName: json['propertyName'] as String?,
      teamMemberIDs: json['teamMemberIDs'] as String?,
      teamMemberName: json['teamMemberName'] as String?,
      teamMemberContactNo: json['teamMemberContactNo'] as String?,
      teamMemberAvatar: json['teamMemberAvatar'] as String?,
      createdBy: (json['createdBy'] as num?)?.toInt(),
      createdByName: json['createdByName'] as String?,
      createdByContactNo: json['createdByContactNo'] as String?,
      ownerName: json['ownerName'] as String?,
      milestoneCategoryID: (json['milestoneCategoryID'] as num?)?.toInt(),
      milestoneCategoryName: json['milestoneCategoryName'] as String?,
      isFileRequired: (json['isFileRequired'] as num?)?.toInt(),
      taskStartDateTime: json['taskStartDateTime'] as String?,
      taskTargetDateTime: json['taskTargetDateTime'] as String?,
      taskEndDateTime: json['taskEndDateTime'] as String?,
      completedDateTime: json['completedDateTime'] as String?,
      replyCount: (json['replyCount'] as num?)?.toInt(),
      createdDateTime: json['createdDateTime'] as String?,
      updatedDateTime: json['updatedDateTime'] as String?,
      clientAppAutoUpdate: json['clientAppAutoUpdate'] as String?,
      clientAppPhotoAutoUpdate: json['clientAppPhotoAutoUpdate'] as String?,
      isFlag: (json['isFlag'] as num?)?.toInt(),
      flagReason: json['flagReason'] as String?,
      isResolved: json['isResolved'] as String?,
      isRead: json['isRead'] as String?,
      isPushCancelled: json['isPushCancelled'] as bool?,
      updatesPushDateTime: json['updatesPushDateTime'] as String?,
      postUpdateCreatedDateTime: json['postUpdateCreatedDateTime'] as String?,
      isPushedToClientApp: json['isPushedToClientApp'] as String?,
      teamMemberID: (json['teamMemberID'] as num?)?.toInt(),
      assignTo: (json['assignTo'] as num?)?.toInt(),
      assignToName: json['assignToName'] as String?,
      assignToContactNumber: json['assignToContactNumber'] as String?,
      flagRemark: json['flagRemark'] as String?,
      taskUpdates: json['taskUpdates'] as String?,
      taskDisclaimer: json['taskDisclaimer'] as String?,
      taskDisclaimerBgColor: json['taskDisclaimerBgColor'] as String?,
      taskDisclaimerBorderColor: json['taskDisclaimerBorderColor'] as String?,
      hideCompletedButton: json['hideCompletedButton'] as bool?,
      stage: json['stage'] == null
          ? null
          : StageModel.fromJson(json['stage'] as Map<String, dynamic>),
      isFileRequiredLabel: json['isFileRequiredLabel'] as String?,
      isTaskTitleEditable: (json['isTaskTitleEditable'] as num?)?.toInt(),
      isAddSubTaskAllowed: (json['isAddSubTaskAllowed'] as num?)?.toInt(),
      isRemoveTaskAllowed: (json['isRemoveTaskAllowed'] as num?)?.toInt(),
      isAllowedToRepeatSpecialTask:
          (json['isAllowedToRepeatSpecialTask'] as num?)?.toInt(),
      isMilestoneEditable: (json['isMilestoneEditable'] as num?)?.toInt(),
      isAssigneeEditable: (json['isAssigneeEditable'] as num?)?.toInt(),
      isRequirementEditable: (json['isRequirementEditable'] as num?)?.toInt(),
      isClientAppUpdateEditable:
          (json['isClientAppUpdateEditable'] as num?)?.toInt(),
      isClientAppPhotoEditable:
          (json['isClientAppPhotoEditable'] as num?)?.toInt(),
      isTaskDescriptionEditable:
          (json['isTaskDescriptionEditable'] as num?)?.toInt(),
      isRaiseInvoiceButtonEditable:
          json['isRaiseInvoiceButtonEditable'] as bool?,
      taskPercentage: json['taskPercentage'] as String?,
      paymentTaskName: json['paymentTaskName'] as String?,
    )
      ..forClient = json['forClient'] == null
          ? null
          : ForClient.fromJson(json['forClient'] as Map<String, dynamic>)
      ..forInternal = json['forInternal'] == null
          ? null
          : ForInternal.fromJson(json['forInternal'] as Map<String, dynamic>);

Map<String, dynamic> _$TaskDetailsModelToJson(TaskDetailsModel instance) =>
    <String, dynamic>{
      'taskID': instance.taskID,
      'taskName': instance.taskName,
      'taskDescription': instance.taskDescription,
      'templateTaskID': instance.templateTaskID,
      'isSpecial': instance.isSpecial,
      'specialTaskType': instance.specialTaskType,
      'taskRelatedDate': instance.taskRelatedDate,
      'taskStatus': instance.taskStatus,
      'projectID': instance.projectID,
      'parentTaskID': instance.parentTaskID,
      'parentTaskName': instance.parentTaskName,
      'isCompleted': instance.isCompleted,
      'propertyName': instance.propertyName,
      'teamMemberIDs': instance.teamMemberIDs,
      'teamMemberName': instance.teamMemberName,
      'teamMemberContactNo': instance.teamMemberContactNo,
      'teamMemberAvatar': instance.teamMemberAvatar,
      'createdBy': instance.createdBy,
      'createdByName': instance.createdByName,
      'createdByContactNo': instance.createdByContactNo,
      'ownerName': instance.ownerName,
      'milestoneCategoryID': instance.milestoneCategoryID,
      'milestoneCategoryName': instance.milestoneCategoryName,
      'isFileRequired': instance.isFileRequired,
      'taskStartDateTime': instance.taskStartDateTime,
      'taskTargetDateTime': instance.taskTargetDateTime,
      'taskEndDateTime': instance.taskEndDateTime,
      'completedDateTime': instance.completedDateTime,
      'createdDateTime': instance.createdDateTime,
      'updatedDateTime': instance.updatedDateTime,
      'clientAppAutoUpdate': instance.clientAppAutoUpdate,
      'clientAppPhotoAutoUpdate': instance.clientAppPhotoAutoUpdate,
      'isFlag': instance.isFlag,
      'flagReason': instance.flagReason,
      'isResolved': instance.isResolved,
      'isRead': instance.isRead,
      'isPushCancelled': instance.isPushCancelled,
      'updatesPushDateTime': instance.updatesPushDateTime,
      'postUpdateCreatedDateTime': instance.postUpdateCreatedDateTime,
      'isPushedToClientApp': instance.isPushedToClientApp,
      'teamMemberID': instance.teamMemberID,
      'assignTo': instance.assignTo,
      'assignToName': instance.assignToName,
      'assignToContactNumber': instance.assignToContactNumber,
      'flagRemark': instance.flagRemark,
      'taskUpdates': instance.taskUpdates,
      'taskDisclaimer': instance.taskDisclaimer,
      'taskDisclaimerBgColor': instance.taskDisclaimerBgColor,
      'taskDisclaimerBorderColor': instance.taskDisclaimerBorderColor,
      'hideCompletedButton': instance.hideCompletedButton,
      'replyCount': instance.replyCount,
      'stage': instance.stage,
      'isFileRequiredLabel': instance.isFileRequiredLabel,
      'isTaskTitleEditable': instance.isTaskTitleEditable,
      'isAddSubTaskAllowed': instance.isAddSubTaskAllowed,
      'isRemoveTaskAllowed': instance.isRemoveTaskAllowed,
      'isAllowedToRepeatSpecialTask': instance.isAllowedToRepeatSpecialTask,
      'isMilestoneEditable': instance.isMilestoneEditable,
      'isAssigneeEditable': instance.isAssigneeEditable,
      'isRequirementEditable': instance.isRequirementEditable,
      'isClientAppUpdateEditable': instance.isClientAppUpdateEditable,
      'isClientAppPhotoEditable': instance.isClientAppPhotoEditable,
      'isTaskDescriptionEditable': instance.isTaskDescriptionEditable,
      'isRaiseInvoiceButtonEditable': instance.isRaiseInvoiceButtonEditable,
      'taskPercentage': instance.taskPercentage,
      'paymentTaskName': instance.paymentTaskName,
      'forClient': instance.forClient,
      'forInternal': instance.forInternal,
    };

SubtaskDetailsModel _$SubtaskDetailsModelFromJson(Map<String, dynamic> json) =>
    SubtaskDetailsModel(
      taskID: (json['taskID'] as num?)?.toInt(),
      parentTaskID: (json['parentTaskID'] as num?)?.toInt(),
      taskName: json['taskName'] as String?,
      taskDescription: json['taskDescription'] as String?,
      taskStatus: json['taskStatus'] as String?,
      specialTaskType: json['specialTaskType'] as String?,
      taskRelatedDate: json['taskRelatedDate'] as String?,
      isCompleted: (json['isCompleted'] as num?)?.toInt(),
      isFileRequired: (json['isFileRequired'] as num?)?.toInt(),
      taskStartDateTime: json['taskStartDateTime'] as String?,
      taskTargetDateTime: json['taskTargetDateTime'] as String?,
      taskEndDateTime: json['taskEndDateTime'] as String?,
      taskUpdateID: (json['taskUpdateID'] as num?)?.toInt(),
      createdDateTime: json['createdDateTime'] as String?,
      updatedDateTime: json['updatedDateTime'] as String?,
      replyCount: (json['replyCount'] as num?)?.toInt(),
      isPushCancelled: json['isPushCancelled'] as bool?,
      isPushedToClientApp: json['isPushedToClientApp'] as String?,
      updatesPushDateTime: json['updatesPushDateTime'] as String?,
      isRead: json['isRead'] as String?,
      flagRemark: json['flagRemark'] as String?,
      isFileRequiredLabel: json['isFileRequiredLabel'] as String?,
      isTaskTitleEditable: (json['isTaskTitleEditable'] as num?)?.toInt(),
      isAddSubTaskAllowed: (json['isAddSubTaskAllowed'] as num?)?.toInt(),
      isRemoveTaskAllowed: (json['isRemoveTaskAllowed'] as num?)?.toInt(),
      isAllowedToRepeatSpecialTask:
          (json['isAllowedToRepeatSpecialTask'] as num?)?.toInt(),
      isMilestoneEditable: (json['isMilestoneEditable'] as num?)?.toInt(),
      isAssigneeEditable: (json['isAssigneeEditable'] as num?)?.toInt(),
      isRequirementEditable: (json['isRequirementEditable'] as num?)?.toInt(),
      isClientAppUpdateEditable:
          (json['isClientAppUpdateEditable'] as num?)?.toInt(),
      isClientAppPhotoEditable:
          (json['isClientAppPhotoEditable'] as num?)?.toInt(),
      clientAppAutoUpdate: json['clientAppAutoUpdate'] as String?,
      clientAppPhotoAutoUpdate: json['clientAppPhotoAutoUpdate'] as String?,
      isSpecial: json['isSpecial'] as bool?,
      requirements: json['requirements'] == null
          ? null
          : Requirements.fromJson(json['requirements'] as Map<String, dynamic>),
      clientAppRequirements: json['clientAppRequirements'] == null
          ? null
          : ClientAppRequirements.fromJson(
              json['clientAppRequirements'] as Map<String, dynamic>),
      isFlag: (json['isFlag'] as num?)?.toInt(),
      templateTaskID: (json['templateTaskID'] as num?)?.toInt(),
    )
      ..forClient = json['forClient'] == null
          ? null
          : ForClient.fromJson(json['forClient'] as Map<String, dynamic>)
      ..forInternal = json['forInternal'] == null
          ? null
          : ForInternal.fromJson(json['forInternal'] as Map<String, dynamic>);

Map<String, dynamic> _$SubtaskDetailsModelToJson(
        SubtaskDetailsModel instance) =>
    <String, dynamic>{
      'taskID': instance.taskID,
      'parentTaskID': instance.parentTaskID,
      'taskName': instance.taskName,
      'taskDescription': instance.taskDescription,
      'taskStatus': instance.taskStatus,
      'specialTaskType': instance.specialTaskType,
      'taskRelatedDate': instance.taskRelatedDate,
      'isCompleted': instance.isCompleted,
      'isFileRequired': instance.isFileRequired,
      'taskStartDateTime': instance.taskStartDateTime,
      'taskTargetDateTime': instance.taskTargetDateTime,
      'taskEndDateTime': instance.taskEndDateTime,
      'taskUpdateID': instance.taskUpdateID,
      'createdDateTime': instance.createdDateTime,
      'updatedDateTime': instance.updatedDateTime,
      'replyCount': instance.replyCount,
      'isPushCancelled': instance.isPushCancelled,
      'isPushedToClientApp': instance.isPushedToClientApp,
      'updatesPushDateTime': instance.updatesPushDateTime,
      'isRead': instance.isRead,
      'flagRemark': instance.flagRemark,
      'isFileRequiredLabel': instance.isFileRequiredLabel,
      'isTaskTitleEditable': instance.isTaskTitleEditable,
      'isAddSubTaskAllowed': instance.isAddSubTaskAllowed,
      'isRemoveTaskAllowed': instance.isRemoveTaskAllowed,
      'isAllowedToRepeatSpecialTask': instance.isAllowedToRepeatSpecialTask,
      'isMilestoneEditable': instance.isMilestoneEditable,
      'isAssigneeEditable': instance.isAssigneeEditable,
      'isRequirementEditable': instance.isRequirementEditable,
      'isClientAppUpdateEditable': instance.isClientAppUpdateEditable,
      'isClientAppPhotoEditable': instance.isClientAppPhotoEditable,
      'clientAppAutoUpdate': instance.clientAppAutoUpdate,
      'clientAppPhotoAutoUpdate': instance.clientAppPhotoAutoUpdate,
      'isSpecial': instance.isSpecial,
      'requirements': instance.requirements,
      'clientAppRequirements': instance.clientAppRequirements,
      'forClient': instance.forClient,
      'forInternal': instance.forInternal,
      'isFlag': instance.isFlag,
      'templateTaskID': instance.templateTaskID,
    };

ForClient _$ForClientFromJson(Map<String, dynamic> json) => ForClient(
      taskUpdates: json['taskUpdates'] as String?,
      taskCompletedBy: json['taskCompletedBy'] as String?,
      isPaymentDueDateEditable:
          (json['isPaymentDueDateEditable'] as num?)?.toInt(),
      updatesImages: (json['updatesImages'] as List<dynamic>?)
          ?.map((e) => AttachModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      updatesFiles: (json['updatesFiles'] as List<dynamic>?)
          ?.map((e) => AttachModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      extraDatesDisplayArr: json['extraDatesDisplayArr'] == null
          ? null
          : ExtraDatesDisplayArr.fromJson(
              json['extraDatesDisplayArr'] as Map<String, dynamic>),
      dropboxLink: json['dropboxLink'] as String?,
    );

Map<String, dynamic> _$ForClientToJson(ForClient instance) => <String, dynamic>{
      'taskUpdates': instance.taskUpdates,
      'taskCompletedBy': instance.taskCompletedBy,
      'isPaymentDueDateEditable': instance.isPaymentDueDateEditable,
      'updatesImages': instance.updatesImages,
      'updatesFiles': instance.updatesFiles,
      'extraDatesDisplayArr': instance.extraDatesDisplayArr,
      'dropboxLink': instance.dropboxLink,
    };

ForInternal _$ForInternalFromJson(Map<String, dynamic> json) => ForInternal(
      taskUpdates: json['taskUpdates'] as String?,
      taskCompletedBy: json['taskCompletedBy'] as String?,
      isPaymentDueDateEditable:
          (json['isPaymentDueDateEditable'] as num?)?.toInt(),
      updatesImages: (json['updatesImages'] as List<dynamic>?)
          ?.map((e) => AttachModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      updatesFiles: (json['updatesFiles'] as List<dynamic>?)
          ?.map((e) => AttachModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      extraDatesDisplayArr: json['extraDatesDisplayArr'] == null
          ? null
          : ExtraDatesDisplayArr.fromJson(
              json['extraDatesDisplayArr'] as Map<String, dynamic>),
      dropboxLink: json['dropboxLink'] as String?,
    );

Map<String, dynamic> _$ForInternalToJson(ForInternal instance) =>
    <String, dynamic>{
      'taskUpdates': instance.taskUpdates,
      'taskCompletedBy': instance.taskCompletedBy,
      'isPaymentDueDateEditable': instance.isPaymentDueDateEditable,
      'updatesImages': instance.updatesImages,
      'updatesFiles': instance.updatesFiles,
      'extraDatesDisplayArr': instance.extraDatesDisplayArr,
      'dropboxLink': instance.dropboxLink,
    };
