import 'package:json_annotation/json_annotation.dart';

part 'alert_model.g.dart';

@JsonSerializable()
class AlertModel {
  bool? status;
  String? message;
  List<AlertDetails>? response;
  final int? totalBadge;
  final int? totalItems;
  final int? currentPageNo;
  final int? limitPerPage;


  AlertModel({this.status, this.message, this.response, this.totalBadge, 
              this.totalItems, this.currentPageNo, this.limitPerPage});

  factory AlertModel.fromJson(Map<String, dynamic> json) => _$AlertModelFromJson(json);

  Map<String, dynamic> toJson() => _$AlertModelToJson(this);
}

@JsonSerializable()
class AlertDetails {
  int? notificationID;
  int? projectID;
  String? affectedModuleID;
  String? moduleName;
  String? isRead;
  String? description;
  String? createdDateTime;
  String? updatedDateTime;
  int? assignedByTeamMemberID;
  String? assignedByTeamMemberName;
  String? teamMemberAvatar;
  int? replyToID;
  List? taskIDs;
  String? delayTaskIDs;
  int? delayDuration;
  String? allNotificationTxt;

  AlertDetails({
    this.notificationID,
    this.projectID,
    this.affectedModuleID,
    this.moduleName,
    this.isRead,
    this.description,
    this.createdDateTime,
    this.updatedDateTime,
    this.assignedByTeamMemberID,
    this.assignedByTeamMemberName,
    this.teamMemberAvatar,
    this.replyToID,
    this.taskIDs,
    this.delayTaskIDs,
    this.delayDuration,
    this.allNotificationTxt,
  });

  factory AlertDetails.fromJson(Map<String, dynamic> json) => _$AlertDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$AlertDetailsToJson(this);
}