import 'package:json_annotation/json_annotation.dart';
import 'package:sources/models/feed_item_model.dart';
import 'package:sources/models/task_details_model.dart';
part 'comment_details_model.g.dart';

@JsonSerializable()
class CommentDetailsModel {
  bool? status;
  String? message;
  CommentDetailsResponse response;
  int? totalItems;
  int? currentPageNo;
  int? limitPerPage;

  CommentDetailsModel({this.status, this.message, required this.response, this.currentPageNo, this.limitPerPage, this.totalItems});

  factory CommentDetailsModel.fromJson(Map<String, dynamic> json) => _$CommentDetailsModelFromJson(json);

  Map<String, dynamic> toJson() => _$CommentDetailsModelToJson(this);
}

@JsonSerializable()
class CommentDetailsResponse {
  TaskDetailsModel? taskDetails;
  List<FeedItemModel>? commentList;
  CommentDetailsResponse({required this.taskDetails, required this.commentList});

  factory CommentDetailsResponse.fromJson(Map<String, dynamic> json) => _$CommentDetailsResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CommentDetailsResponseToJson(this);
}
