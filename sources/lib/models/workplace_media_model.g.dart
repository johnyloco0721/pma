// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'workplace_media_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WorkplaceMediaModel _$WorkplaceMediaModelFromJson(Map<String, dynamic> json) =>
    WorkplaceMediaModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: json['response'] == null
          ? null
          : WorkplaceMediaList.fromJson(
              json['response'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$WorkplaceMediaModelToJson(
        WorkplaceMediaModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
    };

WorkplaceMediaList _$WorkplaceMediaListFromJson(Map<String, dynamic> json) =>
    WorkplaceMediaList(
      media: (json['media'] as List<dynamic>?)
          ?.map(
              (e) => WorkplaceMediaDetails.fromJson(e as Map<String, dynamic>))
          .toList(),
      document: (json['document'] as List<dynamic>?)
          ?.map(
              (e) => WorkplaceMediaDetails.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$WorkplaceMediaListToJson(WorkplaceMediaList instance) =>
    <String, dynamic>{
      'media': instance.media,
      'document': instance.document,
    };

WorkplaceMediaDetails _$WorkplaceMediaDetailsFromJson(
        Map<String, dynamic> json) =>
    WorkplaceMediaDetails(
      mediaID: (json['mediaID'] as num?)?.toInt(),
      projectFeedsMediaName: json['projectFeedsMediaName'] as String?,
      projectFeedsMedia: json['projectFeedsMedia'] as String?,
      projectFeedsMediaType: json['projectFeedsMediaType'] as String?,
      createdDateTime: json['createdDateTime'] as String?,
      caption: json['caption'] as String?,
      module: json['module'] as String?,
    );

Map<String, dynamic> _$WorkplaceMediaDetailsToJson(
        WorkplaceMediaDetails instance) =>
    <String, dynamic>{
      'mediaID': instance.mediaID,
      'projectFeedsMediaName': instance.projectFeedsMediaName,
      'projectFeedsMedia': instance.projectFeedsMedia,
      'projectFeedsMediaType': instance.projectFeedsMediaType,
      'createdDateTime': instance.createdDateTime,
      'caption': instance.caption,
      'module': instance.module,
    };
