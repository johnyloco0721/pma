// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'team_member_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TeamMemberListModel _$TeamMemberListModelFromJson(Map<String, dynamic> json) =>
    TeamMemberListModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: (json['response'] as List<dynamic>?)
          ?.map((e) => TeamMemberDetails.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$TeamMemberListModelToJson(
        TeamMemberListModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
    };
