// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'alert_post_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AlertPostModel _$AlertPostModelFromJson(Map<String, dynamic> json) =>
    AlertPostModel(
      json['notificationID'] as String?,
      json['notificationIDs'] as String?,
    );

Map<String, dynamic> _$AlertPostModelToJson(AlertPostModel instance) =>
    <String, dynamic>{
      'notificationID': instance.notificationID,
      'notificationIDs': instance.notificationIDs,
    };
