// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feed_item_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedItemModel _$FeedItemModelFromJson(Map<String, dynamic> json) =>
    FeedItemModel(
      uuid: json['uuid'] as String?,
      feedID: (json['feedID'] as num?)?.toInt(),
      projectID: (json['projectID'] as num?)?.toInt(),
      description: json['description'] as String?,
      feedType: json['feedType'] as String?,
      postDateTime: json['postDateTime'] as String?,
      updatedDateTime: json['updatedDateTime'] as String?,
      topicID: (json['topicID'] as num?)?.toInt(),
      topicName: json['topicName'] as String?,
      topicIcon: json['topicIcon'] as String?,
      topicColour: json['topicColour'] as String?,
      teamMemberID: (json['teamMemberID'] as num?)?.toInt(),
      teamMemberName: json['teamMemberName'] as String?,
      teamMemberAvatar: json['teamMemberAvatar'] as String?,
      designationName: json['designationName'] as String?,
      moduleID: json['moduleID'] as String?,
      moduleName: json['moduleName'] as String?,
      replyTeamMemberAvatar: (json['replyTeamMemberAvatar'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      replyCount: (json['replyCount'] as num?)?.toInt(),
      replyReadStatus: json['replyReadStatus'] as String?,
      backgroundColor: json['backgroundColor'] as String?,
      outlineColor: json['outlineColor'] as String?,
      fontNameColor: json['fontNameColor'] as String?,
      fontPositionColor: json['fontPositionColor'] as String?,
      fontDescriptionColor: json['fontDescriptionColor'] as String?,
      fontDateColor: json['fontDateColor'] as String?,
      mediaList: (json['mediaList'] as List<dynamic>?)
          ?.map((e) => MediaItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      updatesInfo: json['updatesInfo'] == null
          ? null
          : UpdatesInfoModel.fromJson(
              json['updatesInfo'] as Map<String, dynamic>),
      taskRelatedDate: json['taskRelatedDate'] as String?,
      projectFeedsID: (json['projectFeedsID'] as num?)?.toInt(),
      isRead: json['isRead'] as String?,
      invoiceDate: json['invoiceDate'] as String?,
      extraDatesDisplayArr: json['extraDatesDisplayArr'] == null
          ? null
          : ExtraDatesDisplayArr.fromJson(
              json['extraDatesDisplayArr'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$FeedItemModelToJson(FeedItemModel instance) =>
    <String, dynamic>{
      'uuid': instance.uuid,
      'feedID': instance.feedID,
      'projectID': instance.projectID,
      'description': instance.description,
      'feedType': instance.feedType,
      'postDateTime': instance.postDateTime,
      'updatedDateTime': instance.updatedDateTime,
      'topicID': instance.topicID,
      'topicName': instance.topicName,
      'topicIcon': instance.topicIcon,
      'topicColour': instance.topicColour,
      'teamMemberID': instance.teamMemberID,
      'teamMemberName': instance.teamMemberName,
      'teamMemberAvatar': instance.teamMemberAvatar,
      'designationName': instance.designationName,
      'moduleID': instance.moduleID,
      'moduleName': instance.moduleName,
      'replyTeamMemberAvatar': instance.replyTeamMemberAvatar,
      'replyCount': instance.replyCount,
      'replyReadStatus': instance.replyReadStatus,
      'backgroundColor': instance.backgroundColor,
      'outlineColor': instance.outlineColor,
      'fontNameColor': instance.fontNameColor,
      'fontPositionColor': instance.fontPositionColor,
      'fontDescriptionColor': instance.fontDescriptionColor,
      'fontDateColor': instance.fontDateColor,
      'mediaList': instance.mediaList,
      'updatesInfo': instance.updatesInfo,
      'taskRelatedDate': instance.taskRelatedDate,
      'projectFeedsID': instance.projectFeedsID,
      'isRead': instance.isRead,
      'invoiceDate': instance.invoiceDate,
      'extraDatesDisplayArr': instance.extraDatesDisplayArr,
    };

UpdatesInfoModel _$UpdatesInfoModelFromJson(Map<String, dynamic> json) =>
    UpdatesInfoModel(
      taskID: (json['taskID'] as num?)?.toInt(),
      updatesLabel: json['updatesLabel'] as String?,
      updatesDescription: json['updatesDescription'] as String?,
      updatesImages: (json['updatesImages'] as List<dynamic>?)
          ?.map((e) => AttachModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      updatesFiles: (json['updatesFiles'] as List<dynamic>?)
          ?.map((e) => AttachModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$UpdatesInfoModelToJson(UpdatesInfoModel instance) =>
    <String, dynamic>{
      'taskID': instance.taskID,
      'updatesLabel': instance.updatesLabel,
      'updatesDescription': instance.updatesDescription,
      'updatesImages': instance.updatesImages,
      'updatesFiles': instance.updatesFiles,
    };

ExtraDatesDisplayArr _$ExtraDatesDisplayArrFromJson(
        Map<String, dynamic> json) =>
    ExtraDatesDisplayArr(
      label: json['label'] as String?,
      date: json['date'] as String?,
    );

Map<String, dynamic> _$ExtraDatesDisplayArrToJson(
        ExtraDatesDisplayArr instance) =>
    <String, dynamic>{
      'label': instance.label,
      'date': instance.date,
    };
