// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'team_member_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TeamMemberDetails _$TeamMemberDetailsFromJson(Map<String, dynamic> json) =>
    TeamMemberDetails(
      teamMemberID: (json['teamMemberID'] as num?)?.toInt(),
      teamMemberName: json['teamMemberName'] as String?,
      teamMemberAvatar: json['teamMemberAvatar'] as String?,
      teamMemberContactNo: json['teamMemberContactNo'] as String?,
      designationName: json['designationName'] as String?,
      designationInitial: json['designationInitial'] as String?,
    );

Map<String, dynamic> _$TeamMemberDetailsToJson(TeamMemberDetails instance) =>
    <String, dynamic>{
      'teamMemberID': instance.teamMemberID,
      'teamMemberName': instance.teamMemberName,
      'teamMemberAvatar': instance.teamMemberAvatar,
      'teamMemberContactNo': instance.teamMemberContactNo,
      'designationName': instance.designationName,
      'designationInitial': instance.designationInitial,
    };
