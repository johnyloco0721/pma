import 'package:json_annotation/json_annotation.dart';

part 'cost_estimation_model.g.dart';

@JsonSerializable()
class CostEstimationModel {
  bool? status;
  String? message;
  List<CostEstimationDetails>? response;
  final int? totalItems;
  final int? currentPageNo;
  final int? limitPerPage;


  CostEstimationModel({this.status, this.message, this.response, this.totalItems, 
              this.currentPageNo, this.limitPerPage});

  factory CostEstimationModel.fromJson(Map<String, dynamic> json) => _$CostEstimationModelFromJson(json);

  Map<String, dynamic> toJson() => _$CostEstimationModelToJson(this);
}

@JsonSerializable()
class CostEstimationDetails {
  int? costEstimationID;
  int? projectID;
  String? costEstimationName;
  String? costEstimationType;
  String? costEstimationTotal;
  String? status;
  int? totalCEQty;

  CostEstimationDetails({
    this.costEstimationID,
    this.projectID,
    this.costEstimationName,
    this.costEstimationType,
    this.costEstimationTotal,
    this.status,
    this.totalCEQty,
  });

  factory CostEstimationDetails.fromJson(Map<String, dynamic> json) => _$CostEstimationDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$CostEstimationDetailsToJson(this);
}