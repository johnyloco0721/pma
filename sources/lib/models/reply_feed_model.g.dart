// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reply_feed_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReplyFeedModel _$ReplyFeedModelFromJson(Map<String, dynamic> json) =>
    ReplyFeedModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: json['response'] == null
          ? null
          : ReplyFeedResponse.fromJson(
              json['response'] as Map<String, dynamic>),
      totalItems: (json['totalItems'] as num?)?.toInt(),
      currentPageNo: (json['currentPageNo'] as num?)?.toInt(),
      limitPerPage: (json['limitPerPage'] as num?)?.toInt(),
    );

Map<String, dynamic> _$ReplyFeedModelToJson(ReplyFeedModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
      'totalItems': instance.totalItems,
      'currentPageNo': instance.currentPageNo,
      'limitPerPage': instance.limitPerPage,
    };

ReplyFeedResponse _$ReplyFeedResponseFromJson(Map<String, dynamic> json) =>
    ReplyFeedResponse(
      lastReadFeedID: (json['lastReadFeedID'] as num?)?.toInt(),
      feedList: (json['feedList'] as List<dynamic>?)
          ?.map((e) => FeedItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ReplyFeedResponseToJson(ReplyFeedResponse instance) =>
    <String, dynamic>{
      'lastReadFeedID': instance.lastReadFeedID,
      'feedList': instance.feedList,
    };
