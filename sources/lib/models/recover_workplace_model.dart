import 'package:json_annotation/json_annotation.dart';

part 'recover_workplace_model.g.dart';

@JsonSerializable()
class RecoverWorkplaceModel {
  final int? projectID;
  final int? isArchived;


  RecoverWorkplaceModel({this.projectID, this.isArchived});

  factory RecoverWorkplaceModel.fromJson(Map<String, dynamic> json) => _$RecoverWorkplaceModelFromJson(json);

  Map<String, dynamic> toJson() => _$RecoverWorkplaceModelToJson(this);
}