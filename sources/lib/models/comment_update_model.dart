import 'package:json_annotation/json_annotation.dart';

part 'comment_update_model.g.dart';

@JsonSerializable()
class CommentUpdateModel {
  final int projectID;
  final int taskID;
  final int feedID;
  final String description;
  final List<int?> mediaID;

  CommentUpdateModel({
    required this.projectID,
    required this.taskID,
    required this.feedID,
    required this.mediaID,
    required this.description,
  });

  factory CommentUpdateModel.fromJson(Map<String, dynamic> json) => _$CommentUpdateModelFromJson(json);

  Map<String, dynamic> toJson() => _$CommentUpdateModelToJson(this);
}
