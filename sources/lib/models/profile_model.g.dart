// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProfileModel _$ProfileModelFromJson(Map<String, dynamic> json) => ProfileModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: json['response'] == null
          ? null
          : ProfileResponse.fromJson(json['response'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ProfileModelToJson(ProfileModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
    };

ProfileResponse _$ProfileResponseFromJson(Map<String, dynamic> json) =>
    ProfileResponse(
      profileDetails: json['profileDetails'] == null
          ? null
          : ProfileDetails.fromJson(
              json['profileDetails'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ProfileResponseToJson(ProfileResponse instance) =>
    <String, dynamic>{
      'profileDetails': instance.profileDetails,
    };

ProfileDetails _$ProfileDetailsFromJson(Map<String, dynamic> json) =>
    ProfileDetails(
      id: (json['id'] as num?)?.toInt(),
      username: json['username'] as String?,
      name: json['name'] as String?,
      address: json['address'] as String?,
      email: json['email'] as String?,
      gmail: json['gmail'] as String?,
      branch: json['branch'] as String?,
      designationName: json['designationName'] as String?,
      designationInitial: json['designationInitial'] as String?,
      avatar: json['avatar'] as String?,
      contactNo: json['contactNo'] as String?,
      hash: json['hash'] as String?,
      access: json['access'] as String?,
      teamMemberID: (json['teamMemberID'] as num?)?.toInt(),
    );

Map<String, dynamic> _$ProfileDetailsToJson(ProfileDetails instance) =>
    <String, dynamic>{
      'id': instance.id,
      'username': instance.username,
      'name': instance.name,
      'address': instance.address,
      'email': instance.email,
      'gmail': instance.gmail,
      'branch': instance.branch,
      'designationName': instance.designationName,
      'designationInitial': instance.designationInitial,
      'avatar': instance.avatar,
      'contactNo': instance.contactNo,
      'hash': instance.hash,
      'access': instance.access,
      'teamMemberID': instance.teamMemberID,
    };
