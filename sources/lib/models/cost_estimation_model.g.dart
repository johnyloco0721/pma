// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cost_estimation_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CostEstimationModel _$CostEstimationModelFromJson(Map<String, dynamic> json) =>
    CostEstimationModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: (json['response'] as List<dynamic>?)
          ?.map(
              (e) => CostEstimationDetails.fromJson(e as Map<String, dynamic>))
          .toList(),
      totalItems: (json['totalItems'] as num?)?.toInt(),
      currentPageNo: (json['currentPageNo'] as num?)?.toInt(),
      limitPerPage: (json['limitPerPage'] as num?)?.toInt(),
    );

Map<String, dynamic> _$CostEstimationModelToJson(
        CostEstimationModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
      'totalItems': instance.totalItems,
      'currentPageNo': instance.currentPageNo,
      'limitPerPage': instance.limitPerPage,
    };

CostEstimationDetails _$CostEstimationDetailsFromJson(
        Map<String, dynamic> json) =>
    CostEstimationDetails(
      costEstimationID: (json['costEstimationID'] as num?)?.toInt(),
      projectID: (json['projectID'] as num?)?.toInt(),
      costEstimationName: json['costEstimationName'] as String?,
      costEstimationType: json['costEstimationType'] as String?,
      costEstimationTotal: json['costEstimationTotal'] as String?,
      status: json['status'] as String?,
      totalCEQty: (json['totalCEQty'] as num?)?.toInt(),
    );

Map<String, dynamic> _$CostEstimationDetailsToJson(
        CostEstimationDetails instance) =>
    <String, dynamic>{
      'costEstimationID': instance.costEstimationID,
      'projectID': instance.projectID,
      'costEstimationName': instance.costEstimationName,
      'costEstimationType': instance.costEstimationType,
      'costEstimationTotal': instance.costEstimationTotal,
      'status': instance.status,
      'totalCEQty': instance.totalCEQty,
    };
