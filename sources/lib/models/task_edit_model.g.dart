// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_edit_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MyTaskEditModel _$MyTaskEditModelFromJson(Map<String, dynamic> json) =>
    MyTaskEditModel(
      taskID: (json['taskID'] as num?)?.toInt(),
      forClient: json['forClient'] == null
          ? null
          : ForClient.fromJson(json['forClient'] as Map<String, dynamic>),
      forInternal: json['forInternal'] == null
          ? null
          : ForInternal.fromJson(json['forInternal'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MyTaskEditModelToJson(MyTaskEditModel instance) =>
    <String, dynamic>{
      'taskID': instance.taskID,
      'forClient': instance.forClient,
      'forInternal': instance.forInternal,
    };

ForInternal _$ForInternalFromJson(Map<String, dynamic> json) => ForInternal(
      taskImageID: (json['taskImageID'] as List<dynamic>?)
          ?.map((e) => (e as num).toInt())
          .toList(),
      remark: json['remark'] as String?,
      dropboxLink: json['dropboxLink'] as String?,
      actualHandoverDate: json['actualHandoverDate'] as String?,
      invoiceDate: json['invoiceDate'] as String?,
      paymentDueDate: json['paymentDueDate'] as String?,
    );

Map<String, dynamic> _$ForInternalToJson(ForInternal instance) =>
    <String, dynamic>{
      'taskImageID': instance.taskImageID,
      'remark': instance.remark,
      'dropboxLink': instance.dropboxLink,
      'actualHandoverDate': instance.actualHandoverDate,
      'invoiceDate': instance.invoiceDate,
      'paymentDueDate': instance.paymentDueDate,
    };

ForClient _$ForClientFromJson(Map<String, dynamic> json) => ForClient(
      taskImageID: (json['taskImageID'] as List<dynamic>?)
          ?.map((e) => (e as num).toInt())
          .toList(),
      remark: json['remark'] as String?,
      dropboxLink: json['dropboxLink'] as String?,
      actualHandoverDate: json['actualHandoverDate'] as String?,
      invoiceDate: json['invoiceDate'] as String?,
      paymentDueDate: json['paymentDueDate'] as String?,
    );

Map<String, dynamic> _$ForClientToJson(ForClient instance) => <String, dynamic>{
      'taskImageID': instance.taskImageID,
      'remark': instance.remark,
      'dropboxLink': instance.dropboxLink,
      'actualHandoverDate': instance.actualHandoverDate,
      'invoiceDate': instance.invoiceDate,
      'paymentDueDate': instance.paymentDueDate,
    };
