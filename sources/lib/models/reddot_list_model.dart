import 'package:json_annotation/json_annotation.dart';

part 'reddot_list_model.g.dart';

@JsonSerializable()
class RedDotListModel {
  bool? status;
  String? message;
  RedDotListModelResponse? response;

  RedDotListModel({this.status, this.message, this.response});

  factory RedDotListModel.fromJson(Map<String, dynamic> json) => _$RedDotListModelFromJson(json);

  Map<String, dynamic> toJson() => _$RedDotListModelToJson(this);
}

@JsonSerializable()
class RedDotListModelResponse {
  List<String>? taskDateTime;
  RedDotListModelResponse({required this.taskDateTime});

  factory RedDotListModelResponse.fromJson(Map<String, dynamic> json) => _$RedDotListModelResponseFromJson(json);

  Map<String, dynamic> toJson() => _$RedDotListModelResponseToJson(this);
}