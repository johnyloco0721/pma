// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'push_update_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PushUpdateModel _$PushUpdateModelFromJson(Map<String, dynamic> json) =>
    PushUpdateModel(
      taskID: (json['taskID'] as num?)?.toInt(),
      action: json['action'] as String?,
      remark: json['remark'] as String?,
      taskImageID: json['taskImageID'] as List<dynamic>?,
    );

Map<String, dynamic> _$PushUpdateModelToJson(PushUpdateModel instance) =>
    <String, dynamic>{
      'taskID': instance.taskID,
      'action': instance.action,
      'remark': instance.remark,
      'taskImageID': instance.taskImageID,
    };
