import 'package:json_annotation/json_annotation.dart';

part 'alert_post_model.g.dart';

@JsonSerializable()
class AlertPostModel {
  final String? notificationID;
  final String? notificationIDs;

  AlertPostModel(this.notificationID, this.notificationIDs);

  factory AlertPostModel.fromJson(Map<String, dynamic> json) => _$AlertPostModelFromJson(json);

  Map<String, dynamic> toJson() => _$AlertPostModelToJson(this);
}