// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comment_post_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommentPostModel _$CommentPostModelFromJson(Map<String, dynamic> json) =>
    CommentPostModel(
      projectID: (json['projectID'] as num).toInt(),
      taskID: (json['taskID'] as num).toInt(),
      description: json['description'] as String,
      mediaID: (json['mediaID'] as List<dynamic>)
          .map((e) => (e as num).toInt())
          .toList(),
    );

Map<String, dynamic> _$CommentPostModelToJson(CommentPostModel instance) =>
    <String, dynamic>{
      'projectID': instance.projectID,
      'taskID': instance.taskID,
      'description': instance.description,
      'mediaID': instance.mediaID,
    };
