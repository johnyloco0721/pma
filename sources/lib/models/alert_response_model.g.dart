// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'alert_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AlertResponseModel _$AlertResponseModelFromJson(Map<String, dynamic> json) =>
    AlertResponseModel(
      taskID: (json['taskID'] as num?)?.toInt(),
      replyToID: (json['replyToID'] as num?)?.toInt(),
      projectFeedsID: (json['projectFeedsID'] as num?)?.toInt(),
      companyID: (json['companyID'] as num?)?.toInt(),
      taggabledIDArr: json['taggabledIDArr'] as List<dynamic>?,
    );

Map<String, dynamic> _$AlertResponseModelToJson(AlertResponseModel instance) =>
    <String, dynamic>{
      'taskID': instance.taskID,
      'replyToID': instance.replyToID,
      'projectFeedsID': instance.projectFeedsID,
      'companyID': instance.companyID,
      'taggabledIDArr': instance.taggabledIDArr,
    };
