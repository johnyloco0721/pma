import 'package:json_annotation/json_annotation.dart';

part 'team_member_model.g.dart';

@JsonSerializable()
class TeamMemberDetails {
  int? teamMemberID;
  String? teamMemberName;
  String? teamMemberAvatar;
  String? teamMemberContactNo;
  String? designationName;
  String? designationInitial;

  TeamMemberDetails({this.teamMemberID, this.teamMemberName, this.teamMemberAvatar, this.teamMemberContactNo, this.designationName, this.designationInitial});

  factory TeamMemberDetails.fromJson(Map<String, dynamic> json) => _$TeamMemberDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$TeamMemberDetailsToJson(this);
}
