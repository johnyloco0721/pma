import 'package:json_annotation/json_annotation.dart';
import 'package:sources/models/attach_model.dart';
import 'package:sources/models/media_item_model.dart';

part 'feed_item_model.g.dart';

@JsonSerializable()
class FeedItemModel {
  String? uuid;
  int? feedID;
  int? projectID;
  String? description;
  String? feedType;
  String? postDateTime;
  String? updatedDateTime;
  int? topicID;
  String? topicName;
  String? topicIcon;
  String? topicColour;
  int? teamMemberID;
  String? teamMemberName;
  String? teamMemberAvatar;
  String? designationName;
  String? moduleID;
  String? moduleName;
  List<String>? replyTeamMemberAvatar;
  int? replyCount;
  String? replyReadStatus;
  String? backgroundColor;
  String? outlineColor;
  String? fontNameColor;
  String? fontPositionColor;
  String? fontDescriptionColor;
  String? fontDateColor;
  List<MediaItemModel>? mediaList;
  UpdatesInfoModel? updatesInfo;
  String? taskRelatedDate;
  int? projectFeedsID;
  String? isRead;
  String? invoiceDate;
  ExtraDatesDisplayArr? extraDatesDisplayArr;

  FeedItemModel({this.uuid, this.feedID, this.projectID, this.description, this.feedType, this.postDateTime, this.updatedDateTime, this.topicID, this.topicName, this.topicIcon, this.topicColour, this.teamMemberID, this.teamMemberName, this.teamMemberAvatar, this.designationName, this.moduleID, this.moduleName, this.replyTeamMemberAvatar, this.replyCount, this.replyReadStatus, this.backgroundColor, this.outlineColor, this.fontNameColor, this.fontPositionColor, this.fontDescriptionColor, this.fontDateColor, this.mediaList, this.updatesInfo, this.taskRelatedDate, this.projectFeedsID, this.isRead, this.invoiceDate, this.extraDatesDisplayArr});

  factory FeedItemModel.fromJson(Map<String, dynamic> json) => _$FeedItemModelFromJson(json);

  Map<String, dynamic> toJson() => _$FeedItemModelToJson(this);
}

@JsonSerializable()
class UpdatesInfoModel {
  int? taskID;
  String? updatesLabel;
  String? updatesDescription;
  List<AttachModel>? updatesImages;
  List<AttachModel>? updatesFiles;

  UpdatesInfoModel({
    this.taskID, 
    this.updatesLabel, 
    this.updatesDescription, 
    this.updatesImages, 
    this.updatesFiles
  });

  factory UpdatesInfoModel.fromJson(Map<String, dynamic> json) => _$UpdatesInfoModelFromJson(json);

  Map<String, dynamic> toJson() => _$UpdatesInfoModelToJson(this);
}

@JsonSerializable()
class ExtraDatesDisplayArr {
  String? label;
  String? date;

  ExtraDatesDisplayArr({
    this.label,
    this.date,
  });

  factory ExtraDatesDisplayArr.fromJson(Map<String, dynamic> json) => _$ExtraDatesDisplayArrFromJson(json);

  Map<String, dynamic> toJson() => _$ExtraDatesDisplayArrToJson(this);
}