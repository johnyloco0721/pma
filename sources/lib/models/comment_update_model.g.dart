// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comment_update_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommentUpdateModel _$CommentUpdateModelFromJson(Map<String, dynamic> json) =>
    CommentUpdateModel(
      projectID: (json['projectID'] as num).toInt(),
      taskID: (json['taskID'] as num).toInt(),
      feedID: (json['feedID'] as num).toInt(),
      mediaID: (json['mediaID'] as List<dynamic>)
          .map((e) => (e as num?)?.toInt())
          .toList(),
      description: json['description'] as String,
    );

Map<String, dynamic> _$CommentUpdateModelToJson(CommentUpdateModel instance) =>
    <String, dynamic>{
      'projectID': instance.projectID,
      'taskID': instance.taskID,
      'feedID': instance.feedID,
      'description': instance.description,
      'mediaID': instance.mediaID,
    };
