// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_flagging_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskFlaggingModel _$TaskFlaggingModelFromJson(Map<String, dynamic> json) =>
    TaskFlaggingModel(
      taskID: (json['taskID'] as num?)?.toInt(),
      projectID: (json['projectID'] as num?)?.toInt(),
      reason: json['reason'] as String?,
    );

Map<String, dynamic> _$TaskFlaggingModelToJson(TaskFlaggingModel instance) =>
    <String, dynamic>{
      'taskID': instance.taskID,
      'projectID': instance.projectID,
      'reason': instance.reason,
    };
