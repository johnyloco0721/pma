import 'package:json_annotation/json_annotation.dart';

part 'profile_model.g.dart';

@JsonSerializable()
class ProfileModel {
  bool? status;
  String? message;
  ProfileResponse? response;

  ProfileModel({this.status, this.message, this.response});

  factory ProfileModel.fromJson(Map<String, dynamic> json) => _$ProfileModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProfileModelToJson(this);
}

@JsonSerializable()
class ProfileResponse {
  ProfileDetails? profileDetails;

  ProfileResponse({this.profileDetails});

  factory ProfileResponse.fromJson(Map<String, dynamic> json) => _$ProfileResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ProfileResponseToJson(this);
}

@JsonSerializable()
class ProfileDetails {
  int? id;
  String? username;
  String? name;
  String? address;
  String? email;
  String? gmail;
  String? branch;
  String? designationName;
  String? designationInitial;
  String? avatar;
  String? contactNo;
  String? hash;
  String? access;
  int? teamMemberID;

  ProfileDetails({
    this.id,
    this.username,
    this.name,
    this.address,
    this.email,
    this.gmail,
    this.branch,
    this.designationName,
    this.designationInitial,
    this.avatar,
    this.contactNo,
    this.hash,
    this.access,
    this.teamMemberID,
  });

  factory ProfileDetails.fromJson(Map<String, dynamic> json) =>
      _$ProfileDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$ProfileDetailsToJson(this);
}
