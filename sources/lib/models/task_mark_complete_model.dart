import 'package:json_annotation/json_annotation.dart';

part 'task_mark_complete_model.g.dart';

@JsonSerializable()
class TaskMarkCompleteModel {
  final int projectID;
  final int taskID;
  final List<int> taskImageID;
  final String? remark;
  final String? dropboxLink;
  final String? actualHandoverDate;
  final String? invoiceDate;

  TaskMarkCompleteModel({
    required this.projectID, 
    required this.taskID, 
    required this.taskImageID, 
    this.remark, 
    this.dropboxLink, 
    this.actualHandoverDate, 
    this.invoiceDate, 
  });

  factory TaskMarkCompleteModel.fromJson(Map<String, dynamic> json) => _$TaskMarkCompleteModelFromJson(json);

  Map<String, dynamic> toJson() => _$TaskMarkCompleteModelToJson(this);
}
