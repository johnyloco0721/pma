// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'special_task_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SpecialTaskModel _$SpecialTaskModelFromJson(Map<String, dynamic> json) =>
    SpecialTaskModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: (json['response'] as List<dynamic>?)
          ?.map((e) => SpecialTaskDetails.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$SpecialTaskModelToJson(SpecialTaskModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
    };

SpecialTaskDetails _$SpecialTaskDetailsFromJson(Map<String, dynamic> json) =>
    SpecialTaskDetails(
      templateTaskID: (json['templateTaskID'] as num?)?.toInt(),
      templateTaskName: json['templateTaskName'] as String?,
      templateTaskDescription: json['templateTaskDescription'] as String?,
      milestoneCategoryID: (json['milestoneCategoryID'] as num?)?.toInt(),
      specialTaskOrder: (json['specialTaskOrder'] as num?)?.toInt(),
      requirements: json['requirements'] == null
          ? null
          : Requirements.fromJson(json['requirements'] as Map<String, dynamic>),
      clientAppRequirements: json['clientAppRequirements'] == null
          ? null
          : ClientAppRequirements.fromJson(
              json['clientAppRequirements'] as Map<String, dynamic>),
      disabled: json['disabled'] as bool?,
      teamMemberID: (json['teamMemberID'] as num?)?.toInt(),
      enabledSubtask: json['enabledSubtask'] as bool?,
      defaultSubtask: (json['defaultSubtask'] as List<dynamic>?)
          ?.map(
              (e) => SpecialSubTaskDetails.fromJson(e as Map<String, dynamic>))
          .toList(),
      templateTaskPercentage: json['templateTaskPercentage'] as String?,
      taskPercentage: json['taskPercentage'] as String?,
    );

Map<String, dynamic> _$SpecialTaskDetailsToJson(SpecialTaskDetails instance) =>
    <String, dynamic>{
      'templateTaskID': instance.templateTaskID,
      'templateTaskName': instance.templateTaskName,
      'templateTaskDescription': instance.templateTaskDescription,
      'milestoneCategoryID': instance.milestoneCategoryID,
      'specialTaskOrder': instance.specialTaskOrder,
      'requirements': instance.requirements,
      'clientAppRequirements': instance.clientAppRequirements,
      'disabled': instance.disabled,
      'teamMemberID': instance.teamMemberID,
      'enabledSubtask': instance.enabledSubtask,
      'defaultSubtask': instance.defaultSubtask,
      'templateTaskPercentage': instance.templateTaskPercentage,
      'taskPercentage': instance.taskPercentage,
    };

SpecialSubTaskDetails _$SpecialSubTaskDetailsFromJson(
        Map<String, dynamic> json) =>
    SpecialSubTaskDetails(
      templateTaskID: (json['templateTaskID'] as num?)?.toInt(),
      templateTaskName: json['templateTaskName'] as String?,
      templateTaskDescription: json['templateTaskDescription'] as String?,
      milestoneCategoryID: (json['milestoneCategoryID'] as num?)?.toInt(),
      defaultDesignationID: (json['defaultDesignationID'] as num?)?.toInt(),
      isAddSubTaskAllowed: (json['isAddSubTaskAllowed'] as num?)?.toInt(),
      isRemoveTaskAllowed: (json['isRemoveTaskAllowed'] as num?)?.toInt(),
      isAllowedToRepeatSpecialTask:
          (json['isAllowedToRepeatSpecialTask'] as num?)?.toInt(),
      isMilestoneEditable: (json['isMilestoneEditable'] as num?)?.toInt(),
      isAssigneeEditable: (json['isAssigneeEditable'] as num?)?.toInt(),
      isRequirementEditable: (json['isRequirementEditable'] as num?)?.toInt(),
      requirements: json['requirements'] == null
          ? null
          : Requirements.fromJson(json['requirements'] as Map<String, dynamic>),
      clientAppRequirements: json['clientAppRequirements'] == null
          ? null
          : ClientAppRequirements.fromJson(
              json['clientAppRequirements'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$SpecialSubTaskDetailsToJson(
        SpecialSubTaskDetails instance) =>
    <String, dynamic>{
      'templateTaskID': instance.templateTaskID,
      'templateTaskName': instance.templateTaskName,
      'templateTaskDescription': instance.templateTaskDescription,
      'milestoneCategoryID': instance.milestoneCategoryID,
      'defaultDesignationID': instance.defaultDesignationID,
      'isAddSubTaskAllowed': instance.isAddSubTaskAllowed,
      'isRemoveTaskAllowed': instance.isRemoveTaskAllowed,
      'isAllowedToRepeatSpecialTask': instance.isAllowedToRepeatSpecialTask,
      'isMilestoneEditable': instance.isMilestoneEditable,
      'isAssigneeEditable': instance.isAssigneeEditable,
      'isRequirementEditable': instance.isRequirementEditable,
      'requirements': instance.requirements,
      'clientAppRequirements': instance.clientAppRequirements,
    };
