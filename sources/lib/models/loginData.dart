import 'package:json_annotation/json_annotation.dart';
part 'loginData.g.dart';

@JsonSerializable()
class LoginData {
  final String? username;
  final String? password;
  final String? token;

  LoginData(this.username, this.password, this.token);

  factory LoginData.fromJson(Map<String, dynamic> json) => _$LoginDataFromJson(json);

  Map<String, dynamic> toJson() => _$LoginDataToJson(this);
}