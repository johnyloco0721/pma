import 'package:json_annotation/json_annotation.dart';

part 'feed_topic_model.g.dart';

@JsonSerializable()
class FeedTopicModel {
  bool? status;
  String? message;
  FeedTopicResponse? response;

  FeedTopicModel({this.status, this.message, this.response});

  factory FeedTopicModel.fromJson(Map<String, dynamic> json) => _$FeedTopicModelFromJson(json);

  Map<String, dynamic> toJson() => _$FeedTopicModelToJson(this);
}

@JsonSerializable()
class FeedTopicResponse {
  List<FeedTopic>? topicList;

  FeedTopicResponse({this.topicList});

  factory FeedTopicResponse.fromJson(Map<String, dynamic> json) => _$FeedTopicResponseFromJson(json);

  Map<String, dynamic> toJson() => _$FeedTopicResponseToJson(this);
}

@JsonSerializable()
class FeedTopic {
  int? topicID;
  String? topicName;
  String? topicIcon;
  String? topicColour;

  FeedTopic({this.topicID, this.topicName, this.topicIcon, this.topicColour});

  factory FeedTopic.fromJson(Map<String, dynamic> json) => _$FeedTopicFromJson(json);

  Map<String, dynamic> toJson() => _$FeedTopicToJson(this);
}