// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_delay_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskDelayModel _$TaskDelayModelFromJson(Map<String, dynamic> json) =>
    TaskDelayModel(
      projectID: (json['projectID'] as num?)?.toInt(),
      taskIDs: json['taskIDs'] as List<dynamic>?,
      delayDuration: (json['delayDuration'] as num?)?.toInt(),
      taskList: json['taskList'] as List<dynamic>?,
    );

Map<String, dynamic> _$TaskDelayModelToJson(TaskDelayModel instance) =>
    <String, dynamic>{
      'projectID': instance.projectID,
      'taskIDs': instance.taskIDs,
      'delayDuration': instance.delayDuration,
      'taskList': instance.taskList,
    };
