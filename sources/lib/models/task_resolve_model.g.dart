// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_resolve_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskResolveModel _$TaskResolveModelFromJson(Map<String, dynamic> json) =>
    TaskResolveModel(
      taskID: (json['taskID'] as num?)?.toInt(),
      projectID: (json['projectID'] as num?)?.toInt(),
      mediaID: json['mediaID'] as List<dynamic>?,
      remarks: json['remarks'] as String?,
      dropboxLink: json['dropboxLink'] as String?,
      actualHandoverDate: json['actualHandoverDate'] as String?,
      invoiceDate: json['invoiceDate'] as String?,
    );

Map<String, dynamic> _$TaskResolveModelToJson(TaskResolveModel instance) =>
    <String, dynamic>{
      'taskID': instance.taskID,
      'projectID': instance.projectID,
      'mediaID': instance.mediaID,
      'remarks': instance.remarks,
      'dropboxLink': instance.dropboxLink,
      'actualHandoverDate': instance.actualHandoverDate,
      'invoiceDate': instance.invoiceDate,
    };
