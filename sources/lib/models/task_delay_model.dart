import 'package:json_annotation/json_annotation.dart';

part 'task_delay_model.g.dart';

@JsonSerializable()
class TaskDelayModel {
  int? projectID;
  List? taskIDs;
  int? delayDuration;
  List? taskList;

  TaskDelayModel({this.projectID,this.taskIDs, this.delayDuration,this.taskList});

  factory TaskDelayModel.fromJson(Map<String, dynamic> json) => _$TaskDelayModelFromJson(json);

  Map<String, dynamic> toJson() => _$TaskDelayModelToJson(this);
}
