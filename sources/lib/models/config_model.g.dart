// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'config_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConfigModel _$ConfigModelFromJson(Map<String, dynamic> json) => ConfigModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: json['response'] == null
          ? null
          : ConfigDetails.fromJson(json['response'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ConfigModelToJson(ConfigModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
    };

ConfigDetails _$ConfigDetailsFromJson(Map<String, dynamic> json) =>
    ConfigDetails(
      mainSiteURL: json['mainSiteURL'] as String?,
      PMASiteURL: json['PMASiteURL'] as String?,
      mobileSiteURL: json['mobileSiteURL'] as String?,
      mobileVersion: json['mobileVersion'] as String?,
      underMaintenance: json['underMaintenance'] as bool?,
      support: json['support'] == null
          ? null
          : Support.fromJson(json['support'] as Map<String, dynamic>),
      workplaceMediaUploadQuality:
          (json['workplaceMediaUploadQuality'] as num?)?.toInt(),
      taskMediaUploadQuality: (json['taskMediaUploadQuality'] as num?)?.toInt(),
      AndroidAppUpdateLink: json['AndroidAppUpdateLink'] as String?,
      iOSAppUpdateLink: json['iOSAppUpdateLink'] as String?,
    );

Map<String, dynamic> _$ConfigDetailsToJson(ConfigDetails instance) =>
    <String, dynamic>{
      'mainSiteURL': instance.mainSiteURL,
      'PMASiteURL': instance.PMASiteURL,
      'mobileSiteURL': instance.mobileSiteURL,
      'mobileVersion': instance.mobileVersion,
      'underMaintenance': instance.underMaintenance,
      'support': instance.support,
      'workplaceMediaUploadQuality': instance.workplaceMediaUploadQuality,
      'taskMediaUploadQuality': instance.taskMediaUploadQuality,
      'AndroidAppUpdateLink': instance.AndroidAppUpdateLink,
      'iOSAppUpdateLink': instance.iOSAppUpdateLink,
    };

Support _$SupportFromJson(Map<String, dynamic> json) => Support(
      topic: json['topic'] == null
          ? null
          : SupportTopic.fromJson(json['topic'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$SupportToJson(Support instance) => <String, dynamic>{
      'topic': instance.topic,
    };

SupportTopic _$SupportTopicFromJson(Map<String, dynamic> json) => SupportTopic(
      help: json['help'] as String?,
      feedback: json['feedback'] as String?,
      others: json['others'] as String?,
    );

Map<String, dynamic> _$SupportTopicToJson(SupportTopic instance) =>
    <String, dynamic>{
      'help': instance.help,
      'feedback': instance.feedback,
      'others': instance.others,
    };
