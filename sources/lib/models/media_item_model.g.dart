// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'media_item_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MediaItemModel _$MediaItemModelFromJson(Map<String, dynamic> json) =>
    MediaItemModel(
      mediaID: (json['mediaID'] as num?)?.toInt(),
      url: json['url'] as String?,
      fileExtension: json['fileExtension'] as String?,
      thumbnailPath: json['thumbnailPath'] as String?,
      datetime: json['datetime'] as String?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$MediaItemModelToJson(MediaItemModel instance) =>
    <String, dynamic>{
      'mediaID': instance.mediaID,
      'url': instance.url,
      'fileExtension': instance.fileExtension,
      'thumbnailPath': instance.thumbnailPath,
      'description': instance.description,
      'datetime': instance.datetime,
    };
