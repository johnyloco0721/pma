import 'package:json_annotation/json_annotation.dart';
import 'package:sources/models/team_member_model.dart';

part 'team_member_list_model.g.dart';

@JsonSerializable()
class TeamMemberListModel {
  bool? status;
  String? message;
  List<TeamMemberDetails>? response;

  TeamMemberListModel({this.status, this.message, this.response});

  factory TeamMemberListModel.fromJson(Map<String, dynamic> json) => _$TeamMemberListModelFromJson(json);

  Map<String, dynamic> toJson() => _$TeamMemberListModelToJson(this);
}