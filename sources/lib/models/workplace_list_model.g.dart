// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'workplace_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WorkplaceListModel _$WorkplaceListModelFromJson(Map<String, dynamic> json) =>
    WorkplaceListModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: (json['response'] as List<dynamic>?)
          ?.map((e) => WorkplaceListDetails.fromJson(e as Map<String, dynamic>))
          .toList(),
      totalItems: (json['totalItems'] as num?)?.toInt(),
      currentPageNo: (json['currentPageNo'] as num?)?.toInt(),
      limitPerPage: (json['limitPerPage'] as num?)?.toInt(),
    );

Map<String, dynamic> _$WorkplaceListModelToJson(WorkplaceListModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
      'totalItems': instance.totalItems,
      'currentPageNo': instance.currentPageNo,
      'limitPerPage': instance.limitPerPage,
    };

WorkplaceListDetails _$WorkplaceListDetailsFromJson(
        Map<String, dynamic> json) =>
    WorkplaceListDetails(
      projectID: (json['projectID'] as num?)?.toInt(),
      projectArea: json['projectArea'] as String?,
      projectUnit: json['projectUnit'] as String?,
      projectStatus: json['projectStatus'] as String?,
      isArchived: json['isArchived'] as bool?,
      teamMemberIDs: json['teamMemberIDs'] as String?,
      ownerName: json['ownerName'] as String?,
      isRead: json['isRead'] as String?,
      lastViewedDateTime: json['lastViewedDateTime'] as String?,
      updatedDateTime: json['updatedDateTime'] as String?,
      displayDateTime: json['displayDateTime'] as String?,
      projectName: json['projectName'] as String?,
      propertyName: json['propertyName'] as String?,
      includedMemberArr: (json['includedMemberArr'] as List<dynamic>?)
          ?.map((e) => TeamMemberDetails.fromJson(e as Map<String, dynamic>))
          .toList(),
      lastReadFeedID: (json['lastReadFeedID'] as num?)?.toInt(),
    );

Map<String, dynamic> _$WorkplaceListDetailsToJson(
        WorkplaceListDetails instance) =>
    <String, dynamic>{
      'projectID': instance.projectID,
      'projectArea': instance.projectArea,
      'projectUnit': instance.projectUnit,
      'projectStatus': instance.projectStatus,
      'isArchived': instance.isArchived,
      'teamMemberIDs': instance.teamMemberIDs,
      'ownerName': instance.ownerName,
      'isRead': instance.isRead,
      'lastViewedDateTime': instance.lastViewedDateTime,
      'updatedDateTime': instance.updatedDateTime,
      'displayDateTime': instance.displayDateTime,
      'projectName': instance.projectName,
      'propertyName': instance.propertyName,
      'includedMemberArr': instance.includedMemberArr,
      'lastReadFeedID': instance.lastReadFeedID,
    };
