import 'package:json_annotation/json_annotation.dart';

part 'task_requirements.g.dart';

@JsonSerializable()
class Requirements {
  int? fileRequired;

  Requirements({
    this.fileRequired,
  });

  factory Requirements.fromJson(Map<String, dynamic> json) => _$RequirementsFromJson(json);

  Map<String, dynamic> toJson() => _$RequirementsToJson(this);
}

@JsonSerializable()
class ClientAppRequirements {
  int? autoUpdateCA;
  int? imageUpdateCA;

  ClientAppRequirements({
    this.autoUpdateCA,
    this.imageUpdateCA,
  });

  factory ClientAppRequirements.fromJson(Map<String, dynamic> json) => _$ClientAppRequirementsFromJson(json);

  Map<String, dynamic> toJson() => _$ClientAppRequirementsToJson(this);
}

@JsonSerializable()
class ExtraDatesDisplayArr {
  String? label;
  String? date;

  ExtraDatesDisplayArr({
    this.label,
    this.date,
  });

  factory ExtraDatesDisplayArr.fromJson(Map<String, dynamic> json) => _$ExtraDatesDisplayArrFromJson(json);

  Map<String, dynamic> toJson() => _$ExtraDatesDisplayArrToJson(this);
}