import 'package:json_annotation/json_annotation.dart';
import 'package:sources/models/feed_item_model.dart';

part 'feed_model.g.dart';

@JsonSerializable()
class FeedModel {
  bool? status;
  String? message;
  FeedResponse? response;
  int? totalItems;
  int? currentPageNo;
  int? limitPerPage;

  FeedModel({this.status, this.message, this.response, this.totalItems, this.currentPageNo, this.limitPerPage});

  factory FeedModel.fromJson(Map<String, dynamic> json) => _$FeedModelFromJson(json);

  Map<String, dynamic> toJson() => _$FeedModelToJson(this);
}

@JsonSerializable()
class FeedResponse {
  List<FeedProjectDetail>? projectDetail;
  int? lastReadFeedID;
  List<FeedItemModel>? feedList;

  FeedResponse({this.lastReadFeedID, this.feedList});

  factory FeedResponse.fromJson(Map<String, dynamic> json) => _$FeedResponseFromJson(json);

  Map<String, dynamic> toJson() => _$FeedResponseToJson(this);
}

@JsonSerializable()
class FeedProjectDetail {
  int? projectID;
  String? projectArea;
  String? projectUnit;
  String? projectStatus;
  String? teamMemberIDs;
  String? ownerName;
  String? isRead;
  String? lastViewedDateTime;
  String? projectName;
  String? propertyName;
  int? completedTasks;
  int? totalTasks;

  FeedProjectDetail({
    this.projectID,
    this.projectArea, 
    this.projectUnit, 
    this.projectStatus, 
    this.teamMemberIDs, 
    this.ownerName, 
    this.isRead, 
    this.lastViewedDateTime, 
    this.projectName, 
    this.propertyName,
    this.completedTasks,
    this.totalTasks,
  });

  factory FeedProjectDetail.fromJson(Map<String, dynamic> json) => _$FeedProjectDetailFromJson(json);
  
  Map<String, dynamic> toJson() => _$FeedProjectDetailToJson(this);
}
