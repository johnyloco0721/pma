// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_milestone_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskMilestoneModel _$TaskMilestoneModelFromJson(Map<String, dynamic> json) =>
    TaskMilestoneModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: json['response'] == null
          ? null
          : TaskMilestoneResponse.fromJson(
              json['response'] as Map<String, dynamic>),
      totalItems: (json['totalItems'] as num?)?.toInt(),
      currentPageNo: (json['currentPageNo'] as num?)?.toInt(),
      limitPerPage: (json['limitPerPage'] as num?)?.toInt(),
    );

Map<String, dynamic> _$TaskMilestoneModelToJson(TaskMilestoneModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
      'totalItems': instance.totalItems,
      'currentPageNo': instance.currentPageNo,
      'limitPerPage': instance.limitPerPage,
    };

TaskMilestoneResponse _$TaskMilestoneResponseFromJson(
        Map<String, dynamic> json) =>
    TaskMilestoneResponse(
      permissions: json['permissions'] == null
          ? null
          : PermissionsModel.fromJson(
              json['permissions'] as Map<String, dynamic>),
      teamMemberList: (json['teamMemberList'] as List<dynamic>?)
          ?.map((e) => TeamMemberDetails.fromJson(e as Map<String, dynamic>))
          .toList(),
      milestoneList: (json['milestoneList'] as List<dynamic>?)
          ?.map((e) => TaskMilestoneOption.fromJson(e as Map<String, dynamic>))
          .toList(),
      stageArray: (json['stageArray'] as List<dynamic>?)
          ?.map((e) => StageDetail.fromJson(e as Map<String, dynamic>))
          .toList(),
      disclaimerArr: (json['disclaimerArr'] as List<dynamic>?)
          ?.map((e) => DisclaimerList.fromJson(e as Map<String, dynamic>))
          .toList(),
      projectDetail: json['projectDetail'] == null
          ? null
          : TaskProjectItemModel.fromJson(
              json['projectDetail'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TaskMilestoneResponseToJson(
        TaskMilestoneResponse instance) =>
    <String, dynamic>{
      'projectDetail': instance.projectDetail,
      'permissions': instance.permissions,
      'teamMemberList': instance.teamMemberList,
      'milestoneList': instance.milestoneList,
      'stageArray': instance.stageArray,
      'disclaimerArr': instance.disclaimerArr,
    };

StageDetail _$StageDetailFromJson(Map<String, dynamic> json) => StageDetail(
      label: json['label'] as String?,
      iconColor: json['iconColor'] as String?,
      taskList: (json['taskList'] as List<dynamic>?)
          ?.map((e) => TaskMilestoneDetail.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$StageDetailToJson(StageDetail instance) =>
    <String, dynamic>{
      'label': instance.label,
      'iconColor': instance.iconColor,
      'taskList': instance.taskList,
    };

TaskMilestoneDetail _$TaskMilestoneDetailFromJson(Map<String, dynamic> json) =>
    TaskMilestoneDetail(
      milestoneCategoryID: (json['milestoneCategoryID'] as num?)?.toInt(),
      milestoneCategoryName: json['milestoneCategoryName'] as String?,
      milestoneCategoryOrder: (json['milestoneCategoryOrder'] as num?)?.toInt(),
      startDateTime: json['startDateTime'] as String?,
      endDateTime: json['endDateTime'] as String?,
      milestoneWeek: json['milestoneWeek'] as String?,
      milestoneStatus: json['milestoneStatus'] as String?,
      isExpanded: json['isExpanded'] as bool? ?? true,
      stage: json['stage'] == null
          ? null
          : StageModel.fromJson(json['stage'] as Map<String, dynamic>),
      tasks: (json['tasks'] as List<dynamic>?)
          ?.map((e) => TaskItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$TaskMilestoneDetailToJson(
        TaskMilestoneDetail instance) =>
    <String, dynamic>{
      'milestoneCategoryID': instance.milestoneCategoryID,
      'milestoneCategoryName': instance.milestoneCategoryName,
      'milestoneCategoryOrder': instance.milestoneCategoryOrder,
      'startDateTime': instance.startDateTime,
      'endDateTime': instance.endDateTime,
      'milestoneWeek': instance.milestoneWeek,
      'milestoneStatus': instance.milestoneStatus,
      'isExpanded': instance.isExpanded,
      'stage': instance.stage,
      'tasks': instance.tasks,
    };

TaskMilestoneOption _$TaskMilestoneOptionFromJson(Map<String, dynamic> json) =>
    TaskMilestoneOption(
      milestoneCategoryID: (json['milestoneCategoryID'] as num?)?.toInt(),
      milestoneCategoryName: json['milestoneCategoryName'] as String?,
      milestoneCategoryOrder: (json['milestoneCategoryOrder'] as num?)?.toInt(),
      icon: json['icon'] as String?,
      colour: json['colour'] as String?,
    );

Map<String, dynamic> _$TaskMilestoneOptionToJson(
        TaskMilestoneOption instance) =>
    <String, dynamic>{
      'milestoneCategoryID': instance.milestoneCategoryID,
      'milestoneCategoryName': instance.milestoneCategoryName,
      'milestoneCategoryOrder': instance.milestoneCategoryOrder,
      'icon': instance.icon,
      'colour': instance.colour,
    };

DisclaimerList _$DisclaimerListFromJson(Map<String, dynamic> json) =>
    DisclaimerList(
      borderColor: json['borderColor'] as String?,
      backgroundColor: json['backgroundColor'] as String?,
      htmlText: json['htmlText'] as String?,
    );

Map<String, dynamic> _$DisclaimerListToJson(DisclaimerList instance) =>
    <String, dynamic>{
      'borderColor': instance.borderColor,
      'backgroundColor': instance.backgroundColor,
      'htmlText': instance.htmlText,
    };
