import 'package:json_annotation/json_annotation.dart';
import 'package:sources/models/team_member_model.dart';

part 'workplace_details_model.g.dart';

@JsonSerializable()
class WorkplaceDetailsModel {
  bool? status;
  String? message;
  WorkplaceDetails? response;

  WorkplaceDetailsModel({this.status, this.message, this.response});

  factory WorkplaceDetailsModel.fromJson(Map<String, dynamic> json) => _$WorkplaceDetailsModelFromJson(json);

  Map<String, dynamic> toJson() => _$WorkplaceDetailsModelToJson(this);
}

@JsonSerializable()
class WorkplaceDetails {
  int? projectID;
  bool? projectHeader;
  ProjectArr? projectArr;
  List<TeamMemberDetails>? includedMemberArr;
  bool? allowPosting;

  WorkplaceDetails({
    this.projectID,
    this.projectHeader,
    this.projectArr,
    this.includedMemberArr,
    this.allowPosting,
  });

  factory WorkplaceDetails.fromJson(Map<String, dynamic> json) => _$WorkplaceDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$WorkplaceDetailsToJson(this);
}

@JsonSerializable()
class ProjectArr {
  int? projectID;
  String? projectArea;
  String? projectUnit;
  int? projectSize;
  int? projectBedroom;
  int? projectBathroom;
  String? marketSegment;
  String? themeName;
  String? startDateTime;
  String? targetDateTime;
  String? dueDateTime;
  String? projectStatus;
  String? projectCategory;
  int? createdBy;
  String? teamMemberName;
  String? teamMemberIDs;
  int? companyID;
  int? completedTasks;
  int? totalTasks;
  String? propertyName;
  String? propertyState;
  String? propertyType;
  List<TagsModel>? tags;
  List<ProjectInfo>? infoList;

  ProjectArr({
    this.projectID,
    this.projectArea,
    this.projectUnit,
    this.projectSize,
    this.projectBedroom,
    this.projectBathroom,
    this.marketSegment,
    this.themeName,
    this.startDateTime,
    this.targetDateTime,
    this.dueDateTime,
    this.projectStatus,
    this.projectCategory,
    this.createdBy,
    this.teamMemberName,
    this.teamMemberIDs,
    this.companyID,
    this.completedTasks,
    this.totalTasks,
    this.propertyName,
    this.propertyState,
    this.propertyType,
    this.tags,
    this.infoList,
  });

  factory ProjectArr.fromJson(Map<String, dynamic> json) => _$ProjectArrFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectArrToJson(this);
}


@JsonSerializable()
class TagsModel {
  String? label;
  String? borderColor;
  String? textColor;
  String? backgroundColor;
  String? icon;

  TagsModel({this.label, this.borderColor,this.textColor,this.backgroundColor,this.icon});

  factory TagsModel.fromJson(Map<String, dynamic> json) => _$TagsModelFromJson(json);

  Map<String, dynamic> toJson() => _$TagsModelToJson(this);
}

@JsonSerializable()
class ProjectInfo {
  String? infoLabel;
  String? infoDisplayValue;

  ProjectInfo({
    this.infoLabel,
    this.infoDisplayValue,
  });

  factory ProjectInfo.fromJson(Map<String, dynamic> json) => _$ProjectInfoFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectInfoToJson(this);
}
