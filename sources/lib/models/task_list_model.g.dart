// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskListModel _$TaskListModelFromJson(Map<String, dynamic> json) =>
    TaskListModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: json['response'] as List<dynamic>?,
      totalItems: (json['totalItems'] as num?)?.toInt(),
      currentPageNo: (json['currentPageNo'] as num?)?.toInt(),
      limitPerPage: (json['limitPerPage'] as num?)?.toInt(),
    );

Map<String, dynamic> _$TaskListModelToJson(TaskListModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
      'totalItems': instance.totalItems,
      'currentPageNo': instance.currentPageNo,
      'limitPerPage': instance.limitPerPage,
    };
