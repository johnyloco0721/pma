// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_item_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskItemModel _$TaskItemModelFromJson(Map<String, dynamic> json) =>
    TaskItemModel(
      taskID: (json['taskID'] as num?)?.toInt(),
      templateTaskID: (json['templateTaskID'] as num?)?.toInt(),
      isSpecial: json['isSpecial'] as bool?,
      taskName: json['taskName'] as String?,
      taskStatus: json['taskStatus'] as String?,
      projectID: (json['projectID'] as num?)?.toInt(),
      propertyName: json['propertyName'] as String?,
      projectUnit: json['projectUnit'] as String?,
      ownerName: json['ownerName'] as String?,
      ownerContactNo: json['ownerContactNo'] as String?,
      totalSubTask: (json['totalSubTask'] as num?)?.toInt(),
      completedSubTask: (json['completedSubTask'] as num?)?.toInt(),
      isHandover: (json['isHandover'] as num?)?.toInt(),
      isCompleted: (json['isCompleted'] as num?)?.toInt(),
      createdDateTime: json['createdDateTime'] as String?,
      updatedDateTime: json['updatedDateTime'] as String?,
      startDateTime: json['startDateTime'] as String?,
      endDateTime: json['endDateTime'] as String?,
      completedDateTime: json['completedDateTime'] as String?,
      taskStartDateTime: json['taskStartDateTime'] as String?,
      taskTargetDateTime: json['taskTargetDateTime'] as String?,
      taskEndDateTime: json['taskEndDateTime'] as String?,
      replyCount: (json['replyCount'] as num?)?.toInt(),
      isFileRequired: (json['isFileRequired'] as num?)?.toInt(),
      taskStartDateTimeArr: json['taskStartDateTimeArr'] as List<dynamic>?,
      taskTargetDateTimeArr: json['taskTargetDateTimeArr'] as List<dynamic>?,
      taskEndDateTimeArr: json['taskEndDateTimeArr'] as List<dynamic>?,
      assignTo: (json['assignTo'] as num?)?.toInt(),
      assignToName: json['assignToName'] as String?,
      isRead: json['isRead'] as String?,
      isFileRequiredLabel: json['isFileRequiredLabel'] as String?,
    );

Map<String, dynamic> _$TaskItemModelToJson(TaskItemModel instance) =>
    <String, dynamic>{
      'taskID': instance.taskID,
      'templateTaskID': instance.templateTaskID,
      'isSpecial': instance.isSpecial,
      'taskName': instance.taskName,
      'taskStatus': instance.taskStatus,
      'projectID': instance.projectID,
      'propertyName': instance.propertyName,
      'projectUnit': instance.projectUnit,
      'ownerName': instance.ownerName,
      'ownerContactNo': instance.ownerContactNo,
      'totalSubTask': instance.totalSubTask,
      'completedSubTask': instance.completedSubTask,
      'isHandover': instance.isHandover,
      'isCompleted': instance.isCompleted,
      'createdDateTime': instance.createdDateTime,
      'updatedDateTime': instance.updatedDateTime,
      'startDateTime': instance.startDateTime,
      'endDateTime': instance.endDateTime,
      'completedDateTime': instance.completedDateTime,
      'taskStartDateTime': instance.taskStartDateTime,
      'taskTargetDateTime': instance.taskTargetDateTime,
      'taskEndDateTime': instance.taskEndDateTime,
      'replyCount': instance.replyCount,
      'isFileRequired': instance.isFileRequired,
      'taskStartDateTimeArr': instance.taskStartDateTimeArr,
      'taskTargetDateTimeArr': instance.taskTargetDateTimeArr,
      'taskEndDateTimeArr': instance.taskEndDateTimeArr,
      'assignTo': instance.assignTo,
      'assignToName': instance.assignToName,
      'isRead': instance.isRead,
      'isFileRequiredLabel': instance.isFileRequiredLabel,
    };
