import 'package:json_annotation/json_annotation.dart';

part 'comment_list_model.g.dart';

@JsonSerializable()
class CommentListModel {
  bool? status;
  String? message;
  String? response;

  CommentListModel({this.status, this.message, this.response});

  factory CommentListModel.fromJson(Map<String, dynamic> json) => _$CommentListModelFromJson(json);

  Map<String, dynamic> toJson() => _$CommentListModelToJson(this);
}