// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'attach_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AttachModel _$AttachModelFromJson(Map<String, dynamic> json) => AttachModel(
      taskImageID: (json['taskImageID'] as num?)?.toInt(),
      imageName: json['imageName'] as String?,
      imageType: json['imageType'] as String?,
      taskID: (json['taskID'] as num?)?.toInt(),
      projectID: (json['projectID'] as num?)?.toInt(),
      teamMemberID: (json['teamMemberID'] as num?)?.toInt(),
      selectedStatus: (json['selectedStatus'] as num?)?.toInt(),
      createdDateTime: json['createdDateTime'] as String?,
      imagePath: json['imagePath'] as String?,
      thumbnailPath: json['thumbnailPath'] as String?,
    );

Map<String, dynamic> _$AttachModelToJson(AttachModel instance) =>
    <String, dynamic>{
      'taskImageID': instance.taskImageID,
      'imageName': instance.imageName,
      'imageType': instance.imageType,
      'taskID': instance.taskID,
      'projectID': instance.projectID,
      'teamMemberID': instance.teamMemberID,
      'selectedStatus': instance.selectedStatus,
      'createdDateTime': instance.createdDateTime,
      'imagePath': instance.imagePath,
      'thumbnailPath': instance.thumbnailPath,
    };
