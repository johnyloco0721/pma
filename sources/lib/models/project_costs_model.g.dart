// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'project_costs_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProjectCostsModel _$ProjectCostsModelFromJson(Map<String, dynamic> json) =>
    ProjectCostsModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: (json['response'] as List<dynamic>?)
          ?.map((e) => ProjectCostsDetails.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ProjectCostsModelToJson(ProjectCostsModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
    };

ProjectCostsDetails _$ProjectCostsDetailsFromJson(Map<String, dynamic> json) =>
    ProjectCostsDetails(
      projectCostID: (json['projectCostID'] as num?)?.toInt(),
      projectID: (json['projectID'] as num?)?.toInt(),
      costName: json['costName'] as String?,
      costAmount: json['costAmount'] as String?,
      costIndication: json['costIndication'] as String?,
      bookingFeeTxt: json['bookingFeeTxt'] as String?,
      costRemark: json['costRemark'] as String?,
      media: (json['media'] as List<dynamic>?)
          ?.map((e) => CostsMediaModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      document: (json['document'] as List<dynamic>?)
          ?.map((e) => CostsMediaModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      postDateTime: json['postDateTime'] as String?,
    );

Map<String, dynamic> _$ProjectCostsDetailsToJson(
        ProjectCostsDetails instance) =>
    <String, dynamic>{
      'projectCostID': instance.projectCostID,
      'projectID': instance.projectID,
      'costName': instance.costName,
      'costAmount': instance.costAmount,
      'costIndication': instance.costIndication,
      'bookingFeeTxt': instance.bookingFeeTxt,
      'costRemark': instance.costRemark,
      'media': instance.media,
      'document': instance.document,
      'postDateTime': instance.postDateTime,
    };
