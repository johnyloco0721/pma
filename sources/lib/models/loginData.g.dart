// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'loginData.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginData _$LoginDataFromJson(Map<String, dynamic> json) => LoginData(
      json['username'] as String?,
      json['password'] as String?,
      json['token'] as String?,
    );

Map<String, dynamic> _$LoginDataToJson(LoginData instance) => <String, dynamic>{
      'username': instance.username,
      'password': instance.password,
      'token': instance.token,
    };
