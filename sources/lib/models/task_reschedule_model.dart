import 'package:json_annotation/json_annotation.dart';
import 'task_milestone_model.dart';

part 'task_reschedule_model.g.dart';

@JsonSerializable()
class TaskRescheduleModel {
  bool? status;
  String? message;
  TaskRescheduleResponse? response;

  TaskRescheduleModel({this.status, this.message, this.response});

  factory TaskRescheduleModel.fromJson(Map<String, dynamic> json) => _$TaskRescheduleModelFromJson(json);

  Map<String, dynamic> toJson() => _$TaskRescheduleModelToJson(this);
}

@JsonSerializable()
class TaskRescheduleResponse {
  List<TaskMilestoneDetail>? taskList;
  int? rescheduledAmount;
  String? rescheduledConfig;
  
  TaskRescheduleResponse({this.taskList, this.rescheduledAmount, this.rescheduledConfig});

  factory TaskRescheduleResponse.fromJson(Map<String, dynamic> json) => _$TaskRescheduleResponseFromJson(json);

  Map<String, dynamic> toJson() => _$TaskRescheduleResponseToJson(this);
}
