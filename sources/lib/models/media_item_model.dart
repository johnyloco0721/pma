import 'package:json_annotation/json_annotation.dart';

part 'media_item_model.g.dart';

@JsonSerializable()
class MediaItemModel {
  int? mediaID;
  String? url;
  String? fileExtension;
  String? thumbnailPath;
  String? description;
  String? datetime;

  MediaItemModel({this.mediaID, this.url, this.fileExtension, this.thumbnailPath, this.datetime, this.description});

  factory MediaItemModel.fromJson(Map<String, dynamic> json) => _$MediaItemModelFromJson(json);

  Map<String, dynamic> toJson() => _$MediaItemModelToJson(this);
}
