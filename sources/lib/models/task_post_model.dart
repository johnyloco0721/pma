import 'package:json_annotation/json_annotation.dart';
import 'package:sources/models/task_requirements.dart';

part 'task_post_model.g.dart';

@JsonSerializable()
class TaskPostModel {
  int? taskID;
  int? projectID;
  int? milestoneID;
  int? templateTaskID;
  String? title;
  String? description;
  String? taskPaymentPercentage;
  String? targetDate;
  String? dueDate;
  int? assignTo;
  Requirements? requirements;
  ClientAppRequirements? clientAppRequirements;
  List<SubTasksPostModel> subTasks;
  TaskPostPreviousModel? oldTaskData;

  TaskPostModel({
    this.taskID, 
    this.projectID, 
    this.milestoneID, 
    this.templateTaskID, 
    this.description, 
    this.taskPaymentPercentage,
    this.title, 
    this.targetDate, 
    this.dueDate, 
    this.assignTo, 
    this.requirements, 
    this.clientAppRequirements, 
    required this.subTasks, 
    this.oldTaskData,
  });

  factory TaskPostModel.fromJson(Map<String, dynamic> json) => _$TaskPostModelFromJson(json);

  Map<String, dynamic> toJson() => _$TaskPostModelToJson(this);
}

@JsonSerializable()
class TaskPostPreviousModel {
  int? taskID;
  int? projectID;
  int? milestoneID;
  int? templateTaskID;
  String? title;
  String? description;
  String? taskPaymentPercentage;
  String? targetDate;
  String? dueDate;
  int? assignTo;
  Requirements? requirements;
  ClientAppRequirements? clientAppRequirements;
  List<SubTasksPostModel> subTasks;

  TaskPostPreviousModel({
    this.taskID, 
    this.projectID, 
    this.milestoneID, 
    this.templateTaskID, 
    this.description, 
    this.taskPaymentPercentage,
    this.title, 
    this.targetDate, 
    this.dueDate, 
    this.assignTo, 
    this.requirements, 
    this.clientAppRequirements, 
    required this.subTasks, 
  });

  factory TaskPostPreviousModel.fromJson(Map<String, dynamic> json) => _$TaskPostPreviousModelFromJson(json);

  Map<String, dynamic> toJson() => _$TaskPostPreviousModelToJson(this);
}

@JsonSerializable()
class SubTasksPostModel {
  int taskID;
  String? title;
  String? description;
  String? targetDate;
  String? dueDate;
  Requirements? requirements;
  ClientAppRequirements? clientAppRequirements;
  int? templateTaskID;


  SubTasksPostModel({
    this.taskID = 0,
    this.title,
    this.description,
    this.targetDate,
    this.dueDate,
    this.requirements, 
    this.clientAppRequirements, 
    this.templateTaskID,
  });

  factory SubTasksPostModel.fromJson(Map<String, dynamic> json) => _$SubTasksPostModelFromJson(json);

  Map<String, dynamic> toJson() => _$SubTasksPostModelToJson(this);
}