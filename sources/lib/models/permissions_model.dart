import 'package:json_annotation/json_annotation.dart';

part 'permissions_model.g.dart';

@JsonSerializable()
class PermissionsModel {
  bool? canAccessEditorMode;
  bool? canPushToClientApp;
  bool? canDeleteTask;
  bool? canRaiseInvoice;
  bool? canGenerateMakeoverTask;
  bool? canGenerateTask;

  PermissionsModel({this.canAccessEditorMode, this.canPushToClientApp, this.canDeleteTask, this.canRaiseInvoice, this.canGenerateMakeoverTask, this.canGenerateTask});

  factory PermissionsModel.fromJson(Map<String, dynamic> json) => _$PermissionsModelFromJson(json);

  Map<String, dynamic> toJson() => _$PermissionsModelToJson(this);
}
