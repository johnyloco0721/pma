import 'package:json_annotation/json_annotation.dart';

part 'workplace_media_model.g.dart';

@JsonSerializable()
class WorkplaceMediaModel {
  bool? status;
  String? message;
  WorkplaceMediaList? response;

  WorkplaceMediaModel({this.status, this.message, this.response});

  factory WorkplaceMediaModel.fromJson(Map<String, dynamic> json) => _$WorkplaceMediaModelFromJson(json);

  Map<String, dynamic> toJson() => _$WorkplaceMediaModelToJson(this);
}

@JsonSerializable()
class WorkplaceMediaList {
  List<WorkplaceMediaDetails>? media;
  List<WorkplaceMediaDetails>? document;

  WorkplaceMediaList({
    this.media,
    this.document,
  });

  factory WorkplaceMediaList.fromJson(Map<String, dynamic> json) => _$WorkplaceMediaListFromJson(json);

  Map<String, dynamic> toJson() => _$WorkplaceMediaListToJson(this);
}

@JsonSerializable()
class WorkplaceMediaDetails {
  int? mediaID;
  String? projectFeedsMediaName;
  String? projectFeedsMedia;
  String? projectFeedsMediaType;
  String? createdDateTime;
  String? caption;
  String? module;

  WorkplaceMediaDetails({
    this.mediaID,
    this.projectFeedsMediaName,
    this.projectFeedsMedia,
    this.projectFeedsMediaType,
    this.createdDateTime,
    this.caption,
    this.module,
  });

  factory WorkplaceMediaDetails.fromJson(Map<String, dynamic> json) => _$WorkplaceMediaDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$WorkplaceMediaDetailsToJson(this);
}
