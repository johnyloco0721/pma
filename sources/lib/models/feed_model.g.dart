// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feed_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedModel _$FeedModelFromJson(Map<String, dynamic> json) => FeedModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: json['response'] == null
          ? null
          : FeedResponse.fromJson(json['response'] as Map<String, dynamic>),
      totalItems: (json['totalItems'] as num?)?.toInt(),
      currentPageNo: (json['currentPageNo'] as num?)?.toInt(),
      limitPerPage: (json['limitPerPage'] as num?)?.toInt(),
    );

Map<String, dynamic> _$FeedModelToJson(FeedModel instance) => <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
      'totalItems': instance.totalItems,
      'currentPageNo': instance.currentPageNo,
      'limitPerPage': instance.limitPerPage,
    };

FeedResponse _$FeedResponseFromJson(Map<String, dynamic> json) => FeedResponse(
      lastReadFeedID: (json['lastReadFeedID'] as num?)?.toInt(),
      feedList: (json['feedList'] as List<dynamic>?)
          ?.map((e) => FeedItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    )..projectDetail = (json['projectDetail'] as List<dynamic>?)
        ?.map((e) => FeedProjectDetail.fromJson(e as Map<String, dynamic>))
        .toList();

Map<String, dynamic> _$FeedResponseToJson(FeedResponse instance) =>
    <String, dynamic>{
      'projectDetail': instance.projectDetail,
      'lastReadFeedID': instance.lastReadFeedID,
      'feedList': instance.feedList,
    };

FeedProjectDetail _$FeedProjectDetailFromJson(Map<String, dynamic> json) =>
    FeedProjectDetail(
      projectID: (json['projectID'] as num?)?.toInt(),
      projectArea: json['projectArea'] as String?,
      projectUnit: json['projectUnit'] as String?,
      projectStatus: json['projectStatus'] as String?,
      teamMemberIDs: json['teamMemberIDs'] as String?,
      ownerName: json['ownerName'] as String?,
      isRead: json['isRead'] as String?,
      lastViewedDateTime: json['lastViewedDateTime'] as String?,
      projectName: json['projectName'] as String?,
      propertyName: json['propertyName'] as String?,
      completedTasks: (json['completedTasks'] as num?)?.toInt(),
      totalTasks: (json['totalTasks'] as num?)?.toInt(),
    );

Map<String, dynamic> _$FeedProjectDetailToJson(FeedProjectDetail instance) =>
    <String, dynamic>{
      'projectID': instance.projectID,
      'projectArea': instance.projectArea,
      'projectUnit': instance.projectUnit,
      'projectStatus': instance.projectStatus,
      'teamMemberIDs': instance.teamMemberIDs,
      'ownerName': instance.ownerName,
      'isRead': instance.isRead,
      'lastViewedDateTime': instance.lastViewedDateTime,
      'projectName': instance.projectName,
      'propertyName': instance.propertyName,
      'completedTasks': instance.completedTasks,
      'totalTasks': instance.totalTasks,
    };
