// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comment_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommentListModel _$CommentListModelFromJson(Map<String, dynamic> json) =>
    CommentListModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: json['response'] as String?,
    );

Map<String, dynamic> _$CommentListModelToJson(CommentListModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
    };
