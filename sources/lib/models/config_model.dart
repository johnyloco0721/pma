import 'package:json_annotation/json_annotation.dart';

part 'config_model.g.dart';

@JsonSerializable()
class ConfigModel {
  bool? status;
  String? message;
  ConfigDetails? response;

  ConfigModel({this.status, this.message, this.response});

  factory ConfigModel.fromJson(Map<String, dynamic> json) =>
      _$ConfigModelFromJson(json);

  Map<String, dynamic> toJson() => _$ConfigModelToJson(this);
}

@JsonSerializable()
class ConfigDetails {
  String? mainSiteURL;
  String? PMASiteURL;
  String? mobileSiteURL;
  String? mobileVersion;
  bool? underMaintenance;
  Support? support;
  int? workplaceMediaUploadQuality;
  int? taskMediaUploadQuality;
  String? AndroidAppUpdateLink;
  String? iOSAppUpdateLink;

  ConfigDetails(
      {this.mainSiteURL,
      this.PMASiteURL,
      this.mobileSiteURL,
      this.mobileVersion,
      this.underMaintenance,
      this.support,
      this.workplaceMediaUploadQuality,
      this.taskMediaUploadQuality,
      this.AndroidAppUpdateLink,
      this.iOSAppUpdateLink});

  factory ConfigDetails.fromJson(Map<String, dynamic> json) => _$ConfigDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$ConfigDetailsToJson(this);
}

@JsonSerializable()
class Support {
  SupportTopic? topic;

  Support({this.topic});

  factory Support.fromJson(Map<String, dynamic> json) =>
      _$SupportFromJson(json);

  Map<String, dynamic> toJson() => _$SupportToJson(this);
}

@JsonSerializable()
class SupportTopic {
  String? help;
  String? feedback;
  String? others;

  SupportTopic({this.help, this.feedback, this.others});

  factory SupportTopic.fromJson(Map<String, dynamic> json) => _$SupportTopicFromJson(json);

  Map<String, dynamic> toJson() => _$SupportTopicToJson(this);
}
