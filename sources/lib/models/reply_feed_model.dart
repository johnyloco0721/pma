import 'package:json_annotation/json_annotation.dart';
import 'package:sources/models/feed_item_model.dart';

part 'reply_feed_model.g.dart';

@JsonSerializable()
class ReplyFeedModel {
  bool? status;
  String? message;
  ReplyFeedResponse? response;
  int? totalItems;
  int? currentPageNo;
  int? limitPerPage;

  ReplyFeedModel({this.status, this.message, this.response, this.totalItems, this.currentPageNo, this.limitPerPage});

  factory ReplyFeedModel.fromJson(Map<String, dynamic> json) => _$ReplyFeedModelFromJson(json);

  Map<String, dynamic> toJson() => _$ReplyFeedModelToJson(this);
}

@JsonSerializable()
class ReplyFeedResponse {
  int? lastReadFeedID;
  List<FeedItemModel>? feedList;

  ReplyFeedResponse({this.lastReadFeedID, this.feedList});

  factory ReplyFeedResponse.fromJson(Map<String, dynamic> json) => _$ReplyFeedResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ReplyFeedResponseToJson(this);
}
