import 'package:json_annotation/json_annotation.dart';
part 'alert_response_model.g.dart';

@JsonSerializable()
class AlertResponseModel {
  final int? taskID;
  final int? replyToID;
  final int? projectFeedsID;
  final int? companyID;
  final List? taggabledIDArr;

  AlertResponseModel({this.taskID, 
              this.replyToID, this.projectFeedsID, this.companyID,this.taggabledIDArr});

  factory AlertResponseModel.fromJson(Map<String, dynamic> json) => _$AlertResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$AlertResponseModelToJson(this);
}