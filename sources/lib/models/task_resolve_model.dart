import 'package:json_annotation/json_annotation.dart';

part 'task_resolve_model.g.dart';

@JsonSerializable()
class TaskResolveModel {
  int? taskID;
  int? projectID;
  List? mediaID;
  String? remarks;
  String? dropboxLink;
  String? actualHandoverDate;
  String? invoiceDate;

  TaskResolveModel({this.taskID, this.projectID, this.mediaID, this.remarks, this.dropboxLink, this.actualHandoverDate, this.invoiceDate});

  factory TaskResolveModel.fromJson(Map<String, dynamic> json) => _$TaskResolveModelFromJson(json);

  Map<String, dynamic> toJson() => _$TaskResolveModelToJson(this);
}
