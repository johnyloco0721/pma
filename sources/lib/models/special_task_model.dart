import 'package:json_annotation/json_annotation.dart';
import 'package:sources/models/task_requirements.dart';

part 'special_task_model.g.dart';

@JsonSerializable()
class SpecialTaskModel {
  bool? status;
  String? message;
  List<SpecialTaskDetails>? response;

  SpecialTaskModel({this.status, this.message, this.response});

  factory SpecialTaskModel.fromJson(Map<String, dynamic> json) => _$SpecialTaskModelFromJson(json);

  Map<String, dynamic> toJson() => _$SpecialTaskModelToJson(this);
}

@JsonSerializable()
class SpecialTaskDetails {
  int? templateTaskID;
  String? templateTaskName;
  String? templateTaskDescription;
  int? milestoneCategoryID;
  int? specialTaskOrder;
  Requirements? requirements;
  ClientAppRequirements? clientAppRequirements;
  bool? disabled;
  int? teamMemberID;
  bool? enabledSubtask;
  List<SpecialSubTaskDetails>? defaultSubtask;
  String? templateTaskPercentage;
  String? taskPercentage;

  SpecialTaskDetails({
    this.templateTaskID, 
    this.templateTaskName, 
    this.templateTaskDescription, 
    this.milestoneCategoryID, 
    this.specialTaskOrder, 
    this.requirements,
    this.clientAppRequirements,
    this.disabled, 
    this.teamMemberID, 
    this.enabledSubtask, 
    this.defaultSubtask, 
    this.templateTaskPercentage,
    this.taskPercentage,
  });

  factory SpecialTaskDetails.fromJson(Map<String, dynamic> json) => _$SpecialTaskDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$SpecialTaskDetailsToJson(this);
}

@JsonSerializable()
class SpecialSubTaskDetails {
  int? templateTaskID;
  String? templateTaskName;
  String? templateTaskDescription;
  int? milestoneCategoryID;
  int? defaultDesignationID;
  int? isAddSubTaskAllowed;
  int? isRemoveTaskAllowed;
  int? isAllowedToRepeatSpecialTask;
  int? isMilestoneEditable;
  int? isAssigneeEditable;
  int? isRequirementEditable;
  Requirements? requirements;
  ClientAppRequirements? clientAppRequirements;

  SpecialSubTaskDetails({
    this.templateTaskID, 
    this.templateTaskName, 
    this.templateTaskDescription, 
    this.milestoneCategoryID, 
    this.defaultDesignationID,
    this.isAddSubTaskAllowed,
    this.isRemoveTaskAllowed,
    this.isAllowedToRepeatSpecialTask,
    this.isMilestoneEditable,
    this.isAssigneeEditable,
    this.isRequirementEditable,
    this.requirements,
    this.clientAppRequirements,
  });

  factory SpecialSubTaskDetails.fromJson(Map<String, dynamic> json) => _$SpecialSubTaskDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$SpecialSubTaskDetailsToJson(this);
}