import 'package:json_annotation/json_annotation.dart';
import 'package:sources/models/costs_media_model.dart';

part 'project_costs_model.g.dart';

@JsonSerializable()
class ProjectCostsModel {
  bool? status;
  String? message;
  List<ProjectCostsDetails>? response;


  ProjectCostsModel({this.status, this.message, this.response});

  factory ProjectCostsModel.fromJson(Map<String, dynamic> json) => _$ProjectCostsModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectCostsModelToJson(this);
}

@JsonSerializable()
class ProjectCostsDetails {
  int? projectCostID;
  int? projectID;
  String? costName;
  String? costAmount;
  String? costIndication;
  String? bookingFeeTxt;
  String? costRemark;
  List<CostsMediaModel>? media;
  List<CostsMediaModel>? document;
  String? postDateTime;

  ProjectCostsDetails({
    this.projectCostID,
    this.projectID,
    this.costName,
    this.costAmount,
    this.costIndication,
    this.bookingFeeTxt,
    this.costRemark,
    this.media,
    this.document,
    this.postDateTime,
  });

  factory ProjectCostsDetails.fromJson(Map<String, dynamic> json) => _$ProjectCostsDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectCostsDetailsToJson(this);
}