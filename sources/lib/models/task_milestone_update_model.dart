import 'package:json_annotation/json_annotation.dart';
import 'package:sources/models/task_milestone_model.dart';

part 'task_milestone_update_model.g.dart';

@JsonSerializable()
class MilestoneListUpdateModel {

  List<TaskMilestoneDetail>? taskList;

  MilestoneListUpdateModel({this.taskList});

  factory MilestoneListUpdateModel.fromJson(Map<String, dynamic> json) => _$MilestoneListUpdateModelFromJson(json);

  Map<String, dynamic> toJson() => _$MilestoneListUpdateModelToJson(this);
}