// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comment_details_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommentDetailsModel _$CommentDetailsModelFromJson(Map<String, dynamic> json) =>
    CommentDetailsModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: CommentDetailsResponse.fromJson(
          json['response'] as Map<String, dynamic>),
      currentPageNo: (json['currentPageNo'] as num?)?.toInt(),
      limitPerPage: (json['limitPerPage'] as num?)?.toInt(),
      totalItems: (json['totalItems'] as num?)?.toInt(),
    );

Map<String, dynamic> _$CommentDetailsModelToJson(
        CommentDetailsModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
      'totalItems': instance.totalItems,
      'currentPageNo': instance.currentPageNo,
      'limitPerPage': instance.limitPerPage,
    };

CommentDetailsResponse _$CommentDetailsResponseFromJson(
        Map<String, dynamic> json) =>
    CommentDetailsResponse(
      taskDetails: json['taskDetails'] == null
          ? null
          : TaskDetailsModel.fromJson(
              json['taskDetails'] as Map<String, dynamic>),
      commentList: (json['commentList'] as List<dynamic>?)
          ?.map((e) => FeedItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CommentDetailsResponseToJson(
        CommentDetailsResponse instance) =>
    <String, dynamic>{
      'taskDetails': instance.taskDetails,
      'commentList': instance.commentList,
    };
