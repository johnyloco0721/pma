import 'package:json_annotation/json_annotation.dart';

part 'costs_media_model.g.dart';

@JsonSerializable()
class CostsMediaModel {
  int? mediaID;
  String? url;
  String? fileExtension;
  String? thumbnailPath;
  String? description;
  String? datetime;

  CostsMediaModel({this.mediaID, this.url, this.fileExtension, this.thumbnailPath, this.datetime, this.description});

  factory CostsMediaModel.fromJson(Map<String, dynamic> json) => _$CostsMediaModelFromJson(json);

  Map<String, dynamic> toJson() => _$CostsMediaModelToJson(this);
}
