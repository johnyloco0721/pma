// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feed_topic_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedTopicModel _$FeedTopicModelFromJson(Map<String, dynamic> json) =>
    FeedTopicModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: json['response'] == null
          ? null
          : FeedTopicResponse.fromJson(
              json['response'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$FeedTopicModelToJson(FeedTopicModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
    };

FeedTopicResponse _$FeedTopicResponseFromJson(Map<String, dynamic> json) =>
    FeedTopicResponse(
      topicList: (json['topicList'] as List<dynamic>?)
          ?.map((e) => FeedTopic.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$FeedTopicResponseToJson(FeedTopicResponse instance) =>
    <String, dynamic>{
      'topicList': instance.topicList,
    };

FeedTopic _$FeedTopicFromJson(Map<String, dynamic> json) => FeedTopic(
      topicID: (json['topicID'] as num?)?.toInt(),
      topicName: json['topicName'] as String?,
      topicIcon: json['topicIcon'] as String?,
      topicColour: json['topicColour'] as String?,
    );

Map<String, dynamic> _$FeedTopicToJson(FeedTopic instance) => <String, dynamic>{
      'topicID': instance.topicID,
      'topicName': instance.topicName,
      'topicIcon': instance.topicIcon,
      'topicColour': instance.topicColour,
    };
