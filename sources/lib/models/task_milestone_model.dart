import 'package:json_annotation/json_annotation.dart';
import 'package:sources/models/permissions_model.dart';
import 'package:sources/models/stage_model.dart';
import 'package:sources/models/task_item_model.dart';
import 'package:sources/models/task_project_item_model.dart';
import 'package:sources/models/team_member_model.dart';

part 'task_milestone_model.g.dart';

@JsonSerializable()
class TaskMilestoneModel {
  bool? status;
  String? message;
  TaskMilestoneResponse? response;
  int? totalItems;
  int? currentPageNo;
  int? limitPerPage;

  TaskMilestoneModel({this.status, this.message, this.response, this.totalItems, this.currentPageNo, this.limitPerPage});

  factory TaskMilestoneModel.fromJson(Map<String, dynamic> json) => _$TaskMilestoneModelFromJson(json);

  Map<String, dynamic> toJson() => _$TaskMilestoneModelToJson(this);
}

@JsonSerializable()
class TaskMilestoneResponse {
  TaskProjectItemModel? projectDetail;
  PermissionsModel? permissions;
  List<TeamMemberDetails>? teamMemberList;
  List<TaskMilestoneOption>? milestoneList;
  List<StageDetail>? stageArray;
  List<DisclaimerList>? disclaimerArr;

  TaskMilestoneResponse({
    this.permissions,
    this.teamMemberList,
    this.milestoneList,
    this.stageArray,
    this.disclaimerArr,
    this.projectDetail,
  });

  factory TaskMilestoneResponse.fromJson(Map<String, dynamic> json) => _$TaskMilestoneResponseFromJson(json);

  Map<String, dynamic> toJson() => _$TaskMilestoneResponseToJson(this);
}

@JsonSerializable()
class StageDetail {
  String? label;
  String? iconColor;
  List<TaskMilestoneDetail>? taskList;

  StageDetail({
    this.label,
    this.iconColor,
    this.taskList,
  });

  factory StageDetail.fromJson(Map<String, dynamic> json) => _$StageDetailFromJson(json);

  Map<String, dynamic> toJson() => _$StageDetailToJson(this);
}

@JsonSerializable()
class TaskMilestoneDetail {
  int? milestoneCategoryID;
  String? milestoneCategoryName;
  int? milestoneCategoryOrder;
  String? startDateTime;
  String? endDateTime;
  String? milestoneWeek;
  String? milestoneStatus;
  bool? isExpanded;
  StageModel? stage;
  List<TaskItemModel>? tasks;

  TaskMilestoneDetail({
    this.milestoneCategoryID,
    this.milestoneCategoryName,
    this.milestoneCategoryOrder,
    this.startDateTime,
    this.endDateTime,
    this.milestoneWeek,
    this.milestoneStatus,
    this.isExpanded = true,
    this.stage,
    this.tasks,
  });

  factory TaskMilestoneDetail.fromJson(Map<String, dynamic> json) => _$TaskMilestoneDetailFromJson(json);

  Map<String, dynamic> toJson() => _$TaskMilestoneDetailToJson(this);
}

@JsonSerializable()
class TaskMilestoneOption {
  int? milestoneCategoryID;
  String? milestoneCategoryName;
  int? milestoneCategoryOrder;
  String? icon;
  String? colour;

  TaskMilestoneOption({
    this.milestoneCategoryID,
    this.milestoneCategoryName,
    this.milestoneCategoryOrder,
    this.icon,
    this.colour,
  });

  factory TaskMilestoneOption.fromJson(Map<String, dynamic> json) => _$TaskMilestoneOptionFromJson(json);

  Map<String, dynamic> toJson() => _$TaskMilestoneOptionToJson(this);
}

@JsonSerializable()
class DisclaimerList {
  String? borderColor;
  String? backgroundColor;
  String? htmlText;

  DisclaimerList({
    this.borderColor,
    this.backgroundColor,
    this.htmlText,
  });

  factory DisclaimerList.fromJson(Map<String, dynamic> json) => _$DisclaimerListFromJson(json);

  Map<String, dynamic> toJson() => _$DisclaimerListToJson(this);
}
