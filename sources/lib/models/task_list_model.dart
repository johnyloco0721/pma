import 'package:json_annotation/json_annotation.dart';

part 'task_list_model.g.dart';

@JsonSerializable()
class TaskListModel {
  bool? status;
  String? message;
  List<dynamic>? response;
  int? totalItems;
  int? currentPageNo;
  int? limitPerPage;

  TaskListModel({this.status, this.message, this.response, this.totalItems, this.currentPageNo, this.limitPerPage});

  factory TaskListModel.fromJson(Map<String, dynamic> json) => _$TaskListModelFromJson(json);

  Map<String, dynamic> toJson() => _$TaskListModelToJson(this);
}