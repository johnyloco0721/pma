// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'costs_media_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CostsMediaModel _$CostsMediaModelFromJson(Map<String, dynamic> json) =>
    CostsMediaModel(
      mediaID: (json['mediaID'] as num?)?.toInt(),
      url: json['url'] as String?,
      fileExtension: json['fileExtension'] as String?,
      thumbnailPath: json['thumbnailPath'] as String?,
      datetime: json['datetime'] as String?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$CostsMediaModelToJson(CostsMediaModel instance) =>
    <String, dynamic>{
      'mediaID': instance.mediaID,
      'url': instance.url,
      'fileExtension': instance.fileExtension,
      'thumbnailPath': instance.thumbnailPath,
      'description': instance.description,
      'datetime': instance.datetime,
    };
