import 'package:json_annotation/json_annotation.dart';
import 'package:sources/models/attach_model.dart';
import 'package:sources/models/permissions_model.dart';
import 'package:sources/models/stage_model.dart';
import 'package:sources/models/task_milestone_model.dart';
import 'package:sources/models/task_requirements.dart';
import 'package:sources/models/team_member_model.dart';

part 'task_details_model.g.dart';

@JsonSerializable()
class TaskDetailsResponseModel {
  PermissionsModel? permissions;
  Requirements? requirements;
  ClientAppRequirements? clientAppRequirements;
  ExtraDatesDisplayArr? extraDatesDisplayArr;
  TaskDetailsModel? taskArr;
  List<SubtaskDetailsModel>? subTaskArr;
  List<TeamMemberDetails>? assigneeList;
  List<TaskMilestoneOption>? milestoneList;
  List<String>? paymentPercentageArr;

  TaskDetailsResponseModel({
    this.permissions,
    this.requirements,
    this.clientAppRequirements,
    this.extraDatesDisplayArr,
    this.taskArr,
    this.subTaskArr,
    this.paymentPercentageArr,
  });

  factory TaskDetailsResponseModel.fromJson(Map<String, dynamic> json) => _$TaskDetailsResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$TaskDetailsResponseModelToJson(this);
}

@JsonSerializable()
class TaskDetailsModel {
  int? taskID;
  String? taskName;
  String? taskDescription;
  int? templateTaskID;
  bool? isSpecial;
  String? specialTaskType;
  String? taskRelatedDate; // use to show handover actual date
  String? taskStatus;
  int? projectID;
  int? parentTaskID;
  String? parentTaskName;
  int? isCompleted;
  String? propertyName;
  String? teamMemberIDs;
  String? teamMemberName;
  String? teamMemberContactNo;
  String? teamMemberAvatar;
  int? createdBy;
  String? createdByName;
  String? createdByContactNo;
  String? ownerName;
  int? milestoneCategoryID;
  String? milestoneCategoryName;
  int? isFileRequired;
  String? taskStartDateTime;
  String? taskTargetDateTime;
  String? taskEndDateTime;
  String? completedDateTime;
  String? createdDateTime;
  String? updatedDateTime;
  String? clientAppAutoUpdate;
  String? clientAppPhotoAutoUpdate;
  int? isFlag;
  String? flagReason;
  String? isResolved;
  String? isRead;
  bool? isPushCancelled;
  String? updatesPushDateTime;
  String? postUpdateCreatedDateTime;
  String? isPushedToClientApp;
  int? teamMemberID;
  int? assignTo;
  String? assignToName;
  String? assignToContactNumber;
  String? flagRemark;
  String? taskUpdates;
  String? taskDisclaimer;
  String? taskDisclaimerBgColor;
  String? taskDisclaimerBorderColor;
  bool? hideCompletedButton;
  int? replyCount;
  StageModel? stage;
  String? isFileRequiredLabel;
  int? isTaskTitleEditable;
  int? isAddSubTaskAllowed;
  int?isRemoveTaskAllowed;
  int? isAllowedToRepeatSpecialTask;
  int? isMilestoneEditable;
  int? isAssigneeEditable;
  int? isRequirementEditable;
  int? isClientAppUpdateEditable;
  int? isClientAppPhotoEditable;
  int? isTaskDescriptionEditable;
  bool? isRaiseInvoiceButtonEditable;
  String? taskPercentage;
  String? paymentTaskName;
  ForClient? forClient;
  ForInternal? forInternal;

  TaskDetailsModel({
    this.taskID, 
    this.taskName,
    this.taskDescription,
    this.templateTaskID,
    this.isSpecial,
    this.specialTaskType,
    this.taskRelatedDate,
    this.taskStatus,
    this.projectID, 
    this.parentTaskID, 
    this.parentTaskName, 
    this.isCompleted, 
    this.propertyName, 
    this.teamMemberIDs, 
    this.teamMemberName, 
    this.teamMemberContactNo, 
    this.teamMemberAvatar, 
    this.createdBy, 
    this.createdByName, 
    this.createdByContactNo, 
    this.ownerName, 
    this.milestoneCategoryID, 
    this.milestoneCategoryName, 
    this.isFileRequired, 
    this.taskStartDateTime, 
    this.taskTargetDateTime, 
    this.taskEndDateTime, 
    this.completedDateTime, 
    this.replyCount, 
    this.createdDateTime, 
    this.updatedDateTime, 
    this.clientAppAutoUpdate, 
    this.clientAppPhotoAutoUpdate, 
    this.isFlag, 
    this.flagReason, 
    this.isResolved, 
    this.isRead,
    this.isPushCancelled, 
    this.updatesPushDateTime, 
    this.postUpdateCreatedDateTime, 
    this.isPushedToClientApp, 
    this.teamMemberID, 
    this.assignTo, 
    this.assignToName, 
    this.assignToContactNumber,
    this.flagRemark,
    this.taskUpdates, 
    this.taskDisclaimer, 
    this.taskDisclaimerBgColor, 
    this.taskDisclaimerBorderColor, 
    this.hideCompletedButton, 
    this.stage,
    this.isFileRequiredLabel,
    this.isTaskTitleEditable,
    this.isAddSubTaskAllowed,
    this.isRemoveTaskAllowed,
    this.isAllowedToRepeatSpecialTask,
    this.isMilestoneEditable,
    this.isAssigneeEditable,
    this.isRequirementEditable,
    this.isClientAppUpdateEditable,
    this.isClientAppPhotoEditable,
    this.isTaskDescriptionEditable,
    this.isRaiseInvoiceButtonEditable,
    this.taskPercentage,
    this.paymentTaskName,
  });

  factory TaskDetailsModel.fromJson(Map<String, dynamic> json) => _$TaskDetailsModelFromJson(json);

  Map<String, dynamic> toJson() => _$TaskDetailsModelToJson(this);
}

@JsonSerializable()
class SubtaskDetailsModel {
  int? taskID;
  int? parentTaskID;
  String? taskName;
  String? taskDescription;
  String? taskStatus;
  String? specialTaskType;
  String? taskRelatedDate; // use to show handover actual date
  int? isCompleted;
  int? isFileRequired;
  String? taskStartDateTime;
  String? taskTargetDateTime;
  String? taskEndDateTime;
  int? taskUpdateID;
  String? createdDateTime;
  String? updatedDateTime;
  int? replyCount;
  bool? isPushCancelled;
  String? isPushedToClientApp;
  String? updatesPushDateTime;
  String? isRead;
  String? flagRemark;
  String? isFileRequiredLabel;
  int? isTaskTitleEditable;
  int? isAddSubTaskAllowed;
  int? isRemoveTaskAllowed;
  int? isAllowedToRepeatSpecialTask;
  int? isMilestoneEditable;
  int? isAssigneeEditable;
  int? isRequirementEditable;
  int? isClientAppUpdateEditable;
  int? isClientAppPhotoEditable;
  String? clientAppAutoUpdate;
  String? clientAppPhotoAutoUpdate;
  bool? isSpecial;
  Requirements? requirements;
  ClientAppRequirements? clientAppRequirements;
  ForClient? forClient;
  ForInternal? forInternal;

  int? isFlag;
  int? templateTaskID;

  SubtaskDetailsModel({
    this.taskID,
    this.parentTaskID,
    this.taskName,
    this.taskDescription,
    this.taskStatus,
    this.specialTaskType,
    this.taskRelatedDate,
    this.isCompleted,
    this.isFileRequired,
    this.taskStartDateTime,
    this.taskTargetDateTime,
    this.taskEndDateTime,
    this.taskUpdateID,
    this.createdDateTime,
    this.updatedDateTime,
    this.replyCount,
    this.isPushCancelled,
    this.isPushedToClientApp,
    this.updatesPushDateTime,
    this.isRead,
    this.flagRemark,
    this.isFileRequiredLabel,
    this.isTaskTitleEditable,
    this.isAddSubTaskAllowed,
    this.isRemoveTaskAllowed,
    this.isAllowedToRepeatSpecialTask,
    this.isMilestoneEditable,
    this.isAssigneeEditable,
    this.isRequirementEditable,
    this.isClientAppUpdateEditable,
    this.isClientAppPhotoEditable,
    this.clientAppAutoUpdate, 
    this.clientAppPhotoAutoUpdate, 
    this.isSpecial,
    this.requirements,
    this.clientAppRequirements,
    this.isFlag,
    this.templateTaskID,
  });

  factory SubtaskDetailsModel.fromJson(Map<String, dynamic> json) => _$SubtaskDetailsModelFromJson(json);

  Map<String, dynamic> toJson() => _$SubtaskDetailsModelToJson(this);
}

@JsonSerializable()
class ForClient {
  String? taskUpdates;
  String? taskCompletedBy;
  int? isPaymentDueDateEditable;
  List<AttachModel>? updatesImages;
  List<AttachModel>? updatesFiles;
  ExtraDatesDisplayArr? extraDatesDisplayArr;
  String? dropboxLink;

  ForClient({
    this.taskUpdates,
    this.taskCompletedBy,
    this.isPaymentDueDateEditable,
    this.updatesImages,
    this.updatesFiles,
    this.extraDatesDisplayArr,
    this.dropboxLink,
  });

  factory ForClient.fromJson(Map<String, dynamic> json) => _$ForClientFromJson(json);

  Map<String, dynamic> toJson() => _$ForClientToJson(this);
}

@JsonSerializable()
class ForInternal {
  String? taskUpdates;
  String? taskCompletedBy;
  int? isPaymentDueDateEditable;
  List<AttachModel>? updatesImages;
  List<AttachModel>? updatesFiles;
  ExtraDatesDisplayArr? extraDatesDisplayArr;
  String? dropboxLink;

  ForInternal({
    this.taskUpdates,
    this.taskCompletedBy,
    this.isPaymentDueDateEditable,
    this.updatesImages,
    this.updatesFiles,
    this.extraDatesDisplayArr,
    this.dropboxLink,
  });

  factory ForInternal.fromJson(Map<String, dynamic> json) => _$ForInternalFromJson(json);

  Map<String, dynamic> toJson() => _$ForInternalToJson(this);
}