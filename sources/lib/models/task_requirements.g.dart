// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_requirements.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Requirements _$RequirementsFromJson(Map<String, dynamic> json) => Requirements(
      fileRequired: (json['fileRequired'] as num?)?.toInt(),
    );

Map<String, dynamic> _$RequirementsToJson(Requirements instance) =>
    <String, dynamic>{
      'fileRequired': instance.fileRequired,
    };

ClientAppRequirements _$ClientAppRequirementsFromJson(
        Map<String, dynamic> json) =>
    ClientAppRequirements(
      autoUpdateCA: (json['autoUpdateCA'] as num?)?.toInt(),
      imageUpdateCA: (json['imageUpdateCA'] as num?)?.toInt(),
    );

Map<String, dynamic> _$ClientAppRequirementsToJson(
        ClientAppRequirements instance) =>
    <String, dynamic>{
      'autoUpdateCA': instance.autoUpdateCA,
      'imageUpdateCA': instance.imageUpdateCA,
    };

ExtraDatesDisplayArr _$ExtraDatesDisplayArrFromJson(
        Map<String, dynamic> json) =>
    ExtraDatesDisplayArr(
      label: json['label'] as String?,
      date: json['date'] as String?,
    );

Map<String, dynamic> _$ExtraDatesDisplayArrToJson(
        ExtraDatesDisplayArr instance) =>
    <String, dynamic>{
      'label': instance.label,
      'date': instance.date,
    };
