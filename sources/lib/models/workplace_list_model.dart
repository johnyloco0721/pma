import 'package:json_annotation/json_annotation.dart';
import 'package:sources/models/team_member_model.dart';

part 'workplace_list_model.g.dart';

@JsonSerializable()
class WorkplaceListModel {
  bool? status;
  String? message;
  List<WorkplaceListDetails>? response;
  int? totalItems;
  int? currentPageNo;
  int? limitPerPage;

  WorkplaceListModel({this.status, this.message, this.response, this.totalItems, this.currentPageNo, this.limitPerPage});

  factory WorkplaceListModel.fromJson(Map<String, dynamic> json) => _$WorkplaceListModelFromJson(json);

  Map<String, dynamic> toJson() => _$WorkplaceListModelToJson(this);
}

@JsonSerializable()
class WorkplaceListDetails {
  int? projectID;
  String? projectArea;
  String? projectUnit;
  String? projectStatus;
  bool? isArchived;
  String? teamMemberIDs;
  String? ownerName;
  String? isRead;
  String? lastViewedDateTime;
  String? updatedDateTime;
  String? displayDateTime;
  String? projectName;
  String? propertyName;
  List<TeamMemberDetails>? includedMemberArr;
  int? lastReadFeedID;

  WorkplaceListDetails({
    this.projectID,
    this.projectArea,
    this.projectUnit,
    this.projectStatus,
    this.isArchived,
    this.teamMemberIDs,
    this.ownerName,
    this.isRead,
    this.lastViewedDateTime,
    this.updatedDateTime,
    this.displayDateTime,
    this.projectName,
    this.propertyName,
    this.includedMemberArr,
    this.lastReadFeedID,
  });

  factory WorkplaceListDetails.fromJson(Map<String, dynamic> json) => _$WorkplaceListDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$WorkplaceListDetailsToJson(this);
}
