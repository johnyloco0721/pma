import 'package:json_annotation/json_annotation.dart';

part 'task_item_model.g.dart';

@JsonSerializable()
class TaskItemModel {
  int? taskID;
  int? templateTaskID;
  bool? isSpecial;
  String? taskName;
  String? taskStatus;
  int? projectID;
  String? propertyName;
  String? projectUnit;
  String? ownerName;
  String? ownerContactNo;
  int? totalSubTask;
  int? completedSubTask;
  int? isHandover;
  int? isCompleted;
  String? createdDateTime;
  String? updatedDateTime;
  String? startDateTime;
  String? endDateTime;
  String? completedDateTime;
  String? taskStartDateTime;
  String? taskTargetDateTime;
  String? taskEndDateTime;
  int? replyCount;
  int? isFileRequired;
  List? taskStartDateTimeArr;
  List? taskTargetDateTimeArr;
  List? taskEndDateTimeArr;
  int? assignTo;
  String? assignToName;
  String? isRead;
  String? isFileRequiredLabel;

  TaskItemModel({
    this.taskID,
    this.templateTaskID,
    this.isSpecial,
    this.taskName,
    this.taskStatus,
    this.projectID,
    this.propertyName,
    this.projectUnit,
    this.ownerName,
    this.ownerContactNo,
    this.totalSubTask,
    this.completedSubTask,
    this.isHandover,
    this.isCompleted,
    this.createdDateTime,
    this.updatedDateTime,
    this.startDateTime,
    this.endDateTime,
    this.completedDateTime,
    this.taskStartDateTime,
    this.taskTargetDateTime,
    this.taskEndDateTime,
    this.replyCount,
    this.isFileRequired,
    this.taskStartDateTimeArr,
    this.taskTargetDateTimeArr,
    this.taskEndDateTimeArr,
    this.assignTo,
    this.assignToName,
    this.isRead,
    this.isFileRequiredLabel,
  });

  factory TaskItemModel.fromJson(Map<String, dynamic> json) => _$TaskItemModelFromJson(json);

  Map<String, dynamic> toJson() => _$TaskItemModelToJson(this);
}
