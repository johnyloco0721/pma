// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_live_preview_post_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LivePreviewPostModel _$LivePreviewPostModelFromJson(
        Map<String, dynamic> json) =>
    LivePreviewPostModel(
      projectID: (json['projectID'] as num?)?.toInt(),
      taskIDs: json['taskIDs'] as List<dynamic>?,
      delayDuration: (json['delayDuration'] as num?)?.toInt(),
    );

Map<String, dynamic> _$LivePreviewPostModelToJson(
        LivePreviewPostModel instance) =>
    <String, dynamic>{
      'projectID': instance.projectID,
      'taskIDs': instance.taskIDs,
      'delayDuration': instance.delayDuration,
    };
