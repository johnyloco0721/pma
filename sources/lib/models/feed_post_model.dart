import 'package:json_annotation/json_annotation.dart';

part 'feed_post_model.g.dart';

@JsonSerializable()
class FeedPostModel {
  final int projectID;
  final int topicID;
  final List<int> mediaID;
  final int? replyID;
  final String description;
  final String uuid;
  final String feedType;

  FeedPostModel({
    required this.projectID, 
    required this.topicID, 
    required this.mediaID, 
    required this.replyID, 
    required this.description,
    required this.uuid,
    this.feedType = "normal"
  });

  factory FeedPostModel.fromJson(Map<String, dynamic> json) => _$FeedPostModelFromJson(json);

  Map<String, dynamic> toJson() => _$FeedPostModelToJson(this);
}

@JsonSerializable()
class FeedPutModel {
  final int feedID;
  final int topicID;
  final List<int> mediaID;
  final String description;

  FeedPutModel({required this.feedID, required this.topicID, required this.mediaID, required this.description});

  factory FeedPutModel.fromJson(Map<String, dynamic> json) => _$FeedPutModelFromJson(json);

  Map<String, dynamic> toJson() => _$FeedPutModelToJson(this);
}
