import 'package:json_annotation/json_annotation.dart';

part 'raise_invoice_model.g.dart';

@JsonSerializable()
class RaiseInvoiceModel {
  final int? projectID;
  final int? taskID;
  final String? remarks;

  RaiseInvoiceModel({
    this.projectID, 
    this.taskID, 
    this.remarks, 
  });

  factory RaiseInvoiceModel.fromJson(Map<String, dynamic> json) => _$RaiseInvoiceModelFromJson(json);

  Map<String, dynamic> toJson() => _$RaiseInvoiceModelToJson(this);
}
