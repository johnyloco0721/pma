// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_reschedule_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskRescheduleModel _$TaskRescheduleModelFromJson(Map<String, dynamic> json) =>
    TaskRescheduleModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: json['response'] == null
          ? null
          : TaskRescheduleResponse.fromJson(
              json['response'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TaskRescheduleModelToJson(
        TaskRescheduleModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
    };

TaskRescheduleResponse _$TaskRescheduleResponseFromJson(
        Map<String, dynamic> json) =>
    TaskRescheduleResponse(
      taskList: (json['taskList'] as List<dynamic>?)
          ?.map((e) => TaskMilestoneDetail.fromJson(e as Map<String, dynamic>))
          .toList(),
      rescheduledAmount: (json['rescheduledAmount'] as num?)?.toInt(),
      rescheduledConfig: json['rescheduledConfig'] as String?,
    );

Map<String, dynamic> _$TaskRescheduleResponseToJson(
        TaskRescheduleResponse instance) =>
    <String, dynamic>{
      'taskList': instance.taskList,
      'rescheduledAmount': instance.rescheduledAmount,
      'rescheduledConfig': instance.rescheduledConfig,
    };
