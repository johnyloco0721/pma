// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_mark_complete_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskMarkCompleteModel _$TaskMarkCompleteModelFromJson(
        Map<String, dynamic> json) =>
    TaskMarkCompleteModel(
      projectID: (json['projectID'] as num).toInt(),
      taskID: (json['taskID'] as num).toInt(),
      taskImageID: (json['taskImageID'] as List<dynamic>)
          .map((e) => (e as num).toInt())
          .toList(),
      remark: json['remark'] as String?,
      dropboxLink: json['dropboxLink'] as String?,
      actualHandoverDate: json['actualHandoverDate'] as String?,
      invoiceDate: json['invoiceDate'] as String?,
    );

Map<String, dynamic> _$TaskMarkCompleteModelToJson(
        TaskMarkCompleteModel instance) =>
    <String, dynamic>{
      'projectID': instance.projectID,
      'taskID': instance.taskID,
      'taskImageID': instance.taskImageID,
      'remark': instance.remark,
      'dropboxLink': instance.dropboxLink,
      'actualHandoverDate': instance.actualHandoverDate,
      'invoiceDate': instance.invoiceDate,
    };
