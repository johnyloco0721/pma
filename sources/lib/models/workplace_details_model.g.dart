// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'workplace_details_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WorkplaceDetailsModel _$WorkplaceDetailsModelFromJson(
        Map<String, dynamic> json) =>
    WorkplaceDetailsModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: json['response'] == null
          ? null
          : WorkplaceDetails.fromJson(json['response'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$WorkplaceDetailsModelToJson(
        WorkplaceDetailsModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
    };

WorkplaceDetails _$WorkplaceDetailsFromJson(Map<String, dynamic> json) =>
    WorkplaceDetails(
      projectID: (json['projectID'] as num?)?.toInt(),
      projectHeader: json['projectHeader'] as bool?,
      projectArr: json['projectArr'] == null
          ? null
          : ProjectArr.fromJson(json['projectArr'] as Map<String, dynamic>),
      includedMemberArr: (json['includedMemberArr'] as List<dynamic>?)
          ?.map((e) => TeamMemberDetails.fromJson(e as Map<String, dynamic>))
          .toList(),
      allowPosting: json['allowPosting'] as bool?,
    );

Map<String, dynamic> _$WorkplaceDetailsToJson(WorkplaceDetails instance) =>
    <String, dynamic>{
      'projectID': instance.projectID,
      'projectHeader': instance.projectHeader,
      'projectArr': instance.projectArr,
      'includedMemberArr': instance.includedMemberArr,
      'allowPosting': instance.allowPosting,
    };

ProjectArr _$ProjectArrFromJson(Map<String, dynamic> json) => ProjectArr(
      projectID: (json['projectID'] as num?)?.toInt(),
      projectArea: json['projectArea'] as String?,
      projectUnit: json['projectUnit'] as String?,
      projectSize: (json['projectSize'] as num?)?.toInt(),
      projectBedroom: (json['projectBedroom'] as num?)?.toInt(),
      projectBathroom: (json['projectBathroom'] as num?)?.toInt(),
      marketSegment: json['marketSegment'] as String?,
      themeName: json['themeName'] as String?,
      startDateTime: json['startDateTime'] as String?,
      targetDateTime: json['targetDateTime'] as String?,
      dueDateTime: json['dueDateTime'] as String?,
      projectStatus: json['projectStatus'] as String?,
      projectCategory: json['projectCategory'] as String?,
      createdBy: (json['createdBy'] as num?)?.toInt(),
      teamMemberName: json['teamMemberName'] as String?,
      teamMemberIDs: json['teamMemberIDs'] as String?,
      companyID: (json['companyID'] as num?)?.toInt(),
      completedTasks: (json['completedTasks'] as num?)?.toInt(),
      totalTasks: (json['totalTasks'] as num?)?.toInt(),
      propertyName: json['propertyName'] as String?,
      propertyState: json['propertyState'] as String?,
      propertyType: json['propertyType'] as String?,
      tags: (json['tags'] as List<dynamic>?)
          ?.map((e) => TagsModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      infoList: (json['infoList'] as List<dynamic>?)
          ?.map((e) => ProjectInfo.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ProjectArrToJson(ProjectArr instance) =>
    <String, dynamic>{
      'projectID': instance.projectID,
      'projectArea': instance.projectArea,
      'projectUnit': instance.projectUnit,
      'projectSize': instance.projectSize,
      'projectBedroom': instance.projectBedroom,
      'projectBathroom': instance.projectBathroom,
      'marketSegment': instance.marketSegment,
      'themeName': instance.themeName,
      'startDateTime': instance.startDateTime,
      'targetDateTime': instance.targetDateTime,
      'dueDateTime': instance.dueDateTime,
      'projectStatus': instance.projectStatus,
      'projectCategory': instance.projectCategory,
      'createdBy': instance.createdBy,
      'teamMemberName': instance.teamMemberName,
      'teamMemberIDs': instance.teamMemberIDs,
      'companyID': instance.companyID,
      'completedTasks': instance.completedTasks,
      'totalTasks': instance.totalTasks,
      'propertyName': instance.propertyName,
      'propertyState': instance.propertyState,
      'propertyType': instance.propertyType,
      'tags': instance.tags,
      'infoList': instance.infoList,
    };

TagsModel _$TagsModelFromJson(Map<String, dynamic> json) => TagsModel(
      label: json['label'] as String?,
      borderColor: json['borderColor'] as String?,
      textColor: json['textColor'] as String?,
      backgroundColor: json['backgroundColor'] as String?,
      icon: json['icon'] as String?,
    );

Map<String, dynamic> _$TagsModelToJson(TagsModel instance) => <String, dynamic>{
      'label': instance.label,
      'borderColor': instance.borderColor,
      'textColor': instance.textColor,
      'backgroundColor': instance.backgroundColor,
      'icon': instance.icon,
    };

ProjectInfo _$ProjectInfoFromJson(Map<String, dynamic> json) => ProjectInfo(
      infoLabel: json['infoLabel'] as String?,
      infoDisplayValue: json['infoDisplayValue'] as String?,
    );

Map<String, dynamic> _$ProjectInfoToJson(ProjectInfo instance) =>
    <String, dynamic>{
      'infoLabel': instance.infoLabel,
      'infoDisplayValue': instance.infoDisplayValue,
    };
