import 'package:json_annotation/json_annotation.dart';

part 'task_edit_model.g.dart';

@JsonSerializable()
class MyTaskEditModel {
  int? taskID;
  ForClient? forClient;
  ForInternal? forInternal;

  MyTaskEditModel({
    this.taskID,
    this.forClient,
    this.forInternal,
  });

  factory MyTaskEditModel.fromJson(Map<String, dynamic> json) => _$MyTaskEditModelFromJson(json);

  Map<String, dynamic> toJson() => _$MyTaskEditModelToJson(this);
}

@JsonSerializable()
class ForInternal {
  List<int>? taskImageID;
  String? remark;
  String? dropboxLink;
  String? actualHandoverDate;
  String? invoiceDate;
  String? paymentDueDate;

  ForInternal({
    this.taskImageID,
    this.remark,
    this.dropboxLink,
    this.actualHandoverDate,
    this.invoiceDate,
    this.paymentDueDate,
  });

  factory ForInternal.fromJson(Map<String, dynamic> json) => _$ForInternalFromJson(json);

  Map<String, dynamic> toJson() => _$ForInternalToJson(this);
}

@JsonSerializable()
class ForClient {
  List<int>? taskImageID;
  String? remark;
  String? dropboxLink;
  String? actualHandoverDate;
  String? invoiceDate;
  String? paymentDueDate;

  ForClient({
    this.taskImageID,
    this.remark,
    this.dropboxLink,
    this.actualHandoverDate,
    this.invoiceDate,
    this.paymentDueDate,
  });

  factory ForClient.fromJson(Map<String, dynamic> json) => _$ForClientFromJson(json);

  Map<String, dynamic> toJson() => _$ForClientToJson(this);
}