// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_milestone_update_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MilestoneListUpdateModel _$MilestoneListUpdateModelFromJson(
        Map<String, dynamic> json) =>
    MilestoneListUpdateModel(
      taskList: (json['taskList'] as List<dynamic>?)
          ?.map((e) => TaskMilestoneDetail.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MilestoneListUpdateModelToJson(
        MilestoneListUpdateModel instance) =>
    <String, dynamic>{
      'taskList': instance.taskList,
    };
