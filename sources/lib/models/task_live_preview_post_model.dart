import 'package:json_annotation/json_annotation.dart';

part 'task_live_preview_post_model.g.dart';

@JsonSerializable()
class LivePreviewPostModel {
  int? projectID;
  List? taskIDs;
  int? delayDuration;

  LivePreviewPostModel({this.projectID, this.taskIDs, this.delayDuration});

  factory LivePreviewPostModel.fromJson(Map<String, dynamic> json) => _$LivePreviewPostModelFromJson(json);

  Map<String, dynamic> toJson() => _$LivePreviewPostModelToJson(this);
}
