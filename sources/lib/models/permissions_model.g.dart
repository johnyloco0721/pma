// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'permissions_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PermissionsModel _$PermissionsModelFromJson(Map<String, dynamic> json) =>
    PermissionsModel(
      canAccessEditorMode: json['canAccessEditorMode'] as bool?,
      canPushToClientApp: json['canPushToClientApp'] as bool?,
      canDeleteTask: json['canDeleteTask'] as bool?,
      canRaiseInvoice: json['canRaiseInvoice'] as bool?,
      canGenerateMakeoverTask: json['canGenerateMakeoverTask'] as bool?,
      canGenerateTask: json['canGenerateTask'] as bool?,
    );

Map<String, dynamic> _$PermissionsModelToJson(PermissionsModel instance) =>
    <String, dynamic>{
      'canAccessEditorMode': instance.canAccessEditorMode,
      'canPushToClientApp': instance.canPushToClientApp,
      'canDeleteTask': instance.canDeleteTask,
      'canRaiseInvoice': instance.canRaiseInvoice,
      'canGenerateMakeoverTask': instance.canGenerateMakeoverTask,
      'canGenerateTask': instance.canGenerateTask,
    };
