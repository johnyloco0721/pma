// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'raise_invoice_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RaiseInvoiceModel _$RaiseInvoiceModelFromJson(Map<String, dynamic> json) =>
    RaiseInvoiceModel(
      projectID: (json['projectID'] as num?)?.toInt(),
      taskID: (json['taskID'] as num?)?.toInt(),
      remarks: json['remarks'] as String?,
    );

Map<String, dynamic> _$RaiseInvoiceModelToJson(RaiseInvoiceModel instance) =>
    <String, dynamic>{
      'projectID': instance.projectID,
      'taskID': instance.taskID,
      'remarks': instance.remarks,
    };
