// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feed_post_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedPostModel _$FeedPostModelFromJson(Map<String, dynamic> json) =>
    FeedPostModel(
      projectID: (json['projectID'] as num).toInt(),
      topicID: (json['topicID'] as num).toInt(),
      mediaID: (json['mediaID'] as List<dynamic>)
          .map((e) => (e as num).toInt())
          .toList(),
      replyID: (json['replyID'] as num?)?.toInt(),
      description: json['description'] as String,
      uuid: json['uuid'] as String,
      feedType: json['feedType'] as String? ?? "normal",
    );

Map<String, dynamic> _$FeedPostModelToJson(FeedPostModel instance) =>
    <String, dynamic>{
      'projectID': instance.projectID,
      'topicID': instance.topicID,
      'mediaID': instance.mediaID,
      'replyID': instance.replyID,
      'description': instance.description,
      'uuid': instance.uuid,
      'feedType': instance.feedType,
    };

FeedPutModel _$FeedPutModelFromJson(Map<String, dynamic> json) => FeedPutModel(
      feedID: (json['feedID'] as num).toInt(),
      topicID: (json['topicID'] as num).toInt(),
      mediaID: (json['mediaID'] as List<dynamic>)
          .map((e) => (e as num).toInt())
          .toList(),
      description: json['description'] as String,
    );

Map<String, dynamic> _$FeedPutModelToJson(FeedPutModel instance) =>
    <String, dynamic>{
      'feedID': instance.feedID,
      'topicID': instance.topicID,
      'mediaID': instance.mediaID,
      'description': instance.description,
    };
