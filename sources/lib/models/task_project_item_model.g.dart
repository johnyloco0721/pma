// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_project_item_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskProjectItemModel _$TaskProjectItemModelFromJson(
        Map<String, dynamic> json) =>
    TaskProjectItemModel(
      projectID: (json['projectID'] as num?)?.toInt(),
      propertyName: json['propertyName'] as String?,
      projectUnit: json['projectUnit'] as String?,
      ownerName: json['ownerName'] as String?,
      ownerContactNo: json['ownerContactNo'] as String?,
      taskTargetDateTime: json['taskTargetDateTime'] as String?,
      completedTasks: (json['completedTasks'] as num?)?.toInt(),
      totalTasks: (json['totalTasks'] as num?)?.toInt(),
      projectStatus: json['projectStatus'] as String?,
      teamMemberID: (json['teamMemberID'] as num?)?.toInt(),
      teamMemberName: json['teamMemberName'] as String?,
      stage: json['stage'] == null
          ? null
          : StageModel.fromJson(json['stage'] as Map<String, dynamic>),
    )..taskStatus = json['taskStatus'] as String?;

Map<String, dynamic> _$TaskProjectItemModelToJson(
        TaskProjectItemModel instance) =>
    <String, dynamic>{
      'projectID': instance.projectID,
      'propertyName': instance.propertyName,
      'projectUnit': instance.projectUnit,
      'ownerName': instance.ownerName,
      'ownerContactNo': instance.ownerContactNo,
      'taskTargetDateTime': instance.taskTargetDateTime,
      'completedTasks': instance.completedTasks,
      'totalTasks': instance.totalTasks,
      'projectStatus': instance.projectStatus,
      'taskStatus': instance.taskStatus,
      'teamMemberID': instance.teamMemberID,
      'teamMemberName': instance.teamMemberName,
      'stage': instance.stage,
    };
