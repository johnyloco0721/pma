import 'package:drift/drift.dart';
import 'package:sources/database/app_database.dart';

part 'profile.g.dart';

class Profiles extends Table {
  IntColumn get id => integer().named('id')();
  TextColumn get username => text().nullable().named('username')();
  TextColumn get name => text().nullable().named('name')();
  TextColumn get address => text().nullable().named('address')();
  TextColumn get email => text().nullable().named('email')();
  TextColumn get gmail => text().nullable().named('gmail')();
  TextColumn get branch => text().nullable().named('branch')();
  TextColumn get designationName => text().nullable().named('designationName')();
  TextColumn get designationInitial => text().nullable().named('designationInitial')();
  TextColumn get avatar => text().nullable().named('avatar')();
  TextColumn get contactNo => text().nullable().named('contactNo')();
  TextColumn get access => text().nullable().named('access')(); // new column for DB version 2
  IntColumn get teamMemberID => integer().nullable().named('teamMemberID')(); // new column for DB version 2

  @override
  Set<Column> get primaryKey => {id};
}

@DriftAccessor(tables: [Profiles])
class ProfileDao extends DatabaseAccessor<AppDatabase> with _$ProfileDaoMixin {
  final AppDatabase db;

  ProfileDao(this.db) : super(db);
  
  Stream<Profile> watchProfile() {
    return (select(profiles)).watchSingle();
  }
  Future<Profile?> getSingleProfile(String username) => (select(profiles)..where((tbl) => tbl.username.equals(username))).getSingleOrNull();
  Future insertProfile(Insertable<Profile> profile) => into(profiles).insert(profile);
  Future updateProfile(Insertable<Profile> profile) => update(profiles).replace(profile);
  Future deleteProfile(Insertable<Profile> profile) => delete(profiles).delete(profile);
  Future insertOrUpdateProfile(Insertable<Profile> profile) => into(profiles).insertOnConflictUpdate(profile);

  Future deleteAllProfiles() => delete(profiles).go();
}
