import 'dart:async';
import 'package:drift/drift.dart';
import 'package:sources/database/app_database.dart';

part 'alerts.g.dart';

class Alerts extends Table {
  IntColumn get notificationID => integer().named('notificationID')();
  IntColumn get projectID => integer().named('projectID')();
  TextColumn get affectedModuleID => text().nullable().named('affectedModuleID')();
  TextColumn get moduleName => text().nullable().named('moduleName')();
  TextColumn get isRead => text().nullable().named('isRead')();
  TextColumn get description => text().nullable().named('description')();
  TextColumn get createdDateTime => text().nullable().named('createdDateTime')();
  TextColumn get updatedDateTime => text().nullable().named('updatedDateTime')(); // new column for DB version 2
  IntColumn get assignedByTeamMemberID => integer().named('assignedByTeamMemberID')();
  TextColumn get assignedByTeamMemberName => text().nullable().named('assignedByTeamMemberName')();
  TextColumn get teamMemberAvatar => text().nullable().named('teamMemberAvatar')();
  IntColumn get replyToID => integer().nullable().named('replyToID')();
  TextColumn get delayTaskIDs => text().nullable().named('delayTaskIDs')(); // new column for DB version 14
  IntColumn get delayDuration => integer().nullable().named('delayDuration')(); // new column for DB version 14
  TextColumn get allNotificationTxt => text().nullable().named('allNotificationTxt')(); // new column for DB version 15

  @override
  Set<Column> get primaryKey => {notificationID};
}

@DriftAccessor(tables: [Alerts])
class AlertDao extends DatabaseAccessor<AppDatabase> with _$AlertDaoMixin {
  final AppDatabase db;

  AlertDao(this.db) : super(db);
  
  Stream<List<Alert>> watchAllAlerts() {
    return (select(alerts)
              ..orderBy([
                (t) => OrderingTerm(expression: t.notificationID, mode: OrderingMode.desc),
              ])
            ).watch();
  }
  Stream<List<Alert>> watchCountUnreadAlerts() {
    return (select(alerts)
              ..where((tbl) => tbl.isRead.equals('unread'))
            ).watch();
  }
  Stream<Alert?> watchSingleAlert(int notificationID) {
    return (select(alerts)
              ..where((tbl) => tbl.notificationID.equals(notificationID))
            ).watchSingleOrNull();
  }
  Future<Alert?> getLatestUpdateAlert() {
    return (select(alerts)
              ..orderBy([
                (t) => OrderingTerm(expression: t.updatedDateTime, mode: OrderingMode.desc),
              ])
              ..limit(1)
            ).getSingleOrNull();
  }
  Future insertAlert(Insertable<Alert> alert) => into(alerts).insert(alert);
  Future updateAlert(Insertable<Alert> alert) => update(alerts).replace(alert);
  Future deleteAlert(Insertable<Alert> alert) => delete(alerts).delete(alert);
  Future insertOrUpdateAlert(Insertable<Alert> alert) => into(alerts).insertOnConflictUpdate(alert);
  Future<void> batchInsertOrUpdateAlerts(List<Alert> alertItems) async {
    for (var alert in alertItems) {
      await into(alerts).insertOnConflictUpdate(alert);
    }
  }
  Future updateAlertByID(int notificationID) => (update(alerts)
    ..where((tbl) => tbl.notificationID.equals(notificationID))
  ).write(AlertsCompanion(isRead: Value('read')));
  Future readAllTodayUnreadAlerts(String compareDatetime) => (
    update(alerts)
    ..where((tbl) => tbl.isRead.equals('unread'))
    ..where((tbl) => tbl.createdDateTime.isBiggerOrEqualValue(compareDatetime))
  ).write(AlertsCompanion(isRead: Value('read')));
  Future readAllPastUnreadAlerts(String compareDatetime) => (
    update(alerts)
    ..where((tbl) => tbl.isRead.equals('unread'))
    ..where((tbl) => tbl.createdDateTime.isSmallerThanValue(compareDatetime))
  ).write(AlertsCompanion(isRead: Value('read')));

  Future deleteAllAlerts() => delete(alerts).go();

  // Delete notification if notificationID is not in the array
  Future deleteAlertsByProjectID(int id) => (
    delete(alerts)
    ..where((tbl) => tbl.projectID.equals(id))
  ).go();
}
