// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'workplaces.dart';

// ignore_for_file: type=lint
mixin _$WorkplaceDaoMixin on DatabaseAccessor<AppDatabase> {
  $WorkplacesTable get workplaces => attachedDatabase.workplaces;
}
mixin _$TeamMemberDaoMixin on DatabaseAccessor<AppDatabase> {
  $WorkplacesTable get workplaces => attachedDatabase.workplaces;
  $TeamMembersTable get teamMembers => attachedDatabase.teamMembers;
}
mixin _$CostEstimationDaoMixin on DatabaseAccessor<AppDatabase> {
  $WorkplacesTable get workplaces => attachedDatabase.workplaces;
  $CostEstimationsTable get costEstimations => attachedDatabase.costEstimations;
}
mixin _$WorkplaceInfoDaoMixin on DatabaseAccessor<AppDatabase> {
  $WorkplacesTable get workplaces => attachedDatabase.workplaces;
  $WorkplaceInfosTable get workplaceInfos => attachedDatabase.workplaceInfos;
}
