// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_database.dart';

// ignore_for_file: type=lint
class $WorkplacesTable extends Workplaces
    with TableInfo<$WorkplacesTable, Workplace> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $WorkplacesTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _projectIDMeta =
      const VerificationMeta('projectID');
  @override
  late final GeneratedColumn<int> projectID = GeneratedColumn<int>(
      'projectID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _projectAreaMeta =
      const VerificationMeta('projectArea');
  @override
  late final GeneratedColumn<String> projectArea = GeneratedColumn<String>(
      'projectArea', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _projectUnitMeta =
      const VerificationMeta('projectUnit');
  @override
  late final GeneratedColumn<String> projectUnit = GeneratedColumn<String>(
      'projectUnit', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _projectSizeMeta =
      const VerificationMeta('projectSize');
  @override
  late final GeneratedColumn<int> projectSize = GeneratedColumn<int>(
      'projectSize', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _projectBedroomMeta =
      const VerificationMeta('projectBedroom');
  @override
  late final GeneratedColumn<int> projectBedroom = GeneratedColumn<int>(
      'projectBedroom', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _projectBathroomMeta =
      const VerificationMeta('projectBathroom');
  @override
  late final GeneratedColumn<int> projectBathroom = GeneratedColumn<int>(
      'projectBathroom', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _marketSegmentMeta =
      const VerificationMeta('marketSegment');
  @override
  late final GeneratedColumn<String> marketSegment = GeneratedColumn<String>(
      'marketSegment', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _themeNameMeta =
      const VerificationMeta('themeName');
  @override
  late final GeneratedColumn<String> themeName = GeneratedColumn<String>(
      'themeName', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _startDateTimeMeta =
      const VerificationMeta('startDateTime');
  @override
  late final GeneratedColumn<String> startDateTime = GeneratedColumn<String>(
      'startDateTime', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _targetDateTimeMeta =
      const VerificationMeta('targetDateTime');
  @override
  late final GeneratedColumn<String> targetDateTime = GeneratedColumn<String>(
      'targetDateTime', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _dueDateTimeMeta =
      const VerificationMeta('dueDateTime');
  @override
  late final GeneratedColumn<String> dueDateTime = GeneratedColumn<String>(
      'dueDateTime', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _projectStatusMeta =
      const VerificationMeta('projectStatus');
  @override
  late final GeneratedColumn<String> projectStatus = GeneratedColumn<String>(
      'projectStatus', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _projectCategoryMeta =
      const VerificationMeta('projectCategory');
  @override
  late final GeneratedColumn<String> projectCategory = GeneratedColumn<String>(
      'projectCategory', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _ownerNameMeta =
      const VerificationMeta('ownerName');
  @override
  late final GeneratedColumn<String> ownerName = GeneratedColumn<String>(
      'ownerName', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _propertyNameMeta =
      const VerificationMeta('propertyName');
  @override
  late final GeneratedColumn<String> propertyName = GeneratedColumn<String>(
      'propertyName', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _propertyStateMeta =
      const VerificationMeta('propertyState');
  @override
  late final GeneratedColumn<String> propertyState = GeneratedColumn<String>(
      'propertyState', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _propertyTypeMeta =
      const VerificationMeta('propertyType');
  @override
  late final GeneratedColumn<String> propertyType = GeneratedColumn<String>(
      'propertyType', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _isReadMeta = const VerificationMeta('isRead');
  @override
  late final GeneratedColumn<String> isRead = GeneratedColumn<String>(
      'isRead', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _lastViewedDateTimeMeta =
      const VerificationMeta('lastViewedDateTime');
  @override
  late final GeneratedColumn<String> lastViewedDateTime =
      GeneratedColumn<String>('lastViewedDateTime', aliasedName, true,
          type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _lastReadFeedIDMeta =
      const VerificationMeta('lastReadFeedID');
  @override
  late final GeneratedColumn<int> lastReadFeedID = GeneratedColumn<int>(
      'lastReadFeedID', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _updatedDateTimeMeta =
      const VerificationMeta('updatedDateTime');
  @override
  late final GeneratedColumn<String> updatedDateTime = GeneratedColumn<String>(
      'updatedDateTime', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _displayDateTimeMeta =
      const VerificationMeta('displayDateTime');
  @override
  late final GeneratedColumn<String> displayDateTime = GeneratedColumn<String>(
      'displayDateTime', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _completedTasksMeta =
      const VerificationMeta('completedTasks');
  @override
  late final GeneratedColumn<int> completedTasks = GeneratedColumn<int>(
      'completedTasks', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _totalTasksMeta =
      const VerificationMeta('totalTasks');
  @override
  late final GeneratedColumn<int> totalTasks = GeneratedColumn<int>(
      'totalTasks', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [
        projectID,
        projectArea,
        projectUnit,
        projectSize,
        projectBedroom,
        projectBathroom,
        marketSegment,
        themeName,
        startDateTime,
        targetDateTime,
        dueDateTime,
        projectStatus,
        projectCategory,
        ownerName,
        propertyName,
        propertyState,
        propertyType,
        isRead,
        lastViewedDateTime,
        lastReadFeedID,
        updatedDateTime,
        displayDateTime,
        completedTasks,
        totalTasks
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'workplaces';
  @override
  VerificationContext validateIntegrity(Insertable<Workplace> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('projectID')) {
      context.handle(_projectIDMeta,
          projectID.isAcceptableOrUnknown(data['projectID']!, _projectIDMeta));
    }
    if (data.containsKey('projectArea')) {
      context.handle(
          _projectAreaMeta,
          projectArea.isAcceptableOrUnknown(
              data['projectArea']!, _projectAreaMeta));
    }
    if (data.containsKey('projectUnit')) {
      context.handle(
          _projectUnitMeta,
          projectUnit.isAcceptableOrUnknown(
              data['projectUnit']!, _projectUnitMeta));
    }
    if (data.containsKey('projectSize')) {
      context.handle(
          _projectSizeMeta,
          projectSize.isAcceptableOrUnknown(
              data['projectSize']!, _projectSizeMeta));
    }
    if (data.containsKey('projectBedroom')) {
      context.handle(
          _projectBedroomMeta,
          projectBedroom.isAcceptableOrUnknown(
              data['projectBedroom']!, _projectBedroomMeta));
    }
    if (data.containsKey('projectBathroom')) {
      context.handle(
          _projectBathroomMeta,
          projectBathroom.isAcceptableOrUnknown(
              data['projectBathroom']!, _projectBathroomMeta));
    }
    if (data.containsKey('marketSegment')) {
      context.handle(
          _marketSegmentMeta,
          marketSegment.isAcceptableOrUnknown(
              data['marketSegment']!, _marketSegmentMeta));
    }
    if (data.containsKey('themeName')) {
      context.handle(_themeNameMeta,
          themeName.isAcceptableOrUnknown(data['themeName']!, _themeNameMeta));
    }
    if (data.containsKey('startDateTime')) {
      context.handle(
          _startDateTimeMeta,
          startDateTime.isAcceptableOrUnknown(
              data['startDateTime']!, _startDateTimeMeta));
    }
    if (data.containsKey('targetDateTime')) {
      context.handle(
          _targetDateTimeMeta,
          targetDateTime.isAcceptableOrUnknown(
              data['targetDateTime']!, _targetDateTimeMeta));
    }
    if (data.containsKey('dueDateTime')) {
      context.handle(
          _dueDateTimeMeta,
          dueDateTime.isAcceptableOrUnknown(
              data['dueDateTime']!, _dueDateTimeMeta));
    }
    if (data.containsKey('projectStatus')) {
      context.handle(
          _projectStatusMeta,
          projectStatus.isAcceptableOrUnknown(
              data['projectStatus']!, _projectStatusMeta));
    }
    if (data.containsKey('projectCategory')) {
      context.handle(
          _projectCategoryMeta,
          projectCategory.isAcceptableOrUnknown(
              data['projectCategory']!, _projectCategoryMeta));
    }
    if (data.containsKey('ownerName')) {
      context.handle(_ownerNameMeta,
          ownerName.isAcceptableOrUnknown(data['ownerName']!, _ownerNameMeta));
    }
    if (data.containsKey('propertyName')) {
      context.handle(
          _propertyNameMeta,
          propertyName.isAcceptableOrUnknown(
              data['propertyName']!, _propertyNameMeta));
    }
    if (data.containsKey('propertyState')) {
      context.handle(
          _propertyStateMeta,
          propertyState.isAcceptableOrUnknown(
              data['propertyState']!, _propertyStateMeta));
    }
    if (data.containsKey('propertyType')) {
      context.handle(
          _propertyTypeMeta,
          propertyType.isAcceptableOrUnknown(
              data['propertyType']!, _propertyTypeMeta));
    }
    if (data.containsKey('isRead')) {
      context.handle(_isReadMeta,
          isRead.isAcceptableOrUnknown(data['isRead']!, _isReadMeta));
    }
    if (data.containsKey('lastViewedDateTime')) {
      context.handle(
          _lastViewedDateTimeMeta,
          lastViewedDateTime.isAcceptableOrUnknown(
              data['lastViewedDateTime']!, _lastViewedDateTimeMeta));
    }
    if (data.containsKey('lastReadFeedID')) {
      context.handle(
          _lastReadFeedIDMeta,
          lastReadFeedID.isAcceptableOrUnknown(
              data['lastReadFeedID']!, _lastReadFeedIDMeta));
    }
    if (data.containsKey('updatedDateTime')) {
      context.handle(
          _updatedDateTimeMeta,
          updatedDateTime.isAcceptableOrUnknown(
              data['updatedDateTime']!, _updatedDateTimeMeta));
    }
    if (data.containsKey('displayDateTime')) {
      context.handle(
          _displayDateTimeMeta,
          displayDateTime.isAcceptableOrUnknown(
              data['displayDateTime']!, _displayDateTimeMeta));
    }
    if (data.containsKey('completedTasks')) {
      context.handle(
          _completedTasksMeta,
          completedTasks.isAcceptableOrUnknown(
              data['completedTasks']!, _completedTasksMeta));
    }
    if (data.containsKey('totalTasks')) {
      context.handle(
          _totalTasksMeta,
          totalTasks.isAcceptableOrUnknown(
              data['totalTasks']!, _totalTasksMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {projectID};
  @override
  Workplace map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Workplace(
      projectID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}projectID'])!,
      projectArea: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}projectArea']),
      projectUnit: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}projectUnit']),
      projectSize: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}projectSize']),
      projectBedroom: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}projectBedroom']),
      projectBathroom: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}projectBathroom']),
      marketSegment: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}marketSegment']),
      themeName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}themeName']),
      startDateTime: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}startDateTime']),
      targetDateTime: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}targetDateTime']),
      dueDateTime: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}dueDateTime']),
      projectStatus: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}projectStatus']),
      projectCategory: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}projectCategory']),
      ownerName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}ownerName']),
      propertyName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}propertyName']),
      propertyState: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}propertyState']),
      propertyType: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}propertyType']),
      isRead: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}isRead']),
      lastViewedDateTime: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}lastViewedDateTime']),
      lastReadFeedID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}lastReadFeedID']),
      updatedDateTime: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}updatedDateTime']),
      displayDateTime: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}displayDateTime']),
      completedTasks: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}completedTasks']),
      totalTasks: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}totalTasks']),
    );
  }

  @override
  $WorkplacesTable createAlias(String alias) {
    return $WorkplacesTable(attachedDatabase, alias);
  }
}

class Workplace extends DataClass implements Insertable<Workplace> {
  final int projectID;
  final String? projectArea;
  final String? projectUnit;
  final int? projectSize;
  final int? projectBedroom;
  final int? projectBathroom;
  final String? marketSegment;
  final String? themeName;
  final String? startDateTime;
  final String? targetDateTime;
  final String? dueDateTime;
  final String? projectStatus;
  final String? projectCategory;
  final String? ownerName;
  final String? propertyName;
  final String? propertyState;
  final String? propertyType;
  final String? isRead;
  final String? lastViewedDateTime;
  final int? lastReadFeedID;
  final String? updatedDateTime;
  final String? displayDateTime;
  final int? completedTasks;
  final int? totalTasks;
  const Workplace(
      {required this.projectID,
      this.projectArea,
      this.projectUnit,
      this.projectSize,
      this.projectBedroom,
      this.projectBathroom,
      this.marketSegment,
      this.themeName,
      this.startDateTime,
      this.targetDateTime,
      this.dueDateTime,
      this.projectStatus,
      this.projectCategory,
      this.ownerName,
      this.propertyName,
      this.propertyState,
      this.propertyType,
      this.isRead,
      this.lastViewedDateTime,
      this.lastReadFeedID,
      this.updatedDateTime,
      this.displayDateTime,
      this.completedTasks,
      this.totalTasks});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['projectID'] = Variable<int>(projectID);
    if (!nullToAbsent || projectArea != null) {
      map['projectArea'] = Variable<String>(projectArea);
    }
    if (!nullToAbsent || projectUnit != null) {
      map['projectUnit'] = Variable<String>(projectUnit);
    }
    if (!nullToAbsent || projectSize != null) {
      map['projectSize'] = Variable<int>(projectSize);
    }
    if (!nullToAbsent || projectBedroom != null) {
      map['projectBedroom'] = Variable<int>(projectBedroom);
    }
    if (!nullToAbsent || projectBathroom != null) {
      map['projectBathroom'] = Variable<int>(projectBathroom);
    }
    if (!nullToAbsent || marketSegment != null) {
      map['marketSegment'] = Variable<String>(marketSegment);
    }
    if (!nullToAbsent || themeName != null) {
      map['themeName'] = Variable<String>(themeName);
    }
    if (!nullToAbsent || startDateTime != null) {
      map['startDateTime'] = Variable<String>(startDateTime);
    }
    if (!nullToAbsent || targetDateTime != null) {
      map['targetDateTime'] = Variable<String>(targetDateTime);
    }
    if (!nullToAbsent || dueDateTime != null) {
      map['dueDateTime'] = Variable<String>(dueDateTime);
    }
    if (!nullToAbsent || projectStatus != null) {
      map['projectStatus'] = Variable<String>(projectStatus);
    }
    if (!nullToAbsent || projectCategory != null) {
      map['projectCategory'] = Variable<String>(projectCategory);
    }
    if (!nullToAbsent || ownerName != null) {
      map['ownerName'] = Variable<String>(ownerName);
    }
    if (!nullToAbsent || propertyName != null) {
      map['propertyName'] = Variable<String>(propertyName);
    }
    if (!nullToAbsent || propertyState != null) {
      map['propertyState'] = Variable<String>(propertyState);
    }
    if (!nullToAbsent || propertyType != null) {
      map['propertyType'] = Variable<String>(propertyType);
    }
    if (!nullToAbsent || isRead != null) {
      map['isRead'] = Variable<String>(isRead);
    }
    if (!nullToAbsent || lastViewedDateTime != null) {
      map['lastViewedDateTime'] = Variable<String>(lastViewedDateTime);
    }
    if (!nullToAbsent || lastReadFeedID != null) {
      map['lastReadFeedID'] = Variable<int>(lastReadFeedID);
    }
    if (!nullToAbsent || updatedDateTime != null) {
      map['updatedDateTime'] = Variable<String>(updatedDateTime);
    }
    if (!nullToAbsent || displayDateTime != null) {
      map['displayDateTime'] = Variable<String>(displayDateTime);
    }
    if (!nullToAbsent || completedTasks != null) {
      map['completedTasks'] = Variable<int>(completedTasks);
    }
    if (!nullToAbsent || totalTasks != null) {
      map['totalTasks'] = Variable<int>(totalTasks);
    }
    return map;
  }

  WorkplacesCompanion toCompanion(bool nullToAbsent) {
    return WorkplacesCompanion(
      projectID: Value(projectID),
      projectArea: projectArea == null && nullToAbsent
          ? const Value.absent()
          : Value(projectArea),
      projectUnit: projectUnit == null && nullToAbsent
          ? const Value.absent()
          : Value(projectUnit),
      projectSize: projectSize == null && nullToAbsent
          ? const Value.absent()
          : Value(projectSize),
      projectBedroom: projectBedroom == null && nullToAbsent
          ? const Value.absent()
          : Value(projectBedroom),
      projectBathroom: projectBathroom == null && nullToAbsent
          ? const Value.absent()
          : Value(projectBathroom),
      marketSegment: marketSegment == null && nullToAbsent
          ? const Value.absent()
          : Value(marketSegment),
      themeName: themeName == null && nullToAbsent
          ? const Value.absent()
          : Value(themeName),
      startDateTime: startDateTime == null && nullToAbsent
          ? const Value.absent()
          : Value(startDateTime),
      targetDateTime: targetDateTime == null && nullToAbsent
          ? const Value.absent()
          : Value(targetDateTime),
      dueDateTime: dueDateTime == null && nullToAbsent
          ? const Value.absent()
          : Value(dueDateTime),
      projectStatus: projectStatus == null && nullToAbsent
          ? const Value.absent()
          : Value(projectStatus),
      projectCategory: projectCategory == null && nullToAbsent
          ? const Value.absent()
          : Value(projectCategory),
      ownerName: ownerName == null && nullToAbsent
          ? const Value.absent()
          : Value(ownerName),
      propertyName: propertyName == null && nullToAbsent
          ? const Value.absent()
          : Value(propertyName),
      propertyState: propertyState == null && nullToAbsent
          ? const Value.absent()
          : Value(propertyState),
      propertyType: propertyType == null && nullToAbsent
          ? const Value.absent()
          : Value(propertyType),
      isRead:
          isRead == null && nullToAbsent ? const Value.absent() : Value(isRead),
      lastViewedDateTime: lastViewedDateTime == null && nullToAbsent
          ? const Value.absent()
          : Value(lastViewedDateTime),
      lastReadFeedID: lastReadFeedID == null && nullToAbsent
          ? const Value.absent()
          : Value(lastReadFeedID),
      updatedDateTime: updatedDateTime == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedDateTime),
      displayDateTime: displayDateTime == null && nullToAbsent
          ? const Value.absent()
          : Value(displayDateTime),
      completedTasks: completedTasks == null && nullToAbsent
          ? const Value.absent()
          : Value(completedTasks),
      totalTasks: totalTasks == null && nullToAbsent
          ? const Value.absent()
          : Value(totalTasks),
    );
  }

  factory Workplace.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Workplace(
      projectID: serializer.fromJson<int>(json['projectID']),
      projectArea: serializer.fromJson<String?>(json['projectArea']),
      projectUnit: serializer.fromJson<String?>(json['projectUnit']),
      projectSize: serializer.fromJson<int?>(json['projectSize']),
      projectBedroom: serializer.fromJson<int?>(json['projectBedroom']),
      projectBathroom: serializer.fromJson<int?>(json['projectBathroom']),
      marketSegment: serializer.fromJson<String?>(json['marketSegment']),
      themeName: serializer.fromJson<String?>(json['themeName']),
      startDateTime: serializer.fromJson<String?>(json['startDateTime']),
      targetDateTime: serializer.fromJson<String?>(json['targetDateTime']),
      dueDateTime: serializer.fromJson<String?>(json['dueDateTime']),
      projectStatus: serializer.fromJson<String?>(json['projectStatus']),
      projectCategory: serializer.fromJson<String?>(json['projectCategory']),
      ownerName: serializer.fromJson<String?>(json['ownerName']),
      propertyName: serializer.fromJson<String?>(json['propertyName']),
      propertyState: serializer.fromJson<String?>(json['propertyState']),
      propertyType: serializer.fromJson<String?>(json['propertyType']),
      isRead: serializer.fromJson<String?>(json['isRead']),
      lastViewedDateTime:
          serializer.fromJson<String?>(json['lastViewedDateTime']),
      lastReadFeedID: serializer.fromJson<int?>(json['lastReadFeedID']),
      updatedDateTime: serializer.fromJson<String?>(json['updatedDateTime']),
      displayDateTime: serializer.fromJson<String?>(json['displayDateTime']),
      completedTasks: serializer.fromJson<int?>(json['completedTasks']),
      totalTasks: serializer.fromJson<int?>(json['totalTasks']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'projectID': serializer.toJson<int>(projectID),
      'projectArea': serializer.toJson<String?>(projectArea),
      'projectUnit': serializer.toJson<String?>(projectUnit),
      'projectSize': serializer.toJson<int?>(projectSize),
      'projectBedroom': serializer.toJson<int?>(projectBedroom),
      'projectBathroom': serializer.toJson<int?>(projectBathroom),
      'marketSegment': serializer.toJson<String?>(marketSegment),
      'themeName': serializer.toJson<String?>(themeName),
      'startDateTime': serializer.toJson<String?>(startDateTime),
      'targetDateTime': serializer.toJson<String?>(targetDateTime),
      'dueDateTime': serializer.toJson<String?>(dueDateTime),
      'projectStatus': serializer.toJson<String?>(projectStatus),
      'projectCategory': serializer.toJson<String?>(projectCategory),
      'ownerName': serializer.toJson<String?>(ownerName),
      'propertyName': serializer.toJson<String?>(propertyName),
      'propertyState': serializer.toJson<String?>(propertyState),
      'propertyType': serializer.toJson<String?>(propertyType),
      'isRead': serializer.toJson<String?>(isRead),
      'lastViewedDateTime': serializer.toJson<String?>(lastViewedDateTime),
      'lastReadFeedID': serializer.toJson<int?>(lastReadFeedID),
      'updatedDateTime': serializer.toJson<String?>(updatedDateTime),
      'displayDateTime': serializer.toJson<String?>(displayDateTime),
      'completedTasks': serializer.toJson<int?>(completedTasks),
      'totalTasks': serializer.toJson<int?>(totalTasks),
    };
  }

  Workplace copyWith(
          {int? projectID,
          Value<String?> projectArea = const Value.absent(),
          Value<String?> projectUnit = const Value.absent(),
          Value<int?> projectSize = const Value.absent(),
          Value<int?> projectBedroom = const Value.absent(),
          Value<int?> projectBathroom = const Value.absent(),
          Value<String?> marketSegment = const Value.absent(),
          Value<String?> themeName = const Value.absent(),
          Value<String?> startDateTime = const Value.absent(),
          Value<String?> targetDateTime = const Value.absent(),
          Value<String?> dueDateTime = const Value.absent(),
          Value<String?> projectStatus = const Value.absent(),
          Value<String?> projectCategory = const Value.absent(),
          Value<String?> ownerName = const Value.absent(),
          Value<String?> propertyName = const Value.absent(),
          Value<String?> propertyState = const Value.absent(),
          Value<String?> propertyType = const Value.absent(),
          Value<String?> isRead = const Value.absent(),
          Value<String?> lastViewedDateTime = const Value.absent(),
          Value<int?> lastReadFeedID = const Value.absent(),
          Value<String?> updatedDateTime = const Value.absent(),
          Value<String?> displayDateTime = const Value.absent(),
          Value<int?> completedTasks = const Value.absent(),
          Value<int?> totalTasks = const Value.absent()}) =>
      Workplace(
        projectID: projectID ?? this.projectID,
        projectArea: projectArea.present ? projectArea.value : this.projectArea,
        projectUnit: projectUnit.present ? projectUnit.value : this.projectUnit,
        projectSize: projectSize.present ? projectSize.value : this.projectSize,
        projectBedroom:
            projectBedroom.present ? projectBedroom.value : this.projectBedroom,
        projectBathroom: projectBathroom.present
            ? projectBathroom.value
            : this.projectBathroom,
        marketSegment:
            marketSegment.present ? marketSegment.value : this.marketSegment,
        themeName: themeName.present ? themeName.value : this.themeName,
        startDateTime:
            startDateTime.present ? startDateTime.value : this.startDateTime,
        targetDateTime:
            targetDateTime.present ? targetDateTime.value : this.targetDateTime,
        dueDateTime: dueDateTime.present ? dueDateTime.value : this.dueDateTime,
        projectStatus:
            projectStatus.present ? projectStatus.value : this.projectStatus,
        projectCategory: projectCategory.present
            ? projectCategory.value
            : this.projectCategory,
        ownerName: ownerName.present ? ownerName.value : this.ownerName,
        propertyName:
            propertyName.present ? propertyName.value : this.propertyName,
        propertyState:
            propertyState.present ? propertyState.value : this.propertyState,
        propertyType:
            propertyType.present ? propertyType.value : this.propertyType,
        isRead: isRead.present ? isRead.value : this.isRead,
        lastViewedDateTime: lastViewedDateTime.present
            ? lastViewedDateTime.value
            : this.lastViewedDateTime,
        lastReadFeedID:
            lastReadFeedID.present ? lastReadFeedID.value : this.lastReadFeedID,
        updatedDateTime: updatedDateTime.present
            ? updatedDateTime.value
            : this.updatedDateTime,
        displayDateTime: displayDateTime.present
            ? displayDateTime.value
            : this.displayDateTime,
        completedTasks:
            completedTasks.present ? completedTasks.value : this.completedTasks,
        totalTasks: totalTasks.present ? totalTasks.value : this.totalTasks,
      );
  Workplace copyWithCompanion(WorkplacesCompanion data) {
    return Workplace(
      projectID: data.projectID.present ? data.projectID.value : this.projectID,
      projectArea:
          data.projectArea.present ? data.projectArea.value : this.projectArea,
      projectUnit:
          data.projectUnit.present ? data.projectUnit.value : this.projectUnit,
      projectSize:
          data.projectSize.present ? data.projectSize.value : this.projectSize,
      projectBedroom: data.projectBedroom.present
          ? data.projectBedroom.value
          : this.projectBedroom,
      projectBathroom: data.projectBathroom.present
          ? data.projectBathroom.value
          : this.projectBathroom,
      marketSegment: data.marketSegment.present
          ? data.marketSegment.value
          : this.marketSegment,
      themeName: data.themeName.present ? data.themeName.value : this.themeName,
      startDateTime: data.startDateTime.present
          ? data.startDateTime.value
          : this.startDateTime,
      targetDateTime: data.targetDateTime.present
          ? data.targetDateTime.value
          : this.targetDateTime,
      dueDateTime:
          data.dueDateTime.present ? data.dueDateTime.value : this.dueDateTime,
      projectStatus: data.projectStatus.present
          ? data.projectStatus.value
          : this.projectStatus,
      projectCategory: data.projectCategory.present
          ? data.projectCategory.value
          : this.projectCategory,
      ownerName: data.ownerName.present ? data.ownerName.value : this.ownerName,
      propertyName: data.propertyName.present
          ? data.propertyName.value
          : this.propertyName,
      propertyState: data.propertyState.present
          ? data.propertyState.value
          : this.propertyState,
      propertyType: data.propertyType.present
          ? data.propertyType.value
          : this.propertyType,
      isRead: data.isRead.present ? data.isRead.value : this.isRead,
      lastViewedDateTime: data.lastViewedDateTime.present
          ? data.lastViewedDateTime.value
          : this.lastViewedDateTime,
      lastReadFeedID: data.lastReadFeedID.present
          ? data.lastReadFeedID.value
          : this.lastReadFeedID,
      updatedDateTime: data.updatedDateTime.present
          ? data.updatedDateTime.value
          : this.updatedDateTime,
      displayDateTime: data.displayDateTime.present
          ? data.displayDateTime.value
          : this.displayDateTime,
      completedTasks: data.completedTasks.present
          ? data.completedTasks.value
          : this.completedTasks,
      totalTasks:
          data.totalTasks.present ? data.totalTasks.value : this.totalTasks,
    );
  }

  @override
  String toString() {
    return (StringBuffer('Workplace(')
          ..write('projectID: $projectID, ')
          ..write('projectArea: $projectArea, ')
          ..write('projectUnit: $projectUnit, ')
          ..write('projectSize: $projectSize, ')
          ..write('projectBedroom: $projectBedroom, ')
          ..write('projectBathroom: $projectBathroom, ')
          ..write('marketSegment: $marketSegment, ')
          ..write('themeName: $themeName, ')
          ..write('startDateTime: $startDateTime, ')
          ..write('targetDateTime: $targetDateTime, ')
          ..write('dueDateTime: $dueDateTime, ')
          ..write('projectStatus: $projectStatus, ')
          ..write('projectCategory: $projectCategory, ')
          ..write('ownerName: $ownerName, ')
          ..write('propertyName: $propertyName, ')
          ..write('propertyState: $propertyState, ')
          ..write('propertyType: $propertyType, ')
          ..write('isRead: $isRead, ')
          ..write('lastViewedDateTime: $lastViewedDateTime, ')
          ..write('lastReadFeedID: $lastReadFeedID, ')
          ..write('updatedDateTime: $updatedDateTime, ')
          ..write('displayDateTime: $displayDateTime, ')
          ..write('completedTasks: $completedTasks, ')
          ..write('totalTasks: $totalTasks')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hashAll([
        projectID,
        projectArea,
        projectUnit,
        projectSize,
        projectBedroom,
        projectBathroom,
        marketSegment,
        themeName,
        startDateTime,
        targetDateTime,
        dueDateTime,
        projectStatus,
        projectCategory,
        ownerName,
        propertyName,
        propertyState,
        propertyType,
        isRead,
        lastViewedDateTime,
        lastReadFeedID,
        updatedDateTime,
        displayDateTime,
        completedTasks,
        totalTasks
      ]);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Workplace &&
          other.projectID == this.projectID &&
          other.projectArea == this.projectArea &&
          other.projectUnit == this.projectUnit &&
          other.projectSize == this.projectSize &&
          other.projectBedroom == this.projectBedroom &&
          other.projectBathroom == this.projectBathroom &&
          other.marketSegment == this.marketSegment &&
          other.themeName == this.themeName &&
          other.startDateTime == this.startDateTime &&
          other.targetDateTime == this.targetDateTime &&
          other.dueDateTime == this.dueDateTime &&
          other.projectStatus == this.projectStatus &&
          other.projectCategory == this.projectCategory &&
          other.ownerName == this.ownerName &&
          other.propertyName == this.propertyName &&
          other.propertyState == this.propertyState &&
          other.propertyType == this.propertyType &&
          other.isRead == this.isRead &&
          other.lastViewedDateTime == this.lastViewedDateTime &&
          other.lastReadFeedID == this.lastReadFeedID &&
          other.updatedDateTime == this.updatedDateTime &&
          other.displayDateTime == this.displayDateTime &&
          other.completedTasks == this.completedTasks &&
          other.totalTasks == this.totalTasks);
}

class WorkplacesCompanion extends UpdateCompanion<Workplace> {
  final Value<int> projectID;
  final Value<String?> projectArea;
  final Value<String?> projectUnit;
  final Value<int?> projectSize;
  final Value<int?> projectBedroom;
  final Value<int?> projectBathroom;
  final Value<String?> marketSegment;
  final Value<String?> themeName;
  final Value<String?> startDateTime;
  final Value<String?> targetDateTime;
  final Value<String?> dueDateTime;
  final Value<String?> projectStatus;
  final Value<String?> projectCategory;
  final Value<String?> ownerName;
  final Value<String?> propertyName;
  final Value<String?> propertyState;
  final Value<String?> propertyType;
  final Value<String?> isRead;
  final Value<String?> lastViewedDateTime;
  final Value<int?> lastReadFeedID;
  final Value<String?> updatedDateTime;
  final Value<String?> displayDateTime;
  final Value<int?> completedTasks;
  final Value<int?> totalTasks;
  const WorkplacesCompanion({
    this.projectID = const Value.absent(),
    this.projectArea = const Value.absent(),
    this.projectUnit = const Value.absent(),
    this.projectSize = const Value.absent(),
    this.projectBedroom = const Value.absent(),
    this.projectBathroom = const Value.absent(),
    this.marketSegment = const Value.absent(),
    this.themeName = const Value.absent(),
    this.startDateTime = const Value.absent(),
    this.targetDateTime = const Value.absent(),
    this.dueDateTime = const Value.absent(),
    this.projectStatus = const Value.absent(),
    this.projectCategory = const Value.absent(),
    this.ownerName = const Value.absent(),
    this.propertyName = const Value.absent(),
    this.propertyState = const Value.absent(),
    this.propertyType = const Value.absent(),
    this.isRead = const Value.absent(),
    this.lastViewedDateTime = const Value.absent(),
    this.lastReadFeedID = const Value.absent(),
    this.updatedDateTime = const Value.absent(),
    this.displayDateTime = const Value.absent(),
    this.completedTasks = const Value.absent(),
    this.totalTasks = const Value.absent(),
  });
  WorkplacesCompanion.insert({
    this.projectID = const Value.absent(),
    this.projectArea = const Value.absent(),
    this.projectUnit = const Value.absent(),
    this.projectSize = const Value.absent(),
    this.projectBedroom = const Value.absent(),
    this.projectBathroom = const Value.absent(),
    this.marketSegment = const Value.absent(),
    this.themeName = const Value.absent(),
    this.startDateTime = const Value.absent(),
    this.targetDateTime = const Value.absent(),
    this.dueDateTime = const Value.absent(),
    this.projectStatus = const Value.absent(),
    this.projectCategory = const Value.absent(),
    this.ownerName = const Value.absent(),
    this.propertyName = const Value.absent(),
    this.propertyState = const Value.absent(),
    this.propertyType = const Value.absent(),
    this.isRead = const Value.absent(),
    this.lastViewedDateTime = const Value.absent(),
    this.lastReadFeedID = const Value.absent(),
    this.updatedDateTime = const Value.absent(),
    this.displayDateTime = const Value.absent(),
    this.completedTasks = const Value.absent(),
    this.totalTasks = const Value.absent(),
  });
  static Insertable<Workplace> custom({
    Expression<int>? projectID,
    Expression<String>? projectArea,
    Expression<String>? projectUnit,
    Expression<int>? projectSize,
    Expression<int>? projectBedroom,
    Expression<int>? projectBathroom,
    Expression<String>? marketSegment,
    Expression<String>? themeName,
    Expression<String>? startDateTime,
    Expression<String>? targetDateTime,
    Expression<String>? dueDateTime,
    Expression<String>? projectStatus,
    Expression<String>? projectCategory,
    Expression<String>? ownerName,
    Expression<String>? propertyName,
    Expression<String>? propertyState,
    Expression<String>? propertyType,
    Expression<String>? isRead,
    Expression<String>? lastViewedDateTime,
    Expression<int>? lastReadFeedID,
    Expression<String>? updatedDateTime,
    Expression<String>? displayDateTime,
    Expression<int>? completedTasks,
    Expression<int>? totalTasks,
  }) {
    return RawValuesInsertable({
      if (projectID != null) 'projectID': projectID,
      if (projectArea != null) 'projectArea': projectArea,
      if (projectUnit != null) 'projectUnit': projectUnit,
      if (projectSize != null) 'projectSize': projectSize,
      if (projectBedroom != null) 'projectBedroom': projectBedroom,
      if (projectBathroom != null) 'projectBathroom': projectBathroom,
      if (marketSegment != null) 'marketSegment': marketSegment,
      if (themeName != null) 'themeName': themeName,
      if (startDateTime != null) 'startDateTime': startDateTime,
      if (targetDateTime != null) 'targetDateTime': targetDateTime,
      if (dueDateTime != null) 'dueDateTime': dueDateTime,
      if (projectStatus != null) 'projectStatus': projectStatus,
      if (projectCategory != null) 'projectCategory': projectCategory,
      if (ownerName != null) 'ownerName': ownerName,
      if (propertyName != null) 'propertyName': propertyName,
      if (propertyState != null) 'propertyState': propertyState,
      if (propertyType != null) 'propertyType': propertyType,
      if (isRead != null) 'isRead': isRead,
      if (lastViewedDateTime != null) 'lastViewedDateTime': lastViewedDateTime,
      if (lastReadFeedID != null) 'lastReadFeedID': lastReadFeedID,
      if (updatedDateTime != null) 'updatedDateTime': updatedDateTime,
      if (displayDateTime != null) 'displayDateTime': displayDateTime,
      if (completedTasks != null) 'completedTasks': completedTasks,
      if (totalTasks != null) 'totalTasks': totalTasks,
    });
  }

  WorkplacesCompanion copyWith(
      {Value<int>? projectID,
      Value<String?>? projectArea,
      Value<String?>? projectUnit,
      Value<int?>? projectSize,
      Value<int?>? projectBedroom,
      Value<int?>? projectBathroom,
      Value<String?>? marketSegment,
      Value<String?>? themeName,
      Value<String?>? startDateTime,
      Value<String?>? targetDateTime,
      Value<String?>? dueDateTime,
      Value<String?>? projectStatus,
      Value<String?>? projectCategory,
      Value<String?>? ownerName,
      Value<String?>? propertyName,
      Value<String?>? propertyState,
      Value<String?>? propertyType,
      Value<String?>? isRead,
      Value<String?>? lastViewedDateTime,
      Value<int?>? lastReadFeedID,
      Value<String?>? updatedDateTime,
      Value<String?>? displayDateTime,
      Value<int?>? completedTasks,
      Value<int?>? totalTasks}) {
    return WorkplacesCompanion(
      projectID: projectID ?? this.projectID,
      projectArea: projectArea ?? this.projectArea,
      projectUnit: projectUnit ?? this.projectUnit,
      projectSize: projectSize ?? this.projectSize,
      projectBedroom: projectBedroom ?? this.projectBedroom,
      projectBathroom: projectBathroom ?? this.projectBathroom,
      marketSegment: marketSegment ?? this.marketSegment,
      themeName: themeName ?? this.themeName,
      startDateTime: startDateTime ?? this.startDateTime,
      targetDateTime: targetDateTime ?? this.targetDateTime,
      dueDateTime: dueDateTime ?? this.dueDateTime,
      projectStatus: projectStatus ?? this.projectStatus,
      projectCategory: projectCategory ?? this.projectCategory,
      ownerName: ownerName ?? this.ownerName,
      propertyName: propertyName ?? this.propertyName,
      propertyState: propertyState ?? this.propertyState,
      propertyType: propertyType ?? this.propertyType,
      isRead: isRead ?? this.isRead,
      lastViewedDateTime: lastViewedDateTime ?? this.lastViewedDateTime,
      lastReadFeedID: lastReadFeedID ?? this.lastReadFeedID,
      updatedDateTime: updatedDateTime ?? this.updatedDateTime,
      displayDateTime: displayDateTime ?? this.displayDateTime,
      completedTasks: completedTasks ?? this.completedTasks,
      totalTasks: totalTasks ?? this.totalTasks,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (projectID.present) {
      map['projectID'] = Variable<int>(projectID.value);
    }
    if (projectArea.present) {
      map['projectArea'] = Variable<String>(projectArea.value);
    }
    if (projectUnit.present) {
      map['projectUnit'] = Variable<String>(projectUnit.value);
    }
    if (projectSize.present) {
      map['projectSize'] = Variable<int>(projectSize.value);
    }
    if (projectBedroom.present) {
      map['projectBedroom'] = Variable<int>(projectBedroom.value);
    }
    if (projectBathroom.present) {
      map['projectBathroom'] = Variable<int>(projectBathroom.value);
    }
    if (marketSegment.present) {
      map['marketSegment'] = Variable<String>(marketSegment.value);
    }
    if (themeName.present) {
      map['themeName'] = Variable<String>(themeName.value);
    }
    if (startDateTime.present) {
      map['startDateTime'] = Variable<String>(startDateTime.value);
    }
    if (targetDateTime.present) {
      map['targetDateTime'] = Variable<String>(targetDateTime.value);
    }
    if (dueDateTime.present) {
      map['dueDateTime'] = Variable<String>(dueDateTime.value);
    }
    if (projectStatus.present) {
      map['projectStatus'] = Variable<String>(projectStatus.value);
    }
    if (projectCategory.present) {
      map['projectCategory'] = Variable<String>(projectCategory.value);
    }
    if (ownerName.present) {
      map['ownerName'] = Variable<String>(ownerName.value);
    }
    if (propertyName.present) {
      map['propertyName'] = Variable<String>(propertyName.value);
    }
    if (propertyState.present) {
      map['propertyState'] = Variable<String>(propertyState.value);
    }
    if (propertyType.present) {
      map['propertyType'] = Variable<String>(propertyType.value);
    }
    if (isRead.present) {
      map['isRead'] = Variable<String>(isRead.value);
    }
    if (lastViewedDateTime.present) {
      map['lastViewedDateTime'] = Variable<String>(lastViewedDateTime.value);
    }
    if (lastReadFeedID.present) {
      map['lastReadFeedID'] = Variable<int>(lastReadFeedID.value);
    }
    if (updatedDateTime.present) {
      map['updatedDateTime'] = Variable<String>(updatedDateTime.value);
    }
    if (displayDateTime.present) {
      map['displayDateTime'] = Variable<String>(displayDateTime.value);
    }
    if (completedTasks.present) {
      map['completedTasks'] = Variable<int>(completedTasks.value);
    }
    if (totalTasks.present) {
      map['totalTasks'] = Variable<int>(totalTasks.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('WorkplacesCompanion(')
          ..write('projectID: $projectID, ')
          ..write('projectArea: $projectArea, ')
          ..write('projectUnit: $projectUnit, ')
          ..write('projectSize: $projectSize, ')
          ..write('projectBedroom: $projectBedroom, ')
          ..write('projectBathroom: $projectBathroom, ')
          ..write('marketSegment: $marketSegment, ')
          ..write('themeName: $themeName, ')
          ..write('startDateTime: $startDateTime, ')
          ..write('targetDateTime: $targetDateTime, ')
          ..write('dueDateTime: $dueDateTime, ')
          ..write('projectStatus: $projectStatus, ')
          ..write('projectCategory: $projectCategory, ')
          ..write('ownerName: $ownerName, ')
          ..write('propertyName: $propertyName, ')
          ..write('propertyState: $propertyState, ')
          ..write('propertyType: $propertyType, ')
          ..write('isRead: $isRead, ')
          ..write('lastViewedDateTime: $lastViewedDateTime, ')
          ..write('lastReadFeedID: $lastReadFeedID, ')
          ..write('updatedDateTime: $updatedDateTime, ')
          ..write('displayDateTime: $displayDateTime, ')
          ..write('completedTasks: $completedTasks, ')
          ..write('totalTasks: $totalTasks')
          ..write(')'))
        .toString();
  }
}

class $TeamMembersTable extends TeamMembers
    with TableInfo<$TeamMembersTable, TeamMember> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $TeamMembersTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _teamMemberIDMeta =
      const VerificationMeta('teamMemberID');
  @override
  late final GeneratedColumn<int> teamMemberID = GeneratedColumn<int>(
      'teamMemberID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _teamMemberNameMeta =
      const VerificationMeta('teamMemberName');
  @override
  late final GeneratedColumn<String> teamMemberName = GeneratedColumn<String>(
      'teamMemberName', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _teamMemberAvatarMeta =
      const VerificationMeta('teamMemberAvatar');
  @override
  late final GeneratedColumn<String> teamMemberAvatar = GeneratedColumn<String>(
      'teamMemberAvatar', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _designationNameMeta =
      const VerificationMeta('designationName');
  @override
  late final GeneratedColumn<String> designationName = GeneratedColumn<String>(
      'designationName', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _projectIDMeta =
      const VerificationMeta('projectID');
  @override
  late final GeneratedColumn<int> projectID = GeneratedColumn<int>(
      'projectID', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL REFERENCES workplaces(projectID)');
  static const VerificationMeta _showMeta = const VerificationMeta('show');
  @override
  late final GeneratedColumn<bool> show = GeneratedColumn<bool>(
      'show', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("show" IN (0, 1))'),
      defaultValue: const Constant(false));
  @override
  List<GeneratedColumn> get $columns => [
        teamMemberID,
        teamMemberName,
        teamMemberAvatar,
        designationName,
        projectID,
        show
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'team_members';
  @override
  VerificationContext validateIntegrity(Insertable<TeamMember> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('teamMemberID')) {
      context.handle(
          _teamMemberIDMeta,
          teamMemberID.isAcceptableOrUnknown(
              data['teamMemberID']!, _teamMemberIDMeta));
    } else if (isInserting) {
      context.missing(_teamMemberIDMeta);
    }
    if (data.containsKey('teamMemberName')) {
      context.handle(
          _teamMemberNameMeta,
          teamMemberName.isAcceptableOrUnknown(
              data['teamMemberName']!, _teamMemberNameMeta));
    }
    if (data.containsKey('teamMemberAvatar')) {
      context.handle(
          _teamMemberAvatarMeta,
          teamMemberAvatar.isAcceptableOrUnknown(
              data['teamMemberAvatar']!, _teamMemberAvatarMeta));
    }
    if (data.containsKey('designationName')) {
      context.handle(
          _designationNameMeta,
          designationName.isAcceptableOrUnknown(
              data['designationName']!, _designationNameMeta));
    }
    if (data.containsKey('projectID')) {
      context.handle(_projectIDMeta,
          projectID.isAcceptableOrUnknown(data['projectID']!, _projectIDMeta));
    } else if (isInserting) {
      context.missing(_projectIDMeta);
    }
    if (data.containsKey('show')) {
      context.handle(
          _showMeta, show.isAcceptableOrUnknown(data['show']!, _showMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {projectID, teamMemberID};
  @override
  TeamMember map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return TeamMember(
      teamMemberID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}teamMemberID'])!,
      teamMemberName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}teamMemberName']),
      teamMemberAvatar: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}teamMemberAvatar']),
      designationName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}designationName']),
      projectID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}projectID'])!,
      show: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}show'])!,
    );
  }

  @override
  $TeamMembersTable createAlias(String alias) {
    return $TeamMembersTable(attachedDatabase, alias);
  }
}

class TeamMember extends DataClass implements Insertable<TeamMember> {
  final int teamMemberID;
  final String? teamMemberName;
  final String? teamMemberAvatar;
  final String? designationName;
  final int projectID;
  final bool show;
  const TeamMember(
      {required this.teamMemberID,
      this.teamMemberName,
      this.teamMemberAvatar,
      this.designationName,
      required this.projectID,
      required this.show});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['teamMemberID'] = Variable<int>(teamMemberID);
    if (!nullToAbsent || teamMemberName != null) {
      map['teamMemberName'] = Variable<String>(teamMemberName);
    }
    if (!nullToAbsent || teamMemberAvatar != null) {
      map['teamMemberAvatar'] = Variable<String>(teamMemberAvatar);
    }
    if (!nullToAbsent || designationName != null) {
      map['designationName'] = Variable<String>(designationName);
    }
    map['projectID'] = Variable<int>(projectID);
    map['show'] = Variable<bool>(show);
    return map;
  }

  TeamMembersCompanion toCompanion(bool nullToAbsent) {
    return TeamMembersCompanion(
      teamMemberID: Value(teamMemberID),
      teamMemberName: teamMemberName == null && nullToAbsent
          ? const Value.absent()
          : Value(teamMemberName),
      teamMemberAvatar: teamMemberAvatar == null && nullToAbsent
          ? const Value.absent()
          : Value(teamMemberAvatar),
      designationName: designationName == null && nullToAbsent
          ? const Value.absent()
          : Value(designationName),
      projectID: Value(projectID),
      show: Value(show),
    );
  }

  factory TeamMember.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return TeamMember(
      teamMemberID: serializer.fromJson<int>(json['teamMemberID']),
      teamMemberName: serializer.fromJson<String?>(json['teamMemberName']),
      teamMemberAvatar: serializer.fromJson<String?>(json['teamMemberAvatar']),
      designationName: serializer.fromJson<String?>(json['designationName']),
      projectID: serializer.fromJson<int>(json['projectID']),
      show: serializer.fromJson<bool>(json['show']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'teamMemberID': serializer.toJson<int>(teamMemberID),
      'teamMemberName': serializer.toJson<String?>(teamMemberName),
      'teamMemberAvatar': serializer.toJson<String?>(teamMemberAvatar),
      'designationName': serializer.toJson<String?>(designationName),
      'projectID': serializer.toJson<int>(projectID),
      'show': serializer.toJson<bool>(show),
    };
  }

  TeamMember copyWith(
          {int? teamMemberID,
          Value<String?> teamMemberName = const Value.absent(),
          Value<String?> teamMemberAvatar = const Value.absent(),
          Value<String?> designationName = const Value.absent(),
          int? projectID,
          bool? show}) =>
      TeamMember(
        teamMemberID: teamMemberID ?? this.teamMemberID,
        teamMemberName:
            teamMemberName.present ? teamMemberName.value : this.teamMemberName,
        teamMemberAvatar: teamMemberAvatar.present
            ? teamMemberAvatar.value
            : this.teamMemberAvatar,
        designationName: designationName.present
            ? designationName.value
            : this.designationName,
        projectID: projectID ?? this.projectID,
        show: show ?? this.show,
      );
  TeamMember copyWithCompanion(TeamMembersCompanion data) {
    return TeamMember(
      teamMemberID: data.teamMemberID.present
          ? data.teamMemberID.value
          : this.teamMemberID,
      teamMemberName: data.teamMemberName.present
          ? data.teamMemberName.value
          : this.teamMemberName,
      teamMemberAvatar: data.teamMemberAvatar.present
          ? data.teamMemberAvatar.value
          : this.teamMemberAvatar,
      designationName: data.designationName.present
          ? data.designationName.value
          : this.designationName,
      projectID: data.projectID.present ? data.projectID.value : this.projectID,
      show: data.show.present ? data.show.value : this.show,
    );
  }

  @override
  String toString() {
    return (StringBuffer('TeamMember(')
          ..write('teamMemberID: $teamMemberID, ')
          ..write('teamMemberName: $teamMemberName, ')
          ..write('teamMemberAvatar: $teamMemberAvatar, ')
          ..write('designationName: $designationName, ')
          ..write('projectID: $projectID, ')
          ..write('show: $show')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(teamMemberID, teamMemberName,
      teamMemberAvatar, designationName, projectID, show);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is TeamMember &&
          other.teamMemberID == this.teamMemberID &&
          other.teamMemberName == this.teamMemberName &&
          other.teamMemberAvatar == this.teamMemberAvatar &&
          other.designationName == this.designationName &&
          other.projectID == this.projectID &&
          other.show == this.show);
}

class TeamMembersCompanion extends UpdateCompanion<TeamMember> {
  final Value<int> teamMemberID;
  final Value<String?> teamMemberName;
  final Value<String?> teamMemberAvatar;
  final Value<String?> designationName;
  final Value<int> projectID;
  final Value<bool> show;
  final Value<int> rowid;
  const TeamMembersCompanion({
    this.teamMemberID = const Value.absent(),
    this.teamMemberName = const Value.absent(),
    this.teamMemberAvatar = const Value.absent(),
    this.designationName = const Value.absent(),
    this.projectID = const Value.absent(),
    this.show = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  TeamMembersCompanion.insert({
    required int teamMemberID,
    this.teamMemberName = const Value.absent(),
    this.teamMemberAvatar = const Value.absent(),
    this.designationName = const Value.absent(),
    required int projectID,
    this.show = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : teamMemberID = Value(teamMemberID),
        projectID = Value(projectID);
  static Insertable<TeamMember> custom({
    Expression<int>? teamMemberID,
    Expression<String>? teamMemberName,
    Expression<String>? teamMemberAvatar,
    Expression<String>? designationName,
    Expression<int>? projectID,
    Expression<bool>? show,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (teamMemberID != null) 'teamMemberID': teamMemberID,
      if (teamMemberName != null) 'teamMemberName': teamMemberName,
      if (teamMemberAvatar != null) 'teamMemberAvatar': teamMemberAvatar,
      if (designationName != null) 'designationName': designationName,
      if (projectID != null) 'projectID': projectID,
      if (show != null) 'show': show,
      if (rowid != null) 'rowid': rowid,
    });
  }

  TeamMembersCompanion copyWith(
      {Value<int>? teamMemberID,
      Value<String?>? teamMemberName,
      Value<String?>? teamMemberAvatar,
      Value<String?>? designationName,
      Value<int>? projectID,
      Value<bool>? show,
      Value<int>? rowid}) {
    return TeamMembersCompanion(
      teamMemberID: teamMemberID ?? this.teamMemberID,
      teamMemberName: teamMemberName ?? this.teamMemberName,
      teamMemberAvatar: teamMemberAvatar ?? this.teamMemberAvatar,
      designationName: designationName ?? this.designationName,
      projectID: projectID ?? this.projectID,
      show: show ?? this.show,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (teamMemberID.present) {
      map['teamMemberID'] = Variable<int>(teamMemberID.value);
    }
    if (teamMemberName.present) {
      map['teamMemberName'] = Variable<String>(teamMemberName.value);
    }
    if (teamMemberAvatar.present) {
      map['teamMemberAvatar'] = Variable<String>(teamMemberAvatar.value);
    }
    if (designationName.present) {
      map['designationName'] = Variable<String>(designationName.value);
    }
    if (projectID.present) {
      map['projectID'] = Variable<int>(projectID.value);
    }
    if (show.present) {
      map['show'] = Variable<bool>(show.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TeamMembersCompanion(')
          ..write('teamMemberID: $teamMemberID, ')
          ..write('teamMemberName: $teamMemberName, ')
          ..write('teamMemberAvatar: $teamMemberAvatar, ')
          ..write('designationName: $designationName, ')
          ..write('projectID: $projectID, ')
          ..write('show: $show, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class $TopicsTable extends Topics with TableInfo<$TopicsTable, Topic> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $TopicsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _topicIDMeta =
      const VerificationMeta('topicID');
  @override
  late final GeneratedColumn<int> topicID = GeneratedColumn<int>(
      'topicID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _topicNameMeta =
      const VerificationMeta('topicName');
  @override
  late final GeneratedColumn<String> topicName = GeneratedColumn<String>(
      'topicName', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _topicIconMeta =
      const VerificationMeta('topicIcon');
  @override
  late final GeneratedColumn<String> topicIcon = GeneratedColumn<String>(
      'topicIcon', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _topicColourMeta =
      const VerificationMeta('topicColour');
  @override
  late final GeneratedColumn<String> topicColour = GeneratedColumn<String>(
      'topicColour', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _weightMeta = const VerificationMeta('weight');
  @override
  late final GeneratedColumn<int> weight = GeneratedColumn<int>(
      'weight', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultValue: const Constant(0));
  @override
  List<GeneratedColumn> get $columns =>
      [topicID, topicName, topicIcon, topicColour, weight];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'topics';
  @override
  VerificationContext validateIntegrity(Insertable<Topic> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('topicID')) {
      context.handle(_topicIDMeta,
          topicID.isAcceptableOrUnknown(data['topicID']!, _topicIDMeta));
    }
    if (data.containsKey('topicName')) {
      context.handle(_topicNameMeta,
          topicName.isAcceptableOrUnknown(data['topicName']!, _topicNameMeta));
    }
    if (data.containsKey('topicIcon')) {
      context.handle(_topicIconMeta,
          topicIcon.isAcceptableOrUnknown(data['topicIcon']!, _topicIconMeta));
    }
    if (data.containsKey('topicColour')) {
      context.handle(
          _topicColourMeta,
          topicColour.isAcceptableOrUnknown(
              data['topicColour']!, _topicColourMeta));
    }
    if (data.containsKey('weight')) {
      context.handle(_weightMeta,
          weight.isAcceptableOrUnknown(data['weight']!, _weightMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {topicID};
  @override
  Topic map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Topic(
      topicID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}topicID'])!,
      topicName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}topicName']),
      topicIcon: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}topicIcon']),
      topicColour: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}topicColour']),
      weight: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}weight'])!,
    );
  }

  @override
  $TopicsTable createAlias(String alias) {
    return $TopicsTable(attachedDatabase, alias);
  }
}

class Topic extends DataClass implements Insertable<Topic> {
  final int topicID;
  final String? topicName;
  final String? topicIcon;
  final String? topicColour;
  final int weight;
  const Topic(
      {required this.topicID,
      this.topicName,
      this.topicIcon,
      this.topicColour,
      required this.weight});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['topicID'] = Variable<int>(topicID);
    if (!nullToAbsent || topicName != null) {
      map['topicName'] = Variable<String>(topicName);
    }
    if (!nullToAbsent || topicIcon != null) {
      map['topicIcon'] = Variable<String>(topicIcon);
    }
    if (!nullToAbsent || topicColour != null) {
      map['topicColour'] = Variable<String>(topicColour);
    }
    map['weight'] = Variable<int>(weight);
    return map;
  }

  TopicsCompanion toCompanion(bool nullToAbsent) {
    return TopicsCompanion(
      topicID: Value(topicID),
      topicName: topicName == null && nullToAbsent
          ? const Value.absent()
          : Value(topicName),
      topicIcon: topicIcon == null && nullToAbsent
          ? const Value.absent()
          : Value(topicIcon),
      topicColour: topicColour == null && nullToAbsent
          ? const Value.absent()
          : Value(topicColour),
      weight: Value(weight),
    );
  }

  factory Topic.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Topic(
      topicID: serializer.fromJson<int>(json['topicID']),
      topicName: serializer.fromJson<String?>(json['topicName']),
      topicIcon: serializer.fromJson<String?>(json['topicIcon']),
      topicColour: serializer.fromJson<String?>(json['topicColour']),
      weight: serializer.fromJson<int>(json['weight']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'topicID': serializer.toJson<int>(topicID),
      'topicName': serializer.toJson<String?>(topicName),
      'topicIcon': serializer.toJson<String?>(topicIcon),
      'topicColour': serializer.toJson<String?>(topicColour),
      'weight': serializer.toJson<int>(weight),
    };
  }

  Topic copyWith(
          {int? topicID,
          Value<String?> topicName = const Value.absent(),
          Value<String?> topicIcon = const Value.absent(),
          Value<String?> topicColour = const Value.absent(),
          int? weight}) =>
      Topic(
        topicID: topicID ?? this.topicID,
        topicName: topicName.present ? topicName.value : this.topicName,
        topicIcon: topicIcon.present ? topicIcon.value : this.topicIcon,
        topicColour: topicColour.present ? topicColour.value : this.topicColour,
        weight: weight ?? this.weight,
      );
  Topic copyWithCompanion(TopicsCompanion data) {
    return Topic(
      topicID: data.topicID.present ? data.topicID.value : this.topicID,
      topicName: data.topicName.present ? data.topicName.value : this.topicName,
      topicIcon: data.topicIcon.present ? data.topicIcon.value : this.topicIcon,
      topicColour:
          data.topicColour.present ? data.topicColour.value : this.topicColour,
      weight: data.weight.present ? data.weight.value : this.weight,
    );
  }

  @override
  String toString() {
    return (StringBuffer('Topic(')
          ..write('topicID: $topicID, ')
          ..write('topicName: $topicName, ')
          ..write('topicIcon: $topicIcon, ')
          ..write('topicColour: $topicColour, ')
          ..write('weight: $weight')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(topicID, topicName, topicIcon, topicColour, weight);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Topic &&
          other.topicID == this.topicID &&
          other.topicName == this.topicName &&
          other.topicIcon == this.topicIcon &&
          other.topicColour == this.topicColour &&
          other.weight == this.weight);
}

class TopicsCompanion extends UpdateCompanion<Topic> {
  final Value<int> topicID;
  final Value<String?> topicName;
  final Value<String?> topicIcon;
  final Value<String?> topicColour;
  final Value<int> weight;
  const TopicsCompanion({
    this.topicID = const Value.absent(),
    this.topicName = const Value.absent(),
    this.topicIcon = const Value.absent(),
    this.topicColour = const Value.absent(),
    this.weight = const Value.absent(),
  });
  TopicsCompanion.insert({
    this.topicID = const Value.absent(),
    this.topicName = const Value.absent(),
    this.topicIcon = const Value.absent(),
    this.topicColour = const Value.absent(),
    this.weight = const Value.absent(),
  });
  static Insertable<Topic> custom({
    Expression<int>? topicID,
    Expression<String>? topicName,
    Expression<String>? topicIcon,
    Expression<String>? topicColour,
    Expression<int>? weight,
  }) {
    return RawValuesInsertable({
      if (topicID != null) 'topicID': topicID,
      if (topicName != null) 'topicName': topicName,
      if (topicIcon != null) 'topicIcon': topicIcon,
      if (topicColour != null) 'topicColour': topicColour,
      if (weight != null) 'weight': weight,
    });
  }

  TopicsCompanion copyWith(
      {Value<int>? topicID,
      Value<String?>? topicName,
      Value<String?>? topicIcon,
      Value<String?>? topicColour,
      Value<int>? weight}) {
    return TopicsCompanion(
      topicID: topicID ?? this.topicID,
      topicName: topicName ?? this.topicName,
      topicIcon: topicIcon ?? this.topicIcon,
      topicColour: topicColour ?? this.topicColour,
      weight: weight ?? this.weight,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (topicID.present) {
      map['topicID'] = Variable<int>(topicID.value);
    }
    if (topicName.present) {
      map['topicName'] = Variable<String>(topicName.value);
    }
    if (topicIcon.present) {
      map['topicIcon'] = Variable<String>(topicIcon.value);
    }
    if (topicColour.present) {
      map['topicColour'] = Variable<String>(topicColour.value);
    }
    if (weight.present) {
      map['weight'] = Variable<int>(weight.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TopicsCompanion(')
          ..write('topicID: $topicID, ')
          ..write('topicName: $topicName, ')
          ..write('topicIcon: $topicIcon, ')
          ..write('topicColour: $topicColour, ')
          ..write('weight: $weight')
          ..write(')'))
        .toString();
  }
}

class $FeedsTable extends Feeds with TableInfo<$FeedsTable, Feed> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $FeedsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  @override
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _feedIDMeta = const VerificationMeta('feedID');
  @override
  late final GeneratedColumn<int> feedID = GeneratedColumn<int>(
      'feedID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _parentIDMeta =
      const VerificationMeta('parentID');
  @override
  late final GeneratedColumn<int> parentID = GeneratedColumn<int>(
      'parentID', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _projectIDMeta =
      const VerificationMeta('projectID');
  @override
  late final GeneratedColumn<int> projectID = GeneratedColumn<int>(
      'projectID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  @override
  late final GeneratedColumn<String> description = GeneratedColumn<String>(
      'description', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _originDescMeta =
      const VerificationMeta('originDesc');
  @override
  late final GeneratedColumn<String> originDesc = GeneratedColumn<String>(
      'originDesc', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _feedTypeMeta =
      const VerificationMeta('feedType');
  @override
  late final GeneratedColumn<String> feedType = GeneratedColumn<String>(
      'feedType', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _postDateTimeMeta =
      const VerificationMeta('postDateTime');
  @override
  late final GeneratedColumn<String> postDateTime = GeneratedColumn<String>(
      'postDateTime', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _updatedDateTimeMeta =
      const VerificationMeta('updatedDateTime');
  @override
  late final GeneratedColumn<String> updatedDateTime = GeneratedColumn<String>(
      'updatedDateTime', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _topicIDMeta =
      const VerificationMeta('topicID');
  @override
  late final GeneratedColumn<int> topicID = GeneratedColumn<int>(
      'topicID', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _teamMemberIDMeta =
      const VerificationMeta('teamMemberID');
  @override
  late final GeneratedColumn<int> teamMemberID = GeneratedColumn<int>(
      'teamMemberID', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _moduleIDMeta =
      const VerificationMeta('moduleID');
  @override
  late final GeneratedColumn<String> moduleID = GeneratedColumn<String>(
      'moduleID', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _moduleNameMeta =
      const VerificationMeta('moduleName');
  @override
  late final GeneratedColumn<String> moduleName = GeneratedColumn<String>(
      'moduleName', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _updatesDescriptionMeta =
      const VerificationMeta('updatesDescription');
  @override
  late final GeneratedColumn<String> updatesDescription =
      GeneratedColumn<String>('updatesDescription', aliasedName, true,
          type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _replyTeamMemberAvatarMeta =
      const VerificationMeta('replyTeamMemberAvatar');
  @override
  late final GeneratedColumn<String> replyTeamMemberAvatar =
      GeneratedColumn<String>('replyTeamMemberAvatar', aliasedName, true,
          type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _replyCountMeta =
      const VerificationMeta('replyCount');
  @override
  late final GeneratedColumn<int> replyCount = GeneratedColumn<int>(
      'replyCount', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultValue: const Constant(0));
  static const VerificationMeta _replyReadStatusMeta =
      const VerificationMeta('replyReadStatus');
  @override
  late final GeneratedColumn<String> replyReadStatus = GeneratedColumn<String>(
      'replyReadStatus', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _backgroundColorMeta =
      const VerificationMeta('backgroundColor');
  @override
  late final GeneratedColumn<String> backgroundColor = GeneratedColumn<String>(
      'backgroundColor', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _outlineColorMeta =
      const VerificationMeta('outlineColor');
  @override
  late final GeneratedColumn<String> outlineColor = GeneratedColumn<String>(
      'outlineColor', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _fontNameColorMeta =
      const VerificationMeta('fontNameColor');
  @override
  late final GeneratedColumn<String> fontNameColor = GeneratedColumn<String>(
      'fontNameColor', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _fontPositionColorMeta =
      const VerificationMeta('fontPositionColor');
  @override
  late final GeneratedColumn<String> fontPositionColor =
      GeneratedColumn<String>('fontPositionColor', aliasedName, true,
          type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _fontDescriptionColorMeta =
      const VerificationMeta('fontDescriptionColor');
  @override
  late final GeneratedColumn<String> fontDescriptionColor =
      GeneratedColumn<String>('fontDescriptionColor', aliasedName, true,
          type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _fontDateColorMeta =
      const VerificationMeta('fontDateColor');
  @override
  late final GeneratedColumn<String> fontDateColor = GeneratedColumn<String>(
      'fontDateColor', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _lastReadFeedIDMeta =
      const VerificationMeta('lastReadFeedID');
  @override
  late final GeneratedColumn<int> lastReadFeedID = GeneratedColumn<int>(
      'lastReadFeedID', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _sendStatusMeta =
      const VerificationMeta('sendStatus');
  @override
  late final GeneratedColumn<int> sendStatus = GeneratedColumn<int>(
      'sendStatus', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _isReadMeta = const VerificationMeta('isRead');
  @override
  late final GeneratedColumn<String> isRead = GeneratedColumn<String>(
      'isRead', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultValue: const Constant("unread"));
  @override
  List<GeneratedColumn> get $columns => [
        uuid,
        feedID,
        parentID,
        projectID,
        description,
        originDesc,
        feedType,
        postDateTime,
        updatedDateTime,
        topicID,
        teamMemberID,
        moduleID,
        moduleName,
        updatesDescription,
        replyTeamMemberAvatar,
        replyCount,
        replyReadStatus,
        backgroundColor,
        outlineColor,
        fontNameColor,
        fontPositionColor,
        fontDescriptionColor,
        fontDateColor,
        lastReadFeedID,
        sendStatus,
        isRead
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'feeds';
  @override
  VerificationContext validateIntegrity(Insertable<Feed> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('uuid')) {
      context.handle(
          _uuidMeta, uuid.isAcceptableOrUnknown(data['uuid']!, _uuidMeta));
    }
    if (data.containsKey('feedID')) {
      context.handle(_feedIDMeta,
          feedID.isAcceptableOrUnknown(data['feedID']!, _feedIDMeta));
    }
    if (data.containsKey('parentID')) {
      context.handle(_parentIDMeta,
          parentID.isAcceptableOrUnknown(data['parentID']!, _parentIDMeta));
    }
    if (data.containsKey('projectID')) {
      context.handle(_projectIDMeta,
          projectID.isAcceptableOrUnknown(data['projectID']!, _projectIDMeta));
    } else if (isInserting) {
      context.missing(_projectIDMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    if (data.containsKey('originDesc')) {
      context.handle(
          _originDescMeta,
          originDesc.isAcceptableOrUnknown(
              data['originDesc']!, _originDescMeta));
    }
    if (data.containsKey('feedType')) {
      context.handle(_feedTypeMeta,
          feedType.isAcceptableOrUnknown(data['feedType']!, _feedTypeMeta));
    }
    if (data.containsKey('postDateTime')) {
      context.handle(
          _postDateTimeMeta,
          postDateTime.isAcceptableOrUnknown(
              data['postDateTime']!, _postDateTimeMeta));
    }
    if (data.containsKey('updatedDateTime')) {
      context.handle(
          _updatedDateTimeMeta,
          updatedDateTime.isAcceptableOrUnknown(
              data['updatedDateTime']!, _updatedDateTimeMeta));
    }
    if (data.containsKey('topicID')) {
      context.handle(_topicIDMeta,
          topicID.isAcceptableOrUnknown(data['topicID']!, _topicIDMeta));
    }
    if (data.containsKey('teamMemberID')) {
      context.handle(
          _teamMemberIDMeta,
          teamMemberID.isAcceptableOrUnknown(
              data['teamMemberID']!, _teamMemberIDMeta));
    }
    if (data.containsKey('moduleID')) {
      context.handle(_moduleIDMeta,
          moduleID.isAcceptableOrUnknown(data['moduleID']!, _moduleIDMeta));
    }
    if (data.containsKey('moduleName')) {
      context.handle(
          _moduleNameMeta,
          moduleName.isAcceptableOrUnknown(
              data['moduleName']!, _moduleNameMeta));
    }
    if (data.containsKey('updatesDescription')) {
      context.handle(
          _updatesDescriptionMeta,
          updatesDescription.isAcceptableOrUnknown(
              data['updatesDescription']!, _updatesDescriptionMeta));
    }
    if (data.containsKey('replyTeamMemberAvatar')) {
      context.handle(
          _replyTeamMemberAvatarMeta,
          replyTeamMemberAvatar.isAcceptableOrUnknown(
              data['replyTeamMemberAvatar']!, _replyTeamMemberAvatarMeta));
    }
    if (data.containsKey('replyCount')) {
      context.handle(
          _replyCountMeta,
          replyCount.isAcceptableOrUnknown(
              data['replyCount']!, _replyCountMeta));
    }
    if (data.containsKey('replyReadStatus')) {
      context.handle(
          _replyReadStatusMeta,
          replyReadStatus.isAcceptableOrUnknown(
              data['replyReadStatus']!, _replyReadStatusMeta));
    }
    if (data.containsKey('backgroundColor')) {
      context.handle(
          _backgroundColorMeta,
          backgroundColor.isAcceptableOrUnknown(
              data['backgroundColor']!, _backgroundColorMeta));
    }
    if (data.containsKey('outlineColor')) {
      context.handle(
          _outlineColorMeta,
          outlineColor.isAcceptableOrUnknown(
              data['outlineColor']!, _outlineColorMeta));
    }
    if (data.containsKey('fontNameColor')) {
      context.handle(
          _fontNameColorMeta,
          fontNameColor.isAcceptableOrUnknown(
              data['fontNameColor']!, _fontNameColorMeta));
    }
    if (data.containsKey('fontPositionColor')) {
      context.handle(
          _fontPositionColorMeta,
          fontPositionColor.isAcceptableOrUnknown(
              data['fontPositionColor']!, _fontPositionColorMeta));
    }
    if (data.containsKey('fontDescriptionColor')) {
      context.handle(
          _fontDescriptionColorMeta,
          fontDescriptionColor.isAcceptableOrUnknown(
              data['fontDescriptionColor']!, _fontDescriptionColorMeta));
    }
    if (data.containsKey('fontDateColor')) {
      context.handle(
          _fontDateColorMeta,
          fontDateColor.isAcceptableOrUnknown(
              data['fontDateColor']!, _fontDateColorMeta));
    }
    if (data.containsKey('lastReadFeedID')) {
      context.handle(
          _lastReadFeedIDMeta,
          lastReadFeedID.isAcceptableOrUnknown(
              data['lastReadFeedID']!, _lastReadFeedIDMeta));
    }
    if (data.containsKey('sendStatus')) {
      context.handle(
          _sendStatusMeta,
          sendStatus.isAcceptableOrUnknown(
              data['sendStatus']!, _sendStatusMeta));
    } else if (isInserting) {
      context.missing(_sendStatusMeta);
    }
    if (data.containsKey('isRead')) {
      context.handle(_isReadMeta,
          isRead.isAcceptableOrUnknown(data['isRead']!, _isReadMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {feedID};
  @override
  Feed map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Feed(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid']),
      feedID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}feedID'])!,
      parentID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}parentID']),
      projectID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}projectID'])!,
      description: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}description']),
      originDesc: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}originDesc']),
      feedType: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}feedType']),
      postDateTime: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}postDateTime']),
      updatedDateTime: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}updatedDateTime']),
      topicID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}topicID']),
      teamMemberID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}teamMemberID']),
      moduleID: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}moduleID']),
      moduleName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}moduleName']),
      updatesDescription: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}updatesDescription']),
      replyTeamMemberAvatar: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}replyTeamMemberAvatar']),
      replyCount: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}replyCount'])!,
      replyReadStatus: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}replyReadStatus']),
      backgroundColor: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}backgroundColor']),
      outlineColor: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}outlineColor']),
      fontNameColor: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}fontNameColor']),
      fontPositionColor: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}fontPositionColor']),
      fontDescriptionColor: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}fontDescriptionColor']),
      fontDateColor: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}fontDateColor']),
      lastReadFeedID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}lastReadFeedID']),
      sendStatus: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}sendStatus'])!,
      isRead: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}isRead'])!,
    );
  }

  @override
  $FeedsTable createAlias(String alias) {
    return $FeedsTable(attachedDatabase, alias);
  }
}

class Feed extends DataClass implements Insertable<Feed> {
  final String? uuid;
  final int feedID;
  final int? parentID;
  final int projectID;
  final String? description;
  final String? originDesc;
  final String? feedType;
  final String? postDateTime;
  final String? updatedDateTime;
  final int? topicID;
  final int? teamMemberID;
  final String? moduleID;
  final String? moduleName;
  final String? updatesDescription;
  final String? replyTeamMemberAvatar;
  final int replyCount;
  final String? replyReadStatus;
  final String? backgroundColor;
  final String? outlineColor;
  final String? fontNameColor;
  final String? fontPositionColor;
  final String? fontDescriptionColor;
  final String? fontDateColor;
  final int? lastReadFeedID;
  final int sendStatus;
  final String isRead;
  const Feed(
      {this.uuid,
      required this.feedID,
      this.parentID,
      required this.projectID,
      this.description,
      this.originDesc,
      this.feedType,
      this.postDateTime,
      this.updatedDateTime,
      this.topicID,
      this.teamMemberID,
      this.moduleID,
      this.moduleName,
      this.updatesDescription,
      this.replyTeamMemberAvatar,
      required this.replyCount,
      this.replyReadStatus,
      this.backgroundColor,
      this.outlineColor,
      this.fontNameColor,
      this.fontPositionColor,
      this.fontDescriptionColor,
      this.fontDateColor,
      this.lastReadFeedID,
      required this.sendStatus,
      required this.isRead});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || uuid != null) {
      map['uuid'] = Variable<String>(uuid);
    }
    map['feedID'] = Variable<int>(feedID);
    if (!nullToAbsent || parentID != null) {
      map['parentID'] = Variable<int>(parentID);
    }
    map['projectID'] = Variable<int>(projectID);
    if (!nullToAbsent || description != null) {
      map['description'] = Variable<String>(description);
    }
    if (!nullToAbsent || originDesc != null) {
      map['originDesc'] = Variable<String>(originDesc);
    }
    if (!nullToAbsent || feedType != null) {
      map['feedType'] = Variable<String>(feedType);
    }
    if (!nullToAbsent || postDateTime != null) {
      map['postDateTime'] = Variable<String>(postDateTime);
    }
    if (!nullToAbsent || updatedDateTime != null) {
      map['updatedDateTime'] = Variable<String>(updatedDateTime);
    }
    if (!nullToAbsent || topicID != null) {
      map['topicID'] = Variable<int>(topicID);
    }
    if (!nullToAbsent || teamMemberID != null) {
      map['teamMemberID'] = Variable<int>(teamMemberID);
    }
    if (!nullToAbsent || moduleID != null) {
      map['moduleID'] = Variable<String>(moduleID);
    }
    if (!nullToAbsent || moduleName != null) {
      map['moduleName'] = Variable<String>(moduleName);
    }
    if (!nullToAbsent || updatesDescription != null) {
      map['updatesDescription'] = Variable<String>(updatesDescription);
    }
    if (!nullToAbsent || replyTeamMemberAvatar != null) {
      map['replyTeamMemberAvatar'] = Variable<String>(replyTeamMemberAvatar);
    }
    map['replyCount'] = Variable<int>(replyCount);
    if (!nullToAbsent || replyReadStatus != null) {
      map['replyReadStatus'] = Variable<String>(replyReadStatus);
    }
    if (!nullToAbsent || backgroundColor != null) {
      map['backgroundColor'] = Variable<String>(backgroundColor);
    }
    if (!nullToAbsent || outlineColor != null) {
      map['outlineColor'] = Variable<String>(outlineColor);
    }
    if (!nullToAbsent || fontNameColor != null) {
      map['fontNameColor'] = Variable<String>(fontNameColor);
    }
    if (!nullToAbsent || fontPositionColor != null) {
      map['fontPositionColor'] = Variable<String>(fontPositionColor);
    }
    if (!nullToAbsent || fontDescriptionColor != null) {
      map['fontDescriptionColor'] = Variable<String>(fontDescriptionColor);
    }
    if (!nullToAbsent || fontDateColor != null) {
      map['fontDateColor'] = Variable<String>(fontDateColor);
    }
    if (!nullToAbsent || lastReadFeedID != null) {
      map['lastReadFeedID'] = Variable<int>(lastReadFeedID);
    }
    map['sendStatus'] = Variable<int>(sendStatus);
    map['isRead'] = Variable<String>(isRead);
    return map;
  }

  FeedsCompanion toCompanion(bool nullToAbsent) {
    return FeedsCompanion(
      uuid: uuid == null && nullToAbsent ? const Value.absent() : Value(uuid),
      feedID: Value(feedID),
      parentID: parentID == null && nullToAbsent
          ? const Value.absent()
          : Value(parentID),
      projectID: Value(projectID),
      description: description == null && nullToAbsent
          ? const Value.absent()
          : Value(description),
      originDesc: originDesc == null && nullToAbsent
          ? const Value.absent()
          : Value(originDesc),
      feedType: feedType == null && nullToAbsent
          ? const Value.absent()
          : Value(feedType),
      postDateTime: postDateTime == null && nullToAbsent
          ? const Value.absent()
          : Value(postDateTime),
      updatedDateTime: updatedDateTime == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedDateTime),
      topicID: topicID == null && nullToAbsent
          ? const Value.absent()
          : Value(topicID),
      teamMemberID: teamMemberID == null && nullToAbsent
          ? const Value.absent()
          : Value(teamMemberID),
      moduleID: moduleID == null && nullToAbsent
          ? const Value.absent()
          : Value(moduleID),
      moduleName: moduleName == null && nullToAbsent
          ? const Value.absent()
          : Value(moduleName),
      updatesDescription: updatesDescription == null && nullToAbsent
          ? const Value.absent()
          : Value(updatesDescription),
      replyTeamMemberAvatar: replyTeamMemberAvatar == null && nullToAbsent
          ? const Value.absent()
          : Value(replyTeamMemberAvatar),
      replyCount: Value(replyCount),
      replyReadStatus: replyReadStatus == null && nullToAbsent
          ? const Value.absent()
          : Value(replyReadStatus),
      backgroundColor: backgroundColor == null && nullToAbsent
          ? const Value.absent()
          : Value(backgroundColor),
      outlineColor: outlineColor == null && nullToAbsent
          ? const Value.absent()
          : Value(outlineColor),
      fontNameColor: fontNameColor == null && nullToAbsent
          ? const Value.absent()
          : Value(fontNameColor),
      fontPositionColor: fontPositionColor == null && nullToAbsent
          ? const Value.absent()
          : Value(fontPositionColor),
      fontDescriptionColor: fontDescriptionColor == null && nullToAbsent
          ? const Value.absent()
          : Value(fontDescriptionColor),
      fontDateColor: fontDateColor == null && nullToAbsent
          ? const Value.absent()
          : Value(fontDateColor),
      lastReadFeedID: lastReadFeedID == null && nullToAbsent
          ? const Value.absent()
          : Value(lastReadFeedID),
      sendStatus: Value(sendStatus),
      isRead: Value(isRead),
    );
  }

  factory Feed.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Feed(
      uuid: serializer.fromJson<String?>(json['uuid']),
      feedID: serializer.fromJson<int>(json['feedID']),
      parentID: serializer.fromJson<int?>(json['parentID']),
      projectID: serializer.fromJson<int>(json['projectID']),
      description: serializer.fromJson<String?>(json['description']),
      originDesc: serializer.fromJson<String?>(json['originDesc']),
      feedType: serializer.fromJson<String?>(json['feedType']),
      postDateTime: serializer.fromJson<String?>(json['postDateTime']),
      updatedDateTime: serializer.fromJson<String?>(json['updatedDateTime']),
      topicID: serializer.fromJson<int?>(json['topicID']),
      teamMemberID: serializer.fromJson<int?>(json['teamMemberID']),
      moduleID: serializer.fromJson<String?>(json['moduleID']),
      moduleName: serializer.fromJson<String?>(json['moduleName']),
      updatesDescription:
          serializer.fromJson<String?>(json['updatesDescription']),
      replyTeamMemberAvatar:
          serializer.fromJson<String?>(json['replyTeamMemberAvatar']),
      replyCount: serializer.fromJson<int>(json['replyCount']),
      replyReadStatus: serializer.fromJson<String?>(json['replyReadStatus']),
      backgroundColor: serializer.fromJson<String?>(json['backgroundColor']),
      outlineColor: serializer.fromJson<String?>(json['outlineColor']),
      fontNameColor: serializer.fromJson<String?>(json['fontNameColor']),
      fontPositionColor:
          serializer.fromJson<String?>(json['fontPositionColor']),
      fontDescriptionColor:
          serializer.fromJson<String?>(json['fontDescriptionColor']),
      fontDateColor: serializer.fromJson<String?>(json['fontDateColor']),
      lastReadFeedID: serializer.fromJson<int?>(json['lastReadFeedID']),
      sendStatus: serializer.fromJson<int>(json['sendStatus']),
      isRead: serializer.fromJson<String>(json['isRead']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String?>(uuid),
      'feedID': serializer.toJson<int>(feedID),
      'parentID': serializer.toJson<int?>(parentID),
      'projectID': serializer.toJson<int>(projectID),
      'description': serializer.toJson<String?>(description),
      'originDesc': serializer.toJson<String?>(originDesc),
      'feedType': serializer.toJson<String?>(feedType),
      'postDateTime': serializer.toJson<String?>(postDateTime),
      'updatedDateTime': serializer.toJson<String?>(updatedDateTime),
      'topicID': serializer.toJson<int?>(topicID),
      'teamMemberID': serializer.toJson<int?>(teamMemberID),
      'moduleID': serializer.toJson<String?>(moduleID),
      'moduleName': serializer.toJson<String?>(moduleName),
      'updatesDescription': serializer.toJson<String?>(updatesDescription),
      'replyTeamMemberAvatar':
          serializer.toJson<String?>(replyTeamMemberAvatar),
      'replyCount': serializer.toJson<int>(replyCount),
      'replyReadStatus': serializer.toJson<String?>(replyReadStatus),
      'backgroundColor': serializer.toJson<String?>(backgroundColor),
      'outlineColor': serializer.toJson<String?>(outlineColor),
      'fontNameColor': serializer.toJson<String?>(fontNameColor),
      'fontPositionColor': serializer.toJson<String?>(fontPositionColor),
      'fontDescriptionColor': serializer.toJson<String?>(fontDescriptionColor),
      'fontDateColor': serializer.toJson<String?>(fontDateColor),
      'lastReadFeedID': serializer.toJson<int?>(lastReadFeedID),
      'sendStatus': serializer.toJson<int>(sendStatus),
      'isRead': serializer.toJson<String>(isRead),
    };
  }

  Feed copyWith(
          {Value<String?> uuid = const Value.absent(),
          int? feedID,
          Value<int?> parentID = const Value.absent(),
          int? projectID,
          Value<String?> description = const Value.absent(),
          Value<String?> originDesc = const Value.absent(),
          Value<String?> feedType = const Value.absent(),
          Value<String?> postDateTime = const Value.absent(),
          Value<String?> updatedDateTime = const Value.absent(),
          Value<int?> topicID = const Value.absent(),
          Value<int?> teamMemberID = const Value.absent(),
          Value<String?> moduleID = const Value.absent(),
          Value<String?> moduleName = const Value.absent(),
          Value<String?> updatesDescription = const Value.absent(),
          Value<String?> replyTeamMemberAvatar = const Value.absent(),
          int? replyCount,
          Value<String?> replyReadStatus = const Value.absent(),
          Value<String?> backgroundColor = const Value.absent(),
          Value<String?> outlineColor = const Value.absent(),
          Value<String?> fontNameColor = const Value.absent(),
          Value<String?> fontPositionColor = const Value.absent(),
          Value<String?> fontDescriptionColor = const Value.absent(),
          Value<String?> fontDateColor = const Value.absent(),
          Value<int?> lastReadFeedID = const Value.absent(),
          int? sendStatus,
          String? isRead}) =>
      Feed(
        uuid: uuid.present ? uuid.value : this.uuid,
        feedID: feedID ?? this.feedID,
        parentID: parentID.present ? parentID.value : this.parentID,
        projectID: projectID ?? this.projectID,
        description: description.present ? description.value : this.description,
        originDesc: originDesc.present ? originDesc.value : this.originDesc,
        feedType: feedType.present ? feedType.value : this.feedType,
        postDateTime:
            postDateTime.present ? postDateTime.value : this.postDateTime,
        updatedDateTime: updatedDateTime.present
            ? updatedDateTime.value
            : this.updatedDateTime,
        topicID: topicID.present ? topicID.value : this.topicID,
        teamMemberID:
            teamMemberID.present ? teamMemberID.value : this.teamMemberID,
        moduleID: moduleID.present ? moduleID.value : this.moduleID,
        moduleName: moduleName.present ? moduleName.value : this.moduleName,
        updatesDescription: updatesDescription.present
            ? updatesDescription.value
            : this.updatesDescription,
        replyTeamMemberAvatar: replyTeamMemberAvatar.present
            ? replyTeamMemberAvatar.value
            : this.replyTeamMemberAvatar,
        replyCount: replyCount ?? this.replyCount,
        replyReadStatus: replyReadStatus.present
            ? replyReadStatus.value
            : this.replyReadStatus,
        backgroundColor: backgroundColor.present
            ? backgroundColor.value
            : this.backgroundColor,
        outlineColor:
            outlineColor.present ? outlineColor.value : this.outlineColor,
        fontNameColor:
            fontNameColor.present ? fontNameColor.value : this.fontNameColor,
        fontPositionColor: fontPositionColor.present
            ? fontPositionColor.value
            : this.fontPositionColor,
        fontDescriptionColor: fontDescriptionColor.present
            ? fontDescriptionColor.value
            : this.fontDescriptionColor,
        fontDateColor:
            fontDateColor.present ? fontDateColor.value : this.fontDateColor,
        lastReadFeedID:
            lastReadFeedID.present ? lastReadFeedID.value : this.lastReadFeedID,
        sendStatus: sendStatus ?? this.sendStatus,
        isRead: isRead ?? this.isRead,
      );
  Feed copyWithCompanion(FeedsCompanion data) {
    return Feed(
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      feedID: data.feedID.present ? data.feedID.value : this.feedID,
      parentID: data.parentID.present ? data.parentID.value : this.parentID,
      projectID: data.projectID.present ? data.projectID.value : this.projectID,
      description:
          data.description.present ? data.description.value : this.description,
      originDesc:
          data.originDesc.present ? data.originDesc.value : this.originDesc,
      feedType: data.feedType.present ? data.feedType.value : this.feedType,
      postDateTime: data.postDateTime.present
          ? data.postDateTime.value
          : this.postDateTime,
      updatedDateTime: data.updatedDateTime.present
          ? data.updatedDateTime.value
          : this.updatedDateTime,
      topicID: data.topicID.present ? data.topicID.value : this.topicID,
      teamMemberID: data.teamMemberID.present
          ? data.teamMemberID.value
          : this.teamMemberID,
      moduleID: data.moduleID.present ? data.moduleID.value : this.moduleID,
      moduleName:
          data.moduleName.present ? data.moduleName.value : this.moduleName,
      updatesDescription: data.updatesDescription.present
          ? data.updatesDescription.value
          : this.updatesDescription,
      replyTeamMemberAvatar: data.replyTeamMemberAvatar.present
          ? data.replyTeamMemberAvatar.value
          : this.replyTeamMemberAvatar,
      replyCount:
          data.replyCount.present ? data.replyCount.value : this.replyCount,
      replyReadStatus: data.replyReadStatus.present
          ? data.replyReadStatus.value
          : this.replyReadStatus,
      backgroundColor: data.backgroundColor.present
          ? data.backgroundColor.value
          : this.backgroundColor,
      outlineColor: data.outlineColor.present
          ? data.outlineColor.value
          : this.outlineColor,
      fontNameColor: data.fontNameColor.present
          ? data.fontNameColor.value
          : this.fontNameColor,
      fontPositionColor: data.fontPositionColor.present
          ? data.fontPositionColor.value
          : this.fontPositionColor,
      fontDescriptionColor: data.fontDescriptionColor.present
          ? data.fontDescriptionColor.value
          : this.fontDescriptionColor,
      fontDateColor: data.fontDateColor.present
          ? data.fontDateColor.value
          : this.fontDateColor,
      lastReadFeedID: data.lastReadFeedID.present
          ? data.lastReadFeedID.value
          : this.lastReadFeedID,
      sendStatus:
          data.sendStatus.present ? data.sendStatus.value : this.sendStatus,
      isRead: data.isRead.present ? data.isRead.value : this.isRead,
    );
  }

  @override
  String toString() {
    return (StringBuffer('Feed(')
          ..write('uuid: $uuid, ')
          ..write('feedID: $feedID, ')
          ..write('parentID: $parentID, ')
          ..write('projectID: $projectID, ')
          ..write('description: $description, ')
          ..write('originDesc: $originDesc, ')
          ..write('feedType: $feedType, ')
          ..write('postDateTime: $postDateTime, ')
          ..write('updatedDateTime: $updatedDateTime, ')
          ..write('topicID: $topicID, ')
          ..write('teamMemberID: $teamMemberID, ')
          ..write('moduleID: $moduleID, ')
          ..write('moduleName: $moduleName, ')
          ..write('updatesDescription: $updatesDescription, ')
          ..write('replyTeamMemberAvatar: $replyTeamMemberAvatar, ')
          ..write('replyCount: $replyCount, ')
          ..write('replyReadStatus: $replyReadStatus, ')
          ..write('backgroundColor: $backgroundColor, ')
          ..write('outlineColor: $outlineColor, ')
          ..write('fontNameColor: $fontNameColor, ')
          ..write('fontPositionColor: $fontPositionColor, ')
          ..write('fontDescriptionColor: $fontDescriptionColor, ')
          ..write('fontDateColor: $fontDateColor, ')
          ..write('lastReadFeedID: $lastReadFeedID, ')
          ..write('sendStatus: $sendStatus, ')
          ..write('isRead: $isRead')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hashAll([
        uuid,
        feedID,
        parentID,
        projectID,
        description,
        originDesc,
        feedType,
        postDateTime,
        updatedDateTime,
        topicID,
        teamMemberID,
        moduleID,
        moduleName,
        updatesDescription,
        replyTeamMemberAvatar,
        replyCount,
        replyReadStatus,
        backgroundColor,
        outlineColor,
        fontNameColor,
        fontPositionColor,
        fontDescriptionColor,
        fontDateColor,
        lastReadFeedID,
        sendStatus,
        isRead
      ]);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Feed &&
          other.uuid == this.uuid &&
          other.feedID == this.feedID &&
          other.parentID == this.parentID &&
          other.projectID == this.projectID &&
          other.description == this.description &&
          other.originDesc == this.originDesc &&
          other.feedType == this.feedType &&
          other.postDateTime == this.postDateTime &&
          other.updatedDateTime == this.updatedDateTime &&
          other.topicID == this.topicID &&
          other.teamMemberID == this.teamMemberID &&
          other.moduleID == this.moduleID &&
          other.moduleName == this.moduleName &&
          other.updatesDescription == this.updatesDescription &&
          other.replyTeamMemberAvatar == this.replyTeamMemberAvatar &&
          other.replyCount == this.replyCount &&
          other.replyReadStatus == this.replyReadStatus &&
          other.backgroundColor == this.backgroundColor &&
          other.outlineColor == this.outlineColor &&
          other.fontNameColor == this.fontNameColor &&
          other.fontPositionColor == this.fontPositionColor &&
          other.fontDescriptionColor == this.fontDescriptionColor &&
          other.fontDateColor == this.fontDateColor &&
          other.lastReadFeedID == this.lastReadFeedID &&
          other.sendStatus == this.sendStatus &&
          other.isRead == this.isRead);
}

class FeedsCompanion extends UpdateCompanion<Feed> {
  final Value<String?> uuid;
  final Value<int> feedID;
  final Value<int?> parentID;
  final Value<int> projectID;
  final Value<String?> description;
  final Value<String?> originDesc;
  final Value<String?> feedType;
  final Value<String?> postDateTime;
  final Value<String?> updatedDateTime;
  final Value<int?> topicID;
  final Value<int?> teamMemberID;
  final Value<String?> moduleID;
  final Value<String?> moduleName;
  final Value<String?> updatesDescription;
  final Value<String?> replyTeamMemberAvatar;
  final Value<int> replyCount;
  final Value<String?> replyReadStatus;
  final Value<String?> backgroundColor;
  final Value<String?> outlineColor;
  final Value<String?> fontNameColor;
  final Value<String?> fontPositionColor;
  final Value<String?> fontDescriptionColor;
  final Value<String?> fontDateColor;
  final Value<int?> lastReadFeedID;
  final Value<int> sendStatus;
  final Value<String> isRead;
  const FeedsCompanion({
    this.uuid = const Value.absent(),
    this.feedID = const Value.absent(),
    this.parentID = const Value.absent(),
    this.projectID = const Value.absent(),
    this.description = const Value.absent(),
    this.originDesc = const Value.absent(),
    this.feedType = const Value.absent(),
    this.postDateTime = const Value.absent(),
    this.updatedDateTime = const Value.absent(),
    this.topicID = const Value.absent(),
    this.teamMemberID = const Value.absent(),
    this.moduleID = const Value.absent(),
    this.moduleName = const Value.absent(),
    this.updatesDescription = const Value.absent(),
    this.replyTeamMemberAvatar = const Value.absent(),
    this.replyCount = const Value.absent(),
    this.replyReadStatus = const Value.absent(),
    this.backgroundColor = const Value.absent(),
    this.outlineColor = const Value.absent(),
    this.fontNameColor = const Value.absent(),
    this.fontPositionColor = const Value.absent(),
    this.fontDescriptionColor = const Value.absent(),
    this.fontDateColor = const Value.absent(),
    this.lastReadFeedID = const Value.absent(),
    this.sendStatus = const Value.absent(),
    this.isRead = const Value.absent(),
  });
  FeedsCompanion.insert({
    this.uuid = const Value.absent(),
    this.feedID = const Value.absent(),
    this.parentID = const Value.absent(),
    required int projectID,
    this.description = const Value.absent(),
    this.originDesc = const Value.absent(),
    this.feedType = const Value.absent(),
    this.postDateTime = const Value.absent(),
    this.updatedDateTime = const Value.absent(),
    this.topicID = const Value.absent(),
    this.teamMemberID = const Value.absent(),
    this.moduleID = const Value.absent(),
    this.moduleName = const Value.absent(),
    this.updatesDescription = const Value.absent(),
    this.replyTeamMemberAvatar = const Value.absent(),
    this.replyCount = const Value.absent(),
    this.replyReadStatus = const Value.absent(),
    this.backgroundColor = const Value.absent(),
    this.outlineColor = const Value.absent(),
    this.fontNameColor = const Value.absent(),
    this.fontPositionColor = const Value.absent(),
    this.fontDescriptionColor = const Value.absent(),
    this.fontDateColor = const Value.absent(),
    this.lastReadFeedID = const Value.absent(),
    required int sendStatus,
    this.isRead = const Value.absent(),
  })  : projectID = Value(projectID),
        sendStatus = Value(sendStatus);
  static Insertable<Feed> custom({
    Expression<String>? uuid,
    Expression<int>? feedID,
    Expression<int>? parentID,
    Expression<int>? projectID,
    Expression<String>? description,
    Expression<String>? originDesc,
    Expression<String>? feedType,
    Expression<String>? postDateTime,
    Expression<String>? updatedDateTime,
    Expression<int>? topicID,
    Expression<int>? teamMemberID,
    Expression<String>? moduleID,
    Expression<String>? moduleName,
    Expression<String>? updatesDescription,
    Expression<String>? replyTeamMemberAvatar,
    Expression<int>? replyCount,
    Expression<String>? replyReadStatus,
    Expression<String>? backgroundColor,
    Expression<String>? outlineColor,
    Expression<String>? fontNameColor,
    Expression<String>? fontPositionColor,
    Expression<String>? fontDescriptionColor,
    Expression<String>? fontDateColor,
    Expression<int>? lastReadFeedID,
    Expression<int>? sendStatus,
    Expression<String>? isRead,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (feedID != null) 'feedID': feedID,
      if (parentID != null) 'parentID': parentID,
      if (projectID != null) 'projectID': projectID,
      if (description != null) 'description': description,
      if (originDesc != null) 'originDesc': originDesc,
      if (feedType != null) 'feedType': feedType,
      if (postDateTime != null) 'postDateTime': postDateTime,
      if (updatedDateTime != null) 'updatedDateTime': updatedDateTime,
      if (topicID != null) 'topicID': topicID,
      if (teamMemberID != null) 'teamMemberID': teamMemberID,
      if (moduleID != null) 'moduleID': moduleID,
      if (moduleName != null) 'moduleName': moduleName,
      if (updatesDescription != null) 'updatesDescription': updatesDescription,
      if (replyTeamMemberAvatar != null)
        'replyTeamMemberAvatar': replyTeamMemberAvatar,
      if (replyCount != null) 'replyCount': replyCount,
      if (replyReadStatus != null) 'replyReadStatus': replyReadStatus,
      if (backgroundColor != null) 'backgroundColor': backgroundColor,
      if (outlineColor != null) 'outlineColor': outlineColor,
      if (fontNameColor != null) 'fontNameColor': fontNameColor,
      if (fontPositionColor != null) 'fontPositionColor': fontPositionColor,
      if (fontDescriptionColor != null)
        'fontDescriptionColor': fontDescriptionColor,
      if (fontDateColor != null) 'fontDateColor': fontDateColor,
      if (lastReadFeedID != null) 'lastReadFeedID': lastReadFeedID,
      if (sendStatus != null) 'sendStatus': sendStatus,
      if (isRead != null) 'isRead': isRead,
    });
  }

  FeedsCompanion copyWith(
      {Value<String?>? uuid,
      Value<int>? feedID,
      Value<int?>? parentID,
      Value<int>? projectID,
      Value<String?>? description,
      Value<String?>? originDesc,
      Value<String?>? feedType,
      Value<String?>? postDateTime,
      Value<String?>? updatedDateTime,
      Value<int?>? topicID,
      Value<int?>? teamMemberID,
      Value<String?>? moduleID,
      Value<String?>? moduleName,
      Value<String?>? updatesDescription,
      Value<String?>? replyTeamMemberAvatar,
      Value<int>? replyCount,
      Value<String?>? replyReadStatus,
      Value<String?>? backgroundColor,
      Value<String?>? outlineColor,
      Value<String?>? fontNameColor,
      Value<String?>? fontPositionColor,
      Value<String?>? fontDescriptionColor,
      Value<String?>? fontDateColor,
      Value<int?>? lastReadFeedID,
      Value<int>? sendStatus,
      Value<String>? isRead}) {
    return FeedsCompanion(
      uuid: uuid ?? this.uuid,
      feedID: feedID ?? this.feedID,
      parentID: parentID ?? this.parentID,
      projectID: projectID ?? this.projectID,
      description: description ?? this.description,
      originDesc: originDesc ?? this.originDesc,
      feedType: feedType ?? this.feedType,
      postDateTime: postDateTime ?? this.postDateTime,
      updatedDateTime: updatedDateTime ?? this.updatedDateTime,
      topicID: topicID ?? this.topicID,
      teamMemberID: teamMemberID ?? this.teamMemberID,
      moduleID: moduleID ?? this.moduleID,
      moduleName: moduleName ?? this.moduleName,
      updatesDescription: updatesDescription ?? this.updatesDescription,
      replyTeamMemberAvatar:
          replyTeamMemberAvatar ?? this.replyTeamMemberAvatar,
      replyCount: replyCount ?? this.replyCount,
      replyReadStatus: replyReadStatus ?? this.replyReadStatus,
      backgroundColor: backgroundColor ?? this.backgroundColor,
      outlineColor: outlineColor ?? this.outlineColor,
      fontNameColor: fontNameColor ?? this.fontNameColor,
      fontPositionColor: fontPositionColor ?? this.fontPositionColor,
      fontDescriptionColor: fontDescriptionColor ?? this.fontDescriptionColor,
      fontDateColor: fontDateColor ?? this.fontDateColor,
      lastReadFeedID: lastReadFeedID ?? this.lastReadFeedID,
      sendStatus: sendStatus ?? this.sendStatus,
      isRead: isRead ?? this.isRead,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (feedID.present) {
      map['feedID'] = Variable<int>(feedID.value);
    }
    if (parentID.present) {
      map['parentID'] = Variable<int>(parentID.value);
    }
    if (projectID.present) {
      map['projectID'] = Variable<int>(projectID.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (originDesc.present) {
      map['originDesc'] = Variable<String>(originDesc.value);
    }
    if (feedType.present) {
      map['feedType'] = Variable<String>(feedType.value);
    }
    if (postDateTime.present) {
      map['postDateTime'] = Variable<String>(postDateTime.value);
    }
    if (updatedDateTime.present) {
      map['updatedDateTime'] = Variable<String>(updatedDateTime.value);
    }
    if (topicID.present) {
      map['topicID'] = Variable<int>(topicID.value);
    }
    if (teamMemberID.present) {
      map['teamMemberID'] = Variable<int>(teamMemberID.value);
    }
    if (moduleID.present) {
      map['moduleID'] = Variable<String>(moduleID.value);
    }
    if (moduleName.present) {
      map['moduleName'] = Variable<String>(moduleName.value);
    }
    if (updatesDescription.present) {
      map['updatesDescription'] = Variable<String>(updatesDescription.value);
    }
    if (replyTeamMemberAvatar.present) {
      map['replyTeamMemberAvatar'] =
          Variable<String>(replyTeamMemberAvatar.value);
    }
    if (replyCount.present) {
      map['replyCount'] = Variable<int>(replyCount.value);
    }
    if (replyReadStatus.present) {
      map['replyReadStatus'] = Variable<String>(replyReadStatus.value);
    }
    if (backgroundColor.present) {
      map['backgroundColor'] = Variable<String>(backgroundColor.value);
    }
    if (outlineColor.present) {
      map['outlineColor'] = Variable<String>(outlineColor.value);
    }
    if (fontNameColor.present) {
      map['fontNameColor'] = Variable<String>(fontNameColor.value);
    }
    if (fontPositionColor.present) {
      map['fontPositionColor'] = Variable<String>(fontPositionColor.value);
    }
    if (fontDescriptionColor.present) {
      map['fontDescriptionColor'] =
          Variable<String>(fontDescriptionColor.value);
    }
    if (fontDateColor.present) {
      map['fontDateColor'] = Variable<String>(fontDateColor.value);
    }
    if (lastReadFeedID.present) {
      map['lastReadFeedID'] = Variable<int>(lastReadFeedID.value);
    }
    if (sendStatus.present) {
      map['sendStatus'] = Variable<int>(sendStatus.value);
    }
    if (isRead.present) {
      map['isRead'] = Variable<String>(isRead.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FeedsCompanion(')
          ..write('uuid: $uuid, ')
          ..write('feedID: $feedID, ')
          ..write('parentID: $parentID, ')
          ..write('projectID: $projectID, ')
          ..write('description: $description, ')
          ..write('originDesc: $originDesc, ')
          ..write('feedType: $feedType, ')
          ..write('postDateTime: $postDateTime, ')
          ..write('updatedDateTime: $updatedDateTime, ')
          ..write('topicID: $topicID, ')
          ..write('teamMemberID: $teamMemberID, ')
          ..write('moduleID: $moduleID, ')
          ..write('moduleName: $moduleName, ')
          ..write('updatesDescription: $updatesDescription, ')
          ..write('replyTeamMemberAvatar: $replyTeamMemberAvatar, ')
          ..write('replyCount: $replyCount, ')
          ..write('replyReadStatus: $replyReadStatus, ')
          ..write('backgroundColor: $backgroundColor, ')
          ..write('outlineColor: $outlineColor, ')
          ..write('fontNameColor: $fontNameColor, ')
          ..write('fontPositionColor: $fontPositionColor, ')
          ..write('fontDescriptionColor: $fontDescriptionColor, ')
          ..write('fontDateColor: $fontDateColor, ')
          ..write('lastReadFeedID: $lastReadFeedID, ')
          ..write('sendStatus: $sendStatus, ')
          ..write('isRead: $isRead')
          ..write(')'))
        .toString();
  }
}

class $MediasTable extends Medias with TableInfo<$MediasTable, Media> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $MediasTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _mediaIDMeta =
      const VerificationMeta('mediaID');
  @override
  late final GeneratedColumn<int> mediaID = GeneratedColumn<int>(
      'mediaID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _projectIDMeta =
      const VerificationMeta('projectID');
  @override
  late final GeneratedColumn<int> projectID = GeneratedColumn<int>(
      'projectID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _feedIDMeta = const VerificationMeta('feedID');
  @override
  late final GeneratedColumn<int> feedID = GeneratedColumn<int>(
      'feedID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _urlMeta = const VerificationMeta('url');
  @override
  late final GeneratedColumn<String> url = GeneratedColumn<String>(
      'url', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _fileExtensionMeta =
      const VerificationMeta('fileExtension');
  @override
  late final GeneratedColumn<String> fileExtension = GeneratedColumn<String>(
      'fileExtension', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _localFilePathMeta =
      const VerificationMeta('localFilePath');
  @override
  late final GeneratedColumn<String> localFilePath = GeneratedColumn<String>(
      'localFilePath', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _feedUuidMeta =
      const VerificationMeta('feedUuid');
  @override
  late final GeneratedColumn<String> feedUuid = GeneratedColumn<String>(
      'feedUuid', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns =>
      [mediaID, projectID, feedID, url, fileExtension, localFilePath, feedUuid];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'medias';
  @override
  VerificationContext validateIntegrity(Insertable<Media> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('mediaID')) {
      context.handle(_mediaIDMeta,
          mediaID.isAcceptableOrUnknown(data['mediaID']!, _mediaIDMeta));
    }
    if (data.containsKey('projectID')) {
      context.handle(_projectIDMeta,
          projectID.isAcceptableOrUnknown(data['projectID']!, _projectIDMeta));
    } else if (isInserting) {
      context.missing(_projectIDMeta);
    }
    if (data.containsKey('feedID')) {
      context.handle(_feedIDMeta,
          feedID.isAcceptableOrUnknown(data['feedID']!, _feedIDMeta));
    } else if (isInserting) {
      context.missing(_feedIDMeta);
    }
    if (data.containsKey('url')) {
      context.handle(
          _urlMeta, url.isAcceptableOrUnknown(data['url']!, _urlMeta));
    }
    if (data.containsKey('fileExtension')) {
      context.handle(
          _fileExtensionMeta,
          fileExtension.isAcceptableOrUnknown(
              data['fileExtension']!, _fileExtensionMeta));
    }
    if (data.containsKey('localFilePath')) {
      context.handle(
          _localFilePathMeta,
          localFilePath.isAcceptableOrUnknown(
              data['localFilePath']!, _localFilePathMeta));
    }
    if (data.containsKey('feedUuid')) {
      context.handle(_feedUuidMeta,
          feedUuid.isAcceptableOrUnknown(data['feedUuid']!, _feedUuidMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {mediaID};
  @override
  Media map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Media(
      mediaID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}mediaID'])!,
      projectID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}projectID'])!,
      feedID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}feedID'])!,
      url: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}url']),
      fileExtension: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}fileExtension']),
      localFilePath: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}localFilePath']),
      feedUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}feedUuid']),
    );
  }

  @override
  $MediasTable createAlias(String alias) {
    return $MediasTable(attachedDatabase, alias);
  }
}

class Media extends DataClass implements Insertable<Media> {
  final int mediaID;
  final int projectID;
  final int feedID;
  final String? url;
  final String? fileExtension;
  final String? localFilePath;
  final String? feedUuid;
  const Media(
      {required this.mediaID,
      required this.projectID,
      required this.feedID,
      this.url,
      this.fileExtension,
      this.localFilePath,
      this.feedUuid});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['mediaID'] = Variable<int>(mediaID);
    map['projectID'] = Variable<int>(projectID);
    map['feedID'] = Variable<int>(feedID);
    if (!nullToAbsent || url != null) {
      map['url'] = Variable<String>(url);
    }
    if (!nullToAbsent || fileExtension != null) {
      map['fileExtension'] = Variable<String>(fileExtension);
    }
    if (!nullToAbsent || localFilePath != null) {
      map['localFilePath'] = Variable<String>(localFilePath);
    }
    if (!nullToAbsent || feedUuid != null) {
      map['feedUuid'] = Variable<String>(feedUuid);
    }
    return map;
  }

  MediasCompanion toCompanion(bool nullToAbsent) {
    return MediasCompanion(
      mediaID: Value(mediaID),
      projectID: Value(projectID),
      feedID: Value(feedID),
      url: url == null && nullToAbsent ? const Value.absent() : Value(url),
      fileExtension: fileExtension == null && nullToAbsent
          ? const Value.absent()
          : Value(fileExtension),
      localFilePath: localFilePath == null && nullToAbsent
          ? const Value.absent()
          : Value(localFilePath),
      feedUuid: feedUuid == null && nullToAbsent
          ? const Value.absent()
          : Value(feedUuid),
    );
  }

  factory Media.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Media(
      mediaID: serializer.fromJson<int>(json['mediaID']),
      projectID: serializer.fromJson<int>(json['projectID']),
      feedID: serializer.fromJson<int>(json['feedID']),
      url: serializer.fromJson<String?>(json['url']),
      fileExtension: serializer.fromJson<String?>(json['fileExtension']),
      localFilePath: serializer.fromJson<String?>(json['localFilePath']),
      feedUuid: serializer.fromJson<String?>(json['feedUuid']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'mediaID': serializer.toJson<int>(mediaID),
      'projectID': serializer.toJson<int>(projectID),
      'feedID': serializer.toJson<int>(feedID),
      'url': serializer.toJson<String?>(url),
      'fileExtension': serializer.toJson<String?>(fileExtension),
      'localFilePath': serializer.toJson<String?>(localFilePath),
      'feedUuid': serializer.toJson<String?>(feedUuid),
    };
  }

  Media copyWith(
          {int? mediaID,
          int? projectID,
          int? feedID,
          Value<String?> url = const Value.absent(),
          Value<String?> fileExtension = const Value.absent(),
          Value<String?> localFilePath = const Value.absent(),
          Value<String?> feedUuid = const Value.absent()}) =>
      Media(
        mediaID: mediaID ?? this.mediaID,
        projectID: projectID ?? this.projectID,
        feedID: feedID ?? this.feedID,
        url: url.present ? url.value : this.url,
        fileExtension:
            fileExtension.present ? fileExtension.value : this.fileExtension,
        localFilePath:
            localFilePath.present ? localFilePath.value : this.localFilePath,
        feedUuid: feedUuid.present ? feedUuid.value : this.feedUuid,
      );
  Media copyWithCompanion(MediasCompanion data) {
    return Media(
      mediaID: data.mediaID.present ? data.mediaID.value : this.mediaID,
      projectID: data.projectID.present ? data.projectID.value : this.projectID,
      feedID: data.feedID.present ? data.feedID.value : this.feedID,
      url: data.url.present ? data.url.value : this.url,
      fileExtension: data.fileExtension.present
          ? data.fileExtension.value
          : this.fileExtension,
      localFilePath: data.localFilePath.present
          ? data.localFilePath.value
          : this.localFilePath,
      feedUuid: data.feedUuid.present ? data.feedUuid.value : this.feedUuid,
    );
  }

  @override
  String toString() {
    return (StringBuffer('Media(')
          ..write('mediaID: $mediaID, ')
          ..write('projectID: $projectID, ')
          ..write('feedID: $feedID, ')
          ..write('url: $url, ')
          ..write('fileExtension: $fileExtension, ')
          ..write('localFilePath: $localFilePath, ')
          ..write('feedUuid: $feedUuid')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      mediaID, projectID, feedID, url, fileExtension, localFilePath, feedUuid);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Media &&
          other.mediaID == this.mediaID &&
          other.projectID == this.projectID &&
          other.feedID == this.feedID &&
          other.url == this.url &&
          other.fileExtension == this.fileExtension &&
          other.localFilePath == this.localFilePath &&
          other.feedUuid == this.feedUuid);
}

class MediasCompanion extends UpdateCompanion<Media> {
  final Value<int> mediaID;
  final Value<int> projectID;
  final Value<int> feedID;
  final Value<String?> url;
  final Value<String?> fileExtension;
  final Value<String?> localFilePath;
  final Value<String?> feedUuid;
  const MediasCompanion({
    this.mediaID = const Value.absent(),
    this.projectID = const Value.absent(),
    this.feedID = const Value.absent(),
    this.url = const Value.absent(),
    this.fileExtension = const Value.absent(),
    this.localFilePath = const Value.absent(),
    this.feedUuid = const Value.absent(),
  });
  MediasCompanion.insert({
    this.mediaID = const Value.absent(),
    required int projectID,
    required int feedID,
    this.url = const Value.absent(),
    this.fileExtension = const Value.absent(),
    this.localFilePath = const Value.absent(),
    this.feedUuid = const Value.absent(),
  })  : projectID = Value(projectID),
        feedID = Value(feedID);
  static Insertable<Media> custom({
    Expression<int>? mediaID,
    Expression<int>? projectID,
    Expression<int>? feedID,
    Expression<String>? url,
    Expression<String>? fileExtension,
    Expression<String>? localFilePath,
    Expression<String>? feedUuid,
  }) {
    return RawValuesInsertable({
      if (mediaID != null) 'mediaID': mediaID,
      if (projectID != null) 'projectID': projectID,
      if (feedID != null) 'feedID': feedID,
      if (url != null) 'url': url,
      if (fileExtension != null) 'fileExtension': fileExtension,
      if (localFilePath != null) 'localFilePath': localFilePath,
      if (feedUuid != null) 'feedUuid': feedUuid,
    });
  }

  MediasCompanion copyWith(
      {Value<int>? mediaID,
      Value<int>? projectID,
      Value<int>? feedID,
      Value<String?>? url,
      Value<String?>? fileExtension,
      Value<String?>? localFilePath,
      Value<String?>? feedUuid}) {
    return MediasCompanion(
      mediaID: mediaID ?? this.mediaID,
      projectID: projectID ?? this.projectID,
      feedID: feedID ?? this.feedID,
      url: url ?? this.url,
      fileExtension: fileExtension ?? this.fileExtension,
      localFilePath: localFilePath ?? this.localFilePath,
      feedUuid: feedUuid ?? this.feedUuid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (mediaID.present) {
      map['mediaID'] = Variable<int>(mediaID.value);
    }
    if (projectID.present) {
      map['projectID'] = Variable<int>(projectID.value);
    }
    if (feedID.present) {
      map['feedID'] = Variable<int>(feedID.value);
    }
    if (url.present) {
      map['url'] = Variable<String>(url.value);
    }
    if (fileExtension.present) {
      map['fileExtension'] = Variable<String>(fileExtension.value);
    }
    if (localFilePath.present) {
      map['localFilePath'] = Variable<String>(localFilePath.value);
    }
    if (feedUuid.present) {
      map['feedUuid'] = Variable<String>(feedUuid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('MediasCompanion(')
          ..write('mediaID: $mediaID, ')
          ..write('projectID: $projectID, ')
          ..write('feedID: $feedID, ')
          ..write('url: $url, ')
          ..write('fileExtension: $fileExtension, ')
          ..write('localFilePath: $localFilePath, ')
          ..write('feedUuid: $feedUuid')
          ..write(')'))
        .toString();
  }
}

class $AlertsTable extends Alerts with TableInfo<$AlertsTable, Alert> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $AlertsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _notificationIDMeta =
      const VerificationMeta('notificationID');
  @override
  late final GeneratedColumn<int> notificationID = GeneratedColumn<int>(
      'notificationID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _projectIDMeta =
      const VerificationMeta('projectID');
  @override
  late final GeneratedColumn<int> projectID = GeneratedColumn<int>(
      'projectID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _affectedModuleIDMeta =
      const VerificationMeta('affectedModuleID');
  @override
  late final GeneratedColumn<String> affectedModuleID = GeneratedColumn<String>(
      'affectedModuleID', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _moduleNameMeta =
      const VerificationMeta('moduleName');
  @override
  late final GeneratedColumn<String> moduleName = GeneratedColumn<String>(
      'moduleName', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _isReadMeta = const VerificationMeta('isRead');
  @override
  late final GeneratedColumn<String> isRead = GeneratedColumn<String>(
      'isRead', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  @override
  late final GeneratedColumn<String> description = GeneratedColumn<String>(
      'description', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _createdDateTimeMeta =
      const VerificationMeta('createdDateTime');
  @override
  late final GeneratedColumn<String> createdDateTime = GeneratedColumn<String>(
      'createdDateTime', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _updatedDateTimeMeta =
      const VerificationMeta('updatedDateTime');
  @override
  late final GeneratedColumn<String> updatedDateTime = GeneratedColumn<String>(
      'updatedDateTime', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _assignedByTeamMemberIDMeta =
      const VerificationMeta('assignedByTeamMemberID');
  @override
  late final GeneratedColumn<int> assignedByTeamMemberID = GeneratedColumn<int>(
      'assignedByTeamMemberID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _assignedByTeamMemberNameMeta =
      const VerificationMeta('assignedByTeamMemberName');
  @override
  late final GeneratedColumn<String> assignedByTeamMemberName =
      GeneratedColumn<String>('assignedByTeamMemberName', aliasedName, true,
          type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _teamMemberAvatarMeta =
      const VerificationMeta('teamMemberAvatar');
  @override
  late final GeneratedColumn<String> teamMemberAvatar = GeneratedColumn<String>(
      'teamMemberAvatar', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _replyToIDMeta =
      const VerificationMeta('replyToID');
  @override
  late final GeneratedColumn<int> replyToID = GeneratedColumn<int>(
      'replyToID', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _delayTaskIDsMeta =
      const VerificationMeta('delayTaskIDs');
  @override
  late final GeneratedColumn<String> delayTaskIDs = GeneratedColumn<String>(
      'delayTaskIDs', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _delayDurationMeta =
      const VerificationMeta('delayDuration');
  @override
  late final GeneratedColumn<int> delayDuration = GeneratedColumn<int>(
      'delayDuration', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _allNotificationTxtMeta =
      const VerificationMeta('allNotificationTxt');
  @override
  late final GeneratedColumn<String> allNotificationTxt =
      GeneratedColumn<String>('allNotificationTxt', aliasedName, true,
          type: DriftSqlType.string, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [
        notificationID,
        projectID,
        affectedModuleID,
        moduleName,
        isRead,
        description,
        createdDateTime,
        updatedDateTime,
        assignedByTeamMemberID,
        assignedByTeamMemberName,
        teamMemberAvatar,
        replyToID,
        delayTaskIDs,
        delayDuration,
        allNotificationTxt
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'alerts';
  @override
  VerificationContext validateIntegrity(Insertable<Alert> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('notificationID')) {
      context.handle(
          _notificationIDMeta,
          notificationID.isAcceptableOrUnknown(
              data['notificationID']!, _notificationIDMeta));
    }
    if (data.containsKey('projectID')) {
      context.handle(_projectIDMeta,
          projectID.isAcceptableOrUnknown(data['projectID']!, _projectIDMeta));
    } else if (isInserting) {
      context.missing(_projectIDMeta);
    }
    if (data.containsKey('affectedModuleID')) {
      context.handle(
          _affectedModuleIDMeta,
          affectedModuleID.isAcceptableOrUnknown(
              data['affectedModuleID']!, _affectedModuleIDMeta));
    }
    if (data.containsKey('moduleName')) {
      context.handle(
          _moduleNameMeta,
          moduleName.isAcceptableOrUnknown(
              data['moduleName']!, _moduleNameMeta));
    }
    if (data.containsKey('isRead')) {
      context.handle(_isReadMeta,
          isRead.isAcceptableOrUnknown(data['isRead']!, _isReadMeta));
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    if (data.containsKey('createdDateTime')) {
      context.handle(
          _createdDateTimeMeta,
          createdDateTime.isAcceptableOrUnknown(
              data['createdDateTime']!, _createdDateTimeMeta));
    }
    if (data.containsKey('updatedDateTime')) {
      context.handle(
          _updatedDateTimeMeta,
          updatedDateTime.isAcceptableOrUnknown(
              data['updatedDateTime']!, _updatedDateTimeMeta));
    }
    if (data.containsKey('assignedByTeamMemberID')) {
      context.handle(
          _assignedByTeamMemberIDMeta,
          assignedByTeamMemberID.isAcceptableOrUnknown(
              data['assignedByTeamMemberID']!, _assignedByTeamMemberIDMeta));
    } else if (isInserting) {
      context.missing(_assignedByTeamMemberIDMeta);
    }
    if (data.containsKey('assignedByTeamMemberName')) {
      context.handle(
          _assignedByTeamMemberNameMeta,
          assignedByTeamMemberName.isAcceptableOrUnknown(
              data['assignedByTeamMemberName']!,
              _assignedByTeamMemberNameMeta));
    }
    if (data.containsKey('teamMemberAvatar')) {
      context.handle(
          _teamMemberAvatarMeta,
          teamMemberAvatar.isAcceptableOrUnknown(
              data['teamMemberAvatar']!, _teamMemberAvatarMeta));
    }
    if (data.containsKey('replyToID')) {
      context.handle(_replyToIDMeta,
          replyToID.isAcceptableOrUnknown(data['replyToID']!, _replyToIDMeta));
    }
    if (data.containsKey('delayTaskIDs')) {
      context.handle(
          _delayTaskIDsMeta,
          delayTaskIDs.isAcceptableOrUnknown(
              data['delayTaskIDs']!, _delayTaskIDsMeta));
    }
    if (data.containsKey('delayDuration')) {
      context.handle(
          _delayDurationMeta,
          delayDuration.isAcceptableOrUnknown(
              data['delayDuration']!, _delayDurationMeta));
    }
    if (data.containsKey('allNotificationTxt')) {
      context.handle(
          _allNotificationTxtMeta,
          allNotificationTxt.isAcceptableOrUnknown(
              data['allNotificationTxt']!, _allNotificationTxtMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {notificationID};
  @override
  Alert map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Alert(
      notificationID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}notificationID'])!,
      projectID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}projectID'])!,
      affectedModuleID: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}affectedModuleID']),
      moduleName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}moduleName']),
      isRead: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}isRead']),
      description: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}description']),
      createdDateTime: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}createdDateTime']),
      updatedDateTime: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}updatedDateTime']),
      assignedByTeamMemberID: attachedDatabase.typeMapping.read(
          DriftSqlType.int, data['${effectivePrefix}assignedByTeamMemberID'])!,
      assignedByTeamMemberName: attachedDatabase.typeMapping.read(
          DriftSqlType.string,
          data['${effectivePrefix}assignedByTeamMemberName']),
      teamMemberAvatar: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}teamMemberAvatar']),
      replyToID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}replyToID']),
      delayTaskIDs: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}delayTaskIDs']),
      delayDuration: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}delayDuration']),
      allNotificationTxt: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}allNotificationTxt']),
    );
  }

  @override
  $AlertsTable createAlias(String alias) {
    return $AlertsTable(attachedDatabase, alias);
  }
}

class Alert extends DataClass implements Insertable<Alert> {
  final int notificationID;
  final int projectID;
  final String? affectedModuleID;
  final String? moduleName;
  final String? isRead;
  final String? description;
  final String? createdDateTime;
  final String? updatedDateTime;
  final int assignedByTeamMemberID;
  final String? assignedByTeamMemberName;
  final String? teamMemberAvatar;
  final int? replyToID;
  final String? delayTaskIDs;
  final int? delayDuration;
  final String? allNotificationTxt;
  const Alert(
      {required this.notificationID,
      required this.projectID,
      this.affectedModuleID,
      this.moduleName,
      this.isRead,
      this.description,
      this.createdDateTime,
      this.updatedDateTime,
      required this.assignedByTeamMemberID,
      this.assignedByTeamMemberName,
      this.teamMemberAvatar,
      this.replyToID,
      this.delayTaskIDs,
      this.delayDuration,
      this.allNotificationTxt});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['notificationID'] = Variable<int>(notificationID);
    map['projectID'] = Variable<int>(projectID);
    if (!nullToAbsent || affectedModuleID != null) {
      map['affectedModuleID'] = Variable<String>(affectedModuleID);
    }
    if (!nullToAbsent || moduleName != null) {
      map['moduleName'] = Variable<String>(moduleName);
    }
    if (!nullToAbsent || isRead != null) {
      map['isRead'] = Variable<String>(isRead);
    }
    if (!nullToAbsent || description != null) {
      map['description'] = Variable<String>(description);
    }
    if (!nullToAbsent || createdDateTime != null) {
      map['createdDateTime'] = Variable<String>(createdDateTime);
    }
    if (!nullToAbsent || updatedDateTime != null) {
      map['updatedDateTime'] = Variable<String>(updatedDateTime);
    }
    map['assignedByTeamMemberID'] = Variable<int>(assignedByTeamMemberID);
    if (!nullToAbsent || assignedByTeamMemberName != null) {
      map['assignedByTeamMemberName'] =
          Variable<String>(assignedByTeamMemberName);
    }
    if (!nullToAbsent || teamMemberAvatar != null) {
      map['teamMemberAvatar'] = Variable<String>(teamMemberAvatar);
    }
    if (!nullToAbsent || replyToID != null) {
      map['replyToID'] = Variable<int>(replyToID);
    }
    if (!nullToAbsent || delayTaskIDs != null) {
      map['delayTaskIDs'] = Variable<String>(delayTaskIDs);
    }
    if (!nullToAbsent || delayDuration != null) {
      map['delayDuration'] = Variable<int>(delayDuration);
    }
    if (!nullToAbsent || allNotificationTxt != null) {
      map['allNotificationTxt'] = Variable<String>(allNotificationTxt);
    }
    return map;
  }

  AlertsCompanion toCompanion(bool nullToAbsent) {
    return AlertsCompanion(
      notificationID: Value(notificationID),
      projectID: Value(projectID),
      affectedModuleID: affectedModuleID == null && nullToAbsent
          ? const Value.absent()
          : Value(affectedModuleID),
      moduleName: moduleName == null && nullToAbsent
          ? const Value.absent()
          : Value(moduleName),
      isRead:
          isRead == null && nullToAbsent ? const Value.absent() : Value(isRead),
      description: description == null && nullToAbsent
          ? const Value.absent()
          : Value(description),
      createdDateTime: createdDateTime == null && nullToAbsent
          ? const Value.absent()
          : Value(createdDateTime),
      updatedDateTime: updatedDateTime == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedDateTime),
      assignedByTeamMemberID: Value(assignedByTeamMemberID),
      assignedByTeamMemberName: assignedByTeamMemberName == null && nullToAbsent
          ? const Value.absent()
          : Value(assignedByTeamMemberName),
      teamMemberAvatar: teamMemberAvatar == null && nullToAbsent
          ? const Value.absent()
          : Value(teamMemberAvatar),
      replyToID: replyToID == null && nullToAbsent
          ? const Value.absent()
          : Value(replyToID),
      delayTaskIDs: delayTaskIDs == null && nullToAbsent
          ? const Value.absent()
          : Value(delayTaskIDs),
      delayDuration: delayDuration == null && nullToAbsent
          ? const Value.absent()
          : Value(delayDuration),
      allNotificationTxt: allNotificationTxt == null && nullToAbsent
          ? const Value.absent()
          : Value(allNotificationTxt),
    );
  }

  factory Alert.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Alert(
      notificationID: serializer.fromJson<int>(json['notificationID']),
      projectID: serializer.fromJson<int>(json['projectID']),
      affectedModuleID: serializer.fromJson<String?>(json['affectedModuleID']),
      moduleName: serializer.fromJson<String?>(json['moduleName']),
      isRead: serializer.fromJson<String?>(json['isRead']),
      description: serializer.fromJson<String?>(json['description']),
      createdDateTime: serializer.fromJson<String?>(json['createdDateTime']),
      updatedDateTime: serializer.fromJson<String?>(json['updatedDateTime']),
      assignedByTeamMemberID:
          serializer.fromJson<int>(json['assignedByTeamMemberID']),
      assignedByTeamMemberName:
          serializer.fromJson<String?>(json['assignedByTeamMemberName']),
      teamMemberAvatar: serializer.fromJson<String?>(json['teamMemberAvatar']),
      replyToID: serializer.fromJson<int?>(json['replyToID']),
      delayTaskIDs: serializer.fromJson<String?>(json['delayTaskIDs']),
      delayDuration: serializer.fromJson<int?>(json['delayDuration']),
      allNotificationTxt:
          serializer.fromJson<String?>(json['allNotificationTxt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'notificationID': serializer.toJson<int>(notificationID),
      'projectID': serializer.toJson<int>(projectID),
      'affectedModuleID': serializer.toJson<String?>(affectedModuleID),
      'moduleName': serializer.toJson<String?>(moduleName),
      'isRead': serializer.toJson<String?>(isRead),
      'description': serializer.toJson<String?>(description),
      'createdDateTime': serializer.toJson<String?>(createdDateTime),
      'updatedDateTime': serializer.toJson<String?>(updatedDateTime),
      'assignedByTeamMemberID': serializer.toJson<int>(assignedByTeamMemberID),
      'assignedByTeamMemberName':
          serializer.toJson<String?>(assignedByTeamMemberName),
      'teamMemberAvatar': serializer.toJson<String?>(teamMemberAvatar),
      'replyToID': serializer.toJson<int?>(replyToID),
      'delayTaskIDs': serializer.toJson<String?>(delayTaskIDs),
      'delayDuration': serializer.toJson<int?>(delayDuration),
      'allNotificationTxt': serializer.toJson<String?>(allNotificationTxt),
    };
  }

  Alert copyWith(
          {int? notificationID,
          int? projectID,
          Value<String?> affectedModuleID = const Value.absent(),
          Value<String?> moduleName = const Value.absent(),
          Value<String?> isRead = const Value.absent(),
          Value<String?> description = const Value.absent(),
          Value<String?> createdDateTime = const Value.absent(),
          Value<String?> updatedDateTime = const Value.absent(),
          int? assignedByTeamMemberID,
          Value<String?> assignedByTeamMemberName = const Value.absent(),
          Value<String?> teamMemberAvatar = const Value.absent(),
          Value<int?> replyToID = const Value.absent(),
          Value<String?> delayTaskIDs = const Value.absent(),
          Value<int?> delayDuration = const Value.absent(),
          Value<String?> allNotificationTxt = const Value.absent()}) =>
      Alert(
        notificationID: notificationID ?? this.notificationID,
        projectID: projectID ?? this.projectID,
        affectedModuleID: affectedModuleID.present
            ? affectedModuleID.value
            : this.affectedModuleID,
        moduleName: moduleName.present ? moduleName.value : this.moduleName,
        isRead: isRead.present ? isRead.value : this.isRead,
        description: description.present ? description.value : this.description,
        createdDateTime: createdDateTime.present
            ? createdDateTime.value
            : this.createdDateTime,
        updatedDateTime: updatedDateTime.present
            ? updatedDateTime.value
            : this.updatedDateTime,
        assignedByTeamMemberID:
            assignedByTeamMemberID ?? this.assignedByTeamMemberID,
        assignedByTeamMemberName: assignedByTeamMemberName.present
            ? assignedByTeamMemberName.value
            : this.assignedByTeamMemberName,
        teamMemberAvatar: teamMemberAvatar.present
            ? teamMemberAvatar.value
            : this.teamMemberAvatar,
        replyToID: replyToID.present ? replyToID.value : this.replyToID,
        delayTaskIDs:
            delayTaskIDs.present ? delayTaskIDs.value : this.delayTaskIDs,
        delayDuration:
            delayDuration.present ? delayDuration.value : this.delayDuration,
        allNotificationTxt: allNotificationTxt.present
            ? allNotificationTxt.value
            : this.allNotificationTxt,
      );
  Alert copyWithCompanion(AlertsCompanion data) {
    return Alert(
      notificationID: data.notificationID.present
          ? data.notificationID.value
          : this.notificationID,
      projectID: data.projectID.present ? data.projectID.value : this.projectID,
      affectedModuleID: data.affectedModuleID.present
          ? data.affectedModuleID.value
          : this.affectedModuleID,
      moduleName:
          data.moduleName.present ? data.moduleName.value : this.moduleName,
      isRead: data.isRead.present ? data.isRead.value : this.isRead,
      description:
          data.description.present ? data.description.value : this.description,
      createdDateTime: data.createdDateTime.present
          ? data.createdDateTime.value
          : this.createdDateTime,
      updatedDateTime: data.updatedDateTime.present
          ? data.updatedDateTime.value
          : this.updatedDateTime,
      assignedByTeamMemberID: data.assignedByTeamMemberID.present
          ? data.assignedByTeamMemberID.value
          : this.assignedByTeamMemberID,
      assignedByTeamMemberName: data.assignedByTeamMemberName.present
          ? data.assignedByTeamMemberName.value
          : this.assignedByTeamMemberName,
      teamMemberAvatar: data.teamMemberAvatar.present
          ? data.teamMemberAvatar.value
          : this.teamMemberAvatar,
      replyToID: data.replyToID.present ? data.replyToID.value : this.replyToID,
      delayTaskIDs: data.delayTaskIDs.present
          ? data.delayTaskIDs.value
          : this.delayTaskIDs,
      delayDuration: data.delayDuration.present
          ? data.delayDuration.value
          : this.delayDuration,
      allNotificationTxt: data.allNotificationTxt.present
          ? data.allNotificationTxt.value
          : this.allNotificationTxt,
    );
  }

  @override
  String toString() {
    return (StringBuffer('Alert(')
          ..write('notificationID: $notificationID, ')
          ..write('projectID: $projectID, ')
          ..write('affectedModuleID: $affectedModuleID, ')
          ..write('moduleName: $moduleName, ')
          ..write('isRead: $isRead, ')
          ..write('description: $description, ')
          ..write('createdDateTime: $createdDateTime, ')
          ..write('updatedDateTime: $updatedDateTime, ')
          ..write('assignedByTeamMemberID: $assignedByTeamMemberID, ')
          ..write('assignedByTeamMemberName: $assignedByTeamMemberName, ')
          ..write('teamMemberAvatar: $teamMemberAvatar, ')
          ..write('replyToID: $replyToID, ')
          ..write('delayTaskIDs: $delayTaskIDs, ')
          ..write('delayDuration: $delayDuration, ')
          ..write('allNotificationTxt: $allNotificationTxt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      notificationID,
      projectID,
      affectedModuleID,
      moduleName,
      isRead,
      description,
      createdDateTime,
      updatedDateTime,
      assignedByTeamMemberID,
      assignedByTeamMemberName,
      teamMemberAvatar,
      replyToID,
      delayTaskIDs,
      delayDuration,
      allNotificationTxt);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Alert &&
          other.notificationID == this.notificationID &&
          other.projectID == this.projectID &&
          other.affectedModuleID == this.affectedModuleID &&
          other.moduleName == this.moduleName &&
          other.isRead == this.isRead &&
          other.description == this.description &&
          other.createdDateTime == this.createdDateTime &&
          other.updatedDateTime == this.updatedDateTime &&
          other.assignedByTeamMemberID == this.assignedByTeamMemberID &&
          other.assignedByTeamMemberName == this.assignedByTeamMemberName &&
          other.teamMemberAvatar == this.teamMemberAvatar &&
          other.replyToID == this.replyToID &&
          other.delayTaskIDs == this.delayTaskIDs &&
          other.delayDuration == this.delayDuration &&
          other.allNotificationTxt == this.allNotificationTxt);
}

class AlertsCompanion extends UpdateCompanion<Alert> {
  final Value<int> notificationID;
  final Value<int> projectID;
  final Value<String?> affectedModuleID;
  final Value<String?> moduleName;
  final Value<String?> isRead;
  final Value<String?> description;
  final Value<String?> createdDateTime;
  final Value<String?> updatedDateTime;
  final Value<int> assignedByTeamMemberID;
  final Value<String?> assignedByTeamMemberName;
  final Value<String?> teamMemberAvatar;
  final Value<int?> replyToID;
  final Value<String?> delayTaskIDs;
  final Value<int?> delayDuration;
  final Value<String?> allNotificationTxt;
  const AlertsCompanion({
    this.notificationID = const Value.absent(),
    this.projectID = const Value.absent(),
    this.affectedModuleID = const Value.absent(),
    this.moduleName = const Value.absent(),
    this.isRead = const Value.absent(),
    this.description = const Value.absent(),
    this.createdDateTime = const Value.absent(),
    this.updatedDateTime = const Value.absent(),
    this.assignedByTeamMemberID = const Value.absent(),
    this.assignedByTeamMemberName = const Value.absent(),
    this.teamMemberAvatar = const Value.absent(),
    this.replyToID = const Value.absent(),
    this.delayTaskIDs = const Value.absent(),
    this.delayDuration = const Value.absent(),
    this.allNotificationTxt = const Value.absent(),
  });
  AlertsCompanion.insert({
    this.notificationID = const Value.absent(),
    required int projectID,
    this.affectedModuleID = const Value.absent(),
    this.moduleName = const Value.absent(),
    this.isRead = const Value.absent(),
    this.description = const Value.absent(),
    this.createdDateTime = const Value.absent(),
    this.updatedDateTime = const Value.absent(),
    required int assignedByTeamMemberID,
    this.assignedByTeamMemberName = const Value.absent(),
    this.teamMemberAvatar = const Value.absent(),
    this.replyToID = const Value.absent(),
    this.delayTaskIDs = const Value.absent(),
    this.delayDuration = const Value.absent(),
    this.allNotificationTxt = const Value.absent(),
  })  : projectID = Value(projectID),
        assignedByTeamMemberID = Value(assignedByTeamMemberID);
  static Insertable<Alert> custom({
    Expression<int>? notificationID,
    Expression<int>? projectID,
    Expression<String>? affectedModuleID,
    Expression<String>? moduleName,
    Expression<String>? isRead,
    Expression<String>? description,
    Expression<String>? createdDateTime,
    Expression<String>? updatedDateTime,
    Expression<int>? assignedByTeamMemberID,
    Expression<String>? assignedByTeamMemberName,
    Expression<String>? teamMemberAvatar,
    Expression<int>? replyToID,
    Expression<String>? delayTaskIDs,
    Expression<int>? delayDuration,
    Expression<String>? allNotificationTxt,
  }) {
    return RawValuesInsertable({
      if (notificationID != null) 'notificationID': notificationID,
      if (projectID != null) 'projectID': projectID,
      if (affectedModuleID != null) 'affectedModuleID': affectedModuleID,
      if (moduleName != null) 'moduleName': moduleName,
      if (isRead != null) 'isRead': isRead,
      if (description != null) 'description': description,
      if (createdDateTime != null) 'createdDateTime': createdDateTime,
      if (updatedDateTime != null) 'updatedDateTime': updatedDateTime,
      if (assignedByTeamMemberID != null)
        'assignedByTeamMemberID': assignedByTeamMemberID,
      if (assignedByTeamMemberName != null)
        'assignedByTeamMemberName': assignedByTeamMemberName,
      if (teamMemberAvatar != null) 'teamMemberAvatar': teamMemberAvatar,
      if (replyToID != null) 'replyToID': replyToID,
      if (delayTaskIDs != null) 'delayTaskIDs': delayTaskIDs,
      if (delayDuration != null) 'delayDuration': delayDuration,
      if (allNotificationTxt != null) 'allNotificationTxt': allNotificationTxt,
    });
  }

  AlertsCompanion copyWith(
      {Value<int>? notificationID,
      Value<int>? projectID,
      Value<String?>? affectedModuleID,
      Value<String?>? moduleName,
      Value<String?>? isRead,
      Value<String?>? description,
      Value<String?>? createdDateTime,
      Value<String?>? updatedDateTime,
      Value<int>? assignedByTeamMemberID,
      Value<String?>? assignedByTeamMemberName,
      Value<String?>? teamMemberAvatar,
      Value<int?>? replyToID,
      Value<String?>? delayTaskIDs,
      Value<int?>? delayDuration,
      Value<String?>? allNotificationTxt}) {
    return AlertsCompanion(
      notificationID: notificationID ?? this.notificationID,
      projectID: projectID ?? this.projectID,
      affectedModuleID: affectedModuleID ?? this.affectedModuleID,
      moduleName: moduleName ?? this.moduleName,
      isRead: isRead ?? this.isRead,
      description: description ?? this.description,
      createdDateTime: createdDateTime ?? this.createdDateTime,
      updatedDateTime: updatedDateTime ?? this.updatedDateTime,
      assignedByTeamMemberID:
          assignedByTeamMemberID ?? this.assignedByTeamMemberID,
      assignedByTeamMemberName:
          assignedByTeamMemberName ?? this.assignedByTeamMemberName,
      teamMemberAvatar: teamMemberAvatar ?? this.teamMemberAvatar,
      replyToID: replyToID ?? this.replyToID,
      delayTaskIDs: delayTaskIDs ?? this.delayTaskIDs,
      delayDuration: delayDuration ?? this.delayDuration,
      allNotificationTxt: allNotificationTxt ?? this.allNotificationTxt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (notificationID.present) {
      map['notificationID'] = Variable<int>(notificationID.value);
    }
    if (projectID.present) {
      map['projectID'] = Variable<int>(projectID.value);
    }
    if (affectedModuleID.present) {
      map['affectedModuleID'] = Variable<String>(affectedModuleID.value);
    }
    if (moduleName.present) {
      map['moduleName'] = Variable<String>(moduleName.value);
    }
    if (isRead.present) {
      map['isRead'] = Variable<String>(isRead.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (createdDateTime.present) {
      map['createdDateTime'] = Variable<String>(createdDateTime.value);
    }
    if (updatedDateTime.present) {
      map['updatedDateTime'] = Variable<String>(updatedDateTime.value);
    }
    if (assignedByTeamMemberID.present) {
      map['assignedByTeamMemberID'] =
          Variable<int>(assignedByTeamMemberID.value);
    }
    if (assignedByTeamMemberName.present) {
      map['assignedByTeamMemberName'] =
          Variable<String>(assignedByTeamMemberName.value);
    }
    if (teamMemberAvatar.present) {
      map['teamMemberAvatar'] = Variable<String>(teamMemberAvatar.value);
    }
    if (replyToID.present) {
      map['replyToID'] = Variable<int>(replyToID.value);
    }
    if (delayTaskIDs.present) {
      map['delayTaskIDs'] = Variable<String>(delayTaskIDs.value);
    }
    if (delayDuration.present) {
      map['delayDuration'] = Variable<int>(delayDuration.value);
    }
    if (allNotificationTxt.present) {
      map['allNotificationTxt'] = Variable<String>(allNotificationTxt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('AlertsCompanion(')
          ..write('notificationID: $notificationID, ')
          ..write('projectID: $projectID, ')
          ..write('affectedModuleID: $affectedModuleID, ')
          ..write('moduleName: $moduleName, ')
          ..write('isRead: $isRead, ')
          ..write('description: $description, ')
          ..write('createdDateTime: $createdDateTime, ')
          ..write('updatedDateTime: $updatedDateTime, ')
          ..write('assignedByTeamMemberID: $assignedByTeamMemberID, ')
          ..write('assignedByTeamMemberName: $assignedByTeamMemberName, ')
          ..write('teamMemberAvatar: $teamMemberAvatar, ')
          ..write('replyToID: $replyToID, ')
          ..write('delayTaskIDs: $delayTaskIDs, ')
          ..write('delayDuration: $delayDuration, ')
          ..write('allNotificationTxt: $allNotificationTxt')
          ..write(')'))
        .toString();
  }
}

class $ProfilesTable extends Profiles with TableInfo<$ProfilesTable, Profile> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $ProfilesTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _usernameMeta =
      const VerificationMeta('username');
  @override
  late final GeneratedColumn<String> username = GeneratedColumn<String>(
      'username', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _addressMeta =
      const VerificationMeta('address');
  @override
  late final GeneratedColumn<String> address = GeneratedColumn<String>(
      'address', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _emailMeta = const VerificationMeta('email');
  @override
  late final GeneratedColumn<String> email = GeneratedColumn<String>(
      'email', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _gmailMeta = const VerificationMeta('gmail');
  @override
  late final GeneratedColumn<String> gmail = GeneratedColumn<String>(
      'gmail', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _branchMeta = const VerificationMeta('branch');
  @override
  late final GeneratedColumn<String> branch = GeneratedColumn<String>(
      'branch', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _designationNameMeta =
      const VerificationMeta('designationName');
  @override
  late final GeneratedColumn<String> designationName = GeneratedColumn<String>(
      'designationName', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _designationInitialMeta =
      const VerificationMeta('designationInitial');
  @override
  late final GeneratedColumn<String> designationInitial =
      GeneratedColumn<String>('designationInitial', aliasedName, true,
          type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _avatarMeta = const VerificationMeta('avatar');
  @override
  late final GeneratedColumn<String> avatar = GeneratedColumn<String>(
      'avatar', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _contactNoMeta =
      const VerificationMeta('contactNo');
  @override
  late final GeneratedColumn<String> contactNo = GeneratedColumn<String>(
      'contactNo', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _accessMeta = const VerificationMeta('access');
  @override
  late final GeneratedColumn<String> access = GeneratedColumn<String>(
      'access', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _teamMemberIDMeta =
      const VerificationMeta('teamMemberID');
  @override
  late final GeneratedColumn<int> teamMemberID = GeneratedColumn<int>(
      'teamMemberID', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [
        id,
        username,
        name,
        address,
        email,
        gmail,
        branch,
        designationName,
        designationInitial,
        avatar,
        contactNo,
        access,
        teamMemberID
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'profiles';
  @override
  VerificationContext validateIntegrity(Insertable<Profile> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('username')) {
      context.handle(_usernameMeta,
          username.isAcceptableOrUnknown(data['username']!, _usernameMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    }
    if (data.containsKey('address')) {
      context.handle(_addressMeta,
          address.isAcceptableOrUnknown(data['address']!, _addressMeta));
    }
    if (data.containsKey('email')) {
      context.handle(
          _emailMeta, email.isAcceptableOrUnknown(data['email']!, _emailMeta));
    }
    if (data.containsKey('gmail')) {
      context.handle(
          _gmailMeta, gmail.isAcceptableOrUnknown(data['gmail']!, _gmailMeta));
    }
    if (data.containsKey('branch')) {
      context.handle(_branchMeta,
          branch.isAcceptableOrUnknown(data['branch']!, _branchMeta));
    }
    if (data.containsKey('designationName')) {
      context.handle(
          _designationNameMeta,
          designationName.isAcceptableOrUnknown(
              data['designationName']!, _designationNameMeta));
    }
    if (data.containsKey('designationInitial')) {
      context.handle(
          _designationInitialMeta,
          designationInitial.isAcceptableOrUnknown(
              data['designationInitial']!, _designationInitialMeta));
    }
    if (data.containsKey('avatar')) {
      context.handle(_avatarMeta,
          avatar.isAcceptableOrUnknown(data['avatar']!, _avatarMeta));
    }
    if (data.containsKey('contactNo')) {
      context.handle(_contactNoMeta,
          contactNo.isAcceptableOrUnknown(data['contactNo']!, _contactNoMeta));
    }
    if (data.containsKey('access')) {
      context.handle(_accessMeta,
          access.isAcceptableOrUnknown(data['access']!, _accessMeta));
    }
    if (data.containsKey('teamMemberID')) {
      context.handle(
          _teamMemberIDMeta,
          teamMemberID.isAcceptableOrUnknown(
              data['teamMemberID']!, _teamMemberIDMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Profile map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Profile(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      username: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}username']),
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name']),
      address: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}address']),
      email: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}email']),
      gmail: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}gmail']),
      branch: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}branch']),
      designationName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}designationName']),
      designationInitial: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}designationInitial']),
      avatar: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}avatar']),
      contactNo: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}contactNo']),
      access: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}access']),
      teamMemberID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}teamMemberID']),
    );
  }

  @override
  $ProfilesTable createAlias(String alias) {
    return $ProfilesTable(attachedDatabase, alias);
  }
}

class Profile extends DataClass implements Insertable<Profile> {
  final int id;
  final String? username;
  final String? name;
  final String? address;
  final String? email;
  final String? gmail;
  final String? branch;
  final String? designationName;
  final String? designationInitial;
  final String? avatar;
  final String? contactNo;
  final String? access;
  final int? teamMemberID;
  const Profile(
      {required this.id,
      this.username,
      this.name,
      this.address,
      this.email,
      this.gmail,
      this.branch,
      this.designationName,
      this.designationInitial,
      this.avatar,
      this.contactNo,
      this.access,
      this.teamMemberID});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    if (!nullToAbsent || username != null) {
      map['username'] = Variable<String>(username);
    }
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    if (!nullToAbsent || address != null) {
      map['address'] = Variable<String>(address);
    }
    if (!nullToAbsent || email != null) {
      map['email'] = Variable<String>(email);
    }
    if (!nullToAbsent || gmail != null) {
      map['gmail'] = Variable<String>(gmail);
    }
    if (!nullToAbsent || branch != null) {
      map['branch'] = Variable<String>(branch);
    }
    if (!nullToAbsent || designationName != null) {
      map['designationName'] = Variable<String>(designationName);
    }
    if (!nullToAbsent || designationInitial != null) {
      map['designationInitial'] = Variable<String>(designationInitial);
    }
    if (!nullToAbsent || avatar != null) {
      map['avatar'] = Variable<String>(avatar);
    }
    if (!nullToAbsent || contactNo != null) {
      map['contactNo'] = Variable<String>(contactNo);
    }
    if (!nullToAbsent || access != null) {
      map['access'] = Variable<String>(access);
    }
    if (!nullToAbsent || teamMemberID != null) {
      map['teamMemberID'] = Variable<int>(teamMemberID);
    }
    return map;
  }

  ProfilesCompanion toCompanion(bool nullToAbsent) {
    return ProfilesCompanion(
      id: Value(id),
      username: username == null && nullToAbsent
          ? const Value.absent()
          : Value(username),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      address: address == null && nullToAbsent
          ? const Value.absent()
          : Value(address),
      email:
          email == null && nullToAbsent ? const Value.absent() : Value(email),
      gmail:
          gmail == null && nullToAbsent ? const Value.absent() : Value(gmail),
      branch:
          branch == null && nullToAbsent ? const Value.absent() : Value(branch),
      designationName: designationName == null && nullToAbsent
          ? const Value.absent()
          : Value(designationName),
      designationInitial: designationInitial == null && nullToAbsent
          ? const Value.absent()
          : Value(designationInitial),
      avatar:
          avatar == null && nullToAbsent ? const Value.absent() : Value(avatar),
      contactNo: contactNo == null && nullToAbsent
          ? const Value.absent()
          : Value(contactNo),
      access:
          access == null && nullToAbsent ? const Value.absent() : Value(access),
      teamMemberID: teamMemberID == null && nullToAbsent
          ? const Value.absent()
          : Value(teamMemberID),
    );
  }

  factory Profile.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Profile(
      id: serializer.fromJson<int>(json['id']),
      username: serializer.fromJson<String?>(json['username']),
      name: serializer.fromJson<String?>(json['name']),
      address: serializer.fromJson<String?>(json['address']),
      email: serializer.fromJson<String?>(json['email']),
      gmail: serializer.fromJson<String?>(json['gmail']),
      branch: serializer.fromJson<String?>(json['branch']),
      designationName: serializer.fromJson<String?>(json['designationName']),
      designationInitial:
          serializer.fromJson<String?>(json['designationInitial']),
      avatar: serializer.fromJson<String?>(json['avatar']),
      contactNo: serializer.fromJson<String?>(json['contactNo']),
      access: serializer.fromJson<String?>(json['access']),
      teamMemberID: serializer.fromJson<int?>(json['teamMemberID']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'username': serializer.toJson<String?>(username),
      'name': serializer.toJson<String?>(name),
      'address': serializer.toJson<String?>(address),
      'email': serializer.toJson<String?>(email),
      'gmail': serializer.toJson<String?>(gmail),
      'branch': serializer.toJson<String?>(branch),
      'designationName': serializer.toJson<String?>(designationName),
      'designationInitial': serializer.toJson<String?>(designationInitial),
      'avatar': serializer.toJson<String?>(avatar),
      'contactNo': serializer.toJson<String?>(contactNo),
      'access': serializer.toJson<String?>(access),
      'teamMemberID': serializer.toJson<int?>(teamMemberID),
    };
  }

  Profile copyWith(
          {int? id,
          Value<String?> username = const Value.absent(),
          Value<String?> name = const Value.absent(),
          Value<String?> address = const Value.absent(),
          Value<String?> email = const Value.absent(),
          Value<String?> gmail = const Value.absent(),
          Value<String?> branch = const Value.absent(),
          Value<String?> designationName = const Value.absent(),
          Value<String?> designationInitial = const Value.absent(),
          Value<String?> avatar = const Value.absent(),
          Value<String?> contactNo = const Value.absent(),
          Value<String?> access = const Value.absent(),
          Value<int?> teamMemberID = const Value.absent()}) =>
      Profile(
        id: id ?? this.id,
        username: username.present ? username.value : this.username,
        name: name.present ? name.value : this.name,
        address: address.present ? address.value : this.address,
        email: email.present ? email.value : this.email,
        gmail: gmail.present ? gmail.value : this.gmail,
        branch: branch.present ? branch.value : this.branch,
        designationName: designationName.present
            ? designationName.value
            : this.designationName,
        designationInitial: designationInitial.present
            ? designationInitial.value
            : this.designationInitial,
        avatar: avatar.present ? avatar.value : this.avatar,
        contactNo: contactNo.present ? contactNo.value : this.contactNo,
        access: access.present ? access.value : this.access,
        teamMemberID:
            teamMemberID.present ? teamMemberID.value : this.teamMemberID,
      );
  Profile copyWithCompanion(ProfilesCompanion data) {
    return Profile(
      id: data.id.present ? data.id.value : this.id,
      username: data.username.present ? data.username.value : this.username,
      name: data.name.present ? data.name.value : this.name,
      address: data.address.present ? data.address.value : this.address,
      email: data.email.present ? data.email.value : this.email,
      gmail: data.gmail.present ? data.gmail.value : this.gmail,
      branch: data.branch.present ? data.branch.value : this.branch,
      designationName: data.designationName.present
          ? data.designationName.value
          : this.designationName,
      designationInitial: data.designationInitial.present
          ? data.designationInitial.value
          : this.designationInitial,
      avatar: data.avatar.present ? data.avatar.value : this.avatar,
      contactNo: data.contactNo.present ? data.contactNo.value : this.contactNo,
      access: data.access.present ? data.access.value : this.access,
      teamMemberID: data.teamMemberID.present
          ? data.teamMemberID.value
          : this.teamMemberID,
    );
  }

  @override
  String toString() {
    return (StringBuffer('Profile(')
          ..write('id: $id, ')
          ..write('username: $username, ')
          ..write('name: $name, ')
          ..write('address: $address, ')
          ..write('email: $email, ')
          ..write('gmail: $gmail, ')
          ..write('branch: $branch, ')
          ..write('designationName: $designationName, ')
          ..write('designationInitial: $designationInitial, ')
          ..write('avatar: $avatar, ')
          ..write('contactNo: $contactNo, ')
          ..write('access: $access, ')
          ..write('teamMemberID: $teamMemberID')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      id,
      username,
      name,
      address,
      email,
      gmail,
      branch,
      designationName,
      designationInitial,
      avatar,
      contactNo,
      access,
      teamMemberID);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Profile &&
          other.id == this.id &&
          other.username == this.username &&
          other.name == this.name &&
          other.address == this.address &&
          other.email == this.email &&
          other.gmail == this.gmail &&
          other.branch == this.branch &&
          other.designationName == this.designationName &&
          other.designationInitial == this.designationInitial &&
          other.avatar == this.avatar &&
          other.contactNo == this.contactNo &&
          other.access == this.access &&
          other.teamMemberID == this.teamMemberID);
}

class ProfilesCompanion extends UpdateCompanion<Profile> {
  final Value<int> id;
  final Value<String?> username;
  final Value<String?> name;
  final Value<String?> address;
  final Value<String?> email;
  final Value<String?> gmail;
  final Value<String?> branch;
  final Value<String?> designationName;
  final Value<String?> designationInitial;
  final Value<String?> avatar;
  final Value<String?> contactNo;
  final Value<String?> access;
  final Value<int?> teamMemberID;
  const ProfilesCompanion({
    this.id = const Value.absent(),
    this.username = const Value.absent(),
    this.name = const Value.absent(),
    this.address = const Value.absent(),
    this.email = const Value.absent(),
    this.gmail = const Value.absent(),
    this.branch = const Value.absent(),
    this.designationName = const Value.absent(),
    this.designationInitial = const Value.absent(),
    this.avatar = const Value.absent(),
    this.contactNo = const Value.absent(),
    this.access = const Value.absent(),
    this.teamMemberID = const Value.absent(),
  });
  ProfilesCompanion.insert({
    this.id = const Value.absent(),
    this.username = const Value.absent(),
    this.name = const Value.absent(),
    this.address = const Value.absent(),
    this.email = const Value.absent(),
    this.gmail = const Value.absent(),
    this.branch = const Value.absent(),
    this.designationName = const Value.absent(),
    this.designationInitial = const Value.absent(),
    this.avatar = const Value.absent(),
    this.contactNo = const Value.absent(),
    this.access = const Value.absent(),
    this.teamMemberID = const Value.absent(),
  });
  static Insertable<Profile> custom({
    Expression<int>? id,
    Expression<String>? username,
    Expression<String>? name,
    Expression<String>? address,
    Expression<String>? email,
    Expression<String>? gmail,
    Expression<String>? branch,
    Expression<String>? designationName,
    Expression<String>? designationInitial,
    Expression<String>? avatar,
    Expression<String>? contactNo,
    Expression<String>? access,
    Expression<int>? teamMemberID,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (username != null) 'username': username,
      if (name != null) 'name': name,
      if (address != null) 'address': address,
      if (email != null) 'email': email,
      if (gmail != null) 'gmail': gmail,
      if (branch != null) 'branch': branch,
      if (designationName != null) 'designationName': designationName,
      if (designationInitial != null) 'designationInitial': designationInitial,
      if (avatar != null) 'avatar': avatar,
      if (contactNo != null) 'contactNo': contactNo,
      if (access != null) 'access': access,
      if (teamMemberID != null) 'teamMemberID': teamMemberID,
    });
  }

  ProfilesCompanion copyWith(
      {Value<int>? id,
      Value<String?>? username,
      Value<String?>? name,
      Value<String?>? address,
      Value<String?>? email,
      Value<String?>? gmail,
      Value<String?>? branch,
      Value<String?>? designationName,
      Value<String?>? designationInitial,
      Value<String?>? avatar,
      Value<String?>? contactNo,
      Value<String?>? access,
      Value<int?>? teamMemberID}) {
    return ProfilesCompanion(
      id: id ?? this.id,
      username: username ?? this.username,
      name: name ?? this.name,
      address: address ?? this.address,
      email: email ?? this.email,
      gmail: gmail ?? this.gmail,
      branch: branch ?? this.branch,
      designationName: designationName ?? this.designationName,
      designationInitial: designationInitial ?? this.designationInitial,
      avatar: avatar ?? this.avatar,
      contactNo: contactNo ?? this.contactNo,
      access: access ?? this.access,
      teamMemberID: teamMemberID ?? this.teamMemberID,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (username.present) {
      map['username'] = Variable<String>(username.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (address.present) {
      map['address'] = Variable<String>(address.value);
    }
    if (email.present) {
      map['email'] = Variable<String>(email.value);
    }
    if (gmail.present) {
      map['gmail'] = Variable<String>(gmail.value);
    }
    if (branch.present) {
      map['branch'] = Variable<String>(branch.value);
    }
    if (designationName.present) {
      map['designationName'] = Variable<String>(designationName.value);
    }
    if (designationInitial.present) {
      map['designationInitial'] = Variable<String>(designationInitial.value);
    }
    if (avatar.present) {
      map['avatar'] = Variable<String>(avatar.value);
    }
    if (contactNo.present) {
      map['contactNo'] = Variable<String>(contactNo.value);
    }
    if (access.present) {
      map['access'] = Variable<String>(access.value);
    }
    if (teamMemberID.present) {
      map['teamMemberID'] = Variable<int>(teamMemberID.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ProfilesCompanion(')
          ..write('id: $id, ')
          ..write('username: $username, ')
          ..write('name: $name, ')
          ..write('address: $address, ')
          ..write('email: $email, ')
          ..write('gmail: $gmail, ')
          ..write('branch: $branch, ')
          ..write('designationName: $designationName, ')
          ..write('designationInitial: $designationInitial, ')
          ..write('avatar: $avatar, ')
          ..write('contactNo: $contactNo, ')
          ..write('access: $access, ')
          ..write('teamMemberID: $teamMemberID')
          ..write(')'))
        .toString();
  }
}

class $CostEstimationsTable extends CostEstimations
    with TableInfo<$CostEstimationsTable, CostEstimation> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $CostEstimationsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _costEstimationIDMeta =
      const VerificationMeta('costEstimationID');
  @override
  late final GeneratedColumn<int> costEstimationID = GeneratedColumn<int>(
      'costEstimationID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _costEstimationNameMeta =
      const VerificationMeta('costEstimationName');
  @override
  late final GeneratedColumn<String> costEstimationName =
      GeneratedColumn<String>('costEstimationName', aliasedName, true,
          type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _costEstimationTypeMeta =
      const VerificationMeta('costEstimationType');
  @override
  late final GeneratedColumn<String> costEstimationType =
      GeneratedColumn<String>('costEstimationType', aliasedName, true,
          type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _costEstimationTotalMeta =
      const VerificationMeta('costEstimationTotal');
  @override
  late final GeneratedColumn<String> costEstimationTotal =
      GeneratedColumn<String>('costEstimationTotal', aliasedName, true,
          type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _localFilePathMeta =
      const VerificationMeta('localFilePath');
  @override
  late final GeneratedColumn<String> localFilePath = GeneratedColumn<String>(
      'localFilePath', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _statusMeta = const VerificationMeta('status');
  @override
  late final GeneratedColumn<String> status = GeneratedColumn<String>(
      'status', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _totalCEQtyMeta =
      const VerificationMeta('totalCEQty');
  @override
  late final GeneratedColumn<int> totalCEQty = GeneratedColumn<int>(
      'totalCEQty', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _projectIDMeta =
      const VerificationMeta('projectID');
  @override
  late final GeneratedColumn<int> projectID = GeneratedColumn<int>(
      'projectID', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL REFERENCES workplaces(projectID)');
  @override
  List<GeneratedColumn> get $columns => [
        costEstimationID,
        costEstimationName,
        costEstimationType,
        costEstimationTotal,
        localFilePath,
        status,
        totalCEQty,
        projectID
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'cost_estimations';
  @override
  VerificationContext validateIntegrity(Insertable<CostEstimation> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('costEstimationID')) {
      context.handle(
          _costEstimationIDMeta,
          costEstimationID.isAcceptableOrUnknown(
              data['costEstimationID']!, _costEstimationIDMeta));
    } else if (isInserting) {
      context.missing(_costEstimationIDMeta);
    }
    if (data.containsKey('costEstimationName')) {
      context.handle(
          _costEstimationNameMeta,
          costEstimationName.isAcceptableOrUnknown(
              data['costEstimationName']!, _costEstimationNameMeta));
    }
    if (data.containsKey('costEstimationType')) {
      context.handle(
          _costEstimationTypeMeta,
          costEstimationType.isAcceptableOrUnknown(
              data['costEstimationType']!, _costEstimationTypeMeta));
    }
    if (data.containsKey('costEstimationTotal')) {
      context.handle(
          _costEstimationTotalMeta,
          costEstimationTotal.isAcceptableOrUnknown(
              data['costEstimationTotal']!, _costEstimationTotalMeta));
    }
    if (data.containsKey('localFilePath')) {
      context.handle(
          _localFilePathMeta,
          localFilePath.isAcceptableOrUnknown(
              data['localFilePath']!, _localFilePathMeta));
    }
    if (data.containsKey('status')) {
      context.handle(_statusMeta,
          status.isAcceptableOrUnknown(data['status']!, _statusMeta));
    }
    if (data.containsKey('totalCEQty')) {
      context.handle(
          _totalCEQtyMeta,
          totalCEQty.isAcceptableOrUnknown(
              data['totalCEQty']!, _totalCEQtyMeta));
    } else if (isInserting) {
      context.missing(_totalCEQtyMeta);
    }
    if (data.containsKey('projectID')) {
      context.handle(_projectIDMeta,
          projectID.isAcceptableOrUnknown(data['projectID']!, _projectIDMeta));
    } else if (isInserting) {
      context.missing(_projectIDMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {projectID, costEstimationID};
  @override
  CostEstimation map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return CostEstimation(
      costEstimationID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}costEstimationID'])!,
      costEstimationName: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}costEstimationName']),
      costEstimationType: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}costEstimationType']),
      costEstimationTotal: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}costEstimationTotal']),
      localFilePath: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}localFilePath']),
      status: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}status']),
      totalCEQty: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}totalCEQty'])!,
      projectID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}projectID'])!,
    );
  }

  @override
  $CostEstimationsTable createAlias(String alias) {
    return $CostEstimationsTable(attachedDatabase, alias);
  }
}

class CostEstimation extends DataClass implements Insertable<CostEstimation> {
  final int costEstimationID;
  final String? costEstimationName;
  final String? costEstimationType;
  final String? costEstimationTotal;
  final String? localFilePath;
  final String? status;
  final int totalCEQty;
  final int projectID;
  const CostEstimation(
      {required this.costEstimationID,
      this.costEstimationName,
      this.costEstimationType,
      this.costEstimationTotal,
      this.localFilePath,
      this.status,
      required this.totalCEQty,
      required this.projectID});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['costEstimationID'] = Variable<int>(costEstimationID);
    if (!nullToAbsent || costEstimationName != null) {
      map['costEstimationName'] = Variable<String>(costEstimationName);
    }
    if (!nullToAbsent || costEstimationType != null) {
      map['costEstimationType'] = Variable<String>(costEstimationType);
    }
    if (!nullToAbsent || costEstimationTotal != null) {
      map['costEstimationTotal'] = Variable<String>(costEstimationTotal);
    }
    if (!nullToAbsent || localFilePath != null) {
      map['localFilePath'] = Variable<String>(localFilePath);
    }
    if (!nullToAbsent || status != null) {
      map['status'] = Variable<String>(status);
    }
    map['totalCEQty'] = Variable<int>(totalCEQty);
    map['projectID'] = Variable<int>(projectID);
    return map;
  }

  CostEstimationsCompanion toCompanion(bool nullToAbsent) {
    return CostEstimationsCompanion(
      costEstimationID: Value(costEstimationID),
      costEstimationName: costEstimationName == null && nullToAbsent
          ? const Value.absent()
          : Value(costEstimationName),
      costEstimationType: costEstimationType == null && nullToAbsent
          ? const Value.absent()
          : Value(costEstimationType),
      costEstimationTotal: costEstimationTotal == null && nullToAbsent
          ? const Value.absent()
          : Value(costEstimationTotal),
      localFilePath: localFilePath == null && nullToAbsent
          ? const Value.absent()
          : Value(localFilePath),
      status:
          status == null && nullToAbsent ? const Value.absent() : Value(status),
      totalCEQty: Value(totalCEQty),
      projectID: Value(projectID),
    );
  }

  factory CostEstimation.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return CostEstimation(
      costEstimationID: serializer.fromJson<int>(json['costEstimationID']),
      costEstimationName:
          serializer.fromJson<String?>(json['costEstimationName']),
      costEstimationType:
          serializer.fromJson<String?>(json['costEstimationType']),
      costEstimationTotal:
          serializer.fromJson<String?>(json['costEstimationTotal']),
      localFilePath: serializer.fromJson<String?>(json['localFilePath']),
      status: serializer.fromJson<String?>(json['status']),
      totalCEQty: serializer.fromJson<int>(json['totalCEQty']),
      projectID: serializer.fromJson<int>(json['projectID']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'costEstimationID': serializer.toJson<int>(costEstimationID),
      'costEstimationName': serializer.toJson<String?>(costEstimationName),
      'costEstimationType': serializer.toJson<String?>(costEstimationType),
      'costEstimationTotal': serializer.toJson<String?>(costEstimationTotal),
      'localFilePath': serializer.toJson<String?>(localFilePath),
      'status': serializer.toJson<String?>(status),
      'totalCEQty': serializer.toJson<int>(totalCEQty),
      'projectID': serializer.toJson<int>(projectID),
    };
  }

  CostEstimation copyWith(
          {int? costEstimationID,
          Value<String?> costEstimationName = const Value.absent(),
          Value<String?> costEstimationType = const Value.absent(),
          Value<String?> costEstimationTotal = const Value.absent(),
          Value<String?> localFilePath = const Value.absent(),
          Value<String?> status = const Value.absent(),
          int? totalCEQty,
          int? projectID}) =>
      CostEstimation(
        costEstimationID: costEstimationID ?? this.costEstimationID,
        costEstimationName: costEstimationName.present
            ? costEstimationName.value
            : this.costEstimationName,
        costEstimationType: costEstimationType.present
            ? costEstimationType.value
            : this.costEstimationType,
        costEstimationTotal: costEstimationTotal.present
            ? costEstimationTotal.value
            : this.costEstimationTotal,
        localFilePath:
            localFilePath.present ? localFilePath.value : this.localFilePath,
        status: status.present ? status.value : this.status,
        totalCEQty: totalCEQty ?? this.totalCEQty,
        projectID: projectID ?? this.projectID,
      );
  CostEstimation copyWithCompanion(CostEstimationsCompanion data) {
    return CostEstimation(
      costEstimationID: data.costEstimationID.present
          ? data.costEstimationID.value
          : this.costEstimationID,
      costEstimationName: data.costEstimationName.present
          ? data.costEstimationName.value
          : this.costEstimationName,
      costEstimationType: data.costEstimationType.present
          ? data.costEstimationType.value
          : this.costEstimationType,
      costEstimationTotal: data.costEstimationTotal.present
          ? data.costEstimationTotal.value
          : this.costEstimationTotal,
      localFilePath: data.localFilePath.present
          ? data.localFilePath.value
          : this.localFilePath,
      status: data.status.present ? data.status.value : this.status,
      totalCEQty:
          data.totalCEQty.present ? data.totalCEQty.value : this.totalCEQty,
      projectID: data.projectID.present ? data.projectID.value : this.projectID,
    );
  }

  @override
  String toString() {
    return (StringBuffer('CostEstimation(')
          ..write('costEstimationID: $costEstimationID, ')
          ..write('costEstimationName: $costEstimationName, ')
          ..write('costEstimationType: $costEstimationType, ')
          ..write('costEstimationTotal: $costEstimationTotal, ')
          ..write('localFilePath: $localFilePath, ')
          ..write('status: $status, ')
          ..write('totalCEQty: $totalCEQty, ')
          ..write('projectID: $projectID')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      costEstimationID,
      costEstimationName,
      costEstimationType,
      costEstimationTotal,
      localFilePath,
      status,
      totalCEQty,
      projectID);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is CostEstimation &&
          other.costEstimationID == this.costEstimationID &&
          other.costEstimationName == this.costEstimationName &&
          other.costEstimationType == this.costEstimationType &&
          other.costEstimationTotal == this.costEstimationTotal &&
          other.localFilePath == this.localFilePath &&
          other.status == this.status &&
          other.totalCEQty == this.totalCEQty &&
          other.projectID == this.projectID);
}

class CostEstimationsCompanion extends UpdateCompanion<CostEstimation> {
  final Value<int> costEstimationID;
  final Value<String?> costEstimationName;
  final Value<String?> costEstimationType;
  final Value<String?> costEstimationTotal;
  final Value<String?> localFilePath;
  final Value<String?> status;
  final Value<int> totalCEQty;
  final Value<int> projectID;
  final Value<int> rowid;
  const CostEstimationsCompanion({
    this.costEstimationID = const Value.absent(),
    this.costEstimationName = const Value.absent(),
    this.costEstimationType = const Value.absent(),
    this.costEstimationTotal = const Value.absent(),
    this.localFilePath = const Value.absent(),
    this.status = const Value.absent(),
    this.totalCEQty = const Value.absent(),
    this.projectID = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  CostEstimationsCompanion.insert({
    required int costEstimationID,
    this.costEstimationName = const Value.absent(),
    this.costEstimationType = const Value.absent(),
    this.costEstimationTotal = const Value.absent(),
    this.localFilePath = const Value.absent(),
    this.status = const Value.absent(),
    required int totalCEQty,
    required int projectID,
    this.rowid = const Value.absent(),
  })  : costEstimationID = Value(costEstimationID),
        totalCEQty = Value(totalCEQty),
        projectID = Value(projectID);
  static Insertable<CostEstimation> custom({
    Expression<int>? costEstimationID,
    Expression<String>? costEstimationName,
    Expression<String>? costEstimationType,
    Expression<String>? costEstimationTotal,
    Expression<String>? localFilePath,
    Expression<String>? status,
    Expression<int>? totalCEQty,
    Expression<int>? projectID,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (costEstimationID != null) 'costEstimationID': costEstimationID,
      if (costEstimationName != null) 'costEstimationName': costEstimationName,
      if (costEstimationType != null) 'costEstimationType': costEstimationType,
      if (costEstimationTotal != null)
        'costEstimationTotal': costEstimationTotal,
      if (localFilePath != null) 'localFilePath': localFilePath,
      if (status != null) 'status': status,
      if (totalCEQty != null) 'totalCEQty': totalCEQty,
      if (projectID != null) 'projectID': projectID,
      if (rowid != null) 'rowid': rowid,
    });
  }

  CostEstimationsCompanion copyWith(
      {Value<int>? costEstimationID,
      Value<String?>? costEstimationName,
      Value<String?>? costEstimationType,
      Value<String?>? costEstimationTotal,
      Value<String?>? localFilePath,
      Value<String?>? status,
      Value<int>? totalCEQty,
      Value<int>? projectID,
      Value<int>? rowid}) {
    return CostEstimationsCompanion(
      costEstimationID: costEstimationID ?? this.costEstimationID,
      costEstimationName: costEstimationName ?? this.costEstimationName,
      costEstimationType: costEstimationType ?? this.costEstimationType,
      costEstimationTotal: costEstimationTotal ?? this.costEstimationTotal,
      localFilePath: localFilePath ?? this.localFilePath,
      status: status ?? this.status,
      totalCEQty: totalCEQty ?? this.totalCEQty,
      projectID: projectID ?? this.projectID,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (costEstimationID.present) {
      map['costEstimationID'] = Variable<int>(costEstimationID.value);
    }
    if (costEstimationName.present) {
      map['costEstimationName'] = Variable<String>(costEstimationName.value);
    }
    if (costEstimationType.present) {
      map['costEstimationType'] = Variable<String>(costEstimationType.value);
    }
    if (costEstimationTotal.present) {
      map['costEstimationTotal'] = Variable<String>(costEstimationTotal.value);
    }
    if (localFilePath.present) {
      map['localFilePath'] = Variable<String>(localFilePath.value);
    }
    if (status.present) {
      map['status'] = Variable<String>(status.value);
    }
    if (totalCEQty.present) {
      map['totalCEQty'] = Variable<int>(totalCEQty.value);
    }
    if (projectID.present) {
      map['projectID'] = Variable<int>(projectID.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('CostEstimationsCompanion(')
          ..write('costEstimationID: $costEstimationID, ')
          ..write('costEstimationName: $costEstimationName, ')
          ..write('costEstimationType: $costEstimationType, ')
          ..write('costEstimationTotal: $costEstimationTotal, ')
          ..write('localFilePath: $localFilePath, ')
          ..write('status: $status, ')
          ..write('totalCEQty: $totalCEQty, ')
          ..write('projectID: $projectID, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class $TaskMediasTable extends TaskMedias
    with TableInfo<$TaskMediasTable, TaskMedia> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $TaskMediasTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _taskMediaIDMeta =
      const VerificationMeta('taskMediaID');
  @override
  late final GeneratedColumn<int> taskMediaID = GeneratedColumn<int>(
      'taskMediaID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _mediaNameMeta =
      const VerificationMeta('mediaName');
  @override
  late final GeneratedColumn<String> mediaName = GeneratedColumn<String>(
      'mediaName', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _mediaTypeMeta =
      const VerificationMeta('mediaType');
  @override
  late final GeneratedColumn<String> mediaType = GeneratedColumn<String>(
      'mediaType', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _taskIDMeta = const VerificationMeta('taskID');
  @override
  late final GeneratedColumn<int> taskID = GeneratedColumn<int>(
      'taskID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _feedIDMeta = const VerificationMeta('feedID');
  @override
  late final GeneratedColumn<int> feedID = GeneratedColumn<int>(
      'feedID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _projectIDMeta =
      const VerificationMeta('projectID');
  @override
  late final GeneratedColumn<int> projectID = GeneratedColumn<int>(
      'projectID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _teamMemberIDMeta =
      const VerificationMeta('teamMemberID');
  @override
  late final GeneratedColumn<int> teamMemberID = GeneratedColumn<int>(
      'teamMemberID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _mediaPathMeta =
      const VerificationMeta('mediaPath');
  @override
  late final GeneratedColumn<String> mediaPath = GeneratedColumn<String>(
      'mediaPath', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _localFilePathMeta =
      const VerificationMeta('localFilePath');
  @override
  late final GeneratedColumn<String> localFilePath = GeneratedColumn<String>(
      'localFilePath', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [
        taskMediaID,
        mediaName,
        mediaType,
        taskID,
        feedID,
        projectID,
        teamMemberID,
        mediaPath,
        localFilePath
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'task_medias';
  @override
  VerificationContext validateIntegrity(Insertable<TaskMedia> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('taskMediaID')) {
      context.handle(
          _taskMediaIDMeta,
          taskMediaID.isAcceptableOrUnknown(
              data['taskMediaID']!, _taskMediaIDMeta));
    }
    if (data.containsKey('mediaName')) {
      context.handle(_mediaNameMeta,
          mediaName.isAcceptableOrUnknown(data['mediaName']!, _mediaNameMeta));
    }
    if (data.containsKey('mediaType')) {
      context.handle(_mediaTypeMeta,
          mediaType.isAcceptableOrUnknown(data['mediaType']!, _mediaTypeMeta));
    }
    if (data.containsKey('taskID')) {
      context.handle(_taskIDMeta,
          taskID.isAcceptableOrUnknown(data['taskID']!, _taskIDMeta));
    } else if (isInserting) {
      context.missing(_taskIDMeta);
    }
    if (data.containsKey('feedID')) {
      context.handle(_feedIDMeta,
          feedID.isAcceptableOrUnknown(data['feedID']!, _feedIDMeta));
    } else if (isInserting) {
      context.missing(_feedIDMeta);
    }
    if (data.containsKey('projectID')) {
      context.handle(_projectIDMeta,
          projectID.isAcceptableOrUnknown(data['projectID']!, _projectIDMeta));
    } else if (isInserting) {
      context.missing(_projectIDMeta);
    }
    if (data.containsKey('teamMemberID')) {
      context.handle(
          _teamMemberIDMeta,
          teamMemberID.isAcceptableOrUnknown(
              data['teamMemberID']!, _teamMemberIDMeta));
    } else if (isInserting) {
      context.missing(_teamMemberIDMeta);
    }
    if (data.containsKey('mediaPath')) {
      context.handle(_mediaPathMeta,
          mediaPath.isAcceptableOrUnknown(data['mediaPath']!, _mediaPathMeta));
    }
    if (data.containsKey('localFilePath')) {
      context.handle(
          _localFilePathMeta,
          localFilePath.isAcceptableOrUnknown(
              data['localFilePath']!, _localFilePathMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {taskMediaID};
  @override
  TaskMedia map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return TaskMedia(
      taskMediaID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}taskMediaID'])!,
      mediaName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}mediaName']),
      mediaType: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}mediaType']),
      taskID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}taskID'])!,
      feedID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}feedID'])!,
      projectID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}projectID'])!,
      teamMemberID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}teamMemberID'])!,
      mediaPath: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}mediaPath']),
      localFilePath: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}localFilePath']),
    );
  }

  @override
  $TaskMediasTable createAlias(String alias) {
    return $TaskMediasTable(attachedDatabase, alias);
  }
}

class TaskMedia extends DataClass implements Insertable<TaskMedia> {
  final int taskMediaID;
  final String? mediaName;
  final String? mediaType;
  final int taskID;
  final int feedID;
  final int projectID;
  final int teamMemberID;
  final String? mediaPath;
  final String? localFilePath;
  const TaskMedia(
      {required this.taskMediaID,
      this.mediaName,
      this.mediaType,
      required this.taskID,
      required this.feedID,
      required this.projectID,
      required this.teamMemberID,
      this.mediaPath,
      this.localFilePath});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['taskMediaID'] = Variable<int>(taskMediaID);
    if (!nullToAbsent || mediaName != null) {
      map['mediaName'] = Variable<String>(mediaName);
    }
    if (!nullToAbsent || mediaType != null) {
      map['mediaType'] = Variable<String>(mediaType);
    }
    map['taskID'] = Variable<int>(taskID);
    map['feedID'] = Variable<int>(feedID);
    map['projectID'] = Variable<int>(projectID);
    map['teamMemberID'] = Variable<int>(teamMemberID);
    if (!nullToAbsent || mediaPath != null) {
      map['mediaPath'] = Variable<String>(mediaPath);
    }
    if (!nullToAbsent || localFilePath != null) {
      map['localFilePath'] = Variable<String>(localFilePath);
    }
    return map;
  }

  TaskMediasCompanion toCompanion(bool nullToAbsent) {
    return TaskMediasCompanion(
      taskMediaID: Value(taskMediaID),
      mediaName: mediaName == null && nullToAbsent
          ? const Value.absent()
          : Value(mediaName),
      mediaType: mediaType == null && nullToAbsent
          ? const Value.absent()
          : Value(mediaType),
      taskID: Value(taskID),
      feedID: Value(feedID),
      projectID: Value(projectID),
      teamMemberID: Value(teamMemberID),
      mediaPath: mediaPath == null && nullToAbsent
          ? const Value.absent()
          : Value(mediaPath),
      localFilePath: localFilePath == null && nullToAbsent
          ? const Value.absent()
          : Value(localFilePath),
    );
  }

  factory TaskMedia.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return TaskMedia(
      taskMediaID: serializer.fromJson<int>(json['taskMediaID']),
      mediaName: serializer.fromJson<String?>(json['mediaName']),
      mediaType: serializer.fromJson<String?>(json['mediaType']),
      taskID: serializer.fromJson<int>(json['taskID']),
      feedID: serializer.fromJson<int>(json['feedID']),
      projectID: serializer.fromJson<int>(json['projectID']),
      teamMemberID: serializer.fromJson<int>(json['teamMemberID']),
      mediaPath: serializer.fromJson<String?>(json['mediaPath']),
      localFilePath: serializer.fromJson<String?>(json['localFilePath']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'taskMediaID': serializer.toJson<int>(taskMediaID),
      'mediaName': serializer.toJson<String?>(mediaName),
      'mediaType': serializer.toJson<String?>(mediaType),
      'taskID': serializer.toJson<int>(taskID),
      'feedID': serializer.toJson<int>(feedID),
      'projectID': serializer.toJson<int>(projectID),
      'teamMemberID': serializer.toJson<int>(teamMemberID),
      'mediaPath': serializer.toJson<String?>(mediaPath),
      'localFilePath': serializer.toJson<String?>(localFilePath),
    };
  }

  TaskMedia copyWith(
          {int? taskMediaID,
          Value<String?> mediaName = const Value.absent(),
          Value<String?> mediaType = const Value.absent(),
          int? taskID,
          int? feedID,
          int? projectID,
          int? teamMemberID,
          Value<String?> mediaPath = const Value.absent(),
          Value<String?> localFilePath = const Value.absent()}) =>
      TaskMedia(
        taskMediaID: taskMediaID ?? this.taskMediaID,
        mediaName: mediaName.present ? mediaName.value : this.mediaName,
        mediaType: mediaType.present ? mediaType.value : this.mediaType,
        taskID: taskID ?? this.taskID,
        feedID: feedID ?? this.feedID,
        projectID: projectID ?? this.projectID,
        teamMemberID: teamMemberID ?? this.teamMemberID,
        mediaPath: mediaPath.present ? mediaPath.value : this.mediaPath,
        localFilePath:
            localFilePath.present ? localFilePath.value : this.localFilePath,
      );
  TaskMedia copyWithCompanion(TaskMediasCompanion data) {
    return TaskMedia(
      taskMediaID:
          data.taskMediaID.present ? data.taskMediaID.value : this.taskMediaID,
      mediaName: data.mediaName.present ? data.mediaName.value : this.mediaName,
      mediaType: data.mediaType.present ? data.mediaType.value : this.mediaType,
      taskID: data.taskID.present ? data.taskID.value : this.taskID,
      feedID: data.feedID.present ? data.feedID.value : this.feedID,
      projectID: data.projectID.present ? data.projectID.value : this.projectID,
      teamMemberID: data.teamMemberID.present
          ? data.teamMemberID.value
          : this.teamMemberID,
      mediaPath: data.mediaPath.present ? data.mediaPath.value : this.mediaPath,
      localFilePath: data.localFilePath.present
          ? data.localFilePath.value
          : this.localFilePath,
    );
  }

  @override
  String toString() {
    return (StringBuffer('TaskMedia(')
          ..write('taskMediaID: $taskMediaID, ')
          ..write('mediaName: $mediaName, ')
          ..write('mediaType: $mediaType, ')
          ..write('taskID: $taskID, ')
          ..write('feedID: $feedID, ')
          ..write('projectID: $projectID, ')
          ..write('teamMemberID: $teamMemberID, ')
          ..write('mediaPath: $mediaPath, ')
          ..write('localFilePath: $localFilePath')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(taskMediaID, mediaName, mediaType, taskID,
      feedID, projectID, teamMemberID, mediaPath, localFilePath);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is TaskMedia &&
          other.taskMediaID == this.taskMediaID &&
          other.mediaName == this.mediaName &&
          other.mediaType == this.mediaType &&
          other.taskID == this.taskID &&
          other.feedID == this.feedID &&
          other.projectID == this.projectID &&
          other.teamMemberID == this.teamMemberID &&
          other.mediaPath == this.mediaPath &&
          other.localFilePath == this.localFilePath);
}

class TaskMediasCompanion extends UpdateCompanion<TaskMedia> {
  final Value<int> taskMediaID;
  final Value<String?> mediaName;
  final Value<String?> mediaType;
  final Value<int> taskID;
  final Value<int> feedID;
  final Value<int> projectID;
  final Value<int> teamMemberID;
  final Value<String?> mediaPath;
  final Value<String?> localFilePath;
  const TaskMediasCompanion({
    this.taskMediaID = const Value.absent(),
    this.mediaName = const Value.absent(),
    this.mediaType = const Value.absent(),
    this.taskID = const Value.absent(),
    this.feedID = const Value.absent(),
    this.projectID = const Value.absent(),
    this.teamMemberID = const Value.absent(),
    this.mediaPath = const Value.absent(),
    this.localFilePath = const Value.absent(),
  });
  TaskMediasCompanion.insert({
    this.taskMediaID = const Value.absent(),
    this.mediaName = const Value.absent(),
    this.mediaType = const Value.absent(),
    required int taskID,
    required int feedID,
    required int projectID,
    required int teamMemberID,
    this.mediaPath = const Value.absent(),
    this.localFilePath = const Value.absent(),
  })  : taskID = Value(taskID),
        feedID = Value(feedID),
        projectID = Value(projectID),
        teamMemberID = Value(teamMemberID);
  static Insertable<TaskMedia> custom({
    Expression<int>? taskMediaID,
    Expression<String>? mediaName,
    Expression<String>? mediaType,
    Expression<int>? taskID,
    Expression<int>? feedID,
    Expression<int>? projectID,
    Expression<int>? teamMemberID,
    Expression<String>? mediaPath,
    Expression<String>? localFilePath,
  }) {
    return RawValuesInsertable({
      if (taskMediaID != null) 'taskMediaID': taskMediaID,
      if (mediaName != null) 'mediaName': mediaName,
      if (mediaType != null) 'mediaType': mediaType,
      if (taskID != null) 'taskID': taskID,
      if (feedID != null) 'feedID': feedID,
      if (projectID != null) 'projectID': projectID,
      if (teamMemberID != null) 'teamMemberID': teamMemberID,
      if (mediaPath != null) 'mediaPath': mediaPath,
      if (localFilePath != null) 'localFilePath': localFilePath,
    });
  }

  TaskMediasCompanion copyWith(
      {Value<int>? taskMediaID,
      Value<String?>? mediaName,
      Value<String?>? mediaType,
      Value<int>? taskID,
      Value<int>? feedID,
      Value<int>? projectID,
      Value<int>? teamMemberID,
      Value<String?>? mediaPath,
      Value<String?>? localFilePath}) {
    return TaskMediasCompanion(
      taskMediaID: taskMediaID ?? this.taskMediaID,
      mediaName: mediaName ?? this.mediaName,
      mediaType: mediaType ?? this.mediaType,
      taskID: taskID ?? this.taskID,
      feedID: feedID ?? this.feedID,
      projectID: projectID ?? this.projectID,
      teamMemberID: teamMemberID ?? this.teamMemberID,
      mediaPath: mediaPath ?? this.mediaPath,
      localFilePath: localFilePath ?? this.localFilePath,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (taskMediaID.present) {
      map['taskMediaID'] = Variable<int>(taskMediaID.value);
    }
    if (mediaName.present) {
      map['mediaName'] = Variable<String>(mediaName.value);
    }
    if (mediaType.present) {
      map['mediaType'] = Variable<String>(mediaType.value);
    }
    if (taskID.present) {
      map['taskID'] = Variable<int>(taskID.value);
    }
    if (feedID.present) {
      map['feedID'] = Variable<int>(feedID.value);
    }
    if (projectID.present) {
      map['projectID'] = Variable<int>(projectID.value);
    }
    if (teamMemberID.present) {
      map['teamMemberID'] = Variable<int>(teamMemberID.value);
    }
    if (mediaPath.present) {
      map['mediaPath'] = Variable<String>(mediaPath.value);
    }
    if (localFilePath.present) {
      map['localFilePath'] = Variable<String>(localFilePath.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TaskMediasCompanion(')
          ..write('taskMediaID: $taskMediaID, ')
          ..write('mediaName: $mediaName, ')
          ..write('mediaType: $mediaType, ')
          ..write('taskID: $taskID, ')
          ..write('feedID: $feedID, ')
          ..write('projectID: $projectID, ')
          ..write('teamMemberID: $teamMemberID, ')
          ..write('mediaPath: $mediaPath, ')
          ..write('localFilePath: $localFilePath')
          ..write(')'))
        .toString();
  }
}

class $WorkplaceInfosTable extends WorkplaceInfos
    with TableInfo<$WorkplaceInfosTable, WorkplaceInfo> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $WorkplaceInfosTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _projectIDMeta =
      const VerificationMeta('projectID');
  @override
  late final GeneratedColumn<int> projectID = GeneratedColumn<int>(
      'projectID', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL REFERENCES workplaces(projectID)');
  static const VerificationMeta _infoLabelMeta =
      const VerificationMeta('infoLabel');
  @override
  late final GeneratedColumn<String> infoLabel = GeneratedColumn<String>(
      'infoLabel', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _infoDisplayValueMeta =
      const VerificationMeta('infoDisplayValue');
  @override
  late final GeneratedColumn<String> infoDisplayValue = GeneratedColumn<String>(
      'infoDisplayValue', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _sequanceMeta =
      const VerificationMeta('sequance');
  @override
  late final GeneratedColumn<int> sequance = GeneratedColumn<int>(
      'sequance', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultValue: const Constant(0));
  @override
  List<GeneratedColumn> get $columns =>
      [projectID, infoLabel, infoDisplayValue, sequance];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'workplace_infos';
  @override
  VerificationContext validateIntegrity(Insertable<WorkplaceInfo> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('projectID')) {
      context.handle(_projectIDMeta,
          projectID.isAcceptableOrUnknown(data['projectID']!, _projectIDMeta));
    } else if (isInserting) {
      context.missing(_projectIDMeta);
    }
    if (data.containsKey('infoLabel')) {
      context.handle(_infoLabelMeta,
          infoLabel.isAcceptableOrUnknown(data['infoLabel']!, _infoLabelMeta));
    }
    if (data.containsKey('infoDisplayValue')) {
      context.handle(
          _infoDisplayValueMeta,
          infoDisplayValue.isAcceptableOrUnknown(
              data['infoDisplayValue']!, _infoDisplayValueMeta));
    }
    if (data.containsKey('sequance')) {
      context.handle(_sequanceMeta,
          sequance.isAcceptableOrUnknown(data['sequance']!, _sequanceMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {projectID, infoLabel};
  @override
  WorkplaceInfo map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return WorkplaceInfo(
      projectID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}projectID'])!,
      infoLabel: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}infoLabel']),
      infoDisplayValue: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}infoDisplayValue']),
      sequance: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}sequance'])!,
    );
  }

  @override
  $WorkplaceInfosTable createAlias(String alias) {
    return $WorkplaceInfosTable(attachedDatabase, alias);
  }
}

class WorkplaceInfo extends DataClass implements Insertable<WorkplaceInfo> {
  final int projectID;
  final String? infoLabel;
  final String? infoDisplayValue;
  final int sequance;
  const WorkplaceInfo(
      {required this.projectID,
      this.infoLabel,
      this.infoDisplayValue,
      required this.sequance});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['projectID'] = Variable<int>(projectID);
    if (!nullToAbsent || infoLabel != null) {
      map['infoLabel'] = Variable<String>(infoLabel);
    }
    if (!nullToAbsent || infoDisplayValue != null) {
      map['infoDisplayValue'] = Variable<String>(infoDisplayValue);
    }
    map['sequance'] = Variable<int>(sequance);
    return map;
  }

  WorkplaceInfosCompanion toCompanion(bool nullToAbsent) {
    return WorkplaceInfosCompanion(
      projectID: Value(projectID),
      infoLabel: infoLabel == null && nullToAbsent
          ? const Value.absent()
          : Value(infoLabel),
      infoDisplayValue: infoDisplayValue == null && nullToAbsent
          ? const Value.absent()
          : Value(infoDisplayValue),
      sequance: Value(sequance),
    );
  }

  factory WorkplaceInfo.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return WorkplaceInfo(
      projectID: serializer.fromJson<int>(json['projectID']),
      infoLabel: serializer.fromJson<String?>(json['infoLabel']),
      infoDisplayValue: serializer.fromJson<String?>(json['infoDisplayValue']),
      sequance: serializer.fromJson<int>(json['sequance']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'projectID': serializer.toJson<int>(projectID),
      'infoLabel': serializer.toJson<String?>(infoLabel),
      'infoDisplayValue': serializer.toJson<String?>(infoDisplayValue),
      'sequance': serializer.toJson<int>(sequance),
    };
  }

  WorkplaceInfo copyWith(
          {int? projectID,
          Value<String?> infoLabel = const Value.absent(),
          Value<String?> infoDisplayValue = const Value.absent(),
          int? sequance}) =>
      WorkplaceInfo(
        projectID: projectID ?? this.projectID,
        infoLabel: infoLabel.present ? infoLabel.value : this.infoLabel,
        infoDisplayValue: infoDisplayValue.present
            ? infoDisplayValue.value
            : this.infoDisplayValue,
        sequance: sequance ?? this.sequance,
      );
  WorkplaceInfo copyWithCompanion(WorkplaceInfosCompanion data) {
    return WorkplaceInfo(
      projectID: data.projectID.present ? data.projectID.value : this.projectID,
      infoLabel: data.infoLabel.present ? data.infoLabel.value : this.infoLabel,
      infoDisplayValue: data.infoDisplayValue.present
          ? data.infoDisplayValue.value
          : this.infoDisplayValue,
      sequance: data.sequance.present ? data.sequance.value : this.sequance,
    );
  }

  @override
  String toString() {
    return (StringBuffer('WorkplaceInfo(')
          ..write('projectID: $projectID, ')
          ..write('infoLabel: $infoLabel, ')
          ..write('infoDisplayValue: $infoDisplayValue, ')
          ..write('sequance: $sequance')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(projectID, infoLabel, infoDisplayValue, sequance);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is WorkplaceInfo &&
          other.projectID == this.projectID &&
          other.infoLabel == this.infoLabel &&
          other.infoDisplayValue == this.infoDisplayValue &&
          other.sequance == this.sequance);
}

class WorkplaceInfosCompanion extends UpdateCompanion<WorkplaceInfo> {
  final Value<int> projectID;
  final Value<String?> infoLabel;
  final Value<String?> infoDisplayValue;
  final Value<int> sequance;
  final Value<int> rowid;
  const WorkplaceInfosCompanion({
    this.projectID = const Value.absent(),
    this.infoLabel = const Value.absent(),
    this.infoDisplayValue = const Value.absent(),
    this.sequance = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  WorkplaceInfosCompanion.insert({
    required int projectID,
    this.infoLabel = const Value.absent(),
    this.infoDisplayValue = const Value.absent(),
    this.sequance = const Value.absent(),
    this.rowid = const Value.absent(),
  }) : projectID = Value(projectID);
  static Insertable<WorkplaceInfo> custom({
    Expression<int>? projectID,
    Expression<String>? infoLabel,
    Expression<String>? infoDisplayValue,
    Expression<int>? sequance,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (projectID != null) 'projectID': projectID,
      if (infoLabel != null) 'infoLabel': infoLabel,
      if (infoDisplayValue != null) 'infoDisplayValue': infoDisplayValue,
      if (sequance != null) 'sequance': sequance,
      if (rowid != null) 'rowid': rowid,
    });
  }

  WorkplaceInfosCompanion copyWith(
      {Value<int>? projectID,
      Value<String?>? infoLabel,
      Value<String?>? infoDisplayValue,
      Value<int>? sequance,
      Value<int>? rowid}) {
    return WorkplaceInfosCompanion(
      projectID: projectID ?? this.projectID,
      infoLabel: infoLabel ?? this.infoLabel,
      infoDisplayValue: infoDisplayValue ?? this.infoDisplayValue,
      sequance: sequance ?? this.sequance,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (projectID.present) {
      map['projectID'] = Variable<int>(projectID.value);
    }
    if (infoLabel.present) {
      map['infoLabel'] = Variable<String>(infoLabel.value);
    }
    if (infoDisplayValue.present) {
      map['infoDisplayValue'] = Variable<String>(infoDisplayValue.value);
    }
    if (sequance.present) {
      map['sequance'] = Variable<int>(sequance.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('WorkplaceInfosCompanion(')
          ..write('projectID: $projectID, ')
          ..write('infoLabel: $infoLabel, ')
          ..write('infoDisplayValue: $infoDisplayValue, ')
          ..write('sequance: $sequance, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class $AssociateMediasTable extends AssociateMedias
    with TableInfo<$AssociateMediasTable, AssociateMedia> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $AssociateMediasTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _mediaIDMeta =
      const VerificationMeta('mediaID');
  @override
  late final GeneratedColumn<int> mediaID = GeneratedColumn<int>(
      'mediaID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _projectIDMeta =
      const VerificationMeta('projectID');
  @override
  late final GeneratedColumn<int> projectID = GeneratedColumn<int>(
      'projectID', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _projectFeedsMediaMeta =
      const VerificationMeta('projectFeedsMedia');
  @override
  late final GeneratedColumn<String> projectFeedsMedia =
      GeneratedColumn<String>('projectFeedsMedia', aliasedName, true,
          type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _projectFeedsMediaTypeMeta =
      const VerificationMeta('projectFeedsMediaType');
  @override
  late final GeneratedColumn<String> projectFeedsMediaType =
      GeneratedColumn<String>('projectFeedsMediaType', aliasedName, true,
          type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _createdDateTimeMeta =
      const VerificationMeta('createdDateTime');
  @override
  late final GeneratedColumn<String> createdDateTime = GeneratedColumn<String>(
      'createdDateTime', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _captionMeta =
      const VerificationMeta('caption');
  @override
  late final GeneratedColumn<String> caption = GeneratedColumn<String>(
      'caption', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _moduleMeta = const VerificationMeta('module');
  @override
  late final GeneratedColumn<String> module = GeneratedColumn<String>(
      'module', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _localFilePathMeta =
      const VerificationMeta('localFilePath');
  @override
  late final GeneratedColumn<String> localFilePath = GeneratedColumn<String>(
      'localFilePath', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [
        mediaID,
        projectID,
        projectFeedsMedia,
        projectFeedsMediaType,
        createdDateTime,
        caption,
        module,
        localFilePath
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'associate_medias';
  @override
  VerificationContext validateIntegrity(Insertable<AssociateMedia> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('mediaID')) {
      context.handle(_mediaIDMeta,
          mediaID.isAcceptableOrUnknown(data['mediaID']!, _mediaIDMeta));
    } else if (isInserting) {
      context.missing(_mediaIDMeta);
    }
    if (data.containsKey('projectID')) {
      context.handle(_projectIDMeta,
          projectID.isAcceptableOrUnknown(data['projectID']!, _projectIDMeta));
    } else if (isInserting) {
      context.missing(_projectIDMeta);
    }
    if (data.containsKey('projectFeedsMedia')) {
      context.handle(
          _projectFeedsMediaMeta,
          projectFeedsMedia.isAcceptableOrUnknown(
              data['projectFeedsMedia']!, _projectFeedsMediaMeta));
    }
    if (data.containsKey('projectFeedsMediaType')) {
      context.handle(
          _projectFeedsMediaTypeMeta,
          projectFeedsMediaType.isAcceptableOrUnknown(
              data['projectFeedsMediaType']!, _projectFeedsMediaTypeMeta));
    }
    if (data.containsKey('createdDateTime')) {
      context.handle(
          _createdDateTimeMeta,
          createdDateTime.isAcceptableOrUnknown(
              data['createdDateTime']!, _createdDateTimeMeta));
    } else if (isInserting) {
      context.missing(_createdDateTimeMeta);
    }
    if (data.containsKey('caption')) {
      context.handle(_captionMeta,
          caption.isAcceptableOrUnknown(data['caption']!, _captionMeta));
    } else if (isInserting) {
      context.missing(_captionMeta);
    }
    if (data.containsKey('module')) {
      context.handle(_moduleMeta,
          module.isAcceptableOrUnknown(data['module']!, _moduleMeta));
    } else if (isInserting) {
      context.missing(_moduleMeta);
    }
    if (data.containsKey('localFilePath')) {
      context.handle(
          _localFilePathMeta,
          localFilePath.isAcceptableOrUnknown(
              data['localFilePath']!, _localFilePathMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {mediaID, module};
  @override
  AssociateMedia map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return AssociateMedia(
      mediaID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}mediaID'])!,
      projectID: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}projectID'])!,
      projectFeedsMedia: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}projectFeedsMedia']),
      projectFeedsMediaType: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}projectFeedsMediaType']),
      createdDateTime: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}createdDateTime'])!,
      caption: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}caption'])!,
      module: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}module'])!,
      localFilePath: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}localFilePath']),
    );
  }

  @override
  $AssociateMediasTable createAlias(String alias) {
    return $AssociateMediasTable(attachedDatabase, alias);
  }
}

class AssociateMedia extends DataClass implements Insertable<AssociateMedia> {
  final int mediaID;
  final int projectID;
  final String? projectFeedsMedia;
  final String? projectFeedsMediaType;
  final String createdDateTime;
  final String caption;
  final String module;
  final String? localFilePath;
  const AssociateMedia(
      {required this.mediaID,
      required this.projectID,
      this.projectFeedsMedia,
      this.projectFeedsMediaType,
      required this.createdDateTime,
      required this.caption,
      required this.module,
      this.localFilePath});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['mediaID'] = Variable<int>(mediaID);
    map['projectID'] = Variable<int>(projectID);
    if (!nullToAbsent || projectFeedsMedia != null) {
      map['projectFeedsMedia'] = Variable<String>(projectFeedsMedia);
    }
    if (!nullToAbsent || projectFeedsMediaType != null) {
      map['projectFeedsMediaType'] = Variable<String>(projectFeedsMediaType);
    }
    map['createdDateTime'] = Variable<String>(createdDateTime);
    map['caption'] = Variable<String>(caption);
    map['module'] = Variable<String>(module);
    if (!nullToAbsent || localFilePath != null) {
      map['localFilePath'] = Variable<String>(localFilePath);
    }
    return map;
  }

  AssociateMediasCompanion toCompanion(bool nullToAbsent) {
    return AssociateMediasCompanion(
      mediaID: Value(mediaID),
      projectID: Value(projectID),
      projectFeedsMedia: projectFeedsMedia == null && nullToAbsent
          ? const Value.absent()
          : Value(projectFeedsMedia),
      projectFeedsMediaType: projectFeedsMediaType == null && nullToAbsent
          ? const Value.absent()
          : Value(projectFeedsMediaType),
      createdDateTime: Value(createdDateTime),
      caption: Value(caption),
      module: Value(module),
      localFilePath: localFilePath == null && nullToAbsent
          ? const Value.absent()
          : Value(localFilePath),
    );
  }

  factory AssociateMedia.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return AssociateMedia(
      mediaID: serializer.fromJson<int>(json['mediaID']),
      projectID: serializer.fromJson<int>(json['projectID']),
      projectFeedsMedia:
          serializer.fromJson<String?>(json['projectFeedsMedia']),
      projectFeedsMediaType:
          serializer.fromJson<String?>(json['projectFeedsMediaType']),
      createdDateTime: serializer.fromJson<String>(json['createdDateTime']),
      caption: serializer.fromJson<String>(json['caption']),
      module: serializer.fromJson<String>(json['module']),
      localFilePath: serializer.fromJson<String?>(json['localFilePath']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'mediaID': serializer.toJson<int>(mediaID),
      'projectID': serializer.toJson<int>(projectID),
      'projectFeedsMedia': serializer.toJson<String?>(projectFeedsMedia),
      'projectFeedsMediaType':
          serializer.toJson<String?>(projectFeedsMediaType),
      'createdDateTime': serializer.toJson<String>(createdDateTime),
      'caption': serializer.toJson<String>(caption),
      'module': serializer.toJson<String>(module),
      'localFilePath': serializer.toJson<String?>(localFilePath),
    };
  }

  AssociateMedia copyWith(
          {int? mediaID,
          int? projectID,
          Value<String?> projectFeedsMedia = const Value.absent(),
          Value<String?> projectFeedsMediaType = const Value.absent(),
          String? createdDateTime,
          String? caption,
          String? module,
          Value<String?> localFilePath = const Value.absent()}) =>
      AssociateMedia(
        mediaID: mediaID ?? this.mediaID,
        projectID: projectID ?? this.projectID,
        projectFeedsMedia: projectFeedsMedia.present
            ? projectFeedsMedia.value
            : this.projectFeedsMedia,
        projectFeedsMediaType: projectFeedsMediaType.present
            ? projectFeedsMediaType.value
            : this.projectFeedsMediaType,
        createdDateTime: createdDateTime ?? this.createdDateTime,
        caption: caption ?? this.caption,
        module: module ?? this.module,
        localFilePath:
            localFilePath.present ? localFilePath.value : this.localFilePath,
      );
  AssociateMedia copyWithCompanion(AssociateMediasCompanion data) {
    return AssociateMedia(
      mediaID: data.mediaID.present ? data.mediaID.value : this.mediaID,
      projectID: data.projectID.present ? data.projectID.value : this.projectID,
      projectFeedsMedia: data.projectFeedsMedia.present
          ? data.projectFeedsMedia.value
          : this.projectFeedsMedia,
      projectFeedsMediaType: data.projectFeedsMediaType.present
          ? data.projectFeedsMediaType.value
          : this.projectFeedsMediaType,
      createdDateTime: data.createdDateTime.present
          ? data.createdDateTime.value
          : this.createdDateTime,
      caption: data.caption.present ? data.caption.value : this.caption,
      module: data.module.present ? data.module.value : this.module,
      localFilePath: data.localFilePath.present
          ? data.localFilePath.value
          : this.localFilePath,
    );
  }

  @override
  String toString() {
    return (StringBuffer('AssociateMedia(')
          ..write('mediaID: $mediaID, ')
          ..write('projectID: $projectID, ')
          ..write('projectFeedsMedia: $projectFeedsMedia, ')
          ..write('projectFeedsMediaType: $projectFeedsMediaType, ')
          ..write('createdDateTime: $createdDateTime, ')
          ..write('caption: $caption, ')
          ..write('module: $module, ')
          ..write('localFilePath: $localFilePath')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(mediaID, projectID, projectFeedsMedia,
      projectFeedsMediaType, createdDateTime, caption, module, localFilePath);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is AssociateMedia &&
          other.mediaID == this.mediaID &&
          other.projectID == this.projectID &&
          other.projectFeedsMedia == this.projectFeedsMedia &&
          other.projectFeedsMediaType == this.projectFeedsMediaType &&
          other.createdDateTime == this.createdDateTime &&
          other.caption == this.caption &&
          other.module == this.module &&
          other.localFilePath == this.localFilePath);
}

class AssociateMediasCompanion extends UpdateCompanion<AssociateMedia> {
  final Value<int> mediaID;
  final Value<int> projectID;
  final Value<String?> projectFeedsMedia;
  final Value<String?> projectFeedsMediaType;
  final Value<String> createdDateTime;
  final Value<String> caption;
  final Value<String> module;
  final Value<String?> localFilePath;
  final Value<int> rowid;
  const AssociateMediasCompanion({
    this.mediaID = const Value.absent(),
    this.projectID = const Value.absent(),
    this.projectFeedsMedia = const Value.absent(),
    this.projectFeedsMediaType = const Value.absent(),
    this.createdDateTime = const Value.absent(),
    this.caption = const Value.absent(),
    this.module = const Value.absent(),
    this.localFilePath = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  AssociateMediasCompanion.insert({
    required int mediaID,
    required int projectID,
    this.projectFeedsMedia = const Value.absent(),
    this.projectFeedsMediaType = const Value.absent(),
    required String createdDateTime,
    required String caption,
    required String module,
    this.localFilePath = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : mediaID = Value(mediaID),
        projectID = Value(projectID),
        createdDateTime = Value(createdDateTime),
        caption = Value(caption),
        module = Value(module);
  static Insertable<AssociateMedia> custom({
    Expression<int>? mediaID,
    Expression<int>? projectID,
    Expression<String>? projectFeedsMedia,
    Expression<String>? projectFeedsMediaType,
    Expression<String>? createdDateTime,
    Expression<String>? caption,
    Expression<String>? module,
    Expression<String>? localFilePath,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (mediaID != null) 'mediaID': mediaID,
      if (projectID != null) 'projectID': projectID,
      if (projectFeedsMedia != null) 'projectFeedsMedia': projectFeedsMedia,
      if (projectFeedsMediaType != null)
        'projectFeedsMediaType': projectFeedsMediaType,
      if (createdDateTime != null) 'createdDateTime': createdDateTime,
      if (caption != null) 'caption': caption,
      if (module != null) 'module': module,
      if (localFilePath != null) 'localFilePath': localFilePath,
      if (rowid != null) 'rowid': rowid,
    });
  }

  AssociateMediasCompanion copyWith(
      {Value<int>? mediaID,
      Value<int>? projectID,
      Value<String?>? projectFeedsMedia,
      Value<String?>? projectFeedsMediaType,
      Value<String>? createdDateTime,
      Value<String>? caption,
      Value<String>? module,
      Value<String?>? localFilePath,
      Value<int>? rowid}) {
    return AssociateMediasCompanion(
      mediaID: mediaID ?? this.mediaID,
      projectID: projectID ?? this.projectID,
      projectFeedsMedia: projectFeedsMedia ?? this.projectFeedsMedia,
      projectFeedsMediaType:
          projectFeedsMediaType ?? this.projectFeedsMediaType,
      createdDateTime: createdDateTime ?? this.createdDateTime,
      caption: caption ?? this.caption,
      module: module ?? this.module,
      localFilePath: localFilePath ?? this.localFilePath,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (mediaID.present) {
      map['mediaID'] = Variable<int>(mediaID.value);
    }
    if (projectID.present) {
      map['projectID'] = Variable<int>(projectID.value);
    }
    if (projectFeedsMedia.present) {
      map['projectFeedsMedia'] = Variable<String>(projectFeedsMedia.value);
    }
    if (projectFeedsMediaType.present) {
      map['projectFeedsMediaType'] =
          Variable<String>(projectFeedsMediaType.value);
    }
    if (createdDateTime.present) {
      map['createdDateTime'] = Variable<String>(createdDateTime.value);
    }
    if (caption.present) {
      map['caption'] = Variable<String>(caption.value);
    }
    if (module.present) {
      map['module'] = Variable<String>(module.value);
    }
    if (localFilePath.present) {
      map['localFilePath'] = Variable<String>(localFilePath.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('AssociateMediasCompanion(')
          ..write('mediaID: $mediaID, ')
          ..write('projectID: $projectID, ')
          ..write('projectFeedsMedia: $projectFeedsMedia, ')
          ..write('projectFeedsMediaType: $projectFeedsMediaType, ')
          ..write('createdDateTime: $createdDateTime, ')
          ..write('caption: $caption, ')
          ..write('module: $module, ')
          ..write('localFilePath: $localFilePath, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(e);
  $AppDatabaseManager get managers => $AppDatabaseManager(this);
  late final $WorkplacesTable workplaces = $WorkplacesTable(this);
  late final $TeamMembersTable teamMembers = $TeamMembersTable(this);
  late final $TopicsTable topics = $TopicsTable(this);
  late final $FeedsTable feeds = $FeedsTable(this);
  late final $MediasTable medias = $MediasTable(this);
  late final $AlertsTable alerts = $AlertsTable(this);
  late final $ProfilesTable profiles = $ProfilesTable(this);
  late final $CostEstimationsTable costEstimations =
      $CostEstimationsTable(this);
  late final $TaskMediasTable taskMedias = $TaskMediasTable(this);
  late final $WorkplaceInfosTable workplaceInfos = $WorkplaceInfosTable(this);
  late final $AssociateMediasTable associateMedias =
      $AssociateMediasTable(this);
  late final WorkplaceDao workplaceDao = WorkplaceDao(this as AppDatabase);
  late final TeamMemberDao teamMemberDao = TeamMemberDao(this as AppDatabase);
  late final TopicDao topicDao = TopicDao(this as AppDatabase);
  late final FeedDao feedDao = FeedDao(this as AppDatabase);
  late final MediaDao mediaDao = MediaDao(this as AppDatabase);
  late final AlertDao alertDao = AlertDao(this as AppDatabase);
  late final ProfileDao profileDao = ProfileDao(this as AppDatabase);
  late final CostEstimationDao costEstimationDao =
      CostEstimationDao(this as AppDatabase);
  late final TaskMediaDao taskMediaDao = TaskMediaDao(this as AppDatabase);
  late final WorkplaceInfoDao workplaceInfoDao =
      WorkplaceInfoDao(this as AppDatabase);
  late final AssociateMediaDao associateMediaDao =
      AssociateMediaDao(this as AppDatabase);
  @override
  Iterable<TableInfo<Table, Object?>> get allTables =>
      allSchemaEntities.whereType<TableInfo<Table, Object?>>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [
        workplaces,
        teamMembers,
        topics,
        feeds,
        medias,
        alerts,
        profiles,
        costEstimations,
        taskMedias,
        workplaceInfos,
        associateMedias
      ];
}

typedef $$WorkplacesTableCreateCompanionBuilder = WorkplacesCompanion Function({
  Value<int> projectID,
  Value<String?> projectArea,
  Value<String?> projectUnit,
  Value<int?> projectSize,
  Value<int?> projectBedroom,
  Value<int?> projectBathroom,
  Value<String?> marketSegment,
  Value<String?> themeName,
  Value<String?> startDateTime,
  Value<String?> targetDateTime,
  Value<String?> dueDateTime,
  Value<String?> projectStatus,
  Value<String?> projectCategory,
  Value<String?> ownerName,
  Value<String?> propertyName,
  Value<String?> propertyState,
  Value<String?> propertyType,
  Value<String?> isRead,
  Value<String?> lastViewedDateTime,
  Value<int?> lastReadFeedID,
  Value<String?> updatedDateTime,
  Value<String?> displayDateTime,
  Value<int?> completedTasks,
  Value<int?> totalTasks,
});
typedef $$WorkplacesTableUpdateCompanionBuilder = WorkplacesCompanion Function({
  Value<int> projectID,
  Value<String?> projectArea,
  Value<String?> projectUnit,
  Value<int?> projectSize,
  Value<int?> projectBedroom,
  Value<int?> projectBathroom,
  Value<String?> marketSegment,
  Value<String?> themeName,
  Value<String?> startDateTime,
  Value<String?> targetDateTime,
  Value<String?> dueDateTime,
  Value<String?> projectStatus,
  Value<String?> projectCategory,
  Value<String?> ownerName,
  Value<String?> propertyName,
  Value<String?> propertyState,
  Value<String?> propertyType,
  Value<String?> isRead,
  Value<String?> lastViewedDateTime,
  Value<int?> lastReadFeedID,
  Value<String?> updatedDateTime,
  Value<String?> displayDateTime,
  Value<int?> completedTasks,
  Value<int?> totalTasks,
});

class $$WorkplacesTableTableManager extends RootTableManager<
    _$AppDatabase,
    $WorkplacesTable,
    Workplace,
    $$WorkplacesTableFilterComposer,
    $$WorkplacesTableOrderingComposer,
    $$WorkplacesTableCreateCompanionBuilder,
    $$WorkplacesTableUpdateCompanionBuilder> {
  $$WorkplacesTableTableManager(_$AppDatabase db, $WorkplacesTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$WorkplacesTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$WorkplacesTableOrderingComposer(ComposerState(db, table)),
          updateCompanionCallback: ({
            Value<int> projectID = const Value.absent(),
            Value<String?> projectArea = const Value.absent(),
            Value<String?> projectUnit = const Value.absent(),
            Value<int?> projectSize = const Value.absent(),
            Value<int?> projectBedroom = const Value.absent(),
            Value<int?> projectBathroom = const Value.absent(),
            Value<String?> marketSegment = const Value.absent(),
            Value<String?> themeName = const Value.absent(),
            Value<String?> startDateTime = const Value.absent(),
            Value<String?> targetDateTime = const Value.absent(),
            Value<String?> dueDateTime = const Value.absent(),
            Value<String?> projectStatus = const Value.absent(),
            Value<String?> projectCategory = const Value.absent(),
            Value<String?> ownerName = const Value.absent(),
            Value<String?> propertyName = const Value.absent(),
            Value<String?> propertyState = const Value.absent(),
            Value<String?> propertyType = const Value.absent(),
            Value<String?> isRead = const Value.absent(),
            Value<String?> lastViewedDateTime = const Value.absent(),
            Value<int?> lastReadFeedID = const Value.absent(),
            Value<String?> updatedDateTime = const Value.absent(),
            Value<String?> displayDateTime = const Value.absent(),
            Value<int?> completedTasks = const Value.absent(),
            Value<int?> totalTasks = const Value.absent(),
          }) =>
              WorkplacesCompanion(
            projectID: projectID,
            projectArea: projectArea,
            projectUnit: projectUnit,
            projectSize: projectSize,
            projectBedroom: projectBedroom,
            projectBathroom: projectBathroom,
            marketSegment: marketSegment,
            themeName: themeName,
            startDateTime: startDateTime,
            targetDateTime: targetDateTime,
            dueDateTime: dueDateTime,
            projectStatus: projectStatus,
            projectCategory: projectCategory,
            ownerName: ownerName,
            propertyName: propertyName,
            propertyState: propertyState,
            propertyType: propertyType,
            isRead: isRead,
            lastViewedDateTime: lastViewedDateTime,
            lastReadFeedID: lastReadFeedID,
            updatedDateTime: updatedDateTime,
            displayDateTime: displayDateTime,
            completedTasks: completedTasks,
            totalTasks: totalTasks,
          ),
          createCompanionCallback: ({
            Value<int> projectID = const Value.absent(),
            Value<String?> projectArea = const Value.absent(),
            Value<String?> projectUnit = const Value.absent(),
            Value<int?> projectSize = const Value.absent(),
            Value<int?> projectBedroom = const Value.absent(),
            Value<int?> projectBathroom = const Value.absent(),
            Value<String?> marketSegment = const Value.absent(),
            Value<String?> themeName = const Value.absent(),
            Value<String?> startDateTime = const Value.absent(),
            Value<String?> targetDateTime = const Value.absent(),
            Value<String?> dueDateTime = const Value.absent(),
            Value<String?> projectStatus = const Value.absent(),
            Value<String?> projectCategory = const Value.absent(),
            Value<String?> ownerName = const Value.absent(),
            Value<String?> propertyName = const Value.absent(),
            Value<String?> propertyState = const Value.absent(),
            Value<String?> propertyType = const Value.absent(),
            Value<String?> isRead = const Value.absent(),
            Value<String?> lastViewedDateTime = const Value.absent(),
            Value<int?> lastReadFeedID = const Value.absent(),
            Value<String?> updatedDateTime = const Value.absent(),
            Value<String?> displayDateTime = const Value.absent(),
            Value<int?> completedTasks = const Value.absent(),
            Value<int?> totalTasks = const Value.absent(),
          }) =>
              WorkplacesCompanion.insert(
            projectID: projectID,
            projectArea: projectArea,
            projectUnit: projectUnit,
            projectSize: projectSize,
            projectBedroom: projectBedroom,
            projectBathroom: projectBathroom,
            marketSegment: marketSegment,
            themeName: themeName,
            startDateTime: startDateTime,
            targetDateTime: targetDateTime,
            dueDateTime: dueDateTime,
            projectStatus: projectStatus,
            projectCategory: projectCategory,
            ownerName: ownerName,
            propertyName: propertyName,
            propertyState: propertyState,
            propertyType: propertyType,
            isRead: isRead,
            lastViewedDateTime: lastViewedDateTime,
            lastReadFeedID: lastReadFeedID,
            updatedDateTime: updatedDateTime,
            displayDateTime: displayDateTime,
            completedTasks: completedTasks,
            totalTasks: totalTasks,
          ),
        ));
}

class $$WorkplacesTableFilterComposer
    extends FilterComposer<_$AppDatabase, $WorkplacesTable> {
  $$WorkplacesTableFilterComposer(super.$state);
  ColumnFilters<int> get projectID => $state.composableBuilder(
      column: $state.table.projectID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get projectArea => $state.composableBuilder(
      column: $state.table.projectArea,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get projectUnit => $state.composableBuilder(
      column: $state.table.projectUnit,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get projectSize => $state.composableBuilder(
      column: $state.table.projectSize,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get projectBedroom => $state.composableBuilder(
      column: $state.table.projectBedroom,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get projectBathroom => $state.composableBuilder(
      column: $state.table.projectBathroom,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get marketSegment => $state.composableBuilder(
      column: $state.table.marketSegment,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get themeName => $state.composableBuilder(
      column: $state.table.themeName,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get startDateTime => $state.composableBuilder(
      column: $state.table.startDateTime,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get targetDateTime => $state.composableBuilder(
      column: $state.table.targetDateTime,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get dueDateTime => $state.composableBuilder(
      column: $state.table.dueDateTime,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get projectStatus => $state.composableBuilder(
      column: $state.table.projectStatus,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get projectCategory => $state.composableBuilder(
      column: $state.table.projectCategory,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get ownerName => $state.composableBuilder(
      column: $state.table.ownerName,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get propertyName => $state.composableBuilder(
      column: $state.table.propertyName,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get propertyState => $state.composableBuilder(
      column: $state.table.propertyState,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get propertyType => $state.composableBuilder(
      column: $state.table.propertyType,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get isRead => $state.composableBuilder(
      column: $state.table.isRead,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get lastViewedDateTime => $state.composableBuilder(
      column: $state.table.lastViewedDateTime,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get lastReadFeedID => $state.composableBuilder(
      column: $state.table.lastReadFeedID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get updatedDateTime => $state.composableBuilder(
      column: $state.table.updatedDateTime,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get displayDateTime => $state.composableBuilder(
      column: $state.table.displayDateTime,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get completedTasks => $state.composableBuilder(
      column: $state.table.completedTasks,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get totalTasks => $state.composableBuilder(
      column: $state.table.totalTasks,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ComposableFilter teamMembersRefs(
      ComposableFilter Function($$TeamMembersTableFilterComposer f) f) {
    final $$TeamMembersTableFilterComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.projectID,
        referencedTable: $state.db.teamMembers,
        getReferencedColumn: (t) => t.projectID,
        builder: (joinBuilder, parentComposers) =>
            $$TeamMembersTableFilterComposer(ComposerState($state.db,
                $state.db.teamMembers, joinBuilder, parentComposers)));
    return f(composer);
  }

  ComposableFilter costEstimationsRefs(
      ComposableFilter Function($$CostEstimationsTableFilterComposer f) f) {
    final $$CostEstimationsTableFilterComposer composer =
        $state.composerBuilder(
            composer: this,
            getCurrentColumn: (t) => t.projectID,
            referencedTable: $state.db.costEstimations,
            getReferencedColumn: (t) => t.projectID,
            builder: (joinBuilder, parentComposers) =>
                $$CostEstimationsTableFilterComposer(ComposerState($state.db,
                    $state.db.costEstimations, joinBuilder, parentComposers)));
    return f(composer);
  }

  ComposableFilter workplaceInfosRefs(
      ComposableFilter Function($$WorkplaceInfosTableFilterComposer f) f) {
    final $$WorkplaceInfosTableFilterComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.projectID,
        referencedTable: $state.db.workplaceInfos,
        getReferencedColumn: (t) => t.projectID,
        builder: (joinBuilder, parentComposers) =>
            $$WorkplaceInfosTableFilterComposer(ComposerState($state.db,
                $state.db.workplaceInfos, joinBuilder, parentComposers)));
    return f(composer);
  }
}

class $$WorkplacesTableOrderingComposer
    extends OrderingComposer<_$AppDatabase, $WorkplacesTable> {
  $$WorkplacesTableOrderingComposer(super.$state);
  ColumnOrderings<int> get projectID => $state.composableBuilder(
      column: $state.table.projectID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get projectArea => $state.composableBuilder(
      column: $state.table.projectArea,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get projectUnit => $state.composableBuilder(
      column: $state.table.projectUnit,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get projectSize => $state.composableBuilder(
      column: $state.table.projectSize,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get projectBedroom => $state.composableBuilder(
      column: $state.table.projectBedroom,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get projectBathroom => $state.composableBuilder(
      column: $state.table.projectBathroom,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get marketSegment => $state.composableBuilder(
      column: $state.table.marketSegment,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get themeName => $state.composableBuilder(
      column: $state.table.themeName,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get startDateTime => $state.composableBuilder(
      column: $state.table.startDateTime,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get targetDateTime => $state.composableBuilder(
      column: $state.table.targetDateTime,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get dueDateTime => $state.composableBuilder(
      column: $state.table.dueDateTime,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get projectStatus => $state.composableBuilder(
      column: $state.table.projectStatus,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get projectCategory => $state.composableBuilder(
      column: $state.table.projectCategory,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get ownerName => $state.composableBuilder(
      column: $state.table.ownerName,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get propertyName => $state.composableBuilder(
      column: $state.table.propertyName,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get propertyState => $state.composableBuilder(
      column: $state.table.propertyState,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get propertyType => $state.composableBuilder(
      column: $state.table.propertyType,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get isRead => $state.composableBuilder(
      column: $state.table.isRead,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get lastViewedDateTime => $state.composableBuilder(
      column: $state.table.lastViewedDateTime,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get lastReadFeedID => $state.composableBuilder(
      column: $state.table.lastReadFeedID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get updatedDateTime => $state.composableBuilder(
      column: $state.table.updatedDateTime,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get displayDateTime => $state.composableBuilder(
      column: $state.table.displayDateTime,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get completedTasks => $state.composableBuilder(
      column: $state.table.completedTasks,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get totalTasks => $state.composableBuilder(
      column: $state.table.totalTasks,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

typedef $$TeamMembersTableCreateCompanionBuilder = TeamMembersCompanion
    Function({
  required int teamMemberID,
  Value<String?> teamMemberName,
  Value<String?> teamMemberAvatar,
  Value<String?> designationName,
  required int projectID,
  Value<bool> show,
  Value<int> rowid,
});
typedef $$TeamMembersTableUpdateCompanionBuilder = TeamMembersCompanion
    Function({
  Value<int> teamMemberID,
  Value<String?> teamMemberName,
  Value<String?> teamMemberAvatar,
  Value<String?> designationName,
  Value<int> projectID,
  Value<bool> show,
  Value<int> rowid,
});

class $$TeamMembersTableTableManager extends RootTableManager<
    _$AppDatabase,
    $TeamMembersTable,
    TeamMember,
    $$TeamMembersTableFilterComposer,
    $$TeamMembersTableOrderingComposer,
    $$TeamMembersTableCreateCompanionBuilder,
    $$TeamMembersTableUpdateCompanionBuilder> {
  $$TeamMembersTableTableManager(_$AppDatabase db, $TeamMembersTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$TeamMembersTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$TeamMembersTableOrderingComposer(ComposerState(db, table)),
          updateCompanionCallback: ({
            Value<int> teamMemberID = const Value.absent(),
            Value<String?> teamMemberName = const Value.absent(),
            Value<String?> teamMemberAvatar = const Value.absent(),
            Value<String?> designationName = const Value.absent(),
            Value<int> projectID = const Value.absent(),
            Value<bool> show = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              TeamMembersCompanion(
            teamMemberID: teamMemberID,
            teamMemberName: teamMemberName,
            teamMemberAvatar: teamMemberAvatar,
            designationName: designationName,
            projectID: projectID,
            show: show,
            rowid: rowid,
          ),
          createCompanionCallback: ({
            required int teamMemberID,
            Value<String?> teamMemberName = const Value.absent(),
            Value<String?> teamMemberAvatar = const Value.absent(),
            Value<String?> designationName = const Value.absent(),
            required int projectID,
            Value<bool> show = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              TeamMembersCompanion.insert(
            teamMemberID: teamMemberID,
            teamMemberName: teamMemberName,
            teamMemberAvatar: teamMemberAvatar,
            designationName: designationName,
            projectID: projectID,
            show: show,
            rowid: rowid,
          ),
        ));
}

class $$TeamMembersTableFilterComposer
    extends FilterComposer<_$AppDatabase, $TeamMembersTable> {
  $$TeamMembersTableFilterComposer(super.$state);
  ColumnFilters<int> get teamMemberID => $state.composableBuilder(
      column: $state.table.teamMemberID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get teamMemberName => $state.composableBuilder(
      column: $state.table.teamMemberName,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get teamMemberAvatar => $state.composableBuilder(
      column: $state.table.teamMemberAvatar,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get designationName => $state.composableBuilder(
      column: $state.table.designationName,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<bool> get show => $state.composableBuilder(
      column: $state.table.show,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  $$WorkplacesTableFilterComposer get projectID {
    final $$WorkplacesTableFilterComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.projectID,
        referencedTable: $state.db.workplaces,
        getReferencedColumn: (t) => t.projectID,
        builder: (joinBuilder, parentComposers) =>
            $$WorkplacesTableFilterComposer(ComposerState($state.db,
                $state.db.workplaces, joinBuilder, parentComposers)));
    return composer;
  }
}

class $$TeamMembersTableOrderingComposer
    extends OrderingComposer<_$AppDatabase, $TeamMembersTable> {
  $$TeamMembersTableOrderingComposer(super.$state);
  ColumnOrderings<int> get teamMemberID => $state.composableBuilder(
      column: $state.table.teamMemberID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get teamMemberName => $state.composableBuilder(
      column: $state.table.teamMemberName,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get teamMemberAvatar => $state.composableBuilder(
      column: $state.table.teamMemberAvatar,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get designationName => $state.composableBuilder(
      column: $state.table.designationName,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<bool> get show => $state.composableBuilder(
      column: $state.table.show,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  $$WorkplacesTableOrderingComposer get projectID {
    final $$WorkplacesTableOrderingComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.projectID,
        referencedTable: $state.db.workplaces,
        getReferencedColumn: (t) => t.projectID,
        builder: (joinBuilder, parentComposers) =>
            $$WorkplacesTableOrderingComposer(ComposerState($state.db,
                $state.db.workplaces, joinBuilder, parentComposers)));
    return composer;
  }
}

typedef $$TopicsTableCreateCompanionBuilder = TopicsCompanion Function({
  Value<int> topicID,
  Value<String?> topicName,
  Value<String?> topicIcon,
  Value<String?> topicColour,
  Value<int> weight,
});
typedef $$TopicsTableUpdateCompanionBuilder = TopicsCompanion Function({
  Value<int> topicID,
  Value<String?> topicName,
  Value<String?> topicIcon,
  Value<String?> topicColour,
  Value<int> weight,
});

class $$TopicsTableTableManager extends RootTableManager<
    _$AppDatabase,
    $TopicsTable,
    Topic,
    $$TopicsTableFilterComposer,
    $$TopicsTableOrderingComposer,
    $$TopicsTableCreateCompanionBuilder,
    $$TopicsTableUpdateCompanionBuilder> {
  $$TopicsTableTableManager(_$AppDatabase db, $TopicsTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$TopicsTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$TopicsTableOrderingComposer(ComposerState(db, table)),
          updateCompanionCallback: ({
            Value<int> topicID = const Value.absent(),
            Value<String?> topicName = const Value.absent(),
            Value<String?> topicIcon = const Value.absent(),
            Value<String?> topicColour = const Value.absent(),
            Value<int> weight = const Value.absent(),
          }) =>
              TopicsCompanion(
            topicID: topicID,
            topicName: topicName,
            topicIcon: topicIcon,
            topicColour: topicColour,
            weight: weight,
          ),
          createCompanionCallback: ({
            Value<int> topicID = const Value.absent(),
            Value<String?> topicName = const Value.absent(),
            Value<String?> topicIcon = const Value.absent(),
            Value<String?> topicColour = const Value.absent(),
            Value<int> weight = const Value.absent(),
          }) =>
              TopicsCompanion.insert(
            topicID: topicID,
            topicName: topicName,
            topicIcon: topicIcon,
            topicColour: topicColour,
            weight: weight,
          ),
        ));
}

class $$TopicsTableFilterComposer
    extends FilterComposer<_$AppDatabase, $TopicsTable> {
  $$TopicsTableFilterComposer(super.$state);
  ColumnFilters<int> get topicID => $state.composableBuilder(
      column: $state.table.topicID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get topicName => $state.composableBuilder(
      column: $state.table.topicName,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get topicIcon => $state.composableBuilder(
      column: $state.table.topicIcon,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get topicColour => $state.composableBuilder(
      column: $state.table.topicColour,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get weight => $state.composableBuilder(
      column: $state.table.weight,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));
}

class $$TopicsTableOrderingComposer
    extends OrderingComposer<_$AppDatabase, $TopicsTable> {
  $$TopicsTableOrderingComposer(super.$state);
  ColumnOrderings<int> get topicID => $state.composableBuilder(
      column: $state.table.topicID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get topicName => $state.composableBuilder(
      column: $state.table.topicName,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get topicIcon => $state.composableBuilder(
      column: $state.table.topicIcon,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get topicColour => $state.composableBuilder(
      column: $state.table.topicColour,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get weight => $state.composableBuilder(
      column: $state.table.weight,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

typedef $$FeedsTableCreateCompanionBuilder = FeedsCompanion Function({
  Value<String?> uuid,
  Value<int> feedID,
  Value<int?> parentID,
  required int projectID,
  Value<String?> description,
  Value<String?> originDesc,
  Value<String?> feedType,
  Value<String?> postDateTime,
  Value<String?> updatedDateTime,
  Value<int?> topicID,
  Value<int?> teamMemberID,
  Value<String?> moduleID,
  Value<String?> moduleName,
  Value<String?> updatesDescription,
  Value<String?> replyTeamMemberAvatar,
  Value<int> replyCount,
  Value<String?> replyReadStatus,
  Value<String?> backgroundColor,
  Value<String?> outlineColor,
  Value<String?> fontNameColor,
  Value<String?> fontPositionColor,
  Value<String?> fontDescriptionColor,
  Value<String?> fontDateColor,
  Value<int?> lastReadFeedID,
  required int sendStatus,
  Value<String> isRead,
});
typedef $$FeedsTableUpdateCompanionBuilder = FeedsCompanion Function({
  Value<String?> uuid,
  Value<int> feedID,
  Value<int?> parentID,
  Value<int> projectID,
  Value<String?> description,
  Value<String?> originDesc,
  Value<String?> feedType,
  Value<String?> postDateTime,
  Value<String?> updatedDateTime,
  Value<int?> topicID,
  Value<int?> teamMemberID,
  Value<String?> moduleID,
  Value<String?> moduleName,
  Value<String?> updatesDescription,
  Value<String?> replyTeamMemberAvatar,
  Value<int> replyCount,
  Value<String?> replyReadStatus,
  Value<String?> backgroundColor,
  Value<String?> outlineColor,
  Value<String?> fontNameColor,
  Value<String?> fontPositionColor,
  Value<String?> fontDescriptionColor,
  Value<String?> fontDateColor,
  Value<int?> lastReadFeedID,
  Value<int> sendStatus,
  Value<String> isRead,
});

class $$FeedsTableTableManager extends RootTableManager<
    _$AppDatabase,
    $FeedsTable,
    Feed,
    $$FeedsTableFilterComposer,
    $$FeedsTableOrderingComposer,
    $$FeedsTableCreateCompanionBuilder,
    $$FeedsTableUpdateCompanionBuilder> {
  $$FeedsTableTableManager(_$AppDatabase db, $FeedsTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$FeedsTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$FeedsTableOrderingComposer(ComposerState(db, table)),
          updateCompanionCallback: ({
            Value<String?> uuid = const Value.absent(),
            Value<int> feedID = const Value.absent(),
            Value<int?> parentID = const Value.absent(),
            Value<int> projectID = const Value.absent(),
            Value<String?> description = const Value.absent(),
            Value<String?> originDesc = const Value.absent(),
            Value<String?> feedType = const Value.absent(),
            Value<String?> postDateTime = const Value.absent(),
            Value<String?> updatedDateTime = const Value.absent(),
            Value<int?> topicID = const Value.absent(),
            Value<int?> teamMemberID = const Value.absent(),
            Value<String?> moduleID = const Value.absent(),
            Value<String?> moduleName = const Value.absent(),
            Value<String?> updatesDescription = const Value.absent(),
            Value<String?> replyTeamMemberAvatar = const Value.absent(),
            Value<int> replyCount = const Value.absent(),
            Value<String?> replyReadStatus = const Value.absent(),
            Value<String?> backgroundColor = const Value.absent(),
            Value<String?> outlineColor = const Value.absent(),
            Value<String?> fontNameColor = const Value.absent(),
            Value<String?> fontPositionColor = const Value.absent(),
            Value<String?> fontDescriptionColor = const Value.absent(),
            Value<String?> fontDateColor = const Value.absent(),
            Value<int?> lastReadFeedID = const Value.absent(),
            Value<int> sendStatus = const Value.absent(),
            Value<String> isRead = const Value.absent(),
          }) =>
              FeedsCompanion(
            uuid: uuid,
            feedID: feedID,
            parentID: parentID,
            projectID: projectID,
            description: description,
            originDesc: originDesc,
            feedType: feedType,
            postDateTime: postDateTime,
            updatedDateTime: updatedDateTime,
            topicID: topicID,
            teamMemberID: teamMemberID,
            moduleID: moduleID,
            moduleName: moduleName,
            updatesDescription: updatesDescription,
            replyTeamMemberAvatar: replyTeamMemberAvatar,
            replyCount: replyCount,
            replyReadStatus: replyReadStatus,
            backgroundColor: backgroundColor,
            outlineColor: outlineColor,
            fontNameColor: fontNameColor,
            fontPositionColor: fontPositionColor,
            fontDescriptionColor: fontDescriptionColor,
            fontDateColor: fontDateColor,
            lastReadFeedID: lastReadFeedID,
            sendStatus: sendStatus,
            isRead: isRead,
          ),
          createCompanionCallback: ({
            Value<String?> uuid = const Value.absent(),
            Value<int> feedID = const Value.absent(),
            Value<int?> parentID = const Value.absent(),
            required int projectID,
            Value<String?> description = const Value.absent(),
            Value<String?> originDesc = const Value.absent(),
            Value<String?> feedType = const Value.absent(),
            Value<String?> postDateTime = const Value.absent(),
            Value<String?> updatedDateTime = const Value.absent(),
            Value<int?> topicID = const Value.absent(),
            Value<int?> teamMemberID = const Value.absent(),
            Value<String?> moduleID = const Value.absent(),
            Value<String?> moduleName = const Value.absent(),
            Value<String?> updatesDescription = const Value.absent(),
            Value<String?> replyTeamMemberAvatar = const Value.absent(),
            Value<int> replyCount = const Value.absent(),
            Value<String?> replyReadStatus = const Value.absent(),
            Value<String?> backgroundColor = const Value.absent(),
            Value<String?> outlineColor = const Value.absent(),
            Value<String?> fontNameColor = const Value.absent(),
            Value<String?> fontPositionColor = const Value.absent(),
            Value<String?> fontDescriptionColor = const Value.absent(),
            Value<String?> fontDateColor = const Value.absent(),
            Value<int?> lastReadFeedID = const Value.absent(),
            required int sendStatus,
            Value<String> isRead = const Value.absent(),
          }) =>
              FeedsCompanion.insert(
            uuid: uuid,
            feedID: feedID,
            parentID: parentID,
            projectID: projectID,
            description: description,
            originDesc: originDesc,
            feedType: feedType,
            postDateTime: postDateTime,
            updatedDateTime: updatedDateTime,
            topicID: topicID,
            teamMemberID: teamMemberID,
            moduleID: moduleID,
            moduleName: moduleName,
            updatesDescription: updatesDescription,
            replyTeamMemberAvatar: replyTeamMemberAvatar,
            replyCount: replyCount,
            replyReadStatus: replyReadStatus,
            backgroundColor: backgroundColor,
            outlineColor: outlineColor,
            fontNameColor: fontNameColor,
            fontPositionColor: fontPositionColor,
            fontDescriptionColor: fontDescriptionColor,
            fontDateColor: fontDateColor,
            lastReadFeedID: lastReadFeedID,
            sendStatus: sendStatus,
            isRead: isRead,
          ),
        ));
}

class $$FeedsTableFilterComposer
    extends FilterComposer<_$AppDatabase, $FeedsTable> {
  $$FeedsTableFilterComposer(super.$state);
  ColumnFilters<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get feedID => $state.composableBuilder(
      column: $state.table.feedID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get parentID => $state.composableBuilder(
      column: $state.table.parentID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get projectID => $state.composableBuilder(
      column: $state.table.projectID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get description => $state.composableBuilder(
      column: $state.table.description,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get originDesc => $state.composableBuilder(
      column: $state.table.originDesc,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get feedType => $state.composableBuilder(
      column: $state.table.feedType,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get postDateTime => $state.composableBuilder(
      column: $state.table.postDateTime,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get updatedDateTime => $state.composableBuilder(
      column: $state.table.updatedDateTime,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get topicID => $state.composableBuilder(
      column: $state.table.topicID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get teamMemberID => $state.composableBuilder(
      column: $state.table.teamMemberID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get moduleID => $state.composableBuilder(
      column: $state.table.moduleID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get moduleName => $state.composableBuilder(
      column: $state.table.moduleName,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get updatesDescription => $state.composableBuilder(
      column: $state.table.updatesDescription,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get replyTeamMemberAvatar => $state.composableBuilder(
      column: $state.table.replyTeamMemberAvatar,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get replyCount => $state.composableBuilder(
      column: $state.table.replyCount,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get replyReadStatus => $state.composableBuilder(
      column: $state.table.replyReadStatus,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get backgroundColor => $state.composableBuilder(
      column: $state.table.backgroundColor,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get outlineColor => $state.composableBuilder(
      column: $state.table.outlineColor,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get fontNameColor => $state.composableBuilder(
      column: $state.table.fontNameColor,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get fontPositionColor => $state.composableBuilder(
      column: $state.table.fontPositionColor,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get fontDescriptionColor => $state.composableBuilder(
      column: $state.table.fontDescriptionColor,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get fontDateColor => $state.composableBuilder(
      column: $state.table.fontDateColor,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get lastReadFeedID => $state.composableBuilder(
      column: $state.table.lastReadFeedID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get sendStatus => $state.composableBuilder(
      column: $state.table.sendStatus,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get isRead => $state.composableBuilder(
      column: $state.table.isRead,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));
}

class $$FeedsTableOrderingComposer
    extends OrderingComposer<_$AppDatabase, $FeedsTable> {
  $$FeedsTableOrderingComposer(super.$state);
  ColumnOrderings<String> get uuid => $state.composableBuilder(
      column: $state.table.uuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get feedID => $state.composableBuilder(
      column: $state.table.feedID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get parentID => $state.composableBuilder(
      column: $state.table.parentID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get projectID => $state.composableBuilder(
      column: $state.table.projectID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get description => $state.composableBuilder(
      column: $state.table.description,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get originDesc => $state.composableBuilder(
      column: $state.table.originDesc,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get feedType => $state.composableBuilder(
      column: $state.table.feedType,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get postDateTime => $state.composableBuilder(
      column: $state.table.postDateTime,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get updatedDateTime => $state.composableBuilder(
      column: $state.table.updatedDateTime,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get topicID => $state.composableBuilder(
      column: $state.table.topicID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get teamMemberID => $state.composableBuilder(
      column: $state.table.teamMemberID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get moduleID => $state.composableBuilder(
      column: $state.table.moduleID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get moduleName => $state.composableBuilder(
      column: $state.table.moduleName,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get updatesDescription => $state.composableBuilder(
      column: $state.table.updatesDescription,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get replyTeamMemberAvatar => $state.composableBuilder(
      column: $state.table.replyTeamMemberAvatar,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get replyCount => $state.composableBuilder(
      column: $state.table.replyCount,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get replyReadStatus => $state.composableBuilder(
      column: $state.table.replyReadStatus,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get backgroundColor => $state.composableBuilder(
      column: $state.table.backgroundColor,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get outlineColor => $state.composableBuilder(
      column: $state.table.outlineColor,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get fontNameColor => $state.composableBuilder(
      column: $state.table.fontNameColor,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get fontPositionColor => $state.composableBuilder(
      column: $state.table.fontPositionColor,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get fontDescriptionColor => $state.composableBuilder(
      column: $state.table.fontDescriptionColor,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get fontDateColor => $state.composableBuilder(
      column: $state.table.fontDateColor,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get lastReadFeedID => $state.composableBuilder(
      column: $state.table.lastReadFeedID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get sendStatus => $state.composableBuilder(
      column: $state.table.sendStatus,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get isRead => $state.composableBuilder(
      column: $state.table.isRead,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

typedef $$MediasTableCreateCompanionBuilder = MediasCompanion Function({
  Value<int> mediaID,
  required int projectID,
  required int feedID,
  Value<String?> url,
  Value<String?> fileExtension,
  Value<String?> localFilePath,
  Value<String?> feedUuid,
});
typedef $$MediasTableUpdateCompanionBuilder = MediasCompanion Function({
  Value<int> mediaID,
  Value<int> projectID,
  Value<int> feedID,
  Value<String?> url,
  Value<String?> fileExtension,
  Value<String?> localFilePath,
  Value<String?> feedUuid,
});

class $$MediasTableTableManager extends RootTableManager<
    _$AppDatabase,
    $MediasTable,
    Media,
    $$MediasTableFilterComposer,
    $$MediasTableOrderingComposer,
    $$MediasTableCreateCompanionBuilder,
    $$MediasTableUpdateCompanionBuilder> {
  $$MediasTableTableManager(_$AppDatabase db, $MediasTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$MediasTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$MediasTableOrderingComposer(ComposerState(db, table)),
          updateCompanionCallback: ({
            Value<int> mediaID = const Value.absent(),
            Value<int> projectID = const Value.absent(),
            Value<int> feedID = const Value.absent(),
            Value<String?> url = const Value.absent(),
            Value<String?> fileExtension = const Value.absent(),
            Value<String?> localFilePath = const Value.absent(),
            Value<String?> feedUuid = const Value.absent(),
          }) =>
              MediasCompanion(
            mediaID: mediaID,
            projectID: projectID,
            feedID: feedID,
            url: url,
            fileExtension: fileExtension,
            localFilePath: localFilePath,
            feedUuid: feedUuid,
          ),
          createCompanionCallback: ({
            Value<int> mediaID = const Value.absent(),
            required int projectID,
            required int feedID,
            Value<String?> url = const Value.absent(),
            Value<String?> fileExtension = const Value.absent(),
            Value<String?> localFilePath = const Value.absent(),
            Value<String?> feedUuid = const Value.absent(),
          }) =>
              MediasCompanion.insert(
            mediaID: mediaID,
            projectID: projectID,
            feedID: feedID,
            url: url,
            fileExtension: fileExtension,
            localFilePath: localFilePath,
            feedUuid: feedUuid,
          ),
        ));
}

class $$MediasTableFilterComposer
    extends FilterComposer<_$AppDatabase, $MediasTable> {
  $$MediasTableFilterComposer(super.$state);
  ColumnFilters<int> get mediaID => $state.composableBuilder(
      column: $state.table.mediaID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get projectID => $state.composableBuilder(
      column: $state.table.projectID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get feedID => $state.composableBuilder(
      column: $state.table.feedID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get url => $state.composableBuilder(
      column: $state.table.url,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get fileExtension => $state.composableBuilder(
      column: $state.table.fileExtension,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get localFilePath => $state.composableBuilder(
      column: $state.table.localFilePath,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get feedUuid => $state.composableBuilder(
      column: $state.table.feedUuid,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));
}

class $$MediasTableOrderingComposer
    extends OrderingComposer<_$AppDatabase, $MediasTable> {
  $$MediasTableOrderingComposer(super.$state);
  ColumnOrderings<int> get mediaID => $state.composableBuilder(
      column: $state.table.mediaID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get projectID => $state.composableBuilder(
      column: $state.table.projectID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get feedID => $state.composableBuilder(
      column: $state.table.feedID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get url => $state.composableBuilder(
      column: $state.table.url,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get fileExtension => $state.composableBuilder(
      column: $state.table.fileExtension,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get localFilePath => $state.composableBuilder(
      column: $state.table.localFilePath,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get feedUuid => $state.composableBuilder(
      column: $state.table.feedUuid,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

typedef $$AlertsTableCreateCompanionBuilder = AlertsCompanion Function({
  Value<int> notificationID,
  required int projectID,
  Value<String?> affectedModuleID,
  Value<String?> moduleName,
  Value<String?> isRead,
  Value<String?> description,
  Value<String?> createdDateTime,
  Value<String?> updatedDateTime,
  required int assignedByTeamMemberID,
  Value<String?> assignedByTeamMemberName,
  Value<String?> teamMemberAvatar,
  Value<int?> replyToID,
  Value<String?> delayTaskIDs,
  Value<int?> delayDuration,
  Value<String?> allNotificationTxt,
});
typedef $$AlertsTableUpdateCompanionBuilder = AlertsCompanion Function({
  Value<int> notificationID,
  Value<int> projectID,
  Value<String?> affectedModuleID,
  Value<String?> moduleName,
  Value<String?> isRead,
  Value<String?> description,
  Value<String?> createdDateTime,
  Value<String?> updatedDateTime,
  Value<int> assignedByTeamMemberID,
  Value<String?> assignedByTeamMemberName,
  Value<String?> teamMemberAvatar,
  Value<int?> replyToID,
  Value<String?> delayTaskIDs,
  Value<int?> delayDuration,
  Value<String?> allNotificationTxt,
});

class $$AlertsTableTableManager extends RootTableManager<
    _$AppDatabase,
    $AlertsTable,
    Alert,
    $$AlertsTableFilterComposer,
    $$AlertsTableOrderingComposer,
    $$AlertsTableCreateCompanionBuilder,
    $$AlertsTableUpdateCompanionBuilder> {
  $$AlertsTableTableManager(_$AppDatabase db, $AlertsTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$AlertsTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$AlertsTableOrderingComposer(ComposerState(db, table)),
          updateCompanionCallback: ({
            Value<int> notificationID = const Value.absent(),
            Value<int> projectID = const Value.absent(),
            Value<String?> affectedModuleID = const Value.absent(),
            Value<String?> moduleName = const Value.absent(),
            Value<String?> isRead = const Value.absent(),
            Value<String?> description = const Value.absent(),
            Value<String?> createdDateTime = const Value.absent(),
            Value<String?> updatedDateTime = const Value.absent(),
            Value<int> assignedByTeamMemberID = const Value.absent(),
            Value<String?> assignedByTeamMemberName = const Value.absent(),
            Value<String?> teamMemberAvatar = const Value.absent(),
            Value<int?> replyToID = const Value.absent(),
            Value<String?> delayTaskIDs = const Value.absent(),
            Value<int?> delayDuration = const Value.absent(),
            Value<String?> allNotificationTxt = const Value.absent(),
          }) =>
              AlertsCompanion(
            notificationID: notificationID,
            projectID: projectID,
            affectedModuleID: affectedModuleID,
            moduleName: moduleName,
            isRead: isRead,
            description: description,
            createdDateTime: createdDateTime,
            updatedDateTime: updatedDateTime,
            assignedByTeamMemberID: assignedByTeamMemberID,
            assignedByTeamMemberName: assignedByTeamMemberName,
            teamMemberAvatar: teamMemberAvatar,
            replyToID: replyToID,
            delayTaskIDs: delayTaskIDs,
            delayDuration: delayDuration,
            allNotificationTxt: allNotificationTxt,
          ),
          createCompanionCallback: ({
            Value<int> notificationID = const Value.absent(),
            required int projectID,
            Value<String?> affectedModuleID = const Value.absent(),
            Value<String?> moduleName = const Value.absent(),
            Value<String?> isRead = const Value.absent(),
            Value<String?> description = const Value.absent(),
            Value<String?> createdDateTime = const Value.absent(),
            Value<String?> updatedDateTime = const Value.absent(),
            required int assignedByTeamMemberID,
            Value<String?> assignedByTeamMemberName = const Value.absent(),
            Value<String?> teamMemberAvatar = const Value.absent(),
            Value<int?> replyToID = const Value.absent(),
            Value<String?> delayTaskIDs = const Value.absent(),
            Value<int?> delayDuration = const Value.absent(),
            Value<String?> allNotificationTxt = const Value.absent(),
          }) =>
              AlertsCompanion.insert(
            notificationID: notificationID,
            projectID: projectID,
            affectedModuleID: affectedModuleID,
            moduleName: moduleName,
            isRead: isRead,
            description: description,
            createdDateTime: createdDateTime,
            updatedDateTime: updatedDateTime,
            assignedByTeamMemberID: assignedByTeamMemberID,
            assignedByTeamMemberName: assignedByTeamMemberName,
            teamMemberAvatar: teamMemberAvatar,
            replyToID: replyToID,
            delayTaskIDs: delayTaskIDs,
            delayDuration: delayDuration,
            allNotificationTxt: allNotificationTxt,
          ),
        ));
}

class $$AlertsTableFilterComposer
    extends FilterComposer<_$AppDatabase, $AlertsTable> {
  $$AlertsTableFilterComposer(super.$state);
  ColumnFilters<int> get notificationID => $state.composableBuilder(
      column: $state.table.notificationID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get projectID => $state.composableBuilder(
      column: $state.table.projectID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get affectedModuleID => $state.composableBuilder(
      column: $state.table.affectedModuleID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get moduleName => $state.composableBuilder(
      column: $state.table.moduleName,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get isRead => $state.composableBuilder(
      column: $state.table.isRead,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get description => $state.composableBuilder(
      column: $state.table.description,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get createdDateTime => $state.composableBuilder(
      column: $state.table.createdDateTime,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get updatedDateTime => $state.composableBuilder(
      column: $state.table.updatedDateTime,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get assignedByTeamMemberID => $state.composableBuilder(
      column: $state.table.assignedByTeamMemberID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get assignedByTeamMemberName =>
      $state.composableBuilder(
          column: $state.table.assignedByTeamMemberName,
          builder: (column, joinBuilders) =>
              ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get teamMemberAvatar => $state.composableBuilder(
      column: $state.table.teamMemberAvatar,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get replyToID => $state.composableBuilder(
      column: $state.table.replyToID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get delayTaskIDs => $state.composableBuilder(
      column: $state.table.delayTaskIDs,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get delayDuration => $state.composableBuilder(
      column: $state.table.delayDuration,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get allNotificationTxt => $state.composableBuilder(
      column: $state.table.allNotificationTxt,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));
}

class $$AlertsTableOrderingComposer
    extends OrderingComposer<_$AppDatabase, $AlertsTable> {
  $$AlertsTableOrderingComposer(super.$state);
  ColumnOrderings<int> get notificationID => $state.composableBuilder(
      column: $state.table.notificationID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get projectID => $state.composableBuilder(
      column: $state.table.projectID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get affectedModuleID => $state.composableBuilder(
      column: $state.table.affectedModuleID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get moduleName => $state.composableBuilder(
      column: $state.table.moduleName,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get isRead => $state.composableBuilder(
      column: $state.table.isRead,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get description => $state.composableBuilder(
      column: $state.table.description,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get createdDateTime => $state.composableBuilder(
      column: $state.table.createdDateTime,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get updatedDateTime => $state.composableBuilder(
      column: $state.table.updatedDateTime,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get assignedByTeamMemberID => $state.composableBuilder(
      column: $state.table.assignedByTeamMemberID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get assignedByTeamMemberName =>
      $state.composableBuilder(
          column: $state.table.assignedByTeamMemberName,
          builder: (column, joinBuilders) =>
              ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get teamMemberAvatar => $state.composableBuilder(
      column: $state.table.teamMemberAvatar,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get replyToID => $state.composableBuilder(
      column: $state.table.replyToID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get delayTaskIDs => $state.composableBuilder(
      column: $state.table.delayTaskIDs,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get delayDuration => $state.composableBuilder(
      column: $state.table.delayDuration,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get allNotificationTxt => $state.composableBuilder(
      column: $state.table.allNotificationTxt,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

typedef $$ProfilesTableCreateCompanionBuilder = ProfilesCompanion Function({
  Value<int> id,
  Value<String?> username,
  Value<String?> name,
  Value<String?> address,
  Value<String?> email,
  Value<String?> gmail,
  Value<String?> branch,
  Value<String?> designationName,
  Value<String?> designationInitial,
  Value<String?> avatar,
  Value<String?> contactNo,
  Value<String?> access,
  Value<int?> teamMemberID,
});
typedef $$ProfilesTableUpdateCompanionBuilder = ProfilesCompanion Function({
  Value<int> id,
  Value<String?> username,
  Value<String?> name,
  Value<String?> address,
  Value<String?> email,
  Value<String?> gmail,
  Value<String?> branch,
  Value<String?> designationName,
  Value<String?> designationInitial,
  Value<String?> avatar,
  Value<String?> contactNo,
  Value<String?> access,
  Value<int?> teamMemberID,
});

class $$ProfilesTableTableManager extends RootTableManager<
    _$AppDatabase,
    $ProfilesTable,
    Profile,
    $$ProfilesTableFilterComposer,
    $$ProfilesTableOrderingComposer,
    $$ProfilesTableCreateCompanionBuilder,
    $$ProfilesTableUpdateCompanionBuilder> {
  $$ProfilesTableTableManager(_$AppDatabase db, $ProfilesTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$ProfilesTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$ProfilesTableOrderingComposer(ComposerState(db, table)),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<String?> username = const Value.absent(),
            Value<String?> name = const Value.absent(),
            Value<String?> address = const Value.absent(),
            Value<String?> email = const Value.absent(),
            Value<String?> gmail = const Value.absent(),
            Value<String?> branch = const Value.absent(),
            Value<String?> designationName = const Value.absent(),
            Value<String?> designationInitial = const Value.absent(),
            Value<String?> avatar = const Value.absent(),
            Value<String?> contactNo = const Value.absent(),
            Value<String?> access = const Value.absent(),
            Value<int?> teamMemberID = const Value.absent(),
          }) =>
              ProfilesCompanion(
            id: id,
            username: username,
            name: name,
            address: address,
            email: email,
            gmail: gmail,
            branch: branch,
            designationName: designationName,
            designationInitial: designationInitial,
            avatar: avatar,
            contactNo: contactNo,
            access: access,
            teamMemberID: teamMemberID,
          ),
          createCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<String?> username = const Value.absent(),
            Value<String?> name = const Value.absent(),
            Value<String?> address = const Value.absent(),
            Value<String?> email = const Value.absent(),
            Value<String?> gmail = const Value.absent(),
            Value<String?> branch = const Value.absent(),
            Value<String?> designationName = const Value.absent(),
            Value<String?> designationInitial = const Value.absent(),
            Value<String?> avatar = const Value.absent(),
            Value<String?> contactNo = const Value.absent(),
            Value<String?> access = const Value.absent(),
            Value<int?> teamMemberID = const Value.absent(),
          }) =>
              ProfilesCompanion.insert(
            id: id,
            username: username,
            name: name,
            address: address,
            email: email,
            gmail: gmail,
            branch: branch,
            designationName: designationName,
            designationInitial: designationInitial,
            avatar: avatar,
            contactNo: contactNo,
            access: access,
            teamMemberID: teamMemberID,
          ),
        ));
}

class $$ProfilesTableFilterComposer
    extends FilterComposer<_$AppDatabase, $ProfilesTable> {
  $$ProfilesTableFilterComposer(super.$state);
  ColumnFilters<int> get id => $state.composableBuilder(
      column: $state.table.id,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get username => $state.composableBuilder(
      column: $state.table.username,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get name => $state.composableBuilder(
      column: $state.table.name,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get address => $state.composableBuilder(
      column: $state.table.address,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get email => $state.composableBuilder(
      column: $state.table.email,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get gmail => $state.composableBuilder(
      column: $state.table.gmail,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get branch => $state.composableBuilder(
      column: $state.table.branch,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get designationName => $state.composableBuilder(
      column: $state.table.designationName,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get designationInitial => $state.composableBuilder(
      column: $state.table.designationInitial,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get avatar => $state.composableBuilder(
      column: $state.table.avatar,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get contactNo => $state.composableBuilder(
      column: $state.table.contactNo,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get access => $state.composableBuilder(
      column: $state.table.access,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get teamMemberID => $state.composableBuilder(
      column: $state.table.teamMemberID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));
}

class $$ProfilesTableOrderingComposer
    extends OrderingComposer<_$AppDatabase, $ProfilesTable> {
  $$ProfilesTableOrderingComposer(super.$state);
  ColumnOrderings<int> get id => $state.composableBuilder(
      column: $state.table.id,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get username => $state.composableBuilder(
      column: $state.table.username,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get name => $state.composableBuilder(
      column: $state.table.name,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get address => $state.composableBuilder(
      column: $state.table.address,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get email => $state.composableBuilder(
      column: $state.table.email,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get gmail => $state.composableBuilder(
      column: $state.table.gmail,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get branch => $state.composableBuilder(
      column: $state.table.branch,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get designationName => $state.composableBuilder(
      column: $state.table.designationName,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get designationInitial => $state.composableBuilder(
      column: $state.table.designationInitial,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get avatar => $state.composableBuilder(
      column: $state.table.avatar,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get contactNo => $state.composableBuilder(
      column: $state.table.contactNo,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get access => $state.composableBuilder(
      column: $state.table.access,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get teamMemberID => $state.composableBuilder(
      column: $state.table.teamMemberID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

typedef $$CostEstimationsTableCreateCompanionBuilder = CostEstimationsCompanion
    Function({
  required int costEstimationID,
  Value<String?> costEstimationName,
  Value<String?> costEstimationType,
  Value<String?> costEstimationTotal,
  Value<String?> localFilePath,
  Value<String?> status,
  required int totalCEQty,
  required int projectID,
  Value<int> rowid,
});
typedef $$CostEstimationsTableUpdateCompanionBuilder = CostEstimationsCompanion
    Function({
  Value<int> costEstimationID,
  Value<String?> costEstimationName,
  Value<String?> costEstimationType,
  Value<String?> costEstimationTotal,
  Value<String?> localFilePath,
  Value<String?> status,
  Value<int> totalCEQty,
  Value<int> projectID,
  Value<int> rowid,
});

class $$CostEstimationsTableTableManager extends RootTableManager<
    _$AppDatabase,
    $CostEstimationsTable,
    CostEstimation,
    $$CostEstimationsTableFilterComposer,
    $$CostEstimationsTableOrderingComposer,
    $$CostEstimationsTableCreateCompanionBuilder,
    $$CostEstimationsTableUpdateCompanionBuilder> {
  $$CostEstimationsTableTableManager(
      _$AppDatabase db, $CostEstimationsTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$CostEstimationsTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$CostEstimationsTableOrderingComposer(ComposerState(db, table)),
          updateCompanionCallback: ({
            Value<int> costEstimationID = const Value.absent(),
            Value<String?> costEstimationName = const Value.absent(),
            Value<String?> costEstimationType = const Value.absent(),
            Value<String?> costEstimationTotal = const Value.absent(),
            Value<String?> localFilePath = const Value.absent(),
            Value<String?> status = const Value.absent(),
            Value<int> totalCEQty = const Value.absent(),
            Value<int> projectID = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              CostEstimationsCompanion(
            costEstimationID: costEstimationID,
            costEstimationName: costEstimationName,
            costEstimationType: costEstimationType,
            costEstimationTotal: costEstimationTotal,
            localFilePath: localFilePath,
            status: status,
            totalCEQty: totalCEQty,
            projectID: projectID,
            rowid: rowid,
          ),
          createCompanionCallback: ({
            required int costEstimationID,
            Value<String?> costEstimationName = const Value.absent(),
            Value<String?> costEstimationType = const Value.absent(),
            Value<String?> costEstimationTotal = const Value.absent(),
            Value<String?> localFilePath = const Value.absent(),
            Value<String?> status = const Value.absent(),
            required int totalCEQty,
            required int projectID,
            Value<int> rowid = const Value.absent(),
          }) =>
              CostEstimationsCompanion.insert(
            costEstimationID: costEstimationID,
            costEstimationName: costEstimationName,
            costEstimationType: costEstimationType,
            costEstimationTotal: costEstimationTotal,
            localFilePath: localFilePath,
            status: status,
            totalCEQty: totalCEQty,
            projectID: projectID,
            rowid: rowid,
          ),
        ));
}

class $$CostEstimationsTableFilterComposer
    extends FilterComposer<_$AppDatabase, $CostEstimationsTable> {
  $$CostEstimationsTableFilterComposer(super.$state);
  ColumnFilters<int> get costEstimationID => $state.composableBuilder(
      column: $state.table.costEstimationID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get costEstimationName => $state.composableBuilder(
      column: $state.table.costEstimationName,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get costEstimationType => $state.composableBuilder(
      column: $state.table.costEstimationType,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get costEstimationTotal => $state.composableBuilder(
      column: $state.table.costEstimationTotal,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get localFilePath => $state.composableBuilder(
      column: $state.table.localFilePath,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get status => $state.composableBuilder(
      column: $state.table.status,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get totalCEQty => $state.composableBuilder(
      column: $state.table.totalCEQty,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  $$WorkplacesTableFilterComposer get projectID {
    final $$WorkplacesTableFilterComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.projectID,
        referencedTable: $state.db.workplaces,
        getReferencedColumn: (t) => t.projectID,
        builder: (joinBuilder, parentComposers) =>
            $$WorkplacesTableFilterComposer(ComposerState($state.db,
                $state.db.workplaces, joinBuilder, parentComposers)));
    return composer;
  }
}

class $$CostEstimationsTableOrderingComposer
    extends OrderingComposer<_$AppDatabase, $CostEstimationsTable> {
  $$CostEstimationsTableOrderingComposer(super.$state);
  ColumnOrderings<int> get costEstimationID => $state.composableBuilder(
      column: $state.table.costEstimationID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get costEstimationName => $state.composableBuilder(
      column: $state.table.costEstimationName,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get costEstimationType => $state.composableBuilder(
      column: $state.table.costEstimationType,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get costEstimationTotal => $state.composableBuilder(
      column: $state.table.costEstimationTotal,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get localFilePath => $state.composableBuilder(
      column: $state.table.localFilePath,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get status => $state.composableBuilder(
      column: $state.table.status,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get totalCEQty => $state.composableBuilder(
      column: $state.table.totalCEQty,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  $$WorkplacesTableOrderingComposer get projectID {
    final $$WorkplacesTableOrderingComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.projectID,
        referencedTable: $state.db.workplaces,
        getReferencedColumn: (t) => t.projectID,
        builder: (joinBuilder, parentComposers) =>
            $$WorkplacesTableOrderingComposer(ComposerState($state.db,
                $state.db.workplaces, joinBuilder, parentComposers)));
    return composer;
  }
}

typedef $$TaskMediasTableCreateCompanionBuilder = TaskMediasCompanion Function({
  Value<int> taskMediaID,
  Value<String?> mediaName,
  Value<String?> mediaType,
  required int taskID,
  required int feedID,
  required int projectID,
  required int teamMemberID,
  Value<String?> mediaPath,
  Value<String?> localFilePath,
});
typedef $$TaskMediasTableUpdateCompanionBuilder = TaskMediasCompanion Function({
  Value<int> taskMediaID,
  Value<String?> mediaName,
  Value<String?> mediaType,
  Value<int> taskID,
  Value<int> feedID,
  Value<int> projectID,
  Value<int> teamMemberID,
  Value<String?> mediaPath,
  Value<String?> localFilePath,
});

class $$TaskMediasTableTableManager extends RootTableManager<
    _$AppDatabase,
    $TaskMediasTable,
    TaskMedia,
    $$TaskMediasTableFilterComposer,
    $$TaskMediasTableOrderingComposer,
    $$TaskMediasTableCreateCompanionBuilder,
    $$TaskMediasTableUpdateCompanionBuilder> {
  $$TaskMediasTableTableManager(_$AppDatabase db, $TaskMediasTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$TaskMediasTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$TaskMediasTableOrderingComposer(ComposerState(db, table)),
          updateCompanionCallback: ({
            Value<int> taskMediaID = const Value.absent(),
            Value<String?> mediaName = const Value.absent(),
            Value<String?> mediaType = const Value.absent(),
            Value<int> taskID = const Value.absent(),
            Value<int> feedID = const Value.absent(),
            Value<int> projectID = const Value.absent(),
            Value<int> teamMemberID = const Value.absent(),
            Value<String?> mediaPath = const Value.absent(),
            Value<String?> localFilePath = const Value.absent(),
          }) =>
              TaskMediasCompanion(
            taskMediaID: taskMediaID,
            mediaName: mediaName,
            mediaType: mediaType,
            taskID: taskID,
            feedID: feedID,
            projectID: projectID,
            teamMemberID: teamMemberID,
            mediaPath: mediaPath,
            localFilePath: localFilePath,
          ),
          createCompanionCallback: ({
            Value<int> taskMediaID = const Value.absent(),
            Value<String?> mediaName = const Value.absent(),
            Value<String?> mediaType = const Value.absent(),
            required int taskID,
            required int feedID,
            required int projectID,
            required int teamMemberID,
            Value<String?> mediaPath = const Value.absent(),
            Value<String?> localFilePath = const Value.absent(),
          }) =>
              TaskMediasCompanion.insert(
            taskMediaID: taskMediaID,
            mediaName: mediaName,
            mediaType: mediaType,
            taskID: taskID,
            feedID: feedID,
            projectID: projectID,
            teamMemberID: teamMemberID,
            mediaPath: mediaPath,
            localFilePath: localFilePath,
          ),
        ));
}

class $$TaskMediasTableFilterComposer
    extends FilterComposer<_$AppDatabase, $TaskMediasTable> {
  $$TaskMediasTableFilterComposer(super.$state);
  ColumnFilters<int> get taskMediaID => $state.composableBuilder(
      column: $state.table.taskMediaID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get mediaName => $state.composableBuilder(
      column: $state.table.mediaName,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get mediaType => $state.composableBuilder(
      column: $state.table.mediaType,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get taskID => $state.composableBuilder(
      column: $state.table.taskID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get feedID => $state.composableBuilder(
      column: $state.table.feedID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get projectID => $state.composableBuilder(
      column: $state.table.projectID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get teamMemberID => $state.composableBuilder(
      column: $state.table.teamMemberID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get mediaPath => $state.composableBuilder(
      column: $state.table.mediaPath,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get localFilePath => $state.composableBuilder(
      column: $state.table.localFilePath,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));
}

class $$TaskMediasTableOrderingComposer
    extends OrderingComposer<_$AppDatabase, $TaskMediasTable> {
  $$TaskMediasTableOrderingComposer(super.$state);
  ColumnOrderings<int> get taskMediaID => $state.composableBuilder(
      column: $state.table.taskMediaID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get mediaName => $state.composableBuilder(
      column: $state.table.mediaName,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get mediaType => $state.composableBuilder(
      column: $state.table.mediaType,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get taskID => $state.composableBuilder(
      column: $state.table.taskID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get feedID => $state.composableBuilder(
      column: $state.table.feedID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get projectID => $state.composableBuilder(
      column: $state.table.projectID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get teamMemberID => $state.composableBuilder(
      column: $state.table.teamMemberID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get mediaPath => $state.composableBuilder(
      column: $state.table.mediaPath,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get localFilePath => $state.composableBuilder(
      column: $state.table.localFilePath,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

typedef $$WorkplaceInfosTableCreateCompanionBuilder = WorkplaceInfosCompanion
    Function({
  required int projectID,
  Value<String?> infoLabel,
  Value<String?> infoDisplayValue,
  Value<int> sequance,
  Value<int> rowid,
});
typedef $$WorkplaceInfosTableUpdateCompanionBuilder = WorkplaceInfosCompanion
    Function({
  Value<int> projectID,
  Value<String?> infoLabel,
  Value<String?> infoDisplayValue,
  Value<int> sequance,
  Value<int> rowid,
});

class $$WorkplaceInfosTableTableManager extends RootTableManager<
    _$AppDatabase,
    $WorkplaceInfosTable,
    WorkplaceInfo,
    $$WorkplaceInfosTableFilterComposer,
    $$WorkplaceInfosTableOrderingComposer,
    $$WorkplaceInfosTableCreateCompanionBuilder,
    $$WorkplaceInfosTableUpdateCompanionBuilder> {
  $$WorkplaceInfosTableTableManager(
      _$AppDatabase db, $WorkplaceInfosTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$WorkplaceInfosTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$WorkplaceInfosTableOrderingComposer(ComposerState(db, table)),
          updateCompanionCallback: ({
            Value<int> projectID = const Value.absent(),
            Value<String?> infoLabel = const Value.absent(),
            Value<String?> infoDisplayValue = const Value.absent(),
            Value<int> sequance = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              WorkplaceInfosCompanion(
            projectID: projectID,
            infoLabel: infoLabel,
            infoDisplayValue: infoDisplayValue,
            sequance: sequance,
            rowid: rowid,
          ),
          createCompanionCallback: ({
            required int projectID,
            Value<String?> infoLabel = const Value.absent(),
            Value<String?> infoDisplayValue = const Value.absent(),
            Value<int> sequance = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              WorkplaceInfosCompanion.insert(
            projectID: projectID,
            infoLabel: infoLabel,
            infoDisplayValue: infoDisplayValue,
            sequance: sequance,
            rowid: rowid,
          ),
        ));
}

class $$WorkplaceInfosTableFilterComposer
    extends FilterComposer<_$AppDatabase, $WorkplaceInfosTable> {
  $$WorkplaceInfosTableFilterComposer(super.$state);
  ColumnFilters<String> get infoLabel => $state.composableBuilder(
      column: $state.table.infoLabel,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get infoDisplayValue => $state.composableBuilder(
      column: $state.table.infoDisplayValue,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get sequance => $state.composableBuilder(
      column: $state.table.sequance,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  $$WorkplacesTableFilterComposer get projectID {
    final $$WorkplacesTableFilterComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.projectID,
        referencedTable: $state.db.workplaces,
        getReferencedColumn: (t) => t.projectID,
        builder: (joinBuilder, parentComposers) =>
            $$WorkplacesTableFilterComposer(ComposerState($state.db,
                $state.db.workplaces, joinBuilder, parentComposers)));
    return composer;
  }
}

class $$WorkplaceInfosTableOrderingComposer
    extends OrderingComposer<_$AppDatabase, $WorkplaceInfosTable> {
  $$WorkplaceInfosTableOrderingComposer(super.$state);
  ColumnOrderings<String> get infoLabel => $state.composableBuilder(
      column: $state.table.infoLabel,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get infoDisplayValue => $state.composableBuilder(
      column: $state.table.infoDisplayValue,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get sequance => $state.composableBuilder(
      column: $state.table.sequance,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  $$WorkplacesTableOrderingComposer get projectID {
    final $$WorkplacesTableOrderingComposer composer = $state.composerBuilder(
        composer: this,
        getCurrentColumn: (t) => t.projectID,
        referencedTable: $state.db.workplaces,
        getReferencedColumn: (t) => t.projectID,
        builder: (joinBuilder, parentComposers) =>
            $$WorkplacesTableOrderingComposer(ComposerState($state.db,
                $state.db.workplaces, joinBuilder, parentComposers)));
    return composer;
  }
}

typedef $$AssociateMediasTableCreateCompanionBuilder = AssociateMediasCompanion
    Function({
  required int mediaID,
  required int projectID,
  Value<String?> projectFeedsMedia,
  Value<String?> projectFeedsMediaType,
  required String createdDateTime,
  required String caption,
  required String module,
  Value<String?> localFilePath,
  Value<int> rowid,
});
typedef $$AssociateMediasTableUpdateCompanionBuilder = AssociateMediasCompanion
    Function({
  Value<int> mediaID,
  Value<int> projectID,
  Value<String?> projectFeedsMedia,
  Value<String?> projectFeedsMediaType,
  Value<String> createdDateTime,
  Value<String> caption,
  Value<String> module,
  Value<String?> localFilePath,
  Value<int> rowid,
});

class $$AssociateMediasTableTableManager extends RootTableManager<
    _$AppDatabase,
    $AssociateMediasTable,
    AssociateMedia,
    $$AssociateMediasTableFilterComposer,
    $$AssociateMediasTableOrderingComposer,
    $$AssociateMediasTableCreateCompanionBuilder,
    $$AssociateMediasTableUpdateCompanionBuilder> {
  $$AssociateMediasTableTableManager(
      _$AppDatabase db, $AssociateMediasTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$AssociateMediasTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$AssociateMediasTableOrderingComposer(ComposerState(db, table)),
          updateCompanionCallback: ({
            Value<int> mediaID = const Value.absent(),
            Value<int> projectID = const Value.absent(),
            Value<String?> projectFeedsMedia = const Value.absent(),
            Value<String?> projectFeedsMediaType = const Value.absent(),
            Value<String> createdDateTime = const Value.absent(),
            Value<String> caption = const Value.absent(),
            Value<String> module = const Value.absent(),
            Value<String?> localFilePath = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              AssociateMediasCompanion(
            mediaID: mediaID,
            projectID: projectID,
            projectFeedsMedia: projectFeedsMedia,
            projectFeedsMediaType: projectFeedsMediaType,
            createdDateTime: createdDateTime,
            caption: caption,
            module: module,
            localFilePath: localFilePath,
            rowid: rowid,
          ),
          createCompanionCallback: ({
            required int mediaID,
            required int projectID,
            Value<String?> projectFeedsMedia = const Value.absent(),
            Value<String?> projectFeedsMediaType = const Value.absent(),
            required String createdDateTime,
            required String caption,
            required String module,
            Value<String?> localFilePath = const Value.absent(),
            Value<int> rowid = const Value.absent(),
          }) =>
              AssociateMediasCompanion.insert(
            mediaID: mediaID,
            projectID: projectID,
            projectFeedsMedia: projectFeedsMedia,
            projectFeedsMediaType: projectFeedsMediaType,
            createdDateTime: createdDateTime,
            caption: caption,
            module: module,
            localFilePath: localFilePath,
            rowid: rowid,
          ),
        ));
}

class $$AssociateMediasTableFilterComposer
    extends FilterComposer<_$AppDatabase, $AssociateMediasTable> {
  $$AssociateMediasTableFilterComposer(super.$state);
  ColumnFilters<int> get mediaID => $state.composableBuilder(
      column: $state.table.mediaID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get projectID => $state.composableBuilder(
      column: $state.table.projectID,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get projectFeedsMedia => $state.composableBuilder(
      column: $state.table.projectFeedsMedia,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get projectFeedsMediaType => $state.composableBuilder(
      column: $state.table.projectFeedsMediaType,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get createdDateTime => $state.composableBuilder(
      column: $state.table.createdDateTime,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get caption => $state.composableBuilder(
      column: $state.table.caption,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get module => $state.composableBuilder(
      column: $state.table.module,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get localFilePath => $state.composableBuilder(
      column: $state.table.localFilePath,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));
}

class $$AssociateMediasTableOrderingComposer
    extends OrderingComposer<_$AppDatabase, $AssociateMediasTable> {
  $$AssociateMediasTableOrderingComposer(super.$state);
  ColumnOrderings<int> get mediaID => $state.composableBuilder(
      column: $state.table.mediaID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get projectID => $state.composableBuilder(
      column: $state.table.projectID,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get projectFeedsMedia => $state.composableBuilder(
      column: $state.table.projectFeedsMedia,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get projectFeedsMediaType => $state.composableBuilder(
      column: $state.table.projectFeedsMediaType,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get createdDateTime => $state.composableBuilder(
      column: $state.table.createdDateTime,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get caption => $state.composableBuilder(
      column: $state.table.caption,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get module => $state.composableBuilder(
      column: $state.table.module,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get localFilePath => $state.composableBuilder(
      column: $state.table.localFilePath,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

class $AppDatabaseManager {
  final _$AppDatabase _db;
  $AppDatabaseManager(this._db);
  $$WorkplacesTableTableManager get workplaces =>
      $$WorkplacesTableTableManager(_db, _db.workplaces);
  $$TeamMembersTableTableManager get teamMembers =>
      $$TeamMembersTableTableManager(_db, _db.teamMembers);
  $$TopicsTableTableManager get topics =>
      $$TopicsTableTableManager(_db, _db.topics);
  $$FeedsTableTableManager get feeds =>
      $$FeedsTableTableManager(_db, _db.feeds);
  $$MediasTableTableManager get medias =>
      $$MediasTableTableManager(_db, _db.medias);
  $$AlertsTableTableManager get alerts =>
      $$AlertsTableTableManager(_db, _db.alerts);
  $$ProfilesTableTableManager get profiles =>
      $$ProfilesTableTableManager(_db, _db.profiles);
  $$CostEstimationsTableTableManager get costEstimations =>
      $$CostEstimationsTableTableManager(_db, _db.costEstimations);
  $$TaskMediasTableTableManager get taskMedias =>
      $$TaskMediasTableTableManager(_db, _db.taskMedias);
  $$WorkplaceInfosTableTableManager get workplaceInfos =>
      $$WorkplaceInfosTableTableManager(_db, _db.workplaceInfos);
  $$AssociateMediasTableTableManager get associateMedias =>
      $$AssociateMediasTableTableManager(_db, _db.associateMedias);
}
