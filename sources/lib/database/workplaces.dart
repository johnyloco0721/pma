import 'package:drift/drift.dart';
import 'package:sources/database/app_database.dart';

part 'workplaces.g.dart';

// Workplace
class Workplaces extends Table {
  IntColumn get projectID => integer().named('projectID')();
  TextColumn get projectArea => text().nullable().named('projectArea')();
  TextColumn get projectUnit => text().nullable().named('projectUnit')();
  IntColumn get projectSize => integer().nullable().named('projectSize')();
  IntColumn get projectBedroom => integer().nullable().named('projectBedroom')();
  IntColumn get projectBathroom => integer().nullable().named('projectBathroom')();
  TextColumn get marketSegment => text().nullable().named('marketSegment')();
  TextColumn get themeName => text().nullable().named('themeName')();
  TextColumn get startDateTime => text().nullable().named('startDateTime')();
  TextColumn get targetDateTime => text().nullable().named('targetDateTime')();
  TextColumn get dueDateTime => text().nullable().named('dueDateTime')();
  TextColumn get projectStatus => text().nullable().named('projectStatus')();
  TextColumn get projectCategory => text().nullable().named('projectCategory')();
  TextColumn get ownerName => text().nullable().named('ownerName')();
  TextColumn get propertyName => text().nullable().named('propertyName')();
  TextColumn get propertyState => text().nullable().named('propertyState')();
  TextColumn get propertyType => text().nullable().named('propertyType')();
  TextColumn get isRead => text().nullable().named('isRead')();
  TextColumn get lastViewedDateTime => text().nullable().named('lastViewedDateTime')();
  IntColumn get lastReadFeedID => integer().nullable().named('lastReadFeedID')();
  TextColumn get updatedDateTime => text().nullable().named('updatedDateTime')(); // new column for DB version 2
  TextColumn get displayDateTime => text().nullable().named('displayDateTime')(); // new column for DB version 9
  IntColumn get completedTasks => integer().nullable().named('completedTasks')(); // new column for DB version 8
  IntColumn get totalTasks => integer().nullable().named('totalTasks')(); // new column for DB version 8

  @override
  Set<Column> get primaryKey => {projectID};
}

@DriftAccessor(tables: [Workplaces])
class WorkplaceDao extends DatabaseAccessor<AppDatabase> with _$WorkplaceDaoMixin {
  final AppDatabase db;

  WorkplaceDao(this.db) : super(db);
  
  Future<List<Workplace>> getAllWorkplaces({String keyword = ''}) {
    return (select(workplaces)
              ..where((w) => (w.propertyName+w.projectUnit).contains(keyword))
              ..orderBy([
                (t) => OrderingTerm(expression: t.lastViewedDateTime, mode: OrderingMode.desc),
              ])
            ).get();
  }
  Stream<List<Workplace>> watchAllWorkplaces({List<String> filterBy = const []}) {
    if (filterBy.length == 0) {
      return (select(workplaces)
                ..orderBy([
                  (t) => OrderingTerm(expression: t.displayDateTime, mode: OrderingMode.desc),
                ])
              ).watch();
    }
    else {
      return (select(workplaces)
                ..where((w) => w.isRead.isIn(filterBy))
                ..orderBy([
                  (t) => OrderingTerm(expression: t.displayDateTime, mode: OrderingMode.desc),
                ])
              ).watch();
    }
  }
  Future<Workplace?> getSingleWorkplace(int projectID) {
    return (select(workplaces)
              ..where((tbl) => tbl.projectID.equals(projectID))
            ).getSingleOrNull();
  }
  Stream<Workplace?> watchSingleWorkplace(int projectID) {
    return (select(workplaces)
              ..where((tbl) => tbl.projectID.equals(projectID))
            ).watchSingleOrNull();
  }
  Future<Workplace?> getLatestUpdateWorkplace() {
    return (select(workplaces)
              ..orderBy([
                (t) => OrderingTerm(expression: t.updatedDateTime, mode: OrderingMode.desc),
              ])
              ..limit(1)
            ).getSingleOrNull();
  }
  Future insertWorkplace(Insertable<Workplace> workplace) => into(workplaces).insert(workplace);
  Future updateWorkplace(Insertable<Workplace> workplace) => update(workplaces).replace(workplace);
  Future deleteWorkplace(Insertable<Workplace> workplace) => delete(workplaces).delete(workplace);
  Future insertOrUpdateWorkplace(Insertable<Workplace> workplace) => into(workplaces).insertOnConflictUpdate(workplace);
  Future<void> batchInsertOrUpdateWorkplaces(List<Workplace> workplaceItems) async {
    for (var workplace in workplaceItems) {
      await into(workplaces).insertOnConflictUpdate(workplace);
    }
  }

  Future deleteAllWorkplaces() => delete(workplaces).go();

  Future getWorkplacesByExcludedIds(List<int> ids) => (select(workplaces)..where((tbl) => tbl.projectID.isNotIn(ids))).get();
  Future deleteWorkplacesByExcludedIds(List<int> ids) => (delete(workplaces)..where((tbl) => tbl.projectID.isNotIn(ids))).go();

  Future<List<Workplace>> getPeriodAgoWorkplace(String compareDatetime) => (select(workplaces)
              ..where((w) => w.updatedDateTime.isBiggerOrEqualValue(compareDatetime))
              ..orderBy([
                (t) => OrderingTerm(expression: t.lastViewedDateTime, mode: OrderingMode.desc),
              ])
            ).get();
}

// Team Members
class TeamMembers extends Table {
  IntColumn get teamMemberID => integer().named('teamMemberID')();
  TextColumn get teamMemberName => text().nullable().named('teamMemberName')();
  TextColumn get teamMemberAvatar => text().nullable().named('teamMemberAvatar')();
  TextColumn get designationName => text().nullable().named('designationName')();
  IntColumn get projectID => integer().named('projectID').customConstraint('NOT NULL REFERENCES workplaces(projectID)')();
  BoolColumn get show => boolean().named('show').withDefault(const Constant(false))();

  @override
  Set<Column> get primaryKey => {projectID, teamMemberID};
}

@DriftAccessor(tables: [TeamMembers])
class TeamMemberDao extends DatabaseAccessor<AppDatabase> with _$TeamMemberDaoMixin {
  final AppDatabase db;

  TeamMemberDao(this.db) : super(db);

  Future<List<TeamMember>> getTeamMembersByProject(int workplaceID) {
    return (select(teamMembers)
              ..where((tbl) => tbl.projectID.equals(workplaceID))
              ..where((tbl) => tbl.show.equals(true))
            ).get();
  }
  Future<List<TeamMember>> searchTeamMembersByProject(int workplaceID, String name) {
    return (select(teamMembers)
              ..where((tbl) => tbl.projectID.equals(workplaceID))
              ..where((tbl) => tbl.show.equals(true))
              ..where((tbl) => tbl.teamMemberName.like('%'+name+'%'))
            ).get();
  }
  Stream<List<TeamMember>> watchTeamMembersByProject(int workplaceID) {
    return (select(teamMembers)
              ..where((tbl) => tbl.projectID.equals(workplaceID))
              ..where((tbl) => tbl.show.equals(true))
            ).watch();
  }
  Future<TeamMember?> getSingleTeamMember(int projectID, int memberID) 
    => (select(teamMembers)
        ..where((tbl) => tbl.teamMemberID.equals(memberID))
        ..where((tbl) => tbl.projectID.equals(projectID))
        ).getSingleOrNull();
  Future insertTeamMember(Insertable<TeamMember> teamMember) => into(teamMembers).insert(teamMember);
  Future updateTeamMember(Insertable<TeamMember> teamMember) => update(teamMembers).replace(teamMember);
  Future deleteTeamMember(Insertable<TeamMember> teamMember) => delete(teamMembers).delete(teamMember);
  Future insertOrUpdateTeamMember(Insertable<TeamMember> teamMember) => into(teamMembers).insertOnConflictUpdate(teamMember);

  Future deleteAllTeamMember() => delete(teamMembers).go();
  Future deleteTeamMemberByExcludedIds(int projectID, List<int> ids) 
    => (delete(teamMembers)
        ..where((tbl) => tbl.projectID.equals(projectID))
        ..where((tbl) => tbl.teamMemberID.isNotIn(ids))
        ..where((tbl) => tbl.show.equals(true))
        ).go();
}

// Team Estimations
class CostEstimations extends Table {
  IntColumn get costEstimationID => integer().named('costEstimationID')();
  TextColumn get costEstimationName => text().nullable().named('costEstimationName')();
  TextColumn get costEstimationType => text().nullable().named('costEstimationType')();
  TextColumn get costEstimationTotal => text().nullable().named('costEstimationTotal')();
  TextColumn get localFilePath => text().nullable().named('localFilePath')();
  TextColumn get status => text().nullable().named('status')();
  IntColumn get totalCEQty => integer().named('totalCEQty')();
  IntColumn get projectID => integer().named('projectID').customConstraint('NOT NULL REFERENCES workplaces(projectID)')();

  @override
  Set<Column> get primaryKey => {projectID, costEstimationID};
}

@DriftAccessor(tables: [CostEstimations])
class CostEstimationDao extends DatabaseAccessor<AppDatabase> with _$CostEstimationDaoMixin {
  final AppDatabase db;

  CostEstimationDao(this.db) : super(db);

  Future<List<CostEstimation>> getCostEstimationsByProject(int workplaceID) {
    return (select(costEstimations)
              ..where((tbl) => tbl.projectID.equals(workplaceID))
            ).get();
  }
  Stream<List<CostEstimation>> watchCostEstimationsByProject(int workplaceID) {
    return (select(costEstimations)
              ..where((tbl) => tbl.projectID.equals(workplaceID))
            ).watch();
  }
  Future insertCostEstimation(Insertable<CostEstimation> costEstimation) => into(costEstimations).insert(costEstimation);
  Future updateCostEstimation(Insertable<CostEstimation> costEstimation) => update(costEstimations).replace(costEstimation);
  Future deleteCostEstimation(Insertable<CostEstimation> costEstimation) => delete(costEstimations).delete(costEstimation);
  Future insertOrUpdateCostEstimation(Insertable<CostEstimation> costEstimation) => into(costEstimations).insertOnConflictUpdate(costEstimation);

  Future deleteAllCostEstimation() => delete(costEstimations).go();
}

// Workplace Info
class WorkplaceInfos extends Table {
  IntColumn get projectID => integer().named('projectID').customConstraint('NOT NULL REFERENCES workplaces(projectID)')();
  TextColumn get infoLabel => text().nullable().named('infoLabel')();
  TextColumn get infoDisplayValue => text().nullable().named('infoDisplayValue')();
  IntColumn get sequance => integer().named('sequance').withDefault(const Constant(0))();

  @override
  Set<Column> get primaryKey => {projectID, infoLabel};
}

@DriftAccessor(tables: [WorkplaceInfos])
class WorkplaceInfoDao extends DatabaseAccessor<AppDatabase> with _$WorkplaceInfoDaoMixin {
  final AppDatabase db;

  WorkplaceInfoDao(this.db) : super(db);

  Future insertOrUpdateWorkplaceInfo(Insertable<WorkplaceInfo> workplaceInfo) => into(workplaceInfos).insertOnConflictUpdate(workplaceInfo);

  Future<List<WorkplaceInfo>> getWorkplaceInfosByProject(int workplaceID) {
    return (select(workplaceInfos)
              ..where((tbl) => tbl.projectID.equals(workplaceID))
              ..orderBy([
                (t) => OrderingTerm(expression: t.sequance, mode: OrderingMode.asc),
              ])
            ).get();
  }
}