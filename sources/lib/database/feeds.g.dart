// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feeds.dart';

// ignore_for_file: type=lint
mixin _$FeedDaoMixin on DatabaseAccessor<AppDatabase> {
  $FeedsTable get feeds => attachedDatabase.feeds;
  $WorkplacesTable get workplaces => attachedDatabase.workplaces;
  $TeamMembersTable get teamMembers => attachedDatabase.teamMembers;
  $TopicsTable get topics => attachedDatabase.topics;
  $MediasTable get medias => attachedDatabase.medias;
  $TaskMediasTable get taskMedias => attachedDatabase.taskMedias;
}
mixin _$MediaDaoMixin on DatabaseAccessor<AppDatabase> {
  $MediasTable get medias => attachedDatabase.medias;
  $FeedsTable get feeds => attachedDatabase.feeds;
}
mixin _$TaskMediaDaoMixin on DatabaseAccessor<AppDatabase> {
  $TaskMediasTable get taskMedias => attachedDatabase.taskMedias;
}
mixin _$AssociateMediaDaoMixin on DatabaseAccessor<AppDatabase> {
  $AssociateMediasTable get associateMedias => attachedDatabase.associateMedias;
}
