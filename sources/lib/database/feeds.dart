import 'package:drift/drift.dart';
import 'package:rxdart/rxdart.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/database/workplaces.dart';
import 'package:sources/database/topics.dart';

part 'feeds.g.dart';

// Chat feeds
class Feeds extends Table {
  TextColumn get uuid => text().nullable().named('uuid')(); // new column for DB version 2
  IntColumn get feedID => integer().named('feedID')();
  IntColumn get parentID => integer().nullable().named('parentID')();
  IntColumn get projectID => integer().named('projectID')();
  TextColumn get description => text().nullable().named('description')();
  TextColumn get originDesc => text().nullable().named('originDesc')(); // new column for DB version 2
  TextColumn get feedType => text().nullable().named('feedType')();
  TextColumn get postDateTime => text().nullable().named('postDateTime')();
  TextColumn get updatedDateTime => text().nullable().named('updatedDateTime')(); // new column for DB version 2
  IntColumn get topicID => integer().nullable().named('topicID')();
  IntColumn get teamMemberID => integer().nullable().named('teamMemberID')();
  TextColumn get moduleID => text().nullable().named('moduleID')(); // new column for DB version 10
  TextColumn get moduleName => text().nullable().named('moduleName')(); // new column for DB version 10
  TextColumn get updatesDescription => text().nullable().named('updatesDescription')(); // new column for DB version 10
  TextColumn get replyTeamMemberAvatar => text().nullable().named('replyTeamMemberAvatar')(); // new column for DB version 2
  IntColumn get replyCount => integer().withDefault(const Constant(0)).named('replyCount')();
  TextColumn get replyReadStatus => text().nullable().named('replyReadStatus')();
  TextColumn get backgroundColor => text().nullable().named('backgroundColor')();
  TextColumn get outlineColor => text().nullable().named('outlineColor')();
  TextColumn get fontNameColor => text().nullable().named('fontNameColor')();
  TextColumn get fontPositionColor => text().nullable().named('fontPositionColor')();
  TextColumn get fontDescriptionColor => text().nullable().named('fontDescriptionColor')();
  TextColumn get fontDateColor => text().nullable().named('fontDateColor')();
  IntColumn get lastReadFeedID => integer().nullable().named('lastReadFeedID')(); // This is the last read reply feed ID
  IntColumn get sendStatus => integer().named('sendStatus')(); // new column for DB version 2; 0 - fail, 1 - success, 2 - pending
  TextColumn get isRead => text().withDefault(const Constant("unread")).named('isRead')(); // new column for DB version 2

  @override
  Set<Column> get primaryKey => {feedID};
}

@DriftAccessor(tables: [Feeds, Workplaces, TeamMembers, Topics, Medias, TaskMedias])
class FeedDao extends DatabaseAccessor<AppDatabase> with _$FeedDaoMixin {
  final AppDatabase db;

  FeedDao(this.db) : super(db);

  Future<List<Feed>> getFeeds(int projectID) => (
    select(feeds)
    ..where((tbl) => tbl.projectID.equals(projectID))
    ..where((tbl) => tbl.parentID.isNull())
    ..orderBy([
      (t) => OrderingTerm(expression: t.postDateTime, mode: OrderingMode.desc),
    ])
  ).get();
  Stream<List<Feed>> watchFeeds(int projectID) => (
    select(feeds)
    ..where((tbl) => tbl.projectID.equals(projectID))
    ..where((tbl) => tbl.parentID.isNull())
    ..orderBy([
      (t) => OrderingTerm(expression: t.postDateTime, mode: OrderingMode.desc),
    ])
  ).watch();
  Future<List<Feed>> getReplyFeeds(int feedID) => (
    select(feeds)
    ..where((tbl) => tbl.parentID.equals(feedID))
    ..orderBy([
      (t) => OrderingTerm(expression: t.postDateTime, mode: OrderingMode.desc),
    ])
  ).get();
  Stream<List<Feed>> watchReplyFeeds(int feedID) => (
    select(feeds)
    ..where((tbl) => tbl.parentID.equals(feedID))
    ..orderBy([
      (t) => OrderingTerm(expression: t.postDateTime, mode: OrderingMode.desc),
    ])
  ).watch();
  Future<List<Feed>> getUnreadParentFeeds(int projectID) => (
    select(feeds)
    ..where((tbl) => tbl.projectID.equals(projectID))
    ..where((tbl) => tbl.parentID.isNull())
    ..where((tbl) => tbl.replyReadStatus.equals('unread'))
  ).get();
  Future<Feed?> getLatestUpdateFeed(int projectID) => (
    select(feeds)
    ..where((tbl) => tbl.projectID.equals(projectID))
    ..where((tbl) => tbl.parentID.isNull())
    ..orderBy([
      (t) => OrderingTerm(expression: t.updatedDateTime, mode: OrderingMode.desc),
    ])
    ..limit(1)
  ).getSingleOrNull();
  Future<Feed?> getLatestUpdateReplyFeed(int feedID) => (
    select(feeds)
    ..where((tbl) => tbl.parentID.equals(feedID))
    ..orderBy([
      (t) => OrderingTerm(expression: t.updatedDateTime, mode: OrderingMode.desc),
    ])
    ..limit(1)
  ).getSingleOrNull();
  Future<Feed?> getSingleFeed(int id) => (select(feeds)..where((tbl) => tbl.feedID.equals(id))).getSingleOrNull();
  Future insertOrUpdateFeeds(Insertable<Feed> feed) => into(feeds).insertOnConflictUpdate(feed);
  Future insertFeed(Insertable<Feed> feed) => into(feeds).insert(feed);
  Future updateFeed(Insertable<Feed> feed) => update(feeds).replace(feed);
  Future deleteFeeds(Insertable<Feed> feed) => delete(feeds).delete(feed);
  Future deleteAllFeeds() => delete(feeds).go();
  Future deleteAllUnsentFeeds() => (delete(feeds)
    ..where((tbl) => tbl.feedID.isSmallerOrEqualValue(-1))
  ).go();

  Future<List<Feed>> getLowestFeedID() => (select(feeds)..orderBy([(t) => OrderingTerm(expression: t.feedID)])).get();
  Future updateFeedStatus(String uuid, int newId, int sendStatus) => (update(feeds)..where((tbl) => tbl.uuid.equals(uuid))).write(FeedsCompanion(feedID: Value(newId), sendStatus: Value(sendStatus)));
  Future<Feed?> getFeedbyUuid(String uuid) => (select(feeds)
                                                ..where((tbl) => tbl.uuid.equals(uuid))
                                              ).getSingleOrNull();

  Stream<List<FeedWithDetail>> watchFeedsWithDetail(int projectID, List<int> filterBy) {
    if (filterBy.length == 0) {
      final feedStream = (
        select(feeds)
        ..where((tbl) => tbl.projectID.equals(projectID))
        ..where((tbl) => tbl.parentID.isNull())
        ..orderBy([
          (t) => OrderingTerm(expression: t.postDateTime, mode: OrderingMode.desc),
        ])
      ).watch();
      final workplaceStream = (select(workplaces)..where((tbl) => tbl.projectID.equals(projectID))).watch();
      final teamMemberStream = (select(teamMembers)..where((tbl) => tbl.projectID.equals(projectID))).watch();
      final topicStream = (select(topics).watch());
      final mediaStream = (select(medias).watch());
      final failedFeedReplyStream = (select(feeds)..where((tbl) => tbl.projectID.equals(projectID))..where((tbl) => tbl.feedID.isSmallerOrEqualValue(-1))).watch();
      final taskMediaStream = (select(taskMedias).watch());

      return Rx.combineLatest7(feedStream, workplaceStream, teamMemberStream, topicStream, mediaStream, failedFeedReplyStream, taskMediaStream, 
      (List<Feed> a, List<Workplace> b, List<TeamMember> c, List<Topic> d, List<Media> e, List<Feed> f, List<TaskMedia> g) {
        return a.map((feed) {
          final feedDetail = feed;
          final workplaceDetail = b.firstWhere((element) => element.projectID == feed.projectID);
          final teamMemberDetail = c.firstWhere((element) => element.teamMemberID == feedDetail.teamMemberID);
          final topicDetail = d.firstWhere((element) => element.topicID == feedDetail.topicID);
          final mediaList = e.where((element) => element.feedUuid == feedDetail.uuid).toList();
          final replyFeedFailedCount = f.where((element) => element.parentID == feedDetail.feedID).toList().length;
          final taskMediaList = g.where((element) => element.feedID == feedDetail.feedID).toList();

          return FeedWithDetail(
            feedDetail: feedDetail,
            workplaceDetail: workplaceDetail,
            teamMemberDetail: teamMemberDetail,
            topicDetail: topicDetail,
            mediaList: mediaList,
            replyFeedFailedCount: replyFeedFailedCount,
            taskMediaList: taskMediaList,
          );
        }).toList();
      });
    }
    else {
      final feedStream = (
        select(feeds)
        ..where((tbl) => tbl.projectID.equals(projectID))
        ..where((tbl) => tbl.parentID.isNull())
        ..where((tbl) => tbl.topicID.isIn(filterBy))
        ..orderBy([
          (t) => OrderingTerm(expression: t.postDateTime, mode: OrderingMode.desc),
        ])
      ).watch();
      final workplaceStream = (select(workplaces)..where((tbl) => tbl.projectID.equals(projectID))).watch();
      final teamMemberStream = (select(teamMembers)..where((tbl) => tbl.projectID.equals(projectID))).watch();
      final topicStream = (select(topics).watch());
      final mediaStream = (select(medias).watch());
      final failedFeedReplyStream = (select(feeds)..where((tbl) => tbl.projectID.equals(projectID))..where((tbl) => tbl.feedID.isSmallerOrEqualValue(-1))).watch();
      final taskMediaStream = (select(taskMedias).watch());

      return Rx.combineLatest7(feedStream, workplaceStream, teamMemberStream, topicStream, mediaStream, failedFeedReplyStream, taskMediaStream,
      (List<Feed> a, List<Workplace> b, List<TeamMember> c, List<Topic> d, List<Media> e, List<Feed> f, List<TaskMedia> g) {
        return a.map((feed) {
          final feedDetail = feed;
          final workplaceDetail = b.firstWhere((element) => element.projectID == feed.projectID);
          final teamMemberDetail = c.firstWhere((element) => element.teamMemberID == feedDetail.teamMemberID);
          final topicDetail = d.firstWhere((element) => element.topicID == feedDetail.topicID);
          final mediaList = e.where((element) => element.feedUuid == feedDetail.uuid).toList();
          final replyFeedFailedCount = f.where((element) => element.parentID == feedDetail.feedID).toList().length;
          final taskMediaList = g.where((element) => element.feedID == feedDetail.feedID).toList();

          return FeedWithDetail(
            feedDetail: feedDetail,
            workplaceDetail: workplaceDetail,
            teamMemberDetail: teamMemberDetail,
            topicDetail: topicDetail,
            mediaList: mediaList,
            replyFeedFailedCount: replyFeedFailedCount,
            taskMediaList: taskMediaList,
          );
        }).toList();
      });
    }
  }

  Stream<List<FeedWithDetail>> watchReplyFeedsWithDetail(int projectID, int feedID) {
    final feedStream = (
        select(feeds)
        ..where((tbl) => tbl.projectID.equals(projectID))
        ..where((tbl) => tbl.parentID.equals(feedID))
        ..orderBy([
          (t) => OrderingTerm(expression: t.postDateTime, mode: OrderingMode.desc),
        ])
      ).watch();
      final workplaceStream = (select(workplaces)..where((tbl) => tbl.projectID.equals(projectID))).watch();
      final teamMemberStream = (select(teamMembers)..where((tbl) => tbl.projectID.equals(projectID))).watch();
      final topicStream = (select(topics).watch());
      final mediaStream = (select(medias).watch());

      return Rx.combineLatest5(feedStream, workplaceStream, teamMemberStream, topicStream, mediaStream, 
      (List<Feed> a, List<Workplace> b, List<TeamMember> c, List<Topic> d, List<Media> e) {
        return a.map((feed) {
          final feedDetail = feed;
          final workplaceDetail = b.firstWhere((element) => element.projectID == feed.projectID);
          final teamMemberDetail = c.firstWhere((element) => element.teamMemberID == feedDetail.teamMemberID);
          final topicDetail = d.firstWhere((element) => element.topicID == feedDetail.topicID);
          final mediaList = e.where((element) => element.feedUuid == feedDetail.uuid).toList();

          return FeedWithDetail(
            feedDetail: feedDetail,
            workplaceDetail: workplaceDetail,
            teamMemberDetail: teamMemberDetail,
            topicDetail: topicDetail,
            mediaList: mediaList,
          );
        }).toList();
      });
  }

  // Delete feeds if feedIDs is not in the array
  Future deleteFeedsByExcludedIds(int projectID, List<int> ids) => (
    delete(feeds)
    ..where((tbl) => tbl.projectID.equals(projectID))
    ..where((tbl) => tbl.feedID.isBiggerOrEqualValue(0))
    ..where((tbl) => tbl.feedID.isNotIn(ids))
  ).go();

  Future updateReadStatus(int projectID, int? parentID) {
    if (parentID == null) {
      return (update(feeds)..where((tbl) => tbl.projectID.equals(projectID))..where((tbl) => tbl.parentID.isNull())).write(FeedsCompanion(isRead: Value('read')));
    }
    else {
      return (update(feeds)..where((tbl) => tbl.projectID.equals(projectID))).write(FeedsCompanion(isRead: Value('read')));
      // return (update(feeds)..where((tbl) => tbl.projectID.equals(projectID))..where((tbl) => tbl.parentID.equals(parentID))).write(FeedsCompanion(isRead: Value('read')));
    }
  }
}

class Medias extends Table {
  IntColumn get mediaID => integer().named('mediaID')();
  IntColumn get projectID => integer().named('projectID')();
  IntColumn get feedID => integer().named('feedID')();
  TextColumn get url => text().nullable().named('url')();
  TextColumn get fileExtension => text().nullable().named('fileExtension')();
  TextColumn get localFilePath => text().nullable().named('localFilePath')(); // new column for DB version 2
  TextColumn get feedUuid => text().nullable().named('feedUuid')(); // new column for DB version 2

  @override
  Set<Column> get primaryKey => {mediaID};
}

@DriftAccessor(tables: [Medias, Feeds])
class MediaDao extends DatabaseAccessor<AppDatabase> with _$MediaDaoMixin {
  final AppDatabase db;

  MediaDao(this.db) : super(db);

  Future<List<Media>> getLowestMediaID() => (select(medias)..orderBy([(t) => OrderingTerm(expression: t.mediaID)])).get();
  Future updateMediaData(int oldId, int newId, String url) => (update(medias)..where((tbl) => tbl.mediaID.equals(oldId))).write(MediasCompanion(mediaID: Value(newId), url: Value(url)));
  Future updateMediaFeedID(int oldId, int newId) => (update(medias)..where((tbl) => tbl.feedID.equals(oldId))).write(MediasCompanion(feedID: Value(newId)));
  Future<List<Media>> getMedias(int feedID) => (select(medias)..where((tbl) => tbl.feedID.equals(feedID))).get();
  Stream<List<Media>> watchMedias(int feedID) => (select(medias)..where((tbl) => tbl.feedID.equals(feedID))).watch();
  Future insertOrUpdateMedia(Insertable<Media> media) => into(medias).insertOnConflictUpdate(media);
  Future insertMedia(Insertable<Media> media) => into(medias).insert(media);
  Future deleteMedia(Insertable<Media> media) => delete(medias).delete(media);
  Future deleteAllMedias() => delete(medias).go();
  Future deleteMediaByMediaID(int mediaID) => (delete(medias)..where((tbl) => tbl.mediaID.equals(mediaID))).go();
  Future deleteMediasByFeedID(int feedID) => (delete(medias)..where((tbl) => tbl.feedID.equals(feedID))).go();
  // Delete feeds if feedIDs is not in the array
  Future deleteMediasByExcludedIds(int feedID, List<int> ids) => (
    delete(medias)
    ..where((tbl) => tbl.feedID.equals(feedID))
    ..where((tbl) => tbl.mediaID.isNotIn(ids))
  ).go();
  Future getMediasByProjectID(int projectID) => (select(medias)
    ..where((tbl) => tbl.projectID.equals(projectID))
    ..where((tbl) => tbl.fileExtension.isIn(['jpg', 'jpeg', 'png', 'webp', 'gif', 'heic']))
  ).get();

  // Steam images only by project ID
  Stream<List<MediaWithFeed>> watchImagesWithFeedByProjectID(int projectID) {
    return (select(medias)
      ..where((tbl) => tbl.projectID.equals(projectID))
      ..where((tbl) => tbl.fileExtension.isIn(['jpg', 'jpeg', 'png', 'webp', 'gif', 'heic', 'mp4']))
      ..orderBy([
        (t) => OrderingTerm(expression: t.mediaID, mode: OrderingMode.desc),
      ])
      )
      .join([
        leftOuterJoin(feeds, feeds.uuid.equalsExp(medias.feedUuid)),
      ])
      .watch()
      .map(
        (rows) => rows.map(
          (row) {
            return MediaWithFeed(
              mediaDetail: row.readTable(medias),
              feedDetail: row.readTable(feeds),
            );
          }
        ).toList()
      );
  }

  // Stream documents only by project ID
  Stream<List<MediaWithFeed>> watchDocumentsWithFeedByProjectID(int projectID) {
    return (select(medias)
      ..where((tbl) => tbl.projectID.equals(projectID))
      ..where((tbl) => tbl.fileExtension.isNotIn(['jpg', 'jpeg', 'png', 'webp', 'gif', 'heic', 'mp4']))
      ..orderBy([
        (t) => OrderingTerm(expression: t.mediaID, mode: OrderingMode.desc),
      ])
      )
      .join([
        leftOuterJoin(feeds, feeds.uuid.equalsExp(medias.feedUuid)),
      ])
      .watch()
      .map(
        (rows) => rows.map(
          (row) {
            return MediaWithFeed(
              mediaDetail: row.readTable(medias),
              feedDetail: row.readTable(feeds),
            );
          }
        ).toList()
      );
  }
}

class TaskMedias extends Table {
  IntColumn get taskMediaID => integer().named('taskMediaID')();
  TextColumn get mediaName => text().nullable().named('mediaName')();
  TextColumn get mediaType => text().nullable().named('mediaType')();
  IntColumn get taskID => integer().named('taskID')();
  IntColumn get feedID => integer().named('feedID')();
  IntColumn get projectID => integer().named('projectID')();
  IntColumn get teamMemberID => integer().named('teamMemberID')();
  TextColumn get mediaPath => text().nullable().named('mediaPath')();
  TextColumn get localFilePath => text().nullable().named('localFilePath')();

  @override
  Set<Column> get primaryKey => {taskMediaID};
}

@DriftAccessor(tables: [TaskMedias])
class TaskMediaDao extends DatabaseAccessor<AppDatabase> with _$TaskMediaDaoMixin {
  final AppDatabase db;

  TaskMediaDao(this.db) : super(db);

  Future insertOrUpdateTaskMedia(Insertable<TaskMedia> taskMedia) => into(taskMedias).insertOnConflictUpdate(taskMedia);
  Future getTaskMediasByExcludedIds(int feedID, List<int> ids) 
    => (select(taskMedias)
        ..where((tbl) => tbl.feedID.equals(feedID))
        ..where((tbl) => tbl.taskMediaID.isNotIn(ids))
        ).get();
  Future deleteTaskMediasByExcludedIds(int feedID, List<int> ids) => (
    delete(taskMedias)
    ..where((tbl) => tbl.feedID.equals(feedID))
    ..where((tbl) => tbl.taskMediaID.isNotIn(ids))
  ).go();
  Future deleteAllTaskMedias() => delete(taskMedias).go();
}

class AssociateMedias extends Table {
  IntColumn get mediaID => integer().named('mediaID')();
  IntColumn get projectID => integer().named('projectID')();
  TextColumn get projectFeedsMedia => text().nullable().named('projectFeedsMedia')();
  TextColumn get projectFeedsMediaType => text().nullable().named('projectFeedsMediaType')();
  TextColumn get createdDateTime => text().named('createdDateTime')();
  TextColumn get caption => text().named('caption')();
  TextColumn get module => text().named('module')();
  TextColumn get localFilePath => text().nullable().named('localFilePath')();

  @override
  Set<Column> get primaryKey => {mediaID, module};
}

@DriftAccessor(tables: [AssociateMedias])
class AssociateMediaDao extends DatabaseAccessor<AppDatabase> with _$AssociateMediaDaoMixin {
  final AppDatabase db;

  AssociateMediaDao(this.db) : super(db);
  
  Future insertOrUpdateAssociateMedia(Insertable<AssociateMedia> associateMedia) => into(associateMedias).insertOnConflictUpdate(associateMedia);
  Future deleteAssociateMedia(int projectID, int mediaID) => (delete(associateMedias)..where((tbl) => tbl.mediaID.equals(mediaID))).go();
  Stream<List<AssociateMedia>> watchAssociateImagesWithFeedByProjectID(int projectID) => (select(associateMedias)
    ..where((tbl) => tbl.projectID.equals(projectID))
    ..where((tbl) => tbl.projectFeedsMediaType.isIn(['jpg', 'jpeg', 'png', 'webp', 'gif', 'heic', 'mp4']))
    ..orderBy([
      (tbl) => OrderingTerm(expression: tbl.createdDateTime, mode: OrderingMode.desc),
    ])
  ).watch();
  Stream<List<AssociateMedia>> watchAssociateDocumentsWithFeedByProjectID(int projectID) => (select(associateMedias)
    ..where((tbl) => tbl.projectID.equals(projectID))
    ..where((tbl) => tbl.projectFeedsMediaType.isNotIn(['jpg', 'jpeg', 'png', 'webp', 'gif', 'heic', 'mp4']))
    ..orderBy([
      (tbl) => OrderingTerm(expression: tbl.createdDateTime, mode: OrderingMode.desc),
    ])
  ).watch();
  Future deleteAllAssociateMedias() => delete(associateMedias).go();
}

class FeedWithDetail {
  final Feed feedDetail;
  final Workplace? workplaceDetail;
  final TeamMember? teamMemberDetail;
  final Topic? topicDetail;
  final List<Media>? mediaList;
  final int? replyFeedFailedCount;
  final List<TaskMedia>? taskMediaList;

  FeedWithDetail({required this.feedDetail, this.workplaceDetail, this.teamMemberDetail, this.topicDetail, this.mediaList, this.replyFeedFailedCount, this.taskMediaList});
}

class MediaWithFeed {
  final Media mediaDetail;
  final Feed feedDetail;

  MediaWithFeed({required this.mediaDetail, required this.feedDetail});
}