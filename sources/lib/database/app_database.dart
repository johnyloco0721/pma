import 'package:drift/drift.dart';

import 'package:sources/database/workplaces.dart';
import 'package:sources/database/topics.dart';
import 'package:sources/database/feeds.dart';
import 'package:sources/database/alerts.dart';
import 'package:sources/database/profile.dart';

part 'app_database.g.dart';

@DriftDatabase(tables: [Workplaces, TeamMembers, Topics, Feeds, Medias, Alerts, Profiles, CostEstimations, TaskMedias, WorkplaceInfos, AssociateMedias], 
          daos: [WorkplaceDao, TeamMemberDao, TopicDao, FeedDao, MediaDao, AlertDao, ProfileDao, CostEstimationDao, TaskMediaDao, WorkplaceInfoDao, AssociateMediaDao])
class AppDatabase extends _$AppDatabase {
  AppDatabase(LazyDatabase e) : super(e);

  // this is the new constructor
  AppDatabase.connect(DatabaseConnection connection) : super(connection.executor);

  @override
  int get schemaVersion => 15;

  MigrationStrategy get migration => MigrationStrategy(
    onUpgrade: (Migrator migrator, int from, int to) async {
      if (from <= 1) {
        // upgrade from 1, all migrations from 1 to 2 here
        // await migrator.createTable(teamMembers);
        await migrator.addColumn(feeds, feeds.uuid);
        await migrator.addColumn(feeds, feeds.replyTeamMemberAvatar);
      }
      if (from <= 2) {
        // upgrade from 2, all migrations from 2 to 3 here
        await migrator.addColumn(feeds, feeds.isRead);
        await migrator.addColumn(feeds, feeds.sendStatus);
        await migrator.addColumn(feeds, feeds.originDesc);
        await migrator.addColumn(feeds, feeds.updatedDateTime);
        await migrator.addColumn(alerts, alerts.updatedDateTime);
        await migrator.addColumn(medias, medias.localFilePath);
        await migrator.addColumn(medias, medias.feedUuid);
        await migrator.addColumn(workplaces, workplaces.updatedDateTime);
        await migrator.addColumn(profiles, profiles.teamMemberID);
      }
      if (from <= 3) {
        // upgrade from 3, all migrations from 3 to 4 here
        await migrator.alterTable(TableMigration(feeds));
      }
      if (from <= 4) {
        // upgrade from 4, all migrations from 4 to 5 here
        await migrator.addColumn(costEstimations, costEstimations.localFilePath);
      }
      if (from <= 5) {
        // upgrade from 5, all migrations from 5 to 6 here
        await migrator.alterTable(TableMigration(workplaces));
      }
      if (from <= 6) {
        // upgrade from 6, all migrations from 6 to 7 here
        await migrator.alterTable(TableMigration(workplaces));
      }
      if (from <= 7) {
        // upgrade from 7, all migrations from 7 to 8 here
        await migrator.addColumn(workplaces, workplaces.completedTasks);
        await migrator.addColumn(workplaces, workplaces.totalTasks);
      }
      if (from <= 8) {
        // upgrade from 8, all migrations from 8 to 9 here
        await migrator.addColumn(workplaces, workplaces.displayDateTime);
      }
      if (from <= 9) {
        // upgrade from 9, all migrations from 9 to 10 here
        await migrator.addColumn(feeds, feeds.moduleID);
        await migrator.addColumn(feeds, feeds.moduleName);
        await migrator.addColumn(feeds, feeds.updatesDescription);
        await migrator.createTable(taskMedias);
      }
      if (from <= 10) {
        // upgrade from 10, all migrations from 10 to 11 here
        await migrator.createTable(workplaceInfos);
      }
      if (from <= 11) {
        // upgrade from 11, all migrations from 11 to 12 here
        await migrator.addColumn(workplaceInfos, workplaceInfos.sequance);
      }
      if (from <= 12) {
        // upgrade from 12, all migrations from 12 to 13 here
        await migrator.createTable(associateMedias);
      }
      if (from <= 13) {
        // upgrade from 13, all migrations from 13 to 14 here
        await migrator.addColumn(alerts, alerts.delayTaskIDs);
        await migrator.addColumn(alerts, alerts.delayDuration);
      }
      if (from <= 14) {
        // upgrade from 14, all migrations from 14 to 15 here
        await migrator.addColumn(alerts, alerts.allNotificationTxt);
      }
    },
    // beforeOpen: (details) async {
    //   await customStatement('PRAGMA foreign_keys = ON');
    // },
  );
}