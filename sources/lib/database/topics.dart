import 'package:drift/drift.dart';
import 'package:sources/database/app_database.dart';

part 'topics.g.dart';

// Feed topics
class Topics extends Table {
  IntColumn get topicID => integer().named('topicID')();
  TextColumn get topicName => text().nullable().named('topicName')();
  TextColumn get topicIcon => text().nullable().named('topicIcon')();
  TextColumn get topicColour => text().nullable().named('topicColour')();
  IntColumn get weight => integer().withDefault(const Constant(0)).named('weight')();

  @override
  Set<Column> get primaryKey => {topicID};
}

@DriftAccessor(tables: [Topics])
class TopicDao extends DatabaseAccessor<AppDatabase> with _$TopicDaoMixin {
  final AppDatabase db;

  TopicDao(this.db) : super(db);

  Future<List<Topic>> getTopics() => (select(topics)
    ..orderBy([
      (t) => OrderingTerm(expression: t.weight, mode: OrderingMode.asc),
    ])
  ).get();
  Stream<List<Topic>> watchTopics() => select(topics).watch();
  Future<Topic?> getSingleTopic(int id) => (select(topics)..where((tbl) => tbl.topicID.equals(id))).getSingleOrNull();
  Future insertOrUpdateTopic(Insertable<Topic> topic) => into(topics).insertOnConflictUpdate(topic);
  Future deleteTopic(Insertable<Topic> topic) => delete(topics).delete(topic);
  Future deleteAllTopic() => delete(topics).go();
}
