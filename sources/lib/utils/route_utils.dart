import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:sources/home_view.dart';

import 'package:sources/services/service_locator.dart';
import 'package:sources/view_models/app_view_model.dart';

AppViewModel appViewModel = locator<AppViewModel>();

goAndClearAllPageNamed(BuildContext context, pageName, {args}) {
  Navigator.pushNamedAndRemoveUntil(
      context, pageName, (Route<dynamic> route) => false, arguments: args);
}

goToNextNamedRoute(BuildContext context, pageName, {args, VoidCallback? callback}) {
  Navigator.pushNamed(context, pageName, arguments: args).then((value) => (callback != null) ? callback() : null);
}

goToReplacementNamedRoute(BuildContext context, pageName, {args, VoidCallback? callback}) {
  Navigator.pushReplacementNamed(context, pageName, arguments: args).then((value) => (callback != null) ? callback() : null);
}

goAndClearCurrentScreen(BuildContext context, Widget page) {
  Navigator.of(context, rootNavigator: false).pushReplacement(
    MaterialPageRoute(builder: (_) => page),
  );
}

goAndClearAllScreen(BuildContext context, Widget page, {String name = '/'}) {
  Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (BuildContext context) => page),
      ModalRoute.withName(name));
}

goToNormalPage(BuildContext context, Widget page) {
  Navigator.of(context, rootNavigator: false).push(
    MaterialPageRoute<void>(builder: (_) => page),
  );
}

goToNormalPageWithTransition(BuildContext context, Widget page,
    {transitionType = PageTransitionType.bottomToTop}) {
  Navigator.push(
    context,
    PageTransition(
      child: page,
      type: transitionType,
      duration: Duration(milliseconds: 300),
    ),
  );
}

goToNormalPageByNavigatorKey(GlobalKey<NavigatorState> navigatorKey, String name, {args}) {
  navigatorKey.currentState!.pushNamedAndRemoveUntil(name, (route) => false, arguments: args);
}

notificationRedirection(String name, int currectIndex, {int toIndex = 0, args}) async {
  HomeView.of(appViewModel.tabsNavigatorKey[currectIndex].currentContext!)!.setIndex(toIndex);
  await Future.delayed(Duration(milliseconds: 300));
  appViewModel.tabsNavigatorKey[toIndex].currentState!.pushNamed(name, arguments: args);
}