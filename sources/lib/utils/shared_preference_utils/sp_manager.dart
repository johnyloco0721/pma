import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:sources/models/config_model.dart';
import 'package:sources/models/profile_model.dart';
import 'package:sources/utils/shared_preference_utils/pref_keys.dart';

class SpManager {
  static SpManager? _storageUtil;
  static SharedPreferences? _preferences;

  static Future<SpManager?> getInstance() async {
    _preferences = await SharedPreferences.getInstance();
    if (_storageUtil == null) {
      // keep local instance till it is fully initialized.
      var secureStorage = SpManager._();
      await secureStorage._init();
      _storageUtil = secureStorage;
    }
    return _storageUtil;
  }

  SpManager._();

  Future _init() async {
    _preferences = await SharedPreferences.getInstance();
  }

  // get string
  static String? getString(String key, {String? defValue}) {
    if (_preferences == null) return defValue;
    return _preferences!.getString(key) ?? defValue;
  }

  // put string
  static Future<bool>? putString(String key, String value) {
    if (_preferences == null) return null;
    return _preferences!.setString(key, value);
  }

  // get int
  static int getInt(String key, {int defValue = 0}) {
    if (_preferences == null) return defValue;
    return _preferences!.getInt(key) ?? defValue;
  }

  // put int
  static Future<bool>? putInt(String key, int value) {
    if (_preferences == null) return null;
    return _preferences!.setInt(key, value);
  }

  // set login status
  static setLogin(bool value, {bool defValue = false}) {
    if (_preferences == null) return null;
    return _preferences?.setBool(loginKey, value) ?? defValue;
  }

  // get login status
  static isLogin() {
    if (_preferences == null) return null;
    return _preferences?.getBool(loginKey) ?? false;
  }

  // set user data
  static saveUser(Map<String, dynamic> user) {
    String mapToStr = jsonEncode(user);
    putString(userKey, mapToStr);
  }

  // get user data
  static getUserdata() {
    ProfileDetails? user;
    String? stringValue = _preferences!.getString(userKey);
    if (stringValue != null) {
      Map<String, dynamic> profileMap = jsonDecode(stringValue);
      user = ProfileDetails.fromJson(profileMap);
    }
    return user;
  }

  // set config data
  static saveConfig(dynamic config) {
    String mapToStr = jsonEncode(config);
    putString(configKey, mapToStr);
  }

  // get config data
  static getConfigData() {
    ConfigDetails? config;
    String? stringValue = _preferences!.getString(configKey);
    if (stringValue != null) {
      Map<String, dynamic> configMap = jsonDecode(stringValue);
      config = ConfigDetails.fromJson(configMap);
    }
    return config;
  }

  static saveFcmPayload(String payload) {
    putString('fcmPayload', payload);
  }

  static getFcmPayload() {
    String? stringValue = _preferences!.getString('fcmPayload');
    return stringValue;
  }

  // set synced data API
  static setSyncedAPI(String apiName) {
    String? stringValue = _preferences!.getString(syncedAPI);
    List<dynamic> apiList = jsonDecode(stringValue ?? '[]');
    apiList.add(apiName);
    putString(syncedAPI, jsonEncode(apiList));
  }

  // get synced data API
  static getSyncedAPI() {
    String? stringValue = _preferences!.getString(syncedAPI);
    return jsonDecode(stringValue ?? '[]');
  }

  // remove login status, user, apiToken from share preferences
  static logoutDeleteData(){
    removeItem(userKey);
    removeItem(loginKey);
    removeItem(apiToken);
  }
  
  // remove specific item from share preferences
  static Future removeItem(String key) async {
    _preferences!.remove(key);
  }
}