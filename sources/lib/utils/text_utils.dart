

import 'package:flutter/material.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_fonts.dart';

TextStyle regularFontsStyle({
  fontSize = 19.0,
  color = black,
  height = 1.29,
  letterSpacing,
  decoration,
}) {
  return TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: fontSize,
    color: color,
    height: height,
    letterSpacing: letterSpacing,
    fontFamily: dmSans,
    decoration: decoration,
  );
}

TextStyle italicFontsStyle({
  fontSize = 19.0,
  color = black,
  height = 1.29,
  letterSpacing,
  fontWeight = FontWeight.w400,
  decoration,
}) {
  return TextStyle(
    fontSize: fontSize,
    fontStyle: FontStyle.italic,
    fontWeight: fontWeight,
    color: color,
    height: height,
    letterSpacing: letterSpacing,
    fontFamily: dmSans,
    decoration: decoration,
  );
}

TextStyle boldFontsStyle({
  fontSize = 19.0,
  color = black,
  height = 1.29,
  fontFamily = dmSans,
  fontWeight = FontWeight.w700,
  letterSpacing = 0.0,
  decoration,
}) {
  return TextStyle(
    fontWeight: fontWeight,
    fontSize: fontSize,
    color: color,
    height: height,
    fontFamily: fontFamily,
    letterSpacing: letterSpacing,
    decoration: decoration,
    decorationColor: color,
  );
}

class TextUtils {
  static convertStringToHtml(String string) {
    final Map<String, List<String>> map = {
      r'\*(.*?)\*': ["<b>", "</b>"],
      r'\^(.*?)\^': ["<i>", "</i>"],
      r'_(.*?)\_': ["<u>", "</u>"],
      '~(.*?)~' : ["<del>", "</del>"],
    };

    map.forEach((key, mapping) { 
      String matchedString = "";
      String convertedString = "";

      RegExp regExp = new RegExp(key, caseSensitive: false, multiLine: true);
      if (regExp.hasMatch(string)) {
        switch (key) {
          case r'\*(.*?)\*':
            matchedString = regExp.stringMatch(string).toString(); //*bold*
            convertedString = matchedString.replaceFirst('*', mapping[0]); //<b>bold</b>
            convertedString = convertedString.replaceFirst('*', mapping[1]);
            string = string.replaceFirst(matchedString, convertedString);
            break;
          case r'\^(.*?)\^':
            matchedString = regExp.stringMatch(string).toString();
            convertedString = matchedString.replaceFirst('^', mapping[0]);
            convertedString = convertedString.replaceFirst('^', mapping[1]);
            string = string.replaceFirst(matchedString, convertedString);
            break;
          case r'_(.*?)\_':
            matchedString = regExp.stringMatch(string).toString();
            convertedString = matchedString.replaceFirst('_', mapping[0]);
            convertedString = convertedString.replaceFirst('_', mapping[1]);
            string = string.replaceFirst(matchedString, convertedString);
            break;
          case '~(.*?)~' :
            matchedString = regExp.stringMatch(string).toString();
            convertedString = matchedString.replaceFirst('~', mapping[0]);
            convertedString = convertedString.replaceFirst('~', mapping[1]);
            string = string.replaceFirst(matchedString, convertedString);
            break;
        }
      }
    });

    return string;
  }
}