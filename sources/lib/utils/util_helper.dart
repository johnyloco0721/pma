import 'dart:io';

import 'package:drift/drift.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:sources/constants/app_data_models.dart';
import 'package:sources/constants/app_routes.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/models/config_model.dart';
import 'package:sources/services/api.dart';
import 'package:sources/services/app_service.dart';
import 'package:sources/services/service_locator.dart';
import 'package:sources/utils/route_utils.dart';
import 'package:sources/utils/shared_preference_utils/sp_manager.dart';

// app version comparison
appVersionComparison() async {
  PackageInfo packageInfo = await PackageInfo.fromPlatform();
  String currentAppVersion = packageInfo.version;
  bool result = true;

  try {
    await AppService.getConfig().then((response) async {
      ConfigModel configModel = ConfigModel.fromJson(response.data);
      if (configModel.status == true) {
        var json = configModel.response;
        SpManager.saveConfig(json);
      }
      if (configModel.response!.underMaintenance! && !(AppDataModel.getByPassCode())) {
        goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, appMaintenanceScreen);
        result = false;
      } else {
        int appVersion = getExtendedVersionNumber(currentAppVersion);
        int apiVersion = getExtendedVersionNumber(configModel.response!.mobileVersion ?? '');
        if (appVersion < apiVersion) {
          if (Platform.isAndroid) {
            await AppDataModel.setUpdateAppLink(configModel.response!.AndroidAppUpdateLink); // add new attribute to update new version
          } else if (Platform.isIOS) {
            await AppDataModel.setUpdateAppLink(configModel.response!.iOSAppUpdateLink); // add new attribute to update new version
          }
          goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, appUpdateScreen);
          result = false;
        }
      }
    });
  }
  catch (e) {
    if (e == 'Token mismatched.') {
      goToNormalPageByNavigatorKey(appViewModel.mainNavigatorKey, errorLoginScreen, args: e);
    }
    print('failed config API');
  }
  return result;
}

int getExtendedVersionNumber(String version) {
  List versionCells = version.split('.');
  versionCells = versionCells.map((i) => int.parse(i)).toList();
  return versionCells[0] * 100000 + versionCells[1] * 1000 + versionCells[2];
}

setStatusBarIconColor({iconColor = Brightness.dark}) =>
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: iconColor,
      statusBarBrightness: iconColor,
    ),
  );

// Convert datetime format
String formatDatetime({datetime, String format = 'd MMM y'}) {
  try {
    return DateFormat(format).format(DateTime.parse(datetime));
  } catch (e) {
    return "";
  }
}

// Check datetime null value
bool isNullDatetime(String? datetime) {
  if (datetime == "0000-00-00 00:00:00" || datetime == null) {
    return true;
  }
  return false;
}

// Convert hex color code to Color
Color hexToColor(String code) {
  return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xff000000);
}

// Remove HTML tags from string
String removeAllHtmlTags(String htmlText) {
  RegExp exp = RegExp(
    r"<[^>]*>",
    multiLine: true,
    caseSensitive: true
  );

  return htmlText.replaceAll(exp, '');
}

downloadMediaToCache(BuildContext context, List<Media> mediaList) async {
    List<String> tmpLocalURLs = [];
    await Future.forEach(mediaList, (Media media) async {
      String tmpLocalURL = '';
      if (media.localFilePath != null && await File(media.localFilePath ?? '').exists()) {
        tmpLocalURL = media.localFilePath!;
      } else {
        tmpLocalURL = await Api().downloadFile(media.url ?? '', 'img', context, isShow: false, saveToGallery: false);
        await locator<AppDatabase>().mediaDao.insertOrUpdateMedia(media.copyWith(localFilePath: Value(tmpLocalURL)));
      }
      tmpLocalURLs.add(tmpLocalURL);
    });

    return tmpLocalURLs;
  }

// Reverse MarkupText from HTML
String reverseMarkupText(String markupText) {
  List<String> regExps = [
    r'<b>(.*?)<\/b>',
    r'<i>(.*?)<\/i>',
    r'<u>(.*?)<\/u>',
    '<s>(.*?)<\/s>' ,
  ];
  
  // may need add mentioned teammember in here
  String _pattern = "(${regExps.map((key) => key.startsWith('@') ? RegExp.escape(key) : key).join('|')})";
  String reverseMarkupText = markupText.splitMapJoin(
    RegExp('$_pattern'),
    onMatch: (Match match) {
      final patternMatched = regExps.firstWhere((element) {
        final reg = RegExp(element);
        return reg.hasMatch(match[0]!);
      });
      // Default markup format for mentions
      if (patternMatched == r'<b>(.*?)<\/b>') {
        return "*" + match[0]!.substring(3, match[0]!.length - 4) + "*";
      } else if (patternMatched == r'<i>(.*?)<\/i>') {
        return "^" + match[0]!.substring(3, match[0]!.length - 4) + "^";
      } else if (patternMatched == r'<u>(.*?)<\/u>') {
        return "_" + match[0]!.substring(3, match[0]!.length - 4) + "_";
      } else if (patternMatched == "<s>(.*?)<\/s>") {
        return "~" + match[0]!.substring(3, match[0]!.length - 4) + "~";
      } else {
        return match[0]!;
      }
    },
    onNonMatch: (String text) {
      return text;
    },
  );

  return reverseMarkupText;
}