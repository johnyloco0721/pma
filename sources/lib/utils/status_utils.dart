import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';

class Status {
  SvgPicture icon;
  Color primaryColor;
  Color secondaryColor;
  Color textColor;

  Status({required this.icon, required this.primaryColor, required this.secondaryColor, required this.textColor});
}

Status statusConfig(String status, {double iconWidth = 18.0}) {
  switch (status) {
    case 'flagged':
      return Status(
        icon: SvgPicture.asset(ic_task_flag, width: iconWidth, color: red), 
        primaryColor: red, 
        secondaryColor: pink,
        textColor: red,
      );
    case 'overdue':
      return Status(
        icon: SvgPicture.asset(ic_task_overtime, width: iconWidth, color: red), 
        primaryColor: red, 
        secondaryColor: pink,
        textColor: red,
      );
    case 'dueSoon':
      return Status(
        icon: SvgPicture.asset(ic_task_close_due_date, width: iconWidth, color: darkorange), 
        primaryColor: darkorange, 
        secondaryColor: lightorange,
        textColor: darkorange,
      );
    case 'incomplete':
      return Status(
        icon: SvgPicture.asset(ic_task_incomplete, width: iconWidth, color: black), 
        primaryColor: lightGrey, 
        secondaryColor: white,
        textColor: grey,
      );
    case 'completed':
    default:
      return Status(
        icon: SvgPicture.asset(ic_task_complete, width: iconWidth, color: green), 
        primaryColor: lightGrey, 
        secondaryColor: white,
        textColor: grey,
      );
  }
}