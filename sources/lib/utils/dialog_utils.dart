import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:share_plus/share_plus.dart';
import 'package:sources/args/push_update_info_args.dart';
import 'package:sources/constants/app_colors.dart';
import 'package:sources/constants/app_images.dart';
import 'package:sources/database/app_database.dart';
import 'package:sources/utils/text_utils.dart';
import 'package:sources/utils/util_helper.dart';
import 'package:sources/widgets/push_update_form.dart';
import 'package:url_launcher/url_launcher.dart';

showToastMessage(BuildContext context, {required String message, String position = 'bottom', int duration = 3}) {
  FToast fToast = FToast();
  fToast.init(context);
  Widget toast = GestureDetector(
    onTap: () {
      // Allow user to manually remove toast
      fToast.removeCustomToast();
    },
    child: Container(
      width: MediaQuery.of(context).size.width * 0.8,
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 16.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: darkGrey,
      ),
      child: Center(
        child: Text(
          message,
          style: boldFontsStyle(color: white, fontSize: 17.0),
          textAlign: TextAlign.center,
        ),
      ),
    ),
  );

  // Custom Toast Position
  fToast.showToast(
      child: toast,
      toastDuration: Duration(seconds: duration),
      positionedToastBuilder: (context, child) {
        return Positioned(
          child: child,
          top: position == 'top' ? 120 : null,
          bottom: (position == 'bottom') ? 120.0 : (position == 'bottom2' ? 200 : null),
          left: MediaQuery.of(context).size.width * 0.1,
      );
    }
  );
}

showMessage(BuildContext context, {required String message, String? icon}) {
  if (icon == null) {
    icon = ic_completed;
  }
  showDialog(
    context: context,
    builder: (BuildContext context) => AlertDialog(
      backgroundColor: white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      title: SvgPicture.asset(icon ?? '', width: 60),
      content: Text(
        message,
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: black),
        textAlign: TextAlign.center,
      ),
    ),
  );
}

showError(BuildContext context, String message) {
  showDialog(
    context: context,
    builder: (BuildContext context) => AlertDialog(
      backgroundColor: white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      title: SvgPicture.asset(
        ic_warning,
        width: 40,
      ),
      content: Text(
        message,
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: black),
        textAlign: TextAlign.center,
      ),
    ),
  );
}

showDecision(BuildContext context, String message, String falseButtonLabel, String trueButtonLabel, {Widget? titleIcon, Color? buttonColor, Color? textColor} ) {
  bool clicked = false;
  return showDialog(
    context: context,
    builder: (context) => AlertDialog(
      backgroundColor: white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      title: Column(
        children: [
          Align(
            alignment: Alignment.centerRight,
            child: GestureDetector(
              onTap: () => Navigator.pop(context, false),
              child: SvgPicture.asset(ic_close, width: 24.0, color: black)
            ),
          ),
          if (titleIcon != null)
            titleIcon
        ],
      ),
      content: StatefulBuilder(
        builder: (context, StateSetter setState) {
            return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                message,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, height: 1.5),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 32.0),
              Row(
                children: [
                  Expanded(
                    child: OutlinedButton (
                      style: OutlinedButton.styleFrom(
                        foregroundColor: black,
                        side: BorderSide(width: 1, color: black),
                        padding: EdgeInsets.symmetric(vertical: 12.0),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                      ),
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                      child: Text (
                        falseButtonLabel,
                        style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: black),
                      ),
                    ),
                  ),
                  SizedBox(width: 16.0),
                  Expanded(
                    child: Opacity(
                      opacity: clicked ? 0.5 : 1.0,
                      child: ElevatedButton (
                        style: ElevatedButton.styleFrom(
                          backgroundColor: buttonColor ?? red,
                          padding: EdgeInsets.symmetric(vertical: 12.0),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                        ),
                        onPressed: () {
                          setState(() {
                            clicked = true;
                          });
                          Navigator.pop(context, true);
                        },
                        child: Text (
                          trueButtonLabel,
                          style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: textColor ?? white),
                        ),
                      ),
                    ),
                  )
                ],
              )
            ],
          );
        }
      ),
    ), 
  );
}

mediaSelectionDialog(context, {
  cameraClick, 
  photoClick, 
  takeVideoClick, 
  videoClick, 
  fileClick, 
  bool cameraEnabled = true, 
  bool photoEnabled = true, 
  bool takeVideoEnabled = true, 
  bool videoEnabled = true, 
  bool fileEnabled = true
}) {
  showCupertinoModalPopup(
    context: context,
    builder: (BuildContext context) => CupertinoActionSheet(
      actions: [
        CupertinoActionSheetAction(
          onPressed: () {
            Navigator.pop(context);
            if (cameraEnabled) return cameraClick();
          },
          child: Text(
            'Camera',
            style: TextStyle(color: cameraEnabled ? Colors.blue : Colors.grey),
          ),
        ),
        CupertinoActionSheetAction(
          onPressed: () {
            Navigator.pop(context);
            if (photoEnabled) return photoClick();
          },
          child: Text(
            'Upload Image',
            style: TextStyle(color: photoEnabled ? Colors.blue : Colors.grey),
          ),
        ),
        if (takeVideoClick != null)
          CupertinoActionSheetAction(
            onPressed: () {
              Navigator.pop(context);
              if (takeVideoEnabled) return takeVideoClick();
            },
            child: Text(
              'Take Video',
              style: TextStyle(color: takeVideoEnabled ? Colors.blue : Colors.grey),
            ),
          ),
        if (videoClick != null)
          CupertinoActionSheetAction(
            onPressed: () {
              Navigator.pop(context);
              if (videoEnabled) return videoClick();
            },
            child: Text(
              'Upload Video',
              style: TextStyle(color: videoEnabled ? Colors.blue : Colors.grey),
            ),
          ),
        CupertinoActionSheetAction(
          onPressed: () {
            Navigator.pop(context);
            if (fileEnabled) return fileClick();
          },
          child: Text(
            'Upload PDF',
            style: TextStyle(color: fileEnabled ? Colors.blue : Colors.grey),
          ),
        ),
      ],
      cancelButton: CupertinoActionSheetAction(
        onPressed: () => Navigator.pop(context),
        child: Text('Cancel'),
      ),
    ),
  );
}

showContactDialog(BuildContext context, {String contactName = '', String number = ''}) {
  showModalBottomSheet(
    backgroundColor: transparent,
    context: context,
    builder: (context){
      return Container(
        padding: EdgeInsets.all(24.0),
        decoration: BoxDecoration(
          color: darkGrey,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.0),
            topRight: Radius.circular(30.0),
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                Expanded(
                  child: Text(
                    'Contact $contactName?',
                    style: boldFontsStyle(fontSize: 20.0, height: 1.4, color: white),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(width: 8.0),
                GestureDetector(
                  onTap: () => Navigator.pop(context),
                  child: SvgPicture.asset(ic_close, width: 20, color: white)
                ),    
              ],
            ),
            SizedBox(height: 32.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                // whatsapp button
                Expanded(
                  child: ElevatedButton (
                    style: ElevatedButton.styleFrom(
                      backgroundColor: green,
                      padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                    ),
                    onPressed: () {
                      // jump to whatsapp
                      launch("https://wa.me/$number");
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset(ic_contact_whatsapp, width: 24.0),
                        SizedBox(width: 8.0),
                        Text (
                          "Whatsapp",
                          style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: white),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 16.0),
                // phone call button
                Expanded(
                  child: ElevatedButton (
                    style: ElevatedButton.styleFrom(
                      backgroundColor: yellow,
                      padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                    ),
                    onPressed: () {
                      // jump to phone call
                      launch("tel://$number");
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset(ic_contact_phone, width: 24.0),
                        SizedBox(width: 8.0),
                        Text (
                          "Call",
                          style: boldFontsStyle(fontSize: 16.0, height: 1.5),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      );
    }
  );
}

onManualPush(BuildContext context, PushUpdateInfoArguments? pushUpdateInfo,) async {
  var pushed = false;
  await showCupertinoModalBottomSheet(
    context: context,
    builder: (context) {
      return PushUpdateForm(
        isFileRequire: pushUpdateInfo?.isFileRequired ?? false,
        title: pushUpdateInfo?.updateScreen == 'Edit Update' ? 'Edit Update' 
              : pushUpdateInfo?.updateScreen == 'Flagged' ? 'Flag This Update' 
              : pushUpdateInfo?.updateScreen == 'Resolve Task' ? 'Resolve Task' : 'Push Update',
        // remarkLabel: pushUpdateInfo?.updateScreen == 'Flagged' ?'Reason of Flagging*'
        //               :pushUpdateInfo?.updateScreen == 'Resolve Task' ? 'Remarks':'Update shown in Client App',
        pushUpdateInfo: pushUpdateInfo,
        onPush: (value) {
          pushed = value;
        }
      );
    }
  );
  return pushed;
}

workplaceShareDialog(BuildContext context, String feedDescription, List<Media> feedMediaList) {
  String shareText = removeAllHtmlTags(feedDescription);
  List<Media> pdfList = feedMediaList.where((e) => e.fileExtension == 'pdf').toList();
  List<Media> mediaList = feedMediaList.where((e) => e.fileExtension != 'pdf').toList();
  bool textClicked = false;
  bool imageClicked = false;
  bool pdfClicked = false;
  int onlyClickOnce = 0;

  showModalBottomSheet(
    backgroundColor: transparent,
    context: context,
    builder: (context){
      return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
            return Container(
            padding: EdgeInsets.all(24.0),
            decoration: BoxDecoration(
              color: darkGrey,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30.0),
                topRight: Radius.circular(30.0),
              ),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Share To Other Apps',
                  style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: white),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 16.0),
                // texts only button
                if (shareText != "")
                  Container(
                    margin: EdgeInsets.only(bottom: 16.0),
                    child: Opacity(
                      opacity: textClicked ? 0.5 : 1.0,
                      child: ElevatedButton (
                        style: ElevatedButton.styleFrom(
                          backgroundColor: yellow,
                          padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                        ),
                        onPressed: () async {
                          setState(() { 
                            textClicked = true;
                            onlyClickOnce = onlyClickOnce + 1;
                          });
                          // share Text only
                          if (onlyClickOnce == 1) {
                            await Share.share(
                              shareText,
                              // sharePositionOrigin: box!.localToGlobal(Offset.zero) & box.size
                            ).whenComplete(() {
                              setState(() { 
                                textClicked = false;
                                onlyClickOnce = 0;
                              });
                            });
                          }
                        },
                        child: Container(
                          width: double.infinity,
                          alignment: Alignment.center,
                          child: Text(
                            "Texts Only",
                            style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: black),
                          ),
                        ),
                      ),
                    ),
                  ),
                // Images Only button
                if (mediaList.length > 0)
                  Container(
                    margin: EdgeInsets.only(bottom: 16.0),
                    child: Opacity(
                      opacity: imageClicked ? 0.5 : 1.0,
                      child: ElevatedButton (
                        style: ElevatedButton.styleFrom(
                          backgroundColor: yellow,
                          padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                        ),
                        onPressed: () async {
                          setState(() {
                            imageClicked = true;
                            onlyClickOnce = onlyClickOnce + 1;
                          });
                          // share Images only
                          if (onlyClickOnce == 1) {
                            List<String> tmpLocalURLs = await downloadMediaToCache(context, mediaList);
                            List<XFile> xfileLocalURLs = [];
                            tmpLocalURLs.forEach((url) {
                              xfileLocalURLs.add(XFile(url));
                            });
                            await Share.shareXFiles(
                              xfileLocalURLs,
                              // 22 Dec 2022: Steph & Jonathan agree sahre file/image/video without text when user try to share text + file/image/video (both Android and IOS)
                              // text: removeAllHtmlTags((selectedFeedWithDetail.feedDetail.description ?? '') + " "),
                              // sharePositionOrigin: box!.localToGlobal(Offset.zero) & box.size
                            ).whenComplete(() {
                              setState(() { 
                                imageClicked = false;
                                onlyClickOnce = 0;
                              });
                            });
                          }
                        },
                        child: Container(
                          width: double.infinity,
                          alignment: Alignment.center,
                          child: Text(
                            "Images Only (${mediaList.length})",
                            style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: black),
                          ),
                        ),
                      ),
                    ),
                  ),
                // PDFs Only button
                if (pdfList.length > 0)
                  Container(
                    margin: EdgeInsets.only(bottom: 16.0),
                    child: Opacity(
                      opacity: pdfClicked ? 0.5 : 1.0,
                      child: ElevatedButton (
                        style: ElevatedButton.styleFrom(
                          backgroundColor: yellow,
                          padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 24.0),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                        ),
                        onPressed: () async {
                          setState(() {
                            pdfClicked = true;
                            onlyClickOnce = onlyClickOnce + 1;
                          });
                          // share PDFs only
                          if (onlyClickOnce == 1) {
                            List<String> tmpLocalURLs = await downloadMediaToCache(context, pdfList);
                            List<XFile> xfileLocalURLs = [];
                            tmpLocalURLs.forEach((url) {
                              xfileLocalURLs.add(XFile(url));
                            });
                            await Share.shareXFiles(
                              xfileLocalURLs,
                              // 22 Dec 2022: Steph & Jonathan agree sahre file/image/video without text when user try to share text + file/image/video (both Android and IOS)
                              // text: removeAllHtmlTags((selectedFeedWithDetail.feedDetail.description ?? '') + " "),
                              // sharePositionOrigin: box!.localToGlobal(Offset.zero) & box.size
                            ).whenComplete(() {
                              setState(() { 
                                pdfClicked = false;
                                onlyClickOnce = 0;
                              });
                            });
                          }
                          
                        },
                        child: Container(
                          width: double.infinity,
                          alignment: Alignment.center,
                          child: Text(
                            "PDFs Only (${pdfList.length})",
                            style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: black),
                          ),
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          );
        }
      );
    }
  );
}

confirmDialog(
  BuildContext context, {
  String icon = '',
  String contentMessage = '',
  String rejectText = 'Cancel',
  String confirmText = 'Confirm',
  VoidCallback? onReject,
  VoidCallback? onConfirm,
}) {
  showDialog(
    context: context,
    builder: (BuildContext context) => AlertDialog(
      backgroundColor: white,
      title: Stack(
        children: [
          Positioned(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              IconButton(
                color: transparent,
                splashColor: transparent,
                focusColor: transparent,
                onPressed: () {
                  Navigator.pop(context, true);
                },
                icon: SvgPicture.asset(ic_close, width: 20.0, color: black),
              ),
            ],
          ))
        ],
      ),
      titlePadding: EdgeInsets.fromLTRB(5.0, 10.0, 10.0, 0.0),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (icon != '')
            Padding(
              padding: EdgeInsets.only(bottom: 16.0),
              child: Image.asset(img_tmog_logo, width: 60.0),
            ),
          Text(contentMessage, textAlign: TextAlign.center, style: boldFontsStyle(fontSize: 16.0)),
        ],
      ),
      contentPadding: EdgeInsets.fromLTRB(26.0, 0, 35.0, 0),
      actions: <Widget>[
        ListTile(
          title: Row(
            children: [
              Expanded(
                flex: 1,
                child: TextButton(
                  style: TextButton.styleFrom(
                    padding: EdgeInsets.symmetric(vertical: 12.0),
                    textStyle: boldFontsStyle(
                      fontSize: 16.0,
                    ),
                    foregroundColor: black,
                    side: BorderSide(color: black),
                    shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))), // foreground
                  ),
                  onPressed: () {
                    Navigator.pop(context, true);
                  },
                  child: Text(rejectText),
                ),
              ),
              SizedBox(width: 8.0),
              Expanded(
                flex: 1,
                child: TextButton(
                  style: TextButton.styleFrom(
                    padding: EdgeInsets.symmetric(vertical: 12.0),
                    textStyle: boldFontsStyle(
                      fontSize: 16.0,
                    ),
                    foregroundColor: black,
                    backgroundColor: yellow,
                    side: BorderSide(color: yellow),
                    shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))), // foreground
                  ),
                  onPressed: () {
                    Navigator.pop(context, true);
                    if (onConfirm != null)
                      onConfirm();
                  },
                  child: Text(confirmText),
                ),
              ),
            ],
          ),
        ),
      ],
      actionsAlignment: MainAxisAlignment.center,
      actionsPadding: EdgeInsets.symmetric(vertical: 12.0),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
    ));
}

showDecisionWithFields(BuildContext context, String message, String falseButtonLabel, String trueButtonLabel, TextEditingController remarksController, {Widget? titleIcon, Color? buttonColor, Color? textColor} ) {
  bool clicked = false;
  return showDialog(
    context: context,
    builder: (context) => AlertDialog(
      backgroundColor: white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      title: Column(
        children: [
          Align(
            alignment: Alignment.centerRight,
            child: GestureDetector(
              onTap: () => Navigator.pop(context, false),
              child: SvgPicture.asset(ic_close, width: 24.0, color: black)
            ),
          ),
          if (titleIcon != null)
            titleIcon
        ],
      ),
      content: StatefulBuilder(
        builder: (context, StateSetter setState) {
            return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                message,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, height: 1.5),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 15.0),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Internal Remarks',
                    style: boldFontsStyle(fontSize: 14.0, color: grey, height: 1.29),
                  ),
                  SizedBox(height: 8.0),
                  // remark text field
                  TextFormField(
                    controller: remarksController,
                    cursorColor: black,
                    decoration: InputDecoration(
                      hintText: 'Fill In Internal Remarks',
                      hintStyle: italicFontsStyle(fontSize: 16.0, color: grey, height: 1.5),
                      contentPadding: EdgeInsets.symmetric(vertical: 6, horizontal: 16),
                      focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(6.0), borderSide: BorderSide(color: lightGrey)),
                      enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(6.0), borderSide: BorderSide(color: lightGrey)),
                    ),
                    minLines: 1,
                    maxLines: 2,
                  ),
                ],
              ),
              
              SizedBox(height: 32.0),
              Container(
                child: Opacity(
                  opacity: clicked ? 0.5 : 1.0,
                  child: ElevatedButton (
                    style: ElevatedButton.styleFrom(
                      backgroundColor: buttonColor ?? red,
                      // padding: EdgeInsets.symmetric(vertical: 10, horizontal: 70),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                      minimumSize: Size(300, 40)
                    ),
                    onPressed: () {
                      setState(() {
                        clicked = true;
                      });
                      Navigator.pop(context, true);
                    },
                    child: Text (
                      trueButtonLabel,
                      style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: textColor ?? white),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 8.0),
              Container(
                child: OutlinedButton (
                  style: OutlinedButton.styleFrom(
                    foregroundColor: black,
                    side: BorderSide(width: 1, color: black),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                    minimumSize: Size(300, 40)
                  ),
                  onPressed: () {
                    Navigator.pop(context, false);
                  },
                  child: Text (
                    falseButtonLabel,
                    style: boldFontsStyle(fontSize: 16.0, height: 1.5, color: black),
                  ),
                ),
              )
            ],
          );
        }
      ),
    ), 
  );
}