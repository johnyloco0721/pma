import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sources/utils/dialog_utils.dart';

class ErrorUtils {
  static handleError(DioException error) {
    String _errorMessage = "";
    switch (error.type) {
      case DioExceptionType.cancel:
        _errorMessage = 'Request was cancelled';
        break;
      case DioExceptionType.connectionTimeout:
        _errorMessage = 'Connection timeout';
        break;
      case DioExceptionType.connectionError:
        _errorMessage = 'Connection failed due to internet connection';
        break;
      case DioExceptionType.receiveTimeout:
        _errorMessage = 'Receive timeout in connection';
        break;
      case DioExceptionType.badResponse:
        if (error.response!.statusCode == 401) {
          _errorMessage = (error.response?.data['message'] ?? 'Authentication failed');
        } else {
          _errorMessage = "Received invalid status code: ${error.response?.statusCode}";
        }
        break;
      case DioErrorType.sendTimeout:
        _errorMessage = 'Receive timeout in send request';
        break;
      default:
        _errorMessage = 'Server Error , Please try again later';
        break;
    }
    return _errorMessage;
  }

  static handlePlatformError(BuildContext context, PlatformException error) {
    switch (error.code) {
      case 'camera_access_denied':
        return showDialog(
          context: context, 
          builder: (context) => CupertinoAlertDialog(
            title: Text('Error'),
            content: Text('Access to camera has been prohibited, please enable it in the Settings app to continue.'),
            actions: [
              CupertinoDialogAction(child: Text('Cancel'), onPressed: () => Navigator.of(context).pop(),),
              CupertinoDialogAction(child: Text('Settings'), onPressed: () => openAppSettings()),
            ],
          ),
        );
      case 'no_available_camera':
        return showError(
          context,
          'No cameras available for taking photos.'
        );
      case 'photo_access_denied':
        return showDialog(
          context: context, 
          builder: (context) => CupertinoAlertDialog(
            title: Text('Error'),
            content: Text('Access to photo library has been prohibited, please enable it in the Settings app to continue.'),
            actions: [
              CupertinoDialogAction(child: Text('Cancel'), onPressed: () => Navigator.of(context).pop()),
              CupertinoDialogAction(child: Text('Settings'), onPressed: () => openAppSettings()),
            ],
          ),
        );
      default:
        return showError(context, 'Unexpected error, please try again.');
    }
  }
}
