import 'package:flutter/material.dart';

// scroll to top method
scrollToTop(ScrollController controller) {
  if (controller.positions.isNotEmpty) {
    if (controller.offset > 3000.0) {
      controller.jumpTo(0.0);
    }
    else  {
      controller.animateTo(0.0, duration: Duration(milliseconds: 1000), curve: Curves.ease);
    }
  }
}